'use strict';

goog.provide('Blockly.FieldTextPopup');

goog.require('Blockly.DropDownDiv');
goog.require('Blockly.fieldRegistry');
goog.require('Blockly.FieldTextInput');
goog.require('Blockly.utils.aria');
goog.require('Blockly.utils.object');


/**
 * Custom FieldTextPopup using superClass_.
 * Opens a popup for text input.
 */

Blockly.FieldTextPopup = function(opt_value, opt_validator, opt_config) {
    // this.popupText = opt_value || ""
    this.maxDisplayLength = 31;
    Blockly.FieldTextPopup.superClass_.constructor.call(this, opt_value || "", opt_validator, opt_config);
}

// Inherit from Blockly.Field
Blockly.utils.object.inherits(Blockly.FieldTextPopup, Blockly.Field);

Blockly.FieldTextPopup.prototype.SERIALIZABLE = true;
Blockly.FieldTextPopup.prototype.EDITABLE = false;



/**
 * Show the editor (popup) when the field is clicked.
 */
Blockly.FieldTextPopup.prototype.showEditor_ = function() {
// Create the popup container
var popup = document.createElement("div");
popup.style.position = "fixed";
popup.style.top = "50%";
popup.style.left = "50%";
popup.style.transform = "translate(-50%, -50%)";
popup.style.backgroundColor = "#E8E8E8AB";
popup.style.border = "1px solid #ccc";
popup.style.padding = "20px";
popup.style.zIndex = "1000";
popup.style.boxShadow = "0 4px 8px rgba(0, 0, 0, 0.5)";

// Add a textarea for text input
var textarea = document.createElement("textarea");
textarea.value = this.getValue();
textarea.style.width = "300px";
textarea.style.height = "100px";
textarea.readOnly = true
textarea.className = "blocklyFieldPopupTextarea"
popup.appendChild(textarea);

// Add Save and Cancel buttons
var edit = document.createElement("button");
edit.textContent = Blockly.Msg.EDIT
edit.style.marginRight = "10px";
popup.appendChild(edit);

Blockly.DropDownDiv.getContentDiv().appendChild(popup);
Blockly.DropDownDiv.showPositionedByField(
    this, this.onPopupClose_.bind(this));


// Handle Save button click
var field = this; // Preserve reference to the field
edit.addEventListener("click", function() {
    if(textarea.readOnly){
        edit.textContent = Blockly.Msg.PROTECT
    }else{
        edit.textContent = Blockly.Msg.EDIT
    }
    textarea.readOnly = !textarea.readOnly;
});

this.onPopupOpen_(textarea)

};

Blockly.FieldTextPopup.prototype.onPopupClose_ = function(){
    if (this.onInputWrapper_) {
        Blockly.unbindEvent_(this.onInputWrapper_);
        this.onInputWrapper_ = null;
    }
}

Blockly.FieldTextPopup.prototype.onPopupOpen_ = function(target){
    this.onInputWrapper_ = Blockly.bindEventWithChecks_(target, 'input', this, this.onHtmlInput_.bind(this));
    this.onKeyDownWrapper_ = Blockly.bindEventWithChecks_(target, 'keydown', this, this.onHtmlInputKeyDown_.bind(this));
}

Blockly.FieldTextPopup.prototype.onHtmlInputKeyDown_ = function(e){
    if (e.key == 'Tab') {
        e.preventDefault();
        var start = e.target.selectionStart;
        var end = e.target.selectionEnd;
    
        // set textarea value to: text before caret + tab + text after caret
        e.target.value = e.target.value.substring(0, start) +
        "    " + e.target.value.substring(end);
    
        // put caret at right position again
        e.target.selectionStart = start+4
        e.target.selectionEnd = start + 4;
    }
}

Blockly.FieldTextPopup.prototype.onHtmlInput_ = function(e) {
    e.preventDefault()
    var popupText = e.target.value
    this.setValue(popupText);
};

Blockly.FieldTextPopup.fromJson = function(options) {
    var text = Blockly.utils.replaceMessageReferences(options['value']);
    return new Blockly.FieldTextPopup(text, undefined, options);
};

Blockly.FieldTextPopup.prototype.render_ = function() {
    Blockly.FieldTextPopup.superClass_.render_.call(this);
  
    if (!this.EDITABLE) {
      // Access the SVG element of the field
      var fieldGroup = this.getSvgRoot();
      if (fieldGroup) {
        // Find the background rectangle and change its style
        var background = fieldGroup.querySelector("rect");
        if (background) {
          background.setAttribute("fill", "#f0f0f0"); // Set the desired background color
          background.setAttribute("stroke", "#ccc"); // Optional: Set a border color
        }

        fieldGroup.setAttribute("class", "blocklyEditableText"); // not editable but we want text color to be black
        
      }
    }
  };

Blockly.Css.register([
    '.blocklyFieldPopupTextarea:read-only {',
      'background-color: #fffbfb;',
      'color: #484848',
    '}',
]);

// Register the custom field
Blockly.fieldRegistry.register("field_text_popup", Blockly.FieldTextPopup);