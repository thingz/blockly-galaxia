/**
 * @fileoverview Sprites blocks for Galaxia.
 */

Blockly.defineBlocksWithJsonArray([ // BEGIN JSON EXTRACT
    {
        type: "sprite_rectangle_create",
        message0: "%{BKY_SPRITE_GALAXIA_RECTANGLE_CREATE}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "r",
        },
        {
            type: "input_value",
            name: "X",
            check: "Number",
        },
        {
            type: "input_value",
            name: "Y",
            check: "Number",
        },
        {
            type: "input_value",
            name: "WIDTH",
            check: "Number",
        },
        {
            type: "input_value",
            name: "HEIGHT",
            check: "Number",
        },
        {
            type: "input_value",
            name: "COLOR",
            check: "Colour",
        },
        
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_RECTANGLE_CREATE_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_rectangle_set_x_y",
        message0: "%{BKY_SPRITE_GALAXIA_RECTANGLE_SET_X_Y}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "r",
        },
        {
            type: "field_dropdown",
            name: "X_Y",
            options: [
                ["X", "x"],
                ["Y", "y"],
                ["direction", "direction"],
            ],
        },
        {
            type: "input_value",
            name: "VALUE",
            check: "Number",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_RECTANGLE_SET_X_Y_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_rectangle_get_x_y",
        message0: "%{BKY_SPRITE_GALAXIA_RECTANGLE_GET_X_Y}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "r",
        },
        {
            type: "field_dropdown",
            name: "X_Y",
            options: [
                ["X", "x"],
                ["Y", "y"],
                ["direction", "direction"],
            ],
        },
        ],
        output: "Number",
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_RECTANGLE_GET_X_Y_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_rectangle_set_width_height",
        message0: "%{BKY_SPRITE_GALAXIA_RECTANGLE_SET_WIDTH_HEIGHT}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "r",
        },
        {
            type: "field_dropdown",
            name: "W_H",
            options: [
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_WIDTH}", "width"],
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_HEIGHT}", "height"],
            ],
        },
        {
            type: "input_value",
            name: "VALUE",
            check: "Number",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_RECTANGLE_SET_WIDTH_HEIGHT_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_rectangle_get_width_height",
        message0: "%{BKY_SPRITE_GALAXIA_RECTANGLE_GET_WIDTH_HEIGHT}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "r",
        },
        {
            type: "field_dropdown",
            name: "W_H",
            options: [
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_WIDTH}", "width"],
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_HEIGHT}", "height"],
            ],
        },
        ],
        output: "Number",
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_RECTANGLE_GET_WIDTH_HEIGHT_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_rectangle_set_color",
        message0: "%{BKY_SPRITE_GALAXIA_RECTANGLE_SET_COLOR}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "r",
        },
        {
            type: "input_value",
            name: "VALUE",
            check: "Colour",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_RECTANGLE_SET_COLOR_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_rectangle_get_color",
        message0: "%{BKY_SPRITE_GALAXIA_RECTANGLE_GET_COLOR}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "r",
        },
        ],
        output: "Colour",
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_RECTANGLE_GET_COLOR_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_rectangle_set_hidden",
        message0: "%{BKY_SPRITE_GALAXIA_RECTANGLE_SET_HIDDEN}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "r",
        },
        {
            type: "field_dropdown",
            name: "HIDDEN",
            options: [
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_HIDDEN}", "True"],
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_VISIBLE}", "False"],
            ],
        }
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_RECTANGLE_SET_HIDDEN_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_rectangle_get_hidden",
        message0: "%{BKY_SPRITE_GALAXIA_RECTANGLE_GET_HIDDEN}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "r",
        },
        ],
        output: "Boolean",
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_RECTANGLE_GET_HIDDEN_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_rectangle_move_by",
        message0: "%{BKY_SPRITE_GALAXIA_RECTANGLE_MOVE_BY}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "r",
        },
        {
            type: "input_value",
            name: "VALUE",
            check: "Number",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_RECTANGLE_MOVE_BY_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_rectangle_turn",
        message0: "%{BKY_SPRITE_GALAXIA_RECTANGLE_TURN}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "r",
        },
        {
            type: "field_dropdown",
            name: "DIRECTION",
            options: [
                ["%{BKY_SPRITE_GALAXIA_LEFT}", "-1"],
                ["%{BKY_SPRITE_GALAXIA_RIGHT}", "1"],
            ],
        },
        {
            type: "input_value",
            name: "VALUE",
            check: "Number",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_RECTANGLE_TURN_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_rectangle_bounce_if_edge",
        message0: "%{BKY_SPRITE_GALAXIA_RECTANGLE_BOUNCE_IF_EDGE}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "r",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_RECTANGLE_BOUNCE_IF_EDGE_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    //IMAGE
    {
        type: "sprite_image_create",
        message0: "%{BKY_SPRITE_GALAXIA_IMAGE_CREATE}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "img",
        },
        {
            type: "input_value",
            name: "X",
            check: "Number",
        },
        {
            type: "input_value",
            name: "Y",
            check: "Number",
        },
        {
            type: "input_value",
            name: "SCALE",
            check: "Number",
        },
        {
            type: "input_value",
            name: "PATH",
            check: "String",
        },
        
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_IMAGE_CREATE_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_image_set_x_y",
        message0: "%{BKY_SPRITE_GALAXIA_IMAGE_SET_X_Y}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "img",
        },
        {
            type: "field_dropdown",
            name: "X_Y",
            options: [
                ["X", "x"],
                ["Y", "y"],
                ["direction", "direction"],
            ],
        },
        {
            type: "input_value",
            name: "VALUE",
            check: "Number",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_IMAGE_SET_X_Y_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_image_get_x_y",
        message0: "%{BKY_SPRITE_GALAXIA_IMAGE_GET_X_Y}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "img",
        },
        {
            type: "field_dropdown",
            name: "X_Y",
            options: [
                ["X", "x"],
                ["Y", "y"],
                ["direction", "direction"],
            ],
        },
        ],
        output: "Number",
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_IMAGE_GET_X_Y_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_image_get_width_height",
        message0: "%{BKY_SPRITE_GALAXIA_IMAGE_GET_WIDTH_HEIGHT}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "img",
        },
        {
            type: "field_dropdown",
            name: "W_H",
            options: [
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_WIDTH}", "width"],
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_HEIGHT}", "height"],
            ],
        },
        ],
        output: "Number",
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_IMAGE_GET_WIDTH_HEIGHT_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_image_set_scale",
        message0: "%{BKY_SPRITE_GALAXIA_IMAGE_SET_SCALE}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "img",
        },
        {
            type: "input_value",
            name: "SCALE",
            check: "Number",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_IMAGE_SET_SCALE_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_image_get_scale",
        message0: "%{BKY_SPRITE_GALAXIA_IMAGE_GET_SCALE}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "img",
        },
        ],
        output: "Number",
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_IMAGE_GET_WIDTH_HEIGHT_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_image_set_hidden",
        message0: "%{BKY_SPRITE_GALAXIA_IMAGE_SET_HIDDEN}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "img",
        },
        {
            type: "field_dropdown",
            name: "HIDDEN",
            options: [
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_HIDDEN}", "True"],
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_VISIBLE}", "False"],
            ],
        }
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_IMAGE_SET_HIDDEN_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_image_get_hidden",
        message0: "%{BKY_SPRITE_GALAXIA_IMAGE_GET_HIDDEN}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "img",
        },
        ],
        output: "Boolean",
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_IMAGE_GET_HIDDEN_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_image_move_by",
        message0: "%{BKY_SPRITE_GALAXIA_IMAGE_MOVE_BY}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "img",
        },
        {
            type: "input_value",
            name: "VALUE",
            check: "Number",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_IMAGE_MOVE_BY_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_image_turn",
        message0: "%{BKY_SPRITE_GALAXIA_IMAGE_TURN}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "img",
        },
        {
            type: "field_dropdown",
            name: "DIRECTION",
            options: [
                ["%{BKY_SPRITE_GALAXIA_LEFT}", "-1"],
                ["%{BKY_SPRITE_GALAXIA_RIGHT}", "1"],
            ],
        },
        {
            type: "input_value",
            name: "VALUE",
            check: "Number",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_IMAGE_TURN_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_image_bounce_if_edge",
        message0: "%{BKY_SPRITE_GALAXIA_IMAGE_BOUNCE_IF_EDGE}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "img",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_IMAGE_BOUNCE_IF_EDGE_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    //ICON
    {
        type: "sprite_icon_create",
        message0: "%{BKY_SPRITE_GALAXIA_ICON_CREATE}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "icon",
        },
        {
            type: "input_value",
            name: "X",
            check: "Number",
        },
        {
            type: "input_value",
            name: "Y",
            check: "Number",
        },
        {
            type: "input_value",
            name: "SCALE",
            check: "Number",
        },
        {
            type: "input_value",
            name: "COLOR",
            check: "Colour",
        },
        {
            type: "field_dropdown",
            name: "NAME",
            options: [
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_CROSS}", "cross"],
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_CIRCLE}", "circle"],
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_EMOJI}", "emoji"],
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_HEART}", "heart"],
            ],
        }
        
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_ICON_CREATE_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_icon_set_x_y",
        message0: "%{BKY_SPRITE_GALAXIA_ICON_SET_X_Y}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "icon",
        },
        {
            type: "field_dropdown",
            name: "X_Y",
            options: [
                ["X", "x"],
                ["Y", "y"],
                ["direction", "direction"],
            ],
        },
        {
            type: "input_value",
            name: "VALUE",
            check: "Number",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_ICON_SET_X_Y_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_icon_get_x_y",
        message0: "%{BKY_SPRITE_GALAXIA_ICON_GET_X_Y}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "icon",
        },
        {
            type: "field_dropdown",
            name: "X_Y",
            options: [
                ["X", "x"],
                ["Y", "y"],
                ["direction", "direction"],
            ],
        },
        ],
        output: "Number",
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_IMAGE_GET_X_Y_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_icon_get_width_height",
        message0: "%{BKY_SPRITE_GALAXIA_ICON_GET_WIDTH_HEIGHT}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "icon",
        },
        {
            type: "field_dropdown",
            name: "W_H",
            options: [
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_WIDTH}", "width"],
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_HEIGHT}", "height"],
            ],
        },
        ],
        output: "Number",
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_ICON_GET_WIDTH_HEIGHT_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_icon_set_scale",
        message0: "%{BKY_SPRITE_GALAXIA_ICON_SET_SCALE}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "icon",
        },
        {
            type: "input_value",
            name: "SCALE",
            check: "Number",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_ICON_SET_SCALE_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_icon_get_scale",
        message0: "%{BKY_SPRITE_GALAXIA_ICON_GET_SCALE}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "icon",
        },
        ],
        output: "Number",
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_ICON_GET_SCALE_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },
    {
        type: "sprite_icon_set_color",
        message0: "%{BKY_SPRITE_GALAXIA_ICON_SET_COLOR}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "r",
        },
        {
            type: "input_value",
            name: "VALUE",
            check: "Colour",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_ICON_SET_COLOR_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_icon_get_color",
        message0: "%{BKY_SPRITE_GALAXIA_ICON_GET_COLOR}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "r",
        },
        ],
        output: "Colour",
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_ICON_GET_COLOR_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_icon_set_hidden",
        message0: "%{BKY_SPRITE_GALAXIA_ICON_SET_HIDDEN}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "icon",
        },
        {
            type: "field_dropdown",
            name: "HIDDEN",
            options: [
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_HIDDEN}", "True"],
                ["%{BKY_SPRITE_GALAXIA_RECTANGLE_VISIBLE}", "False"],
            ],
        }
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_ICON_SET_HIDDEN_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_icon_get_hidden",
        message0: "%{BKY_SPRITE_GALAXIA_ICON_GET_HIDDEN}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "img",
        },
        ],
        output: "Boolean",
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_ICON_GET_HIDDEN_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_icon_move_by",
        message0: "%{BKY_SPRITE_GALAXIA_ICON_MOVE_BY}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "icon",
        },
        {
            type: "input_value",
            name: "VALUE",
            check: "Number",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_ICON_MOVE_BY_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_icon_turn",
        message0: "%{BKY_SPRITE_GALAXIA_ICON_TURN}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "icon",
        },
        {
            type: "field_dropdown",
            name: "DIRECTION",
            options: [
                ["%{BKY_SPRITE_GALAXIA_LEFT}", "-1"],
                ["%{BKY_SPRITE_GALAXIA_RIGHT}", "1"],
            ],
        },
        {
            type: "input_value",
            name: "VALUE",
            check: "Number",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_ICON_TURN_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_icon_bounce_if_edge",
        message0: "%{BKY_SPRITE_GALAXIA_ICON_BOUNCE_IF_EDGE}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "icon",
        },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_ICON_BOUNCE_IF_EDGE_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_collision",
        message0: "%{BKY_SPRITE_GALAXIA_COLLISION}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "s1",
        },
        {
            type: "field_variable",
            name: "VAR2",
            variable: "s2",
        },
        ],
        output: "Boolean",
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_COLLISION_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

    {
        type: "sprite_border_collision",
        message0: "%{BKY_SPRITE_GALAXIA_BORDER_COLLISION}",
        args0: [
        {
            type: "field_variable",
            name: "VAR",
            variable: "s1",
        },
        {
            type: "field_dropdown",
            name: "BORDER",
            options: [
                ["%{BKY_SPRITE_GALAXIA_BORDER_NORTH}", "n"],
                ["%{BKY_SPRITE_GALAXIA_BORDER_SOUTH}", "s"],
                ["%{BKY_SPRITE_GALAXIA_BORDER_WEST}", "w"],
                ["%{BKY_SPRITE_GALAXIA_BORDER_EAST}", "e"],
            ],
        }
        ],
        output: "Boolean",
        style: "sprite_blocks",
        tooltip: "%{BKY_SPRITE_GALAXIA_BORDER_COLLISION_TOOLTIP}",
        helpUrl: THINGZ_SITE,
        extensions: ["contextMenu_newGetVariableBlock", "block_field_variable_colour"],
    },

]); // END JSON EXTRACT (Do not delete this comment.)