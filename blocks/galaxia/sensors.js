/**
 * @fileoverview Sensors blocks for Galaxia.
 */

Blockly.defineBlocksWithJsonArray([
  // BEGIN JSON EXTRACT

  /* Begin board sensors blocks*/

  // BLOCK GET ACCELERATION
  {
    type: "sensors_galaxia_getAcceleration",
    message0: "%{BKY_SENSORS_GALAXIA_GETACCELERATION_TITLE}",
    args0: [
      {
        type: "field_dropdown",
        name: "AXIS",
        options: [
          ["x", "x"],
          ["y", "y"],
          ["z", "z"],
          ["[x,y,z]", "get_values"],
        ],
      },
    ],
    output: "Number",
    style: "sensors_blocks",
    tooltip: "%{BKY_SENSORS_GALAXIA_GETACCELERATION_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK ON ACCELEROMETER EVENT
  {
    type: "sensors_galaxia_onAccelerometerEvent",
    message0: "%{BKY_SENSORS_GALAXIA_ONACCELEROMETEREVENT_TITLE}",
    args0: [
      {
        type: "field_dropdown",
        name: "GESTURE",
        options: [
          ["%{BKY_SENSORS_GALAXIA_ONACCELEROMETEREVENT_SHAKE}", "shake"],
          ["%{BKY_SENSORS_GALAXIA_ONACCELEROMETEREVENT_UP}", "up"],
          ["%{BKY_SENSORS_GALAXIA_ONACCELEROMETEREVENT_LEFT}", "left"],
          ["%{BKY_SENSORS_GALAXIA_ONACCELEROMETEREVENT_RIGHT}", "right"],
          ["%{BKY_SENSORS_GALAXIA_ONACCELEROMETEREVENT_DOWN}", "down"],
          ["%{BKY_SENSORS_GALAXIA_ONACCELEROMETEREVENT_FACEUP}", "face up"],
          ["%{BKY_SENSORS_GALAXIA_ONACCELEROMETEREVENT_FACEDOWN}", "face down"],
          ["%{BKY_SENSORS_GALAXIA_ONACCELEROMETEREVENT_FREEFALL}", "freefall"],
          ["%{BKY_SENSORS_GALAXIA_ONACCELEROMETEREVENT_3G}", "3g"],
          ["%{BKY_SENSORS_GALAXIA_ONACCELEROMETEREVENT_6G}", "6g"],
          ["%{BKY_SENSORS_GALAXIA_ONACCELEROMETEREVENT_8G}", "8g"],
        ],
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    style: "sensors_blocks",
    tooltip: "%{SENSORS_GALAXIA_ONACCELEROMETEREVENT_TOOLTIP}",
    helpUrl: THINGZ_SITE,
    extensions: ["disable_lower_blocks"],
  },

  // BLOCK GET MAGNETIC FORCE
  {
    type: "sensors_galaxia_getMagneticForce",
    message0: "%{BKY_SENSORS_GALAXIA_GETMAGNETICFORCE_TITLE}",
    args0: [
      {
        type: "field_dropdown",
        name: "AXIS",
        options: [
          ["x", "x"],
          ["y", "y"],
          ["z", "z"],
          ["[x,y,z]", "get_values"],
        ],
      },
    ],
    output: "Number",
    style: "sensors_blocks",
    tooltip: "%{BKY_SENSORS_GALAXIA_GETMAGNETICFORCE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GET ACCELERATION SIGNAL
  {
    type: "sensors_galaxia_getAccelerationSignal",
    message0: "%{BKY_SENSORS_GALAXIA_GETACCELERATION_SIGNAL_TITLE}",
    args0: [
      {
        type: "field_dropdown",
        name: "AXIS",
        options: [
          ["x", "x"],
          ["y", "y"],
          ["z", "z"],
          ["[x,y,z]", "get_values"],
        ],
      },
    ],
    output: "Number",
    style: "sensors_blocks",
    tooltip: "%{BKY_SENSORS_GALAXIA_GETACCELERATION_SIGNAL_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK GET MAGNETIC FORCE SIGNAL
  {
    type: "sensors_galaxia_getMagneticForceSignal",
    message0: "%{BKY_SENSORS_GALAXIA_GETMAGNETICFORCE_SIGNAL_TITLE}",
    args0: [
      {
        type: "field_dropdown",
        name: "AXIS",
        options: [
          ["x", "x"],
          ["y", "y"],
          ["z", "z"],
        ],
      },
    ],
    output: "Number",
    style: "sensors_blocks",
    tooltip: "%{BKY_SENSORS_GALAXIA_GETMAGNETICFORCE_SIGNAL_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK GET LIGHT SIGNAL
  {
    "type": "sensors_getLightSignal",
    "message0": "%{BKY_SENSORS_GETLIGHT_SIGNAL_TITLE}",
    "output": "Number",
    "style": "sensors_blocks",
    "tooltip": "%{BKY_SENSORS_GETLIGHT_SIGNAL_TOOLTIP}",
    "helpUrl": THINGZ_SITE
  },

  // BLOCK GET TEMPERATURE SIGNAL
  {
    "type": "sensors_getTemperatureSignal",
    "message0": "%{BKY_SENSORS_GETTEMPERATURE_SIGNAL_TITLE}",
    "args0": [],
    "output": "Number",
    "style": "sensors_blocks",
    "tooltip": "%{BKY_SENSORS_GETTEMPERATURE_SIGNAL_TOOLTIP}",
    "helpUrl": THINGZ_SITE
  },
  
]); // END JSON EXTRACT (Do not delete this comment.)
