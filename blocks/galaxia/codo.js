/**
 * @fileoverview Robots blocks for Micro:bit.
 */

Blockly.defineBlocksWithJsonArray([ // BEGIN JSON EXTRACT

    /* Begin codo blocks */


    // BLOCK CODO CONTROL GO
    {
        "type": "robots_galaxia_setCodoGo",
        "message0": "%{BKY_ROBOTS_GALAXIA_CODO_GO_TITLE}",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "DIR",
            "options": [
                ["%{BKY_ROBOTS_CODO_GO_FORWARD}", "forward"],
                ["%{BKY_ROBOTS_CODO_GO_BACKWARD}", "backward"]
            ]
            }, {
                "type": "input_value",
                "name": "SPEED",
                "check": "Number"
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_GALAXIA_CODO_GO_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK CODO CONTROL MOTOR
    {
        "type": "robots_galaxia_controlCodoMotor",
        "message0": "%{BKY_ROBOTS_GALAXIA_CODO_CONTROLMOTOR_TITLE}",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "MOTOR",
                "options": [
                    ["%{BKY_ROBOTS_CODO_RIGHT}", "right"],
                    ["%{BKY_ROBOTS_CODO_LEFT}", "left"]
                ]
            }, 
            {
                "type": "field_dropdown",
                "name": "DIR",
                "options": [
                    ["↻", "forward"],
                    ["↺", "backward"]
                ]
            }, 
            {
                "type": "input_value",
                "name": "SPEED",
                "check": "Number"
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_GALAXIA_CODO_CONTROLMOTOR_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK CODO STOP MOTORS
    {
        "type": "robots_galaxia_stopCodoMotors",
        "message0": "%{BKY_ROBOTS_GALAXIA_CODO_STOPMOTORS_TITLE}",
        "args0": [ 
            {
                "type": "field_dropdown",
                "name": "MOTOR",
                "options": [
                    ["%{BKY_ROBOTS_CODO_RIGHT}", "left"],
                    ["%{BKY_ROBOTS_CODO_LEFT}", "right"],
                    ["%{BKY_ROBOTS_CODO_RIGHT&LEFT}", "both"]
                ]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_GALAXIA_CODO_STOPMOTORS_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

]); // END JSON EXTRACT (Do not delete this comment.)