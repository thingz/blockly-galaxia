/**
 * @fileoverview Motor blocks.
 */

Blockly.defineBlocksWithJsonArray([ // BEGIN JSON EXTRACT

    /* Begin codo blocks */

    // BLOCK MOTOR SPEED
    {
        "type": "motor_galaxia_speed",
        "message0": "%{BKY_MOTOR_GALAXIA_SPEED_TITLE}",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "MOTOR",
            "options": [
                ["M1", "drf0548.Motors[\"M1\"]"],
                ["M2", "drf0548.Motors[\"M2\"]"],
                ["M3", "drf0548.Motors[\"M3\"]"],
                ["M4", "drf0548.Motors[\"M4\"]"]
            ]
            },
            {
                "type": "field_dropdown",
                "name": "DIR",
                "options": [
                    ["%{BKY_MOTOR_GALAXIA_CLOCKWISE}", "drf0548.Dir[\"CW\"]"],
                    ["%{BKY_MOTOR_GALAXIA_COUNTER_CLOCKWISE}", "drf0548.Dir[\"CCW\"]"],
                ]
            }, {
                "type": "input_value",
                "name": "SPEED",
                "check": "Number"
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "motor_blocks",
        "tooltip": "%{BKY_MOTOR_GALAXIA_SPEED_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },
    // MOTOR STOP
    {
        "type": "motor_galaxia_stop",
        "message0": "%{BKY_MOTOR_GALAXIA_STOP_TITLE}",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "MOTOR",
            "options": [
                ["M1", "drf0548.Motors[\"M1\"]"],
                ["M2", "drf0548.Motors[\"M2\"]"],
                ["M3", "drf0548.Motors[\"M3\"]"],
                ["M4", "drf0548.Motors[\"M4\"]"]
            ]
            },
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "motor_blocks",
        "tooltip": "%{BKY_MOTOR_GALAXIA_STOP_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // MOTOR STOP ALL
    {
        "type": "motor_galaxia_stop_all",
        "message0": "%{BKY_MOTOR_GALAXIA_STOP_ALL_TITLE}",
        "args0": [
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "motor_blocks",
        "tooltip": "%{BKY_MOTOR_GALAXIA_STOP_ALL_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // MOTOR SERVO
    {
        "type": "motor_galaxia_servo",
        "message0": "%{BKY_MOTOR_GALAXIA_SERVO_TITLE}",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "MOTOR",
            "options": [
                ["S1", "drf0548.Servos[\"S1\"]"],
                ["S2", "drf0548.Servos[\"S2\"]"],
                ["S3", "drf0548.Servos[\"S3\"]"],
                ["S4", "drf0548.Servos[\"S4\"]"],
                ["S5", "drf0548.Servos[\"S5\"]"],
                ["S6", "drf0548.Servos[\"S6\"]"],
                ["S7", "drf0548.Servos[\"S7\"]"],
                ["S8", "drf0548.Servos[\"S8\"]"]
            ]
            }
            , {
                "type": "input_value",
                "name": "ANGLE",
                "check": "Number"
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "motor_blocks",
        "tooltip": "%{BKY_MOTOR_GALAXIA_SERVO_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK MOTOR STEP 42
    { 
        "type": "motor_galaxia_step_42",
        "message0": "%{BKY_MOTOR_GALAXIA_STEP_42_TITLE}",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "MOTOR",
            "options": [
                ["M1-M2", "drf0548.Steppers[\"M1_M2\"]"],
                ["M3-M4", "drf0548.Steppers[\"M3_M4\"]"],
            ]
            },
            {
                "type": "field_dropdown",
                "name": "DIR",
                "options": [
                    ["%{BKY_MOTOR_GALAXIA_CLOCKWISE}", "drf0548.Dir[\"CW\"]"],
                    ["%{BKY_MOTOR_GALAXIA_COUNTER_CLOCKWISE}", "drf0548.Dir[\"CCW\"]"],
                ]
            }, {
                "type": "input_value",
                "name": "DEGREE",
                "check": "Number"
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "motor_blocks",
        "tooltip": "%{BKY_MOTOR_GALAXIA_STEP_42_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK MOTOR STEP 42 TURN
    {
        "type": "motor_galaxia_step_42_turn",
        "message0": "%{BKY_MOTOR_GALAXIA_STEP_42_TURN_TITLE}",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "MOTOR",
            "options": [
                ["M1-M2", "drf0548.Steppers[\"M1_M2\"]"],
                ["M3-M4", "drf0548.Steppers[\"M3_M4\"]"],
            ]
            },
            {
                "type": "field_dropdown",
                "name": "DIR",
                "options": [
                    ["%{BKY_MOTOR_GALAXIA_CLOCKWISE}", "drf0548.Dir[\"CW\"]"],
                    ["%{BKY_MOTOR_GALAXIA_COUNTER_CLOCKWISE}", "drf0548.Dir[\"CCW\"]"],
                ]
            }, {
                "type": "input_value",
                "name": "TURN",
                "check": "Number"
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "motor_blocks",
        "tooltip": "%{BKY_MOTOR_GALAXIA_STEP_42_TURN_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK MOTOR STEP 28
    { 
        "type": "motor_galaxia_step_28",
        "message0": "%{BKY_MOTOR_GALAXIA_STEP_28_TITLE}",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "MOTOR",
            "options": [
                ["M1-M2", "drf0548.Steppers[\"M1_M2\"]"],
                ["M3-M4", "drf0548.Steppers[\"M3_M4\"]"],
            ]
            },
            {
                "type": "field_dropdown",
                "name": "DIR",
                "options": [
                    ["%{BKY_MOTOR_GALAXIA_CLOCKWISE}", "drf0548.Dir[\"CW\"]"],
                    ["%{BKY_MOTOR_GALAXIA_COUNTER_CLOCKWISE}", "drf0548.Dir[\"CCW\"]"],
                ]
            }, {
                "type": "input_value",
                "name": "DEGREE",
                "check": "Number"
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "motor_blocks",
        "tooltip": "%{BKY_MOTOR_GALAXIA_STEP_28_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK MOTOR STEP 28 TURN
    {
        "type": "motor_galaxia_step_28_turn",
        "message0": "%{BKY_MOTOR_GALAXIA_STEP_28_TURN_TITLE}",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "MOTOR",
            "options": [
                ["M1-M2", "drf0548.Steppers[\"M1_M2\"]"],
                ["M3-M4", "drf0548.Steppers[\"M3_M4\"]"],
            ]
            },
            {
                "type": "field_dropdown",
                "name": "DIR",
                "options": [
                    ["%{BKY_MOTOR_GALAXIA_CLOCKWISE}", "drf0548.Dir[\"CW\"]"],
                    ["%{BKY_MOTOR_GALAXIA_COUNTER_CLOCKWISE}", "drf0548.Dir[\"CCW\"]"],
                ]
            }, {
                "type": "input_value",
                "name": "TURN",
                "check": "Number"
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "motor_blocks",
        "tooltip": "%{BKY_MOTOR_GALAXIA_STEP_28_TURN_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK MOTOR STEP DUAL
    {
        "type": "motor_galaxia_step_dual",
        "message0": "%{BKY_MOTOR_GALAXIA_STEP_DUAL_TITLE}",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "TYPE",
                "options": [
                    ["42", "drf0548.Stepper[\"Ste1\"]"],
                    ["28", "drf0548.Stepper[\"Ste2\"]"],
                ]
            },
            {
                "type": "field_dropdown",
                "name": "DIR1",
                "options": [
                    ["%{BKY_MOTOR_GALAXIA_CLOCKWISE}", "drf0548.Dir[\"CW\"]"],
                    ["%{BKY_MOTOR_GALAXIA_COUNTER_CLOCKWISE}", "drf0548.Dir[\"CCW\"]"],
                ]
            }, {
                "type": "input_value",
                "name": "DEGREE1",
                "check": "Number"
            },
            {
                "type": "field_dropdown",
                "name": "DIR2",
                "options": [
                    ["%{BKY_MOTOR_GALAXIA_CLOCKWISE}", "drf0548.Dir[\"CW\"]"],
                    ["%{BKY_MOTOR_GALAXIA_COUNTER_CLOCKWISE}", "drf0548.Dir[\"CCW\"]"],
                ]
            }, {
                "type": "input_value",
                "name": "DEGREE2",
                "check": "Number"
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "motor_blocks",
        "tooltip": "%{BKY_MOTOR_GALAXIA_STEP_DUAL_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK MOTOR STEP DUAL
    {
        "type": "motor_galaxia_step_dual_turn",
        "message0": "%{BKY_MOTOR_GALAXIA_STEP_DUAL_TURN_TITLE}",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "TYPE",
                "options": [
                    ["42", "drf0548.Stepper[\"Ste1\"]"],
                    ["28", "drf0548.Stepper[\"Ste2\"]"],
                ]
            },
            {
                "type": "field_dropdown",
                "name": "DIR1",
                "options": [
                    ["%{BKY_MOTOR_GALAXIA_CLOCKWISE}", "drf0548.Dir[\"CW\"]"],
                    ["%{BKY_MOTOR_GALAXIA_COUNTER_CLOCKWISE}", "drf0548.Dir[\"CCW\"]"],
                ]
            }, {
                "type": "input_value",
                "name": "TURN1",
                "check": "Number"
            },
            {
                "type": "field_dropdown",
                "name": "DIR2",
                "options": [
                    ["%{BKY_MOTOR_GALAXIA_CLOCKWISE}", "drf0548.Dir[\"CW\"]"],
                    ["%{BKY_MOTOR_GALAXIA_COUNTER_CLOCKWISE}", "drf0548.Dir[\"CCW\"]"],
                ]
            }, {
                "type": "input_value",
                "name": "TURN2",
                "check": "Number"
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "motor_blocks",
        "tooltip": "%{BKY_MOTOR_GALAXIA_STEP_DUAL_TURN_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

]); // END JSON EXTRACT (Do not delete this comment.)