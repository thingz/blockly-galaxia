/**
 * @fileoverview Robots blocks for Micro:bit.
 */

Blockly.defineBlocksWithJsonArray([ // BEGIN JSON EXTRACT

    /* Begin Neopixel blocks */


    // BLOCK NEOPIXEL DEFINE
    {
        "type": "galaxia_neopixel_define",
        "message0": "%{BKY_GALAXIA_NEOPIXEL_DEFINE_TITLE}",
        "args0": [
            {
                "type": "input_value",
                "name": "LEN",
                "check": "Number"
            },
            {
                "type": "field_dropdown",
                "name": "IO",
                "options": Blockly.Constants.inputOutput.PINS
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "neopixel_blocks",
        "tooltip": "%{BKY_GALAXIA_NEOPIXEL_DEFINE_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK NEOPIXEL SET LED rgb
    {
        "type": "galaxia_neopixel_set_led_rgb",
        "message0": "%{BKY_GALAXIA_NEOPIXEL_SET_LED_RGB_TITLE}",
        "args0": [
            {
                "type": "input_value",
                "name": "INDEX",
                "check": "Number"
            },
            {
                "type": "input_value",
                "name": "R",
                "check": "Number"
            },
            {
                "type": "input_value",
                "name": "G",
                "check": "Number"
            },
            {
                "type": "input_value",
                "name": "B",
                "check": "Number"
            },
            {
                "type": "field_dropdown",
                "name": "IO",
                "options": Blockly.Constants.inputOutput.PINS
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "neopixel_blocks",
        "tooltip": "%{BKY_GALAXIA_NEOPIXEL_SET_LED_RGB_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK NEOPIXEL SET LED COLOR
    {
        "type": "galaxia_neopixel_set_led_color",
        "message0": "%{BKY_GALAXIA_NEOPIXEL_SET_LED_COLOR_TITLE}",
        "args0": [ 
            {
                "type": "input_value",
                "name": "INDEX",
                "check": "Number"
            },
            {
                "type": "input_value",
                "name": "COLOR",
                "check": "Colour"
            },
            {
                "type": "field_dropdown",
                "name": "IO",
                "options": Blockly.Constants.inputOutput.PINS
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "style": "neopixel_blocks",
        "tooltip": "%{BKY_GALAXIA_NEOPIXEL_SET_LED_COLOR_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK NEOPIXEL SET LEDS rgb
    {
        "type": "galaxia_neopixel_set_leds_rgb",
        "message0": "%{BKY_GALAXIA_NEOPIXEL_SET_LEDS_RGB_TITLE}",
        "args0": [
            {
                "type": "input_value",
                "name": "R",
                "check": "Number"
            },
            {
                "type": "input_value",
                "name": "G",
                "check": "Number"
            },
            {
                "type": "input_value",
                "name": "B",
                "check": "Number"
            },
            {
                "type": "field_dropdown",
                "name": "IO",
                "options": Blockly.Constants.inputOutput.PINS
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "neopixel_blocks",
        "tooltip": "%{BKY_GALAXIA_NEOPIXEL_SET_LEDS_RGB_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK NEOPIXEL SET LEDS COLOR
    {
        "type": "galaxia_neopixel_set_leds_color",
        "message0": "%{BKY_GALAXIA_NEOPIXEL_SET_LEDS_COLOR_TITLE}",
        "args0": [ 
            {
                "type": "input_value",
                "name": "COLOR",
                "check": "Colour"
            },
            {
                "type": "field_dropdown",
                "name": "IO",
                "options": Blockly.Constants.inputOutput.PINS
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "style": "neopixel_blocks",
        "tooltip": "%{BKY_GALAXIA_NEOPIXEL_SET_LEDS_COLOR_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

]); // END JSON EXTRACT (Do not delete this comment.)