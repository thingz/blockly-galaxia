/**
 * @format
 * @fileoverview Input/Output blocks for Galaxia.
 */

 Blockly.Constants.inputOutput = Object.create(null);

Blockly.Constants.inputOutput.PINS = [
  ["P0", "P0"],
  ["P1", "P1"],
  ["P2", "P2"],
  ["P6", "P6"],
  ["P7", "P7"],
  ["P8", "P8"],
  ["P12", "P12"],
  ["P13", "P13"],
  ["P14", "P14"],
  ["P15", "P15"],
  ["P16", "P16"],
  ["P19", "P19"],
  ["P20", "P20"],
]

Blockly.defineBlocksWithJsonArray([
  // BEGIN JSON EXTRACT

  /* Begin Galaxia blocks */

  // BLOCK IO PAUSE
  {
    type: "io_galaxia_pause",
    message0: "%{BKY_IO_GALAXIA_PAUSE_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "TIME",
        check: "Number",
      },
      {
        type: "field_dropdown",
        name: "UNIT",
        options: [
          ["%{BKY_IO_PAUSE_SECOND}", "1"],
          ["%{BKY_IO_PAUSE_MILLISECOND}", "0.001"],
        ],
      },
    ],
    previousStatement: null,
    nextStatement: null,
    style: "io_blocks",
    tooltip: "%{BKY_IO_GALAXIA_PAUSE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  {
    type: "io_galaxia_timestamp",
    message0: "%{BKY_IO_GALAXIA_TIMESTAMP_TITLE}",
    args0: [
        {
            type: "field_dropdown",
            name: "UNIT",
            options: [
                ["(s)", "1000.0"],
                ["(ms)", "1.0"],
            ],
        },
    ],
    output: "Number",
    style: "io_blocks",
    tooltip: "%{BKY_IO_GALAXIA_TIMESTAMP_TOOLTIP}",
    helpUrl: THINGZ_SITE,
},

  // BLOCK "IS BUTTON PRESSED" DEFINED IN MICRO:BIT FILE
  // BLOCK "IF BUTTON PRESSED" DEFINED IN MICRO:BIT FILE

  // BLOCK ON BUTTON EVENT
  {
    type: "io_onButtonEvent",
    message0: "%{BKY_IO_ONBUTTONEVENT_TITLE}",
    args0: [
      {
        type: "field_dropdown",
        name: "BUTTON",
        options: [
          ["A", "a"],
          ["B", "b"],
          // ["A+B", "a+b"],
        ],
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    style: "io_blocks",
    tooltip: "%{BKY_IO_ONBUTTONEVENT_TOOLTIP}",
    helpUrl: THINGZ_SITE,
    extensions: ["disable_lower_blocks"],
  },

  // BLOCK IS TOUCH-SENSITIVE BUTTON TOUCHED
  {
    type: "io_isTouchSensitiveButtonTouched",
    message0: "%{BKY_IO_ISTOUCHSENSITIVEBUTTONTOUCHED_TITLE}",
    args0: [
      {
        type: "field_dropdown",
        name: "BUTTON",
        options: [
          ["%{BKY_IO_TOUCH_NORTH}", "n"],
          ["%{BKY_IO_TOUCH_SOUTH}", "s"],
          ["%{BKY_IO_TOUCH_EST}", "e"],
          ["%{BKY_IO_TOUCH_WEST}", "w"],
        ],
      },
      {
        type: "field_dropdown",
        name: "STATE",
        options: [
          ["%{BKY_IO_ISPRESSED}", "is_"],
          ["%{BKY_IO_WASPRESSED}", "was_"],
        ],
      },
    ],
    output: "Boolean",
    style: "io_blocks",
    tooltip: "%{BKY_IO_ISTOUCHSENSITIVEBUTTONTOUCHED_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK IF TOUCH-SENSITIVE BUTTON TOUCHED
  {
    type: "io_ifTouchSensitiveButtonTouched",
    message0: "%{BKY_IO_IFTOUCHSENSITIVEBUTTONTOUCHED_TITLE}",
    args0: [
      {
        type: "field_dropdown",
        name: "BUTTON",
        options: [
          ["%{BKY_IO_TOUCH_NORTH}", "n"],
          ["%{BKY_IO_TOUCH_SOUTH}", "s"],
          ["%{BKY_IO_TOUCH_EST}", "e"],
          ["%{BKY_IO_TOUCH_WEST}", "w"],
        ],
      },
      {
        type: "field_dropdown",
        name: "STATE",
        options: [
          ["%{BKY_IO_ISPRESSED}", "is_"],
          ["%{BKY_IO_WASPRESSED}", "was_"],
        ],
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    previousStatement: null,
    nextStatement: null,
    style: "io_blocks",
    tooltip: "%{BKY_IO_IFTOUCHSENSITIVEBUTTONTOUCHED_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK ON TOUCH-SENSITIVE BUTTON EVENT
  {
    type: "io_onTouchSensitiveButtonEvent",
    message0: "%{BKY_IO_ONTOUCHSENSITIVEBUTTONEVENT_TITLE}",
    args0: [
      {
        type: "field_dropdown",
        name: "BUTTON",
        options: [
          ["%{BKY_IO_TOUCH_NORTH}", "n"],
          ["%{BKY_IO_TOUCH_SOUTH}", "s"],
          ["%{BKY_IO_TOUCH_EST}", "e"],
          ["%{BKY_IO_TOUCH_WEST}", "w"],
        ],
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    style: "io_blocks",
    tooltip: "%{BKY_IO_ONTOUCHSENSITIVEBUTTONEVENT_TOOLTIP}",
    helpUrl: THINGZ_SITE,
    extensions: ["disable_lower_blocks"],
  },

  // BLOCK ON/OFF BOOLEAN
  {
    type: "io_galaxia_digital_signal",
    message0: "%{BKY_IO_DIGITALSIGNAL_TITLE}",
    args0: [
        {
            type: "field_dropdown",
            name: "BOOL",
            options: [
                ["%{BKY_IO_DIGITALSIGNAL_HIGH}", "HIGH"],
                ["%{BKY_IO_DIGITALSIGNAL_LOW}", "LOW"],
            ],
        },
    ],
    output: "Boolean",
    style: "io_blocks",
    tooltip: "%{BKY_IO_DIGITALSIGNAL_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK READ DIGITAL PIN
  {
    type: "io_galaxia_readDigitalPin",
    message0: "%{BKY_IO_READDIGITALPIN_TITLE}",
    args0: [
        {
            type: "field_dropdown",
            name: "PIN",
            options: Blockly.Constants.inputOutput.PINS,
        },
    ],
    style: "io_blocks",
    output: "Boolean",
    tooltip: "%{BKY_IO_READDIGITALPIN_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK WRITE DIGITAL PIN
  {
      type: "io_galaxia_writeDigitalPin",
      message0: "%{BKY_IO_WRITEDIGITALPIN_TITLE}",
      args0: [
          {
              type: "field_dropdown",
              name: "PIN",
              options: Blockly.Constants.inputOutput.PINS,
          },
          {
              type: "input_value",
              name: "STATE",
              check: "Boolean",
          },
      ],
      inputsInline: true,
      previousStatement: null,
      nextStatement: null,
      style: "io_blocks",
      tooltip: "%{BKY_IO_WRITEDIGITALPIN_TOOLTIP}",
      helpUrl: THINGZ_SITE,
  },

  // BLOCK PWM PIN
  {
    type: "io_galaxia_pwm",
    message0: "%{BKY_IO_PWM_TITLE}",
    args0: [
        {
            type: "field_dropdown",
            name: "PIN",
            options: Blockly.Constants.inputOutput.PINS,
        },
        {
            type: "input_value",
            name: "DUTY",
            check: "Number",
        },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "io_blocks",
    tooltip: "%{BKY_IO_PWM_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK PWM PIN
  {
    type: "io_galaxia_pwm_with_freq",
    message0: "%{BKY_IO_PWM_WITH_FREQ_TITLE}",
    args0: [
        {
            type: "field_dropdown",
            name: "PIN",
            options: Blockly.Constants.inputOutput.PINS,
        },
        {
            type: "input_value",
            name: "DUTY",
            check: "Number",
        },
        {
          type: "input_value",
          name: "FREQ",
          check: "Number",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "io_blocks",
    tooltip: "%{BKY_IO_PWM_WITH_FREQ_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK READ ANALOG PIN
  {
      type: "io_galaxia_readAnalogPin",
      message0: "%{BKY_IO_READANALOGPIN_TITLE}",
      args0: [
          {
              type: "field_dropdown",
              name: "PIN",
              options: Blockly.Constants.inputOutput.PINS,
          },
      ],
      output: "Number",
      style: "io_blocks",
      tooltip: "%{BKY_IO_READANALOGPIN_TOOLTIP}",
      helpUrl: THINGZ_SITE,
  },

  // BLOCK IS TOUCH-SENSITIVE BUTTON TOUCHED SIGNAL
  {
    type: "io_isTouchSensitiveButtonTouchedSignal",
    message0: "%{BKY_IO_ISTOUCHSENSITIVEBUTTONTOUCHED_SIGNAL_TITLE}",
    args0: [
      {
        type: "field_dropdown",
        name: "BUTTON",
        options: [
          ["%{BKY_IO_TOUCH_NORTH}", "n"],
          ["%{BKY_IO_TOUCH_SOUTH}", "s"],
          ["%{BKY_IO_TOUCH_EST}", "e"],
          ["%{BKY_IO_TOUCH_WEST}", "w"],
        ],
      },
      {
        type: "field_dropdown",
        name: "STATE",
        options: [
          ["%{BKY_IO_ISPRESSED}", "is_"],
          ["%{BKY_IO_WASPRESSED}", "was_"],
        ],
      },
    ],
    output: "Number",
    style: "io_blocks",
    tooltip: "%{BKY_IO_ISTOUCHSENSITIVEBUTTONTOUCHED_SIGNAL_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK READ DIGITAL PIN SIGNAL
  {
    type: "io_galaxia_readDigitalPinSignal",
    message0: "%{BKY_IO_READDIGITALPIN_SIGNAL_TITLE}",
    args0: [
        {
            type: "field_dropdown",
            name: "PIN",
            options: Blockly.Constants.inputOutput.PINS,
        },
    ],
    style: "io_blocks",
    output: "Number",
    tooltip: "%{BKY_IO_READDIGITALPIN_SIGNAL_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK IS BUTTON PRESSED SIGNAL
  {
    type: "io_isButtonPressedSignal",
    message0: "%{BKY_IO_ISBUTTONPRESSED_SIGNAL_TITLE}",
    args0: [
        {
            type: "field_dropdown",
            name: "BUTTON",
            options: [
                ["A", "a"],
                ["B", "b"],
                // ["A+B", "a+b"],
            ],
        },
        {
            type: "field_dropdown",
            name: "STATE",
            options: [
                ["%{BKY_IO_ISPRESSED}", "is_"],
                ["%{BKY_IO_WASPRESSED}", "was_"],
            ],
        },
    ],
    output: "Number",
    style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
    tooltip: "%{BKY_IO_ISBUTTONPRESSED_SIGNAL_TOOLTIP}",
    helpUrl: VITTASCIENCE_SITE,
},

  
]); // END JSON EXTRACT (Do not delete this comment.)

// Disable all instances of the block present in the workspace, except the highest one
Blockly.Constants.inputOutput.DISABLE_LOWER_BLOCKS_EXTENSION = function () {
  this.setOnChange(onUpadate_);
  /** Met à jour l'état d'activation des blocs supérieurs */
  function onUpadate_(event) {
    if (
      ((event.type == Blockly.Events.BLOCK_MOVE || event.type == Blockly.Events.BLOCK_CREATE || (event.type == Blockly.Events.BLOCK_CHANGE && event.extensionNoUpdate !== true)) && event.blockId === this.id) ||
      (event.type == Blockly.Events.BLOCK_DELETE && event.oldXml.outerHTML.includes(this.type))
    ) {
      // Lister les blocs supérieurs
      var topBlocks = this.workspace.getTopBlocks();
      var sameTypeBlocks = [];
      var blocksOrderedByParam = {};
      var highestBlocks = [];

      // Parmis tous les blocs supérieurs rendus,
      // sauvegarder les blocs du même type que celui qui a déclenché l'événement (this)
      for (var i = 0, block; (block = topBlocks[i]); i++) {
        if (block.type === this.type && block.rendered == true) {
          sameTypeBlocks.push(block);
        }
      }

      var blockDropDownValue = undefined;

      /** Parmis sameTypeBlocks, regrouper les blocs possédant un paramètre identique
       * dans un tableau associatif blocksOrderedByParam
       * Les blocs ne possédant pas d'option seront simplement triés par ordre d'apparition
       */
      for (var i = 0, block; (block = sameTypeBlocks[i]); i++) {
        blockDropDownValue = getDropDownValue_(block);
        if (blockDropDownValue) {
          if (!blocksOrderedByParam[blockDropDownValue]) {
            blocksOrderedByParam[blockDropDownValue] = [];
          }
          blocksOrderedByParam[blockDropDownValue].push(block);
        } else {
          if (!blocksOrderedByParam["default"]) {
            blocksOrderedByParam["default"] = [];
          }
          blocksOrderedByParam["default"].push(block);
        }
      }

      var highestBlock = undefined;

      // Pour chaque groupe / clé, savegarder le bloc le plus haut
      for (var key in blocksOrderedByParam) {
        highestBlock = undefined;
        for (var block of blocksOrderedByParam[key]) {
          if (
            highestBlock === undefined ||
            block.getRelativeToSurfaceXY().y < highestBlock.getRelativeToSurfaceXY().y ||
            (block.getRelativeToSurfaceXY().y == highestBlock.getRelativeToSurfaceXY().y && block.getRelativeToSurfaceXY().x < highestBlock.getRelativeToSurfaceXY().x)
          ) {
            highestBlock = block;
          }
        }
        highestBlocks.push(highestBlock);
      }

      // met à jour le statut d'activation de chaque bloc et refait le rendu du block et de ses enfants
      for (var block of sameTypeBlocks) {
        var previousState = block.disabled;
        if (!highestBlocks.includes(block)) {
          block.disabled = true;
        } else {
          block.disabled = false;
        }
        if (block.disabled != previousState) {
          // childBlock include the block itself
          for (var i = 0, childBlock; (childBlock = block.getDescendants(true)[i]); i++) {
            childBlock.disabled = block.disabled;
            childBlock.initSvg();
          }
        }
      }
      
      /**
       * "Fire" d'événenement nécessaire !
       * Notions : 
       * - Le code peut être généré automatiquement suite à certains événements.
       * - La génération de code est alors faite AVANT l'exécution de cette extension.
       * Suite à l'activation d'un bloc dû à cette extension :
       * Pour que le code généré automatiquement le prenne en compte, il est nécessaire de lancer
       * un événement en fin de processus pour déclencher une génération de code qui prend en compte le bloc nouvellement actif.
       * @param {boolean} extensionNoUpdate Paramètre utilisé pour ne pas boucler indéfiniment dans cette fonction d'Update.
       */
      var newEvent = new Blockly.Events.BlockChange(block, "field", "BUTTON", "fromExtensionUpdate", "");
      newEvent.extensionNoUpdate = true;
      Blockly.Events.fire(newEvent);
    }
  }

  function getDropDownValue_(block) {
    var i = 0;
    var trouve = undefined;
    while (i < block.inputList.length && !trouve) {
      var j = 0;
      while (j < block.inputList[i].fieldRow.length && !trouve) {
        if (block.inputList[i].fieldRow[j] instanceof Blockly.FieldDropdown) {
          trouve = block.inputList[i].fieldRow[j].value_;
        }
        j++;
      }
      i++;
    }
    return trouve;
  }
};

// Initialization extensions
Blockly.Extensions.register("disable_lower_blocks", Blockly.Constants.inputOutput.DISABLE_LOWER_BLOCKS_EXTENSION);
