/**
 * @fileoverview Actuators blocks for Galaxia.
 */

Blockly.defineBlocksWithJsonArray([
  // BEGIN JSON EXTRACT

  // BLOCK GALAXIA SET FRENQUENCY
  {
    type: "actuators_play_frequency",
    message0: "%{BKY_ACTUATORS_PLAY_FREQUENCY_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "FREQ",
        check: "Number",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sound_blocks" : "actuators_blocks",
    tooltip: "%{BKY_ACTUATORS_PLAY_FREQUENCY_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },
  // BLOCK GALAXIA PLAY SET FRENQUENCY WITH DURATION
  {
    type: "actuators_galaxia_play_frequency_with_duration",
    message0: "%{BKY_ACTUATORS_GALAXIA_PLAY_FREQUENCY_WITH_DURATION_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "FREQUENCY",
        check: "Number",
      },
      {
        type: "input_value",
        name: "DURATION",
        check: "Number",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "actuators_blocks",
    tooltip: "%{BKY_ACTUATORS_GALAXIA_PLAY_FREQUENCY_WITH_DURATION_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },
  // BLOCK GALAXIA SET VOLUME
  {
    type: "actuators_galaxia_play_set_volume",
    message0: "%{BKY_ACTUATORS_GALAXIA_PLAY_SET_VOLUME_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "VOLUME",
        check: "Number",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "actuators_blocks",
    tooltip: "%{BKY_ACTUATORS_GALAXIA_PLAY_SET_VOLUME_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },
  // BLOCK GALAXIA PLAY SAMPLE
  {
    type: "actuators_galaxia_play_sample",
    message0: "%{BKY_ACTUATORS_GALAXIA_PLAY_SAMPLE_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "FILE",
        check: "String",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "actuators_blocks",
    tooltip: "%{BKY_ACTUATORS_GALAXIA_PLAY_SAMPLE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },
]); // END JSON EXTRACT (Do not delete this comment.)

// Blockly.Constants.Actuators = Object.create(null);

Blockly.Blocks["actuators_galaxia_sound_power"] = {
  /**
   * Block to play a selected frequency or mute the sound
   * @this {Blockly.Block}
   */
  init: function () {
    this._savedFrenquency = 999;
    this.SWITCH_OPTIONS = [
      [Blockly.Msg["ACTUATORS_BUZZER_ON"], "ON"],
      [Blockly.Msg["ACTUATORS_BUZZER_OFF"], "OFF"],
    ];
    this.setStyle("actuators_blocks");
    this.setInputsInline(true);
    this.setOutput(false);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.updateBlock_(true);
    this.setOnChange(this.onFreqUpdate_);
    this.setTooltip(function () {
      return Blockly.Msg["ACTUATORS_BUZZER_ON_TOOLTIP"];
    });
  },

  /**
   * Create or delete an input for the frequency value.
   * @param {boolean} isFreq True if the frequency input should exist.
   * @private
   * @this {Blockly.Block}
   */
  updateBlock_: function (isFreq) {
    this.saveFreq_();
    // Destroy old inputs.
    if (this.getInput("FREQUENCY")) this.removeInput("FREQUENCY");
    if (this.getInput("SWITCH")) this.removeInput("SWITCH");
    if (this.getInput("TEXT")) this.removeInput("TEXT");
    if (this.getInput("HZ")) this.removeInput("HZ");
    this.appendDummyInput("SWITCH");
    // Create either a value 'Frequency' input and it's associated texte or append the correct text
    if (isFreq) {
      // Create a new shadow block
      var numberShadowBlock = this.workspace.newBlock("math_number");
      numberShadowBlock.setShadow(true);
      numberShadowBlock.getField("NUM").setValue(this._savedFrenquency);
      // Render it
      numberShadowBlock.initSvg();
      numberShadowBlock.render();
      // get an output connection
      var blockOutputConnection = numberShadowBlock.outputConnection;
      // create text elements and input block
      this.appendDummyInput("TEXT").appendField(Blockly.Msg["ACTUATORS_BUZZER_ON_TITLE"]);
      this.appendValueInput("FREQUENCY").setCheck("Number");
      this.appendDummyInput("HZ").appendField("Hz");
      // get an input connection
      var inputConnection = this.getInput("FREQUENCY").connection;
      inputConnection.connect(blockOutputConnection);
    } else {
      this.appendDummyInput("TEXT").appendField(Blockly.Msg["ACTUATORS_BUZZER_OFF_TITLE"]);
    }
    var menu = new Blockly.FieldDropdown(this.SWITCH_OPTIONS, function (value) {
      var newAt = value == "ON";
      // The 'isFreq' variable is available due to this function being a closure.
      if (newAt != isFreq) {
        var block = this.getSourceBlock();
        block.updateBlock_(newAt);
        // This menu has been destroyed and replaced. Update the replacement.
        block.setFieldValue(value, "SWITCH");
        block.setStyle("actuators_blocks");
        return null;
      }
      return undefined;
    });
    this.getInput("SWITCH").appendField(menu, "SWITCH");
  },

  /**
   * Block update callback
   * @param {!Blockly.Events.Abstract} event The event that triggered this listener.
   * @private
   * @this {Blockly.Block}
   */
  onFreqUpdate_: function (event) {
    if ((event.type == Blockly.Events.BLOCK_CREATE && this.id != event.blockId) || (event.type != Blockly.Events.BLOCK_CREATE && event.type != Blockly.Events.BLOCK_CHANGE)) {
      return;
    }
    this.saveFreq_();
  },

  /**
   * Saves the last frequency value entered manually
   * @private
   * @this {Blockly.Block}
   */
  saveFreq_: function () {
    if (this.getChildren()) {
      for (var i = 0; i < this.getChildren().length; i++) {
        if (this.getChildren()[i].isShadow()) {
          this._savedFrenquency = this.getChildren()[i].getField("NUM").getValue();
        }
      }
    }
  },
};
