/**
 * @fileoverview Kitronik blocks.
 */

Blockly.defineBlocksWithJsonArray([ // BEGIN JSON EXTRACT

    /* Begin Maqueen blocks */


    //KITRONIK_ACCESS_MOVE
    {
        "type": "kitronik_access_move",
        "message0": "%{BKY_KITRONIK_ACCESS_MOVE_TITLE}",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "DIR",
            "options": [
                ["%{BKY_KITRONIK_ACCESS_UP}", "0"],
                ["%{BKY_KITRONIK_ACCESS_DOWN}", "90"]
            ]
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "kitronik_blocks",
        "tooltip": "%{BKY_KITRONIK_ACCESS_MOVE_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    //KITRONIK_ACCESS_SOUND
    {
        "type": "kitronik_access_sound",
        "message0": "%{BKY_KITRONIK_ACCESS_SOUND_TITLE}",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "DURATION",
            "options": [
                ["%{BKY_KITRONIK_ACCESS_SHORT}", "0.2"],
                ["%{BKY_KITRONIK_ACCESS_LONG}", "1"]
            ]
            },
            {
                "type": "input_value",
                "name": "TIMES",
                "check": "Number"
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "kitronik_blocks",
        "tooltip": "%{BKY_KITRONIK_ACCESS_SOUND_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    //KITRONIK_LAMP_DEADBAND

    {
        "type": "kitronik_lamp_deadband",
        "message0": "%{BKY_KITRONIK_LAMP_DEADBAND_TITLE}",
        "args0": [
            
        ],
        "output": "Number",
        "style": "kitronik_blocks",
        "tooltip": "%{BKY_KITRONIK_LAMP_DEADBAND_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    //KITRONIK_LAMP_READ_LIGHT_LEVEL

    {
        "type": "kitronik_lamp_read_light_level",
        "message0": "%{BKY_KITRONIK_LAMP_READ_LIGHT_LEVEL_TITLE}",
        "args0": [
            
        ],
        "output": "Number",
        "style": "kitronik_blocks",
        "tooltip": "%{BKY_KITRONIK_LAMP_READ_LIGHT_LEVEL_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    //KITRONIK_LAMP_SET_LIGHT

    {
        "type": "kitronik_lamp_set_light",
        "message0": "%{BKY_KITRONIK_LAMP_SET_LIGHT_TITLE}",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "STATE",
            "options": [
                ["%{BKY_KITRONIK_LAMP_ON}", "True"],
                ["%{BKY_KITRONIK_LAMP_OFF}", "False"]
            ]
            },
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "kitronik_blocks",
        "tooltip": "%{BKY_KITRONIK_LAMP_SET_LIGHT_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },
    
    //KITRONIK_LAMP_SET_LAMP_DEADBAND

    {
        "type": "kitronik_lamp_set_lamp_deadband",
        "message0": "%{BKY_KITRONIK_LAMP_SET_LAMP_DEADBAND_TITLE}",
        "args0": [
            {
                "type": "input_value",
                "name": "PERCENT",
                "check": "Number"
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "kitronik_blocks",
        "tooltip": "%{BKY_KITRONIK_LAMP_SET_LAMP_DEADBAND_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },
    
    //KITRONIK_STOP_SET_TRAFFIC_LIGHT

    {
        "type": "kitronik_stop_set_traffic_light",
        "message0": "%{BKY_KITRONIK_STOP_SET_TRAFFIC_LIGHT_TITLE}",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "STATE",
            "options": [
                ["%{BKY_KITRONIK_STOP_TRAFFIC_LIGHT_STATE_STOP}", "stop"],
                ["%{BKY_KITRONIK_STOP_TRAFFIC_LIGHT_STATE_GET_READY}", "get_ready"],
                ["%{BKY_KITRONIK_STOP_TRAFFIC_LIGHT_STATE_GO}", "go"],
                ["%{BKY_KITRONIK_STOP_TRAFFIC_LIGHT_STATE_READY_TO_STOP}", "ready_to_stop"]
            ]
            },
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "kitronik_blocks",
        "tooltip": "%{BKY_KITRONIK_STOP_SET_TRAFFIC_LIGHT_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },
    
    //KITRONIK_STOP_SET_COLOR
    {
        "type": "kitronik_stop_set_color",
        "message0": "%{BKY_KITRONIK_STOP_SET_COLOR_TITLE}",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "STATE",
                "options": [
                    ["%{BKY_KITRONIK_STOP_ON}", "True"],
                    ["%{BKY_KITRONIK_STOP_OFF}", "False"],
                ]
            },
            {
            "type": "field_dropdown",
            "name": "COLOR",
            "options": [
                ["%{BKY_KITRONIK_STOP_COLOR_RED}", "0"],
                ["%{BKY_KITRONIK_STOP_COLOR_ORANGE}", "1"],
                ["%{BKY_KITRONIK_STOP_COLOR_GREEN}", "2"],
            ]
            },
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "kitronik_blocks",
        "tooltip": "%{BKY_KITRONIK_STOP_SET_COLOR_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

]); // END JSON EXTRACT (Do not delete this comment.)