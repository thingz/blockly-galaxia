Blockly.defineBlocksWithJsonArray([  
    // BLOCK GALAXIA TIMER START
    {
      type: "time_galaxia_timer_start",
      message0: "%{BKY_TIME_GALAXIA_TIMER_START_TITLE}",
      args0: [
        
      ],
      previousStatement: null,
      nextStatement: null,
      style: "time_blocks",
      tooltip: "%{BKY_TIME_GALAXIA_TIMER_START_TOOLTIP}",
      helpUrl: THINGZ_SITE,
    },

    // BLOCK GALAXIA TIMER MEASURE
    {
        type: "time_galaxia_timer_measure",
        message0: "%{BKY_TIME_GALAXIA_TIMER_MEASURE_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "UNIT",
                options: [
                    ["%{BKY_TIME_GALAXIA_TIMER_MEASURE_S}", "1000000000"],
                    ["%{BKY_TIME_GALAXIA_TIMER_MEASURE_MS}", "1000000"],
                    ["%{BKY_TIME_GALAXIA_TIMER_MEASURE_US}", "1000"],
                ],
            },
        ],
        output: "Number",
        style: "time_blocks",
        tooltip: "%{BKY_TIME_GALAXIA_TIMER_MEASURE_TOOLTIP}",
        helpUrl: THINGZ_SITE,
      }
])