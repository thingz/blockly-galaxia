/**
 * @fileoverview Display blocks for Galaxia.
 */

Blockly.defineBlocksWithJsonArray([
  // BEGIN JSON EXTRACT

  // BLOCK SHOW
  {
    type: "display_galaxia_show",
    message0: "%{BKY_DISPLAY_GALAXIA_SHOW_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "VALUE",
      },
    ],
    previousStatement: null,
    nextStatement: null,
    style: "display_blocks",
    tooltip: "%{BKY_DISPLAY_GALAXIA_SHOW_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK SHOW
  {
    type: "display_galaxia_clear",
    message0: "%{BKY_DISPLAY_GALAXIA_CLEAR_TITLE}",
    previousStatement: null,
    nextStatement: null,
    style: "display_blocks",
    tooltip: "%{BKY_DISPLAY_GALAXIA_CLEAR_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK SET GALAXIA LED COLORS
  {
    type: "display_galaxia_led_set_colors",
    message0: "%{BKY_DISPLAY_GALAXIA_SET_LED_COLORS_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "RED",
        check: "Number",
      },
      {
        type: "input_value",
        name: "GREEN",
        check: "Number",
      },
      {
        type: "input_value",
        name: "BLUE",
        check: "Number",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "display_blocks",
    tooltip: "%{BKY_DISPLAY_GALAXIA_SET_LED_COLORS_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK SET GALAXIA LED RED INTENSITY
  {
    type: "display_galaxia_led_set_red",
    message0: "%{BKY_DISPLAY_GALAXIA_LED_RED_CONTROL_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "RED",
        check: "Number",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "display_blocks",
    tooltip: "%{BKY_DISPLAY_GALAXIA_LED_RED_CONTROL_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK SET GALAXIA LED GREEN INTENSITY
  {
    type: "display_galaxia_led_set_green",
    message0: "%{BKY_DISPLAY_GALAXIA_LED_GREEN_CONTROL_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "GREEN",
        check: "Number",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "display_blocks",
    tooltip: "%{BKY_DISPLAY_GALAXIA_LED_GREEN_CONTROL_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK SET GALAXIA LED BLUE INTENSITY
  {
    type: "display_galaxia_led_set_blue",
    message0: "%{BKY_DISPLAY_GALAXIA_LED_BLUE_CONTROL_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "BLUE",
        check: "Number",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "display_blocks",
    tooltip: "%{BKY_DISPLAY_GALAXIA_LED_BLUE_CONTROL_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK GET GALAXIA LED RED INTENSITY
  {
    type: "display_galaxia_led_get_red",
    message0: "%{BKY_DISPLAY_GALAXIA_LED_GET_RED_TITLE}",
    output: "Number",
    style: "display_blocks",
    tooltip: "%{BKY_DISPLAY_GALAXIA_LED_GET_RED_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK GET GALAXIA LED GREEN INTENSITY
  {
    type: "display_galaxia_led_get_green",
    message0: "%{BKY_DISPLAY_GALAXIA_LED_GET_GREEN_TITLE}",
    output: "Number",
    style: "display_blocks",
    tooltip: "%{BKY_DISPLAY_GALAXIA_LED_GET_GREEN_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK GET GALAXIA LED BLUE INTENSITY
  {
    type: "display_galaxia_led_get_blue",
    message0: "%{BKY_DISPLAY_GALAXIA_LED_GET_BLUE_TITLE}",
    output: "Number",
    style: "display_blocks",
    tooltip: "%{BKY_DISPLAY_GALAXIA_LED_GET_BLUE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK PLOT ADD POINT
  {
    type: "display_galaxia_plot_add_point",
    message0: "%{BKY_DISPLAY_GALAXIA_PLOT_ADD_POINT_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "POINT",
        check: "Number",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "display_blocks",
    tooltip: "%{BKY_DISPLAY_GALAXIA_PLOT_ADD_POINT_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK PLOT SET Y SCALE
  {
    type: "display_galaxia_plot_set_y_scale",
    message0: "%{BKY_DISPLAY_GALAXIA_PLOT_SET_Y_SCALE_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "MIN",
        check: "Number",
      },
      {
        type: "input_value",
        name: "MAX",
        check: "Number",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "display_blocks",
    tooltip: "%{BKY_DISPLAY_GALAXIA_PLOT_SET_Y_SCALE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK DISPLAY SET MODE
  {
    type: "display_galaxia_set_mode",
    message0: "%{BKY_DISPLAY_GALAXIA_SET_MODE_TITLE}",
    args0: [
      {
        type: "field_dropdown",
        name: "MODE",
        options: [
          ["%{BKY_DISPLAY_GALAXIA_SET_MODE_PLOT}", "plot.show"],
          ["%{BKY_DISPLAY_GALAXIA_SET_MODE_CONSOLE}", "console.show"],
        ],
      },
    ],
    previousStatement: null,
    nextStatement: null,
    style: "display_blocks",
    tooltip: "%{BKY_DISPLAY_GALAXIA_SET_MODE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

   // BLOCK DISPLAY ANIMATE FUNCTION
   {
    type: "display_galaxia_animate_function",
    message0: "%{BKY_DISPLAY_GALAXIA_ANIMATE_FUNCTION_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "INTERVAL",
        check: "Number",
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    message2: "%{BKY_DISPLAY_GALAXIA_ANIMATE_FUNCTION_NEW_POINT}",
    args2: [
      {
        type: "input_value",
        align: "right",
        name: "POINT",
      },
    ],
    style: "display_blocks",
    tooltip: "%{BKY_DISPLAY_GALAXIA_ANIMATE_FUNCTION_TOOLTIP}",
    helpUrl: THINGZ_SITE,
    extensions: ["disable_lower_blocks"],
  },


]); // END JSON EXTRACT (Do not delete this comment.)

// Blockly.Constants.Display = Object.create(null);

// /**
//  * Performs setup of 'show_leds' block for screen checkboxes display.
//  * @this {Blockly.Block}
//  */
// Blockly.Constants.Display.DISPLAY_SHOW_LEDS_INIT_EXTENSION = function () {
//     for (var row = 0; row < 5; row++) {
//         var rowBoxes = this.appendDummyInput("ROW" + row);
//         for (var column = 0; column < 5; column++) {
//             rowBoxes.appendField(new Blockly.FieldCheckbox("FALSE"), "LED" + row + "" + column);
//         }
//     }
// };

// // Initialization extensions
// Blockly.Extensions.register("show_leds_screen_init", Blockly.Constants.Display.DISPLAY_SHOW_LEDS_INIT_EXTENSION);
