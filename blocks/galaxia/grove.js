Blockly.defineBlocksWithJsonArray([  
    
    // BLOCK GALAXIA GROVE TEMPERATURE
    {
        type: "grove_galaxia_temperature",
        message0: "%{BKY_GROVE_GALAXIA_TEMPERATURE_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "NUMBER",
                options: [
                    ["%{BKY_GROVE_GALAXIA_GROVE_1}", "P19"],
                    ["%{BKY_GROVE_GALAXIA_GROVE_2}", "P7"],
                ],
            },
        ],
        output: "Number",
        style: "grove_blocks",
        tooltip: "%{BKY_GROVE_GALAXIA_TEMPERATURE_TOOLTIP}",
        helpUrl: THINGZ_SITE,
    },

    //BLOCK GALAXIA GROVE ULTRASONIC
    {
        type: "grove_galaxia_ultrasonic",
        message0: "%{BKY_GROVE_GALAXIA_ULTRASONIC_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "NUMBER",
                options: [
                    ["%{BKY_GROVE_GALAXIA_GROVE_1}", "P19"],
                    ["%{BKY_GROVE_GALAXIA_GROVE_2}", "P7"],
                ],
            },

            {
                type: "field_dropdown",
                name: "UNIT",
                options: [
                    ["%{BKY_GROVE_GALAXIA_ULTRASONIC_CM}", "0.017"],
                    ["%{BKY_GROVE_GALAXIA_ULTRASONIC_MM}", "0.17"],
                ],
            },
        ],
        output: "Number",
        style: "grove_blocks",
        tooltip: "%{BKY_GROVE_GALAXIA_ULTRASONIC_TOOLTIP}",
        helpUrl: THINGZ_SITE,
    },

    //BLOCK GALAXIA GROVE DHT11
    {
        type: "grove_galaxia_dht11",
        message0: "%{BKY_GROVE_GALAXIA_DHT11_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: [
                    ["%{BKY_GROVE_GALAXIA_GROVE_1}", "P19"],
                    ["%{BKY_GROVE_GALAXIA_GROVE_2}", "P7"],
                ],
            },

            {
                type: "field_dropdown",
                name: "VALUE",
                options: [
                    ["%{BKY_GROVE_GALAXIA_DHT11_TEMP}", "temperature"],
                    ["%{BKY_GROVE_GALAXIA_DHT11_HUMIDITY}", "humidity"],
                ],
            },
        ],
        output: "Number",
        style: "grove_blocks",
        tooltip: "%{BKY_GROVE_GALAXIA_DHT11_TOOLTIP}",
        helpUrl: THINGZ_SITE,
    },
    {
        type: "grove_galaxia_dht11_signal",
        message0: "%{BKY_GROVE_GALAXIA_DHT11_SIGNAL_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: [
                    ["%{BKY_GROVE_GALAXIA_GROVE_1}", "P19"],
                    ["%{BKY_GROVE_GALAXIA_GROVE_2}", "P7"],
                ],
            },

            {
                type: "field_dropdown",
                name: "VALUE",
                options: [
                    ["%{BKY_GROVE_GALAXIA_DHT11_TEMP}", "temperature"],
                    ["%{BKY_GROVE_GALAXIA_DHT11_HUMIDITY}", "humidity"],
                ],
            },
        ],
        output: "Number",
        style: "grove_blocks",
        tooltip: "%{BKY_GROVE_GALAXIA_DHT11_SIGNAL_TOOLTIP}",
        helpUrl: THINGZ_SITE,
    },
    {
        type: "grove_galaxia_temperature_signal",
        message0: "%{BKY_GROVE_GALAXIA_TEMPERATURE_SIGNAL_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "NUMBER",
                options: [
                    ["%{BKY_GROVE_GALAXIA_GROVE_1}", "P19"],
                    ["%{BKY_GROVE_GALAXIA_GROVE_2}", "P7"],
                ],
            },
        ],
        output: "Number",
        style: "grove_blocks",
        tooltip: "%{BKY_GROVE_GALAXIA_TEMPERATURE_SIGNAL_TOOLTIP}",
        helpUrl: THINGZ_SITE,
    },
    {
        type: "grove_galaxia_servo",
        message0: "%{BKY_GROVE_GALAXIA_SERVO_TITLE}",
        args0: [
            {
                "type": "input_value",
                "name": "ANGLE",
                "check": "Number"
            },
            {
                type: "field_dropdown",
                name: "PIN",
                options: [
                    ["%{BKY_GROVE_GALAXIA_GROVE_1}", "P19"],
                    ["%{BKY_GROVE_GALAXIA_GROVE_2}", "P7"],
                    ["P0", "P0"],
                    ["P1", "P1"],
                    ["P2", "P2"],
                    ["P6", "P6"],
                    ["P7", "P7"],
                    ["P8", "P8"],
                    ["P12", "P12"],
                    ["P13", "P13"],
                    ["P14", "P14"],
                    ["P15", "P15"],
                    ["P16", "P16"],
                    ["P19", "P19"],
                    ["P20", "P20"],
                ],
            },
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        style: "grove_blocks",
        tooltip: "%{BKY_GROVE_GALAXIA_SERVO_TOOLTIP}",
        helpUrl: THINGZ_SITE,
    },
    {
        type: "grove_galaxia_ir_sender",
        message0: "%{BKY_GROVE_GALAXIA_IR_SENDER_TITLE}",
        args0: [
            {
                "type": "field_dropdown",
                "name": "ENABLE",
                "options": [
                    ["%{BKY_GROVE_GALAXIA_ENABLE}", "32768"],
                    ["%{BKY_GROVE_GALAXIA_DISABLE}", "0"],
                ],
            },
            {
                type: "field_dropdown",
                name: "PIN",
                options: [
                    ["%{BKY_GROVE_GALAXIA_GROVE_1}", "P19"],
                    ["%{BKY_GROVE_GALAXIA_GROVE_2}", "P7"],
                    ["P0", "P0"],
                    ["P1", "P1"],
                    ["P2", "P2"],
                    ["P6", "P6"],
                    ["P7", "P7"],
                    ["P8", "P8"],
                    ["P12", "P12"],
                    ["P13", "P13"],
                    ["P14", "P14"],
                    ["P15", "P15"],
                    ["P16", "P16"],
                    ["P19", "P19"],
                    ["P20", "P20"],
                ],
            },
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        style: "grove_blocks",
        tooltip: "%{BKY_GROVE_GALAXIA_IR_SENDER_TOOLTIP}",
        helpUrl: THINGZ_SITE,
    },
])