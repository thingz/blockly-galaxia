Blockly.defineBlocksWithJsonArray([ // BEGIN JSON EXTRACT

    /* Begin Maqueen blocks */


    // BLOCK DATA SET COLUMNS
    {
        "type": "data_galaxia_set_columns",
        "previousStatement": null,
        "nextStatement": null,
        "style": "data_blocks",
        "tooltip": "%{BKY_DATA_GALAXIA_SET_COLUMNS_TOOLTIP}",
        "extensions": [
          "block_buttons_plus_minus", 
          "data_set_columns"
        ],
        "mutator": "data_set_columns_mutator"
      },

    // BLOCK DATA ADD
    {
        "type": "data_galaxia_add",
        "previousStatement": null,
        "nextStatement": null,
        "style": "data_blocks",
        "tooltip": "%{BKY_DATA_GALAXIA_ADD_TOOLTIP}",
        "extensions": [
          "block_buttons_plus_minus", 
          "data_add"
        ],
        "mutator": "data_add_mutator"
      },

    // BLOCK DATA ADD
    {
      "type": "data_galaxia_add_v2",
      "previousStatement": null,
      "nextStatement": null,
      "style": "data_blocks",
      "tooltip": "%{BKY_DATA_GALAXIA_ADD_TOOLTIP}",
      "extensions": [
        "block_buttons_plus_minus", 
        "data_add_v2"
      ],
      "mutator": "data_add_mutator_v2"
    },
    
    // BLOCK DATA DELETE
    {
      "type": "data_galaxia_delete",
      "previousStatement": null,
      "nextStatement": null,
      "style": "data_blocks",
      "tooltip": "%{BKY_DATA_GALAXIA_DELETE_TOOLTIP}",
      "message0": "%{BKY_DATA_GALAXIA_DELETE_TITLE}",
    },

    {
      "type": "data_galaxia_column_value",
      "message0": "%{BKY_DATA_GALAXIA_COLUMN_VALUE_TITLE}",
      "args0": [
        {
          type: "input_value",
          name: "COLUMN",
          check: "String",
        },
        {
          type: "input_value",
          name: "VALUE",
        },
      ],
      "output": "Tuple",
      "style": "data_blocks",
      "inputsInline": true,
      "tooltip": "%{BKY_DATA_GALAXIA_COLUMN_VALUE_TOOLTIP}",
      helpUrl: THINGZ_SITE,
    }

]); // END JSON EXTRACT (Do not delete this comment.)

Blockly.Constants.Data = Object.create(null);

Blockly.Constants.Data.SET_COLUMN_EXTENSION = function() {
  this.itemCount_ = 1;
  this.updateShape_();
};

Blockly.Constants.Data.ADD_EXTENSION = function() {
    this.itemCount_ = 1;
    this.updateShape_();
};

Blockly.Constants.Data.ADD_EXTENSION_V2 = function() {
  this.itemCount_ = 1;
  this.firstUpdate = true
  this.updateShape_();
};


Blockly.Constants.Data.SET_COLUMN_MUTATOR_MIXIN = {
  /**
   * Create XML to represent list inputs.
   * @return {!Element} XML storage element.
   * @this {Blockly.Block}
   */
  mutationToDom: function() {
    var container = Blockly.utils.xml.createElement('mutation');
    container.setAttribute('columns', this.itemCount_);
    return container;
  },
  /**
   * Parse XML to restore the list inputs.
   * @param {!Element} xmlElement XML storage element.
   * @this {Blockly.Block}
   */
  domToMutation: function(xmlElement) {
    this.itemCount_ = parseInt(xmlElement.getAttribute('columns'), 10);
    this.updateShape_();
  },
  /**
   * Store pointers to any connected child blocks.
   */
  storeConnections_: function() {
    this.valueConnections_ = [];
    for (var i = 0; i < this.itemCount_; i++) {
      this.valueConnections_.push(this.getInput('ADD' + i).connection.targetConnection);
    }
  },
  restoreConnections_: function() {
    for (var i = 0; i < this.itemCount_; i++) {
      if(this.valueConnections_[i] && !this.valueConnections_[i].disposed){
        for(let input of this.inputList){
          if(input.name.endsWith(''+i+'')){
            input.connection.setShadowDom(null)
            input.connection.respawnShadow_()
            input.connection.connect(this.valueConnections_[i])
          }
        }
      }
    }

    // //remove shadow blocks
    // if(this.workspace && !this.workspace.isFlyout){
    //   let clean = false
    //   for(let i of this.workspace.topBlocks_){
    //     console.log(i, i.isShadow_, i.isShadow())
    //     if(i.isShadow_ === true){
    //       clean = true
    //       this.workspace.removeBlockById(i.id)
    //     }
    //   }
      // if(clean)
      //   this.workspace.clear()
      // console.log(clean)
    // }
  },
  addItem_: function() {
    this.storeConnections_();
    var update = function() {
      this.itemCount_++;
    };
    this.update_(update);
    this.restoreConnections_();
  },
  removeItem_: function() {
    this.storeConnections_();
    this.getInput('ADD' + (this.itemCount_-1)).connection.setShadowDom(null)
    this.getInput('ADD' + (this.itemCount_-1)).connection.respawnShadow_();
    var update = function() {
      this.itemCount_--;
    };
    this.update_(update);
    this.restoreConnections_();
  },
  update_: function(update) {
    return Blockly.Constants.Utils.UPDATE_BLOCK_MUTATOR_MIXIN(this, update);
  },
  /**
   * Modify this block to have the correct number of inputs.
   * @private
   * @this {Blockly.Block}
   */
  updateShape_: function() {
    var that = this;
    var remove = function() {
      that.removeItem_();
    };
    var add = function() {
      that.addItem_();
    };
    // Remove all inputs
    if (this.getInput('TOP')) this.removeInput('TOP');
    var i = 0;
    while (this.getInput('ADD' + i)) {
      this.removeInput('ADD' + i);
      i++;
    }
    // Update inputs
    var top = this.appendDummyInput('TOP');
    if (this.itemCount_ > 0) {
      top.appendField(Blockly.Msg['DATA_GALAXIA_SET_COLUMNS']);
      top.appendField(new Blockly.FieldImage(this.ADD_IMAGE_DATAURI, this.buttonSize, this.buttonSize, "*", add, false));
      if(this.itemCount_ > 1)
        top.appendField(new Blockly.FieldImage(this.REMOVE_IMAGE_DATAURI, this.buttonSize, this.buttonSize, "*", remove, false));
      for (var i = 0; i < this.itemCount_; i++) {
        let input = this.appendValueInput('ADD' + i);
        if(!this.valueConnections_ || !this.valueConnections_[i] || (this.valueConnections_[i] && (this.valueConnections_[i].sourceBlock_.isShadow))){
          const xml = `<shadow type="text"></shadow>`;
          let shadowDom = Blockly.Xml.textToDom(xml)
          shadowDom.setAttribute('id', Blockly.utils.genUid());
          input.connection.setShadowDom(shadowDom);
          input.connection.respawnShadow_();
        }
      }
    }
    
    /* Switch to vertical list when the list is too long */
    var showHorizontalList = this.itemCount_ <= 5;
    this.setInputsInline(showHorizontalList);
    this.setOutputShape(showHorizontalList ?
      Blockly.OUTPUT_SHAPE_ROUND : Blockly.OUTPUT_SHAPE_SQUARE);

    // //remove shadow blocks
    // if(this.workspace && !this.workspace.isFlyout){
    //   let clean = false
    //   for(let i of this.workspace.topBlocks_){
    //     console.log(i, i.isShadow_, i.isShadow())
    //     if(i.isShadow_ === true){
    //       clean = true
    //     }
    //   }

    //   console.log(clean)
    // }
  }
};

Blockly.Constants.Data.ADD_MUTATOR_MIXIN = {
  /**
     * Create XML to represent list inputs.
     * @return {!Element} XML storage element.
     * @this {Blockly.Block}
     */
  mutationToDom: function() {
    var container = Blockly.utils.xml.createElement('mutation');
    container.setAttribute('columns', this.itemCount_);
    return container;
  },
  /**
   * Parse XML to restore the list inputs.
   * @param {!Element} xmlElement XML storage element.
   * @this {Blockly.Block}
   */
  domToMutation: function(xmlElement) {
    this.itemCount_ = parseInt(xmlElement.getAttribute('columns'), 10);
    this.updateShape_();
  },
  /**
   * Store pointers to any connected child blocks.
   */
  storeConnections_: function() {
    this.valueConnections_ = [];
    for (var i = 0; i < this.itemCount_; i++) {
      this.valueConnections_.push(this.getInput('ADD_KEY' + i).connection.targetConnection);
      this.valueConnections_.push(this.getInput('ADD_VALUE' + i).connection.targetConnection);
    }
  },
  restoreConnections_: function() {

    for (var i = 0; i < this.itemCount_; i++) {
      if(this.valueConnections_[i*2] && !this.valueConnections_[i*2].disposed){
        for(let input of this.inputList){
          if(input.name.endsWith('ADD_KEY'+i+'')){
            input.connection.setShadowDom(null)
            input.connection.respawnShadow_()
            input.connection.connect(this.valueConnections_[i*2])
          }
        }
      }
      if(this.valueConnections_[(i*2)+1] && !this.valueConnections_[(i*2)+1].disposed){
        for(let input of this.inputList){
          if(input.name.endsWith('ADD_VALUE'+i+'')){
            input.connection.setShadowDom(null)
            input.connection.respawnShadow_()
            input.connection.connect(this.valueConnections_[(i*2)+1])
          }
        }
      }
    }
  },
  addItem_: function() {
    this.storeConnections_();
    var update = function() {
      this.itemCount_++;
    };
    this.update_(update);
    this.restoreConnections_();
  },
  removeItem_: function() {
    this.storeConnections_();
    this.getInput('ADD_KEY' + (this.itemCount_-1)).connection.setShadowDom(null)
    this.getInput('ADD_KEY' + (this.itemCount_-1)).connection.respawnShadow_()
    this.getInput('ADD_VALUE' + (this.itemCount_-1)).connection.setShadowDom(null)
    this.getInput('ADD_VALUE' + (this.itemCount_-1)).connection.respawnShadow_()
    var update = function() {
      this.itemCount_--;
    };
    this.update_(update);
    this.restoreConnections_();
  },
  update_: function(update) {
    return Blockly.Constants.Utils.UPDATE_BLOCK_MUTATOR_MIXIN(this, update);
  },
  /**
   * Modify this block to have the correct number of inputs.
   * @private
   * @this {Blockly.Block}
   */
  updateShape_: function() {
    var that = this;
    var remove = function() {
      that.removeItem_();
    };
    var add = function() {
      that.addItem_();
    };
    // Remove all inputs
    if (this.getInput('TOP')) this.removeInput('TOP');
    var i = 0;
    while (this.getInput('ADD_KEY' + i)) {
      this.removeInput('TITLE_KEY' + i);
      this.removeInput('ADD_KEY' + i);
      i++;
    }
    i = 0;
    while (this.getInput('ADD_VALUE' + i)) {
      this.removeInput('TITLE_VALUE' + i);
      this.removeInput('ADD_VALUE' + i);
      i++;
    }
    // Update inputs
    var top = this.appendDummyInput('TOP');
    if (this.itemCount_ > 0) {
      top.appendField(Blockly.Msg['DATA_GALAXIA_ADD']);
      top.appendField(new Blockly.FieldImage(this.ADD_IMAGE_DATAURI, this.buttonSize, this.buttonSize, "*", add, false));
      if(this.itemCount_ > 1)
        top.appendField(new Blockly.FieldImage(this.REMOVE_IMAGE_DATAURI, this.buttonSize, this.buttonSize, "*", remove, false));
      for (var i = 0; i < this.itemCount_; i++) {
        let title = this.appendDummyInput("TITLE_KEY"+i)
        title.appendField(Blockly.Msg['DATA_GALAXIA_COLUMN']+" "+i)
        let input = this.appendValueInput('ADD_KEY' + i);
        // if(!this.valueConnections_ || !this.valueConnections_[i*2] || (this.valueConnections_[i*2] && (!this.valueConnections_[i*2].sourceBlock_.isShadow))){
          let xml = `<shadow type="text"></shadow>`;
          let shadowDom = Blockly.Xml.textToDom(xml)
          shadowDom.setAttribute('id', Blockly.utils.genUid());
          input.connection.setShadowDom(shadowDom);
          input.connection.respawnShadow_();
        // }
        
        title = this.appendDummyInput("TITLE_VALUE"+i)
        title.appendField(Blockly.Msg['DATA_GALAXIA_VALUE']+" "+i)
        input = this.appendValueInput('ADD_VALUE' + i);
        // if(!this.valueConnections_ || !this.valueConnections_[i*2+1] || (this.valueConnections_[i*2+1] && (!this.valueConnections_[i*2+1].sourceBlock_.isShadow))){
           xml = `<shadow type="text"></shadow>`;
           shadowDom = Blockly.Xml.textToDom(xml)
          shadowDom.setAttribute('id', Blockly.utils.genUid());
          input.connection.setShadowDom(shadowDom);
          input.connection.respawnShadow_();
        // }

      }
    }
    
    /* Switch to vertical list when the list is too long */
    var showHorizontalList = this.itemCount_ <= 4;
    this.setInputsInline(showHorizontalList);
    this.setOutputShape(showHorizontalList ?
      Blockly.OUTPUT_SHAPE_ROUND : Blockly.OUTPUT_SHAPE_SQUARE);
  }
};

Blockly.Constants.Data.ADD_MUTATOR_MIXIN_V2 = {
  /**
     * Create XML to represent list inputs.
     * @return {!Element} XML storage element.
     * @this {Blockly.Block}
     */
  mutationToDom: function() {
    var container = Blockly.utils.xml.createElement('mutation');
    container.setAttribute('columns', this.itemCount_);
    return container;
  },
  /**
   * Parse XML to restore the list inputs.
   * @param {!Element} xmlElement XML storage element.
   * @this {Blockly.Block}
   */
  domToMutation: function(xmlElement) {
    this.itemCount_ = parseInt(xmlElement.getAttribute('columns'), 10);
    this.updateShape_();
  },
  /**
   * Store pointers to any connected child blocks.
   */
  storeConnections_: function() {
    let connections = []
    // console.log("STORE################")
    for (var i = 0; i < this.itemCount_; i++) {
      // console.log(i, this)
      if(this.getInput('DATA' + i))
        connections.push(this.getInput('DATA' + i).connection.targetConnection);
    }
    if(this.valueConnections_ || connections.length > 0){
      this.valueConnections_ = connections
    }
  },
  restoreConnections_: function() {
    if(!this.valueConnections_)
      return
    for (var i = 0; i < this.itemCount_; i++) {
      let input = this.getInput('DATA' + i)
      if(input && this.valueConnections_[i]){
        input.connection.connect(this.valueConnections_[i])
      }
    }
  },
  addItem_: function() {
    this.storeConnections_();
    var update = function() {
      this.itemCount_++;
    };
    this.update_(update);
    this.restoreConnections_();
  },
  removeItem_: function() {
    this.storeConnections_();
    var update = function() {
      this.itemCount_--;
    };
    this.update_(update);
    this.restoreConnections_();
  },
  update_: function(update) {
    return Blockly.Constants.Utils.UPDATE_BLOCK_MUTATOR_MIXIN(this, update);
  },
  /**
   * Modify this block to have the correct number of inputs.
   * @private
   * @this {Blockly.Block}
   */
  updateShape_: function() {
    if(!this.workspace.isFlyout && this.firstUpdate){
      this.firstUpdate = false
      return
    }
    this.storeConnections_();
    var that = this;
    var remove = function() {
      that.removeItem_();
    };
    var add = function() {
      that.addItem_();
    };

    if (this.getInput('TOP')) this.removeInput('TOP');
    var countItem = 0;
    while (this.getInput('DATA' + countItem)) {
      this.removeInput('DATA' + countItem);
      countItem++;
    }

    let block = null
    // Update inputs
    var top = this.appendDummyInput('TOP');
    if (this.itemCount_ > 0) {
      top.appendField(Blockly.Msg['DATA_GALAXIA_ADD']);
      top.appendField(new Blockly.FieldImage(this.ADD_IMAGE_DATAURI, this.buttonSize, this.buttonSize, "*", add, false));
      if(this.itemCount_ > 1)
        top.appendField(new Blockly.FieldImage(this.REMOVE_IMAGE_DATAURI, this.buttonSize, this.buttonSize, "*", remove, false));
      for (var i = 0; i < this.itemCount_; i++) {
        let input = this.appendValueInput('DATA' + i);
        input.setCheck("Tuple")
      }

      //only create block when itemCount increases
      if(this.firstUpdate || (this.valueConnections_ && this.valueConnections_.length != 0 && this.valueConnections_.length < this.itemCount_)){  
        let block = null
        
        let tops = this.workspace.getTopBlocks()
        if(this.itemCount_ == 1){
          for(let b of tops){
            //When the block is created from the flyout one child is also added to the workspace, search for it
            if(b.type == "data_galaxia_column_value"){
              block = b              
            }
          }
        }
        let index = this.valueConnections_ ? this.valueConnections_.length : 0;
        if(!block){
          block = new Blockly.BlockSvg(this.workspace, "data_galaxia_column_value")
          const xml = `<shadow type="text"></shadow>`;
          let shadowDom = Blockly.Xml.textToDom(xml)
          shadowDom.setAttribute('id', Blockly.utils.genUid());
          let input = block.getInput("COLUMN")
          console.log(input)
          input.connection.setShadowDom(shadowDom);
          input.connection.respawnShadow_();
          input = block.getInput("VALUE")
          input.connection.setShadowDom(shadowDom);
          input.connection.respawnShadow_();
          block.initSvg()
          block.render() 
        }
        this.getInput('DATA' + index).connection.connect(block.outputConnection)
      }
    }
    this.restoreConnections_();
    /* Switch to vertical list when the list is too long */
    var showHorizontalList = this.itemCount_ <= 4;
    this.setInputsInline(showHorizontalList);
    this.setOutputShape(showHorizontalList ?
      Blockly.OUTPUT_SHAPE_ROUND : Blockly.OUTPUT_SHAPE_SQUARE);
  }
};

Blockly.Extensions.register("data_set_columns",
  Blockly.Constants.Data.SET_COLUMN_EXTENSION);

Blockly.Extensions.registerMutator('data_set_columns_mutator',
  Blockly.Constants.Data.SET_COLUMN_MUTATOR_MIXIN);

Blockly.Extensions.register("data_add",
  Blockly.Constants.Data.ADD_EXTENSION);

Blockly.Extensions.registerMutator('data_add_mutator',
  Blockly.Constants.Data.ADD_MUTATOR_MIXIN);

  Blockly.Extensions.register("data_add_v2",
    Blockly.Constants.Data.ADD_EXTENSION_V2);
  
  Blockly.Extensions.registerMutator('data_add_mutator_v2',
    Blockly.Constants.Data.ADD_MUTATOR_MIXIN_V2);