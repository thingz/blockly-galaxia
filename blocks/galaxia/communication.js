//// FUNCTIONS / PROCEDURES METHODS DEFINITIONS
// /**
//  * Function argument reporters cannot exist outside functions that define them
//  * as arguments. Enforce this whenever an event is fired.
//  * @param {!Blockly.Events.Abstract} event Change event.
//  * @this Blockly.Block
//  */
// Blockly.Procedures.onReporterChange = function (event) {
//   if (!this.workspace || this.workspace.isFlyout) {
//     // Block is deleted or is in a flyout.
//     return;
//   }

//   var thisWasCreated = event.type === Blockly.Events.BLOCK_CREATE && event.ids.indexOf(this.id) != -1;
//   var thisWasDragged = event.type === Blockly.Events.END_DRAG && event.allNestedIds.indexOf(this.id) != -1;

//   if (thisWasCreated || thisWasDragged) {
//     var rootBlock = this.getRootBlock();
//     var isTopBlock = Blockly.Procedures.isFunctionArgumentReporter(rootBlock);

//     if (isTopBlock || rootBlock.previousConnection != null) {
//       // Reporter is by itself on the workspace, or it is slotted into a
//       // stack of statements that is not attached to a function or event. Let
//       // it exist until it is connected to a function or event handler.
//       return;
//     }

//     // Ensure an argument with this name and type is defined on the root block.
//     if (!Blockly.Procedures.hasMatchingArgumentReporter(rootBlock, this)) {
//       // No argument with this name is defined on the root block; delete this
//       // reporter.
//       Blockly.Events.setGroup(event.group);
//       this.dispose();
//       Blockly.Events.setGroup(false);
//     }
//   }
// };

// // Argument editor and reporter helpers

// /**
//  * Returns the type associated with this reporter or editor.
//  * @return {string} This argument's type.
//  * @this Blockly.Block
//  */
// Blockly.Procedures.getTypeName = function () {
//   return this.typeName_;
// };

// /**
//  * Create XML to represent the type name of an argument editor or reporter.
//  * @return {!Element} XML storage element.
//  * @this Blockly.Block
//  */
// Blockly.Procedures.argumentMutationToDom = function () {
//   var container = document.createElement("mutation");
//   container.setAttribute("typename", this.typeName_);
//   return container;
// };

// /**
//  * Parse XML to restore the type name of an argument editor or reporter.
//  * @param {!Element} xmlElement XML storage element.
//  * @this Blockly.Block
//  */
// Blockly.Procedures.argumentDomToMutation = function (xmlElement) {
//   this.typeName_ = xmlElement.getAttribute("typename");
//   this.setOutput(true, this.typeName_);
// };

// /**
//  * Creates a custom argument reporter or editor with the correct mutation for
//  * the specified type name.
//  * @param {string} blockType The block type, argument_editor_custom or
//  *  argument_reporter_custom.
//  * @param {string} typeName The type of the argument.
//  * @param {!Blockly.Workspace} ws The workspace to create the block in.
//  * @return {!Blockly.block} The created block.
//  */
// Blockly.Procedures.createCustomArgumentBlock = function (blockType, typeName, ws) {
//   var blockText = "<xml>" + '<block type="' + blockType + '">' + '<mutation typename="' + typeName + '"></mutation>' + "</block>" + "</xml>";
//   var blockDom = Blockly.Xml.textToDom(blockText);
//   return Blockly.Xml.domToBlock(blockDom.firstChild, ws);
// };

// /**
//  * Creates an argument_reporter_custom block with the correct mutation for the
//  * specified type name.
//  * @param {string} typeName The type of the argument.
//  * @param {!Blockly.Workspace} ws The workspace to create the block in.
//  * @return {!Blockly.block} The created block.
//  */
// Blockly.Procedures.createCustomArgumentReporter = function (typeName, ws) {
//   return Blockly.Procedures.createCustomArgumentBlock("argument_reporter_custom", typeName, ws);
// };

// Blockly.Procedures.hasMatchingArgumentReporter = function (t, e) {
//   for (var o = e.getFieldValue("VALUE"), i = e.getTypeName(), n = 0; n < t.inputList.length; ++n) {
//     var l = t.inputList[n];
//     if (l.type == Blockly.INPUT_VALUE) {
//       var r = l.connection.targetBlock();
//       if (r && r.type == e.type && ((l = r.getFieldValue("VALUE")), (r = r.getTypeName()), l == o && i == r)) return !0;
//     }
//   }
//   return !1;
// };

// Blockly.Procedures.isFunctionArgumentReporter = function (t) {
//   return "argument_reporter_boolean" == t.type || "argument_reporter_number" == t.type || "argument_reporter_string" == t.type || "argument_reporter_custom" == t.type;
// };
//// END FUNCTIONS / PROCEDURES METHODS DEFINITIONS

//// LABEL DEFINITION
// //Blockly.fieldRegistry.register("field_label", Blockly.FieldLabel),

// (Blockly.FieldLabelHover = function (t, e) {
//   Blockly.FieldLabelHover.superClass_.constructor.call(this, t, e), (this.arrowWidth_ = 0);
// }),
//   Blockly.utils.object.inherits(Blockly.FieldLabelHover, Blockly.FieldLabel),
//   (Blockly.FieldLabelHover.prototype.initView = function () {
//     Blockly.FieldLabelHover.superClass_.initView.call(this),
//       this.sourceBlock_.isEditable() &&
//         ((this.mouseOverWrapper_ = Blockly.bindEvent_(this.getClickTarget_(), "mouseover", this, this.onMouseOver_)),
//         (this.mouseOutWrapper_ = Blockly.bindEvent_(this.getClickTarget_(), "mouseout", this, this.onMouseOut_)));
//   }),
//   (Blockly.FieldLabelHover.fromJson = function (t) {
//     var e = Blockly.utils.replaceMessageReferences(t.text);
//     return new Blockly.FieldLabelHover(e, t.class);
//   }),
//   (Blockly.FieldLabelHover.prototype.EDITABLE = !1),
//   (Blockly.FieldLabelHover.prototype.SERIALIZABLE = !0),
//   (Blockly.FieldLabelHover.prototype.updateWidth = function () {
//     this.size_.width = Blockly.utils.dom.getFastTextWidth(this.textElement_, this.constants_.FIELD_TEXT_FONTSIZE, this.constants_.FIELD_TEXT_FONTWEIGHT, this.constants_.FIELD_TEXT_FONTFAMILY);
//   }),
//   (Blockly.FieldLabelHover.prototype.onMouseOver_ = function (t) {
//     !this.sourceBlock_.isInFlyout &&
//       this.sourceBlock_.isShadow() &&
//       (((t = this.sourceBlock_.workspace.getGesture(t)) && t.isDragging()) ||
//         !this.sourceBlock_.pathObject.svgPath ||
//         (Blockly.utils.dom.addClass(this.sourceBlock_.pathObject.svgPath, "blocklyFieldHover"), (this.sourceBlock_.pathObject.svgPath.style.strokeDasharray = "2")));
//   }),
//   (Blockly.FieldLabelHover.prototype.clearHover = function () {
//     this.sourceBlock_.pathObject.svgPath &&
//       (Blockly.utils.dom.removeClass(this.sourceBlock_.pathObject.svgPath, "blocklyFieldHover"), (this.sourceBlock_.pathObject.svgPath.style.strokeDasharray = ""));
//   }),
//   (Blockly.FieldLabelHover.prototype.onMouseOut_ = function (t) {
//     !this.sourceBlock_.isInFlyout && this.sourceBlock_.isShadow() && (((t = this.sourceBlock_.workspace.getGesture(t)) && t.isDragging()) || this.clearHover());
//   }),
//   (Blockly.FieldLabelHover.dispose = function () {
//     this.mouseOverWrapper_ && (Blockly.unbindEvent_(this.mouseOverWrapper_), (this.mouseOverWrapper_ = null)),
//       this.mouseOutWrapper_ && (Blockly.unbindEvent_(this.mouseOutWrapper_), (this.mouseOutWrapper_ = null)),
//       Blockly.FieldLabelHover.superClass_.dispose.call(this),
//       (this.variableMap_ = this.workspace_ = null);
//   }),
//   Blockly.fieldRegistry.register("field_label_hover", Blockly.FieldLabelHover),
//   (Blockly.FieldVariableGetter = function (t, e, o, i, n) {
//     (this.defaultVariableName = t || ""), (this.size_ = new Blockly.utils.Size(0, 0)), n && this.configure_(n), e && this.setValidator(e), n || this.setTypes_(o, i);
//   });

//// END LABEL DEFINITION

//// START UNCOMPILED LABEL HOBER DEFINITON
//// goog.require("Blockly.FieldLabel");
// goog.provide("Blockly.FieldLabelHover");

// /**
//  * Class for a variable getter field.
//  * @param {string} text The initial content of the field.
//  * @param {string} opt_class Optional CSS class for the field's text.
//  * @extends {Blockly.FieldLabel}
//  * @constructor
//  *
//  */
// Blockly.FieldLabelHover = function (text, opt_class) {
//   Blockly.FieldLabelHover.superClass_.constructor.call(this, text, opt_class);
//   // Used in base field rendering, but we don't need it.
//   this.arrowWidth_ = 0;
// };

// console.log("UTILS", Blockly.utils);
// Blockly.utils.object.inherits(Blockly.FieldLabelHover, Blockly.FieldLabel);

// /**
//  * Install this field on a block.
//  */
// Blockly.FieldLabelHover.prototype.init = function () {
//   if (this.fieldGroup_) {
//     // Field has already been initialized once.
//     return;
//   }
//   Blockly.FieldLabelHover.superClass_.init.call(this);

//   if (this.sourceBlock_.isEditable()) {
//     this.mouseOverWrapper_ = Blockly.bindEvent_(this.getClickTarget_(), "mouseover", this, this.onMouseOver_);
//     this.mouseOutWrapper_ = Blockly.bindEvent_(this.getClickTarget_(), "mouseout", this, this.onMouseOut_);
//   }
// };

// /**
//  * Construct a FieldLabelHover from a JSON arg object,
//  * dereferencing any string table references.
//  * @param {!Object} options A JSON object with options (text, and class).
//  * @returns {!Blockly.FieldLabelHover} The new field instance.
//  * @package
//  * @nocollapse
//  */
// Blockly.FieldLabelHover.fromJson = function (options) {
//   var text = Blockly.utils.replaceMessageReferences(options["text"]);
//   return new Blockly.FieldLabelHover(text, options["class"]);
// };

// /**
//  * Editable fields usually show some sort of UI for the user to change them.
//  * This field should be serialized, but only edited programmatically.
//  * @type {boolean}
//  * @public
//  */
// Blockly.FieldLabelHover.prototype.EDITABLE = false;

// /**
//  * Serializable fields are saved by the XML renderer, non-serializable fields
//  * are not.  This field should be serialized, but only edited programmatically.
//  * @type {boolean}
//  * @public
//  */
// Blockly.FieldLabelHover.prototype.SERIALIZABLE = true;

// /**
//  * Updates the width of the field. This calls getCachedWidth which won't cache
//  * the approximated width on IE/Edge when `getComputedTextLength` fails. Once
//  * it eventually does succeed, the result will be cached.
//  **/
// Blockly.FieldLabelHover.prototype.updateWidth = function () {
//   // Set width of the field.
//   // Unlike the base Field class, this doesn't add space to editable fields.
//   this.size_.width = Blockly.utils.dom.getFastTextWidth(this.textElement_, this.constants_.FIELD_TEXT_FONTSIZE, this.constants_.FIELD_TEXT_FONTWEIGHT, this.constants_.FIELD_TEXT_FONTFAMILY);
// };

// /**
//  * Handle a mouse over event on a input field.
//  * @param {!Event} e Mouse over event.
//  * @private
//  */
// Blockly.FieldLabelHover.prototype.onMouseOver_ = function (e) {
//   if (this.sourceBlock_.isInFlyout || !this.sourceBlock_.isShadow()) return;
//   var gesture = this.sourceBlock_.workspace.getGesture(e);
//   if (gesture && gesture.isDragging()) return;
//   if (this.sourceBlock_.svgPath_) {
//     Blockly.utils.addClass(this.sourceBlock_.svgPath_, "blocklyFieldHover");
//     this.sourceBlock_.svgPath_.style.strokeDasharray = "2";
//   }
// };

// /**
//  * Clear hover effect on the block
//  * @param {!Event} e Clear hover effect
//  */
// Blockly.FieldLabelHover.prototype.clearHover = function () {
//   if (this.sourceBlock_.svgPath_) {
//     Blockly.utils.removeClass(this.sourceBlock_.svgPath_, "blocklyFieldHover");
//     this.sourceBlock_.svgPath_.style.strokeDasharray = "";
//   }
// };

// /**
//  * Handle a mouse out event on a input field.
//  * @param {!Event} e Mouse out event.
//  * @private
//  */
// Blockly.FieldLabelHover.prototype.onMouseOut_ = function (e) {
//   if (this.sourceBlock_.isInFlyout || !this.sourceBlock_.isShadow()) return;
//   var gesture = this.sourceBlock_.workspace.getGesture(e);
//   if (gesture && gesture.isDragging()) return;
//   this.clearHover();
// };

// /**
//  * Dispose of this field.
//  * @public
//  */
// Blockly.FieldLabelHover.dispose = function () {
//   if (this.mouseOverWrapper_) {
//     Blockly.unbindEvent_(this.mouseOverWrapper_);
//     this.mouseOverWrapper_ = null;
//   }
//   if (this.mouseOutWrapper_) {
//     Blockly.unbindEvent_(this.mouseOutWrapper_);
//     this.mouseOutWrapper_ = null;
//   }
//   Blockly.FieldLabelHover.superClass_.dispose.call(this);
//   this.workspace_ = null;
//   this.variableMap_ = null;
// };

// Blockly.fieldRegistry.register("field_label_hover", Blockly.FieldLabelHover);

//// END UNCOMPILED LABEL HOBER DEFINITON

/**
 * @format
 * @fileoverview Communication blocks for Galaxia.
 */

Blockly.Constants.communication = Object.create(null);

Blockly.defineBlocksWithJsonArray([
  // BEGIN JSON EXTRACT

  /*Begin Galaxia radio blocks*/

  // BLOCK GALAXIA RADIO SEND VALUE
  {
    type: "communication_galaxia_radioSendValue",
    message0: "%{BKY_COMMUNICATION_GALAXIA_RADIO_SENDVALUE_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "VALUE",
        check: ["Boolean", "Number", "String"],
      },
    ],
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_RADIO_SENDVALUE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA RADIO ON DATA RECEIVED
  {
    type: "communication_galaxia_onRadioReceived",
    message0: "%{BKY_COMMUNICATION_GALAXIA_RADIO_ONRECEIVED_TITLE}",
    args0: [
      {
        type: "field_variable",
        name: "VALUE",
        variable: "%{BKY_COMMUNICATION_GALAXIA_RADIO_VALUE}",
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_RADIO_ONRECEIVED_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA RADIO DATA RECEIVED
  {
    type: "communication_galaxia_RadioDataReceived",
    message0: "%{BKY_COMMUNICATION_GALAXIA_RADIO_DATA_RECEIVED_TITLE}",

    output: ["String", "Number"],
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_RADIO_DATA_RECEIVED_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK GALAXIA RADIO SET CHANNEL
  {
    type: "communication_galaxia_radioSetChannel",
    message0: "%{BKY_COMMUNICATION_GALAXIA_RADIO_SET_CHANNEL_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "CHANNEL",
        check: ["Number"],
      },
    ],
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_RADIO_SET_CHANNEL_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK GALAXIA RADIO SET CHANNEL
  {
    type: "communication_galaxia_radioSetChannelDropdown",
    message0: "%{BKY_COMMUNICATION_GALAXIA_RADIO_SET_CHANNEL_TITLE}",
    args0: [
      {
        type: "field_dropdown",
        name: "CHANNEL",
        options: [
          ["1", "1"],
          ["6", "6"],
          // ["11", "11"],
        ],
      },
    ],
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_RADIO_SET_CHANNEL_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA RADIO GET CHANNEL
  {
    type: "communication_galaxia_radioGetChannel",
    message0: "%{BKY_COMMUNICATION_GALAXIA_RADIO_GET_CHANNEL_TITLE}",

    output: ["Number"],
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_RADIO_GET_CHANNEL_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE WIFI CONNECT BASIC
  {
    type: "communication_galaxia_simpleWifi_connect_basic",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_BASIC_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "SSID",
        check: "String",
      },
      {
        type: "input_value",
        name: "PWD",
        check: "String",
      },
    ],
    // inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_BASIC_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK GALAXIA SIMPLE WIFI CONNECT (3 params)
  {
    type: "communication_galaxia_simpleWifi_connect",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "SSID",
        check: "String",
      },
      {
        type: "input_value",
        name: "PWD",
        check: "String",
      },
      {
        type: "input_value",
        name: "IP",
        check: "String",
      },
    ],
    // inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE WIFI CONNECT COMPLETE
  {
    type: "communication_galaxia_simpleWifi_connect_complete",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_COMPLETE_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "SSID",
        check: "String",
      },
      {
        type: "input_value",
        name: "PWD",
        check: "String",
      },
      {
        type: "input_value",
        name: "IP", // Static IP
        check: "String",
      },
      {
        type: "input_value",
        name: "PORT", // Server port
        check: "Number",
      },
    ],
    // inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_COMPLETE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  {
    type: "communication_galaxia_simpleWifi_create_ap",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_CREATE_AP_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "SSID",
        check: "String",
      },
      {
        type: "input_value",
        name: "PWD",
        check: "String",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_CREATE_AP_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK GALAXIA SIMPLE WIFI CONNECT ETH
  {
    type: "communication_galaxia_simpleWifi_connect_eth_with_ip",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_ETH_WITH_IP_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "IP",
        check: "String",
      },
    ],
    // inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_ETH_WITH_IP_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK GALAXIA SIMPLE WIFI CONNECT ETH
  {
    type: "communication_galaxia_simpleWifi_connect_eth",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_ETH_TITLE}",
    args0: [

    ],
    // inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_ETH_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE WIFI GET LAST CONNECTED IP
  {
    type: "communication_galaxia_simpleWifi_getLastConnectedIp",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_GETLASTCONNECTEDIP_TITLE}",
    output: ["String"],
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_GETLASTCONNECTEDIP_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE WIFI GET MY IP
  {
    type: "communication_galaxia_simpleWifi_getMyIp",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_GETMYIP_TITLE}",
    output: ["String"],
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_GETMYIP_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE WIFI RECEIVE BASIC
  {
    type: "communication_galaxia_simpleWifi_receive_basic",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_BASIC_TITLE}",
    args0: [
      {
        type: "field_variable",
        name: "VALUE",
        variable: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_MESSAGE}",
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_BASIC_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE WIFI RECEIVE DELIMITER
  {
    type: "communication_galaxia_simpleWifi_receive_delimiter",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_TITLE}",
    args0: [
      {
        type: "field_variable",
        name: "VALUE",
        variable: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_MESSAGE}",
      },
      {
        type: "field_dropdown",
        name: "DELIMITER",
        options: [
          ["%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_N}", "\\n"],
          ["%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_R}", "\\r"],
          ["%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_RN}", "\\r\\n"],
        ],
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE WIFI RECEIVE COMPLETE
  {
    type: "communication_galaxia_simpleWifi_receive",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "IP",
      },
      {
        type: "field_variable",
        name: "VALUE",
        variable: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_MESSAGE}",
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE WIFI RECEIVE COMPLETE
  {
    type: "communication_galaxia_simpleWifi_receive_complete",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_COMPLETE_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "IP",
      },
      {
        type: "input_value",
        name: "TIMEOUT",
      },
      {
        type: "field_variable",
        name: "VALUE",
        variable: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_MESSAGE}",
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_COMPLETE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE WIFI SERVER RECEIVE ASYNC

  {
    type: "communication_galaxia_simpleWifi_server_receive_async",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RECEIVE_ASYNC_TITLE}",
    output: "String",
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RECEIVE_ASYNC_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  {
    type: "communication_galaxia_simpleWifi_server_receive_event",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RECEIVE_EVENT_TITLE}",
    args0: [
      {
        type: "field_variable",
        name: "DATA",
        variable: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RECEIVED_DATA}",
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RECEIVE_EVENT_TOOLTIP}",
    helpUrl: THINGZ_SITE,
    extensions: ["disable_lower_blocks"],
  },

  //BLOCK GALAXIA SIMPLE WIFI SERVER IS CLIENT CONNECTED
  {
    type: "communication_galaxia_simpleWifi_server_is_client_connected",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_IS_CLIENT_CONNECTED_TITLE}",
    output: "Boolean",
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_IS_CLIENT_CONNECTED_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE WIFI SERVER RESPOND BASIC
  {
    type: "communication_galaxia_simpleWifi_server_respond",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RESPOND_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "VALUE",
        check: ["String", "Number", "Boolean"],
      }
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RESPOND_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE WIFI SERVER RESPOND WITH DELIMETER
  {
    type: "communication_galaxia_simpleWifi_server_respond_with_delimiter",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RESPOND_WITH_DELIMITER_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "VALUE",
        check: ["String", "Number", "Boolean"],
      },
      {
        type: "field_dropdown",
        name: "DELIMITER",
        options: [
          ["%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_N}", "\\n"],
          ["%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_R}", "\\r"],
          ["%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_RN}", "\\r\\n"],
          ["%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_NONE}", ""],
        ],
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RESPOND_WITH_DELIMITER_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE WIFI SEND BASIC
  {
    type: "communication_galaxia_simpleWifi_send_basic",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_BASIC_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "VALUE",
        check: ["String", "Number", "Boolean"],
      },
      {
        type: "input_value",
        name: "IP",
        check: "String",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_BASIC_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  {
    type: "communication_galaxia_simpleWifi_send_basic_with_delimiter",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_BASIC_WITH_DELIMITER_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "VALUE",
        check: ["String", "Number", "Boolean"],
      },
      {
        type: "input_value",
        name: "IP",
        check: "String",
      },
      {
        type: "field_dropdown",
        name: "DELIMITER",
        options: [
          ["%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_N}", "\\n"],
          ["%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_R}", "\\r"],
          ["%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_RN}", "\\r\\n"],
          ["%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_NONE}", ""],
        ],
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_BASIC_WITH_DELIMITER_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE WIFI SEND AND WAIT RESPOND
  {
    type: "communication_galaxia_simpleWifi_send_wait_respond",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_WAIT_RESPOND_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "VALUE",
        check: ["String", "Number", "Boolean"],
      },
      {
        type: "input_value",
        name: "IP",
        check: "String",
      },
      {
        type: "field_variable",
        name: "RESPONSE_VALUE",
        variable: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RESPONSE}",
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_WAIT_RESPOND_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE WIFI SEND AND WAIT RESPOND WITH DELIMITER
  {
    type: "communication_galaxia_simpleWifi_send_wait_respond_with_delimiter",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_WAIT_RESPOND_WITH_DELIMITER_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "VALUE",
        check: ["String", "Number", "Boolean"],
      },
      {
        type: "input_value",
        name: "IP",
        check: "String",
      },
      {
        type: "field_dropdown",
        name: "DELIMITER",
        options: [
          ["%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_N}", "\\n"],
          ["%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_R}", "\\r"],
          ["%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_RN}", "\\r\\n"],
        ],
      },
      {
        type: "field_variable",
        name: "RESPONSE_VALUE",
        variable: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_RESPONSE}",
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_WAIT_RESPOND_WITH_DELIMITER_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE WIFI SEND COMPLETE
  {
    type: "communication_galaxia_simpleWifi_send_complete",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_COMPLETE_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "VALUE",
        check: ["String", "Number", "Boolean"],
      },
      {
        type: "input_value",
        name: "IP",
        check: "String",
      },
      {
        type: "input_value",
        name: "PORT",
        check: "Number",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_COMPLETE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE HTTP RESPOND BASIC
  {
    type: "communication_galaxia_simpleHttp_respond_basic",
    message0: "%{BKY_COMMUNICATION_GALAXIA_HTTP_RESPOND_BASIC_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "VALUE",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_HTTP_RESPOND_BASIC_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE HTTP RESPOND COMPLETE
  {
    type: "communication_galaxia_simpleHttp_respond_complete",
    message0: "%{BKY_COMMUNICATION_GALAXIA_HTTP_RESPOND_COMPLETE_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "VALUE",
      },
      {
        type: "input_value",
        name: "CODE",
        check: "Number",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_HTTP_RESPOND_COMPLETE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE HTTP WAIT REQUEST
  {
    type: "communication_galaxia_simpleHttp_wait_request",
    message0: "%{BKY_COMMUNICATION_GALAXIA_HTTP_WAIT_REQUEST_TITLE}",
    args0: [
      {
        type: "field_variable",
        name: "VALUE",
        variable: "%{BKY_COMMUNICATION_GALAXIA_HTTP_RESPONSE}",
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_HTTP_WAIT_REQUEST_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK GALAXIA SIMPLE HTTP GENERATE PAGE
  {
    type: "communication_galaxia_simpleHttp_generate_page",
    message0: "%{BKY_COMMUNICATION_GALAXIA_HTTP_GENERATE_PAGE}",
    args0: [
      {
        type: "input_value",
        name: "RELOAD",
        check: "Number",
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_HTTP_GENERATE_PAGE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
    extensions: ["disable_lower_blocks"],
  },

  {
    type: "communication_galaxia_simpleHttp_generate_page_no_refresh",
    message0: "%{BKY_COMMUNICATION_GALAXIA_HTTP_GENERATE_PAGE_NO_REFRESH}",
    args0: [
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_HTTP_GENERATE_PAGE_NO_REFRESH_TOOLTIP}",
    helpUrl: THINGZ_SITE,
    extensions: ["disable_lower_blocks"],
  },

  //BLOCK GALAXIA SIMPLE HTTP ADD TO PAGE
  {
    type: "communication_galaxia_simpleHttp_add_to_page",
    message0: "%{BKY_COMMUNICATION_GALAXIA_HTTP_ADD_TO_PAGE}",
    args0: [
      {
        type: "input_value",
        name: "DATA",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_HTTP_ADD_TO_PAGE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE HTTP BUTTON ADD TO PAGE
  {
    type: "communication_galaxia_simpleHttp_add_button_to_page",
    message0: "%{BKY_COMMUNICATION_GALAXIA_HTTP_ADD_BUTTON_TO_PAGE}",
    args0: [
      {
        type: "input_value",
        name: "TITLE",
      },
      {
        type: "input_value",
        name: "CMD",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_HTTP_ADD_BUTTON_TO_PAGE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE HTTP BUTTON EVENT
  {
    type: "communication_galaxia_simpleHttp_button_event",
    message0: "%{BKY_COMMUNICATION_GALAXIA_HTTP_BUTTON_EVENT_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "CMD",
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_HTTP_BUTTON_EVENT_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE HTTP GAUGE ADD TO PAGE
  {
    type: "communication_galaxia_simpleHttp_add_gauge_to_page",
    message0: "%{BKY_COMMUNICATION_GALAXIA_HTTP_ADD_GAUGE_TO_PAGE}",
    args0: [
      {
        type: "input_value",
        name: "TITLE",
      },
      {
        type: "input_value",
        name: "VALUE",
        check: "Number",
      },
      {
        type: "input_value",
        name: "MIN",
        check: "Number",
      },
      {
        type: "input_value",
        name: "MAX",
        check: "Number",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_HTTP_ADD_GAUGE_TO_PAGE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE HTTP GAUGE ADD TO PAGE
  {
    type: "communication_galaxia_simpleHttp_add_gauge_to_page_without_text",
    message0: "%{BKY_COMMUNICATION_GALAXIA_HTTP_ADD_GAUGE_TO_PAGE_WITHOUT_TEXT}",
    args0: [
      {
        type: "input_value",
        name: "TITLE",
      },
      {
        type: "input_value",
        name: "VALUE",
        check: "Number",
      },
      {
        type: "input_value",
        name: "MIN",
        check: "Number",
      },
      {
        type: "input_value",
        name: "MAX",
        check: "Number",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_HTTP_ADD_GAUGE_TO_PAGE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  // BLOCK GALAXIA SIMPLE_HTTP_REQUEST GET URL
  // {
  //   type: "communication_galaxia_SimpleHttpRequest_getURL",
  //   message0: "%{BKY_COMMUNICATION_GALAXIA_SIMLPLE_HTPP_REQUEST_TITLE}",
  //   args0: [
  //     {
  //       type: "input_value",
  //       name: "VALUE",
  //     },
  //   ],
  //   previousStatement: null,
  //   nextStatement: null,
  //   style: "communication_blocks",
  //   tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMLPLE_HTPP_REQUEST_TOOLTIP}",
  //   helpUrl: THINGZ_SITE,
  // },

  // BLOCK RADIO DRAG (TEST)
  {
    type: "radio_on_string_drag",
    message0: "on radio received %1",
    args0: [
      {
        type: "input_value",
        name: "HANDLER_DRAG_PARAM_receivedString",
      },
    ],

    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "HANDLER",
      },
    ],
    style: "communication_blocks",
  },

  //BLOCLY SIMPLE MQTT RECEIVE
  {
    type: "communication_galaxia_simple_mqtt_receive",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_RECEIVE}",
    args0: [
      {
        type: "field_variable",
        name: "TOPIC",
        variable: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_TOPIC}",
      },
      {
        type: "field_variable",
        name: "VALUE",
        variable: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_MESSAGE}",
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_RECEIVE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
    extensions: ["disable_lower_blocks"],
  },

  {
    type: "communication_galaxia_simple_mqtt_receive_v2",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_RECEIVE}",
    args0: [
      {
        type: "input_value",
        name: "TOPIC",
        check: "LocalCbVar"
      },
      {
        type: "input_value",
        name: "VALUE",
        check: "LocalCbVar"
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_RECEIVE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
    extensions: ["disable_lower_blocks", "local_cb_variable"],
  },

  {
    type: "communication_galaxia_simple_mqtt_receive_in_topic",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_RECEIVE}",
    args0: [
      {
        type: "input_value",
        name: "TOPIC",
        check: "String"
      },
      {
        type: "input_value",
        name: "VALUE",
        check: "LocalCbVar"
      },
    ],
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_RECEIVE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
    extensions: ["disable_lower_blocks", "local_cb_variable"],
  },

  //BLOCLY SIMPLE MQTT ON CONNECT
  {
    type: "communication_galaxia_simple_mqtt_on_connect",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_ON_CONNECT}",
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_ON_CONNECT_TOOLTIP}",
    helpUrl: THINGZ_SITE,
    extensions: ["disable_lower_blocks"],
  },

  //BLOCLY SIMPLE MQTT ON DISCONNECT
  {
    type: "communication_galaxia_simple_mqtt_on_disconnect",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_ON_DISCONNECT}",
    message1: "%1",
    args1: [
      {
        type: "input_statement",
        name: "DO",
      },
    ],
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_ON_DISCONNECT_TOOLTIP}",
    helpUrl: THINGZ_SITE,
    extensions: ["disable_lower_blocks"],
  },
  //BLOCKLY SIMPLE MQTT CONNECT
  {
    type: "communication_galaxia_simple_mqtt_connect_complete",
    message0: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_CONNECT_COMPLETE_TITLE}",
    args0: [
      {
        type: "input_value",
        name: "BROKER",
        check: "String",
      },
      {
        type: "input_value",
        name: "USERNAME",
        check: "String",
      },
      {
        type: "input_value",
        name: "PASSWORD",
        check: "String",
      },
      {
        type: "input_value",
        name: "PORT",
        check: "Number",
      },
    ],
    // inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_CONNECT_COMPLETE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE MQTT SUBSCRIBE
  {
    type: "communication_galaxia_simple_mqtt_subscribe",
    message0: "%{BKY_COMMUNICATION_GALAXIA_MQTT_SUBSCRIBE}",
    args0: [
      {
        type: "input_value",
        name: "TOPIC",
        check: "String",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_SUBSCRIBE_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE MQTT PUBLISH
  {
    type: "communication_galaxia_simple_mqtt_publish",
    message0: "%{BKY_COMMUNICATION_GALAXIA_MQTT_PUBLISH}",
    args0: [
      {
        type: "input_value",
        name: "TOPIC",
        check: "String",
      },
      {
        type: "input_value",
        name: "VALUE",
      },
    ],
    inputsInline: true,
    previousStatement: null,
    nextStatement: null,
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_PUBLISH_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },

  //BLOCK GALAXIA SIMPLE MQTT IS CONNECTED
  {
    type: "communication_galaxia_simple_mqtt_is_connected",
    message0: "%{BKY_COMMUNICATION_GALAXIA_MQTT_IS_CONNECTED}",
    args0: [
    ],
    output: "Boolean",
    style: "communication_blocks",
    tooltip: "%{BKY_COMMUNICATION_GALAXIA_SIMPLE_MQTT_PUBLISH_TOOLTIP}",
    helpUrl: THINGZ_SITE,
  },
]); // END JSON EXTRACT (Do not delete this comment.)

// Blockly.Constants.Communication = Object.create(null);

// Argument reporter blocks

Blockly.Blocks["argument_reporter_boolean"] = {
  init: function () {
    this.jsonInit({
      message0: " %1",
      args0: [
        {
          type: "field_label_hover",
          name: "VALUE",
          text: "",
        },
      ],
      colour: Blockly.Msg.PROCEDURES_HUE,
      extensions: ["output_boolean"],
    });
    this.typeName_ = "boolean";
  },
  onchange: Blockly.Procedures.onReporterChange,
  getTypeName: Blockly.Procedures.getTypeName,
};

Blockly.Blocks["argument_reporter_number"] = {
  init: function () {
    this.jsonInit({
      message0: " %1",
      args0: [
        {
          type: "field_label_hover",
          name: "VALUE",
          text: "",
        },
      ],
      colour: Blockly.Msg.PROCEDURES_HUE,
      extensions: ["output_number"],
    });
    this.typeName_ = "number";
  },
  onchange: Blockly.Procedures.onReporterChange,
  getTypeName: Blockly.Procedures.getTypeName,
};

Blockly.Blocks["argument_reporter_string"] = {
  /**
   * Block for calling a procedure with no return value.
   * @this {Blockly.Block}
   */
  init: function () {
    this.jsonInit({
      message0: " %1",
      args0: [
        {
          type: "field_label_hover",
          name: "VALUE",
          text: "",
        },
      ],
      style: "display_blocks",
      inputsInline: true,
      outputShape: Blockly.OUTPUT_SHAPE_ROUND,
      output: "String",
    });
    this.typeName_ = "string";
  },
  onchange: Blockly.Procedures.onReporterChange,
  getTypeName: Blockly.Procedures.getTypeName,
};

Blockly.Blocks["argument_reporter_custom"] = {
  init: function () {
    this.jsonInit({
      message0: " %1",
      args0: [
        {
          type: "field_label_hover",
          name: "VALUE",
          text: "",
        },
      ],
      colour: Blockly.Msg.PROCEDURES_HUE,
      inputsInline: true,
      outputShape: Blockly.OUTPUT_SHAPE_ROUND,
      output: null,
    });
    this.typeName_ = "";
  },
  onchange: Blockly.Procedures.onReporterChange,
  getTypeName: Blockly.Procedures.getTypeName,
  mutationToDom: Blockly.Procedures.argumentMutationToDom,
  domToMutation: Blockly.Procedures.argumentDomToMutation,
};

// Disable all instances of the block present in the workspace, except the highest one
Blockly.Constants.communication.LOCAL_CB_VARIABLE = function () {
  this.setOnChange(onUpdate_);

  function onUpdate_(event) {
    console.log(event, this, this.id)

    //Transform check localCbVar to localCbVarINPUTNAME (keep localCbVar to prevent ui bug)
    if (event.blockId == this.id) {
      for (let i = 0; i < this.inputList.length; i++) {
        let check = this.inputList[i].connection ? this.inputList[i].connection.getCheck() : null

        if (check && check.includes("LocalCbVar")) {
          if (this.inputList[i].connection.targetConnection) {
            let id = this.inputList[i].connection.targetConnection.getSourceBlock().id
            let name = this.inputList[i].connection.targetConnection.getSourceBlock().getFieldValue("VAR")

            this.inputList[i].connection.targetConnection.getSourceBlock().unplug()
            let b = this.workspace.getBlockById(id)
            b.setOutput(true, "LocalCbVar" + name)
            console.log("BLOCK", b)
            this.inputList[i].connection.setCheck(["LocalCbVar","LocalCbVar" + name])
            // let check = this.inputList[i].connection.checkType 
            // this.inputList[i].connection.checkType = function(otherConnection){
            //   console.log("OTHER", otherConnection.check_)
            //   if(!otherConnection.check_){
            //     return false;// do not accept connection with non-typed block
            //   }
            //   return check(otherConnection)
            // }
            this.inputList[i].connection.connect(b.outputConnection)
            // this.render()

          }
          // console.log(this.inputList[i].connection.targetConnection)
          // check[check.indexOf("LocalCbVar")] = "LocalCbVar"+this.inputList[i].name
          // this.inputList[i].connection.setCheck(check)
        }
      }
    }
    //Search for drag stop, check if this block is waiting to duplicate and replace the local var
    if (event.type == Blockly.Events.UI
      && event.element == "dragStop"
      && this.duplicateOnDragStop
      && this.duplicateOnDragStop.id == event.blockId
    ) {
      let input = this.getInput(this.duplicateOnDragStop.input)
      if (input && input.connection.targetConnection == null) {
        let origin = this.workspace.getBlockById(this.duplicateOnDragStop.id)
        Blockly.duplicate(origin)
        let top = this.workspace.getTopBlocks()
        for (let i = 0; i < top.length; i++) {
          if (top[i].type == origin.type
            && top[i].getFieldValue("VAR") == origin.getFieldValue("VAR")
            && top[i].id != origin.id) {
            input.connection.connect(top[i].outputConnection)
            break;
          }
        }
      }
      this.duplicateOnDragStop = null
    }
    if (
      (event.oldParentId && event.oldParentId == this.id && this.workspace.getBlockById(event.blockId).type == "variables_local")
    ) {

      for (let i = 0; i < this.inputList.length; i++) {
        if (event.oldInputName) {
          //register a duplicate, wait for drag stop to duplicate
          this.duplicateOnDragStop = { input: event.oldInputName, id: event.blockId }

        }
      }
    }
  }
};

// Initialization extensions
Blockly.Extensions.register("local_cb_variable", Blockly.Constants.communication.LOCAL_CB_VARIABLE);