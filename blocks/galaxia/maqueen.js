/**
 * @fileoverview Robots blocks for Micro:bit.
 */

Blockly.defineBlocksWithJsonArray([ // BEGIN JSON EXTRACT

    /* Begin Maqueen blocks */


    // BLOCK MAQUEEN CONTROL GO
    {
        "type": "robots_galaxia_setMaqueenGo",
        "message0": "%{BKY_ROBOTS_GALAXIA_MAQUEEN_GO_TITLE}",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "DIR",
            "options": [
                ["%{BKY_ROBOTS_MAQUEEN_GO_FORWARD}", "0x0"],
                ["%{BKY_ROBOTS_MAQUEEN_GO_REVERSE}", "0x1"]
            ]
            }, {
                "type": "input_value",
                "name": "SPEED",
                "check": "Number"
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_GALAXIA_MAQUEEN_GO_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK MAQUEEN CONTROL MOTOR
    {
        "type": "robots_galaxia_controlMaqueenMotor",
        "message0": "%{BKY_ROBOTS_GALAXIA_MAQUEEN_CONTROLMOTOR_TITLE}",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "MOTOR",
                "options": [
                    ["%{BKY_ROBOTS_MAQUEEN_RIGHT}", "0x02"],
                    ["%{BKY_ROBOTS_MAQUEEN_LEFT}", "0x00"]
                ]
            }, 
            {
                "type": "field_dropdown",
                "name": "DIR",
                "options": [
                    ["↻", "0x0"],
                    ["↺", "0x1"]
                ]
            }, 
            {
                "type": "input_value",
                "name": "SPEED",
                "check": "Number"
            }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_GALAXIA_MAQUEEN_CONTROLMOTOR_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK MAQUEEN STOP MOTORS
    {
        "type": "robots_galaxia_stopMaqueenMotors",
        "message0": "%{BKY_ROBOTS_GALAXIA_MAQUEEN_STOPMOTORS_TITLE}",
        "args0": [ 
            {
                "type": "field_dropdown",
                "name": "MOTOR",
                "options": [
                    ["%{BKY_ROBOTS_MAQUEEN_RIGHT}", "0x02"],
                    ["%{BKY_ROBOTS_MAQUEEN_LEFT}", "0x00"],
                    ["%{BKY_ROBOTS_MAQUEEN_RIGHTANDLEFT}", "both"]
                ]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_GALAXIA_MAQUEEN_STOPMOTORS_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK MAQUEEN READ LINE FINDER
    {
        "type": "robots_galaxia_readMaqueenPatrol",
        "message0": "%{BKY_ROBOTS_GALAXIA_MAQUEEN_READPATROL_TITLE}",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "PIN",
                "options": [
                    ["%{BKY_ROBOTS_MAQUEEN_RIGHT}", "P14"],
                    ["%{BKY_ROBOTS_MAQUEEN_LEFT}", "P13"]
                ]
            }
        ],
        "output": "Boolean",
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_GALAXIA_MAQUEEN_READPATROL_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    // BLOCK MAQUEEN READ LINE FINDER
    {
        "type": "robots_galaxia_readMaqueenPlusPatrol",
        "message0": "%{BKY_ROBOTS_GALAXIA_MAQUEEN_PLUS_READPATROL_TITLE}",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "PIN",
                "options": [
                    ["%{BKY_ROBOTS_MAQUEEN_LEFT}", "l1"],
                    ["%{BKY_ROBOTS_MAQUEEN_LEFT2}", "l2"],
                    ["%{BKY_ROBOTS_MAQUEEN_RIGHT}", "r1"],
                    ["%{BKY_ROBOTS_MAQUEEN_RIGHT2}", "r2"],
                    ["%{BKY_ROBOTS_MAQUEEN_MIDDLE}", "m"],
                ]
            }
        ],
        "output": "Boolean",
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_GALAXIA_MAQUEEN_PLUS_READPATROL_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    {
        "type": "robots_galaxia_readMaqueenPlusUltrasound",
        "message0": "%{BKY_ROBOTS_GALAXIA_MAQUEEN_PLUS_READ_ULTRASOUND_TITLE}",
        "args0": [
            {
                type: "field_dropdown",
                name: "UNIT",
                options: [
                    ["%{BKY_GROVE_GALAXIA_ULTRASONIC_CM}", "0.017"],
                    ["%{BKY_GROVE_GALAXIA_ULTRASONIC_MM}", "0.17"],
                ],
            },
        ],
        "output": "Number",
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_GALAXIA_MAQUEEN_READ_ULTRASOUND_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

    {
        "type": "robots_galaxia_readMaqueenUltrasound",
        "message0": "%{BKY_ROBOTS_GALAXIA_MAQUEEN_READ_ULTRASOUND_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "UNIT",
                options: [
                    ["%{BKY_GROVE_GALAXIA_ULTRASONIC_CM}", "0.017"],
                    ["%{BKY_GROVE_GALAXIA_ULTRASONIC_MM}", "0.17"],
                ],
            },
        ],
        "output": "Number",
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_GALAXIA_MAQUEEN_READ_ULTRASOUND_TOOLTIP}",
        "helpUrl": THINGZ_SITE
    },

]); // END JSON EXTRACT (Do not delete this comment.)