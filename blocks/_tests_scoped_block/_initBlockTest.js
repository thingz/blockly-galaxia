function initBlock(block, info, fn, comp) {
  const ns = (fn.attributes.blockNamespace || fn.namespace).split(".")[0];
  const instance = fn.kind == pxtc.SymbolKind.Method || fn.kind == pxtc.SymbolKind.Property;
  const nsinfo = info.apis.byQName[ns];
  const color =
    // blockNamespace overrides color on block
    (fn.attributes.blockNamespace && nsinfo && nsinfo.attributes.color) || fn.attributes.color || (nsinfo && nsinfo.attributes.color) || pxt.toolbox.getNamespaceColor(ns) || 255;

  if (fn.attributes.help) {
    const helpUrl = fn.attributes.help.replace(/^\//, "");
    if (/^github:/.test(helpUrl)) {
      block.setHelpUrl(helpUrl);
    } else if (helpUrl !== "none") {
      block.setHelpUrl("/reference/" + helpUrl);
    }
  } else if (fn.pkg && !pxt.appTarget.bundledpkgs[fn.pkg]) {
    // added package
    let anchor = fn.qName.toLowerCase().split(".");
    if (anchor[0] == fn.pkg) anchor.shift();
    block.setHelpUrl(`/pkg/${fn.pkg}#${encodeURIComponent(anchor.join("-"))}`);
  }

  block.setColour(color);
  let blockShape = Blockly.OUTPUT_SHAPE_ROUND;
  if (fn.retType == "boolean") blockShape = Blockly.OUTPUT_SHAPE_HEXAGONAL;

  block.setOutputShape(blockShape);
  if (fn.attributes.undeletable) block.setDeletable(false);

  buildBlockFromDef(fn.attributes._def);
  let hasHandler = false;

  if (fn.attributes.mutate) {
    addMutation(block, fn, fn.attributes.mutate);
  } else if (fn.attributes.defaultInstance) {
    addMutation(block, fn, MutatorTypes.DefaultInstanceMutator);
  } else if (fn.attributes._expandedDef && fn.attributes.expandableArgumentMode !== "disabled") {
    const shouldToggle = fn.attributes.expandableArgumentMode === "toggle";
    initExpandableBlock(info, block, fn.attributes._expandedDef, comp, shouldToggle, () => buildBlockFromDef(fn.attributes._expandedDef, true));
  } else if (comp.handlerArgs.length) {
    /**
     * We support four modes for handler parameters: variable dropdowns,
     * expandable variable dropdowns with +/- buttons (used for chat commands),
     * draggable variable blocks, and draggable reporter blocks.
     */
    hasHandler = true;
    if (fn.attributes.optionalVariableArgs) {
      initVariableArgsBlock(block, comp.handlerArgs);
    } else if (fn.attributes.draggableParameters) {
      comp.handlerArgs
        .filter((a) => !a.inBlockDef)
        .forEach((arg) => {
          const i = block.appendValueInput("HANDLER_DRAG_PARAM_" + arg.name);
          if (fn.attributes.draggableParameters == "reporter") {
            i.setCheck(getBlocklyCheckForType(arg.type, info));
          } else {
            i.setCheck("Variable");
          }
        });
    } else {
      let i = block.appendDummyInput();
      comp.handlerArgs
        .filter((a) => !a.inBlockDef)
        .forEach((arg) => {
          i.appendField(new Blockly.FieldVariable(arg.name), "HANDLER_" + arg.name);
        });
    }
  }
  // Add mutation to save and restore custom field settings
  appendMutation(block, {
    mutationToDom: (el) => {
      block.inputList.forEach((input) => {
        input.fieldRow.forEach((fieldRow) => {
          if (fieldRow.isFieldCustom_ && fieldRow.saveOptions) {
            const getOptions = fieldRow.saveOptions();
            if (getOptions) {
              el.setAttribute(`customfield`, JSON.stringify(getOptions));
            }
          }
        });
      });
      return el;
    },
    domToMutation: (saved) => {
      block.inputList.forEach((input) => {
        input.fieldRow.forEach((fieldRow) => {
          if (fieldRow.isFieldCustom_ && fieldRow.restoreOptions) {
            const options = JSON.parse(saved.getAttribute(`customfield`));
            if (options) {
              fieldRow.restoreOptions(options);
            }
          }
        });
      });
    },
  });
  if (fn.attributes.imageLiteral) {
    const columns = (fn.attributes.imageLiteralColumns || 5) * fn.attributes.imageLiteral;
    const rows = fn.attributes.imageLiteralRows || 5;
    const scale = fn.attributes.imageLiteralScale;
    let ri = block.appendDummyInput();
    ri.appendField(new pxtblockly.FieldMatrix("", { columns, rows, scale }), "LEDS");
  }

  if (fn.attributes.inlineInputMode === "external") {
    block.setInputsInline(false);
  } else if (fn.attributes.inlineInputMode === "inline") {
    block.setInputsInline(true);
  } else {
    block.setInputsInline(!fn.parameters || (fn.parameters.length < 4 && !fn.attributes.imageLiteral));
  }

  const body = fn.parameters ? fn.parameters.filter((pr) => pr.type == "() => void" || pr.type == "Action")[0] : undefined;
  if (body || hasHandler) {
    block.appendStatementInput("HANDLER").setCheck(null);
    block.setInputsInline(true);
  }

  setOutputCheck(block, fn.retType, info);

  // hook up/down if return value is void
  const hasHandlers = hasArrowFunction(fn);
  block.setPreviousStatement(!(hasHandlers && !fn.attributes.handlerStatement) && fn.retType == "void");
  block.setNextStatement(!(hasHandlers && !fn.attributes.handlerStatement) && fn.retType == "void");

  block.setTooltip(/^__/.test(fn.namespace) ? "" : fn.attributes.jsDoc);
  function buildBlockFromDef(def, expanded = false) {
    let anonIndex = 0;
    let firstParam = !expanded && !!comp.thisParameter;

    const inputs = splitInputs(def);
    const imgConv = new ImageConverter();

    if (fn.attributes.shim === "ENUM_GET" || fn.attributes.shim === "KIND_GET") {
      if (comp.parameters.length > 1 || comp.thisParameter) {
        console.warn(`Enum blocks may only have 1 parameter but ${fn.attributes.blockId} has ${comp.parameters.length}`);
        return;
      }
    }

    inputs.forEach((inputParts) => {
      const fields = [];
      let inputName;
      let inputCheck;
      let hasParameter = false;

      inputParts.forEach((part) => {
        if (part.kind !== "param") {
          const f = newLabel(part);
          if (f) {
            fields.push({ field: f });
          }
        } else if (fn.attributes.shim === "ENUM_GET") {
          U.assert(!!fn.attributes.enumName, "Trying to create an ENUM_GET block without a valid enum name");
          fields.push({
            name: "MEMBER",
            field: new pxtblockly.FieldUserEnum(info.enumsByName[fn.attributes.enumName]),
          });
          return;
        } else if (fn.attributes.shim === "KIND_GET") {
          fields.push({
            name: "MEMBER",
            field: new pxtblockly.FieldKind(info.kindsByName[fn.attributes.kindNamespace || fn.attributes.blockNamespace || fn.namespace]),
          });
          return;
        } else {
          // find argument
          let pr = getParameterFromDef(part, comp, firstParam);

          firstParam = false;
          if (!pr) {
            console.error("block " + fn.attributes.blockId + ": unknown parameter " + part.name + (part.ref ? ` (${part.ref})` : ""));
            return;
          }

          if (isHandlerArg(pr)) {
            inputName = "HANDLER_DRAG_PARAM_" + pr.name;
            inputCheck = fn.attributes.draggableParameters === "reporter" ? getBlocklyCheckForType(pr.type, info) : "Variable";
            return;
          }

          let typeInfo = U.lookup(info.apis.byQName, pr.type);

          hasParameter = true;
          const defName = pr.definitionName;
          const actName = pr.actualName;

          let isEnum = typeInfo && typeInfo.kind == pxtc.SymbolKind.Enum;
          let isFixed = typeInfo && !!typeInfo.attributes.fixedInstances && !pr.shadowBlockId;
          let isConstantShim = !!fn.attributes.constantShim;
          let isCombined = pr.type == "@combined@";
          let customField = pr.fieldEditor;
          let fieldLabel = defName.charAt(0).toUpperCase() + defName.slice(1);
          let fieldType = pr.type;

          if (isEnum || isFixed || isConstantShim || isCombined) {
            let syms;

            if (isEnum) {
              syms = getEnumDropdownValues(info.apis, pr.type);
            } else if (isFixed) {
              syms = getFixedInstanceDropdownValues(info.apis, typeInfo.qName);
            } else if (isCombined) {
              syms = fn.combinedProperties.map((p) => U.lookup(info.apis.byQName, p));
            } else {
              syms = getConstantDropdownValues(info.apis, fn.qName);
            }

            if (syms.length == 0) {
              console.error(`no instances of ${typeInfo.qName} found`);
            }
            const dd = syms.map((v) => {
              let k = v.attributes.block || v.attributes.blockId || v.name;
              let comb = v.attributes.blockCombine;
              if (v.attributes.jresURL && !v.attributes.iconURL && U.startsWith(v.attributes.jresURL, "data:image/x-mkcd-f")) {
                v.attributes.iconURL = imgConv.convert(v.attributes.jresURL);
              }
              if (!!comb) k = k.replace(/@set/, "");
              return [
                v.attributes.iconURL || v.attributes.blockImage
                  ? {
                      src: v.attributes.iconURL || Util.pathJoin(pxt.webConfig.commitCdnUrl, `blocks/${v.namespace.toLowerCase()}/${v.name.toLowerCase()}.png`),
                      alt: k,
                      width: 36,
                      height: 36,
                      value: v.name,
                    }
                  : k,
                v.namespace + "." + v.name,
              ];
            });
            // if a value is provided, move it first
            if (pr.defaultValue) {
              let shadowValueIndex = -1;
              dd.some((v, i) => {
                // pr = BlockParameter
                if (v[1] === pr.defaultValue) {
                  shadowValueIndex = i;
                  return true;
                }
                return false;
              });
              if (shadowValueIndex > -1) {
                const shadowValue = dd.splice(shadowValueIndex, 1)[0];
                dd.unshift(shadowValue);
              }
            }

            if (customField) {
              let defl = fn.attributes.paramDefl[actName] || "";
              // Blockly.FieldCustomDropdownOptions:
              const options = {
                data: dd,
                colour: color,
                label: fieldLabel,
                type: fieldType,
                blocksInfo: info,
              };
              Util.jsonMergeFrom(options, (fn.attributes.paramFieldEditorOptions && fn.attributes.paramFieldEditorOptions[actName]) || {});
              fields.push(namedField(createFieldEditor(customField, defl, options), defName));
            } else fields.push(namedField(new Blockly.FieldDropdown(dd), defName));
          } else if (customField) {
            const defl = fn.attributes.paramDefl[pr.actualName] || "";
            const options = {
              colour: color,
              label: fieldLabel,
              type: fieldType,
              blocksInfo: info,
            };
            Util.jsonMergeFrom(options, (fn.attributes.paramFieldEditorOptions && fn.attributes.paramFieldEditorOptions[pr.actualName]) || {});
            fields.push(namedField(createFieldEditor(customField, defl, options), pr.definitionName));
          } else {
            inputName = defName;
            if (instance && part.name === "this") {
              inputCheck = pr.type;
            } else if (pr.type == "number" && pr.shadowBlockId && pr.shadowBlockId == "value") {
              inputName = undefined;
              fields.push(namedField(new Blockly.FieldTextInput("0", Blockly.FieldTextInput.numberValidator), defName));
            } else if (pr.type == "string" && pr.shadowOptions && pr.shadowOptions.toString) {
              inputCheck = null;
            } else {
              inputCheck = getBlocklyCheckForType(pr.type, info);
            }
          }
        }
      });

      let input;

      if (inputName) {
        input = block.appendValueInput(inputName);
        input.setAlign(Blockly.ALIGN_LEFT);
      } else if (expanded) {
        const prefix = hasParameter ? optionalInputWithFieldPrefix : optionalDummyInputPrefix;
        input = block.appendDummyInput(prefix + anonIndex++);
      } else {
        input = block.appendDummyInput();
      }

      if (inputCheck) {
        input.setCheck(inputCheck);
      }

      fields.forEach((f) => input.appendField(f.field, f.name));
    });

    imgConv.logTime();
  }
}
