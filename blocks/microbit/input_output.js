/**
 * @fileoverview Input/Output blocks for Micro:bit.
 */

Blockly.defineBlocksWithJsonArray([
    // BEGIN JSON EXTRACT

    /*Begin micro:bit blocks*/

    // MICRO:BIT PAUSE JSON
    {
        type: "io_pause",
        message0: "%{BKY_IO_PAUSE_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "TIME",
                check: "Number",
            },
            {
                type: "field_dropdown",
                name: "UNIT",
                options: [
                    ["%{BKY_IO_PAUSE_SECOND}", "1000"],
                    ["%{BKY_IO_PAUSE_MILLISECOND}", "1"],
                ],
            },
        ],
        previousStatement: null,
        nextStatement: null,
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "control_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_PAUSE_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK INIT CHRONOMETER
    {
        type: "io_initChronometer",
        message0: "%{BKY_IO_INITCHRONOMETER_TITLE}",
        previousStatement: null,
        nextStatement: null,
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_INITCHRONOMETER_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GET CHRONOMETER
    {
        type: "io_getChronometer",
        message0: "%{BKY_IO_GETCHRONOMETER_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "UNIT",
                options: [
                    ["(s)", "1000.0"],
                    ["(ms)", "1.0"],
                ],
            },
        ],
        output: "Number",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_GETCHRONOMETER_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK IF BUTTON PRESSED
    {
        type: "io_ifButtonPressed",
        message0: "%{BKY_IO_IFBUTTONPRESSED_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "BUTTON",
                options: [
                    ["A", "a"],
                    ["B", "b"],
                    // ["A+B", "a+b"],
                ],
            },
            {
                type: "field_dropdown",
                name: "STATE",
                options: [
                    ["%{BKY_IO_ISPRESSED}", "is_"],
                    ["%{BKY_IO_WASPRESSED}", "was_"],
                ],
            },
        ],
        message1: "%1",
        args1: [
            {
                type: "input_statement",
                name: "DO",
            },
        ],
        previousStatement: null,
        nextStatement: null,
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_IFBUTTONPRESSED_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK ON PIN PRESSED
    {
        type: "io_onPinPressed",
        message0: "%{BKY_IO_ONPINTOUCHED_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_TOUCH_PINS,
            },
        ],
        message1: "%1",
        args1: [
            {
                type: "input_statement",
                name: "DO",
            },
        ],
        previousStatement: null,
        nextStatement: null,
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_ONPINTOUCHED_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK ON MOUVEMENT
    {
        type: "io_onMovement",
        message0: "%{BKY_IO_ONMOVEMENT_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "MOV",
                options: [
                    ["%{BKY_IO_ONMOVEMENT_SHAKE}", "shake"],
                    ["%{BKY_IO_ONMOVEMENT_UP}", "up"], //logo up
                    ["%{BKY_IO_ONMOVEMENT_DOWN}", "down"], //logo down
                    ["%{BKY_IO_ONMOVEMENT_FACE_UP}", "face up"], //screen up
                    ["%{BKY_IO_ONMOVEMENT_FACE_DOWN}", "face down"], //screen down
                    ["%{BKY_IO_ONMOVEMENT_LEFT}", "left"], //tilt left
                    ["%{BKY_IO_ONMOVEMENT_RIGHT}", "right"], //tilt right
                    ["%{BKY_IO_ONMOVEMENT_FREEFALL}", "freefall"],
                    ["3g", "3g"],
                    ["6g", "6g"],
                    ["8g", "8g"],
                ],
            },
        ],
        message1: "%1",
        args1: [
            {
                type: "input_statement",
                name: "DO",
            },
        ],
        previousStatement: null,
        nextStatement: null,
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_ONMOVEMENT_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK IS BUTTON PRESSED
    {
        type: "io_isButtonPressed",
        message0: "%{BKY_IO_ISBUTTONPRESSED_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "BUTTON",
                options: [
                    ["A", "a"],
                    ["B", "b"],
                    // ["A+B", "a+b"],
                ],
            },
            {
                type: "field_dropdown",
                name: "STATE",
                options: [
                    ["%{BKY_IO_ISPRESSED}", "is_"],
                    ["%{BKY_IO_WASPRESSED}", "was_"],
                ],
            },
        ],
        output: "Boolean",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_ISBUTTONPRESSED_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK IS PIN PRESSED
    {
        type: "io_isPinPressed",
        message0: "%{BKY_IO_ISPINTOUCHED_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_TOUCH_PINS,
            },
        ],
        output: "Boolean",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_ISPINTOUCHED_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    /*Begin microphone module blocks */

    {
        type: "io_micro_onSoundDetected",
        message0: "%{BKY_IO_MICRO_ONSOUNDDETECTED_TITLE}",
        args0: [
            {
                type: "field_image",
                src: "assets/media/microphone.svg",
                width: 18,
                height: 25,
                alt: "*",
            },
            {
                type: "field_dropdown",
                name: "STATE",
                options: [
                    ["%{BKY_IO_MICRO_LOUD}", "LOUD"],
                    ["%{BKY_IO_MICRO_QUIET}", "QUIET"],
                ],
            },
            {
                type: "field_dropdown",
                name: "TYPE",
                options: [
                    ["%{BKY_IO_MICRO_IS}", "IS"],
                    ["%{BKY_IO_MICRO_WAS}", "WAS"],
                ],
            },
        ],
        message1: "%1",
        args1: [
            {
                type: "input_statement",
                name: "DO",
            },
        ],
        previousStatement: null,
        nextStatement: null,
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sound_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_MICRO_ONSOUNDDETECTED_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK MICROPHONE _ GET CURRENT SOUND
    {
        type: "io_micro_getCurrentSound",
        message0: "%{BKY_IO_MICRO_GETCURRENTSOUND_TITLE}",
        args0: [
            {
                type: "field_image",
                src: "assets/media/microphone.svg",
                width: 18,
                height: 25,
                alt: "*",
            },
        ],
        output: "String",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sound_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_MICRO_GETCURRENTSOUND_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK MICRO _ WAS SOUND DETECTED
    {
        type: "io_micro_wasSoundDetected",
        message0: "%{BKY_IO_MICRO_WASSOUNDDETECTED_TITLE}",
        args0: [
            {
                type: "field_image",
                src: "assets/media/microphone.svg",
                width: 18,
                height: 25,
                alt: "*",
            },
            {
                type: "field_dropdown",
                name: "STATE",
                options: [
                    ["%{BKY_IO_MICRO_LOUD}", "LOUD"],
                    ["%{BKY_IO_MICRO_QUIET}", "QUIET"],
                ],
            },
        ],
        output: "Boolean",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sound_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_MICRO_WASSOUNDDETECTED_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK MICROPHONE _ GET SOUND LEVEL
    {
        type: "io_micro_getSoundLevel",
        message0: "%{BKY_IO_MICRO_GETSOUNDLEVEL_TITLE}",
        args0: [
            {
                type: "field_image",
                src: "assets/media/microphone.svg",
                width: 18,
                height: 25,
                alt: "*",
            },
        ],
        output: "Number",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sound_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_MICRO_GETSOUNDLEVEL_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK MICROPHONE _ GET HISTORY SOUND
    {
        type: "io_micro_getHistorySounds",
        message0: "%{BKY_IO_MICRO_GETHISTORYSOUND_TITLE}",
        args0: [
            {
                type: "field_image",
                src: "assets/media/microphone.svg",
                width: 18,
                height: 25,
                alt: "*",
            },
        ],
        output: "Array",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sound_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_MICRO_GETHISTORYSOUND_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK MICROPHONE _ ON LOUD/QUIET SOUND WAS
    {
        type: "io_micro_setSoundThreshold",
        message0: "%{BKY_IO_MICRO_SETSOUNDTHRESHOLD_TITLE}",
        args0: [
            {
                type: "field_image",
                src: "assets/media/microphone.svg",
                width: 18,
                height: 25,
                alt: "*",
            },
            {
                type: "field_dropdown",
                name: "STATE",
                options: [
                    ["%{BKY_IO_MICRO_LOUD}", "LOUD"],
                    ["%{BKY_IO_MICRO_QUIET}", "QUIET"],
                ],
            },
            {
                type: "input_value",
                name: "THRESH",
                check: "Number",
            },
        ],
        previousStatement: null,
        nextStatement: null,
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sound_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_MICRO_SETSOUNDTHRESHOLD_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK MICROHONE _ GET (LOUD/QUIET) CONSTANT
    {
        type: "io_micro_soundCondition",
        message0: "%{BKY_IO_MICRO_SOUNDCONDITION_TITLE}",
        args0: [
            {
                type: "field_image",
                src: "assets/media/microphone.svg",
                width: 18,
                height: 25,
                alt: "*",
            },
            {
                type: "field_dropdown",
                name: "STATE",
                options: [
                    ["%{BKY_IO_MICRO_LOUD}", "LOUD"],
                    ["%{BKY_IO_MICRO_QUIET}", "QUIET"],
                ],
            },
        ],
        output: "String",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sound_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_MICRO_SOUNDCONDITION_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    /*Begin external module blocks*/

    // BLOCK GROVE KEYPAD _ GET NUMBER
    {
        type: "io_getKeypadNumber",
        message0: "%{BKY_IO_GROVEKEYPAD_GETNUMBER_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "RX",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
            {
                type: "field_dropdown",
                name: "TX",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        output: "String",
        inputsInline: true,
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_GROVEKEYPAD_GETNUMBER_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GROVE THUMB JOYSTICK _ GET POSITION
    {
        type: "io_getGroveThumbJoystick",
        message0: "%{BKY_IO_GROVEJOYSTICK_GETAXIS_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "AXIS",
                options: [
                    ["X", "X"],
                    ["Y", "Y"],
                ],
            },
            {
                type: "field_dropdown",
                name: "A1",
                options: Blockly.Constants.Pins.MICROBIT_ANALOG_READ_PINS,
            },
            {
                type: "field_dropdown",
                name: "A0",
                options: Blockly.Constants.Pins.MICROBIT_ANALOG_READ_PINS,
            },
        ],
        output: "Number",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_GROVEJOYSTICK_GETAXIS_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GROVE COLORED BUTTON _ READ DIGITAL
    {
        type: "io_getGroveColoredButton",
        message0: "%{BKY_IO_GROVECOLOREDBUTTON_GET_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        output: "Boolean",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_GROVECOLOREDBUTTON_GET_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GROVE COLORED BUTTON _ WRITE DIGITAL
    {
        type: "io_setGroveColoredButton",
        message0: "%{BKY_IO_GROVECOLOREDBUTTON_SETLED_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "STATE",
                check: "Boolean",
            },
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_GROVECOLOREDBUTTON_SETLED_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GROVE SLIDE POTENTIOMETER _ READ ANALOG
    {
        type: "io_getGroveSlidePotentiometer",
        message0: "%{BKY_IO_GETGROVESLIDEPOTENTIOMETER_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_ANALOG_READ_PINS,
            },
        ],
        output: "Number",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_GETGROVESLIDEPOTENTIOMETER_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GROVE ROTARY POTENTIOMETER _ READ ANALOG
    {
        type: "io_getGroveRotaryAngle",
        message0: "%{BKY_IO_GETGROVEROTARYANGLE_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_ANALOG_READ_PINS,
            },
        ],
        output: "Number",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_GETGROVEROTARYANGLE_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GROVE TACTILE BUTTON _ READ DIGITAL
    {
        type: "io_getGroveTactile",
        message0: "%{BKY_IO_GETGROVETACTILE_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        output: "Boolean",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_GETGROVETACTILE_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GROVE SIMPLE BUTTON _ READ DIGITAL
    {
        type: "io_getGroveButton",
        message0: "%{BKY_IO_GETGROVEBUTTON_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        output: "Boolean",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_GETGROVEBUTTON_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GROVE SWITCH BUTTON _ READ DIGITAL
    {
        type: "io_getGroveSwitch",
        message0: "%{BKY_IO_GETGROVESWITCH_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        output: "Boolean",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_GETGROVESWITCH_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    /*Begin control pins blocks*/

    // BLOCK ON/OFF BOOLEAN
    {
        type: "io_digital_signal",
        message0: "%{BKY_IO_DIGITALSIGNAL_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "BOOL",
                options: [
                    ["%{BKY_IO_DIGITALSIGNAL_HIGH}", "HIGH"],
                    ["%{BKY_IO_DIGITALSIGNAL_LOW}", "LOW"],
                ],
            },
        ],
        output: "Boolean",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_DIGITALSIGNAL_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK READ DIGITAL PIN
    {
        type: "io_readDigitalPin",
        message0: "%{BKY_IO_READDIGITALPIN_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        output: "Boolean",
        tooltip: "%{BKY_IO_READDIGITALPIN_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK WRITE DIGITAL PIN
    {
        type: "io_writeDigitalPin",
        message0: "%{BKY_IO_WRITEDIGITALPIN_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
            {
                type: "input_value",
                name: "STATE",
                check: "Boolean",
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_WRITEDIGITALPIN_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK READ ANALOG PIN
    {
        type: "io_readAnalogPin",
        message0: "%{BKY_IO_READANALOGPIN_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_ANALOG_READ_PINS,
            },
        ],
        output: "Number",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_READANALOGPIN_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK WRITE ANALOG
    {
        type: "io_writeAnalogPin",
        message0: "%{BKY_IO_WRITEANALOGPIN_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
            {
                type: "input_value",
                name: "VALUE",
                check: "Number",
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_WRITEANALOGPIN_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK SET PWM
    {
        type: "io_setPwm",
        message0: "%{BKY_IO_SETPWM_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "PERIOD",
                check: "Number",
            },
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_SETPWM_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK PULSE IN HIGH/LOW
    {
        type: "io_readPulseIn",
        message0: "%{BKY_IO_READPULSEIN_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "STATE",
                check: "Boolean",
            },
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        inputsInline: true,
        output: "Number",
        style: Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sensors_blocks" : "io_blocks",
        tooltip: "%{BKY_IO_READPULSEIN_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },
]); // END JSON EXTRACT (Do not delete this comment.)
