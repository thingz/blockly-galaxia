/**
 * @fileoverview Display blocks for Micro:bit.
 */

Blockly.defineBlocksWithJsonArray([
    // BEGIN JSON EXTRACT

    /* Start microbit screen blocks */

    //BLOCK SHOW NUMBER
    {
        type: "show_number",
        message0: "%{BKY_SHOW_NUMBER_TITLE} %1",
        args0: [
            {
                type: "input_value",
                name: "VALUE",
                check: "Number",
            },
        ],
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        helpurl: VITTASCIENCE_SITE,
    },

    //BLOCK SHOW LEDS
    {
        type: "show_leds",
        message0: "%{BKY_SHOW_LEDS_TITLE}",
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        helpurl: VITTASCIENCE_SITE,
        extensions: ["show_leds_screen_init"],
    },

    //BLOCK SHOW STRING
    {
        type: "show_string",
        message0: "%{BKY_SHOW_STRING_TITLE} %1",
        args0: [
            {
                type: "input_value",
                name: "TEXT",
            },
        ],
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        helpurl: VITTASCIENCE_SITE,
    },

    //BLOCK SHOW ICON
    {
        type: "show_icon",
        message0: "%{BKY_SHOW_ICON_TITLE} %1",
        args0: [
            {
                type: "field_dropdown",
                name: "ICON",
                options: [
                    [{ src: "assets/media/blocks_icons/heart.png", width: 32, height: 32, alt: "Red" }, "HEART"],
                    [{ src: "assets/media/blocks_icons/happy.png", width: 32, height: 32, alt: "Red" }, "HAPPY"],
                    [{ src: "assets/media/blocks_icons/sad.png", width: 32, height: 32, alt: "Red" }, "SAD"],
                    [{ src: "assets/media/blocks_icons/yes.png", width: 32, height: 32, alt: "Red" }, "YES"],
                    [{ src: "assets/media/blocks_icons/no.png", width: 32, height: 32, alt: "Red" }, "NO"],
                    [{ src: "assets/media/blocks_icons/man.png", width: 32, height: 32, alt: "Red" }, "STICKFIGURE"],
                    [{ src: "assets/media/blocks_icons/pitchfork.png", width: 32, height: 32, alt: "Red" }, "PITCHFORK"],
                    [{ src: "assets/media/blocks_icons/umbrella.png", width: 32, height: 32, alt: "Red" }, "UMBRELLA"],
                    [{ src: "assets/media/blocks_icons/skull.png", width: 32, height: 32, alt: "Red" }, "SKULL"],
                    [{ src: "assets/media/blocks_icons/chessboard.png", width: 32, height: 32, alt: "Red" }, "CHESSBOARD"],
                    [{ src: "assets/media/blocks_icons/butterfly.png", width: 32, height: 32, alt: "Red" }, "BUTTERFLY"],
                ],
            },
        ],
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        helpurl: VITTASCIENCE_SITE,
    },

    // BLOCK SHOW GAUGE
    {
        type: "show_gauge",
        message0: "%{BKY_SHOW_GAUGE_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "VALUE",
                check: "Number",
            },
            {
                type: "input_value",
                name: "MAX",
                check: "Number",
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_SHOW_GAUGE_TOOLTIP}",
        helpurl: VITTASCIENCE_SITE,
    },

    // BLOCK SET PIXEL
    {
        type: "set_pixel",
        message0: "%{BKY_SET_PIXEL_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "X",
                check: "Number",
            },
            {
                type: "input_value",
                name: "Y",
                check: "Number",
            },
            {
                type: "input_value",
                name: "STATE",
                check: "Boolean",
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_SET_PIXEL_TOOLTIP}",
        helpurl: VITTASCIENCE_SITE,
    },

    // BLOCK SET LIGHT PIXEL
    {
        type: "set_light_pixel",
        message0: "%{BKY_SET_LIGHT_PIXEL_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "X",
                check: "Number",
            },
            {
                type: "input_value",
                name: "Y",
                check: "Number",
            },
            {
                type: "input_value",
                name: "LIGHT",
                check: "Number",
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_SET_LIGHT_PIXEL_TOOLTIP}",
        helpurl: VITTASCIENCE_SITE,
    },

    //BLOCK SHOW CLOCK ICON
    {
        type: "show_clock",
        message0: "%{BKY_SHOW_CLOCK_TITLE} %1",
        args0: [
            {
                type: "input_value",
                name: "CLOCK",
                check: "Number",
            },
        ],
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        helpurl: VITTASCIENCE_SITE,
    },

    //BLOCK SHOW ARROW ICON
    {
        type: "show_arrow",
        message0: "%{BKY_SHOW_ARROW_TITLE} %1",
        args0: [
            {
                type: "field_dropdown",
                name: "ARROW",
                options: [
                    ["Nord", "N"],
                    ["Nord-Est", "NE"],
                    ["Est", "E"],
                    ["Sud-Est", "SE"],
                    ["Sud", "S"],
                    ["Sud-Ouest", "SW"],
                    ["Ouest", "W"],
                    ["Nord-Ouest", "NW"],
                ],
            },
        ],
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_SHOW_ARROW_TOOLTIP}",
        helpurl: VITTASCIENCE_SITE,
    },

    //BLOCK SCREEN CLEAR
    {
        type: "clear",
        message0: "%{BKY_CLEAR_TITLE}",
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
    },

    /* Start external screens blocks */

    // GROVE I2C LCD1602 RGB MODULE _ SET TEXT JSON
    {
        type: "display_lcdSetText",
        message0: "%{BKY_DISPLAY_LCD_SETTEXT_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "TEXT",
            },
            {
                type: "field_dropdown",
                name: "LINE",
                options: [
                    ["0", "0"],
                    ["1", "1"],
                ],
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_LCD_SETTEXT_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // GROVE I2C LCD1602 RGB MODULE _ CLEAR SCREEN JSON
    {
        type: "display_lcdClear",
        message0: "%{BKY_DISPLAY_LCD_CLEAR_TITLE}",
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_LCD_CLEAR_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK OLED ADD TEXT
    {
        type: "display_addOledText",
        message0: "%{BKY_DISPLAY_OLED_ADDTEXT_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "TEXT",
                check: "String",
            },
            {
                type: "input_value",
                name: "X",
                check: "Number",
            },
            {
                type: "input_value",
                name: "Y",
                check: "Number",
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_OLED_ADDTEXT_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK OLED SET PIXEL
    {
        type: "display_setOledPixel",
        message0: "%{BKY_DISPLAY_OLED_SETPIXEL_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "X",
                check: "Number",
            },
            {
                type: "input_value",
                name: "Y",
                check: "Number",
            },
            {
                type: "input_value",
                name: "STATE",
                check: "Boolean",
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_OLED_SETPIXEL_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK OLED SET PIXEL
    {
        type: "display_showOledIcon",
        message0: "%{BKY_DISPLAY_OLED_DRAWICON_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "ICON",
                options: [
                    [{ src: "assets/media/blocks_icons/heart.png", width: 32, height: 32, alt: "Red" }, "HEART"],
                    [{ src: "assets/media/blocks_icons/happy.png", width: 32, height: 32, alt: "Red" }, "HAPPY"],
                    [{ src: "assets/media/blocks_icons/sad.png", width: 32, height: 32, alt: "Red" }, "SAD"],
                    [{ src: "assets/media/blocks_icons/yes.png", width: 32, height: 32, alt: "Red" }, "YES"],
                    [{ src: "assets/media/blocks_icons/no.png", width: 32, height: 32, alt: "Red" }, "NO"],
                    [{ src: "assets/media/blocks_icons/man.png", width: 32, height: 32, alt: "Red" }, "STICKFIGURE"],
                    [{ src: "assets/media/blocks_icons/pitchfork.png", width: 32, height: 32, alt: "Red" }, "PITCHFORK"],
                    [{ src: "assets/media/blocks_icons/umbrella.png", width: 32, height: 32, alt: "Red" }, "UMBRELLA"],
                    [{ src: "assets/media/blocks_icons/skull.png", width: 32, height: 32, alt: "Red" }, "SKULL"],
                    [{ src: "assets/media/blocks_icons/chessboard.png", width: 32, height: 32, alt: "Red" }, "CHESSBOARD"],
                    [{ src: "assets/media/blocks_icons/butterfly.png", width: 32, height: 32, alt: "Red" }, "BUTTERFLY"],
                ],
            },
            {
                type: "input_value",
                name: "X",
                check: "Number",
            },
            {
                type: "input_value",
                name: "Y",
                check: "Number",
            },
            {
                type: "input_value",
                name: "STATE",
                check: "Boolean",
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_OLED_DRAWICON_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK OLED CLEAR DISPLAY
    {
        type: "display_clearOledScreen",
        message0: "%{BKY_DISPLAY_OLED_CLEARSCREEN_TITLE}",
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_OLED_CLEARSCREEN_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    /* Start led module blocks */

    // GROVE LED MODULE _ WRITE DIGITAL JSON
    {
        type: "display_setGroveSocketLed",
        message0: "%{BKY_DISPLAY_SETGROVELED_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "STATE",
                check: "Boolean",
            },
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_SETGROVELED_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // LED MODULE _ WRITE ANALOG PWM
    {
        type: "display_setLEDintensity",
        message0: "%{BKY_DISPLAY_SETLEDINTENSITY_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "VALUE",
                check: "Number",
            },
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_SETLEDINTENSITY_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK NEOPIXEL _ DEFINE NEOPIXEL
    {
        type: "display_defineNeopixel",
        message0: "%{BKY_DISPLAY_NEOPIXEL_DEFINE_TITLE}",
        args0: [
            {
                type: "field_number",
                name: "N",
                value: 20,
            },
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_NEOPIXEL_DEFINE_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK CONTROL NEOPIXEL LED
    {
        type: "display_controlNeopixelLed",
        message0: "%{BKY_DISPLAY_NEOPIXEL_LEDCONTROL_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "LED",
                check: "Number",
            },
            {
                type: "input_value",
                name: "R",
                check: "Number",
            },
            {
                type: "input_value",
                name: "G",
                check: "Number",
            },
            {
                type: "input_value",
                name: "B",
                check: "Number",
            },
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_NEOPIXEL_LEDCONTROL_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK CONTROL NEOPIXEL LED WITH COLOR
    {
        type: "display_controlColorNeopixelLed",
        message0: "%{BKY_DISPLAY_NEOPIXEL_SETPALETTECOLOR_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "LED",
                check: "Number",
            },
            {
                type: "input_value",
                name: "COLOR",
                check: "Colour",
            },
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_NEOPIXEL_SETPALETTECOLOR_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK CONTROL NEOPIXEL LED WITH COLOR
    {
        type: "display_neopixel_controlAllLedRGB",
        message0: "%{BKY_DISPLAY_NEOPIXEL_SETALLLEDRGB_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "R",
                check: "Number",
            },
            {
                type: "input_value",
                name: "G",
                check: "Number",
            },
            {
                type: "input_value",
                name: "B",
                check: "Number",
            },
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_NEOPIXEL_SETALLLEDRGB_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK CONTROL NEOPIXEL LED WITH COLOR
    {
        type: "display_neopixel_controlAllLedPalette",
        message0: "%{BKY_DISPLAY_NEOPIXEL_SETALLLEDCOLOR_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "COLOR",
                check: "Colour",
            },
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_NEOPIXEL_SETALLLEDCOLOR_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK RAINBOW NEOPIXEL
    {
        type: "display_rainbowNeopixel",
        message0: "%{BKY_DISPLAY_NEOPIXEL_RAINBOW_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_NEOPIXEL_RAINBOW_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK SET NUMBER GROVE 4DIGIT
    {
        type: "display_setNumberGrove4Digit",
        message0: "%{BKY_DISPLAY_4DIGIT_SETNUMBER_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "SHOW",
                options: [
                    ["%{BKY_DISPLAY_4DIGIT_NUMBER}", "NUM"],
                    ["%{BKY_DISPLAY_4DIGIT_TEMPERATURE}", "TEMP"],
                ],
            },
            {
                type: "input_value",
                name: "N",
                check: "Number",
            },
            {
                type: "field_dropdown",
                name: "CLK",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
            {
                type: "field_dropdown",
                name: "DIO",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_4DIGIT_SETNUMBER_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GROVE 4DIGIT SET CLOCK
    {
        type: "display_setClockGrove4Digit",
        message0: "%{BKY_DISPLAY_4DIGIT_SETCLOCK_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "CLK",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
            {
                type: "field_dropdown",
                name: "DIO",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_4DIGIT_SETCLOCK_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // GROVE LED BAR MODULE _  DISPLAY JSON
    {
        type: "display_setLevelLedBar",
        message0: "%{BKY_DISPLAY_LEDBARSETLEVEL_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "VALUE",
                check: "Number",
            },
            {
                type: "field_dropdown",
                name: "DI",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
            {
                type: "field_dropdown",
                name: "DCKI",
                options: Blockly.Constants.Pins.MICROBIT_PINS,
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_LEDBARSETLEVEL_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK SET TRAFFIC LIGHT
    {
        type: "display_setTrafficLight",
        message0: "%{BKY_DISPLAY_TRAFFICLIGHT_SETLED_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "PIN",
                options: [
                    ["%{BKY_DISPLAY_TRAFFICLIGHT_RED}", "pin0"],
                    ["%{BKY_DISPLAY_TRAFFICLIGHT_ORANGE}", "pin1"],
                    ["%{BKY_DISPLAY_TRAFFICLIGHT_GREEN}", "pin2"],
                ],
            },
            {
                type: "input_value",
                name: "STATE",
                check: "Boolean",
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_TRAFFICLIGHT_SETLED_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    /* Start morpion blocks */

    // BLOCK MORPION NEW GAME
    {
        type: "display_morpionNewGame",
        message0: "%{BKY_DISPLAY_MORPION_NEWGAME_TITLE}",
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_MORPION_NEWGAME_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK MORPION MOVE CURSOR
    {
        type: "display_morpionMoveCursor",
        message0: "%{BKY_DISPLAY_MORPION_MOVECURSOR_TITLE}",
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_MORPION_MOVECURSOR_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK MORPION SET CROSSES/CIRCLE
    {
        type: "display_morpionSetPlayerFigure",
        message0: "%{BKY_DISPLAY_MORPION_SETPLAYERFIGURE_TITLE}",
        args0: [
            {
                type: "field_dropdown",
                name: "FIGURE",
                options: [
                    ["%{BKY_DISPLAY_MORPION_CROSS}", "X"],
                    ["%{BKY_DISPLAY_MORPION_CIRCLE}", "O"],
                ],
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_MORPION_SETPLAYERFIGURE_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK MORPION IS END GAME
    {
        type: "display_morpionIsEndGame",
        message0: "%{BKY_DISPLAY_MORPION_ISENDGAME_TITLE}",
        inputsInline: true,
        output: "Boolean",
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_MORPION_ISENDGAME_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GAMES _ CREATE SPRITE
    {
        type: "display_games_createSprite",
        message0: "%{BKY_DISPLAY_GAMES_CREATESPRITE_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "X",
                check: "Number",
            },
            {
                type: "input_value",
                name: "Y",
                check: "Number",
            },
        ],
        inputsInline: true,
        output: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_GAMES_CREATESPRITE_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GAMES _ DELETE SPRITE
    {
        type: "display_games_deleteSprite",
        message0: "%{BKY_DISPLAY_GAMES_DELETESPRITE_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "SPRITE",
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_GAMES_DELETESPRITE_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GAMES _ IS SPRITE DELETED
    {
        type: "display_games_isSpriteDeleted",
        message0: "%{BKY_DISPLAY_GAMES_ISSPRITEDELETED_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "SPRITE",
            },
        ],
        inputsInline: true,
        output: "Boolean",
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_GAMES_ISSPRITEDELETED_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GAMES _ MOVE SPRITE
    {
        type: "display_games_moveSprite",
        message0: "%{BKY_DISPLAY_GAMES_MOVESPRITE_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "SPRITE",
            },
            {
                type: "input_value",
                name: "STEP",
                check: "Number",
            },
            {
                type: "field_dropdown",
                name: "DIR",
                options: [
                    ["%{BKY_DISPLAY_GAMES_LEFT}", "LEFT"],
                    ["%{BKY_DISPLAY_GAMES_RIGHT}", "RIGHT"],
                    ["%{BKY_DISPLAY_GAMES_UP}", "UP"],
                    ["%{BKY_DISPLAY_GAMES_DOWN}", "DOWN"],
                ],
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_GAMES_MOVESPRITE_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GAMES _ SPRITE POSITION
    {
        type: "display_games_getSpritePosition",
        message0: "%{BKY_DISPLAY_GAMES_GETSPRITEPOSITION_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "SPRITE",
            },
            {
                type: "field_dropdown",
                name: "POS",
                options: [
                    ["x", "x"],
                    ["y", "y"],
                ],
            },
        ],
        inputsInline: true,
        output: "Number",
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_GAMES_GETSPRITEPOSITION_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GAMES _ CHANGE SCORE
    {
        type: "display_games_changeScore",
        message0: "%{BKY_DISPLAY_GAMES_CHANGESCORE_TITLE}",
        args0: [
            {
                type: "input_value",
                name: "N",
                check: "Number",
            },
        ],
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_GAMES_CHANGESCORE_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GAMES _ GET SCORE
    {
        type: "display_games_getScore",
        message0: "%{BKY_DISPLAY_GAMES_GETSCORE_TITLE}",
        inputsInline: true,
        output: "Number",
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_GAMES_GETSCORE_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GAMES _ STOP GAME
    {
        type: "display_games_stopGame",
        message0: "%{BKY_DISPLAY_GAMES_STOPGAME_TITLE}",
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_GAMES_STOPGAME_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GAMES _ IS END GAME ?
    {
        type: "display_games_isEndGame",
        message0: "%{BKY_DISPLAY_GAMES_ISENDGAME_TITLE}",
        inputsInline: true,
        output: "Boolean",
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_GAMES_ISENDGAME_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    // BLOCK GAMES _ RESTART GAME
    {
        type: "display_games_restartGame",
        message0: "%{BKY_DISPLAY_GAMES_RESTARTGAME_TITLE}",
        inputsInline: true,
        previousStatement: null,
        nextStatement: null,
        style: "display_blocks",
        tooltip: "%{BKY_DISPLAY_GAMES_RESTARTGAME_TOOLTIP}",
        helpUrl: VITTASCIENCE_SITE,
    },

    /* Start games blocks */
]); // END JSON EXTRACT (Do not delete this comment.)

Blockly.Constants.Display = Object.create(null);

/**
 * Performs setup of 'show_leds' block for screen checkboxes display.
 * @this {Blockly.Block}
 */
Blockly.Constants.Display.DISPLAY_SHOW_LEDS_INIT_EXTENSION = function () {
    for (var row = 0; row < 5; row++) {
        var rowBoxes = this.appendDummyInput("ROW" + row);
        for (var column = 0; column < 5; column++) {
            rowBoxes.appendField(new Blockly.FieldCheckbox("FALSE"), "LED" + row + "" + column);
        }
    }
};

// Initialization extensions
Blockly.Extensions.register("show_leds_screen_init", Blockly.Constants.Display.DISPLAY_SHOW_LEDS_INIT_EXTENSION);
