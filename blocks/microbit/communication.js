/**
 * @fileoverview Communication blocks for Micro:bit.
 */

Blockly.defineBlocksWithJsonArray([ // BEGIN JSON EXTRACT

    /*Begin micro:bit radio blocks*/

    // BLOCK RADIO SEND STRING
    {
        "type": "communication_radioSendString",
        "message0": "%{BKY_COMMUNICATION_RADIO_SENDSTRING_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/radio.svg",
            "width": 25,
            "height": 25,
            "alt": "*"
          }, {
            "type": "input_value",
            "name": "STR",
            "check": "String"
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_RADIO_SENDSTRING_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK RADIO SEND NUMBER
    {
        "type": "communication_radioSendNumber",
        "message0": "%{BKY_COMMUNICATION_RADIO_SENDNUMBER_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/radio.svg",
            "width": 25,
            "height": 25,
            "alt": "*"
          }, {
            "type": "input_value",
            "name": "N",
            "check": ["Number", "Boolean"]
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_RADIO_SENDNUMBER_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK RADIO SEND VALUE
    {
        "type": "communication_radioSendValue",
        "message0": "%{BKY_COMMUNICATION_RADIO_SENDVALUE_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/radio.svg",
            "width": 25,
            "height": 25,
            "alt": "*"
          }, {
            "type": "input_value",
            "name": "NAME",
            "check": "String"
        }, {
            "type": "input_value",
            "name": "VALUE",
            "check": ["Number", "Boolean"]
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_RADIO_SENDVALUE_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    //BLOCK ON RADIO DATA RECEIVED
    {
        "type": "communication_onRadioDataReceived",
        "message0": "%{BKY_COMMUNICATION_RADIO_ONSTRINGRECEIVED_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/radio.svg",
            "width": 25,
            "height": 25,
            "alt": "*"
          }, {
            "type": "field_variable",
            "name": "VAR",
            "variable": "stringData"
        }],
        "message1": "%1",
        "args1": [{
            "type": "input_statement",
            "name": "DO"
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_RADIO_ONSTRINGRECEIVED_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE,
        "extensions": [
            "block_field_variable_colour"            
        ]
    },

    //BLOCK ON RADIO DATA RECEIVED
    {
        "type": "communication_onRadioNumberReceived",
        "message0": "%{BKY_COMMUNICATION_RADIO_ONNUMBERRECEIVED_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/radio.svg",
            "width": 25,
            "height": 25,
            "alt": "*"
          }, {
            "type": "field_variable",
            "name": "VAR",
            "variable": "numberData"
        }],
        "message1": "%1",
        "args1": [{
            "type": "input_statement",
            "name": "DO"
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_RADIO_ONNUMBERRECEIVED_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE,
        "extensions": [
            "block_field_variable_colour"            
        ]
    },

    //BLOCK ON RADIO DATA RECEIVED
    {
        "type": "communication_onRadioValueReceived",
        "message0": "%{BKY_COMMUNICATION_RADIO_ONVALUERECEIVED_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/radio.svg",
            "width": 25,
            "height": 25,
            "alt": "*"
          }, {
            "type": "field_variable",
            "name": "NAME",
            "variable": "name"
        }, {
            "type": "field_variable",
            "name": "VALUE",
            "variable": "value"
        }],
        "message1": "%1",
        "args1": [{
            "type": "input_statement",
            "name": "DO"
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_RADIO_ONVALUERECEIVED_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE,
        "extensions": [
            "block_field_variable_colour"            
        ]
    },

    // BLOCK RADIO CONFIGURATION
    {
        "type": "communication_radioConfig",
        "message0": "%{BKY_COMMUNICATION_RADIO_CONFIG_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/radio.svg",
            "width": 25,
            "height": 25,
            "alt": "*"
          }, {
            "type": "input_value",
            "name": "CANAL",
            "check": "Number"
        }, {
            "type": "input_value",
            "name": "POWER",
            "check": "Number"
        }, {
            "type": "input_value",
            "name": "LEN",
            "check": "Number"
        }, {
            "type": "input_value",
            "name": "GROUP",
            "check": "Number"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_RADIO_CONFIG_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    /*Begin serial transmission blocks*/

    // BLOCK SERIAL WRITE 
    {
        "type": "communication_serialWrite",
        "message0": "%{BKY_COMMUNICATION_SERIAL_WRITE_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/laptop.svg",
            "width": 25,
            "height": 20,
            "alt": "*"
          }, {
            "type": "input_value",
            "name": "TEXT"
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_SERIAL_WRITE_TOOLTIP}",
    },

    // BLOCK ON SERIAL DATA AVAILABLE _ READ DATA 
    {
        "type": "communication_onSerialDataReceived",
        "message0": "%{BKY_COMMUNICATION_SERIAL_ONDATARECEIVED_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/laptop.svg",
            "width": 25,
            "height": 20,
            "alt": "*"
          }, {
            "type": "field_variable",
            "name": "VAR",
            "variable": "serialData"
          }],
        "message1": "%1",
        "args1": [{
            "type": "input_statement",
            "name": "DO"
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_SERIAL_ONDATARECEIVED_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE,
        "extensions": [
            "block_field_variable_colour"
        ]
    },

    // BLOCK GRAPH _ SERIAL WRITE
    {
        "type": "communication_graphSerialWrite",
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_WRITEGRAPH_TOOLTIP}",
        "extensions": [
            "block_buttons_plus_minus",
            "communication_graph_join_init"
          ],
        "mutator": "communication_graph_join_mutator"
    },

    // BLOCK GRAPH _ DATA FORMAT
    {
        "type": "communication_graphSerialWrite_datasFormat",
        "message0": "%{BKY_COMMUNICATION_PRINT_DATAS_TITLE}",
        "args0": [{
            "type": "field_input",
            "name": "NAME"
        }, {
            "type": "input_value",
            "name": "DATA"
        }],
        "output": "Number",
        "inputsInline": true,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_PRINT_DATAS_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK COMPUTER MUSIC _ PLAY NOTE
    {
        "type": "communication_playComputerMusic",
        "message0": "%{BKY_COMMUNICATION_COMPUTER_PLAYNOTE_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/laptop.svg",
            "width": 25,
            "height": 20,
            "alt": "*"
          }, {
            "type": "field_dropdown",
            "name": "NOTE",
            "options": [
                ["%{BKY_NOTE_C}", "261.63"],
                ["%{BKY_NOTE_C_SHARP}", "277.18"],
                ["%{BKY_NOTE_D}", "293.66"],
                ["%{BKY_NOTE_D_SHARP}", "311.13"],
                ["%{BKY_NOTE_E}", "329.63"],
                ["%{BKY_NOTE_F}", "349.23"],
                ["%{BKY_NOTE_F_SHARP}", "369.99"],
                ["%{BKY_NOTE_G}", "392.0"],
                ["%{BKY_NOTE_G_SHARP}", "415.3"],
                ["%{BKY_NOTE_A}", "440.0"],
                ["%{BKY_NOTE_A_SHARP}", "466.16"],
                ["%{BKY_NOTE_B}", "493.88"]
            ]
        }],
        "nextStatement": null,
        "previousStatement": null,
        "style": Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sound_blocks" : "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_COMPUTER_PLAYNOTE_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK COMPUTER MUSIC _ SET FREQUENCY
    {
        "type": "communication_playComputerFrequency",
        "message0": "%{BKY_COMMUNICATION_COMPUTER_SETFREQUENCY_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/laptop.svg",
            "width": 25,
            "height": 20,
            "alt": "*"
          }, {
            "type": "input_value",
            "name": "FREQUENCY",
            "check": "Number"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sound_blocks" : "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_COMPUTER_SETFREQUENCY_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK COMPUTER MUSIC _ STOP MUSIC
    {
        "type": "communication_stopComputerMusic",
        "message0": "%{BKY_COMMUNICATION_COMPUTER_STOPMUSIC_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/laptop.svg",
            "width": 25,
            "height": 20,
            "alt": "*"
        }],
        "nextStatement": null,
        "previousStatement": null,
        "style": Blockly.Constants.getToolboxStyle() == TOOLBOX_STYLE_SCRATCH ? "sound_blocks" : "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_COMPUTER_STOPMUSIC_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    //BLOCK SERIAL REDIRECT USB
    {
        "type": "communication_serialRedirectUSB",
        "message0": "%{BKY_COMMUNICATION_SERIAL_REDIRECTTOUSB_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/laptop.svg",
            "width": 25,
            "height": 20,
            "alt": "*"
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_SERIAL_REDIRECTTOUSB_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    //BLOCK SERIAL INIT
    {
        "type": "communication_serialInit",
        "message0": "%{BKY_COMMUNICATION_SERIAL_INIT_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/laptop.svg",
            "width": 25,
            "height": 20,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "BAUD",
            "options": [
                ["9600", "9600"],
                ["14400", "14400"],
                ["19200", "19200"],
                ["28800", "28800"],
                ["38400", "38400"],
                ["57600", "57600"],
                ["115200", "115200"]
            ]
        }, {
            "type": "field_dropdown",
            "name": "RX",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }, {
            "type": "field_dropdown",
            "name": "TX",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_SERIAL_INIT_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    /*Begin data logging blocks*/

    // BLOCK WRITE ON OPENLOG SD CARD
    {
        "type": "communication_writeOpenLogSd",
        "message0": "%{BKY_COMMUNICATION_OPENLOG_WRITE_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/sd.svg",
            "width": 18,
            "height": 24,
            "alt": "*"
        }, {
            "type": "input_dummy"
        }, {
            "type": "field_dropdown",
            "name": "RX",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }, {
            "type": "field_dropdown",
            "name": "TX",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }, {
            "type": "input_dummy"
        }, {
            "type": "input_value",
            "name": "DATA",
            "check": ["String"]
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_OPENLOG_WRITE_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    /*Begin wireless communication blocks*/

    // BLOCK HC05 SERIAL BLUETOOTH _ SEND DATA
    {
        "type": "communication_sendBluetoothData",
        "message0": "%{BKY_COMMUNICATION_BLUETOOTH_SENDDATA_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/bluetooth.svg",
            "width": 20,
            "height": 20,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "RX",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }, {
            "type": "field_dropdown",
            "name": "TX",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }, {
            "type": "input_value",
            "name": "DATA",
            "check": ["String", "Number", "Boolean"]
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_BLUETOOTH_SENDDATA_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK HC05 SERIAL BLUETOOTH _ ON DATA RECEIVED
    {
        "type": "communication_onBluetoothDataReceived",
        "message0": "%{BKY_COMMUNICATION_BLUETOOTH_ONDATARECEIVED_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/bluetooth.svg",
            "width": 20,
            "height": 20,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "RX",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }, {
            "type": "field_dropdown",
            "name": "TX",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }, {
            "type": "field_variable",
            "name": "VAR",
            "variable": "bluetoothData"
        }],
        "message1": "%1",
        "args1": [{
            "type": "input_statement",
            "name": "DO"
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_BLUETOOTH_ONDATARECEIVED_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE,
        "extensions": [
            "block_field_variable_colour"
        ]
    },

    /*Begin tracking modules blocks*/

    // GROVE GPS MODULE _ ON GPS DATA AVAILABLE
    {
        "type": "communication_onGPSDataReceived",
        "message0": "%{BKY_COMMUNICATION_GPS_ONDATARECEIVED_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/gps.svg",
            "width": 20,
            "height": 25,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "RX",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }, {
            "type": "field_dropdown",
            "name": "TX",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }, {
            "type": "field_variable",
            "name": "VAR",
            "variable": "gpsData"
        }],
        "message1": "%1",
        "args1": [{
            "type": "input_statement",
            "name": "DO"
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_GPS_ONDATARECEIVED_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE,
        "extensions": [
            "block_field_variable_colour"
        ]
    },

    //BLOCK GET GPS INFORMATIONS
    {
        "type": "communication_analyzeGPSInfo",
        "message0": "%{BKY_COMMUNICATION_GPS_GETINFORMATIONS_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/gps.svg",
            "width": 20,
            "height": 25,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "INFO",
            "options": [
                ["%{BKY_COMMUNICATION_GPS_INFO_CLOCK}", "1"],
                ["%{BKY_COMMUNICATION_GPS_INFO_LATITUDE}", "2"],
                ["%{BKY_COMMUNICATION_GPS_INFO_LONGITUDE}", "4"],
                ["%{BKY_COMMUNICATION_GPS_INFO_SATELLITE}", "7"],
                ["%{BKY_COMMUNICATION_GPS_INFO_ALTITUDE}", "9"],
                ["%{BKY_COMMUNICATION_GPS_INFO_ALL_FRAME}", "0"]
            ]
        }, {
            "type": "input_value",
            "name": "DATA"
        }],
        "output": null,
        "style": "communication_blocks",
        "tooltip": "%{BKY_COMMUNICATION_GPS_GETINFORMATIONS_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    }

]); // END JSON EXTRACT (Do not delete this comment.)

Blockly.Constants.Communication = Object.create(null);

/**
 * Performs final setup of 'communication_graphSerialWrite' block.
 * @this {Blockly.Block}
 */
Blockly.Constants.Communication.COMMUNICATION_GRAPH_JOIN_INIT_EXTENSION = function() {
  this.itemCount_ = 1;
  this.updateShape_();
};

/**
 * Mixin for mutator functions in the 'communication_graph_join_mutator' extension.
 * @mixin
 * @augments Blockly.Block
 * @package
 */
Blockly.Constants.Communication.COMMUNICATION_GRAPH_JOIN_MUTATOR_MIXIN = {
    /**
     * Create XML to represent number of data inputs.
     * @return {!Element} XML storage element.
     * @this {Blockly.Block}
     */
    mutationToDom: function() {
        var container = Blockly.utils.xml.createElement('mutation');
        container.setAttribute('items', this.itemCount_);
        return container;
    },
    /**
     * Parse XML to restore the data inputs.
     * @param {!Element} xmlElement XML storage element.
     * @this {Blockly.Block}
     */
    domToMutation: function(xmlElement) {
        this.itemCount_ = parseInt(xmlElement.getAttribute('items'), 10);
        this.updateShape_();
    },
    /**
     * Store pointers to any connected child blocks.
     * @param {!Blockly.Block} containerBlock Root block in mutator.
     * @this {Blockly.Block}
     */
    saveConnections: function(containerBlock) {
        var itemBlock = containerBlock.getInputTargetBlock('STACK');
        var i = 0;
        while (itemBlock) {
            var input = this.getInput('ADD' + i);
            itemBlock.valueConnection_ = input && input.connection.targetConnection;
            i++;
            itemBlock = itemBlock.nextConnection &&
                itemBlock.nextConnection.targetBlock();
        }
    },
    storeValueConnections_: function() {
        this.valueConnections_ = [];
        for (var i = 0; i < this.itemCount_; i++) {
            this.valueConnections_.push(this.getInput('ADD' + i).connection.targetConnection);
        }
    },
    restoreValueConnections_: function() {
        for (var i = 0; i < this.itemCount_; i++) {
            Blockly.Mutator.reconnect(this.valueConnections_[i], this, 'ADD' + i);
        }
    },
    addItem_: function() {
        this.storeValueConnections_();
        var update = function() {
            this.itemCount_++;
        };
        this.update_(update);
        // Add a data block
        if (this.itemCount_ > 1) {
            this.addDataFormatBlock();
        }
        this.restoreValueConnections_();
    },
    removeItem_: function() {
        this.storeValueConnections_();
        var update = function() {
            this.itemCount_--;
        };
        this.update_(update);
        this.restoreValueConnections_();
    },
    update_: function(update) {
        return Blockly.Constants.Utils.UPDATE_BLOCK_MUTATOR_MIXIN(this, update);
    },
    /**
     * Modify this block to have the correct number of inputs.
     * @private
     * @this {Blockly.Block}
     */
    updateShape_: function() {
        var that = this;
        var remove = function() {
            that.removeItem_();
        };
        var add = function() {
            that.addItem_();
        };
        // Remove all inputs
        if (this.getInput('TOP')) this.removeInput('TOP');
        var i = 0;
        while (this.getInput('ADD' + i)) {
            this.removeInput('ADD' + i);
            i++;
        }
        // Update inputs
        var top = this.appendDummyInput('TOP');
        top.appendField(new Blockly.FieldImage("assets/media/laptop.svg", 25, 20, "*"));
        top.appendField(Blockly.Msg['COMMUNICATION_WRITEGRAPH_TITLE']);
        top.appendField(new Blockly.FieldImage(this.ADD_IMAGE_DATAURI, this.buttonSize, this.buttonSize, "*", add, false));
        if (this.itemCount_ > 1) {
            top.appendField(new Blockly.FieldImage(this.REMOVE_IMAGE_DATAURI, this.buttonSize, this.buttonSize, "*", remove, false));
        }
        for (var i = 0; i < this.itemCount_; i++) {
            this.appendValueInput('ADD' + i);
        }
        this.setOutputShape(Blockly.OUTPUT_SHAPE_SQUARE);
    },
    addDataFormatBlock: function() {
        var dataBlockName = "communication_graphSerialWrite_datasFormat";
        if (Blockly.Blocks[dataBlockName]) {
            var newBlock = Blockly.utils.xml.createElement('block');
            newBlock.setAttribute('type', dataBlockName);
            if (newBlock) {
                var id = Blockly.utils.genUid()
                newBlock.setAttribute('id', id);
                var field = Blockly.utils.xml.createElement('field');
                field.setAttribute('name', 'NAME');
                field.appendChild(Blockly.utils.xml.createTextNode(Blockly.Msg['COMMUNICATION_DATA'] + this.itemCount_));
                newBlock.appendChild(field);
                Blockly.Xml.domToBlock(newBlock, this.workspace);
                var block = this.workspace.getBlockById(id);
                this.valueConnections_.push(block.outputConnection);
            }
        }
    }
};

// Initialization extensions
Blockly.Extensions.register("communication_graph_join_init",
    Blockly.Constants.Communication.COMMUNICATION_GRAPH_JOIN_INIT_EXTENSION);

// Mutator
Blockly.Extensions.registerMutator('communication_graph_join_mutator',
    Blockly.Constants.Communication.COMMUNICATION_GRAPH_JOIN_MUTATOR_MIXIN);