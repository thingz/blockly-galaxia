/**
 * @fileoverview Robots blocks for Micro:bit.
 */

Blockly.defineBlocksWithJsonArray([ // BEGIN JSON EXTRACT

    /* Begin Maqueen blocks */

    // BLOCK MAQUEEN ULTRASONIC RANGER
    {
        "type": "robots_getMaqueenUltrasonicRanger",
        "message0": "%{BKY_ROBOTS_MAQUEEN_ULTRASONICRANGER_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/maqueen.png",
            "width": 35,
            "height": 35,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "DATA",
            "options": [
                ["%{BKY_ROBOTS_MAQUEEN_ULTRASONIC_DISTANCE}", "distance"],
                ["%{BKY_ROBOTS_MAQUEEN_ULTRASONIC_DURATION}", "duration"]
            ]
        }],
        "output": "Number",
        "inputsInline": true,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_MAQUEEN_ULTRASONICRANGER_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK MAQUEEN READ LINE FINDER
    {
        "type": "robots_readMaqueenPatrol",
        "message0": "%{BKY_ROBOTS_MAQUEEN_READPATROL_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/maqueen.png",
            "width": 35,
            "height": 35,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "PIN",
            "options": [
                ["%{BKY_ROBOTS_MAQUEEN_RIGHT}", "pin14"],
                ["%{BKY_ROBOTS_MAQUEEN_LEFT}", "pin13"]
            ]
        }],
        "output": "Boolean",
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_MAQUEEN_READPATROL_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK MAQUEEN CONTROL LED
    {
        "type": "robots_controlMaqueenLed",
        "message0": "%{BKY_ROBOTS_MAQUEEN_CONTROLLED_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/maqueen.png",
            "width": 35,
            "height": 35,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "LED",
            "options": [
                ["%{BKY_ROBOTS_MAQUEEN_RIGHT}", "1"],
                ["%{BKY_ROBOTS_MAQUEEN_LEFT}", "0"]
            ]
        }, {
            "type": "input_value",
            "name": "STATE",
            "check": "Boolean"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_MAQUEEN_CONTROLLED_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK MAQUEEN CONTROL GO
    {
        "type": "robots_setMaqueenGo",
        "message0": "%{BKY_ROBOTS_MAQUEEN_GO_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/maqueen.png",
            "width": 35,
            "height": 35,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "DIR",
            "options": [
                ["%{BKY_ROBOTS_MAQUEEN_GO_FORWARD}", "0x0"],
                ["%{BKY_ROBOTS_MAQUEEN_GO_REVERSE}", "0x1"]
            ]
        }, {
            "type": "input_value",
            "name": "SPEED",
            "check": "Number"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_MAQUEEN_GO_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK MAQUEEN CONTROL MOTOR
    {
        "type": "robots_controlMaqueenMotor",
        "message0": "%{BKY_ROBOTS_MAQUEEN_CONTROLMOTOR_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/maqueen.png",
            "width": 35,
            "height": 35,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "MOTOR",
            "options": [
                ["%{BKY_ROBOTS_MAQUEEN_RIGHT}", "0x02"],
                ["%{BKY_ROBOTS_MAQUEEN_LEFT}", "0x00"]
            ]
        }, {
            "type": "field_dropdown",
            "name": "DIR",
            "options": [
                ["↻", "0x0"],
                ["↺", "0x1"]
            ]
        }, {
            "type": "input_value",
            "name": "SPEED",
            "check": "Number"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_MAQUEEN_CONTROLMOTOR_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK MAQUEEN STOP MOTORS
    {
        "type": "robots_stopMaqueenMotors",
        "message0": "%{BKY_ROBOTS_MAQUEEN_STOPMOTORS_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/maqueen.png",
            "width": 35,
            "height": 35,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "MOTOR",
            "options": [
                ["%{BKY_ROBOTS_MAQUEEN_RIGHT}", "0x02"],
                ["%{BKY_ROBOTS_MAQUEEN_LEFT}", "0x00"],
                ["%{BKY_ROBOTS_MAQUEEN_RIGHT&LEFT}", "both"]
            ]
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_MAQUEEN_STOPMOTORS_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK MAQUEEN SET SERVO ANGLE
    {
        "type": "robots_setMaqueenServoAngle",
        "message0": "%{BKY_ROBOTS_MAQUEEN_SETSERVOANGLE_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/maqueen.png",
            "width": 35,
            "height": 35,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "SERVO",
            "options": [
                ["%{BKY_ROBOTS_MAQUEEN_S1}", "0x14"],
                ["%{BKY_ROBOTS_MAQUEEN_S2}", "0x15"],
                ["%{BKY_ROBOTS_MAQUEEN_SERVO_BOTH}", "both"]
            ]
        }, {
            "type": "input_value",
            "name": "ANGLE",
            "check": "Number"
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_MAQUEEN_SETSERVOANGLE_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK MAQUEEN SET NEOPIXEL
    {
        "type": "robots_setMaqueenNeopixel",
        "message0": "%{BKY_ROBOTS_MAQUEEN_SETNEOPIXEL_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/maqueen.png",
            "width": 35,
            "height": 35,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "LED",
            "options": [
                ["0", "0"],
                ["1", "1"],
                ["2", "2"],
                ["3", "3"],
                ["les 4 LED", "all"]
            ]
        }, {
            "type": "input_value",
            "name": "R",
            "check": "Number"
        }, {
            "type": "input_value",
            "name": "G",
            "check": "Number"
        }, {
            "type": "input_value",
            "name": "B",
            "check": "Number"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_MAQUEEN_SETNEOPIXEL_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK MAQUEEN SET NEOPIXEL PALETTE
    {
        "type": "robots_setMaqueenNeopixelPalette",
        "message0": "%{BKY_ROBOTS_MAQUEEN_SETPALETTECOLOR_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/maqueen.png",
            "width": 35,
            "height": 35,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "LED",
            "options": [
                ["0", "0"],
                ["1", "1"],
                ["2", "2"],
                ["3", "3"],
                ["les 4 LED", "all"]
            ]
        }, {
            "type": "input_value",
            "name": "COLOR",
            "check": "Colour"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_MAQUEEN_SETPALETTECOLOR_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK MAQUEEN RAINBOW
    {
        "type": "robots_setMaqueenRainbow",
        "message0": "%{BKY_ROBOTS_MAQUEEN_SETRAINBOW_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/maqueen.png",
            "width": 35,
            "height": 35,
            "alt": "*"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_MAQUEEN_SETRAINBOW_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK MAQUEEN SET BUZZER
    {
        "type": "robots_setMaqueenBuzzer",
        "message0": "%{BKY_ROBOTS_MAQUEEN_SETBUZZER_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/maqueen.png",
            "width": 35,
            "height": 35,
            "alt": "*"
        }, {
            "type": "input_value",
            "name": "FREQ",
            "check": "Number"
        }, {
            "type": "input_value",
            "name": "TIME",
            "check": "Number"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_MAQUEEN_SETBUZZER_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK MAQUEEN PLAY MUSIC
    {
        "type": "robots_playMaqueenMusic",
        "message0": "%{BKY_ROBOTS_MAQUEEN_PLAYMUSIC_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/maqueen.png",
            "width": 35,
            "height": 35,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "MUSIC",
            "options": [
                ["Gamme", "GAMME"],
                ["Star Wars", "SW"],
                ["R2D2", "R2D2"],
            ]
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_MAQUEEN_PLAYMUSIC_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    /* Begin CODO blocks */

    // BLOCK CODO CONTROL GO
    {
        "type": "robots_setCodoGo",
        "message0": "%{BKY_ROBOTS_CODO_GO_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/codo.png",
            "width": 45,
            "height": 35,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "DIR",
            "options": [
                ["%{BKY_ROBOTS_CODO_GO_FORWARD}", "FORWARD"],
                ["%{BKY_ROBOTS_CODO_GO_BACKWARD}", "BACKWARD"]
            ]
        }, {
            "type": "input_value",
            "name": "SPEED",
            "check": "Number"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_CODO_GO_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK CODO TURN ROBOT
    {
        "type": "robots_setCodoTurn",
        "message0": "%{BKY_ROBOTS_CODO_TURN_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/codo.png",
            "width": 45,
            "height": 35,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "DIR",
            "options": [
                ["%{BKY_ROBOTS_CODO_TURN_RIGHT}", "RIGHT"],
                ["%{BKY_ROBOTS_CODO_TURN_LEFT}", "LEFT"]
            ]
        }, {
            "type": "input_value",
            "name": "SPEED",
            "check": "Number"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_CODO_TURN_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK CODO TURN ROBOT
    {
        "type": "robots_setCodoStop",
        "message0": "%{BKY_ROBOTS_CODO_STOP_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/codo.png",
            "width": 45,
            "height": 35,
            "alt": "*"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_CODO_STOP_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK CODO CONTROL MOTOR
    {
        "type": "robots_controlCodoMotors",
        "message0": "%{BKY_ROBOTS_CODO_CONTROLMOTOR_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/codo.png",
            "width": 45,
            "height": 35,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "MOTOR",
            "options": [
                ["%{BKY_ROBOTS_CODO_MOTOR_RIGHT}", "RIGHT"],
                ["%{BKY_ROBOTS_CODO_MOTOR_LEFT}", "LEFT"]
            ]
        }, {
            "type": "field_dropdown",
            "name": "DIR",
            "options": [
                ["↻", "CLOCKWISE"],
                ["↺", "ANTICLOCKWISE"]
            ]
        }, {
            "type": "input_value",
            "name": "SPEED",
            "check": "Number"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_CODO_CONTROLMOTOR_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    /* Begin Bit:Bot blocks definitions */

    // BLOCK BITBOT READ LIGHT SENSOR
    {
        "type": "robots_readBitBotLightSensor",
        "message0": "%{BKY_ROBOTS_BITBOT_READLIGHTSENSOR_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "RL",
            "options": [
                ["%{BKY_ROBOTS_MAQUEEN_RIGHT}", "1"],
                ["%{BKY_ROBOTS_MAQUEEN_LEFT}", "0"]
            ]
        }],
        "output": "Number",
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_BITBOT_READLIGHTSENSOR_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK BITBOT CONTROL MOTOR GO
    {
        "type": "robots_setBitbotGo",
        "message0": "%{BKY_ROBOTS_BITBOT_GO_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "DIR",
            "options": [
                ["%{BKY_ROBOTS_BITBOT_GO_FORWARD}", "0"],
                ["%{BKY_ROBOTS_BITBOT_GO_REVERSE}", "1"]
            ]
        }, {
            "type": "input_value",
            "name": "SPEED",
            "check": "Number"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_BITBOT_GO_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK BITBOT CONTROL MOTOR
    {
        "type": "robots_controlBitBotMotor",
        "message0": "%{BKY_ROBOTS_BITBOT_CONTROLMOTOR_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "MOTOR",
            "options": [
                ["%{BKY_ROBOTS_BITBOT_RIGHT}", "pin1/pin12"],
                ["%{BKY_ROBOTS_BITBOT_LEFT}", "pin0/pin8"]
            ]
        }, {
            "type": "field_dropdown",
            "name": "DIR",
            "options": [
                ["↻", "0"],
                ["↺", "1"]
            ]
        }, {
            "type": "input_value",
            "name": "SPEED",
            "check": "Number"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_BITBOT_CONTROLMOTOR_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK BITBOT STOP MOTORS
    {
        "type": "robots_stopBitBotMotors",
        "message0": "%{BKY_ROBOTS_BITBOT_STOPMOTORS_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "MOTOR",
            "options": [
                ["%{BKY_ROBOTS_BITBOT_RIGHT}", "pin1/pin12"],
                ["%{BKY_ROBOTS_BITBOT_LEFT}", "pin0/pin8"],
                ["%{BKY_ROBOTS_BITBOT_RIGHTANDLEFT}", "all"]
            ]
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_BITBOT_STOPMOTORS_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK BITBOT SET NEOPIXEL PALETTE
    {
        "type": "robots_setBitBotNeopixelPalette",
        "message0": "%{BKY_ROBOTS_BITBOT_SETPALETTECOLOR_TITLE}",
        "args0": [{
            "type": "input_value",
            "name": "LED",
            "check": "Number"
        }, {
            "type": "input_value",
            "name": "COLOR",
            "check": "Colour"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_BITBOT_SETPALETTECOLOR_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK BITBOT SET NEOPIXEL
    {
        "type": "robots_setBitBotNeopixel",
        "message0": "%{BKY_ROBOTS_BITBOT_SETNEOPIXEL_TITLE}",
        "args0": [{
            "type": "input_value",
            "name": "LED",
            "check": "Number"
        }, {
            "type": "input_value",
            "name": "R",
            "check": "Number"
        }, {
            "type": "input_value",
            "name": "G",
            "check": "Number"
        }, {
            "type": "input_value",
            "name": "B",
            "check": "Number"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_BITBOT_SETNEOPIXEL_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK BITBOT RAINBOW
    {
        "type": "robots_setBitBotRainbow",
        "message0": "%{BKY_ROBOTS_BITBOT_SETRAINBOW_TITLE}",
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_BITBOT_SETRAINBOW_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK BITBOT READ LINE FINDER
    {
        "type": "robots_readBitBotPatrol",
        "message0": "%{BKY_ROBOTS_BITBOT_READPATROL_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "PIN",
            "options": [
                ["%{BKY_ROBOTS_BITBOT_RIGHT}", "pin5"],
                ["%{BKY_ROBOTS_BITBOT_LEFT}", "pin11"]
            ]
        }],
        "output": "Boolean",
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_BITBOT_READPATROL_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    /* Begin Gamepad blocks definitions */

    // BLOCK GAMEPAD CONTROL LED
    {
        "type": "robots_setGamepadLED",
        "message0": "%{BKY_ROBOTS_GAMEPAD_CONTROLLED_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/gamepad.png",
            "width": 40,
            "height": 20,
            "alt": "*"
        }, {
            "type": "input_value",
            "name": "STATE",
            "check": "Boolean"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_GAMEPAD_CONTROLLED_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GAMEPAD CONTROL LED
    {
        "type": "robots_setGamepadMotorVibration",
        "message0": "%{BKY_ROBOTS_GAMEPAD_SETMOTORVIBRATION_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/gamepad.png",
            "width": 40,
            "height": 20,
            "alt": "*"
        }, {
            "type": "input_value",
            "name": "STATE",
            "check": "Boolean"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_GAMEPAD_SETMOTORVIBRATION_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GAMEPAD SET BUZZER
    {
        "type": "robots_setGamepadBuzzerFreq",
        "message0": "%{BKY_ROBOTS_GAMEPAD_SETBUZZERFREQ_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/gamepad.png",
            "width": 40,
            "height": 20,
            "alt": "*"
        }, {
            "type": "input_value",
            "name": "FREQ",
            "check": "Number"
        }, {
            "type": "input_value",
            "name": "TIME",
            "check": "Number"
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_GAMEPAD_SETBUZZERFREQ_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GAMEPAD PLAY MUSIC
    {
        "type": "robots_playGamepadMusic",
        "message0": "%{BKY_ROBOTS_GAMEPAD_PLAYMUSIC_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/gamepad.png",
            "width": 40,
            "height": 20,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "MUSIC",
            "options": [
                ["Gamme", "GAMME"],
                ["Star Wars", "SW"],
                ["R2D2", "R2D2"],
            ]
        }],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_GAMEPAD_PLAYMUSIC_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    //BLOCK GAMEPAD ON BUTTON EVENT
    {
        "type": "robots_onGamepadButtonEvent",
        "message0": "%{BKY_ROBOTS_GAMEPAD_ONBUTTONEVENT_TITLE}",
        "args0": [{
            "type": "field_image",
            "src": "assets/media/gamepad.png",
            "width": 40,
            "height": 20,
            "alt": "*"
        }, {
            "type": "field_dropdown",
            "name": "BUTTON",
            "options": [
                ["X", "pin1"],
                ["Y", "pin2"],
                ["UP", "pin8"],
                ["DOWN", "pin13"],
                ["LEFT", "pin14"],
                ["RIGHT", "pin15"]
            ]
        }, {
            "type": "field_dropdown",
            "name": "STATE",
            "options": [
                ["%{BKY_ROBOTS_GAMEPAD_PRESSED}", "PRESSED"],
                ["%{BKY_ROBOTS_GAMEPAD_RELEASED}", "RELEASED"],
            ]
        }],
        "message1": "%1",
        "args1": [{
            "type": "input_statement",
            "name": "DO"
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "robots_blocks",
        "tooltip": "%{BKY_ROBOTS_GAMEPAD_ONBUTTONEVENT_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    }

]); // END JSON EXTRACT (Do not delete this comment.)