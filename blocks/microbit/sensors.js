/**
 * @fileoverview Sensors blocks for Micro:bit.
 */

Blockly.defineBlocksWithJsonArray([ // BEGIN JSON EXTRACT

    /* Begin board sensors blocks*/

    // BLOCK GET ACCELERATION
    {
        "type": "sensors_getAcceleration",
        "message0": "%{BKY_SENSORS_GETACCELERATION_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "AXIS",
            "options": [
                ["x", "x"],
                ["y", "y"],
                ["z", "z"],
                ["strength", "strength"]
            ]
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETACCELERATION_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GET LIGHT
    {
        "type": "sensors_getLight",
        "message0": "%{BKY_SENSORS_GETLIGHT_TITLE}",
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETLIGHT_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK CALIBRATE COMPASS
    {
        "type": "sensors_calibrateCompass",
        "message0": "%{BKY_SENSORS_CALIBRATECOMPASS_TITLE}",
        "previousStatement": null,
        "nextStatement": null,
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_CALIBRATECOMPASS_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GET COMPASS
    {
        "type": "sensors_getCompass",
        "message0": "%{BKY_SENSORS_GETCOMPASS_TITLE}",
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETCOMPASS_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK IS COMPASS CALIBRATED
    {
        "type": "sensors_isCompassCalibrated",
        "message0": "%{BKY_SENSORS_ISCOMPASSCALIBRATED_TITLE}",
        "output": "Boolean",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_ISCOMPASSCALIBRATED_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GET TEMPERATURE
    {
        "type": "sensors_getTemperature",
        "message0": "%{BKY_SENSORS_GETTEMPERATURE_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "UNIT",
            "options": [
                ["(°C)", "CELSIUS"],
                ["(°F)", "FAHRENHEIT"],
                ["(K)", "KELVIN"]
              ]
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETTEMPERATURE_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GET ROTATION
    {
        "type": "sensors_getRotation",
        "message0": "%{BKY_SENSORS_GETROTATION_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "AXIS",
            "options": [
                ["%{BKY_SENSORS_GETROTATION_PITCH}", "pitch"],
                ["%{BKY_SENSORS_GETROTATION_ROLL}", "roll"]
            ]
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETROTATION_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GET MAGNETIC FORCE
    {
        "type": "sensors_getMagneticForce",
        "message0": "%{BKY_SENSORS_GETMAGNETICFORCE_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "AXIS",
            "options": [
                ["x", "x"],
                ["y", "y"],
                ["z", "z"],
                ["strength", "NORM"]
            ]
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETMAGNETICFORCE_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    /* Begin gas sensors blocks*/

    // BLOCK GROVE SGP30 SENSOR _ GET GAS (I2C)
    {
        "type": "sensors_getSgp30Gas",
        "message0": "%{BKY_SENSORS_SGP30_READDATA_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "GAS",
            "options": [
                ["%{BKY_SENSORS_SGP30_CO2}", "CO2"],
                ["%{BKY_SENSORS_SGP30_TVOC}", "TVOC"]
            ]
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_SGP30_READDATA_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // GROVE MULTICHANNEL GAS SENSOR _ GET GAS (I2C)
    {
        "type": "sensors_getMultichannelGas",
        "message0": "%{BKY_SENSORS_MULTICHANNEL_GETGAS_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "GAS",
            "options": [
                ["%{BKY_GAS_CO}", "0"],
                ["%{BKY_GAS_NO2}", "1"],
                ["%{BKY_GAS_NH3}", "2"],
                ["%{BKY_GAS_C3H8}", "3"],
                ["%{BKY_GAS_C4H10}", "4"],
                ["%{BKY_GAS_CH4}", "5"],
                ["%{BKY_GAS_H2}", "6"],
                ["%{BKY_GAS_C2H5OH}", "7"]
            ]
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_MULTICHANNEL_GETGAS_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // GROVE MULTICHANNEL GAS V2 SENSOR _ READ GAS JSON
    {
        "type": "sensors_getMultichannelGasV2",
        "message0": "%{BKY_SENSORS_MULTICHANNELV2_GETGAS_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "GAS",
            "options": [
                ["%{BKY_GAS_NO2}", "NO2"],
                ["%{BKY_GAS_CO}", "CO"],
                ["%{BKY_GAS_C2H5OH}", "C2H5OH"],
                ["%{BKY_GAS_VOC}", "VOC"]   
            ]
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_MULTICHANNELV2_GETGAS_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // GROVE O2 GAS SENSOR _ GET CONCENTRATION
    {
        "type": "sensors_getO2gas",
        "message0": "%{BKY_SENSORS_O2_GAS_READDATA_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_ANALOG_READ_PINS
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_O2_GAS_READDATA_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // AIR QUALITY SENSOR _ READ AIR QUALITY VALUE
    {
        "type": "sensors_getAirQualityValue",
        "message0": "%{BKY_SENSORS_AIR_QUALITY_GETVALUE_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_ANALOG_READ_PINS
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_AIR_QUALITY_GETVALUE_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GROVE HM330X _ GET PARTICULE MATTER (I2C)
    {
        "type": "sensors_getParticulateMatter",
        "message0": "%{BKY_SENSORS_HM330X_GETPARTICULE_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "TYPE",
            "options": [
                ["%{BKY_SENSORS_HM330X_ATM_PM1}", "3"],
                ["%{BKY_SENSORS_HM330X_ATM_PM2_5}", "4"],
                ["%{BKY_SENSORS_HM330X_ATM_PM10}", "5"]
            ]
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_HM330X_GETPARTICULE_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    /* Begin climate sensors blocks*/

    // GROVE BMP280 SENSOR _ READ DATA (I2C)
    {
        "type": "sensors_getBmp280Data",
        "message0": "%{BKY_SENSORS_BMP280_READDATA_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "ADDR",
            "options": [
                ["0x76", "0x76"],
                ["0x77 (Grove)", "0x77"]
            ]
        }, {
            "type": "field_dropdown",
            "name": "DATA",
            "options": [
                ["%{BKY_SENSORS_BMP280_TEMPERATURE}", "TEMP"],
                ["%{BKY_SENSORS_BMP280_PRESSURE}", "PRESS"],
                ["%{BKY_SENSORS_BMP280_ALTITUDE}", "ALT"]
            ]
        }],
        "output": "Number",
        "inputsInline": true,
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_BMP280_READDATA_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE,
        "mutator": "sensors_temperature_mutator"
    },

    // BLOCK GROVE MOISTURE SENSOR
    {
        "type": "sensors_getGroveMoisture",
        "message0": "%{BKY_SENSORS_GETGROVEMOISTURE_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_ANALOG_READ_PINS
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETGROVEMOISTURE_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GROVE TEMPERATURE SENSOR
    {
        "type": "sensors_getGroveTemperature",
        "message0": "%{BKY_SENSORS_GETGROVETEMPERATURE_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "UNIT",
            "options": [
                ["(°C)", "CELSIUS"],
                ["(°F)", "FAHRENHEIT"],
                ["(K)", "KELVIN"]
              ]
        }, {
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_ANALOG_READ_PINS
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETGROVETEMPERATURE_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // GROVE HIGH TEMPERATURE 
    {
        "type": "sensors_getGroveHighTemperature",
        "message0": "%{BKY_SENSORS_GETGROVEHIGHTEMP_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "UNIT",
            "options": [
                ["(°C)", "CELSIUS"],
                ["(°F)", "FAHRENHEIT"],
                ["(K)", "KELVIN"]
              ]
        }, {
            "type": "field_dropdown",
            "name": "A0",
            "options": Blockly.Constants.Pins.MICROBIT_ANALOG_READ_PINS
        }, {
            "type": "field_dropdown",
            "name": "A1",
            "options": Blockly.Constants.Pins.MICROBIT_ANALOG_READ_PINS
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETGROVEHIGHTEMP_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GROVE DHT 11/22 SENSORS _ READ DATA
    {
        "type": "sensors_dhtReadData",
        "message0": "%{BKY_SENSORS_DHT_READDATA_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "DATA",
            "options": [
                ["%{BKY_SENSORS_TEMPERATURE}", "TEMP"],
                ["%{BKY_SENSORS_HUMIDITY}", "HUM"]
            ]
        }, {
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }],
        "output": "Number",
        "inputsInline": true,
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_DHT_READDATA_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE,
        "mutator": "sensors_temperature_mutator"
    },

    // GROVE TH02 SENSOR _ READ DATA (I2C)
    {
        "type": "sensors_TH02readData",
        "message0": "%{BKY_SENSORS_TH02_READDATA_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "DATA",
            "options": [
                ["%{BKY_SENSORS_TEMPERATURE}", "TEMP"],
                ["%{BKY_SENSORS_HUMIDITY}", "HUM"]
            ]
        }],
        "output": "Number",
        "inputsInline": true,
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_TH02_READDATA_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE,
        "mutator": "sensors_temperature_mutator"
    },

    // GROVE SHT31 SENSOR _ READ DATA (I2C)
    {
        "type": "sensors_SHT31readData",
        "message0": "%{BKY_SENSORS_SHT31_READDATA_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "DATA",
            "options": [
                ["%{BKY_SENSORS_TEMPERATURE}", "TEMP"],
                ["%{BKY_SENSORS_HUMIDITY}", "HUM"]
            ]
        }],
        "output": "Number",
        "inputsInline": true,
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_SHT31_READDATA_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE,
        "mutator": "sensors_temperature_mutator"
    },

    // BLOCK GROVE WATER SENSOR
    {
        "type": "sensors_getGroveWaterAmount",
        "message0": "%{BKY_SENSORS_GETGROVEWATER_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_ANALOG_READ_PINS
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETGROVEWATER_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    //  BLOCK RAIN GAUGE _ GET STATE
    {
        "type": "sensors_getRainGauge",
        "message0": "%{BKY_SENSORS_GETRAINGAUGE_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }],
        "output": "Boolean",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETRAINGAUGE_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    //  BLOCK ANEMOMETER _ GET STATE
    {
        "type": "sensors_getAnemometer",
        "message0": "%{BKY_SENSORS_GETANEMOMETER_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }],
        "output": "Boolean",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETANEMOMETER_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    /* Begin sound & light sensors blocks*/

    // BLOCK GROVE LIGHT SENSOR JSON
    {
        "type": "sensors_getGroveLight",
        "message0": "%{BKY_SENSORS_GETGROVELIGHT_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_ANALOG_READ_PINS
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETGROVELIGHT_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GROVE SI1145 SENSOR _ READ LIGHT (I2C)
    {
        "type": "sensors_getSi1145Light",
        "message0": "%{BKY_SENSORS_SI1145_GETLIGHT_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "LIGHT",
            "options": [
                ["%{BKY_SENSORS_SI1145_UV}", "UV"],
                ["%{BKY_SENSORS_SI1145_VISIBLE}", "VIS"],
                ["%{BKY_SENSORS_SI1145_IR}", "IR"],
            ]
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_SI1145_GETLIGHT_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GROVE UV SENSOR _ GET INDEX
    {
        "type": "sensors_getUVindex",
        "message0": "%{BKY_SENSORS_GETUVINDEX_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_ANALOG_READ_PINS
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETUVINDEX_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GROVE SOUND SENSOR
    {
        "type": "sensors_getGroveSound",
        "message0": "%{BKY_SENSORS_GETGROVESOUND_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_ANALOG_READ_PINS
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETGROVESOUND_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    /* Begin distance & movement sensors blocks*/

    // GROVE GROVE ULTRASONIC SENSOR _ GET DISTANCE
    {
        "type": "sensors_getGroveUltrasonicRanger",
        "message0": "%{BKY_SENSORS_GETGROVEULTRASONIC_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "DATA",
            "options": [
                ["%{BKY_SENSORS_ULTRASONIC_DISTANCE}", "DIST"],
                ["%{BKY_SENSORS_ULTRASONIC_DURATION}", "TIME"]
            ],
        }, {
            "type": "field_dropdown",
            "name": "TRIG",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }, {
            "type": "field_dropdown",
            "name": "ECHO",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETGROVEULTRASONIC_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GROVE GESTURE SENSOR (I2C) _ GET GESTURE
    {
        "type": "sensors_getGesture",
        "message0": "%{BKY_SENSORS_GETGESTURE_TITLE}",
        "output": "String",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETGESTURE_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GROVE GESTURE SENSOR (I2C) _ ON GESTURE ... DO
    {
        "type": "sensors_onGestureTypeDetected",
        "message0": "%{BKY_SENSORS_ONGESTUREDETECTED_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "GESTURE",
            "options": [
                ["%{BKY_SENSORS_GESTURE_RIGHT}", "right"],
                ["%{BKY_SENSORS_GESTURE_LEFT}", "left"],
                ["%{BKY_SENSORS_GESTURE_UP}", "up"],
                ["%{BKY_SENSORS_GESTURE_DOWN}", "down"],
                ["%{BKY_SENSORS_GESTURE_FORWARD}", "forward"],
                ["%{BKY_SENSORS_GESTURE_BACKWARD}", "backward"],
                ["%{BKY_SENSORS_GESTURE_CLOCKWISE}", "clockwise"],
                ["%{BKY_SENSORS_GESTURE_ANTICLOCKWISE}", "anticlockwise"],
                ["%{BKY_SENSORS_GESTURE_WAVE}", "wave"]
            ]
        }],
        "message1": "%1",
        "args1": [{
            "type": "input_statement",
            "name": "DO"
        }],
        "previousStatement": null,
        "nextStatement": null,
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_ONGESTUREDETECTED_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GROVE LINE FINDER _ GET STATE
    {
        "type": "sensors_getGroveLineFinder",
        "message0": "%{BKY_SENSORS_GETGROVELINEFINDER_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }],
        "output": "Boolean",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETGROVELINEFINDER_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GROVE TILT MODULE _ GET STATE
    {
        "type": "sensors_getGroveTilt",
        "message0": "%{BKY_SENSORS_GETGROVETILT_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }],
        "output": "Boolean",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETGROVETILT_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GROVE PIR MOTION SENSOR _ GET STATE
    {
        "type": "sensors_getGroveMotion",
        "message0": "%{BKY_SENSORS_GETGROVEMOTION_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }],
        "output": "Boolean",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETGROVEMOTION_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GROVE PIEZO VIBRATION SENSOR _ GET STATE
    {
        "type": "sensors_getPiezoVibration",
        "message0": "%{BKY_SENSORS_GETPIEZOVIBRATION_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }],
        "output": "Boolean",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETPIEZOVIBRATION_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },

    // BLOCK GROVE SIMPLE BUTTON _ READ DIGITAL 
    {
        "type": "sensors_getGroveButton",
        "message0": "%{BKY_SENSORS_GETGROVEBUTTON_TITLE}",
        "args0": [{
            "type": "field_dropdown",
            "name": "TYPE",
            "options": [
                ["%{BKY_SENSORS_GETGROVEBUTTON_VOLTAGE}", "VOLT"],
                ["%{BKY_SENSORS_GETGROVEBUTTON_STATE}", "STATE"],
            ]
        }, {
            "type": "field_dropdown",
            "name": "PIN",
            "options": Blockly.Constants.Pins.MICROBIT_PINS
        }],
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETGROVEBUTTON_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    },
    {
        "type": "sensors_getCompassSignal",
        "message0": "%{BKY_SENSORS_GETCOMPASS_SIGNAL_TITLE}",
        "output": "Number",
        "style": "sensors_blocks",
        "tooltip": "%{BKY_SENSORS_GETCOMPASS_SIGNAL_TOOLTIP}",
        "helpUrl": VITTASCIENCE_SITE
    }

]); // END JSON EXTRACT (Do not delete this comment.)

Blockly.Constants.Sensors = Object.create(null);

/**
 * Performs final setup of temperature blocks.
 * @this {Blockly.Block}
 */
Blockly.Constants.Sensors.SENSORS_TEMPERATURE_INIT_EXTENSION = function() {
    this.TEMPERATURE_UNIT = [
      ["(°C)", 'CELSIUS'],
      ["(°F)", 'FAHRENHEIT'],
      ["(K)", 'KELVIN']
    ];
    var dropdown = this.getField("DATA");
    dropdown.setValidator(function(value) {
      var newTemp = (value == "TEMP");
      if (newTemp != this.isTemp_) {
        var block = this.getSourceBlock();
        block.updateField_(newTemp);
      }
    });
    var isTempInital = (this.getFieldValue("DATA") == "TEMP");
    this.updateField_(isTempInital);
};

/**
 * Mixin for mutator functions in the 'sensors_temperature_mutator' extension.
 * @mixin
 * @augments Blockly.Block
 * @package
 */
Blockly.Constants.Sensors.SENSORS_TEMPERATURE_MUTATOR_MIXIN = {
  /**
   * Create XML to represent whether there is an 'temp' dropdown field.
   * @return {!Element} XML storage element.
   * @this {Blockly.Block}
   */
  mutationToDom: function() {
    var container = Blockly.utils.xml.createElement('mutation');
    container.setAttribute('temp', !!this.isTemp_);
    return container;
  },
  /**
   * Parse XML to restore the 'temp' dropdown field.
   * @param {!Element} xmlElement XML storage element.
   * @this {Blockly.Block}
   */
  domToMutation: function(xmlElement) {
    var isTemp = (xmlElement.getAttribute('temp') != 'false');
    this.updateField_(isTemp);
  },
  /**
   * Create or delete temperature unit field_dropdown.
   * @param {boolean} isTemp True if the dropdown should exist.
   * @private
   * @this {Blockly.Block}
   */
  updateField_: function(isTemp) {
    // Destroy old 'UNIT' field.
    if (this.getInput("TEMP_UNIT")) {
        this.removeInput("TEMP_UNIT");
    }
    // Create either a value 'TEMP' dropdown field.
    if (isTemp) {
      this.appendDummyInput("TEMP_UNIT")
          .appendField(Blockly.Msg["SENSORS_TEMPERATURE_IN"])
          .appendField(new Blockly.FieldDropdown(this.TEMPERATURE_UNIT), "UNIT");
    /**
     * WAITING GOOGLE ISSUE (dropdown colour)
     */
    this.setStyle("sensors_blocks");
    /**
     * END
     */
    }
    this.isTemp_ = isTemp;
  }
};

Blockly.Extensions.registerMutator('sensors_temperature_mutator',
  Blockly.Constants.Sensors.SENSORS_TEMPERATURE_MUTATOR_MIXIN,
  Blockly.Constants.Sensors.SENSORS_TEMPERATURE_INIT_EXTENSION);
