Blockly.GalaxiaMicropython.maqueen_init = function(){
    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);

    Blockly.GalaxiaMicropython.userSetups_["%maqueen_i2c"] = 'i2c = machine.I2C(1, scl=machine.Pin(machine.Pin.P19), sda=machine.Pin(machine.Pin.P20))'

};

Blockly.GalaxiaMicropython.robots_galaxia_setMaqueenGo = function(block){
    Blockly.GalaxiaMicropython.maqueen_init()

    var speed = Blockly.GalaxiaMicropython.valueToCode(block, "SPEED", Blockly.GalaxiaMicropython.ORDER_NONE) || "0";
    var dir = block.getFieldValue("DIR");
    if (speed > 255) speed = 255;
    if (speed < 0) speed = 0;
    return ""+
    "try:" + NEWLINE
    + Blockly.GalaxiaMicropython.INDENT + "i2c.writeto(0x10, bytes([0x00, " + dir + ", " + speed + "]))" + NEWLINE
    + Blockly.GalaxiaMicropython.INDENT + "i2c.writeto(0x10, bytes([0x02, " + dir + ", " + speed + "]))" + NEWLINE
    + "except:" + NEWLINE
    + Blockly.GalaxiaMicropython.INDENT + "print('Aucun Maqueen trouvé')"+ NEWLINE

};

Blockly.GalaxiaMicropython.robots_galaxia_controlMaqueenMotor = function (block) {
    Blockly.GalaxiaMicropython.maqueen_init()
    
    var speed = Blockly.GalaxiaMicropython.valueToCode(block, "SPEED", Blockly.GalaxiaMicropython.ORDER_NONE) || "0";
    if (speed > 255) speed = 255;
    if (speed < 0) speed = 0;
    return ""+
    "try:" + NEWLINE
    + Blockly.GalaxiaMicropython.INDENT + "i2c.writeto(0x10, bytes([" + block.getFieldValue("MOTOR") + ", " + block.getFieldValue("DIR") + ", " + speed + "]))" + NEWLINE 
    + "except:" + NEWLINE
    + Blockly.GalaxiaMicropython.INDENT + "print('Aucun Maqueen trouvé')"+ NEWLINE
};

Blockly.GalaxiaMicropython.robots_galaxia_stopMaqueenMotors = function (block) {
    Blockly.GalaxiaMicropython.maqueen_init()
    
    if (block.getFieldValue("MOTOR") == "both") {
        return ""+
        "try:" + NEWLINE
        + Blockly.GalaxiaMicropython.INDENT + "i2c.writeto(0x10, bytes([0x00, 0, 0]))" + NEWLINE 
        + Blockly.GalaxiaMicropython.INDENT + "i2c.writeto(0x10, bytes([0x02, 0, 0]))" + NEWLINE 
        + "except:" + NEWLINE
        + Blockly.GalaxiaMicropython.INDENT + "print('Aucun Maqueen trouvé')"+ NEWLINE
    } else {
        return ""
        +"try:" + NEWLINE
        + Blockly.GalaxiaMicropython.INDENT + "i2c.writeto(0x10, bytes([" + block.getFieldValue("MOTOR") + ", 0, 0]))" + NEWLINE
        + "except:" + NEWLINE
        + Blockly.GalaxiaMicropython.INDENT + "print('Aucun Maqueen trouvé')"+ NEWLINE
    }
};

Blockly.GalaxiaMicropython.robots_galaxia_readMaqueenPatrol = function (block) {
    Blockly.GalaxiaMicropython.io_digital_read()
    let pin = block.getFieldValue("PIN");

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);

    let varName = "pin"+pin.substring(1);
    Blockly.GalaxiaMicropython.userSetups_["io_pin_"+pin] = varName+" = []";
    
    return ["digital_read("+varName+", machine.Pin."+pin+")", Blockly.GalaxiaMicropython.ORDER_ATOMIC]
};

Blockly.GalaxiaMicropython.robots_galaxia_readMaqueenPlusPatrol = function (block) {
    Blockly.GalaxiaMicropython.maqueen_init()

    Blockly.GalaxiaMicropython.userFunctions_["%thingz_maqueen_plus_patrol" ] = "def read_patrol(sensor):"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+"try:"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"status = bytearray(1)"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"i2c.writeto(0x10, bytes([0x1D]))"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"i2c.readfrom_into(0x10, status)"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"if sensor == \"l1\":"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"return (status[0] & 0x8) == 0X8"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"elif sensor == \"l2\":"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"return (status[0] & 0x10) == 0X10"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"elif sensor == \"r1\":"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"return (status[0] & 0x2) == 0X2"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"elif sensor == \"r2\":"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"return (status[0] & 0x1) == 0X1"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"elif sensor == \"m\":"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"return (status[0] & 0x4) == 0X4 "+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"else:"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"return False"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+"except:"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"print('Aucun Maqueen trouvé')"+NEWLINE
    +Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"return False"+NEWLINE
    
    let pin = block.getFieldValue("PIN");

    return ["read_patrol(\""+pin+"\")", Blockly.GalaxiaMicropython.ORDER_ATOMIC]
};

Blockly.GalaxiaMicropython.robots_galaxia_readMaqueenUltrasound = function(block){
    let unit = block.getFieldValue("UNIT");
    Blockly.GalaxiaMicropython.maqueen_init()
    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);
    Blockly.GalaxiaMicropython.addImport("import_time", IMPORT_TIME);

    Blockly.GalaxiaMicropython.userSetups_["io_pin_1"] = "pin1 = machine.Pin(machine.Pin.P1, mode=machine.Pin.OUT)"+NEWLINE
    Blockly.GalaxiaMicropython.userSetups_["io_pin_2"] = "pin2 = machine.Pin(machine.Pin.P2, mode=machine.Pin.IN)"+NEWLINE

    Blockly.GalaxiaMicropython.userFunctions_["%thingz_maqueen_ultrasound" ] = "def ultrasound(trigger, echo):"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+"trigger.value(1)"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+"time.sleep(0.00001)"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+"trigger.value(0)"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+"pulse_time = machine.time_pulse_us(echo, 1)"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+"if pulse_time < 0:"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"print(\"Distance: pas de r\\u00e9ponse\")"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"return 0"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+"distance = round(pulse_time * "+unit+" "+(unit == "0.017" ? ", 1)" : ")")+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+"return distance"+NEWLINE

    return ["ultrasound(pin1, pin2)", Blockly.GalaxiaMicropython.ORDER_ATOMIC]

}

Blockly.GalaxiaMicropython.robots_galaxia_readMaqueenPlusUltrasound = function(block){
    let unit = block.getFieldValue("UNIT");
    Blockly.GalaxiaMicropython.maqueen_init()
    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);
    Blockly.GalaxiaMicropython.addImport("import_time", IMPORT_TIME);

    Blockly.GalaxiaMicropython.userSetups_["io_pin_13"] = "pin13 = machine.Pin(machine.Pin.P13, mode=machine.Pin.OUT)";
    Blockly.GalaxiaMicropython.userSetups_["io_pin_14"] = "pin14 = machine.Pin(machine.Pin.P14, mode=machine.Pin.IN)";

    Blockly.GalaxiaMicropython.userFunctions_["%thingz_maqueen_ultrasound" ] = "def ultrasound(trigger, echo):"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+"trigger.value(1)"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+"time.sleep(0.00001)"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+"trigger.value(0)"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+"pulse_time = machine.time_pulse_us(echo, 1)"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+"if pulse_time < 0:"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"print(\"Distance: pas de r\\u00e9ponse\")"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"return 0"+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+"distance = round(pulse_time * "+unit+" "+(unit == "0.017" ? ", 1)" : ")")+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+"return distance"+NEWLINE

    return ["ultrasound(pin13, pin14)", Blockly.GalaxiaMicropython.ORDER_ATOMIC]

}