/**
 * @fileoverview Maqueen generators for Galaxia.
 */

Blockly.GalaxiaMicropython.codo_init = function(){
    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);;

    Blockly.GalaxiaMicropython.userSetups_["%codo_init"] = 'codo = []'+NEWLINE+
    'codo.append(machine.PWM(machine.Pin(machine.Pin.P15), freq=5000))'+NEWLINE+
    'codo.append(machine.PWM(machine.Pin(machine.Pin.P16), freq=5000))'+NEWLINE+
    'codo.append(machine.PWM(machine.Pin(machine.Pin.P13), freq=5000))'+NEWLINE+
    'codo.append(machine.PWM(machine.Pin(machine.Pin.P14), freq=5000))'+NEWLINE+NEWLINE+
    'def codo_set_motor(motor, direction, speed):'+NEWLINE+Blockly.GalaxiaMicropython.INDENT+
        'global codo'+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+'index = 0'+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+'if speed < 0:'+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+'speed = 0'+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+'elif speed > 100:'+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+'speed = 100'+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+'if motor == "left":'+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+'index = 2'+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+'if direction == "forward":'+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+'codo[index].duty_u16(0)'+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+'codo[index+1].duty_u16(int(65535*(speed/100)))'+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+'else:'+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+'codo[index].duty_u16(int(65535*(speed/100)))'+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+'codo[index+1].duty_u16(0)'+NEWLINE+
            
    'def codo_move(direction, speed):'+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+'codo_set_motor("left", direction, speed)'+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+'codo_set_motor("right", direction, speed)'+NEWLINE

};

Blockly.GalaxiaMicropython.robots_galaxia_setCodoGo = function(block){
    Blockly.GalaxiaMicropython.codo_init()

    var speed = Blockly.GalaxiaMicropython.valueToCode(block, "SPEED", Blockly.GalaxiaMicropython.ORDER_NONE) || "0";
    var dir = block.getFieldValue("DIR");
    
    return 'codo_move("'+dir+'", '+speed+')'+NEWLINE
};

Blockly.GalaxiaMicropython.robots_galaxia_controlCodoMotor = function (block) {
    Blockly.GalaxiaMicropython.codo_init()
    
    var motor = block.getFieldValue("MOTOR");
    var speed = Blockly.GalaxiaMicropython.valueToCode(block, "SPEED", Blockly.GalaxiaMicropython.ORDER_NONE) || "0";
    var dir = block.getFieldValue("DIR");
    
    return 'codo_set_motor("'+motor+'","'+dir+'", '+speed+')'+NEWLINE
};

Blockly.GalaxiaMicropython.robots_galaxia_stopCodoMotors = function (block) {
    Blockly.GalaxiaMicropython.codo_init()
    
    if (block.getFieldValue("MOTOR") == "both") {
        return 'codo_set_motor("left", "forward", 0)'+NEWLINE+'codo_set_motor("right", "forward", 0)'+ NEWLINE
    } else {
        return 'codo_set_motor('+block.getFieldValue("MOTOR")+', "forward", 0)' + NEWLINE;
    }
};