Blockly.GalaxiaMicropython.data_galaxia_set_columns = function(block){
    Blockly.GalaxiaMicropython.addImport("import_galaxia_log", IMPORT_GALAXIA_LOG);
    let i = 0;
    let col = this.valueToCode(block, "ADD"+i, this.ORDER_NONE) 
    let columns = []
    while(col !== ""){
        columns[columns.length] = 'str('+col+')'
        i++;
        col = this.valueToCode(block, "ADD"+i, this.ORDER_NONE) 
    }
    return "log.set_columns(["+columns.join()+"])"+ NEWLINE;
};

Blockly.GalaxiaMicropython.data_galaxia_add = function(block){
    Blockly.GalaxiaMicropython.addImport("import_galaxia_log", IMPORT_GALAXIA_LOG);
    let i = 0;
    let col = this.valueToCode(block, "ADD_KEY"+i, this.ORDER_NONE) 
    let val = this.valueToCode(block, "ADD_VALUE"+i, this.ORDER_NONE) 
    let columns = []
    while(col !== ""){
        columns[columns.length] = '(str('+col+'), '+val+')'
        i++;
        col = this.valueToCode(block, "ADD_KEY"+i, this.ORDER_NONE) 
        val = this.valueToCode(block, "ADD_VALUE"+i, this.ORDER_NONE) 
    }
    return "log.add(["+columns.join()+"])"+ NEWLINE;
};

Blockly.GalaxiaMicropython.data_galaxia_add_v2 = function(block){
    Blockly.GalaxiaMicropython.addImport("import_galaxia_log", IMPORT_GALAXIA_LOG);
    let i = 0;
    let data = this.valueToCode(block, "DATA"+i, this.ORDER_NONE) 
    let columns = []
    while(data !== ""){
        columns[columns.length] = data
        i++;
        data = this.valueToCode(block, "DATA"+i, this.ORDER_NONE) 
    }
    return "log.add(["+columns.join()+"])"+ NEWLINE;
};

Blockly.GalaxiaMicropython.data_galaxia_delete = function (block) {
    Blockly.GalaxiaMicropython.addImport("import_galaxia_log", IMPORT_GALAXIA_LOG);
    return "log.delete()" + NEWLINE
};


Blockly.GalaxiaMicropython.data_galaxia_column_value = function(block){
    Blockly.GalaxiaMicropython.addImport("import_galaxia_log", IMPORT_GALAXIA_LOG);

    let col = this.valueToCode(block, "COLUMN", this.ORDER_NONE) 
    let val = this.valueToCode(block, "VALUE", this.ORDER_NONE) 

    return ['(str('+col+'), '+val+')', Blockly.GalaxiaMicropython.ORDER_ATOMIC]
}