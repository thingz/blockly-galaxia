Blockly.GalaxiaMicropython.time_galaxia_timer_start = function (block) {
    this.addImport("time", IMPORT_TIME);
    this.addConstant("galaxia_chronometer", GALAXIA_MICROPYTHON_CHRONOMETER);
    
    return "galaxiaChronometer = time.ticks_us()" + NEWLINE;
};

Blockly.GalaxiaMicropython.time_galaxia_timer_measure = function (block) {
    this.addImport("time", IMPORT_TIME);
    this.addConstant("galaxia_chronometer", GALAXIA_MICROPYTHON_CHRONOMETER);
    
    var divider = block.getFieldValue("UNIT");
    return ["int((time.ticks_us() - galaxiaChronometer)/"+"("+divider+"/1000))", Blockly.Python.ORDER_ATOMIC];
};