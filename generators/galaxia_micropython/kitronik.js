/**
 * @fileoverview Maqueen generators for GalaxiaMicropython.
 */

// Blockly.GalaxiaGenerator.prototype.maqueen_init = function(){
//     return "print('/!\\ Maqueen non disponible sur ce firmware')" + NEWLINE

// };

Blockly.GalaxiaMicropython.kitronik_access_move = function(block){
    let dir = block.getFieldValue("DIR");
    let pin = "P0"
    let pwm ="pwm"+pin


    Blockly.GalaxiaMicropython.userSetups_[pwm] = pwm+" = machine.PWM(machine.Pin(machine.Pin."+pin+"), freq=50)"

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);

    Blockly.GalaxiaMicropython.codeFunctions_["grove_set_angle"] = 'def set_angle(a, pwm):\r\n'+
	Blockly.GalaxiaMicropython.INDENT+'duty = int(0.025*(2**16 -1) + (a*0.1*(2**16 -1))/180)\r\n'+
    Blockly.GalaxiaMicropython.INDENT+'pwm.duty_u16(duty)\r\n'
    
    return 'set_angle('+dir+', '+pwm+')' + NEWLINE;
};

Blockly.GalaxiaMicropython.kitronik_access_sound = function (block) {
    let duration = block.getFieldValue("DURATION");
    let repeat = Blockly.GalaxiaMicropython.valueToCode(block, "TIMES", Blockly.GalaxiaMicropython.ORDER_NONE);

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);
    Blockly.GalaxiaMicropython.addImport("import_time", IMPORT_TIME);

    Blockly.GalaxiaMicropython.codeFunctions_["kitronik_buzz"] = 'def kitronik_buzzer(pin, duration, repeat):\r\n'+
    Blockly.GalaxiaMicropython.INDENT+'p = machine.PWM(machine.Pin(pin), freq=440)\r\n'+
    Blockly.GalaxiaMicropython.INDENT+'for i in range(repeat):\r\n'+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+'pwm.duty_u16(32768)\r\n'+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+'time.sleep(duration)\r\n'+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+'pwm.duty_u16(0)\r\n'+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+'if i < repeat -1:\r\n'+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+'time.sleep(duration)\r\n'+
    Blockly.GalaxiaMicropython.INDENT+"p.deinit()"

    return "kitronik_buzzer(machine.Pin.P1, "+duration+", "+repeat+")\r\n"
    
};

Blockly.GalaxiaMicropython.kitronik_lamp_deadband = function (block) {
    let dead  = 0
    if(Blockly.GalaxiaMicropython.userSetups_["kitronik_lamp_deadband"]){
        dead = Blockly.GalaxiaMicropython.userSetups_["kitronik_lamp_deadband"].split(" = ")[1]
    }
    return [""+dead, Blockly.Python.ORDER_ATOMIC]

};

Blockly.GalaxiaMicropython.kitronik_lamp_read_light_level = function (block) {
    Blockly.GalaxiaMicropython.io_analog_read()
    let pin = "P1";

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);
    Blockly.GalaxiaMicropython.addImport("import_math", IMPORT_MATH);

    let varName = "pin"+pin.substring(1);
    Blockly.GalaxiaMicropython.userSetups_["io_pin_"+pin] = varName+" = []";
    let code = "math.trunc((analog_read("+varName+", machine.Pin."+pin+")/65535)*1023)"
    return [code, Blockly.Python.ORDER_ATOMIC]
};

Blockly.GalaxiaMicropython.kitronik_lamp_set_light = function (block) {
    Blockly.GalaxiaMicropython.io_digital_write()
    let state = block.getFieldValue("STATE");

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);

    let varName = "pin0";
    Blockly.GalaxiaMicropython.userSetups_["io_pin_P0"] = varName+" = []";

    return "digital_write("+varName+", machine.Pin.P0, "+state+")\r\n"
};

Blockly.GalaxiaMicropython.kitronik_lamp_set_lamp_deadband = function(block){
    let dead = Blockly.GalaxiaMicropython.valueToCode(block, "PERCENT", Blockly.GalaxiaMicropython.ORDER_NONE);
    if( dead < 0 ){
        dead = 0;
    }else if (dead > 100){
        dead = 100
    }
    Blockly.GalaxiaMicropython.userSetups_["kitronik_lamp_deadband"] = "kitronic_dead = "+dead
    return ""
}

Blockly.GalaxiaMicropython.kitronik_stop_set_traffic_light = function(block){
    let state = block.getFieldValue("STATE");
    let colors = {
        green: "False",
        orange: "False",
        red: "False"
    }
    switch(state){
        case "stop":
            colors["red"] = "True"
        break;
        case "get_ready":
            colors["red"] = "True"
            colors["orange"] = "True"
        break;
        case "go":
            colors["green"] = "True"
        break;
        case "ready_to_stop":
            colors["orange"] = "True"
        break;
    }
    Blockly.GalaxiaMicropython.io_digital_write()

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);


    let red = "pin0";
    let orange = "pin1";
    let green = "pin2";
    Blockly.GalaxiaMicropython.userSetups_["io_pin_"+red] = red+" = []";
    Blockly.GalaxiaMicropython.userSetups_["io_pin_"+orange] = orange+" = []";
    Blockly.GalaxiaMicropython.userSetups_["io_pin_"+green] = green+" = []";

    let code = "digital_write("+red+", machine.Pin.P0, "+colors["red"]+")"+ NEWLINE
    + "digital_write("+orange+", machine.Pin.P1, "+colors["orange"]+")"+ NEWLINE
    + "digital_write("+green+", machine.Pin.P2, "+colors["green"]+")"+ NEWLINE

    return code
}

Blockly.GalaxiaMicropython.kitronik_stop_set_color = function (block) {
    let state = block.getFieldValue("STATE");
    let color = block.getFieldValue("COLOR");

    Blockly.GalaxiaMicropython.io_digital_write()

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);

    let varName = "pin"+color;
    Blockly.GalaxiaMicropython.userSetups_["io_pin_P"+color] = varName+" = []";

    return "digital_write("+varName+", machine.Pin.P"+color+", "+state+")\r\n"

};
