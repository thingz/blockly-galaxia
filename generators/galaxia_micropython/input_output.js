Blockly.GalaxiaMicropython.io_galaxia_timestamp = function (block) {
    this.addImport("time", IMPORT_TIME);
    var duration = this.valueToCode(block, "TIME", this.ORDER_NONE) || "0";
    return ["time.ticks_ms()/"+ block.getFieldValue("UNIT"), Blockly.Galaxia.ORDER_ATOMIC];
};

Blockly.GalaxiaMicropython.io_configure_pin = function(){
    Blockly.GalaxiaMicropython.codeFunctions_["io_configure_pin"] = "def configure_pin(pin, pinNumber, type, frequency=50):\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"if type == \"digital_write\":\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"if len(pin) and isinstance(pin[0], machine.Pin):\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"pin[0].init(mode=machine.Pin.OUT)\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"else:\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"if len(pin):\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"pin[0] = machine.Pin(pinNumber, mode=machine.Pin.OUT)\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"else:\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"pin.append(machine.Pin(pinNumber, mode=machine.Pin.OUT))\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"elif type == \"digital_read\":\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"if len(pin) and isinstance(pin[0], machine.Pin):\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"pin[0].init(mode=machine.Pin.IN)\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"else:\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"if len(pin):\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"pin[0] = machine.Pin(pinNumber, mode=machine.Pin.IN)\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"else:\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"pin.append(machine.Pin(pinNumber, mode=machine.Pin.IN))\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"elif type == \"analog_read\":\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"if len(pin) == 0 or not isinstance(pin[0], machine.ADC):\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"if len(pin):\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"pin[0] = machine.ADC(machine.Pin(pinNumber, mode=machine.Pin.IN), atten=machine.ADC.ATTN_11DB)\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"else:\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"pin.append(machine.ADC(machine.Pin(pinNumber, mode=machine.Pin.IN), atten=machine.ADC.ATTN_11DB))\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"elif type == \"pwm\":\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"if len(pin) == 0 or not isinstance(pin[0], machine.PWM) or pin[0].freq() != frequency:\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"if len(pin):\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"pin[0] = machine.PWM(machine.Pin(pinNumber), freq=frequency)\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"else:\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"pin.append(machine.PWM(machine.Pin(pinNumber), freq=frequency))\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"return pin\r\n"

}

Blockly.GalaxiaMicropython.io_analog_read = function(){
    Blockly.GalaxiaMicropython.io_configure_pin()
    Blockly.GalaxiaMicropython.codeFunctions_["io_analog_read"] = "def analog_read(pin, pinNumber):\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"pin = configure_pin(pin, pinNumber, \"analog_read\")\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"return pin[0].read_u16()\r\n"
}

Blockly.GalaxiaMicropython.io_digital_read = function(){
    Blockly.GalaxiaMicropython.io_configure_pin()
    Blockly.GalaxiaMicropython.codeFunctions_["io_digital_read"] = "def digital_read(pin, pinNumber):\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"pin = configure_pin(pin, pinNumber, \"digital_read\")\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"return pin[0].value()\r\n"
}

Blockly.GalaxiaMicropython.io_digital_write = function(){
    Blockly.GalaxiaMicropython.io_configure_pin()
    Blockly.GalaxiaMicropython.codeFunctions_["io_digital_write"] = "def digital_write(pin, pinNumber, state):\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"pin = configure_pin(pin, pinNumber, \"digital_write\")\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"pin[0].value(state)\r\n"
}

Blockly.GalaxiaMicropython.io_pwm = function(){
    Blockly.GalaxiaMicropython.io_configure_pin()
    Blockly.GalaxiaMicropython.codeFunctions_["io_pwm"] = "def pwm(pin, pinNumber, duty, frequency):\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"pin = configure_pin(pin, pinNumber, \"pwm\", frequency=frequency)\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"pin[0].duty_u16(duty)\r\n"
}

Blockly.GalaxiaMicropython.io_galaxia_readDigitalPin = function(block){
    Blockly.GalaxiaMicropython.io_digital_read()
    let pin = block.getFieldValue("PIN");

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);


    let varName = "pin"+pin.substring(1);
    Blockly.GalaxiaMicropython.userSetups_["io_pin_"+pin] = varName+" = []";
    
    return ["digital_read("+varName+", machine.Pin."+pin+")", Blockly.GalaxiaMicropython.ORDER_ATOMIC]
}

Blockly.GalaxiaMicropython.io_galaxia_writeDigitalPin = function(block){
    Blockly.GalaxiaMicropython.io_digital_write()
    let pin = block.getFieldValue("PIN");
    let state = Blockly.GalaxiaMicropython.valueToCode(block, "STATE", Blockly.GalaxiaMicropython.ORDER_NONE);

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);

    let varName = "pin"+pin.substring(1);
    Blockly.GalaxiaMicropython.userSetups_["io_pin_"+pin] = varName+" = []";

    return "digital_write("+varName+", machine.Pin."+pin+", "+state+")\r\n"
}

Blockly.GalaxiaMicropython.io_galaxia_readAnalogPin = function(block){
    Blockly.GalaxiaMicropython.io_analog_read()
    let pin = block.getFieldValue("PIN");

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);

    let varName = "pin"+pin.substring(1);
    Blockly.GalaxiaMicropython.userSetups_["io_pin_"+pin] = varName+" = []";
    
    return ["analog_read("+varName+", machine.Pin."+pin+")", Blockly.GalaxiaMicropython.ORDER_ATOMIC]
}

Blockly.GalaxiaMicropython.io_galaxia_pwm = function(block){
    Blockly.GalaxiaMicropython.io_pwm()
    let pin = block.getFieldValue("PIN");
    let duty = Blockly.GalaxiaMicropython.valueToCode(block, "DUTY", Blockly.GalaxiaMicropython.ORDER_NONE);

    duty = Math.trunc(65535 *(duty/100))

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);

    let varName = "pin"+pin.substring(1);
    Blockly.GalaxiaMicropython.userSetups_["io_pin_"+pin] = varName+" = []";

    return "pwm("+varName+", machine.Pin."+pin+", "+duty+", 50)\r\n"
}
Blockly.GalaxiaMicropython.io_galaxia_pwm_with_freq = function(block){
    Blockly.GalaxiaMicropython.io_pwm()
    let pin = block.getFieldValue("PIN");
    let duty = Blockly.GalaxiaMicropython.valueToCode(block, "DUTY", Blockly.Galaxia.ORDER_NONE);
    let freq = Blockly.GalaxiaMicropython.valueToCode(block, "FREQ", Blockly.Galaxia.ORDER_NONE);

    duty = Math.trunc(65535 *(duty/100))

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);

    let varName = "pin"+pin.substring(1);
    Blockly.GalaxiaMicropython.userSetups_["io_pin_"+pin] = varName+" = []";

    return "pwm("+varName+", machine.Pin."+pin+", "+duty+", "+freq+")\r\n"
}