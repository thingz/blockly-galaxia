Blockly.GalaxiaMicropython.grove_galaxia_temperature = function (block) {
    Blockly.GalaxiaMicropython.io_analog_read()
    let pin = block.getFieldValue("NUMBER");

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);
    Blockly.GalaxiaMicropython.addImport('math', IMPORT_MATH);

    let varName = "pin"+pin.substring(1);
    Blockly.GalaxiaMicropython.userSetups_["io_pin_"+pin] = varName+" = []";
    
    //Adjust temp by changing 273.15 to 263.15
    return ["round(1/(math.log((65535*5/3.3/analog_read("+varName+", machine.Pin."+pin+")- 1))/4275+1/298.15)-263.15,1)", Blockly.GalaxiaMicropython.ORDER_ATOMIC]
};

Blockly.GalaxiaMicropython.grove_galaxia_ultrasonic = function (block){
    // Blockly.Galaxia.io_digital_read()
    // Blockly.Galaxia.io_digital_write()
    let pin = block.getFieldValue("NUMBER");
    let unit = block.getFieldValue("UNIT");

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);
    Blockly.GalaxiaMicropython.addImport("import_time", IMPORT_TIME);

    Blockly.GalaxiaMicropython.codeFunctions_["grove_ultrasonic"] = "def ultrasonic(pin, pinNumber):\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"trigger = machine.Pin(pinNumber, mode=machine.Pin.OUT, pull=None)\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"trigger.value(0)\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"time.sleep_us(5)\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"trigger.value(1)\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"time.sleep_us(10)\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"trigger.value(0)\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"trigger.init(mode=machine.Pin.IN)\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"pulse_time = machine.time_pulse_us(trigger, 1)\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"if pulse_time < 0:\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"print(\"Distance: pas de r\\u00e9ponse\")\r\n"+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+"return 0\r\n"+
    Blockly.GalaxiaMicropython.INDENT+"distance = round(pulse_time * "+unit+" "+(unit == "0.017" ? ", 1)" : ")")+NEWLINE+
    Blockly.GalaxiaMicropython.INDENT+"return distance\r\n"
    
    

    let varName = "pin"+pin.substring(1);
    
    Blockly.GalaxiaMicropython.userSetups_["ultrasonic_def"+pin] = varName+" = []";

    return ["ultrasonic("+varName+", machine.Pin."+pin+")", Blockly.GalaxiaMicropython.ORDER_ATOMIC]    
}


Blockly.GalaxiaMicropython.grove_galaxia_dht11 = function (block){

    let pin = block.getFieldValue("PIN");
    let value = block.getFieldValue("VALUE");

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);

    Blockly.GalaxiaMicropython.codeFunctions_["grove_dht11"] = 'def dht_measure(dht, temp):\r\n'+
    Blockly.GalaxiaMicropython.INDENT+'dht.measure()\r\n'+
    Blockly.GalaxiaMicropython.INDENT+'if temp:\r\n'+
    Blockly.GalaxiaMicropython.INDENT+Blockly.GalaxiaMicropython.INDENT+'return dht.temperature()\r\n'+
    Blockly.GalaxiaMicropython.INDENT+"return dht.humidity()"

    let varName = "dht11_"+pin.substring(1);
    
    Blockly.GalaxiaMicropython.userSetups_["dht11_def"+pin] = varName+" = dht.DHT11(machine.Pin."+pin+")";
    
    return ["dht_measure(varName, "+value == "temperature" ? "True" : "False"+")", Blockly.GalaxiaMicropython.ORDER_ATOMIC]    
}

Blockly.GalaxiaMicropython.grove_galaxia_dht11_signal = function(block){
    let value = block.getFieldValue("VALUE");
    if(value == "temperature")
        return Blockly.GalaxiaMicropython.map(Blockly.GalaxiaMicropython.grove_galaxia_dht11(block), -20, 80, 0, 3.3)    
    return Blockly.GalaxiaMicropython.map(Blockly.GalaxiaMicropython.grove_galaxia_dht11(block), 0, 100, 0, 3.3)  
}

Blockly.GalaxiaMicropython.grove_galaxia_temperature_signal = function(block){
    return Blockly.GalaxiaMicropython.map(Blockly.GalaxiaMicropython.grove_galaxia_temperature(block), -20, 80, 0, 3.3)    
}

Blockly.GalaxiaMicropython.grove_galaxia_servo = function(block){
    let angle = Blockly.GalaxiaMicropython.valueToCode(block, "ANGLE", Blockly.GalaxiaMicropython.ORDER_NONE) || 90;
    let pin = block.getFieldValue("PIN");

    let pwm ="pwm"+pin

    if(angle < 0 ){
        angle = 0
    }else if(angle > 180){
        angle = 180
    }

    Blockly.GalaxiaMicropython.userSetups_[pwm] = pwm+" = machine.PWM(machine.Pin(machine.Pin."+pin+"), freq=50)"

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);

    Blockly.GalaxiaMicropython.codeFunctions_["grove_set_angle"] = 'def set_angle(a, pwm):\r\n'+
	Blockly.GalaxiaMicropython.INDENT+'duty = int(0.025*(2**16 -1) + (a*0.1*(2**16 -1))/180)\r\n'+
    Blockly.GalaxiaMicropython.INDENT+'pwm.duty_u16(duty)\r\n'
    
    return 'set_angle('+angle+', '+pwm+')' + NEWLINE;
}

Blockly.GalaxiaMicropython.grove_galaxia_ir_sender = function(block){
    let enable = block.getFieldValue("ENABLE");
    let pin = block.getFieldValue("PIN");

    let pwm ="pwm"+pin

    Blockly.GalaxiaMicropython.userSetups_[pwm] = pwm+" = machine.PWM(machine.Pin(machine.Pin."+pin+"), freq=50)"

    Blockly.GalaxiaMicropython.addImport("import_machine", IMPORT_MACHINE);
    
    return pwm+'.duty_u16(' +enable +")"+ NEWLINE;
}