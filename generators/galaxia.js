// This file tells what each block should generate
// in terms of Galaxia language.

/**
 * Galaxia code generator.
 * @type {!Blockly.Generator}
 */
Blockly.GalaxiaGenerator = function(name){
    Blockly.Generator.call(this, name)

    /**
     * List of illegal variable names.
     * This is not intended to be a security feature.  Blockly is 100% client-side,
     * so bypassing this list is trivial. This is intended to prevent users from
     * accidentally clobbering a built-in object or function.
     * @private
     */

    Blockly.Generator.prototype.addReservedWords.call(this,
        "False,None,True,and,as,assert,break,class,continue,def,del,elif,else," +
            "except,exec,finally,for,from,global,if,import,in,is,lambda,nonlocal,not," +
            "or,pass,print,raise,return,try,while,with,yield," +
            "NotImplemented,Ellipsis,__debug__,quit,exit,copyright,license,credits," +
            "ArithmeticError,AssertionError,AttributeError,BaseException," +
            "BlockingIOError,BrokenPipeError,BufferError,BytesWarning," +
            "ChildProcessError,ConnectionAbortedError,ConnectionError," +
            "ConnectionRefusedError,ConnectionResetError,DeprecationWarning,EOFError," +
            "Ellipsis,EnvironmentError,Exception,FileExistsError,FileNotFoundError," +
            "FloatingPointError,FutureWarning,GeneratorExit,IOError,ImportError," +
            "ImportWarning,IndentationError,IndexError,InterruptedError," +
            "IsADirectoryError,KeyError,KeyboardInterrupt,LookupError,MemoryError," +
            "ModuleNotFoundError,NameError,NotADirectoryError,NotImplemented," +
            "NotImplementedError,OSError,OverflowError,PendingDeprecationWarning," +
            "PermissionError,ProcessLookupError,RecursionError,ReferenceError," +
            "ResourceWarning,RuntimeError,RuntimeWarning,StandardError," +
            "StopAsyncIteration,StopIteration,SyntaxError,SyntaxWarning,SystemError," +
            "SystemExit,TabError,TimeoutError,TypeError,UnboundLocalError," +
            "UnicodeDecodeError,UnicodeEncodeError,UnicodeError," +
            "UnicodeTranslateError,UnicodeWarning,UserWarning,ValueError,Warning," +
            "ZeroDivisionError,_,__build_class__,__debug__,__doc__,__import__," +
            "__loader__,__name__,__package__,__spec__,abs,all,any,apply,ascii," +
            "basestring,bin,bool,buffer,bytearray,bytes,callable,chr,classmethod,cmp," +
            "coerce,compile,complex,copyright,credits,delattr,dict,dir,divmod," +
            "enumerate,eval,exec,execfile,exit,file,filter,float,format,frozenset," +
            "getattr,globals,hasattr,hash,help,hex,id,input,int,intern,isinstance," +
            "issubclass,iter,len,license,list,locals,long,map,max,memoryview,min," +
            "next,object,oct,open,ord,pow,print,property,quit,range,raw_input,reduce," +
            "reload,repr,reversed,round,set,setattr,slice,sorted,staticmethod,str," +
            "sum,super,tuple,type,unichr,unicode,vars,xrange,zip,math,random"
    );

    /**
     * Order of operation ENUMs.
     * http://docs.Galaxia.org/reference/expressions.html#summary
     */
    this.ORDER_ATOMIC = 0; // 0 "" ...
    this.ORDER_COLLECTION = 1; // tuples, lists, dictionaries
    this.ORDER_STRING_CONVERSION = 1; // `expression...`
    this.ORDER_MEMBER = 2.1; // . []
    this.ORDER_FUNCTION_CALL = 2.2; // ()
    this.ORDER_EXPONENTIATION = 3; // **
    this.ORDER_UNARY_SIGN = 4; // + -
    this.ORDER_BITWISE_NOT = 4; // ~
    this.ORDER_MULTIPLICATIVE = 5; // * / // %
    this.ORDER_ADDITIVE = 6; // + -
    this.ORDER_BITWISE_SHIFT = 7; // << >>
    this.ORDER_BITWISE_AND = 8; // &
    this.ORDER_BITWISE_XOR = 9; // ^
    this.ORDER_BITWISE_OR = 10; // |
    this.ORDER_RELATIONAL = 11; // in, not in, is, is not, <, <=, >, >=, <>, !=, ==
    this.ORDER_LOGICAL_NOT = 12; // not
    this.ORDER_LOGICAL_AND = 13; // and
    this.ORDER_LOGICAL_OR = 14; // or
    this.ORDER_CONDITIONAL = 15; // if else
    this.ORDER_LAMBDA = 16; // lambda
    this.ORDER_NONE = 99; // (...)

    /**
     * List of outer-inner pairings that do NOT require parentheses.
     * @type {!Array.<!Array.<number>>}
     */
    this.ORDER_OVERRIDES = [
        [this.ORDER_FUNCTION_CALL, this.ORDER_MEMBER],
        [this.ORDER_FUNCTION_CALL, this.ORDER_FUNCTION_CALL],
        [this.ORDER_MEMBER, this.ORDER_MEMBER],
        [this.ORDER_MEMBER, this.ORDER_FUNCTION_CALL],
        [this.ORDER_LOGICAL_NOT, this.ORDER_LOGICAL_NOT],
        [this.ORDER_LOGICAL_AND, this.ORDER_LOGICAL_AND],
        [this.ORDER_LOGICAL_OR, this.ORDER_LOGICAL_OR],
    ];
}
Blockly.GalaxiaGenerator.prototype = Object.create(Blockly.Generator.prototype)
Blockly.GalaxiaGenerator.prototype.constructor = Blockly.GalaxiaGenerator

/**
 * Initialise the database of variable names.
 * @param {!Blockly.Workspace} workspace Workspace to generate code from.
 * @this {Blockly.Generator}
 */
Blockly.GalaxiaGenerator.prototype.init = function (workspace) {
    this.Python = Blockly.Python;
    Blockly.Python = this;
    // Écrasement de la valeur de this.INDENT et par conséquent de Blockly.Galaxia.INDENT
    this.INDENT = "    ";

    this.PASS = this.INDENT + "pass" + NEWLINE;
    // Create a dictionary of imports to be printed at the top of code.
    this.imports_ = Object.create(null);
    // Create a dictionary of constants to be printed after definitions.
    this.constants_ = Object.create(null);
    // Create a dictionary of object initializations to be printed after definitions.
    this.inits_ = Object.create(null);
    // Create a dictionary of code functions to be printed after user functions.
    this.codeFunctions_ = Object.create(null);
    
    // Create a dictionary mapping desired function names in codeFunctions_
    // to actual function names (to avoid collisions with user functions).
    this.functionNames_ = Object.create(null);

    // Create a dictionary of user setups to be printed after user functions.
    this.userFunctions_ = Object.create(null);
    // Create a dictionary of user functions to be printed after constants.
    this.userSetups_ = Object.create(null);
    // Create a dictionary of powers to be printed after definitions.
    this.powers_ = Object.create(null);

    if (!this.variableDB_) {
        this.variableDB_ = new Blockly.Names(this.RESERVED_WORDS_);
    } else {
        this.variableDB_.reset();
    }
    this.variableDB_.setVariableMap(workspace.getVariableMap());
};

/**
 * Prepend the generated code with the variable definitions.
 * @param {string} userLoop Generated code.
 * @return {string} Completed code.
 */
Blockly.GalaxiaGenerator.prototype.finish = function (userLoop) {
    // Merge all functions defined by user and by blocks code.
    this.functions_ = Object.assign({}, this.codeFunctions_, this.userFunctions_);

    // Create all definitions in head of Galaxia code.
    var imports = this.convertObjectInLists(this.imports_),
        constants = this.convertObjectInLists(this.constants_),
        inits = this.convertObjectInLists(this.inits_),
        functions = this.convertObjectInLists(this.functions_),
        powers = this.convertObjectInLists(this.powers_);

    var head = imports.join("\n") + "\n\n" + constants.join("\n") + "\n" + "\n\n" + inits.join("\n") + functions.join("\n\n") + powers.join("\n");

    // Add user setups after head of Galaxia code (from block 'on_start').
    var userSetups = [];
    for (var setup in this.userSetups_) {
        userSetups.push(this.userSetups_[setup]);
    }
    var userSetup = userSetups.join("\n");

    // Deleting objects and reseting workspace variables.
    delete this.userSetups_;
    delete this.functionNames_;
    this.variableDB_.reset();

    Blockly.Python = this.Python;

    return (head + userSetup).replace(/\n\n+/g, "\n\n").replace(/\n*$/, "\n\n") + userLoop;
};

/**
 * Adds a string of "import" code to be added at the top of sketch.
 * Once a include is added it will not get overwritten with new code.
 * @param {!string} importTag Identifier for this include code.
 * @param {!string} code Code to be included at the very top of the sketch.
 */
Blockly.GalaxiaGenerator.prototype.addImport = function (importTag, code) {
    if (this.imports_[importTag] === undefined) {
        this.imports_[importTag] = code;
    }
};

/**
 * Adds a string of "constant" code to be added after the imports.
 * Once a include is added it will not get overwritten with new code.
 * @param {!string} constantTag Identifier for this include code.
 * @param {!string} code Code to be included at the very top of the sketch.
 */
Blockly.GalaxiaGenerator.prototype.addConstant = function (constantTag, code) {
    if (this.constants_[constantTag] === undefined) {
        this.constants_[constantTag] = code;
    }
};

/**
 * Adds a string of "init" code to be added after functions.
 * Once a include is added it will not get overwritten with new code.
 * @param {!string} initTag Identifier for this include code.
 * @param {!string} code Code to be included at the very top of the sketch.
 */
Blockly.GalaxiaGenerator.prototype.addInit = function (initTag, code) {
    if (this.inits_[initTag] === undefined) {
        this.inits_[initTag] = code;
    }
};

/**
 * Adds a string of "function" code to be added after constants.
 * Once a include is added it will not get overwritten with new code.
 * @param {!string} functionTag Identifier for this include code.
 * @param {!string} code Code to be included at the very top of the sketch.
 */
Blockly.GalaxiaGenerator.prototype.addFunction = function (functionTag, code) {
    if (this.codeFunctions_[functionTag] === undefined) {
        this.codeFunctions_[functionTag] = code;
    }
};

/**
 * Adds a string of "powers" code to be added after initializations.
 * Once a include is added it will not get overwritten with new code.
 * @param {!string} initTag Identifier for this include code.
 * @param {!string} code Code to be included at the very top of the sketch.
 */
Blockly.GalaxiaGenerator.prototype.addPowerOn = function (powerTag, code) {
    if (this.powers_[powerTag] === undefined) {
        this.powers_[powerTag] = code;
    }
};

/**
 * Naked values are top-level blocks with outputs that aren't plugged into
 * anything.
 * @param {string} line Line of generated code.
 * @return {string} Legal line of code.
 */
Blockly.GalaxiaGenerator.prototype.scrubNakedValue = function (line) {
    return line + NEWLINE;
};

/**
 * Encode a string as a properly escaped Galaxia string, complete with quotes.
 * @param {string} string Text to encode.
 * @return {string} Galaxia string.
 * @private
 */
Blockly.GalaxiaGenerator.prototype.quote_ = function (string) {
    // Can't use goog.string.quote since % must also be escaped.
    string = string.replace(/\\/g, "\\\\").replace(/\n/g, "\\\n");

    // Follow the CGalaxia behaviour of repr() for a non-byte string.
    var quote = "'";
    if (string.indexOf("'") !== -1) {
        if (string.indexOf('"') === -1) {
            quote = '"';
        } else {
            string = string.replace(/'/g, "\\'");
        }
    }
    return quote + string + quote;
};

/**
 * Encode a string as a properly escaped multiline Galaxia string, complete
 * with quotes.
 * @param {string} string Text to encode.
 * @return {string} Galaxia string.
 * @private
 */
Blockly.GalaxiaGenerator.prototype.multiline_quote_ = function (string) {
    // Can't use goog.string.quote since % must also be escaped.
    string = string.replace(/'''/g, "\\'\\'\\'");
    return "'''" + string + "'''";
};

/**
 * Common tasks for generating Galaxia from blocks.
 * Handles comments for the specified block and any connected value blocks.
 * Calls any statements following this block.
 * @param {!Blockly.Block} block The current block.
 * @param {string} code The Galaxia code created for this block.
 * @param {boolean=} opt_thisOnly True to generate code for only this statement.
 * @return {string} Galaxia code with comments and subsequent blocks added.
 * @private
 */
Blockly.GalaxiaGenerator.prototype.scrub_ = function (block, code, opt_thisOnly) {
    var commentCode = "";
    // Only collect comments for blocks that aren't inline.
    if (!block.outputConnection || !block.outputConnection.targetConnection) {
        // Collect comment for this block.
        var comment = block.getCommentText();
        if (comment) {
            comment = Blockly.utils.string.wrap(comment, this.COMMENT_WRAP - 3);
            commentCode += this.prefixLines(comment + NEWLINE, "# ");
        }
        // Collect comments for all value arguments.
        // Don't collect comments for nested statements.
        for (var i = 0; i < block.inputList.length; i++) {
            if (block.inputList[i].type == Blockly.INPUT_VALUE) {
                var childBlock = block.inputList[i].connection.targetBlock();
                if (childBlock) {
                    comment = this.allNestedComments(childBlock);
                    if (comment) {
                        commentCode += this.prefixLines(comment, "# ");
                    }
                }
            }
        }
    }
    var nextBlock = block.nextConnection && block.nextConnection.targetBlock();
    var nextCode = opt_thisOnly ? "" : this.blockToCode(nextBlock);
    return commentCode + code + nextCode;
};

/**
 * Gets a property and adjusts the value, taking into account indexing, and
 * casts to an integer.
 * @param {!Blockly.Block} block The block.
 * @param {string} atId The property ID of the element to get.
 * @param {number=} opt_delta Value to add.
 * @param {boolean=} opt_negate Whether to negate the value.
 * @return {string|number}
 */
Blockly.GalaxiaGenerator.prototype.getAdjustedInt = function (block, atId, opt_delta, opt_negate) {
    var delta = opt_delta || 0;
    if (block.workspace.options.oneBasedIndex) {
        delta--;
    }
    var defaultAtIndex = block.workspace.options.oneBasedIndex ? "1" : "0";
    var atOrder = delta ? this.ORDER_ADDITIVE : this.ORDER_NONE;
    var at = this.valueToCode(block, atId, atOrder) || defaultAtIndex;

    if (Blockly.isNumber(at)) {
        // If the index is a naked number, adjust it right now.
        at = parseInt(at, 10) + delta;
        if (opt_negate) {
            at = -at;
        }
    } else {
        // If the index is dynamic, adjust it in code.
        if (delta > 0) {
            at = "int(" + at + " + " + delta + ")";
        } else if (delta < 0) {
            at = "int(" + at + " - " + -delta + ")";
        } else {
            at = "int(" + at + ")";
        }
        if (opt_negate) {
            at = "-" + at;
        }
    }
    return at;
};

/**
 * Convert an object into list of object values.
 * @param {Object} object
 * @return {!Array.<string>} list
 */
Blockly.GalaxiaGenerator.prototype.convertObjectInLists = function (object) {
    var list = [];
    for (var i in object) {
        list.push(object[i]);
    }
    if (list.length) {
        list.push(NEWLINE);
    }
    return list;
};

/**
 * Get all blocks defined in workspace by type.
 * @return {object} list
 */
Blockly.GalaxiaGenerator.prototype.getAllBlocksByType = function (type) {
    var blocks = Object.create(null);
    var blockDB = Blockly.Workspace.getAll()[0].blockDB_;
    for (block in blockDB) {
        if (blockDB[block].type == type) {
            blocks[block] = blockDB[block];
        }
    }
    return blocks;
};

/**
 * Re-establish Blockly.Python object.
 * Must be called before throwing any error from workspaceToCode.
 */
Blockly.GalaxiaGenerator.prototype.reestablishBlocklyPy = function () {
    if (this.Python) {
        Blockly.Python = this.Python;
    }
};

/**
 */
Blockly.GalaxiaGenerator.prototype.workspaceToCode = function (workspace) {
    try {
        return Blockly.Generator.prototype.workspaceToCode.call(this, workspace);
    } catch (e) {
        this.reestablishBlocklyPy();
        throw e;
    }
};

Blockly.GalaxiaGenerator.prototype.getUsedGlobalVarInBlock = function(sourceBlock, blockCode, exclude){
    /// Get the workspace's variables
    var variables = sourceBlock.workspace.getVariablesOfType("");
    var isVarInWorkspace = Object.keys(variables).length > 0;
    var usedVariables = [];
    var globalVar = "";

    function findVarInUserSetup(name){
        for(let code in Blockly.Python.userSetups_){
            if(Blockly.Python.userSetups_[code].includes(name))
                return true;
        }
        return false;
    }

    function includeGeneratedObject(tab, code){
        if(code && code.includes("galaxiaChronometer")){
            tab.push("galaxiaChronometer")
        }
        return tab
    }
    
    
    /// The names of the variables found in the generated code (branch) are declared as global at the start of the function.
    if (isVarInWorkspace) {
        for (var variable of variables) {
            var name = Blockly.Python.variableDB_.getName(variable.name, Blockly.VARIABLE_CATEGORY_NAME);
            if (blockCode.includes(name) && (!exclude || !exclude.includes(name))) {
                usedVariables.push(name);
            }
        }
    }
    usedVariables = includeGeneratedObject(usedVariables, blockCode);
    if (usedVariables.length > 0) {
        globalVar = Blockly.Python.INDENT + "global " + usedVariables.toString().replaceAll(",", ", ") + NEWLINE;
    }
    return globalVar;
}

Blockly.GalaxiaGenerator.prototype.map = function(value, in_min, in_max, out_min, out_max){
    Blockly.Python.userFunctions_["%thingz_map" ] = "def map(value, in_min, in_max, out_min, out_max):"+NEWLINE
                                                    +this.INDENT+"return (value - in_min) * (out_max - out_min) / (in_max - in_min) + out_min";
    
    return ["map("+value[0]+", "+in_min+", "+in_max+", "+out_min+", "+out_max+")", this.ORDER_ATOMIC]
}

/**
 * Galaxia code generator.
 * @type {!Blockly.Generator}
 */
Blockly.GalaxiaMicropython = new Blockly.GalaxiaGenerator("GalaxiaMicropython");

/**
 * Galaxia code generator.
 * @type {!Blockly.Generator}
 */
Blockly.Galaxia = new Blockly.GalaxiaGenerator("Galaxia");