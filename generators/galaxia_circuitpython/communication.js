// // Galaxia WiFi

// Blockly.Galaxia.communication_galaxia_simpleWifi_connect_basic = function (block) {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     var SSID = Blockly.Galaxia.valueToCode(block, "SSID", Blockly.Galaxia.ORDER_NONE) || "''";
//     var PWD = Blockly.Galaxia.valueToCode(block, "PWD", Blockly.Galaxia.ORDER_NONE) || "''";
//     return "simpleWifi.connect(" + SSID + ", " + PWD + ")" + NEWLINE;
// };

// Blockly.Galaxia.communication_galaxia_simpleWifi_connect = function (block) {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     var SSID = Blockly.Galaxia.valueToCode(block, "SSID", Blockly.Galaxia.ORDER_NONE) || "''";
//     var PWD = Blockly.Galaxia.valueToCode(block, "PWD", Blockly.Galaxia.ORDER_NONE) || "''";
//     var IP = Blockly.Galaxia.valueToCode(block, "IP", Blockly.Galaxia.ORDER_NONE) || "''";
//     var args = {};
//     var code = "simpleWifi.connect(" + SSID + ", " + PWD;
//     args["IP"] = IP.length > 2;
//     for (var arg in args) {
//         if (arg == "IP" && args[arg] == true) {
//             code += ", static_ip=" + IP;
//         }
//     }
//     return code + ")" + NEWLINE;
// };

// Blockly.Galaxia.communication_galaxia_simpleWifi_connect_complete = function (block) {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     var SSID = Blockly.Galaxia.valueToCode(block, "SSID", Blockly.Galaxia.ORDER_NONE) || "''";
//     var PWD = Blockly.Galaxia.valueToCode(block, "PWD", Blockly.Galaxia.ORDER_NONE) || "''";
//     var IP = Blockly.Galaxia.valueToCode(block, "IP", Blockly.Galaxia.ORDER_NONE) || "''";
//     var PORT = Blockly.Galaxia.valueToCode(block, "PORT", Blockly.Galaxia.ORDER_NONE) || "0";
//     var args = {};
//     var code = "simpleWifi.connect(" + SSID + ", " + PWD;
//     args["IP"] = IP.length > 2;
//     args["PORT"] = PORT > 0;
//     for (var arg in args) {
//         if (arg == "IP" && args[arg] == true) {
//             code += ", static_ip=" + IP;
//         } else if (arg == "PORT" && args[arg] == true) {
//             code += ", server_port=" + PORT;
//         }
//     }
//     return code + ")" + NEWLINE;
// };

// Blockly.Galaxia.communication_galaxia_simpleWifi_create_ap = function(block){
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     var SSID = Blockly.Galaxia.valueToCode(block, "SSID", Blockly.Galaxia.ORDER_NONE) || "''";
//     var PWD = Blockly.Galaxia.valueToCode(block, "PWD", Blockly.Galaxia.ORDER_NONE) || "''";
//     return "simpleWifi.start_access_point(" + SSID + ", " + PWD + ")" + NEWLINE;
// }

// Blockly.Galaxia.communication_galaxia_simpleWifi_getLastConnectedIp = function () {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     return ["simpleWifi.get_last_connected_ip()", Blockly.Python.ORDER_ATOMIC];
// };

// Blockly.Galaxia.communication_galaxia_simpleWifi_getMyIp = function () {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     return ["simpleWifi.get_my_ip()", Blockly.Python.ORDER_ATOMIC];
// };

// Blockly.Galaxia.communication_galaxia_simpleWifi_receive_basic = function (block) {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     var branchCode = Blockly.Galaxia.statementToCode(block, "DO") || Blockly.Galaxia.PASS;
//     var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VALUE"), Blockly.VARIABLE_CATEGORY_NAME);
//     // retrait de l'indentation induite par la méthode statementToCode
//     branchCode = branchCode.substring(Blockly.Galaxia.INDENT.length);
//     var regex = new RegExp("\n"+Blockly.Galaxia.INDENT, "g");
//     branchCode = branchCode.replace(regex, "\n");
//     return dataVar + " = simpleWifi.receive(stopSequence=\"\\n\", reuseClient=True)" + NEWLINE + branchCode;
// };

// Blockly.Galaxia.communication_galaxia_simpleWifi_receive_delimiter = function (block) {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     var branchCode = Blockly.Galaxia.statementToCode(block, "DO") || Blockly.Galaxia.PASS;
//     var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VALUE"), Blockly.VARIABLE_CATEGORY_NAME);
//     var delimiter = block.getFieldValue("DELIMITER");
//     // retrait de l'indentation induite par la méthode statementToCode
//     branchCode = branchCode.substring(Blockly.Galaxia.INDENT.length);
//     var regex = new RegExp("\n"+Blockly.Galaxia.INDENT, "g");
//     branchCode = branchCode.replace(regex, "\n");
//     return dataVar + " = simpleWifi.receive(stopSequence=\""+delimiter+"\", reuseClient=True)" + NEWLINE + branchCode;
// };

// Blockly.Galaxia.communication_galaxia_simpleWifi_receive = function (block) {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     var branchCode = Blockly.Galaxia.statementToCode(block, "DO") || Blockly.Galaxia.PASS;
//     var IP = Blockly.Galaxia.valueToCode(block, "IP", Blockly.Galaxia.ORDER_NONE) || "''";
//     var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VALUE"), Blockly.VARIABLE_CATEGORY_NAME);
//     // retrait de l'indentation induite par la méthode statementToCode
//     branchCode = branchCode.substring(Blockly.Galaxia.INDENT.length);
//     var regex = new RegExp("\n"+Blockly.Galaxia.INDENT, "g");
//     branchCode = branchCode.replace(regex, "\n");
//     // var length egal 2 if the string is empty : "''"
//     return dataVar + " = simpleWifi.receive(stopSequence=\"\\n\" " + (IP.length > 2 ? "ip=" + IP : "") + ")" + NEWLINE + branchCode;
// };

// Blockly.Galaxia.communication_galaxia_simpleWifi_receive_complete = function (block) {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     var branchCode = Blockly.Galaxia.statementToCode(block, "DO") || Blockly.Galaxia.PASS;
//     var IP = Blockly.Galaxia.valueToCode(block, "IP", Blockly.Galaxia.ORDER_NONE) || "''";
//     var TIMEOUT = Blockly.Galaxia.valueToCode(block, "TIMEOUT", Blockly.Galaxia.ORDER_NONE) || "0";
//     var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VALUE"), Blockly.VARIABLE_CATEGORY_NAME);
//     // var length egal 2 if the string is empty : "''"
//     return (
//         dataVar +
//         " = simpleWifi.receive(stopSequence=\"\\r\" " +
//         (IP.length > 2 ? "ip=" + IP : "") +
//         (IP.length > 2 && TIMEOUT > 0 ? ", " : "") +
//         (TIMEOUT > 0 ? "timeout=" + TIMEOUT : "") +
//         ")" +
//         NEWLINE +
//         "if " +
//         dataVar +
//         ":" +
//         NEWLINE +
//         branchCode
//     );
// };

// Blockly.Galaxia.communication_galaxia_simpleWifi_server_receive_async = function(block){
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     Blockly.Galaxia.userSetups_["%simple_wifi_receive_async"] = "simple_wifi_server_data_received = ''"

//     Blockly.Galaxia.codeFunctions_["%simple_wifi_receive_async"] = "def server_receive():"+NEWLINE
//     +Blockly.Galaxia.INDENT+"global simple_wifi_server_data_received"+NEWLINE
//     +Blockly.Galaxia.INDENT+'r = simpleWifi.receive(stopSequence="\\n", reuseClient=False, block=False)'+NEWLINE
//     +Blockly.Galaxia.INDENT+'if r:'+NEWLINE
//     +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"simple_wifi_server_data_received = r"

//     return ["simple_wifi_server_data_received", Blockly.Python.ORDER_ATOMIC];

// }

// Blockly.Galaxia.communication_galaxia_simpleWifi_server_receive_event = function(block){
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addImport("import_supervisor", IMPORT_SUPERVISOR);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     Blockly.Galaxia.userSetups_["%simple_wifi_receive_async"] = "simple_wifi_server_data_received = ''"

//     let branch = Blockly.Galaxia.statementToCode(block, "DO", Blockly.Galaxia.ORDER_NONE) || "''";
//     // console.log(branch)
//     branch = Blockly.Galaxia.INDENT+branch;
//     var regex = new RegExp("\n", "g");
//     branch = branch.replace(regex, "\n"+Blockly.Galaxia.INDENT);

//     let dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("DATA"), Blockly.VARIABLE_CATEGORY_NAME);

//     Blockly.Galaxia.codeFunctions_["%simple_wifi_receive_async"] = "def server_receive():"+NEWLINE
//     +Blockly.Galaxia.INDENT+"global simple_wifi_server_data_received"+NEWLINE
//     +Blockly.Galaxia.INDENT+dataVar+' = simpleWifi.receive(stopSequence="\\n", reuseClient=False, block=False)'+NEWLINE
//     +Blockly.Galaxia.INDENT+'if '+dataVar+':'+NEWLINE
//     +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"simple_wifi_server_data_received = "+dataVar+NEWLINE
//     +branch

//     Blockly.Galaxia.userSetups_["%simple_wifi_receive_event"] = 'supervisor.add_background_user_callback(server_receive)' + NEWLINE
//     return "";
// }

// Blockly.Galaxia.communication_galaxia_simpleWifi_server_is_client_connected = function(block){
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);

//     return ["simpleWifi.is_client_connected()", Blockly.Python.ORDER_ATOMIC];
// }

// Blockly.Galaxia.communication_galaxia_simpleWifi_server_respond = function(block){
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);

//     var data = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "''";
//     data = "str("+data+")"
//     return "simpleWifi.send_to_client("+data+", stopSequence=\"\\n\")" + NEWLINE;
// }

// Blockly.Galaxia.communication_galaxia_simpleWifi_server_respond_with_delimiter = function(block){
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);

//     var delimiter = block.getFieldValue("DELIMITER");
//     var data = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "''";
//     data = "str("+data+")"
//     return "simpleWifi.send_to_client("+data+", stopSequence=\""+delimiter+"\")" + NEWLINE;
// }

// Blockly.Galaxia.communication_galaxia_simpleWifi_send_basic = function (block) {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     var VALUE = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "''";
//     var IP = Blockly.Galaxia.valueToCode(block, "IP", Blockly.Galaxia.ORDER_NONE) || "''";
//     return "simpleWifi.send(" + VALUE + ", " + IP + ", stopSequence=\"\\n\")" + NEWLINE;
// };

// Blockly.Galaxia.communication_galaxia_simpleWifi_send_basic_with_delimiter = function (block) {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     var VALUE = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "''";
//     var IP = Blockly.Galaxia.valueToCode(block, "IP", Blockly.Galaxia.ORDER_NONE) || "''";
//     var delimiter = block.getFieldValue("DELIMITER");
//     return "simpleWifi.send(" + VALUE + ", " + IP + ", stopSequence='"+delimiter+"')" + NEWLINE;
// };

// Blockly.Galaxia.communication_galaxia_simpleWifi_send_wait_respond_with_delimiter = function (block){
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     var VALUE = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "''";
//     var IP = Blockly.Galaxia.valueToCode(block, "IP", Blockly.Galaxia.ORDER_NONE) || "''";
//     var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("RESPONSE_VALUE"), Blockly.VARIABLE_CATEGORY_NAME);
//     var branchCode = Blockly.Galaxia.statementToCode(block, "DO") || Blockly.Galaxia.PASS;
//     // retrait de l'indentation induite par la méthode statementToCode
//     branchCode = branchCode.substring(Blockly.Galaxia.INDENT.length);
//     var regex = new RegExp("\n"+Blockly.Galaxia.INDENT, "g");
//     branchCode = branchCode.replace(regex, "\n");
//     var delimiter = block.getFieldValue("DELIMITER");
//     return dataVar+" = simpleWifi.send(" + VALUE + ", " + IP + ", stopSequence='"+delimiter+"', waitResponse=True)" + NEWLINE + branchCode;
// }

// Blockly.Galaxia.communication_galaxia_simpleWifi_send_wait_respond = function (block){
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     var VALUE = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "''";
//     var IP = Blockly.Galaxia.valueToCode(block, "IP", Blockly.Galaxia.ORDER_NONE) || "''";
//     var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("RESPONSE_VALUE"), Blockly.VARIABLE_CATEGORY_NAME);
//     var branchCode = Blockly.Galaxia.statementToCode(block, "DO") || Blockly.Galaxia.PASS;
//     // retrait de l'indentation induite par la méthode statementToCode
//     branchCode = branchCode.substring(Blockly.Galaxia.INDENT.length);
//     var regex = new RegExp("\n"+Blockly.Galaxia.INDENT, "g");
//     branchCode = branchCode.replace(regex, "\n");
//     return dataVar+" = simpleWifi.send(" + VALUE + ", " + IP + ", stopSequence=\"\\n\", waitResponse=True)" + NEWLINE + branchCode;
// }

// Blockly.Galaxia.communication_galaxia_simpleWifi_send_complete = function (block) {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     var VALUE = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "''";
//     var IP = Blockly.Galaxia.valueToCode(block, "IP", Blockly.Galaxia.ORDER_NONE) || "''";
//     var PORT = Blockly.Galaxia.valueToCode(block, "PORT", Blockly.Galaxia.ORDER_NONE) || "0";
//     return "simpleWifi.send(" + VALUE + ", " + IP + (PORT > 0 ? ", port=" + PORT : "") + ")" + NEWLINE;
// };

// // Galaxia HTTP

// Blockly.Galaxia.communication_galaxia_simpleHttp_respond_basic = function (block) {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     Blockly.Galaxia.addConstant("galaxia_http_init", GALAXIA_HTTP_INIT);
//     var VALUE = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "''";
//     return "http.respond(" + VALUE + ")" + NEWLINE;
// };

// Blockly.Galaxia.communication_galaxia_simpleHttp_respond_complete = function (block) {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     Blockly.Galaxia.addConstant("galaxia_http_init", GALAXIA_HTTP_INIT);
//     var VALUE = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "''";
//     var CODE = Blockly.Galaxia.valueToCode(block, "CODE", Blockly.Galaxia.ORDER_NONE) || "''";
//     return "http.respond(" + VALUE + (CODE > 0 ? ", code=" + CODE : "") + ")" + NEWLINE;
// };

// Blockly.Galaxia.communication_galaxia_simpleHttp_wait_request = function (block) {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     Blockly.Galaxia.addConstant("galaxia_http_init", GALAXIA_HTTP_INIT);
//     var branchCode = Blockly.Galaxia.statementToCode(block, "DO") || "";
//     var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VALUE"), Blockly.VARIABLE_CATEGORY_NAME);
//     // retrait de l'indentation induite par la méthode statementToCode
//     branchCode = branchCode.substring(Blockly.Galaxia.INDENT.length);
//     var regex = new RegExp("\n"+Blockly.Galaxia.INDENT, "g");
//     branchCode = branchCode.replace(regex, "\n");
//     return "response = http.wait_request()" + NEWLINE + dataVar + " = response.get_url()" + NEWLINE + branchCode;
// };

// Blockly.Galaxia.communication_galaxia_simpleHttp_generate_page = function (block) {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addImport("import_supervisor", IMPORT_SUPERVISOR);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     Blockly.Galaxia.addConstant("galaxia_http_init", GALAXIA_HTTP_INIT);
//     var branchCode = Blockly.Galaxia.statementToCode(block, "DO") || "";
//     var reload = Blockly.Galaxia.valueToCode(block, "RELOAD", Blockly.Galaxia.ORDER_NONE) || 10;

//     // re-indent statemnt
//     branchCode = Blockly.Galaxia.INDENT+branchCode;
//     var regex = new RegExp("\n", "g");
//     branchCode = branchCode.replace(regex, "\n"+Blockly.Galaxia.INDENT);

//     branchCode = branchCode.substring(Blockly.Galaxia.INDENT.length);
//     var replace = "\n" + Blockly.Galaxia.INDENT;
//     var regex = new RegExp(replace, "g");
//     branchCode = branchCode.replace(regex, "\n");

//     var get_request = Blockly.Galaxia.INDENT + "thgz_response = ''"

//     var globalVar = Blockly.Galaxia.getUsedGlobalVarInBlock(block, branchCode)
//     var defEvent = "def index_page():" + NEWLINE
//     + globalVar + get_request + NEWLINE 
//     + branchCode 
//     + Blockly.Galaxia.INDENT+"http.respond_with_html(thgz_response, template=\"autoreload.html\", tags=[(\"reload\", "+reload*1000+")])"+ NEWLINE

//     var callEvent = 'http.add_web_page_url("", index_page)'+ NEWLINE + "web_pages = lambda: http.handle_web_pages()"+ NEWLINE +"supervisor.set_background_user_callback(web_pages)";

//     Blockly.Galaxia.codeFunctions_["%user_callback"] = defEvent;
//     Blockly.Galaxia.userSetups_["%user_callback"] = callEvent;

//     return ""
// };

// Blockly.Galaxia.communication_galaxia_simpleHttp_generate_page_no_refresh = function (block) {
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addImport("import_supervisor", IMPORT_SUPERVISOR);
//     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     Blockly.Galaxia.addConstant("galaxia_http_init", GALAXIA_HTTP_INIT);
//     var branchCode = Blockly.Galaxia.statementToCode(block, "DO") || "";

//     // re-indent statemnt
//     branchCode = Blockly.Galaxia.INDENT+branchCode;
//     var regex = new RegExp("\n", "g");
//     branchCode = branchCode.replace(regex, "\n"+Blockly.Galaxia.INDENT);

//     branchCode = branchCode.substring(Blockly.Galaxia.INDENT.length);
//     var replace = "\n" + Blockly.Galaxia.INDENT;
//     var regex = new RegExp(replace, "g");
//     branchCode = branchCode.replace(regex, "\n");

//     var get_request = Blockly.Galaxia.INDENT + "thgz_response = ''"

//     var globalVar = Blockly.Galaxia.getUsedGlobalVarInBlock(block, branchCode)
//     var defEvent = "def index_page():" + NEWLINE
//     + globalVar + get_request + NEWLINE 
//     + branchCode 
//     + Blockly.Galaxia.INDENT+"http.respond_with_html(thgz_response, template=\"autoreload.html\")"+ NEWLINE

//     var callEvent = 'http.add_web_page_url("", index_page)'+ NEWLINE + "web_pages = lambda: http.handle_web_pages()"+ NEWLINE +"supervisor.set_background_user_callback(web_pages)";

//     Blockly.Galaxia.codeFunctions_["%user_callback"] = defEvent;
//     Blockly.Galaxia.userSetups_["%user_callback"] = callEvent;

//     return ""
// };

// Blockly.Galaxia.communication_galaxia_simpleHttp_add_to_page = function (block) {
//     var VALUE = Blockly.Galaxia.valueToCode(block, "DATA", Blockly.Galaxia.ORDER_NONE) || "''";
//     return "thgz_response += '<div>'+str("+VALUE + ") + '</div>'" + NEWLINE
// };

// Blockly.Galaxia.communication_galaxia_simpleHttp_add_button_to_page = function (block) {
//     var VALUE = Blockly.Galaxia.valueToCode(block, "TITLE", Blockly.Galaxia.ORDER_NONE) || "''";
//     var CMD = Blockly.Galaxia.valueToCode(block, "CMD", Blockly.Galaxia.ORDER_NONE) || "''";
//     CMD = CMD.replace(/'/g, '"')
//     VALUE = VALUE.replace(/'/g, "") 

//     return "thgz_response += '<div><button class=\"tgz_button\" onclick=\\'cmd(\"/\"+"+CMD+", this)\\'>"+VALUE + "</button></div>'" + NEWLINE
// };

// Blockly.Galaxia.communication_galaxia_simpleHttp_button_event = function(block){

//     var branchCode = Blockly.Galaxia.statementToCode(block, "DO") || "";
//     var cmd = Blockly.Galaxia.valueToCode(block, "CMD", Blockly.Galaxia.ORDER_NONE) || "";
//     cmd = cmd.replace(/'/g, '')

//     if(branchCode.length == 0){
//         branchCode = Blockly.Galaxia.INDENT+'pass'
//     }
//     // re-indent statemnt
//     branchCode = Blockly.Galaxia.INDENT+branchCode;
//     var regex = new RegExp("\n", "g");
//     branchCode = branchCode.replace(regex, "\n"+Blockly.Galaxia.INDENT);

//     branchCode = branchCode.substring(Blockly.Galaxia.INDENT.length);
//     var replace = "\n" + Blockly.Galaxia.INDENT;
//     var regex = new RegExp(replace, "g");
//     branchCode = branchCode.replace(regex, "\n");

//     var globalVar = Blockly.Galaxia.getUsedGlobalVarInBlock(block, branchCode)
//     var defEvent = "def on_web_button_"+cmd+"():" + NEWLINE
    
//     if(globalVar.length > 0){
//         defEvent += globalVar + NEWLINE
//     }
//     defEvent += branchCode 

//     var callEvent = 'http.add_web_button("'+cmd+'", on_web_button_'+cmd+')'+ NEWLINE;

//     Blockly.Galaxia.codeFunctions_["%on_web_button_"+cmd] = defEvent;
//     Blockly.Galaxia.userSetups_["%on_web_button_"+cmd] = callEvent;

//     return ""
// }

// Blockly.Galaxia.communication_galaxia_simpleHttp_add_gauge_to_page = function (block) {
//     var VALUE = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "0";
//     var MIN = Blockly.Galaxia.valueToCode(block, "MIN", Blockly.Galaxia.ORDER_NONE) || "0";
//     var MAX = Blockly.Galaxia.valueToCode(block, "MAX", Blockly.Galaxia.ORDER_NONE) || "100";

//     var TITLE = Blockly.Galaxia.valueToCode(block, "TITLE", Blockly.Galaxia.ORDER_NONE) || "";
//     return "thgz_response += '<div>'+str("+TITLE+")+': <meter value=\"'+str("+VALUE+")+'\" min=\"'+str("+MIN+")+'\" max=\"'+str("+MAX+")+'\">'+str("+TITLE+")+'</meter>'+str("+VALUE+")+'</div>'" + NEWLINE
// };

// Blockly.Galaxia.communication_galaxia_simpleHttp_add_gauge_to_page_without_text = function (block) {
//     var VALUE = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "0";
//     var MIN = Blockly.Galaxia.valueToCode(block, "MIN", Blockly.Galaxia.ORDER_NONE) || "0";
//     var MAX = Blockly.Galaxia.valueToCode(block, "MAX", Blockly.Galaxia.ORDER_NONE) || "100";

//     var TITLE = Blockly.Galaxia.valueToCode(block, "TITLE", Blockly.Galaxia.ORDER_NONE) || "";
//     return "thgz_response += '<div>'+str("+TITLE+")+': <meter value=\"'+str("+VALUE+")+'\" min=\"'+str("+MIN+")+'\" max=\"'+str("+MAX+")+'\">'+str("+TITLE+")+'</meter>'+'</div>'" + NEWLINE
// };
// // Galaxia simple HTTP REQUEST

// // Blockly.Galaxia.communication_galaxia_SimpleHttpRequest_getURL = function (block) {
// //     Blockly.Galaxia.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
// //     Blockly.Galaxia.addConstant("galaxia_http_init", GALAXIA_HTTP_INIT);
// //     var reponse = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "''";
// //     return reponse + ".get_url()" + NEWLINE;
// // };


// Blockly.Galaxia.communication_galaxia_simple_mqtt_receive = function (block) {
//     var branchCode = Blockly.Galaxia.statementToCode(block, "DO") || "";

//     var topic = Blockly.Python.variableDB_.getName(block.getFieldValue("TOPIC"), Blockly.VARIABLE_CATEGORY_NAME);
//     var message = Blockly.Python.variableDB_.getName(block.getFieldValue("VALUE"), Blockly.VARIABLE_CATEGORY_NAME);

//     var globalVar = Blockly.Galaxia.getUsedGlobalVarInBlock(block, branchCode, [topic, message])
//     var defEvent = "def simple_mqtt_on_message("+topic+","+message+"):" + NEWLINE
//     + globalVar + (globalVar.length ? NEWLINE : "")
//     + (branchCode.length ? branchCode : Blockly.Galaxia.INDENT+ "pass" + NEWLINE)

//     Blockly.Galaxia.codeFunctions_["%simple_mqtt_on_message"] = defEvent;

//     return ""
// };

// Blockly.Galaxia.communication_galaxia_simple_mqtt_on_connect = function(block){
//     var branchCode = Blockly.Galaxia.statementToCode(block, "DO") || "";

//     var globalVar = Blockly.Galaxia.getUsedGlobalVarInBlock(block, branchCode)
//     var defEvent = "def simple_mqtt_on_connected():" + NEWLINE
//     + globalVar + (globalVar.length ? NEWLINE : "")
//     + (branchCode.length ? branchCode : Blockly.Galaxia.INDENT+ "pass" + NEWLINE)

//     Blockly.Galaxia.codeFunctions_["%simple_mqtt_on_connect"] = defEvent;

//     return ""
// }

// Blockly.Galaxia.communication_galaxia_simple_mqtt_on_disconnect = function(block){
//     var branchCode = Blockly.Galaxia.statementToCode(block, "DO") || "";

//     var globalVar = Blockly.Galaxia.getUsedGlobalVarInBlock(block, branchCode)
//     var defEvent = "def simple_mqtt_on_disconnected():" + NEWLINE
//     + globalVar + (globalVar.length ? NEWLINE : "")
//     + (branchCode.length ? branchCode : Blockly.Galaxia.INDENT+ "pass" + NEWLINE)

//     Blockly.Galaxia.codeFunctions_["%simple_mqtt_on_disconnect"] = defEvent;

//     return ""
// }

// Blockly.Galaxia.communication_galaxia_simple_mqtt_connect_complete = function(block){
//     Blockly.Galaxia.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
//     Blockly.Galaxia.addImport("import_galaxia_mqtt", IMPORT_GALAXIA_MQTT);
//     Blockly.Galaxia.addImport("import_supervisor", IMPORT_SUPERVISOR);
    
//     if(!Blockly.Galaxia.codeFunctions_["%simple_mqtt_on_disconnect"] || !Blockly.Galaxia.codeFunctions_["%simple_mqtt_on_disconnect"].length){
//         Blockly.Galaxia.codeFunctions_["%simple_mqtt_on_disconnect"] = "def simple_mqtt_on_disconnected():" + NEWLINE + Blockly.Galaxia.INDENT + "pass" + NEWLINE
//     }

//     if(!Blockly.Galaxia.codeFunctions_["%simple_mqtt_on_connect"] || !Blockly.Galaxia.codeFunctions_["%simple_mqtt_on_connect"].length){
//         Blockly.Galaxia.codeFunctions_["%simple_mqtt_on_connect"] = "def simple_mqtt_on_connected():" + NEWLINE + Blockly.Galaxia.INDENT + "pass" + NEWLINE
//     }

//     if(!Blockly.Galaxia.codeFunctions_["%simple_mqtt_on_message"] || !Blockly.Galaxia.codeFunctions_["%simple_mqtt_on_message"].length){
//         Blockly.Galaxia.codeFunctions_["%simple_mqtt_on_message"] = "def simple_mqtt_on_message(topic, message):" + NEWLINE + Blockly.Galaxia.INDENT + "pass" + NEWLINE
//     }

//     var broker = Blockly.Galaxia.valueToCode(block, "BROKER", Blockly.Galaxia.ORDER_NONE) || "''";
//     var username = Blockly.Galaxia.valueToCode(block, "USERNAME", Blockly.Galaxia.ORDER_NONE) || "'"+Blockly.utils.genUid()+"'";
//     var password = Blockly.Galaxia.valueToCode(block, "PASSWORD", Blockly.Galaxia.ORDER_NONE) || "''";
//     var port = Blockly.Galaxia.valueToCode(block, "PORT", Blockly.Galaxia.ORDER_NONE) || 1883;

//     if(username == "''"){
//         username = "'"+Blockly.utils.genUid()+"'"
//     }

//     Blockly.Galaxia.userSetups_["%simple_mqtt_instance"] = 'simpleMqtt = simple_mqtt.SimpleMQTT('+username+', '+password+', '+broker+', '+port+', simple_wifi._simple_tcp_socket_pool, simple_mqtt_on_connected, simple_mqtt_on_disconnected, simple_mqtt_on_message)'
//     Blockly.Galaxia.userSetups_["%simple_mqtt_cb"] = 'simpleMqttCb = lambda: simpleMqtt.process()'
//     Blockly.Galaxia.userSetups_["%simple_mqtt_supervisor"] = ""
//     + "try:" + NEWLINE
//     + Blockly.Galaxia.INDENT + 'supervisor.add_background_user_callback(simpleMqttCb, period=300)' + NEWLINE
//     + "except TypeError:" + NEWLINE
//     + Blockly.Galaxia.INDENT + 'supervisor.add_background_user_callback(simpleMqttCb)' + NEWLINE

//     return "simpleMqtt.connect()" + NEWLINE
// }

// Blockly.Galaxia.communication_galaxia_simple_mqtt_subscribe = function(block){
//     var topic = Blockly.Galaxia.valueToCode(block, "TOPIC", Blockly.Galaxia.ORDER_NONE) || "''";

//     return "simpleMqtt.subscribe("+topic+")"+ NEWLINE
// }

// Blockly.Galaxia.communication_galaxia_simple_mqtt_publish = function(block){
//     var topic = Blockly.Galaxia.valueToCode(block, "TOPIC", Blockly.Galaxia.ORDER_NONE) || "''";
//     var message = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "''";
    
//     return "simpleMqtt.publish("+topic+", "+message+")"+ NEWLINE
// }

// Blockly.Galaxia.communication_galaxia_simple_mqtt_is_connected = function(block){
//     var topic = Blockly.Galaxia.valueToCode(block, "TOPIC", Blockly.Galaxia.ORDER_NONE) || "''";
//     var message = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "''";
    
//     return ["simpleMqtt.is_connected()", Blockly.Python.ORDER_ATOMIC];
// }