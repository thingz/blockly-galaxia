Blockly.Galaxia.galaxia_neopixel_define = function (block) {
    Blockly.Galaxia.addImport('adafruit_neopixel', IMPORT_ADAFRUIT_NEOPIXEL);
    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    
    let len = Blockly.Python.valueToCode(block, "LEN", Blockly.Python.ORDER_NONE) || "0";
    let pin = block.getFieldValue("IO");
    let varName = "pin"+pin.substring(1);

    Blockly.Galaxia.userSetups_["io_pin_"+pin] = varName+" = adafruit_neopixel.NeoPixel(board."+pin+", "+len+")";
    
    return ""
    
};

Blockly.Galaxia.galaxia_neopixel_set_led_rgb = function (block){    
    let pin = block.getFieldValue("IO");
    let varName = "pin"+pin.substring(1);
    let r = Blockly.Python.valueToCode(block, "R", Blockly.Python.ORDER_NONE) || "0";
    let g = Blockly.Python.valueToCode(block, "G", Blockly.Python.ORDER_NONE) || "0";
    let b = Blockly.Python.valueToCode(block, "B", Blockly.Python.ORDER_NONE) || "0";
    let index = Blockly.Python.valueToCode(block, "INDEX", Blockly.Python.ORDER_NONE) || "0";
    if (r > 255) r = 255;
    if (r < 0) r = 0;
    if (g > 255) g = 255;
    if (g < 0) g = 0;
    if (b > 255) b = 255;
    if (b < 0) b = 0;
    
   
    return varName+"[" + index + "] = (" + r + ", " + g + ", " + b + ")" + NEWLINE +varName +".show()" + NEWLINE;

};


Blockly.Galaxia.galaxia_neopixel_set_led_color = function (block){
    let pin = block.getFieldValue("IO");
    let varName = "pin"+pin.substring(1);
    let color = Blockly.Galaxia.valueToCode(block, "COLOR", Blockly.Galaxia.ORDER_NONE) || "(255, 0, 0)";
    let index = Blockly.Python.valueToCode(block, "INDEX", Blockly.Python.ORDER_NONE) || "0";

    let hex = color.match('([0-9]+), ([0-9]+), ([0-9]+)')

    hex[1] = "0x"+parseInt(hex[1]).toString(16).padStart(2, '0')
    hex[2] = "0x"+parseInt(hex[2]).toString(16).padStart(2, '0')
    hex[3] = "0x"+parseInt(hex[3]).toString(16).padStart(2, '0')
    
   
    return varName+"[" + index + "] = (" + hex[1] + ", " + hex[2] + ", " + hex[3] + ")" + NEWLINE +varName +".show()" + NEWLINE;

};

Blockly.Galaxia.galaxia_neopixel_set_leds_rgb = function(block){
    let pin = block.getFieldValue("IO");
    let varName = "pin"+pin.substring(1);
    let r = Blockly.Python.valueToCode(block, "R", Blockly.Python.ORDER_NONE) || "0";
    let g = Blockly.Python.valueToCode(block, "G", Blockly.Python.ORDER_NONE) || "0";
    let b = Blockly.Python.valueToCode(block, "B", Blockly.Python.ORDER_NONE) || "0";
    if (r > 255) r = 255;
    if (r < 0) r = 0;
    if (g > 255) g = 255;
    if (g < 0) g = 0;
    if (b > 255) b = 255;
    if (b < 0) b = 0;

    return "for i in range(len("+varName+")):" + NEWLINE +
    Blockly.Galaxia.INDENT+varName+"[i] = ("+ r + ", " + g + ", " + b + ")" + NEWLINE +varName +".show()" + NEWLINE;
};

Blockly.Galaxia.galaxia_neopixel_set_leds_color = function(block){
    let pin = block.getFieldValue("IO");
    let varName = "pin"+pin.substring(1);
    let color = Blockly.Galaxia.valueToCode(block, "COLOR", Blockly.Galaxia.ORDER_NONE) || "(255, 0, 0)";

    let hex = color.match('([0-9]+), ([0-9]+), ([0-9]+)')

    hex[1] = "0x"+parseInt(hex[1]).toString(16).padStart(2, '0')
    hex[2] = "0x"+parseInt(hex[2]).toString(16).padStart(2, '0')
    hex[3] = "0x"+parseInt(hex[3]).toString(16).padStart(2, '0')

    return "for i in range(len("+varName+")):" + NEWLINE +
    Blockly.Galaxia.INDENT+varName+"[i] = ("+ hex[1] + ", " + hex[2] + ", " + hex[3] + ")" + NEWLINE +varName +".show()" + NEWLINE;
};

