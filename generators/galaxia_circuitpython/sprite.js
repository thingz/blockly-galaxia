Blockly.Galaxia.sprite_rectangle_create = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let r = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x = Blockly.Galaxia.valueToCode(block, "X", Blockly.Galaxia.ORDER_NONE) || 0;
    let y = Blockly.Galaxia.valueToCode(block, "Y", Blockly.Galaxia.ORDER_NONE) || 0;
    let width = Blockly.Galaxia.valueToCode(block, "WIDTH", Blockly.Galaxia.ORDER_NONE) || 10;
    let height = Blockly.Galaxia.valueToCode(block, "HEIGHT", Blockly.Galaxia.ORDER_NONE) || 20;
    let color = Blockly.Galaxia.valueToCode(block, "COLOR", Blockly.Galaxia.ORDER_NONE) || "(255, 0, 0)";
    let hex = color.match('([0-9]+), ([0-9]+), ([0-9]+)')

    hex[1] = parseInt(hex[1]).toString(16).padStart(2, '0')
    hex[2] = parseInt(hex[2]).toString(16).padStart(2, '0')
    hex[3] = parseInt(hex[3]).toString(16).padStart(2, '0')

    return r+" = sprites.rectangle("+x+", "+y+", 0x"+hex[1]+hex[2]+hex[3]+", "+width+", "+height+")\n"
}

Blockly.Galaxia.sprite_rectangle_set_x_y = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let r = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x_y = block.getFieldValue("X_Y")
    let value = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || 0;

    return r+"."+x_y+" = "+value+"\n"
}

Blockly.Galaxia.sprite_rectangle_get_x_y = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let r = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x_y = block.getFieldValue("X_Y")
    return [r+"."+x_y, Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.sprite_rectangle_set_width_height = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let r = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let w_h = block.getFieldValue("W_H")
    let value = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || 0;

    return r+"."+w_h+" = "+value+"\n"
}

Blockly.Galaxia.sprite_rectangle_get_width_height = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let r = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let w_h = block.getFieldValue("W_H")

    return [r+"."+w_h, Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.sprite_rectangle_set_color = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let r = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let color = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "(255, 0, 0)";
    let hex = color.match('([0-9]+), ([0-9]+), ([0-9]+)')

    hex[1] = parseInt(hex[1]).toString(16).padStart(2, '0')
    hex[2] = parseInt(hex[2]).toString(16).padStart(2, '0')
    hex[3] = parseInt(hex[3]).toString(16).padStart(2, '0')

    return r+".color = 0x"+hex[1]+hex[2]+hex[3]+"\n"
}

Blockly.Galaxia.sprite_rectangle_get_color = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let r = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    
    return [r+".color", Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.sprite_rectangle_set_hidden = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let r = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let hidden = block.getFieldValue("HIDDEN")

    return r+".hidden ="+hidden+"\n"
}

Blockly.Galaxia.sprite_rectangle_get_hidden = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let r = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return ["not "+r+".hidden", Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.sprite_rectangle_move_by = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let pixels = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || 0;

    return i+".moveBy("+pixels+")\n"
}   

Blockly.Galaxia.sprite_rectangle_turn= function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let direction =  block.getFieldValue("DIRECTION")
    let angle = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || 0;
    return i+".direction += "+direction+"*"+angle+"\n"
}

Blockly.Galaxia.sprite_rectangle_bounce_if_edge= function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return i+".bounceIfEdge()\n"
}

Blockly.Galaxia.sprite_image_create = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x = Blockly.Galaxia.valueToCode(block, "X", Blockly.Galaxia.ORDER_NONE) || 0;
    let y = Blockly.Galaxia.valueToCode(block, "Y", Blockly.Galaxia.ORDER_NONE) || 0;
    let scale = Blockly.Galaxia.valueToCode(block, "SCALE", Blockly.Galaxia.ORDER_NONE) || 1;
    let path = Blockly.Galaxia.valueToCode(block, "PATH", Blockly.Galaxia.ORDER_NONE) || "'/img.bmp'";

    return i+" = sprites.image("+x+", "+y+", "+scale+", "+path+")\n"
}

Blockly.Galaxia.sprite_image_set_x_y = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x_y = block.getFieldValue("X_Y")
    let value = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || 0;

    return i+"."+x_y+" = "+value+"\n"
}

Blockly.Galaxia.sprite_image_get_x_y = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x_y = block.getFieldValue("X_Y")

    return [i+"."+x_y, Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.sprite_image_get_width_height = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let r = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let w_h = block.getFieldValue("W_H")

    return [r+"."+w_h, Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.sprite_image_set_scale = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let scale = Blockly.Galaxia.valueToCode(block, "SCALE", Blockly.Galaxia.ORDER_NONE) || 0;

    return i+".scale = "+scale+"\n"
}

Blockly.Galaxia.sprite_image_get_scale = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return [i+".scale", Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.sprite_icon_set_color = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let color = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || "(255, 0, 0)";
    let hex = color.match('([0-9]+), ([0-9]+), ([0-9]+)')

    hex[1] = parseInt(hex[1]).toString(16).padStart(2, '0')
    hex[2] = parseInt(hex[2]).toString(16).padStart(2, '0')
    hex[3] = parseInt(hex[3]).toString(16).padStart(2, '0')

    return i+".color = 0x"+hex[1]+hex[2]+hex[3]+"\n"
}

Blockly.Galaxia.sprite_icon_get_color = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    
    return [i+".color", Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.sprite_image_set_hidden = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let hidden = block.getFieldValue("HIDDEN")

    return i+".hidden ="+hidden+"\n"
}

Blockly.Galaxia.sprite_image_get_hidden = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return ["not "+i+".hidden", Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.sprite_image_move_by = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let pixels = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || 0;

    return i+".moveBy("+pixels+")\n"
}   

Blockly.Galaxia.sprite_image_turn= function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let direction =  block.getFieldValue("DIRECTION")
    let angle = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || 0;
    return i+".direction += "+direction+"*"+angle+"\n"
}

Blockly.Galaxia.sprite_image_bounce_if_edge= function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return i+".bounceIfEdge()\n"
}

Blockly.Galaxia.sprite_icon_create = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x = Blockly.Galaxia.valueToCode(block, "X", Blockly.Galaxia.ORDER_NONE) || 0;
    let y = Blockly.Galaxia.valueToCode(block, "Y", Blockly.Galaxia.ORDER_NONE) || 0;
    let scale = Blockly.Galaxia.valueToCode(block, "SCALE", Blockly.Galaxia.ORDER_NONE) || 1;
    let name = block.getFieldValue("NAME") || "cross";
    let color = Blockly.Galaxia.valueToCode(block, "COLOR", Blockly.Galaxia.ORDER_NONE) || "(255, 0, 0)";
    let hex = color.match('([0-9]+), ([0-9]+), ([0-9]+)')

    hex[1] = parseInt(hex[1]).toString(16).padStart(2, '0')
    hex[2] = parseInt(hex[2]).toString(16).padStart(2, '0')
    hex[3] = parseInt(hex[3]).toString(16).padStart(2, '0')

    return i+" = sprites.icon("+x+", "+y+", "+scale+", '"+name+"', 0x"+hex[1]+hex[2]+hex[3]+")\n"
}

Blockly.Galaxia.sprite_icon_set_x_y = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x_y = block.getFieldValue("X_Y")
    let value = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || 0;

    return i+"."+x_y+" = "+value+"\n"
}

Blockly.Galaxia.sprite_icon_get_x_y = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x_y = block.getFieldValue("X_Y")

    return [i+"."+x_y, Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.sprite_icon_get_width_height = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let r = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let w_h = block.getFieldValue("W_H")

    return [r+"."+w_h, Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.sprite_icon_set_scale = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let scale = Blockly.Galaxia.valueToCode(block, "SCALE", Blockly.Galaxia.ORDER_NONE) || 0;

    return i+".scale = "+scale+"\n"
}

Blockly.Galaxia.sprite_icon_get_scale = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return [i+".scale", Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.sprite_icon_set_hidden = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let hidden = block.getFieldValue("HIDDEN")

    return i+".hidden ="+hidden+"\n"
}

Blockly.Galaxia.sprite_icon_get_hidden = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return ["not "+i+".hidden", Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.sprite_icon_move_by = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let pixels = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || 0;

    return i+".moveBy("+pixels+")\n"
}   

Blockly.Galaxia.sprite_icon_turn= function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let direction =  block.getFieldValue("DIRECTION")
    let angle = Blockly.Galaxia.valueToCode(block, "VALUE", Blockly.Galaxia.ORDER_NONE) || 0;
    return i+".direction += "+direction+"*"+angle+"\n"
}

Blockly.Galaxia.sprite_icon_bounce_if_edge= function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);
    let i = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return i+".bounceIfEdge()\n"
}

Blockly.Galaxia.sprite_collision = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let s1 = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let s2 = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR2"), Blockly.VARIABLE_CATEGORY_NAME)

    return ["sprites.collision("+s1+","+s2+")", Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.sprite_border_collision = function(block){
    Blockly.Galaxia.addImport("import_sprites", IMPORT_SPRITES);

    let s1 = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let border = block.getFieldValue("BORDER")

    return ["sprites.border_collision('"+border+"',"+s1+")", Blockly.Python.ORDER_ATOMIC]
}