Blockly.Galaxia.maqueen_init = function(){
    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_busio", IMPORT_BUSIO);

    Blockly.Galaxia.userSetups_["%maqueen_i2c"] = 'i2c = busio.I2C(board.P19, board.P20)'
    +NEWLINE// + "while not i2c.try_lock():"+NEWLINE + Blockly.Galaxia.INDENT + "pass";

};

Blockly.Galaxia.robots_galaxia_setMaqueenGo = function(block){
    Blockly.Galaxia.maqueen_init()

    var speed = Blockly.Galaxia.valueToCode(block, "SPEED", Blockly.Galaxia.ORDER_NONE) || "0";
    var dir = block.getFieldValue("DIR");
    if (speed > 255) speed = 255;
    if (speed < 0) speed = 0;
    return ""+
    "try:" + NEWLINE
    + Blockly.Galaxia.INDENT + "i2c.try_lock()" + NEWLINE 
    + Blockly.Galaxia.INDENT + "i2c.writeto(0x10, bytes([0x00, " + dir + ", " + speed + "]))" + NEWLINE
    + Blockly.Galaxia.INDENT + "i2c.writeto(0x10, bytes([0x02, " + dir + ", " + speed + "]))" + NEWLINE
    + Blockly.Galaxia.INDENT + "i2c.unlock()" + NEWLINE
    + "except OSError:" + NEWLINE
    + Blockly.Galaxia.INDENT + "print('Aucun Maqueen trouvé')"+ NEWLINE

};

Blockly.Galaxia.robots_galaxia_controlMaqueenMotor = function (block) {
    Blockly.Galaxia.maqueen_init()
    
    var speed = Blockly.Galaxia.valueToCode(block, "SPEED", Blockly.Galaxia.ORDER_NONE) || "0";
    if (speed > 255) speed = 255;
    if (speed < 0) speed = 0;
    return ""+
    "try:" + NEWLINE
    + Blockly.Galaxia.INDENT + "i2c.try_lock()" + NEWLINE 
    + Blockly.Galaxia.INDENT + "i2c.writeto(0x10, bytes([" + block.getFieldValue("MOTOR") + ", " + block.getFieldValue("DIR") + ", " + speed + "]))" + NEWLINE 
    + Blockly.Galaxia.INDENT + "i2c.unlock()" + NEWLINE
    + "except OSError:" + NEWLINE
    + Blockly.Galaxia.INDENT + "print('Aucun Maqueen trouvé')"+ NEWLINE
};

Blockly.Galaxia.robots_galaxia_stopMaqueenMotors = function (block) {
    Blockly.Galaxia.maqueen_init()
    
    if (block.getFieldValue("MOTOR") == "both") {
        return ""+
        "try:" + NEWLINE
        + Blockly.Galaxia.INDENT + "i2c.try_lock()" + NEWLINE 
        + Blockly.Galaxia.INDENT + "i2c.writeto(0x10, bytes([0x00, 0, 0]))" + NEWLINE 
        + Blockly.Galaxia.INDENT + "i2c.writeto(0x10, bytes([0x02, 0, 0]))" + NEWLINE 
        + Blockly.Galaxia.INDENT + "i2c.unlock()" + NEWLINE
        + "except OSError:" + NEWLINE
        + Blockly.Galaxia.INDENT + "print('Aucun Maqueen trouvé')"+ NEWLINE
    } else {
        return ""
        +"try:" + NEWLINE
        + Blockly.Galaxia.INDENT + "i2c.try_lock()" + NEWLINE 
        + Blockly.Galaxia.INDENT + "i2c.writeto(0x10, bytes([" + block.getFieldValue("MOTOR") + ", 0, 0]))" + NEWLINE
        + Blockly.Galaxia.INDENT + "i2c.unlock()" + NEWLINE
        + "except OSError:" + NEWLINE
        + Blockly.Galaxia.INDENT + "print('Aucun Maqueen trouvé')"+ NEWLINE
    }
};

Blockly.Galaxia.robots_galaxia_readMaqueenPatrol = function (block) {
    Blockly.Galaxia.io_digital_read()
    let pin = block.getFieldValue("PIN");

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_digitalio", IMPORT_DIGITALIO);

    let varName = "pin"+pin.substring(1);
    Blockly.Galaxia.userSetups_["io_pin_"+pin] = varName+" = []";
    
    return ["digital_read("+varName+", board."+pin+")", Blockly.Python.ORDER_ATOMIC]
};

Blockly.Galaxia.robots_galaxia_readMaqueenPlusPatrol = function (block) {
    Blockly.Galaxia.maqueen_init()

    Blockly.Python.userFunctions_["%thingz_maqueen_plus_patrol" ] = "def read_patrol(sensor):"+NEWLINE
    +Blockly.Galaxia.INDENT+"try:"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"i2c.try_lock()"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"status = bytearray(1)"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"i2c.writeto(0x10, bytes([0x1D]))"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"i2c.readfrom_into(0x10, status)"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"i2c.unlock()"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"if sensor == \"l1\":"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"return (status[0] & 0x8) == 0X8"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"elif sensor == \"l2\":"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"return (status[0] & 0x10) == 0X10"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"elif sensor == \"r1\":"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"return (status[0] & 0x2) == 0X2"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"elif sensor == \"r2\":"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"return (status[0] & 0x1) == 0X1"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"elif sensor == \"m\":"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"return (status[0] & 0x4) == 0X4 "+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"else:"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"return False"+NEWLINE
    +Blockly.Galaxia.INDENT+"except OSError:"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"print('Aucun Maqueen trouvé')"+NEWLINE
    +Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"return False"+NEWLINE
    
    let pin = block.getFieldValue("PIN");

    return ["read_patrol(\""+pin+"\")", Blockly.Python.ORDER_ATOMIC]
};

Blockly.Galaxia.robots_galaxia_readMaqueenUltrasound = function(block){
    let unit = block.getFieldValue("UNIT");
    Blockly.Galaxia.maqueen_init()
    Blockly.Galaxia.addImport("import_digitalio", IMPORT_DIGITALIO);
    Blockly.Galaxia.addImport("import_pulseio", IMPORT_PULSEIO);
    Blockly.Galaxia.addImport("import_time", IMPORT_TIME);

    Blockly.Galaxia.userSetups_["io_pin_1"] = "pin1 = digitalio.DigitalInOut(board.P1)"+NEWLINE+"pin1.direction = digitalio.Direction.OUTPUT";
    Blockly.Galaxia.userSetups_["io_pin_2"] = "pin2 = pulseio.PulseIn(board.P2)"+NEWLINE+"pin2.pause()";

    Blockly.Python.userFunctions_["%thingz_maqueen_ultrasound" ] = "def ultrasound(trigger, echo):"+NEWLINE+
    Blockly.Galaxia.INDENT+"echo.clear()"+NEWLINE+
    Blockly.Galaxia.INDENT+"echo.resume()"+NEWLINE+
    Blockly.Galaxia.INDENT+"trigger.value = True"+NEWLINE+
    Blockly.Galaxia.INDENT+"time.sleep(0.00001)"+NEWLINE+
    Blockly.Galaxia.INDENT+"trigger.value = False"+NEWLINE+
    Blockly.Galaxia.INDENT+"start = time.monotonic_ns()"+NEWLINE+
    Blockly.Galaxia.INDENT+"while not echo:"+NEWLINE+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"if time.monotonic_ns() - start > 1000000000:"+NEWLINE+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"echo.pause()"+NEWLINE+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"print('Distance: pas de réponse')"+NEWLINE+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"return 0"+NEWLINE+
    Blockly.Galaxia.INDENT+"distance = round(echo[0] * "+unit+" "+(unit == "0.017" ? ", 1)" : ")")+NEWLINE+
    Blockly.Galaxia.INDENT+"echo.pause()"+NEWLINE+
    Blockly.Galaxia.INDENT+"return distance"+NEWLINE

    return ["ultrasound(pin1, pin2)", Blockly.Python.ORDER_ATOMIC]

}

Blockly.Galaxia.robots_galaxia_readMaqueenPlusUltrasound = function(block){
    let unit = block.getFieldValue("UNIT");
    Blockly.Galaxia.maqueen_init()
    Blockly.Galaxia.addImport("import_digitalio", IMPORT_DIGITALIO);
    Blockly.Galaxia.addImport("import_pulseio", IMPORT_PULSEIO);
    Blockly.Galaxia.addImport("import_time", IMPORT_TIME);

    Blockly.Galaxia.userSetups_["io_pin_13"] = "pin13 = digitalio.DigitalInOut(board.P13)"+NEWLINE+"pin13.direction = digitalio.Direction.OUTPUT";
    Blockly.Galaxia.userSetups_["io_pin_14"] = "pin14 = pulseio.PulseIn(board.P14)"+NEWLINE+"pin14.pause()";

    Blockly.Python.userFunctions_["%thingz_maqueen_ultrasound" ] = "def ultrasound(trigger, echo):"+NEWLINE+
    Blockly.Galaxia.INDENT+"echo.clear()"+NEWLINE+
    Blockly.Galaxia.INDENT+"echo.resume()"+NEWLINE+
    Blockly.Galaxia.INDENT+"trigger.value = True"+NEWLINE+
    Blockly.Galaxia.INDENT+"time.sleep(0.00001)"+NEWLINE+
    Blockly.Galaxia.INDENT+"trigger.value = False"+NEWLINE+
    Blockly.Galaxia.INDENT+"start = time.monotonic_ns()"+NEWLINE+
    Blockly.Galaxia.INDENT+"while not echo:"+NEWLINE+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"if time.monotonic_ns() - start > 1000000000:"+NEWLINE+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"echo.pause()"+NEWLINE+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"print('Distance: pas de réponse')"+NEWLINE+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"return 0"+NEWLINE+
    Blockly.Galaxia.INDENT+"distance = round(echo[0] * "+unit+" "+(unit == "0.017" ? ", 1)" : ")")+NEWLINE+
    Blockly.Galaxia.INDENT+"echo.pause()"+NEWLINE+
    Blockly.Galaxia.INDENT+"return distance"+NEWLINE

    return ["ultrasound(pin13, pin14)", Blockly.Python.ORDER_ATOMIC]

}