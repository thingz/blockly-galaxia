Blockly.Galaxia.grove_galaxia_temperature = function (block) {
    Blockly.Galaxia.io_analog_read()
    let pin = block.getFieldValue("NUMBER");

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_analogio", IMPORT_ANALOGIO);
    Blockly.Galaxia.addImport('math', IMPORT_MATH);

    let varName = "pin"+pin.substring(1);
    Blockly.Galaxia.userSetups_["io_pin_"+pin] = varName+" = []";
    
    //Adjust temp by changing 273.15 to 263.15
    return ["round(1/(math.log((65535*5/3.3/analog_read("+varName+", board."+pin+")- 1))/4275+1/298.15)-263.15,1)", Blockly.Python.ORDER_ATOMIC]
};

Blockly.Galaxia.grove_galaxia_ultrasonic = function (block){
    // Blockly.Galaxia.io_digital_read()
    // Blockly.Galaxia.io_digital_write()
    let pin = block.getFieldValue("NUMBER");
    let unit = block.getFieldValue("UNIT");

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_pulseio", IMPORT_PULSEIO);
    Blockly.Galaxia.addImport("import_time", IMPORT_TIME);

    Blockly.Galaxia.codeFunctions_["grove_ultrasonic"] = "def ultrasonic(pin, pinNumber):\r\n"+
    Blockly.Galaxia.INDENT+"start = time.monotonic_ns()\r\n"+
    Blockly.Galaxia.INDENT+"pulses = pulseio.PulseIn(pinNumber)\r\n"+
    Blockly.Galaxia.INDENT+"pulses.clear()\r\n"+
    Blockly.Galaxia.INDENT+"pulses.resume(10)\r\n"+
    Blockly.Galaxia.INDENT+"while not pulses:\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"if time.monotonic_ns() - start > 1000000000:\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pulses.deinit()\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"print(\"Distance: pas de réponse\")\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"return 0\r\n"+
    Blockly.Galaxia.INDENT+"distance = round(pulses[0] * "+unit+" "+(unit == "0.017" ? ", 1)" : ")")+NEWLINE+
    Blockly.Galaxia.INDENT+"pulses.deinit()\r\n"+
    Blockly.Galaxia.INDENT+"return distance\r\n"

    let varName = "pin"+pin.substring(1);
    
    Blockly.Galaxia.userSetups_["ultrasonic_def"+pin] = varName+" = []";

    return ["ultrasonic("+varName+", board."+pin+")", Blockly.Python.ORDER_ATOMIC]    
}


Blockly.Galaxia.grove_galaxia_dht11 = function (block){

    let pin = block.getFieldValue("PIN");
    let value = block.getFieldValue("VALUE");

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_pulseio", IMPORT_PULSEIO);
    Blockly.Galaxia.addImport("import_time", IMPORT_TIME);
    Blockly.Galaxia.addImport("import_array", IMPORT_ARRAY);

    Blockly.Galaxia.codeFunctions_["grove_dht11"] = 'class DHT11:\r\n'+
    Blockly.Galaxia.INDENT+'__hiLevel = 51\r\n'+
    Blockly.Galaxia.INDENT+'def __init__(self, pin, trig_wait=18000, max_pulses=81):\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self._humidity = 0\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self._temperature = 0\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self._last_called = 0\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self._pin = pin\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self._max_pulses = max_pulses\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self._trig_wait = trig_wait\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self.pulse_in = pulseio.PulseIn(self._pin, maxlen=self._max_pulses, idle_state=True)\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self.pulse_in.pause()\r\n'+
    Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+'def _get_pulses_pulseio(self):\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'pulses = array.array("H")\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self.pulse_in.clear()\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self.pulse_in.resume(self._trig_wait)\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'time.sleep(0.25)\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self.pulse_in.pause()\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'while self.pulse_in:\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'pulses.append(self.pulse_in.popleft())\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'return pulses\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+'def _pulses_to_binary(self, pulses: array.array, start: int, stop: int):\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'binary = 0\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'hi_sig = False\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'for bit_inx in range(start, stop):\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'if hi_sig:\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'bit = 0\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'if pulses[bit_inx] > self.__hiLevel:\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'bit = 1\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'binary = binary << 1 | bit\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'hi_sig = not hi_sig\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'return binary\r\n'+
    Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+'def measure(self):\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'delay_between_readings = 2\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'if self._last_called == 0 or (time.monotonic() - self._last_called) > delay_between_readings:\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self._last_called = time.monotonic()\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'new_temperature = 0\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'new_humidity = 0\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'pulses = self._get_pulses_pulseio()\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'if len(pulses) < 10:\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'print("DHT11 - sensor not found, check wiring")\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'return\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'if len(pulses) < 80:\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'print("DHT11- A full buffer was not returned. Try again.")\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'return\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'buf = array.array("B")\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'for byte_start in range(0, 80, 16):\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'buf.append(self._pulses_to_binary(pulses, byte_start, byte_start + 16))\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'new_humidity = buf[0]\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'new_temperature = buf[2]\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'chk_sum = 0\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'for b in buf[0:4]:\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'chk_sum += b\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'if chk_sum & 0xFF != buf[4]:\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'print("DHT11 - bad checksum. Try again.")\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'return\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'if new_humidity < 0 or new_humidity > 100:\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'print("Received unplausible data. Try again.")\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self._temperature = new_temperature\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self._humidity = new_humidity\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'\r\n'+
    Blockly.Galaxia.INDENT+'@property\r\n'+
    Blockly.Galaxia.INDENT+'def temperature(self):\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self.measure()\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'return self._temperature\r\n'+
    Blockly.Galaxia.INDENT+'@property\r\n'+
    Blockly.Galaxia.INDENT+'def humidity(self):\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'self.measure()\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'return self._humidity'

    let varName = "dht11_"+pin.substring(1);
    
    Blockly.Galaxia.userSetups_["dht11_def"+pin] = varName+" = DHT11(board."+pin+")";

    return [varName+"."+value, Blockly.Python.ORDER_ATOMIC]    
}

Blockly.Galaxia.grove_galaxia_dht11_signal = function(block){
    let value = block.getFieldValue("VALUE");
    if(value == "temperature")
        return Blockly.Galaxia.map(Blockly.Galaxia.grove_galaxia_dht11(block), -20, 80, 0, 3.3)    
    return Blockly.Galaxia.map(Blockly.Galaxia.grove_galaxia_dht11(block), 0, 100, 0, 3.3)  
}

Blockly.Galaxia.grove_galaxia_temperature_signal = function(block){
    return Blockly.Galaxia.map(Blockly.Galaxia.grove_galaxia_temperature(block), -20, 80, 0, 3.3)    
}

Blockly.Galaxia.grove_galaxia_servo = function(block){
    let angle = Blockly.Galaxia.valueToCode(block, "ANGLE", Blockly.Galaxia.ORDER_NONE) || 90;
    let pin = block.getFieldValue("PIN");

    let pwm ="pwm"+pin

    if(angle < 0 ){
        angle = 0
    }else if(angle > 180){
        angle = 180
    }

    Blockly.Galaxia.userSetups_[pwm] = pwm+" = pwmio.PWMOut(board."+pin+", duty_cycle=3185, frequency=50)"

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_pwmio", IMPORT_PWMIO);

    Blockly.Galaxia.codeFunctions_["grove_set_angle"] = 'def set_angle(a, pwm):\r\n'+
	Blockly.Galaxia.INDENT+'duty = int(0.025*(2**16 -1) + (a*0.1*(2**16 -1))/180)\r\n'+
    Blockly.Galaxia.INDENT+'pwm.duty_cycle = duty\r\n'
    
    return 'set_angle('+angle+', '+pwm+')' + NEWLINE;
}

Blockly.Galaxia.grove_galaxia_ir_sender = function(block){
    let enable = block.getFieldValue("ENABLE");
    let pin = block.getFieldValue("PIN");

    let pwm ="pwm"+pin

    Blockly.Galaxia.userSetups_[pwm] = pwm+" = pwmio.PWMOut(board."+pin+", duty_cycle=0, frequency=38000)"

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_pwmio", IMPORT_PWMIO);
    
    return pwm+'.duty_cycle = ' +enable + NEWLINE;
}