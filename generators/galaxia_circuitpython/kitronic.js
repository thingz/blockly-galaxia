/**
 * @fileoverview Maqueen generators for Galaxia.
 */

// Blockly.GalaxiaGenerator.prototype.maqueen_init = function(){
//     return "print('/!\\ Maqueen non disponible sur ce firmware')" + NEWLINE

// };

Blockly.GalaxiaGenerator.prototype.kitronik_access_move = function(block){
    let dir = block.getFieldValue("DIR");
    let pin = "P0"
    let pwm ="pwm"+pin


    Blockly.Galaxia.userSetups_[pwm] = pwm+" = pwmio.PWMOut(board."+pin+", duty_cycle=3185, frequency=50)"

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_pwmio", IMPORT_PWMIO);

    Blockly.Galaxia.codeFunctions_["grove_set_angle"] = 'def set_angle(a, pwm):\r\n'+
	Blockly.Galaxia.INDENT+'duty = int(0.025*(2**16 -1) + (a*0.1*(2**16 -1))/180)\r\n'+
    Blockly.Galaxia.INDENT+'pwm.duty_cycle = duty\r\n'
    
    return 'set_angle('+dir+', '+pwm+')' + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.kitronik_access_sound = function (block) {
    let duration = block.getFieldValue("DURATION");
    let repeat = Blockly.Galaxia.valueToCode(block, "TIMES", Blockly.Galaxia.ORDER_NONE);

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_pwmio", IMPORT_PWMIO);
    Blockly.Galaxia.addImport("import_time", IMPORT_TIME);

    Blockly.Galaxia.codeFunctions_["kitronik_buzz"] = 'def kitronik_buzzer(pin, duration, repeat):\r\n'+
    Blockly.Galaxia.INDENT+'p = pwmio.PWMOut(pin, duty_cycle=0, frequency=440)\r\n'+
    Blockly.Galaxia.INDENT+'for i in range(repeat):\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'p.duty_cycle = 32768\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'time.sleep(duration)\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'p.duty_cycle = 0\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'if i < repeat -1:\r\n'+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'time.sleep(duration)\r\n'+
    Blockly.Galaxia.INDENT+"p.deinit()"

    return "kitronik_buzzer(board.P1, "+duration+", "+repeat+")\r\n"
    
};

Blockly.GalaxiaGenerator.prototype.kitronik_lamp_deadband = function (block) {
    let dead  = 0
    if(Blockly.Galaxia.userSetups_["kitronik_lamp_deadband"]){
        dead = Blockly.Galaxia.userSetups_["kitronik_lamp_deadband"].split(" = ")[1]
    }
    return [""+dead, Blockly.Python.ORDER_ATOMIC]

};

Blockly.GalaxiaGenerator.prototype.kitronik_lamp_read_light_level = function (block) {
    Blockly.Galaxia.io_analog_read()
    let pin = "P1";

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_analogio", IMPORT_ANALOGIO);
    Blockly.Galaxia.addImport("import_math", IMPORT_MATH);

    let varName = "pin"+pin.substring(1);
    Blockly.Galaxia.userSetups_["io_pin_"+pin] = varName+" = []";
    let code = "math.trunc((analog_read("+varName+", board."+pin+")/65535)*1023)"
    return [code, Blockly.Python.ORDER_ATOMIC]
};

Blockly.Galaxia.kitronik_lamp_set_light = function (block) {
    Blockly.Galaxia.io_digital_write()
    let state = block.getFieldValue("STATE");

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_digitalio", IMPORT_DIGITALIO);

    let varName = "pin0";
    Blockly.Galaxia.userSetups_["io_pin_P0"] = varName+" = []";

    return "digital_write("+varName+", board.P0, "+state+")\r\n"
};

Blockly.GalaxiaGenerator.prototype.kitronik_lamp_set_lamp_deadband = function(block){
    let dead = Blockly.Galaxia.valueToCode(block, "PERCENT", Blockly.Galaxia.ORDER_NONE);
    if( dead < 0 ){
        dead = 0;
    }else if (dead > 100){
        dead = 100
    }
    Blockly.Galaxia.userSetups_["kitronik_lamp_deadband"] = "kitronic_dead = "+dead
    return ""
}

Blockly.GalaxiaGenerator.prototype.kitronik_stop_set_traffic_light = function(block){
    let state = block.getFieldValue("STATE");
    let colors = {
        green: "False",
        orange: "False",
        red: "False"
    }
    switch(state){
        case "stop":
            colors["red"] = "True"
        break;
        case "get_ready":
            colors["red"] = "True"
            colors["orange"] = "True"
        break;
        case "go":
            colors["green"] = "True"
        break;
        case "ready_to_stop":
            colors["orange"] = "True"
        break;
    }
    Blockly.Galaxia.io_digital_write()

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_digitalio", IMPORT_DIGITALIO);

    let red = "pin0";
    let orange = "pin1";
    let green = "pin2";
    Blockly.Galaxia.userSetups_["io_pin_"+red] = red+" = []";
    Blockly.Galaxia.userSetups_["io_pin_"+orange] = orange+" = []";
    Blockly.Galaxia.userSetups_["io_pin_"+green] = green+" = []";

    let code = "digital_write("+red+", board.P0, "+colors["red"]+")"+ NEWLINE
    + "digital_write("+orange+", board.P1, "+colors["orange"]+")"+ NEWLINE
    + "digital_write("+green+", board.P2, "+colors["green"]+")"+ NEWLINE

    return code
}

Blockly.GalaxiaGenerator.prototype.kitronik_stop_set_color = function (block) {
    let state = block.getFieldValue("STATE");
    let color = block.getFieldValue("COLOR");

    Blockly.Galaxia.io_digital_write()

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_digitalio", IMPORT_DIGITALIO);

    let varName = "pin"+color;
    Blockly.Galaxia.userSetups_["io_pin_P"+color] = varName+" = []";

    return "digital_write("+varName+", board.P"+color+", "+state+")\r\n"

};
