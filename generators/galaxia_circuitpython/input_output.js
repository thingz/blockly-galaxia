Blockly.GalaxiaMicropython.io_galaxia_timestamp = function (block) {
    this.addImport("time", IMPORT_TIME);
    return ["time.monotonic()/" + block.getFieldValue("UNIT"), Blockly.Galaxia.ORDER_ATOMIC];
};

Blockly.Galaxia.io_configure_pin = function(){
    Blockly.Galaxia.codeFunctions_["io_configure_pin"] = "def configure_pin(pin, pinNumber, type, frequency=50):\r\n"+
    Blockly.Galaxia.INDENT+"if type == \"digital_write\":\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"if len(pin) and isinstance(pin[0], digitalio.DigitalInOut):\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin[0].direction = digitalio.Direction.OUTPUT\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"else:\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"if len(pin):\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin[0].deinit()\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin[0] = digitalio.DigitalInOut(pinNumber)\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin[0].direction = digitalio.Direction.OUTPUT\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"else:\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin.append(digitalio.DigitalInOut(pinNumber))\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin[0].direction = digitalio.Direction.OUTPUT\r\n"+
    Blockly.Galaxia.INDENT+"elif type == \"digital_read\":\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"if len(pin) and isinstance(pin[0], digitalio.DigitalInOut):\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin[0].direction = digitalio.Direction.INPUT\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"else:\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"if len(pin):\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin[0].deinit()\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin[0] = digitalio.DigitalInOut(pinNumber)\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin[0].direction = digitalio.Direction.INPUT\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"else:\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin.append(digitalio.DigitalInOut(pinNumber))\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin[0].direction = digitalio.Direction.INPUT\r\n"+
    Blockly.Galaxia.INDENT+"elif type == \"analog_read\":\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"if len(pin) == 0 or not isinstance(pin[0], analogio.AnalogIn):\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"if len(pin):\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin[0].deinit()\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin[0] = analogio.AnalogIn(pinNumber)\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"else:\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin.append(analogio.AnalogIn(pinNumber))\r\n"+
    Blockly.Galaxia.INDENT+"elif type == \"pwm\":\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"if len(pin) == 0 or not isinstance(pin[0], pwmio.PWMOut) or pin[0].frequency != frequency:\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"if len(pin):\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin[0].deinit()\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin[0] = pwmio.PWMOut(pinNumber, frequency=frequency)\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"else:\r\n"+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+"pin.append(pwmio.PWMOut(pinNumber, frequency=frequency))\r\n"+
    Blockly.Galaxia.INDENT+"return pin\r\n"

}

Blockly.Galaxia.io_analog_read = function(){
    Blockly.Galaxia.io_configure_pin()
    Blockly.Galaxia.codeFunctions_["io_analog_read"] = "def analog_read(pin, pinNumber):\r\n"+
    Blockly.Galaxia.INDENT+"pin = configure_pin(pin, pinNumber, \"analog_read\")\r\n"+
    Blockly.Galaxia.INDENT+"return pin[0].value\r\n"
}

Blockly.Galaxia.io_digital_read = function(){
    Blockly.Galaxia.io_configure_pin()
    Blockly.Galaxia.codeFunctions_["io_digital_read"] = "def digital_read(pin, pinNumber):\r\n"+
    Blockly.Galaxia.INDENT+"pin = configure_pin(pin, pinNumber, \"digital_read\")\r\n"+
    Blockly.Galaxia.INDENT+"return pin[0].value\r\n"
}

Blockly.Galaxia.io_digital_write = function(){
    Blockly.Galaxia.io_configure_pin()
    Blockly.Galaxia.codeFunctions_["io_digital_write"] = "def digital_write(pin, pinNumber, state):\r\n"+
    Blockly.Galaxia.INDENT+"pin = configure_pin(pin, pinNumber, \"digital_write\")\r\n"+
    Blockly.Galaxia.INDENT+"pin[0].value = state\r\n"
}

Blockly.Galaxia.io_pwm = function(){
    Blockly.Galaxia.io_configure_pin()
    Blockly.Galaxia.codeFunctions_["io_pwm"] = "def pwm(pin, pinNumber, duty, frequency):\r\n"+
    Blockly.Galaxia.INDENT+"pin = configure_pin(pin, pinNumber, \"pwm\", frequency=frequency)\r\n"+
    Blockly.Galaxia.INDENT+"pin[0].duty_cycle = duty\r\n"
}

Blockly.Galaxia.io_galaxia_readDigitalPin = function(block){
    Blockly.Galaxia.io_digital_read()
    let pin = block.getFieldValue("PIN");

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_digitalio", IMPORT_DIGITALIO);

    let varName = "pin"+pin.substring(1);
    Blockly.Galaxia.userSetups_["io_pin_"+pin] = varName+" = []";
    
    return ["digital_read("+varName+", board."+pin+")", Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.io_galaxia_writeDigitalPin = function(block){
    Blockly.Galaxia.io_digital_write()
    let pin = block.getFieldValue("PIN");
    let state = Blockly.Galaxia.valueToCode(block, "STATE", Blockly.Galaxia.ORDER_NONE);

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_digitalio", IMPORT_DIGITALIO);

    let varName = "pin"+pin.substring(1);
    Blockly.Galaxia.userSetups_["io_pin_"+pin] = varName+" = []";

    return "digital_write("+varName+", board."+pin+", "+state+")\r\n"
}

Blockly.Galaxia.io_galaxia_readAnalogPin = function(block){
    Blockly.Galaxia.io_analog_read()
    let pin = block.getFieldValue("PIN");

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_analogio", IMPORT_ANALOGIO);

    let varName = "pin"+pin.substring(1);
    Blockly.Galaxia.userSetups_["io_pin_"+pin] = varName+" = []";
    
    return ["analog_read("+varName+", board."+pin+")", Blockly.Python.ORDER_ATOMIC]
}

Blockly.Galaxia.io_galaxia_pwm = function(block){
    Blockly.Galaxia.io_pwm()
    let pin = block.getFieldValue("PIN");
    let duty = Blockly.Galaxia.valueToCode(block, "DUTY", Blockly.Galaxia.ORDER_NONE);

    duty = Math.trunc(65535 *(duty/100))

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_pwmio", IMPORT_PWMIO);

    let varName = "pin"+pin.substring(1);
    Blockly.Galaxia.userSetups_["io_pin_"+pin] = varName+" = []";

    return "pwm("+varName+", board."+pin+", "+duty+", 50)\r\n"
}
Blockly.Galaxia.io_galaxia_pwm_with_freq = function(block){
    Blockly.Galaxia.io_pwm()
    let pin = block.getFieldValue("PIN");
    let duty = Blockly.Galaxia.valueToCode(block, "DUTY", Blockly.Galaxia.ORDER_NONE);
    let freq = Blockly.Galaxia.valueToCode(block, "FREQ", Blockly.Galaxia.ORDER_NONE);

    duty = Math.trunc(65535 *(duty/100))

    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_pwmio", IMPORT_PWMIO);

    let varName = "pin"+pin.substring(1);
    Blockly.Galaxia.userSetups_["io_pin_"+pin] = varName+" = []";

    return "pwm("+varName+", board."+pin+", "+duty+", "+freq+")\r\n"
}