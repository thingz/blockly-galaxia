Blockly.Galaxia.time_galaxia_timer_start = function (block) {
    this.addImport("time", IMPORT_TIME);
    this.addConstant("galaxia_chronometer", GALAXIA_CHRONOMETER);
    
    return "galaxiaChronometer = time.monotonic_ns()" + NEWLINE;
};

Blockly.Galaxia.time_galaxia_timer_measure = function (block) {
    this.addImport("time", IMPORT_TIME);
    this.addConstant("galaxia_chronometer", GALAXIA_CHRONOMETER);
    
    var divider = block.getFieldValue("UNIT");
    return ["int((time.monotonic_ns() - galaxiaChronometer)/"+divider+")", Blockly.Python.ORDER_ATOMIC];
};