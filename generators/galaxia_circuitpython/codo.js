/**
 * @fileoverview Maqueen generators for Galaxia.
 */

Blockly.Galaxia.codo_init = function(){
    Blockly.Galaxia.addImport("import_board", IMPORT_BOARD);
    Blockly.Galaxia.addImport("import_pwmio", IMPORT_PWMIO);

    Blockly.Galaxia.userSetups_["%codo_init"] = 'codo = []'+NEWLINE+
    'codo.append(pwmio.PWMOut(board.P15, frequency=5000, duty_cycle=0))'+NEWLINE+
    'codo.append(pwmio.PWMOut(board.P16, frequency=5000, duty_cycle=0))'+NEWLINE+
    'codo.append(pwmio.PWMOut(board.P13, frequency=5000, duty_cycle=0))'+NEWLINE+
    'codo.append(pwmio.PWMOut(board.P14, frequency=5000, duty_cycle=0))'+NEWLINE+NEWLINE+
    'def codo_set_motor(motor, direction, speed):'+NEWLINE+Blockly.Galaxia.INDENT+
        'global codo'+NEWLINE+
    Blockly.Galaxia.INDENT+'index = 0'+NEWLINE+
    Blockly.Galaxia.INDENT+'if speed < 0:'+NEWLINE+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'speed = 0'+NEWLINE+
    Blockly.Galaxia.INDENT+'elif speed > 100:'+NEWLINE+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'speed = 100'+NEWLINE+
    Blockly.Galaxia.INDENT+'if motor == "left":'+NEWLINE+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'index = 2'+NEWLINE+
    Blockly.Galaxia.INDENT+'if direction == "forward":'+NEWLINE+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'codo[index].duty_cycle = 0'+NEWLINE+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'codo[index+1].duty_cycle = int(65535*(speed/100))'+NEWLINE+
    Blockly.Galaxia.INDENT+'else:'+NEWLINE+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'codo[index].duty_cycle = int(65535*(speed/100))'+NEWLINE+
    Blockly.Galaxia.INDENT+Blockly.Galaxia.INDENT+'codo[index+1].duty_cycle = 0'+NEWLINE+
            
    'def codo_move(direction, speed):'+NEWLINE+
    Blockly.Galaxia.INDENT+'codo_set_motor("left", direction, speed)'+NEWLINE+
    Blockly.Galaxia.INDENT+'codo_set_motor("right", direction, speed)'+NEWLINE

};

Blockly.Galaxia.robots_galaxia_setCodoGo = function(block){
    Blockly.Galaxia.codo_init()

    var speed = Blockly.Galaxia.valueToCode(block, "SPEED", Blockly.Galaxia.ORDER_NONE) || "0";
    var dir = block.getFieldValue("DIR");
    
    return 'codo_move("'+dir+'", '+speed+')'+NEWLINE
};

Blockly.Galaxia.robots_galaxia_controlCodoMotor = function (block) {
    Blockly.Galaxia.codo_init()
    
    var motor = block.getFieldValue("MOTOR");
    var speed = Blockly.Galaxia.valueToCode(block, "SPEED", Blockly.Galaxia.ORDER_NONE) || "0";
    var dir = block.getFieldValue("DIR");
    
    return 'codo_set_motor("'+motor+'","'+dir+'", '+speed+')'+NEWLINE
};

Blockly.Galaxia.robots_galaxia_stopCodoMotors = function (block) {
    Blockly.Galaxia.codo_init()
    
    if (block.getFieldValue("MOTOR") == "both") {
        return 'codo_set_motor("left", "forward", 0)'+NEWLINE+'codo_set_motor("right", "forward", 0)'+ NEWLINE
    } else {
        return 'codo_set_motor('+block.getFieldValue("MOTOR")+', "forward", 0)' + NEWLINE;
    }
};