/**
 * @fileoverview Display generators for Micro:bit.
 */

// Micro:bit screen

Blockly.Python.show_leds = function (block) {
    const BRIGHTNESS = 9;
    var image = "";
    for (var row = 0; row < 5; row++) {
        for (var column = 0; column < 5; column++) {
            var label = "LED" + row + "" + column;
            image += (block.getFieldValue(label, Blockly.Python.ORDER_MEMBER) === "TRUE") ? BRIGHTNESS : "0";
        }
        image += (row < 4) ? ":" : "";
    }
    return "led_image = Image('" + image + "')" + NEWLINE + "display.show(led_image)" + NEWLINE;
};

Blockly.Python.show_number = function (block) {
    var number = Blockly.Python.valueToCode(block, "VALUE", Blockly.Python.ORDER_NONE) || "0";
    return "display.show(str(" + number + "))" + NEWLINE;
};

Blockly.Python.show_string = function (block) {
    var text = Blockly.Python.valueToCode(block, "TEXT", Blockly.Python.ORDER_NONE) || "''";
    let inputBlock = block.getInput("TEXT").connection.targetBlock();
    if (inputBlock && (inputBlock.type == "text" || inputBlock.type == "text_join")) {
        return "display.scroll(" + text + ")" + NEWLINE;
    } else {
        return "display.scroll(str(" + text + "))" + NEWLINE;
    }
};

Blockly.Python.show_icon = function (block) {
    var icon = block.getFieldValue("ICON") || "NO";
    return "display.show(Image." + icon + ")" + NEWLINE;
};

Blockly.Python.show_clock = function (block) {
    Blockly.Python.addFunction('showClock', DEF_MICROBIT_SHOW_CLOCK);
    var hour = Blockly.Python.valueToCode(block, "CLOCK", Blockly.Python.ORDER_NONE) || "0";
    return "showClock(" + hour + ")" + NEWLINE;
};

Blockly.Python.show_arrow = function (block) {
    var dir = block.getFieldValue("ARROW") || "NO";
    return "display.show(Image.ARROW_" + dir + ")" + NEWLINE;
};

Blockly.Python.show_gauge = function (block) {
    Blockly.Python.addFunction('plotBarGraph', DEF_MICROBIT_LED_GAUGE);
    var value = Blockly.Python.valueToCode(block, "VALUE", Blockly.Python.ORDER_NONE) || "0";
    var max = Blockly.Python.valueToCode(block, "MAX", Blockly.Python.ORDER_NONE) || "0";
    return "plotBarGraph(" + value + ", " + max + ")" + NEWLINE;
};

Blockly.Python.set_pixel = function (block) {
    var x = Blockly.Python.valueToCode(block, "X", Blockly.Python.ORDER_NONE) || "0";
    if (x < 0) x = 0;
    if (x > 5) x = 5;
    var y = Blockly.Python.valueToCode(block, "Y", Blockly.Python.ORDER_NONE) || "0";
    if (y < 0) y = 0;
    if (y > 5) y = 5;
    var state = Blockly.Python.valueToCode(block, "STATE", Blockly.Python.ORDER_NONE) || "0";
    return "display.set_pixel(" + x + "," + y + ", 9*" + state + ")" + NEWLINE;
};

Blockly.Python.set_light_pixel = function (block) {
    var x = Blockly.Python.valueToCode(block, "X", Blockly.Python.ORDER_NONE) || "0";
    if (x < 0) x = 0;
    if (x > 5) x = 5;
    var y = Blockly.Python.valueToCode(block, "Y", Blockly.Python.ORDER_NONE) || "0";
    if (y < 0) y = 0;
    if (y > 5) y = 5;
    var light = Blockly.Python.valueToCode(block, "LIGHT", Blockly.Python.ORDER_NONE) || "0";
    if (light < 0) light = 0;
    if (light > 9) light = 9;
    return "display.set_pixel(" + x + ',' + y + ',' + light + ')' + NEWLINE;
};

Blockly.Python.clear = function () {
    return "display.clear()" + NEWLINE;
};

// Screens

Blockly.Python.display_lcdSetText = function (block) {
    Blockly.Python.addImport('lcd1602', IMPORT_LCD1602);
    Blockly.Python.addInit('lcd1602', "lcd = LCD1602()");
    let txt = Blockly.Python.valueToCode(block, "TEXT", Blockly.Python.ORDER_NONE) || "''";
    let line = block.getFieldValue("LINE");
    let inputBlock = block.getInput("TEXT").connection.targetBlock();
    if (inputBlock && (inputBlock.type == "text" || inputBlock.type == "text_join")) {
        return "lcd.setCursor(0," + line + ")" + NEWLINE + "lcd.writeTxt(" + txt + ")" + NEWLINE;
    } else {
        return "lcd.setCursor(0," + line + ")" + NEWLINE + "lcd.writeTxt(str(" + txt + "))" + NEWLINE;
    }
};

Blockly.Python.display_lcdClear = function (block) {
    Blockly.Python.addImport('lcd1602', IMPORT_LCD1602);
    Blockly.Python.addInit('lcd1602', "lcd = LCD1602()");
    return "lcd.clear()" + NEWLINE;
};

Blockly.Python.display_addOledText = function (block) {
    Blockly.Python.addImport('oled', IMPORT_OLED);
    Blockly.Python.addInit('oled', "oled = OLED()");
    let str = Blockly.Python.valueToCode(block, "TEXT", Blockly.Python.ORDER_NONE) || "''";
    let x = Blockly.Python.valueToCode(block, "X", Blockly.Python.ORDER_NONE) || "0";
    if (x > 11) x = 11;
    if (x < 0) x = 0;
    let y = Blockly.Python.valueToCode(block, "Y", Blockly.Python.ORDER_NONE) || "0";
    if (y > 3) y = 3;
    if (y < 0) y = 0;
    return "oled.addTxt(" + x + ", " + y + ", " + str + ")" + NEWLINE;
};

Blockly.Python.display_setOledPixel = function (block) {
    Blockly.Python.addImport('oled', IMPORT_OLED);
    Blockly.Python.addInit('oled', "oled = OLED()");
    let x = Blockly.Python.valueToCode(block, "X", Blockly.Python.ORDER_NONE) || "0";
    if (x > 63) x = 63;
    if (x < 0) x = 0;
    let y = Blockly.Python.valueToCode(block, "Y", Blockly.Python.ORDER_NONE) || "0";
    if (y > 31) y = 31;
    if (y < 0) y = 0;
    let state = Blockly.Python.valueToCode(block, "STATE", Blockly.Python.ORDER_NONE) || "0";
    return "oled.set_px(" + x + ", " + y + ", " + state + ")" + NEWLINE;
};

Blockly.Python.display_showOledIcon = function (block) {
    Blockly.Python.addImport('oled', IMPORT_OLED);
    Blockly.Python.addInit('oled', "oled = OLED()");
    let i = block.getFieldValue("ICON") || "NO";
    let x = Blockly.Python.valueToCode(block, "X", Blockly.Python.ORDER_NONE) || "0";
    if (x > 59) x = 59;
    if (x < 0) x = 0;
    let y = Blockly.Python.valueToCode(block, "Y", Blockly.Python.ORDER_NONE) || "0";
    if (y > 31) y = 31;
    if (y < 0) y = 0;
    let state = Blockly.Python.valueToCode(block, "STATE", Blockly.Python.ORDER_NONE) || "0";
    Blockly.Python.addPowerOn('stamp_' + i, "STAMP_" + i + " = oled.create_stamp(Image." + i + ")")
    return "oled.draw_stamp(" + x + ', ' + y + ', ' + "STAMP_" + i + ", s=" + state + ")" + NEWLINE;
};

Blockly.Python.display_clearOledScreen = function () {
    Blockly.Python.addImport('oled', IMPORT_OLED);
    Blockly.Python.addInit('oled', "oled = OLED()");
    return "oled.clear()" + NEWLINE;
};

Blockly.Python.display_setGroveSocketLed = function (block) {
    let pin = block.getFieldValue("PIN");
    let state = Blockly.Python.valueToCode(block, "STATE", Blockly.Python.ORDER_NONE) || "0";
    Blockly.Python.addInit('socket_led_module_' + pin, "# Socket LED on " + pin);
    return pin + ".write_digital(" + state + ")" + NEWLINE;
};

Blockly.Python.display_setLEDintensity = function (block) {
    let pin = block.getFieldValue("PIN");
    let value = Blockly.Python.valueToCode(block, "VALUE", Blockly.Python.ORDER_NONE) || "0";
    Blockly.Python.addInit('intensity_led_module_' + pin, "# Intensity LED on " + pin);
    return pin + ".write_analog(" + value + ");" + NEWLINE;
};

/**
 * This is not a block generator, it's util function used in all neopixel blocks.
 * Add neopixel init code.
 * @param {int} numPin
 * @param {int} ledCount
 */
Blockly.Python.neopixel_codeInitialization = function (pin, ledCount) {
    let numPin = pin.substr(3);
    Blockly.Python.addImport('neopixel', IMPORT_NEOPIXEL);
    Blockly.Python.addConstant('led_count_' + numPin, 'LED_COUNT_NP' + numPin + " = " + ledCount);
    Blockly.Python.addInit('neopixel_' + numPin, "np_" + numPin + " = neopixel.NeoPixel(" + pin + ", LED_COUNT_NP" + numPin + ")");
};

/**
 * This is not a block generator, it's util function used in all neopixel blocks.
 * Check if neopixel is already defined on given pin.
 * @return {bool} alreadyDefined
 */
Blockly.Python.neopixel_checkDefinedBlock = function (pin) {
    let defineBlocks = Blockly.Python.getAllBlocksByType('display_defineNeopixel');
    var alreadyDefined = false;
    for (block in defineBlocks) {
        let fieldDropdownPin = defineBlocks[block].getField('PIN');
        let selectedOption =  fieldDropdownPin.selectedOption_[1];
        if (selectedOption == pin && !defineBlocks[block].disabled) {
            alreadyDefined = true;
        }
    }
    return alreadyDefined;
};

Blockly.Python.display_defineNeopixel = function (block) {
    let ledCount = block.getFieldValue("N");
    let pin = block.getFieldValue("PIN");
    Blockly.Python.neopixel_codeInitialization(pin, ledCount);
    return "";
};

Blockly.Python.display_controlNeopixelLed = function (block) {
    let pin = block.getFieldValue("PIN");
    var led = Blockly.Python.valueToCode(block, "LED", Blockly.Python.ORDER_NONE) || "0";
    var r = Blockly.Python.valueToCode(block, "R", Blockly.Python.ORDER_NONE) || "0";
    var g = Blockly.Python.valueToCode(block, "G", Blockly.Python.ORDER_NONE) || "0";
    var b = Blockly.Python.valueToCode(block, "B", Blockly.Python.ORDER_NONE) || "0";
    if (!Blockly.Python.neopixel_checkDefinedBlock(pin)) {
        Blockly.Python.neopixel_codeInitialization(pin, "30");
    }
    return "np_" + pin.substr(3) + "[" + led + "] = (" + r + ", " + g + ", " + b + ")" + NEWLINE + "np_" + pin.substr(3) + ".show()" + NEWLINE;
};

Blockly.Python.display_controlColorNeopixelLed = function (block) {
    let pin = block.getFieldValue("PIN");
    let ledIndex = Blockly.Python.valueToCode(block, "LED", Blockly.Python.ORDER_NONE) || "0";
    let colour = Blockly.Python.valueToCode(block, "COLOR", Blockly.Python.ORDER_NONE) || "(0,0,0)";
    Blockly.Python.neopixel_codeInitialization(pin, "30");
    return "np_" + pin.substr(3) + "[" + ledIndex + "] = " + colour + NEWLINE + "np_" + pin.substr(3) + ".show()" + NEWLINE;
};

Blockly.Python.display_neopixel_controlAllLedRGB = function (block) {
    let pin = block.getFieldValue("PIN");
    var r = Blockly.Python.valueToCode(block, "R", Blockly.Python.ORDER_NONE) || "0";
    var g = Blockly.Python.valueToCode(block, "G", Blockly.Python.ORDER_NONE) || "0";
    var b = Blockly.Python.valueToCode(block, "B", Blockly.Python.ORDER_NONE) || "0";
    if (!Blockly.Python.neopixel_checkDefinedBlock(pin)) {
        Blockly.Python.neopixel_codeInitialization(pin, "30");
    }
    Blockly.Python.addFunction('neopixel_showAllLed', DEF_NEOPIXEL_SHOW_ALL_LED);
    return "neopixel_showAllLed(np_" + pin.substr(3) + ", LED_COUNT_NP" + pin.substr(3) + ", " + r + ", " + g + ", " + b + ")" + NEWLINE;
};

Blockly.Python.display_neopixel_controlAllLedPalette = function (block) {
    let pin = block.getFieldValue("PIN");
    let colour = Blockly.Python.valueToCode(block, "COLOR", Blockly.Python.ORDER_NONE) || "(0,0,0)";
    let colourList = colour.match(/([0-9]{1,3})/g)
    if (!Blockly.Python.neopixel_checkDefinedBlock(pin)) {
        Blockly.Python.neopixel_codeInitialization(pin, "30");
    }
    Blockly.Python.addFunction('neopixel_showAllLed', DEF_NEOPIXEL_SHOW_ALL_LED);
    return "neopixel_showAllLed(np_" + pin.substr(3) + ", LED_COUNT_NP" + pin.substr(3) + ", " + colourList[0] + ", " + colourList[1] + ", " + colourList[2] + ")" + NEWLINE;
};

Blockly.Python.display_rainbowNeopixel = function (block) {
    let pin = block.getFieldValue("PIN");
    if (!Blockly.Python.neopixel_checkDefinedBlock(pin)) {
        Blockly.Python.neopixel_codeInitialization(pin, "30");
    }
    Blockly.Python.addFunction('neopixel_showAllLed', DEF_NEOPIXEL_SHOW_ALL_LED);
    Blockly.Python.addFunction('neopixel_rainbow', DEF_NEOPIXEL_RAINBOW);
    return "neopixel_rainbow(np_" + pin.substr(3) + ", LED_COUNT_NP" + pin.substr(3) + ")" + NEWLINE;
};

Blockly.Python.display_setNumberGrove4Digit = function (block) {
    let pinCLK = block.getFieldValue("CLK");
    let pinDIO = block.getFieldValue("DIO");
    Blockly.Python.addConstant('const_ascii_num', DEF_4DIGIT_ASCII_NUMERIC_TABLE);
    Blockly.Python.addInit('tm1637_functions', DEF_4DIGIT_INIT);
    Blockly.Python.addInit('tm1637_encodeNum', DEF_4DIGIT_ENCODE_NUM);
    Blockly.Python.addInit('tm1637', "tm1637_init(" + pinCLK + ", " + pinDIO + ")");
    var n = Blockly.Python.valueToCode(block, "N", Blockly.Python.ORDER_NONE) || "0";
    var displayOption = block.getFieldValue("SHOW")
    switch (displayOption) {
        case "NUM":
            Blockly.Python.addFunction('tm1637_setNumber', DEF_4DIGIT_SET_NUMBER);
            return "tm1637_setNumber(" + pinCLK + ", " + pinDIO + ", " + n + ")" + NEWLINE;
        case "TEMP":
            Blockly.Python.addFunction('tm1637_setTemp', DEF_4DIGIT_SET_TEMP);
            return "tm1637_setTemp(" + pinCLK + ", " + pinDIO + ", int(" + n + "))" + NEWLINE;
        default:
            throw Error("Unhandled display option: " + displayOption);
    }
};

Blockly.Python.display_setClockGrove4Digit = function (block) {
    var date = new Date();
    var pinCLK = block.getFieldValue("CLK");
    var pinDIO = block.getFieldValue("DIO");
    Blockly.Python.addConstant('chronometer', "chrono0 = 0");
    Blockly.Python.addConstant('time_read',
        "#Warning, the clock is recovered by browser when micro:bit is flashed." + NEWLINE +
        "#If micro:bit is powered off, time will not flow." + NEWLINE +
        "MIN_START = " + date.getMinutes() + NEWLINE +
        "HOUR_START = " + date.getHours());
    Blockly.Python.addConstant('const_ascii_num', DEF_4DIGIT_ASCII_NUMERIC_TABLE);
    Blockly.Python.addInit('tm1637_functions', DEF_4DIGIT_INIT);
    Blockly.Python.addInit('tm1637_encodeNum', DEF_4DIGIT_ENCODE_NUM);
    Blockly.Python.addInit('tm1637_setClock', DEF_4DIGIT_SET_CLOCK);
    Blockly.Python.addInit('tm1637_getTime', DEF_4DIGIT_GET_TIME);
    Blockly.Python.addInit('tm1637_init', "tm1637_init(" + pinCLK + ", " + pinDIO + ")");
    return "tm1637_setClock(" + pinCLK + ", " + pinDIO + ", tm1637_getTime()[0], tm1637_getTime()[1])" + NEWLINE;
};

Blockly.Python.display_setLevelLedBar = function (block) {
    Blockly.Python.addFunction('ledBar_latch', DEF_LEDBAR_INIT);
    Blockly.Python.addFunction('ledBar_setLevel', DEF_LEDBAR_LEVEL);
    let val = Blockly.Python.valueToCode(block, "VALUE", Blockly.Python.ORDER_NONE) || "0";
    if (val > 10) val = 10;
    if (val < 0) val = 0;
    return "ledBar_setLevel(" + block.getFieldValue("DI") + ", " + block.getFieldValue("DCKI") + ", " + val + ")" + NEWLINE;
};

Blockly.Python.display_setTrafficLight = function (block) {
    let state = Blockly.Python.valueToCode(block, "STATE", Blockly.Python.ORDER_NONE) || "0";
    Blockly.Python.addInit('traffic_light_module_', "# Traffic Light");
    return block.getFieldValue("PIN") + ".write_digital(" + state + ")" + NEWLINE;
};

// Morpion

// Functions used in morpion blocks for generators
Blockly.Python.initializeMorpionGenerator = function () {
    Blockly.Python.addImport('oledm', IMPORT_OLEDM);
    Blockly.Python.addImport('morpion', IMPORT_MORPION);
    Blockly.Python.addInit('oledm', "oled = OLEDM()");
    Blockly.Python.addInit('morpion', "morpion = MORPION(oled._s)");
};

Blockly.Python.display_morpionNewGame = function () {
    Blockly.Python.initializeMorpionGenerator();
    return "morpion.newGame()" + NEWLINE;
};

Blockly.Python.display_morpionMoveCursor = function () {
    Blockly.Python.initializeMorpionGenerator();
    return "morpion.mvCursor()" + NEWLINE;
};

Blockly.Python.display_morpionSetPlayerFigure = function (block) {
    Blockly.Python.initializeMorpionGenerator();
    let fig = block.getFieldValue("FIGURE")
    if (fig == "X") {
        return "morpion.addCross()" + NEWLINE;
    }
    if (fig == "O") {
        return "morpion.addCircle()" + NEWLINE;
    }
};

Blockly.Python.display_morpionIsEndGame = function () {
    Blockly.Python.initializeMorpionGenerator();
    return ["morpion.endGame()", Blockly.Python.ORDER_ATOMIC];
};

// Games

// Functions used in morpion blocks for generators
Blockly.Python.initializeGamesGenerator = function () {
    Blockly.Python.addImport('game', IMPORT_GAME);
    Blockly.Python.addInit('myGame', "myGame = GAME()");
};

Blockly.Python.display_games_createSprite = function (block) {
    Blockly.Python.initializeGamesGenerator();
    let x = Blockly.Python.valueToCode(block, "X", Blockly.Python.ORDER_NONE) || "0";
    let y = Blockly.Python.valueToCode(block, "Y", Blockly.Python.ORDER_NONE) || "0";
    return ["myGame.createSprite(" + x + "," + y + ")", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.display_games_deleteSprite = function (block) {
    Blockly.Python.initializeGamesGenerator();
    var spriteVar = Blockly.Python.valueToCode(block, "SPRITE", Blockly.Python.ORDER_MEMBER) || "''";
    return spriteVar + ".delete()" + NEWLINE;
};

Blockly.Python.display_games_isSpriteDeleted = function (block) {
    Blockly.Python.initializeGamesGenerator();
    var spriteVar = Blockly.Python.valueToCode(block, "SPRITE", Blockly.Python.ORDER_MEMBER) || "''";
    return [spriteVar + ".deleted", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.display_games_moveSprite = function (block) {
    Blockly.Python.initializeGamesGenerator();
    var spriteVar = Blockly.Python.valueToCode(block, "SPRITE", Blockly.Python.ORDER_MEMBER) || "''";
    let step = Blockly.Python.valueToCode(block, "STEP", Blockly.Python.ORDER_MEMBER) || "0";
    let dir = block.getFieldValue("DIR")
    switch (dir) {
        case "LEFT":
            return spriteVar + ".move('left', " + step + ")" + NEWLINE;
        case "RIGHT":
            return spriteVar + ".move('right', " + step + ")" + NEWLINE;
        case "UP":
            return spriteVar + ".move('up', " + step + ")" + NEWLINE;
        case "DOWN":
            return spriteVar + ".move('down', " + step + ")" + NEWLINE;
        default:
            throw Error("Unhandled move option: " + dir);
    }
};

Blockly.Python.display_games_getSpritePosition = function (block) {
    Blockly.Python.initializeGamesGenerator();
    var spriteVar = Blockly.Python.valueToCode(block, "SPRITE", Blockly.Python.ORDER_MEMBER) || "''";
    return [spriteVar + "." + block.getFieldValue("POS"), Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.display_games_changeScore = function (block) {
    Blockly.Python.initializeGamesGenerator();
    var n = Blockly.Python.valueToCode(block, "N", Blockly.Python.ORDER_MEMBER) || "0";
    return "myGame.changeScore(" + n + ")" + NEWLINE;
};

Blockly.Python.display_games_getScore = function () {
    Blockly.Python.initializeGamesGenerator();
    return ["myGame.score", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.display_games_stopGame = function () {
    Blockly.Python.initializeGamesGenerator();
    return "myGame.stopGame()" + NEWLINE;
};

Blockly.Python.display_games_isEndGame = function () {
    Blockly.Python.initializeGamesGenerator();
    return ["myGame.endGame", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.display_games_restartGame = function () {
    Blockly.Python.initializeGamesGenerator();
    return "myGame.startGame()" + NEWLINE;
};