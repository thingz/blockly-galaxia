/**
 * @fileoverview Sensors generators for Micro:bit.
 */

// Micro:bit board sensors

Blockly.Python.sensors_getAcceleration = function (block) {
    let axis = block.getFieldValue("AXIS");
    if (axis !== "strength") {
        return ["accelerometer.get_" + axis + "()", Blockly.Python.ORDER_ATOMIC];
    } else {
        Blockly.Python.addImport('math', IMPORT_MATH);
        return ["math.sqrt(accelerometer.get_x()**2 + accelerometer.get_y()**2 + accelerometer.get_z()**2)", Blockly.Python.ORDER_ATOMIC];
    }
};

Blockly.Python.sensors_getLight = function () {
    return ["display.read_light_level()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_calibrateCompass = function () {
    return "compass.calibrate()" + NEWLINE;
};

Blockly.Python.sensors_getCompass = function () {
    return ["compass.heading()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_isCompassCalibrated = function () {
    return ["compass.is_calibrated()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_getTemperature = function (block) {
    var code = "temperature()";
    switch (block.getFieldValue("UNIT")) {
        case "FAHRENHEIT":
            code += "*9/5 + 32";
            break;
        case "KELVIN":
            code += " + 273.15";
            break;
    }
    return [code, Blockly.Python.ORDER_ATOMIC]
};

Blockly.Python.sensors_getRotation = function (block) {
    Blockly.Python.addImport('math', IMPORT_MATH);
    if (block.getFieldValue("AXIS") === "pitch") {
        return ["math.atan2(accelerometer.get_y(), -accelerometer.get_z()) * 180.0/math.pi", Blockly.Python.ORDER_ATOMIC];
    } else {
        return ["math.atan2(accelerometer.get_x(), math.sqrt(accelerometer.get_y()**2 + accelerometer.get_z()**2)) * 180.0/math.pi", Blockly.Python.ORDER_ATOMIC];
    }
};

Blockly.Python.sensors_getMagneticForce = function (block) {
    var option = block.getFieldValue("AXIS");
    if (option == "NORM") {
        Blockly.Python.addImport('math', IMPORT_MATH);
        return ["math.sqrt((compass.get_x() ** 2 + compass.get_y() ** 2) + compass.get_z() ** 2)", Blockly.Python.ORDER_ATOMIC];
    } else {
        return ["compass.get_" + option + "()", Blockly.Python.ORDER_ATOMIC];
    }
};

// Gas sensors

Blockly.Python.sensors_getSgp30Gas = function (block) {
    Blockly.Python.addImport('sgp30', IMPORT_SGP30);
    Blockly.Python.addInit('sgp30', "sgp30 = SGP30()");
    switch (block.getFieldValue("GAS")) {
        case "CO2":
            return ["sgp30.eCO2()", Blockly.Python.ORDER_ATOMIC];
        case "TVOC":
            return ["sgp30.TVOC()", Blockly.Python.ORDER_ATOMIC];
    }
};

Blockly.Python.sensors_getMultichannelGas = function (block) {
    Blockly.Python.addImport('multichannel_gas', IMPORT_MULTICHANNELGAS);
    Blockly.Python.addInit('multichannel_gas', "gas = GAS()");
    Blockly.Python.addPowerOn('multichannel_gas', "gas.power_on()");
    return ["gas.get_gas(" + block.getFieldValue("GAS") + ")", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_getMultichannelGasV2 = function (block) {
    Blockly.Python.addImport('gas_gmxxx', IMPORT_GAS_GMXXX);
    Blockly.Python.addInit('gas_gmxxx', "gas_v2 = GAS_GMXXX(0x08)");
    return ["gas_v2.calcVol(gas_v2.measure_" + block.getFieldValue("GAS") + "())", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_getO2gas = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('dioxygen_module_' + pin, "# Dioxygen Sensor on " + pin);
    Blockly.Python.addFunction('readO2', DEF_O2SENSOR_READ);
    return ["readO2(" + pin + ")", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_getAirQualityValue = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('air_quality_module_' + pin, "# Air Quality Sensor on " + pin);
    return [pin + ".read_analog()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_getParticulateMatter = function (block) {
    Blockly.Python.addImport('hm330x', IMPORT_HM330X);
    Blockly.Python.addInit('hm330x', "hm3301 = HM330X()");
    return ["hm3301.getData(" + block.getFieldValue("TYPE") + ")", Blockly.Python.ORDER_ATOMIC];
};

// Climate sensors

Blockly.Python.sensors_getBmp280Data = function (block) {
    Blockly.Python.addImport('bmp280', IMPORT_BMP280);
    Blockly.Python.addInit('bmp280', "bmp280 = BMP280(" + block.getFieldValue("ADDR") + ")");
    switch (block.getFieldValue("DATA")) {
        case "TEMP":
            var code = "bmp280.Temperature()";
            if (block.getInput("TEMP_UNIT")) {
                switch (block.getFieldValue("UNIT")) {
                    case "FAHRENHEIT":
                        code += "*9/5 + 32";
                        break;
                    case "KELVIN":
                        code += " + 273.15";
                        break;
                }
            }
            return [code, Blockly.Python.ORDER_ATOMIC];
        case "PRESS":
            return ["bmp280.Pressure()", Blockly.Python.ORDER_ATOMIC];
        case "ALT":
            Blockly.Python.addPowerOn('init_altitude', "h0 = bmp280.Altitude()");
            return ["bmp280.Altitude()-h0", Blockly.Python.ORDER_ATOMIC];
    }
};

Blockly.Python.sensors_getGroveMoisture = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('moisture_module_' + pin, "# Moisture Sensor on " + pin);
    return [pin + ".read_analog()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_getGroveTemperature = function (block) {
    var code;
    var pin = block.getFieldValue("PIN");
    Blockly.Python.addImport('math', IMPORT_MATH);
    Blockly.Python.addInit('grove_temperature_module_' + pin, "# Temperature Sensor on " + pin);
    Blockly.Python.addFunction('getGroveTemperature', DEF_GROVE_GET_TEMP);
    switch (block.getFieldValue("UNIT")) {
        case "CELSIUS":
            code = "getGroveTemperature(" + pin + ")";
            break;
        case "FAHRENHEIT":
            code = "getGroveTemperature(" + pin + ", unit='fahrenheit')";
            break;
        case "KELVIN":
            code = "getGroveTemperature(" + pin + ", unit='kelvin')";
            break;
    }
    return [code, Blockly.Python.ORDER_ATOMIC]
};

Blockly.Python.sensors_getGroveHighTemperature = function (block) {
    let pinA0 = block.getFieldValue("A0");
    let pinA1 = block.getFieldValue("A1");
    Blockly.Python.addImport('math', IMPORT_MATH);
    Blockly.Python.addConstant('thmc_table', DEF_GROVE_HIGHTEMP_THMC_TABLE);
    Blockly.Python.addInit('high_temp_module_' + pinA0, "# High Temperature Sensor on " + pinA0);
    Blockly.Python.addFunction('getThmcTemp', DEF_GROVE_HIGHTEMP_GET_THMC_TEMP);
    Blockly.Python.addFunction('getRoomTemp', DEF_GROVE_HIGHTEMP_GET_ROOM_TEMP);
    Blockly.Python.addFunction('K_VtoT', DEF_GROVE_HIGHTEMP_KVTOT);
    Blockly.Python.addPowerOn('power_high_temp', "tempRoom = getRoomTemp(" + pinA1 + ")");
    var code = "getThmcTemp(" + pinA0 + ", tempRoom)";
    switch (block.getFieldValue("UNIT")) {
        case "FAHRENHEIT":
            code += "*9/5 + 32";
            break;
        case "KELVIN":
            code += " + 273.15";
            break;
    }
    return [code, Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_dhtReadData = function (block) {
    Blockly.Python.addImport('dht11', IMPORT_DHT11);
    Blockly.Python.addInit('dht11', "dht11 = DHT11(" + block.getFieldValue("PIN") + ")");
    switch (block.getFieldValue("DATA")) {
        case "TEMP":
            var code = "dht11.getData(d=1)";
            if (block.getInput("TEMP_UNIT")) {
                switch (block.getFieldValue("UNIT")) {
                    case "FAHRENHEIT":
                        code += "*9/5 + 32";
                        break;
                    case "KELVIN":
                        code += " + 273.15";
                        break;
                }
            }
            return [code, Blockly.Python.ORDER_ATOMIC];
        case "HUM":
            return ["dht11.getData(d=2)", Blockly.Python.ORDER_ATOMIC];
    }
};

Blockly.Python.sensors_TH02readData = function (block) {
    Blockly.Python.addImport('th02', IMPORT_TH02);
    Blockly.Python.addInit('th02', 'th02 = TH02()');
    let data = block.getFieldValue("DATA");
    switch (data) {
        case "TEMP":
            var code = "th02.ReadTemperature()";
            if (block.getInput("TEMP_UNIT")) {
                switch (block.getFieldValue("UNIT")) {
                    case "FAHRENHEIT":
                        code += "*9/5 + 32";
                        break;
                    case "KELVIN":
                        code += " + 273.15";
                        break;
                }
            }
            return [code, Blockly.Python.ORDER_ATOMIC];
        case "HUM":
            return ["th02.ReadHumidity", Blockly.Python.ORDER_ATOMIC];
        default:
            throw Error("Unhandled data option for th02 sensor :'" + data + "'")
    }
};

Blockly.Python.sensors_SHT31readData = function (block) {
    Blockly.Python.addImport('sht31', IMPORT_SHT31);
    Blockly.Python.addInit('sht31', 'sht31 = SHT31()');
    let data = block.getFieldValue("DATA");
    switch (data) {
        case "TEMP":
            var code = "sht31.get_temp_humi(data='t')";
            if (block.getInput("TEMP_UNIT")) {
                switch (block.getFieldValue("UNIT")) {
                    case "FAHRENHEIT":
                        code += "*9/5 + 32";
                        break;
                    case "KELVIN":
                        code += " + 273.15";
                        break;
                }
            }
            return [code, Blockly.Python.ORDER_ATOMIC];
        case "HUM":
            return ["sht31.get_temp_humi(data='h')", Blockly.Python.ORDER_ATOMIC];
        default:
            throw Error("Unhandled data option for sht31 sensor :'" + data + "'")
    }
};

Blockly.Python.sensors_getGroveWaterAmount = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('water_module_' + pin, "# Water Sensor on " + pin);
    return [pin + ".read_analog()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_getRainGauge = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('rain_module_' + pin, "# Rain Gauge on " + pin);
    return [pin + ".read_digital()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_getAnemometer = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('rain_module_' + pin, "# Anemometer on " + pin);
    return [pin + ".read_digital()", Blockly.Python.ORDER_ATOMIC];
};

// Sound & Light sensors

Blockly.Python.sensors_getGroveLight = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('light_module_' + pin, "# Light Sensor on " + pin);
    return [pin + ".read_analog()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_getSi1145Light = function (block) {
    Blockly.Python.addImport('si1145', IMPORT_SI1145);
    Blockly.Python.addInit('si1145', "si1145 = SI1145()");
    switch (block.getFieldValue("LIGHT")) {
        case "UV":
            return ["si1145.readUV()", Blockly.Python.ORDER_ATOMIC];
        case "VIS":
            return ["si1145.readVisible()", Blockly.Python.ORDER_ATOMIC];
        case "IR":
            return ["si1145.readIR()", Blockly.Python.ORDER_ATOMIC];
    }
};

Blockly.Python.sensors_getUVindex = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('uv_module_' + pin, "# UV Sensor on " + pin);
    Blockly.Python.addFunction("getUVindex", DEF_GROVE_GET_UV_INDEX);
    return ["getUVindex(" + pin + ")", Blockly.Python.ORDER_ATOMIC]
};

Blockly.Python.sensors_getGroveSound = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('sound_module_' + pin, "# Sound Sensor on " + pin);
    return [pin + ".read_analog()", Blockly.Python.ORDER_ATOMIC];
};

// Distance & Movement sensors

Blockly.Python.sensors_getGroveUltrasonicRanger = function (block) {
    let pinTRIG = block.getFieldValue("TRIG");
    Blockly.Python.addImport('machine_pulse', IMPORT_MACHINE_PULSE_MS);
    Blockly.Python.addImport('utime', IMPORT_UTIME);
    Blockly.Python.addInit('ultrasonic_module_' + pinTRIG, "# Ultrasonic on TRIG " + pinTRIG);
    Blockly.Python.addFunction('getUltrasonicData', DEF_GROVE_ULTRASONIC);
    var data = block.getFieldValue("DATA");
    var code = "";
    switch (data) {
        case "DIST":
            code = "distance";
            break;
        case "TIME":
            code = "duration";
            break;
    }
    return ["getUltrasonicData(" + pinTRIG + ", " + block.getFieldValue("ECHO") + ", '" + code + "')", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_getGesture = function () {
    Blockly.Python.addImport('gesture', IMPORT_GESTURE);
    Blockly.Python.addInit('gesture', "gesture = GESTURE()");
    return ["gesture.readGesture()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_onGestureTypeDetected = function (block) {
    Blockly.Python.addImport('gesture', IMPORT_GESTURE);
    Blockly.Python.addInit('gesture', "gesture = GESTURE()");
    var branchCode = Blockly.Python.statementToCode(block, "DO") || Blockly.Python.PASS;
    return "if gesture.readGesture() == '" + block.getFieldValue("GESTURE") + "':" + NEWLINE + branchCode;
};

Blockly.Python.sensors_getGroveLineFinder = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('line_finder_module_' + pin, "# Line Finder on " + pin);
    return [pin + ".read_digital()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_getGroveTilt = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('tilt_module_' + pin, "# Tilt Sensor on " + pin);
    return [pin + ".read_digital()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_getGroveMotion = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('motion_module_' + pin, "# Motion Sensor on " + pin);
    return [pin + ".read_digital()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.sensors_getPiezoVibration = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('vibration_module_' + pin, "# Vibration Sensor on " + pin);
    return [pin + ".read_digital()", Blockly.Python.ORDER_ATOMIC];
};

// Other sensors

Blockly.Python.sensors_getGroveButton = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('button_module_' + pin, "# Button on " + pin);
    switch (this.getFieldValue("TYPE")) {
        case "VOLT":
            return [pin + ".read_digital()*3.3", Blockly.Python.ORDER_ATOMIC];
        case "STATE":
            return [pin + ".read_digital()", Blockly.Python.ORDER_ATOMIC];
    }
};