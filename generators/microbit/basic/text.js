// /**
//  * @fileoverview Text generators for Micro:bit.
//  */

// Blockly.Python.text_comment = ;

// Blockly.Python.text = ;

// /**
//  * Enclose the provided value in "str(...)" function.
//  * Leave string literals alone.
//  * @param {string} value Code evaluating to a value.
//  * @return {string} Code evaluating to a string.
//  * @private
//  */
// Blockly.Python.text.forceString_ = ;

// /**
//  * Regular expression to detect a single-quoted string literal.
//  */
// Blockly.Python.text.forceString_.strRegExp = /^\s*'([^']|\\')*'\s*$/;

// Blockly.Python.text_join = ;

// Blockly.Python.text_append = ;

// Blockly.Python.text_length = ;

// Blockly.Python.text_isEmpty = ;

// Blockly.Python.text_indexOf = ;

// Blockly.Python.text_charAt = ;

// Blockly.Python.text_getSubstring = ;

// Blockly.Python.text_changeCase = ;

// Blockly.Python.text_trim = ;

// Blockly.Python.text_count = ;

// Blockly.Python.text_replace = ;

// Blockly.Python.text_reverse = ;
