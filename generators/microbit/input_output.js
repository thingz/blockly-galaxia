/**
 * @fileoverview Input/Output generators for Micro:bit.
 */

// Micro:bit

Blockly.Python.io_pause = function (block) {
    var duration = Blockly.Python.valueToCode(block, "TIME", Blockly.Python.ORDER_NONE) || "0";
    return "sleep((" + duration + ')*' + block.getFieldValue("UNIT") + ")" + NEWLINE;
};

Blockly.Python.io_initChronometer = function () {
    Blockly.Python.addConstant('chronometer', "chrono0 = 0");
    return "chrono0 = running_time()" + NEWLINE;
};

Blockly.Python.io_getChronometer = function (block) {
    Blockly.Python.addConstant('chronometer', "chrono0 = 0");
    return ["(running_time()-chrono0)/" + block.getFieldValue("UNIT"), Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.io_ifButtonPressed = function (block) {
    var button = block.getFieldValue("BUTTON");
    var branchCode = Blockly.Python.statementToCode(block, "DO") || Blockly.Python.PASS;
    if (button === "a" || button === "b") {
        return "if button_" + button + "." + block.getFieldValue("STATE") + "pressed():" + NEWLINE + branchCode;
    } else {
        return "if button_a." + block.getFieldValue("STATE") + "pressed() and button_b." + block.getFieldValue("STATE") + "pressed():" + NEWLINE + branchCode;
    }
};

Blockly.Python.io_onPinPressed = function (block) {
    var branchCode = Blockly.Python.statementToCode(block, "DO") || Blockly.Python.PASS;
    return "if " + block.getFieldValue("PIN") + ".is_touched():" + NEWLINE + branchCode;
};

Blockly.Python.io_onMovement = function (block) {
    var branchCode = Blockly.Python.statementToCode(block, "DO") || Blockly.Python.PASS;
    return "if accelerometer.current_gesture() == '" + block.getFieldValue("MOV") + "' :" + NEWLINE + branchCode;
};

Blockly.Python.io_isButtonPressed = function (block) {
    var code;
    var button = block.getFieldValue("BUTTON");
    var state = block.getFieldValue("STATE");
    if (button === "a" || button === "b") {
        code = "button_" + button + "." + state + "pressed()";
    } else {
        code = "button_a." + state + "pressed() and button_b." + state + "pressed()";
    }
    return [code, Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.io_isPinPressed = function (block) {
    return [block.getFieldValue("PIN") + ".is_touched()", Blockly.Python.ORDER_ATOMIC];
};

// Microphone module

Blockly.Python.io_micro_onSoundDetected = function (block) {
    var branchCode = Blockly.Python.statementToCode(block, "DO") || Blockly.Python.PASS;
    var state = block.getFieldValue("STATE");
    var type = block.getFieldValue("TYPE");
    switch (type) {
        case "IS":
            return "if microphone.current_sound() == microphone." + state + ":" + NEWLINE + branchCode;
        case "WAS":
            return "if microphone.was_sound(microphone." + state + "):" + NEWLINE + branchCode;
        default:
            throw Error("Unhandled type option for microphone sensor :'" + type + "'") 
    }
};

Blockly.Python.io_micro_getCurrentSound = function () {
    return ["microphone.current_sound()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.io_micro_wasSoundDetected = function (block) {
    let state = block.getFieldValue("STATE");
    return ["microphone.was_sound(microphone." + state + ")", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.io_micro_getSoundLevel = function () {
    return ["microphone.sound_level()", Blockly.Python.ORDER_ATOMIC]
};

Blockly.Python.io_micro_getHistorySounds = function () {
    return ["microphone.get_sounds()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.io_micro_setSoundThreshold = function (block) {
    let state = block.getFieldValue("STATE");
    let threshold = Blockly.Python.valueToCode(block, "THRESH", Blockly.Python.ORDER_NONE) || "0";
    return "microphone.set_threshold(microphone." + state + ", " + threshold + ")" + NEWLINE;
};

Blockly.Python.io_micro_soundCondition = function (block) {
    let state = block.getFieldValue("STATE");
    return ["microphone." + state, Blockly.Python.ORDER_ATOMIC];
};

// External modules

Blockly.Python.io_getKeypadNumber = function (block) {
    Blockly.Python.addInit('init_one_uart_module', "uart.init(baudrate=9600, bits=8, parity=None, stop=1, tx=" + block.getFieldValue("TX") + ", rx=" + block.getFieldValue("RX") + ")");
    Blockly.Python.addFunction('getKeypadNumber', DEF_KEYPAD_GET_NUMBER);
    return ["getKeypadNumber()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.io_getGroveThumbJoystick = function (block) {
    var axis = block.getFieldValue("AXIS") || "0";
    if (axis == "X") {
        return [block.getFieldValue("A0") + ".read_analog()", Blockly.Python.ORDER_ATOMIC];
    } else if (axis == "Y") {
        return [block.getFieldValue("A1") + ".read_analog()", Blockly.Python.ORDER_ATOMIC];
    } else {
        return ["", Blockly.Python.ORDER_ATOMIC];
    }
};

Blockly.Python.io_getGroveColoredButton = function (block) {
    let pinSIG2 = block.getFieldValue("PIN");
    Blockly.Python.addInit('colored_button_module_'+ pinSIG2, "# Colored Button ('read') on " + pinSIG2);
    return [pinSIG2 + ".read_digital()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.io_setGroveColoredButton = function (block) {
    let pinSIG1 = block.getFieldValue("PIN");
    var state = Blockly.Python.valueToCode(block, "STATE", Blockly.Python.ORDER_NONE) || "0";
    Blockly.Python.addInit('colored_button_module_'+ pinSIG1, "# Colored Button ('write') on " + pinSIG1);
    return pinSIG1 + ".write_digital(" + state + ")" + NEWLINE;
};

Blockly.Python.io_getGroveSlidePotentiometer = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('potentiometer_module_'+ pin, "# Potentiometer on " + pin);
    return [pin + ".read_analog()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.io_getGroveRotaryAngle = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('potentiometer_module_' + pin, "# Potentiometer on " + pin);
    return [pin + ".read_analog()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.io_getGroveTactile = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('touch_module_'+ pin, "# Touch Button on " + pin);
    return [pin + ".read_digital()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.io_getGroveButton = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('button_module_' + pin, "# Button on " + pin);
    return [pin + ".read_digital()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.io_getGroveSwitch = function (block) {
    let pin = block.getFieldValue("PIN");
    Blockly.Python.addInit('switch_module_' + pin, "# Switch Button on " + pin);
    return [pin + ".read_digital()", Blockly.Python.ORDER_ATOMIC];
};

// Pins

Blockly.Python.io_digital_signal = function (block) {
    return ["HIGH" == block.getFieldValue("BOOL") ? 1 : 0, Blockly.Python.ORDER_ATOMIC]
};

Blockly.Python.io_readDigitalPin = function (block) {
    return [block.getFieldValue("PIN") + ".read_digital()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.io_writeDigitalPin = function (block) {
    var state = Blockly.Python.valueToCode(block, "STATE", Blockly.Python.ORDER_NONE) || "0";
    return block.getFieldValue("PIN") + ".write_digital(" + state + ")" + NEWLINE;
};

Blockly.Python.io_readAnalogPin = function (block) {
    return [block.getFieldValue("PIN") + ".read_analog()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.io_writeAnalogPin = function (block) {
    var value = Blockly.Python.valueToCode(block, "VALUE", Blockly.Python.ORDER_NONE) || "0";
    if (value < 0) value = 0;
    if (value > 1023) value = 1023;
    return block.getFieldValue("PIN") + ".write_analog(" + value + ")" + NEWLINE;
};

Blockly.Python.io_setPwm = function (block) {
    var period = Blockly.Python.valueToCode(block, "PERIOD", Blockly.Python.ORDER_NONE) || "0";
    if (period < 256) period = 256;
    return block.getFieldValue("PIN") + ".set_analog_period_microseconds(" + period + ")" + NEWLINE;
};

Blockly.Python.io_readPulseIn = function (block) {
    Blockly.Python.addImport('utime', IMPORT_UTIME);
    Blockly.Python.addFunction('pulseIn', DEF_IO_PULSE_IN);
    var state = Blockly.Python.valueToCode(block, "STATE", Blockly.Python.ORDER_NONE) || "0";
    return ["pulseIn(" + block.getFieldValue("PIN") + ", " + state + ")", Blockly.Python.ORDER_ATOMIC]
};