/**
 * @fileoverview Start generators for Micro:bit.
 */

Blockly.Python.on_start = function (block) {
    Blockly.Python.addImport('microbit_all', IMPORT_MICROBIT_ALL);
    var statementStack = Blockly.Python.statementToCode(block, "DO");
    var statements = statementStack.split(NEWLINE);
    var formatted = function () {
        var formattedSetup = "";
        statements.forEach(function (statement) {
            if (statement !== "") {
                if (statement.search(/\S/) <= 2)
                    formattedSetup += statement.trim();
                else {
                    formattedSetup += statement;
                }
                formattedSetup += NEWLINE;
            }
        });
        return formattedSetup;
    };
    statementStack = formatted();
    Blockly.Python.userSetups_.setup = statementStack
    return "";
};

Blockly.Python.forever = function (block) {
    Blockly.Python.addImport('microbit_all', IMPORT_MICROBIT_ALL);
    var statements = Blockly.Python.statementToCode(block, "DO");
    statements = Blockly.Python.addLoopTrap(statements, block.id) || Blockly.Python.PASS;
    return "while True:" + NEWLINE + statements;
};

Blockly.Python.scratch_on_start = function () {
    Blockly.Python.addImport('microbit_all', IMPORT_MICROBIT_ALL);
    return "";
};

Blockly.Python.scratch_forever = function (block) {
    Blockly.Python.addImport('microbit_all', IMPORT_MICROBIT_ALL);
    var statements = Blockly.Python.statementToCode(block, "DO");
    statements = Blockly.Python.addLoopTrap(statements, block.id) || Blockly.Python.PASS;
    let n = NEWLINE;
    if (block.previousConnection.targetBlock() !== null && block.previousConnection.targetBlock().type === 'scratch_on_start') {
        n = "";
    }
    return n + "while True:" + NEWLINE + statements;
};