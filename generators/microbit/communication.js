/**
 * @fileoverview Communication generators for Micro:bit.
 */

// Micro:bit radio

Blockly.Python.communication_radioSendString = function (block) {
    Blockly.Python.addImport('radio', IMPORT_RADIO);
    Blockly.Python.addPowerOn('radio', "radio.on()");
    var str = Blockly.Python.valueToCode(block, "STR", Blockly.Python.ORDER_NONE) || "''";
    let inputBlock = block.getInput("STR").connection.targetBlock();
    if (inputBlock && (inputBlock.type == "text" || inputBlock.type == "text_join")) {
        return "radio.send(" + str + ")" + NEWLINE;
    } else {
        return "radio.send(str(" + str + "))" + NEWLINE;
    }
};

Blockly.Python.communication_radioSendNumber = function (block) {
    Blockly.Python.addImport('radio', IMPORT_RADIO);
    Blockly.Python.addFunction('radio_sendNumber', DEF_COM_RADIO_SEND_NUMBER);
    Blockly.Python.addPowerOn('radio', "radio.on()");
    var n = Blockly.Python.valueToCode(block, "N", Blockly.Python.ORDER_NONE) || "0";
    let inputBlock = block.getInput("N").connection.targetBlock();
    if (inputBlock && inputBlock.outputConnection.check_ == "Number") {
        return "radio_sendNumber(" + n + ", type='number')" + NEWLINE;
    } else {
        return "radio_sendNumber(" + n + ", type='boolean')" + NEWLINE;
    }
};

Blockly.Python.communication_radioSendValue = function (block) {
    Blockly.Python.addImport('radio', IMPORT_RADIO);
    Blockly.Python.addFunction('radio_sendValue', DEF_COM_RADIO_SEND_VALUE);
    Blockly.Python.addPowerOn('radio', "radio.on()");
    let name = Blockly.Python.valueToCode(block, "NAME", Blockly.Python.ORDER_NONE) || "''";
    let value = Blockly.Python.valueToCode(block, "VALUE", Blockly.Python.ORDER_NONE) || "0";
    if (block.getInput("VALUE").connection) {
        let inputBlock = block.getInput("VALUE").connection.targetBlock();
        if (inputBlock && inputBlock.outputConnection.check_ == "Number") {
            return "radio_sendValue(" + name + ", " + value + ", type='number')" + NEWLINE;
        } else {
            return "radio_sendValue(" + name + ", " + value + ", type='boolean')" + NEWLINE;
        }
    }
    return "radio_sendValue(" + name + ", " + value + ")" + NEWLINE;

};

Blockly.Python.communication_onRadioDataReceived = function (block) {
    Blockly.Python.addImport('radio', IMPORT_RADIO);
    Blockly.Python.addFunction('radio_receiveData', DEF_COM_RADIO_RECEIVE_DATA);
    Blockly.Python.addPowerOn('radio', "radio.on()");
    var branchCode = Blockly.Python.statementToCode(block, "DO") || Blockly.Python.PASS;
    var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME);
    return dataVar + " = radio_receiveData()" + NEWLINE + "if " + dataVar + ":" + NEWLINE + branchCode;
};

Blockly.Python.communication_onRadioNumberReceived = function (block) {
    Blockly.Python.addImport('radio', IMPORT_RADIO);
    Blockly.Python.addFunction('radio_receiveData', DEF_COM_RADIO_RECEIVE_DATA);
    Blockly.Python.addPowerOn('radio', "radio.on()");
    var branchCode = Blockly.Python.statementToCode(block, "DO") || Blockly.Python.PASS;
    var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME);
    return dataVar + " = radio_receiveData()" + NEWLINE + "if " + dataVar + ":" + NEWLINE + branchCode;
};

Blockly.Python.communication_onRadioValueReceived = function (block) {
    Blockly.Python.addImport('radio', IMPORT_RADIO);
    Blockly.Python.addFunction('radio_receiveValue', DEF_COM_RADIO_RECEIVE_VALUE);
    Blockly.Python.addPowerOn('radio', "radio.on()");
    var branchCode = Blockly.Python.statementToCode(block, "DO") || Blockly.Python.PASS;
    var nameVar = Blockly.Python.variableDB_.getName(block.getFieldValue("NAME"), Blockly.VARIABLE_CATEGORY_NAME) || "''";
    var valueVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VALUE"), Blockly.VARIABLE_CATEGORY_NAME) || "''";
    return nameVar + ", " + valueVar + " = radio_receiveValue()" + NEWLINE + "if " + nameVar + " and " + valueVar + ":" + NEWLINE + branchCode;
};

Blockly.Python.communication_radioConfig = function (block) {
    Blockly.Python.addImport('radio', IMPORT_RADIO);
    var canal = Blockly.Python.valueToCode(block, "CANAL", Blockly.Python.ORDER_NONE) || "0";
    if (canal > 83) canal = 83;
    if (canal < 0) canal = 0;
    var power = Blockly.Python.valueToCode(block, "POWER", Blockly.Python.ORDER_NONE) || "0";
    if (power > 7) power = 7;
    if (power < 0) power = 0;
    var len = Blockly.Python.valueToCode(block, "LEN", Blockly.Python.ORDER_NONE) || "0";
    if (len > 251) len = 251;
    if (len < 0) len = 0;
    return "radio.config(channel = " + canal + ", power = " + power + ", length = " + len + ")" + NEWLINE;
};

// Serial connection

Blockly.Python.communication_serialWrite = function (block) {
    var text = Blockly.Python.valueToCode(block, "TEXT", Blockly.Python.ORDER_NONE) || "''";
    let inputBlock = block.getInput("TEXT").connection.targetBlock();
    if (inputBlock && (inputBlock.type == "text" || inputBlock.type == "text_join")) {
        return "print(" + text + ")" + NEWLINE;
    } else {
        return "print(str(" + text + "))" + NEWLINE;
    } 
};

Blockly.Python.communication_onSerialDataReceived = function (block) {
    Blockly.Python.addInit('serial_receive', "# Serial Receive used ");
    var branchCode = Blockly.Python.statementToCode(block, "DO") || Blockly.Python.PASS;
    var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME);
    return "if uart.any():" + NEWLINE + "  " + dataVar + " = uart.read()" + NEWLINE + branchCode;
};

Blockly.Python.communication_graphSerialWrite = function (block) {
    var c = [];
    let b = "";
    b = "print('@Graph:";
    for (var d = 1; d < block.itemCount_ + 1; d++) {
        c = Blockly.Python.valueToCode(block, "ADD" + (d - 1), Blockly.Python.ORDER_NONE);
        if (c[c.length - 1] === '|') {
            c = c.substring(0, c.length - 1);
            let data = c.split(':');
            b += data[0] + ":' + str(" + data[1] + ") + '|";
        }
    }
    b += "')" + NEWLINE;
    return (b);
};

Blockly.Python.communication_graphSerialWrite_datasFormat = function (block) {
    var name = block.getFieldValue("NAME");
    var data = Blockly.Python.valueToCode(block, "DATA", Blockly.Python.ORDER_ATOMIC);
    if (name == "") name = '""';
    if (!isNaN(data)) {
        data = data.toString();
    }
    let syntax = name + ":" + data + "|";
    return [syntax.toString(), Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.communication_playComputerMusic = function (block) {
    let note = block.getFieldValue("NOTE");
    return "print('@music:" + note + "|')" + NEWLINE;
};

Blockly.Python.communication_playComputerFrequency = function (block) {
    let frequency = Blockly.Python.valueToCode(block, "FREQUENCY", Blockly.Python.ORDER_ATOMIC);
    return "print('@music:' + str(" + frequency + ") + '|')" + NEWLINE;
};

Blockly.Python.communication_stopComputerMusic = function () {
    return "print('@music:stop|')" + NEWLINE;
};

Blockly.Python.communication_serialInit = function (block) {
    let pinTX = block.getFieldValue("TX");
    let pinRX = block.getFieldValue("RX");
    return "uart.init(baudrate=" + block.getFieldValue("BAUD") + ", bits=8, parity=None, stop=1, tx=" + pinTX + ", rx=" + pinRX + ")" + NEWLINE;
};

Blockly.Python.communication_serialRedirectUSB = function () {
    return "uart.init(baudrate=9600, bits=8, parity=None, stop=1)" + NEWLINE;
};

// Data logging

Blockly.Python.communication_writeOpenLogSd = function (block) {
    Blockly.Python.addInit('sd_module_' + block.getFieldValue("TX"), "# LecteurSD on " + block.getFieldValue("TX"));
    let pinTX = block.getFieldValue("TX");
    let pinRX = block.getFieldValue("RX");
    Blockly.Python.addPowerOn('init_one_uart_module', "uart.init(baudrate=4800, tx=" + pinTX + ", rx=" + pinRX + ")");
    var data = Blockly.Python.valueToCode(block, "DATA", Blockly.Python.ORDER_NONE) || "''";
    return "uart.write(" + data + " + '\\n')" + NEWLINE;
};

// Wireless transmission

Blockly.Python.communication_sendBluetoothData = function (block) {
    let pinTX = block.getFieldValue("TX");
    let pinRX = block.getFieldValue("RX");
    Blockly.Python.addPowerOn('init_one_uart_module', "uart.init(baudrate=9600, bits=8, parity=None, stop=1, tx=" + pinTX + ", rx=" + pinRX + ")");
    var data = Blockly.Python.valueToCode(block, "DATA", Blockly.Python.ORDER_NONE) || "''";
    let inputBlock = block.getInput("DATA").connection.targetBlock();
    if (inputBlock && (inputBlock.type == "text" || inputBlock.type == "text_join")) {
        return "uart.write(" + data + ")" + NEWLINE;
    } else {
        return "uart.write(str(" + data + "))" + NEWLINE;
    }
};

Blockly.Python.communication_onBluetoothDataReceived = function (block) {
    let pinTX = block.getFieldValue("TX");
    let pinRX = block.getFieldValue("RX");
    Blockly.Python.addPowerOn('init_one_uart_module', "uart.init(baudrate=9600, bits=8, parity=None, stop=1, tx=" + pinTX + ", rx=" + pinRX + ")");
    var branchCode = Blockly.Python.statementToCode(block, "DO") || Blockly.Python.PASS;
    var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME);
    return "if uart.any():" + NEWLINE + "  " + dataVar + " = uart.read()" + NEWLINE + branchCode;
};

// Tracking modules

Blockly.Python.communication_onGPSDataReceived = function (block) {
    Blockly.Python.addInit('gps_module', "# GPS on UART");
    let pinTX = block.getFieldValue("TX");
    let pinRX = block.getFieldValue("RX");
    Blockly.Python.addPowerOn('init_one_uart_module', "uart.init(baudrate=9600, bits=8, parity=None, stop=1, tx=" + pinTX + ", rx=" + pinRX + ")");
    var branchCode = Blockly.Python.statementToCode(block, "DO") || Blockly.Python.PASS;
    var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME);
    return "if uart.any():" + NEWLINE + "  " + dataVar + " = uart.read()" + NEWLINE + branchCode;
};

Blockly.Python.communication_analyzeGPSInfo = function (block) {
    Blockly.Python.addFunction('gps_getInformation', DEF_COM_GPS_READ);
    Blockly.Python.addFunction('gps_getTime', DEF_COM_GPS_GET_CLOCK);
    Blockly.Python.addFunction('gps_getPosition', DEF_COM_GPS_GET_POS);
    var dataVar = Blockly.Python.valueToCode(block, "DATA", Blockly.Python.ORDER_MEMBER) || "''";
    return ["gps_getInformations(" + dataVar + ", info=" + block.getFieldValue("INFO") + ")", Blockly.Python.ORDER_ATOMIC];
};