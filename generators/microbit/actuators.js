/**
 * @fileoverview Actuators generators for Micro:bit.
 */

Blockly.Python.actuators_setServoAngle = function (block) {
    let angle = Blockly.Python.valueToCode(block, "ANGLE", Blockly.Python.ORDER_NONE) || "0";
    Blockly.Python.addInit('servo_module_' + block.getFieldValue("PIN"), "# Servo on " + block.getFieldValue("PIN"));
    Blockly.Python.addFunction('setServoAngle', DEF_SERVO_SET_ANGLE);
    return "setServoAngle(" + block.getFieldValue("PIN") + ", " + angle + ")" + NEWLINE;
};

Blockly.Python.actuators_continuousServo_setSpeed = function (block) {
    let pin = block.getFieldValue("PIN");
    let dir = block.getFieldValue("DIR");
    let speed = Blockly.Python.valueToCode(block, "SPEED", Blockly.Python.ORDER_NONE);
    Blockly.Python.addFunction('setServoSpeed', DEF_SERVO_SET_SPEED);
    Blockly.Python.addInit('continuous_servo_module_' + block.getFieldValue("PIN"), "# Continuous Servo on " + block.getFieldValue("PIN"));
    return "setServoSpeed(" + pin + ", " + dir + ", " + speed + ")" + NEWLINE;
};

Blockly.Python.actuators_setMotorPower = function (block) {
    var power = Blockly.Python.valueToCode(block, "POWER", Blockly.Python.ORDER_NONE) || "0";
    if (power < 0) power = 0;
    if (power > 1023) power = 1023;
    Blockly.Python.addInit('motor_module_' + block.getFieldValue("PIN"), "# Motor on " + block.getFieldValue("PIN"));
    return block.getFieldValue("PIN") + ".write_analog(" + power + ")" + NEWLINE;
};

Blockly.Python.actuators_setVibrationMotorState = function (block) {
    let state = Blockly.Python.valueToCode(block, "STATE", Blockly.Python.ORDER_NONE) || "0";
    return block.getFieldValue("PIN") + ".write_digital(" + state + ")" + NEWLINE;
};

Blockly.Python.actuators_setGroveRelayState = function (block) {
    let state = Blockly.Python.valueToCode(block, "STATE", Blockly.Python.ORDER_NONE) || "0";
    return block.getFieldValue("PIN") + ".write_digital(" + state + ")" + NEWLINE;
};

Blockly.Python.actuators_playMusicGroveBuzzer = function (block) {
    var pin = block.getFieldValue("PIN");
    var music = block.getFieldValue("MUSIC");
    Blockly.Python.addImport('utime', IMPORT_UTIME);
    Blockly.Python.addInit('buzzer_module_' + block.getFieldValue("PIN"), "# Buzzer on " + block.getFieldValue("PIN"));
    Blockly.Python.addFunction('beep', DEF_BUZZER_BEEP);
    switch (music) {
        case "GAMME":
            Blockly.Python.addFunction('BuzzerGamme', DEF_BUZZER_GAMME);
            return "BuzzerGamme(" + pin + ")" + NEWLINE;
        case "SW":
            Blockly.Python.addFunction('BuzzerStarWars', DEF_BUZZER_STARWARS);
            return "BuzzerStarWars(" + pin + ")" + NEWLINE;
        case "R2D2":
            Blockly.Python.addFunction('BuzzerR2D2', DEF_BUZZER_R2D2);
            return "BuzzerR2D2(" + pin + ")" + NEWLINE;
        default:
            throw Error("Unhandled music option: " + music);
    }
};

Blockly.Python.actuators_music_playSong = function (block) {
    Blockly.Python.addImport('music', IMPORT_MUSIC);
    let song = block.getFieldValue("SONG");
    let pin = block.getFieldValue("PIN");
    let loop = block.getFieldValue("LOOP");
    var code;
    switch (loop) {
        case "ONCE":
            code = "music.play(music." + song + ", pin=" + pin + ", loop=False)" + NEWLINE;
            break;
        case "LOOP":
            code = "music.play(music." + song + ", pin=" + pin + ", loop=True)" + NEWLINE;
            break;
        default:
            throw Error("Unhandled loop option for music: " + loop);
    }
    return code;
};

Blockly.Python.actuators_music_playNotes = function (block) {
    Blockly.Python.addImport('music', IMPORT_MUSIC);
    // ToDo :corriger block.itemCount_ = NaN
    // console.log({block})
    var notes = new Array(block.itemCount_);
    for (var i = 0; i < block.itemCount_; i++) {
        notes[i] = Blockly.Python.valueToCode(block, "ADD" + i, Blockly.Python.ORDER_NONE) || "None";
    }
    return "music.play([" + notes.join(", ") + "], pin=" + block.getFieldValue("PIN") + ")" + NEWLINE;
};

Blockly.Python.actuators_music_note = function (block) {
    var note = block.getFieldValue("NOTE");
    var octave = block.getFieldValue("OCTAVE");
    if (octave == "4") octave = "";
    var duration = ":" + block.getFieldValue("DURATION");
    if (duration == ":1") duration = "";
    return ["'" + note + octave + duration + "'", Blockly.Python.ORDER_ATOMIC]
};

Blockly.Python.actuators_music_playFrequency = function (block) {
    let freq = Blockly.Python.valueToCode(block, "FREQ", Blockly.Python.ORDER_NONE) || "0";
    let duration = Blockly.Python.valueToCode(block, "DURATION", Blockly.Python.ORDER_NONE) || "0";
    Blockly.Python.addImport('music', IMPORT_MUSIC);
    return "music.pitch(" + freq + ", duration=" + duration + ", pin=" + block.getFieldValue("PIN") + ")" + NEWLINE;
};

Blockly.Python.actuators_music_stop = function (block) {
    Blockly.Python.addImport('music', IMPORT_MUSIC);
    return "music.stop(pin=" + block.getFieldValue("PIN") + ")" + NEWLINE;
};

Blockly.Python.actuators_music_setVolume = function (block) {
    let volume = Blockly.Python.valueToCode(block, "VOL", Blockly.Python.ORDER_NONE);
    return "set_volume(" + volume + ")" + NEWLINE;
};


Blockly.Python.actuators_music_setTempo = function (block) {
    let ticks = Blockly.Python.valueToCode(block, "TICKS", Blockly.Python.ORDER_NONE) || "0";
    let bpm = Blockly.Python.valueToCode(block, "BPM", Blockly.Python.ORDER_NONE) || "0";
    Blockly.Python.addImport('music', IMPORT_MUSIC);
    return "music.set_tempo(ticks=" + ticks + ", bpm=" + bpm + ")" + NEWLINE;
};

Blockly.Python.actuators_music_getTempo = function () {
    Blockly.Python.addImport('music', IMPORT_MUSIC);
    return ["music.get_tempo()", Blockly.Python.ORDER_ATOMIC]
};

Blockly.Python.actuators_speech_saySomething = function (block) {
    let text = Blockly.Python.valueToCode(block, "TEXT", Blockly.Python.ORDER_NONE) || "''";
    let speed = Blockly.Python.valueToCode(block, "SPEED", Blockly.Python.ORDER_NONE) || "0";
    let pitch = Blockly.Python.valueToCode(block, "PITCH", Blockly.Python.ORDER_NONE) || "0";
    Blockly.Python.addImport('speech', IMPORT_SPEECH);
    let inputBlock = block.getInput("TEXT").connection.targetBlock();
    if (inputBlock && (inputBlock.type == "text" || inputBlock.type == "text_join")) {
        return "speech.say(" + text + ", speed=" + speed + ", pitch=" + pitch + ")" + NEWLINE;
    } else {
        return "speech.say(str(" + text + "), speed=" + speed + ", pitch=" + pitch + ")" + NEWLINE;
    }
};