/**
 * @fileoverview Variables generators for Micro:bit.
 */

Blockly.Python.variables_get = function (block) {
    var varName = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME);
    return [varName, Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.variables_set = function (block) {
    var argument0 = Blockly.Python.valueToCode(block, "VALUE", Blockly.Python.ORDER_NONE) || "0",
        varName = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME);
    return varName + " = " + argument0 + NEWLINE;
};

Blockly.Python.variables_increment = function (block) {
    var increment = Blockly.Python.valueToCode(block, "DELTA", Blockly.Python.ORDER_ADDITIVE) || "0",
        varName = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME);
    return varName + " = (" + varName + " if isinstance(" + varName + ", int or float) else 0) + " + increment + NEWLINE;
};

Blockly.Python.variables_force_type = function (block) {
    var value = Blockly.Python.valueToCode(block, "VALUE", Blockly.Python.ORDER_MEMBER) || "0";
    return [block.getFieldValue("TYPE") + "(" + value + ")", Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.variables_type_of = function (block) {
    var varName = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME);
    return ["type(" + varName + ")", Blockly.Python.ORDER_ATOMIC];
};
