/**
 * @fileoverview Loops generators for Micro:bit.
 */

Blockly.Python.controls_repeat = function(block) {
  var repeats = Blockly.Python.valueToCode(block, "TIMES", Blockly.Python.ORDER_NONE) || "0";
  if (Blockly.isNumber(repeats)) {
    repeats = parseInt(repeats, 10);
  } else {
    repeats = "int(" + repeats + ")";
  }
  var branch = Blockly.Python.statementToCode(block, "DO");
  branch = Blockly.Python.addLoopTrap(branch, block) || Blockly.Python.PASS;
  var loopVar = Blockly.Python.variableDB_.getDistinctName("count", Blockly.VARIABLE_CATEGORY_NAME);
  var code = "for " + loopVar + " in range(" + repeats + "):" + NEWLINE + branch;
  return code;
};

Blockly.Python.controls_whileUntil = function(block) {
  var until = block.getFieldValue("MODE") == "UNTIL",
      argument0 = Blockly.Python.valueToCode(block, "BOOL", until ? Blockly.Python.ORDER_LOGICAL_NOT : Blockly.Python.ORDER_NONE) || "False",
      branch = Blockly.Python.statementToCode(block, "DO");
  branch = Blockly.Python.addLoopTrap(branch, block) || Blockly.Python.PASS;
  if (until) {
    argument0 = "not " + argument0;
  }
  return "while " + argument0 + ":" + NEWLINE + branch;
};

Blockly.Python.controls_for = function(block) {
  var variable0 = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME),
      argument0 = Blockly.Python.valueToCode(block, "FROM", Blockly.Python.ORDER_NONE) || "0",
      argument1 = Blockly.Python.valueToCode(block, "TO", Blockly.Python.ORDER_NONE) || "0",
      increment = Blockly.Python.valueToCode(block, "BY", Blockly.Python.ORDER_NONE) || "1",
      branch = Blockly.Python.statementToCode(block, "DO");
  branch = Blockly.Python.addLoopTrap(branch, block) || Blockly.Python.PASS;
  var code = "", range;
  // Helper functions.
  var defineUpRange = function() {
    Blockly.Python.addFunction("func_upRange", DEF_LOOPS_UP_RANGE);
    return "upRange";
  };
  var defineDownRange = function() {
    Blockly.Python.addFunction("func_downRange", DEF_LOOPS_DOWN_RANGE);
    return "downRange";
  };
  // Arguments are legal Python code (numbers or strings returned by scrub()).
  var generateUpDownRange = function(start, end, inc) {
    return "(" + start + " <= " + end + ") and " +
        defineUpRange() + "(" + start + ", " + end + ", " + inc + ") or " +
        defineDownRange() + "(" + start + ", " + end + ", " + inc + ")";
  };
  if (Blockly.isNumber(argument0) && Blockly.isNumber(argument1) && Blockly.isNumber(increment)) {
    // All parameters are simple numbers.
    argument0 = Number(argument0);
    argument1 = Number(argument1);
    increment = Math.abs(Number(increment));
    if (argument0 % 1 === 0 && argument1 % 1 === 0 && increment % 1 === 0) {
      // All parameters are integers.
      if (argument0 <= argument1) {
        // Count up.
        argument1++;
        if (argument0 == 0 && increment == 1) {
          // If starting index is 0, omit it.
          range = argument1;
        } else {
          range = argument0 + ", " + argument1;
        }
        // If increment isn"t 1, it must be explicit.
        if (increment != 1) {
          range += ", " + increment;
        }
      } else {
        // Count down.
        argument1--;
        range = argument0 + ", " + argument1 + ", -" + increment;
      }
      range = "range(" + range + ")";
    } else {
      // At least one of the parameters is not an integer.
      if (argument0 < argument1) {
        range = defineUpRange();
      } else {
        range = defineDownRange();
      }
      range += "(" + argument0 + ", " + argument1 + ", " + increment + ")";
    }
  } else {
    // Cache non-trivial values to variables to prevent repeated look-ups.
    var scrub = function(arg, suffix) {
      if (Blockly.isNumber(arg)) {
        // Simple number.
        arg = Number(arg);
      } else if (arg.match(/^\w+$/)) {
        // Variable.
        arg = "float(" + arg + ")";
      } else {
        // It"s complicated.
        var varName = Blockly.Python.variableDB_.getDistinctName(variable0 + suffix, Blockly.VARIABLE_CATEGORY_NAME);
        code += varName + " = float(" + arg + ")" + NEWLINE;
        arg = varName;
      }
      return arg;
    };
    var startVar = scrub(argument0, "_start");
    var endVar = scrub(argument1, "_end");
    var incVar = scrub(increment, "_inc");
    if (typeof startVar == "number" && typeof endVar == "number") {
      if (startVar < endVar) {
        range = defineUpRange() + "(" + startVar + ", " + endVar + ", " + incVar + ")";
      } else {
        range = defineDownRange() + "(" + startVar + ", " + endVar + ", " + incVar + ")";
      }
    } else {
      // We cannot determine direction statically.
      range = generateUpDownRange(startVar, endVar, incVar);
    }
  }
  code += "for " + variable0 + " in " + range + ":" + NEWLINE + branch;
  return code;
};

Blockly.Python.controls_forEach = function(block) {
  // For each loop.
  var variable0 = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME);
  var argument0 = Blockly.Python.valueToCode(block, "LIST", Blockly.Python.ORDER_RELATIONAL) || "[]";
  var branch = Blockly.Python.statementToCode(block, "DO");
  branch = Blockly.Python.addLoopTrap(branch, block) || Blockly.Python.PASS;
  return "for " + variable0 + " in " + argument0 + ":" + NEWLINE + branch;
};

Blockly.Python.controls_flow_statements = function(block) {
  var xfix = "";
  if (Blockly.Python.STATEMENT_PREFIX) {
    // Automatic prefix insertion is switched off for this block.  Add manually.
    xfix += Blockly.Python.injectId(Blockly.Python.STATEMENT_PREFIX, block);
  }
  if (Blockly.Python.STATEMENT_SUFFIX) {
    // Inject any statement suffix here since the regular one at the end
    // will not get executed if the break/continue is triggered.
    xfix += Blockly.Python.injectId(Blockly.Python.STATEMENT_SUFFIX, block);
  }
  if (Blockly.Python.STATEMENT_PREFIX) {
    var loop = Blockly.Constants.Loops.CONTROL_FLOW_IN_LOOP_CHECK_MIXIN.getSurroundLoop(block);
    if (loop && !loop.suppressPrefixSuffix) {
      // Inject loop"s statement prefix here since the regular one at the end
      // of the loop will not get executed if "continue" is triggered.
      // In the case of "break", a prefix is needed due to the loop"s suffix.
      xfix += Blockly.Python.injectId(Blockly.Python.STATEMENT_PREFIX, loop);
    }
  }
  switch (block.getFieldValue("FLOW")) {
    case "BREAK":
      return xfix + "break" + NEWLINE;
    case "CONTINUE":
      return xfix + "continue" + NEWLINE;
  }
  throw Error("Unknown flow statement.");
};
