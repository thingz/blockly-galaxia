/**
 * @fileoverview Text generators for Micro:bit.
 */

Blockly.Python.text_comment = function (block) {
  return "#" + block.getFieldValue("TEXT") + NEWLINE || "" + NEWLINE;
};

Blockly.Python.text = function(block) {
  return [Blockly.Python.quote_(block.getFieldValue("TEXT")), Blockly.Python.ORDER_ATOMIC];
};

/**
 * Enclose the provided value in "str(...)" function.
 * Leave string literals alone.
 * @param {string} value Code evaluating to a value.
 * @return {string} Code evaluating to a string.
 * @private
 */
Blockly.Python.text.forceString_ = function(value) {
  if (Blockly.Python.text.forceString_.strRegExp.test(value)) {
    return value;
  }
  return "str(" + value + ")";
};

/**
 * Regular expression to detect a single-quoted string literal.
 */
Blockly.Python.text.forceString_.strRegExp = /^\s*'([^']|\\')*'\s*$/;

Blockly.Python.text_join = function(block) {
  switch (block.itemCount_) {
    case 0:
      return ["''", Blockly.Python.ORDER_ATOMIC];
    case 1:
      var element = Blockly.Python.valueToCode(block, "ADD0", Blockly.Python.ORDER_NONE) || "''";
      return [Blockly.Python.text.forceString_(element), Blockly.Python.ORDER_FUNCTION_CALL];
    case 2:
      var element0 = Blockly.Python.valueToCode(block, "ADD0", Blockly.Python.ORDER_NONE) || "''";
      var element1 = Blockly.Python.valueToCode(block, "ADD1", Blockly.Python.ORDER_NONE) || "''";
      return [Blockly.Python.text.forceString_(element0) + " + " + Blockly.Python.text.forceString_(element1), Blockly.Python.ORDER_ADDITIVE];
    default:
      var elements = [];
      for (var i = 0; i < block.itemCount_; i++) {
        elements[i] = Blockly.Python.valueToCode(block, "ADD" + i, Blockly.Python.ORDER_NONE) || "''";
      }
      var tempVar = Blockly.Python.variableDB_.getDistinctName('x', Blockly.VARIABLE_CATEGORY_NAME);
      return ["''.join([str(" + tempVar + ") for " + tempVar + " in [" + elements.join(", ") + "]])", Blockly.Python.ORDER_FUNCTION_CALL];
  }
};

Blockly.Python.text_append = function(block) {
  var varName = Blockly.Python.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME),
      value = Blockly.Python.valueToCode(block, "TEXT", Blockly.Python.ORDER_NONE) || "''";
  return varName + " = str(" + varName + ") + " + Blockly.Python.text.forceString_(value) + NEWLINE;
};

Blockly.Python.text_length = function(block) {
  var text = Blockly.Python.valueToCode(block, "VALUE", Blockly.Python.ORDER_NONE) || "''";
  return ["len(" + text + ")", Blockly.Python.ORDER_FUNCTION_CALL];
};

Blockly.Python.text_isEmpty = function(block) {
  var text = Blockly.Python.valueToCode(block, "VALUE", Blockly.Python.ORDER_NONE) || "''";
  return ["not len(" + text + ")", Blockly.Python.ORDER_LOGICAL_NOT];
};

Blockly.Python.text_indexOf = function(block) {
  var operator = block.getFieldValue("END") == "FIRST" ? "find" : "rfind",
      substring = Blockly.Python.valueToCode(block, "FIND", Blockly.Python.ORDER_NONE) || "''",
      text = Blockly.Python.valueToCode(block, "VALUE", Blockly.Python.ORDER_MEMBER) || "''",
      code = text + "." + operator + "(" + substring + ")";
  if (block.workspace.options.oneBasedIndex) {
    return [code + " + 1", Blockly.Python.ORDER_ADDITIVE];
  }
  return [code, Blockly.Python.ORDER_FUNCTION_CALL];
};

Blockly.Python.text_charAt = function(block) {
  var where = block.getFieldValue("WHERE") || "FROM_START",
      text = Blockly.Python.valueToCode(block, "VALUE", Blockly.Python.ORDER_MEMBER) || "''";
  switch (where) {
    case "FIRST":
      return [text + "[0]", Blockly.Python.ORDER_MEMBER];
    case "LAST":
      return [text + "[-1]", Blockly.Python.ORDER_MEMBER];
    case "FROM_START":
      var at = Blockly.Python.getAdjustedInt(block, "AT");
      return [text + "[" + at + "]", Blockly.Python.ORDER_MEMBER];
    case "FROM_END":
      var at = Blockly.Python.getAdjustedInt(block, "AT", 1, true);
      return [text + "[" + at + "]", Blockly.Python.ORDER_MEMBER];
    case "RANDOM":
      Blockly.Python.addImport('random', IMPORT_RANDOM);
      Blockly.Python.addFunction('random_letter', DEF_TEXT_RANDOM_LETTER);
      return ["random_letter(" + text + ")", Blockly.Python.ORDER_FUNCTION_CALL];
  }
  throw Error("Unhandled option (text_charAt).");
};

Blockly.Python.text_getSubstring = function(block) {
  var text = Blockly.Python.valueToCode(block, "STRING", Blockly.Python.ORDER_MEMBER) || "''";
  switch (block.getFieldValue("WHERE1")) {
    case "FROM_START":
      var at1 = Blockly.Python.getAdjustedInt(block, "AT1");
      if (at1 == "0") {
        at1 = "";
      }
      break;
    case "FROM_END":
      var at1 = Blockly.Python.getAdjustedInt(block, "AT1", 1, true);
      break;
    case "FIRST":
      var at1 = "";
      break;
    default:
      throw Error("Unhandled option (text_getSubstring)");
  }
  switch (block.getFieldValue("WHERE2")) {
    case "FROM_START":
      var at2 = Blockly.Python.getAdjustedInt(block, "AT2", 1);
      break;
    case "FROM_END":
      var at2 = Blockly.Python.getAdjustedInt(block, "AT2", 0, true);
      if (!Blockly.isNumber(String(at2))) {
        Blockly.Python.addImport('sys', IMPORT_SYS);
        at2 += " or sys.maxsize";
      } else if (at2 == "0") {
        at2 = "";
      }
      break;
    case "LAST":
      var at2 = "";
      break;
    default:
      throw Error("Unhandled option (text_getSubstring)");
  }
  return [text + "[" + at1 + " : " + at2 + "]", Blockly.Python.ORDER_MEMBER];
};

Blockly.Python.text_changeCase = function(block) {
  var OPERATORS = {
    "UPPERCASE": ".upper()",
    "LOWERCASE": ".lower()",
    "TITLECASE": ".title()"
  };
  var operator = OPERATORS[block.getFieldValue("CASE")],
      text = Blockly.Python.valueToCode(block, "TEXT", Blockly.Python.ORDER_MEMBER) || "''";
  return [text + operator, Blockly.Python.ORDER_FUNCTION_CALL];
};

Blockly.Python.text_trim = function(block) {
  var OPERATORS = {
    "LEFT": ".lstrip()",
    "RIGHT": ".rstrip()",
    "BOTH": ".strip()"
  };
  var operator = OPERATORS[block.getFieldValue("MODE")],
      text = Blockly.Python.valueToCode(block, "TEXT", Blockly.Python.ORDER_MEMBER) || "''";
  return [text + operator, Blockly.Python.ORDER_FUNCTION_CALL];
};

Blockly.Python.text_count = function(block) {
  var text = Blockly.Python.valueToCode(block, "TEXT", Blockly.Python.ORDER_MEMBER) || "''",
      sub = Blockly.Python.valueToCode(block, "SUB", Blockly.Python.ORDER_NONE) || "''";
  return [text + ".count(" + sub + ")", Blockly.Python.ORDER_MEMBER];
};

Blockly.Python.text_replace = function(block) {
  var text = Blockly.Python.valueToCode(block, "TEXT", Blockly.Python.ORDER_MEMBER) || "''",
      from = Blockly.Python.valueToCode(block, "FROM", Blockly.Python.ORDER_NONE) || "''",
      to = Blockly.Python.valueToCode(block, "TO", Blockly.Python.ORDER_NONE) || "''";
  return [text + ".replace(" + from + ", " + to + ")", Blockly.Python.ORDER_MEMBER];
};

Blockly.Python.text_reverse = function(block) {
  var text = Blockly.Python.valueToCode(block, "TEXT", Blockly.Python.ORDER_MEMBER) || "''";
  return [text + "[::-1]", Blockly.Python.ORDER_MEMBER];
};
