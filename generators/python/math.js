/**
 * @fileoverview Math generators for Micro:bit.
 */

// If any new block imports any library, add that library name here.
Blockly.Python.addReservedWords("math,random");

Blockly.Python.math_number = function(block) {
  var code = Number(block.getFieldValue("NUM"));
  var order;
  if (code == Infinity) {
    code = "float('inf')";
    order = Blockly.Python.ORDER_FUNCTION_CALL;
  } else if (code == -Infinity) {
    code = "-float('inf')";
    order = Blockly.Python.ORDER_UNARY_SIGN;
  } else {
    order = code < 0 ? Blockly.Python.ORDER_UNARY_SIGN : Blockly.Python.ORDER_ATOMIC;
  }
  return [code, order];
};

Blockly.Python.math_arithmetic = function(block) {
  // Basic arithmetic operators, and power.
  var OPERATORS = {
    "ADD": [" + ", Blockly.Python.ORDER_ADDITIVE],
    "MINUS": [" - ", Blockly.Python.ORDER_ADDITIVE],
    "MULTIPLY": [" * ", Blockly.Python.ORDER_MULTIPLICATIVE],
    "DIVIDE": [" / ", Blockly.Python.ORDER_MULTIPLICATIVE],
    "POWER": [" ** ", Blockly.Python.ORDER_EXPONENTIATION]
  };
  var tuple = OPERATORS[block.getFieldValue("OP")];
  var operator = tuple[0];
  var order = tuple[1];
  var argument0 = Blockly.Python.valueToCode(block, "A", order) || "0";
  var argument1 = Blockly.Python.valueToCode(block, "B", order) || "0";
  var code = argument0 + operator + argument1;
  return [code, order];
  // In case of "DIVIDE", division between integers returns different results
  // in Python 2 and 3. However, is not an issue since Blockly does not
  // guarantee identical results in all languages.  To do otherwise would
  // require every operator to be wrapped in a function call.  This would kill
  // legibility of the generated code.
};

Blockly.Python.math_single = function(block) {
  // Math operators with single operand.
  var operator = block.getFieldValue("OP");
  var code;
  var arg;
  if (operator == "NEG") {
    // Negation is a special case given its different operator precedence.
    code = Blockly.Python.valueToCode(block, "NUM", Blockly.Python.ORDER_UNARY_SIGN) || "0";
    return ["-" + code, Blockly.Python.ORDER_UNARY_SIGN];
  }
  Blockly.Python.addImport('math', IMPORT_MATH);
  if (operator == "SIN" || operator == "COS" || operator == "TAN") {
    arg = Blockly.Python.valueToCode(block, "NUM", Blockly.Python.ORDER_MULTIPLICATIVE) || "0";
  } else {
    arg = Blockly.Python.valueToCode(block, "NUM", Blockly.Python.ORDER_NONE) || "0";
  }
  // First, handle cases which generate values that don"t need parentheses
  // wrapping the code.
  switch (operator) {
    case "ABS":
      code = "math.fabs(" + arg + ")";
      break;
    case "ROOT":
      code = "math.sqrt(" + arg + ")";
      break;
    case "LN":
      code = "math.log(" + arg + ")";
      break;
    case "LOG10":
      code = "math.log10(" + arg + ")";
      break;
    case "EXP":
      code = "math.exp(" + arg + ")";
      break;
    case "POW10":
      code = "math.pow(10," + arg + ")";
      break;
    case "ROUND":
      code = "round(" + arg + ")";
      break;
    case "ROUNDUP":
      code = "math.ceil(" + arg + ")";
      break;
    case "ROUNDDOWN":
      code = "math.floor(" + arg + ")";
      break;
    case "SIN":
      code = "math.sin(" + arg + " / 180.0 * math.pi)";
      break;
    case "COS":
      code = "math.cos(" + arg + " / 180.0 * math.pi)";
      break;
    case "TAN":
      code = "math.tan(" + arg + " / 180.0 * math.pi)";
      break;
  }
  if (code) {
    return [code, Blockly.Python.ORDER_FUNCTION_CALL];
  }
  // Second, handle cases which generate values that may need parentheses
  // wrapping the code.
  switch (operator) {
    case "ASIN":
      code = "math.asin(" + arg + ") / math.pi * 180";
      break;
    case "ACOS":
      code = "math.acos(" + arg + ") / math.pi * 180";
      break;
    case "ATAN":
      code = "math.atan(" + arg + ") / math.pi * 180";
      break;
    default:
      throw Error("Unknown math operator: " + operator);
  }
  return [code, Blockly.Python.ORDER_MULTIPLICATIVE];
};

Blockly.Python.math_constant = function(block) {
  // Constants: PI, E, the Golden Ratio, sqrt(2), 1/sqrt(2), INFINITY.
  var CONSTANTS = {
    "PI": ["math.pi", Blockly.Python.ORDER_MEMBER],
    "E": ["math.e", Blockly.Python.ORDER_MEMBER],
    "GOLDEN_RATIO": ["(1 + math.sqrt(5)) / 2", Blockly.Python.ORDER_MULTIPLICATIVE],
    "SQRT2": ["math.sqrt(2)", Blockly.Python.ORDER_MEMBER],
    "SQRT1_2": ["math.sqrt(1.0 / 2)", Blockly.Python.ORDER_MEMBER],
    "INFINITY": ["float(\"inf\")", Blockly.Python.ORDER_ATOMIC]
  };
  var constant = block.getFieldValue("CONSTANT")
  if (constant != "INFINITY") {
    Blockly.Python.addImport('math', IMPORT_MATH);
  }
  return CONSTANTS[constant];
};

Blockly.Python.math_number_property = function(block) {
  // Check if a number is even, odd, prime, whole, positive, or negative
  // or if it is divisible by certain number. Returns true or false.
  var number_to_check = Blockly.Python.valueToCode(block, "NUMBER_TO_CHECK", Blockly.Python.ORDER_MULTIPLICATIVE) || "0";
  var dropdown_property = block.getFieldValue("PROPERTY");
  var code;
  if (dropdown_property == "PRIME") {
    Blockly.Python.addImport('math', IMPORT_MATH);
    Blockly.Python.addFunction('func_isPrime', DEF_MATH_IS_PRIME)
    code = "math_isPrime(" + number_to_check + ")";
    return [code, Blockly.Python.ORDER_FUNCTION_CALL];
  }
  switch (dropdown_property) {
    case "EVEN":
      code = number_to_check + " % 2 == 0";
      break;
    case "ODD":
      code = number_to_check + " % 2 == 1";
      break;
    case "WHOLE":
      code = number_to_check + " % 1 == 0";
      break;
    case "POSITIVE":
      code = number_to_check + " > 0";
      break;
    case "NEGATIVE":
      code = number_to_check + " < 0";
      break;
    case "DIVISIBLE_BY":
      var divisor = Blockly.Python.valueToCode(block, "DIVISOR", Blockly.Python.ORDER_MULTIPLICATIVE);
      // If "divisor" is some code that evals to 0, Python will raise an error.
      if (!divisor || divisor == "0") {
        return ["False", Blockly.Python.ORDER_ATOMIC];
      }
      code = number_to_check + " % " + divisor + " == 0";
      break;
  }
  return [code, Blockly.Python.ORDER_RELATIONAL];
};

// Rounding functions have a single operand.
Blockly.Python.math_round = Blockly.Python.math_single;

// Trigonometry functions have a single operand.
Blockly.Python.math_trig = Blockly.Python.math_single;

Blockly.Python.math_map = function (block) {
  Blockly.Python.addImport('math', IMPORT_MATH);
  var value = Blockly.Python.valueToCode(block, "VALUE", Blockly.Python.ORDER_ADDITIVE) || "0";
  var min1 = Blockly.Python.valueToCode(block, "MIN1", Blockly.Python.ORDER_ADDITIVE) || "0";
  var max1 = Blockly.Python.valueToCode(block, "MAX1", Blockly.Python.ORDER_ADDITIVE) || "0";
  var min2 = Blockly.Python.valueToCode(block, "MIN2", Blockly.Python.ORDER_ADDITIVE) || "0";
  var max2 = Blockly.Python.valueToCode(block, "MAX2", Blockly.Python.ORDER_ADDITIVE) || "0";
  var newValue = "round((" + value + "-" + min1 + ") * (" + max2 + "-" + min2 + ") / (" + max1 + "-" + min1 + ") + " + min2 + ", 2)";
  return [newValue, Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.math_modulo = function(block) {
  var argument0 = Blockly.Python.valueToCode(block, "DIVIDEND", Blockly.Python.ORDER_MULTIPLICATIVE) || "0";
  var argument1 = Blockly.Python.valueToCode(block, "DIVISOR", Blockly.Python.ORDER_MULTIPLICATIVE) || "0";
  return [argument0 + " % " + argument1, Blockly.Python.ORDER_MULTIPLICATIVE];
};

Blockly.Python.math_constrain = function(block) {
  var argument0 = Blockly.Python.valueToCode(block, "VALUE", Blockly.Python.ORDER_NONE) || "0";
  var argument1 = Blockly.Python.valueToCode(block, "LOW", Blockly.Python.ORDER_NONE) || "0";
  var argument2 = Blockly.Python.valueToCode(block, "HIGH", Blockly.Python.ORDER_NONE) || "float(\"inf\")";
  return ["min(max(" + argument0 + ", " + argument1 + "), " + argument2 + ")", Blockly.Python.ORDER_FUNCTION_CALL];
};

Blockly.Python.math_random_int = function(block) {
  Blockly.Python.addImport('random', IMPORT_RANDOM);
  var argument0 = Blockly.Python.valueToCode(block, "FROM", Blockly.Python.ORDER_NONE) || "0";
  var argument1 = Blockly.Python.valueToCode(block, "TO", Blockly.Python.ORDER_NONE) || "0";
  return ["random.randint(" + argument0 + ", " + argument1 + ")", Blockly.Python.ORDER_FUNCTION_CALL];
};

Blockly.Python.math_random_float = function() {
  Blockly.Python.addImport('random', IMPORT_RANDOM);
  return ["random.random()", Blockly.Python.ORDER_FUNCTION_CALL];
};

Blockly.Python.math_atan2 = function(block) {
  Blockly.Python.addImport('math', IMPORT_MATH);
  var argument0 = Blockly.Python.valueToCode(block, "X", Blockly.Python.ORDER_NONE) || "0";
  var argument1 = Blockly.Python.valueToCode(block, "Y", Blockly.Python.ORDER_NONE) || "0";
  return ["math.atan2(" + argument1 + ", " + argument0 + ") / math.pi * 180", Blockly.Python.ORDER_MULTIPLICATIVE];
};
