/**
 * @fileoverview Procedures generators for Micro:bit.
 */

Blockly.Python.procedures_defreturn = function (block) {
  var funcName = Blockly.Python.variableDB_.getName(block.getFieldValue("NAME"), Blockly.PROCEDURE_CATEGORY_NAME),
      xfix1 = "";
  if (Blockly.Python.STATEMENT_PREFIX) {
    xfix1 += Blockly.Python.injectId(Blockly.Python.STATEMENT_PREFIX, block);
  }
  if (Blockly.Python.STATEMENT_SUFFIX) {
    xfix1 += Blockly.Python.injectId(Blockly.Python.STATEMENT_SUFFIX, block);
  }
  if (xfix1) {
    xfix1 = Blockly.Python.prefixLines(xfix1, Blockly.Python.INDENT);
  }
  var loopTrap = "";
  if (Blockly.Python.INFINITE_LOOP_TRAP) {
    loopTrap = Blockly.Python.prefixLines(
        Blockly.Python.injectId(Blockly.Python.INFINITE_LOOP_TRAP, block),
        Blockly.Python.INDENT);
  }
  var branch = Blockly.Python.statementToCode(block, "STACK"),
      returnValue = Blockly.Python.valueToCode(block, "RETURN", Blockly.Python.ORDER_NONE) || "",
      xfix2 = "";
  if (branch && returnValue) {
    xfix2 = xfix1;
  }

  var globalVar = Blockly.Python.getUsedGlobalVarInBlock(block, branch)

  if (returnValue) {
    returnValue = Blockly.Python.INDENT + "return " + returnValue + NEWLINE;
  } else if (!branch) {
    branch = Blockly.Python.PASS;
  }
  var args = [];
  for (var i = 0; i < block.arguments_.length; i++) {
    args[i] = Blockly.Python.variableDB_.getName(block.arguments_[i], Blockly.VARIABLE_CATEGORY_NAME);
  }

  var globalVar = Blockly.Python.getUsedGlobalVarInBlock(block, branch, args)

  var code = "def " + funcName + "(" + args.join(", ") + "):" + NEWLINE + xfix1 + loopTrap + globalVar + branch + xfix2 + returnValue;
  code = Blockly.Python.scrub_(block, code);
  Blockly.Python.userFunctions_["%" + funcName] = code;
  return null;
};

Blockly.Python.procedures_defnoreturn = Blockly.Python.procedures_defreturn;

Blockly.Python.procedures_callreturn = function (block) {
  var funcName = Blockly.Python.variableDB_.getName(block.getFieldValue("NAME"), Blockly.PROCEDURE_CATEGORY_NAME),
      args = [];
  for (var i = 0; i < block.arguments_.length; i++) {
    args[i] = Blockly.Python.valueToCode(block, "ARG" + i, Blockly.Python.ORDER_NONE) || "None";
  }
  return [funcName + "(" + args.join(", ") + ")", Blockly.Python.ORDER_FUNCTION_CALL];
};

Blockly.Python.procedures_callnoreturn = function (block) {
  var tuple = Blockly.Python.procedures_callreturn(block);
  return tuple[0] + NEWLINE;
};

Blockly.Python.procedures_ifreturn = function (block) {
  var condition = Blockly.Python.valueToCode(block, "CONDITION", Blockly.Python.ORDER_NONE) || "False",
      code = "if " + condition + ":" + NEWLINE;
  if (Blockly.Python.STATEMENT_SUFFIX) {
    code += Blockly.Python.prefixLines(
        Blockly.Python.injectId(Blockly.Python.STATEMENT_SUFFIX, block),
        Blockly.Python.INDENT);
  }
  if (block.hasReturnValue_) {
    var value = Blockly.Python.valueToCode(block, "VALUE", Blockly.Python.ORDER_NONE) || "None";
    code += Blockly.Python.INDENT + "return " + value + NEWLINE;
  } else {
    code += Blockly.Python.INDENT + "return" + NEWLINE;
  }
  return code;
};
