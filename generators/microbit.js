// This file tells what each block should generate
// in terms of Microbit language.

/**
 * Microbit code generator.
 * @type {!Blockly.Generator}
 */
 Blockly.Microbit = new Blockly.Generator('Microbit');

 /**
  * List of illegal variable names.
  * This is not intended to be a security feature.  Blockly is 100% client-side,
  * so bypassing this list is trivial. This is intended to prevent users from
  * accidentally clobbering a built-in object or function.
  * @private
  */
 Blockly.Microbit.addReservedWords(
     'False,None,True,and,as,assert,break,class,continue,def,del,elif,else,' +
     'except,exec,finally,for,from,global,if,import,in,is,lambda,nonlocal,not,' +
     'or,pass,print,raise,return,try,while,with,yield,' +
     'NotImplemented,Ellipsis,__debug__,quit,exit,copyright,license,credits,' +
     'ArithmeticError,AssertionError,AttributeError,BaseException,' +
     'BlockingIOError,BrokenPipeError,BufferError,BytesWarning,' +
     'ChildProcessError,ConnectionAbortedError,ConnectionError,' +
     'ConnectionRefusedError,ConnectionResetError,DeprecationWarning,EOFError,' +
     'Ellipsis,EnvironmentError,Exception,FileExistsError,FileNotFoundError,' +
     'FloatingPointError,FutureWarning,GeneratorExit,IOError,ImportError,' +
     'ImportWarning,IndentationError,IndexError,InterruptedError,' +
     'IsADirectoryError,KeyError,KeyboardInterrupt,LookupError,MemoryError,' +
     'ModuleNotFoundError,NameError,NotADirectoryError,NotImplemented,' +
     'NotImplementedError,OSError,OverflowError,PendingDeprecationWarning,' +
     'PermissionError,ProcessLookupError,RecursionError,ReferenceError,' +
     'ResourceWarning,RuntimeError,RuntimeWarning,StandardError,' +
     'StopAsyncIteration,StopIteration,SyntaxError,SyntaxWarning,SystemError,' +
     'SystemExit,TabError,TimeoutError,TypeError,UnboundLocalError,' +
     'UnicodeDecodeError,UnicodeEncodeError,UnicodeError,' +
     'UnicodeTranslateError,UnicodeWarning,UserWarning,ValueError,Warning,' +
     'ZeroDivisionError,_,__build_class__,__debug__,__doc__,__import__,' +
     '__loader__,__name__,__package__,__spec__,abs,all,any,apply,ascii,' +
     'basestring,bin,bool,buffer,bytearray,bytes,callable,chr,classmethod,cmp,' +
     'coerce,compile,complex,copyright,credits,delattr,dict,dir,divmod,' +
     'enumerate,eval,exec,execfile,exit,file,filter,float,format,frozenset,' +
     'getattr,globals,hasattr,hash,help,hex,id,input,int,intern,isinstance,' +
     'issubclass,iter,len,license,list,locals,long,map,max,memoryview,min,' +
     'next,object,oct,open,ord,pow,print,property,quit,range,raw_input,reduce,' +
     'reload,repr,reversed,round,set,setattr,slice,sorted,staticmethod,str,' +
     'sum,super,tuple,type,unichr,unicode,vars,xrange,zip'
 );
 
 /**
  * Order of operation ENUMs.
  * http://docs.Microbit.org/reference/expressions.html#summary
  */
 Blockly.Microbit.ORDER_ATOMIC = 0;            // 0 "" ...
 Blockly.Microbit.ORDER_COLLECTION = 1;        // tuples, lists, dictionaries
 Blockly.Microbit.ORDER_STRING_CONVERSION = 1; // `expression...`
 Blockly.Microbit.ORDER_MEMBER = 2.1;          // . []
 Blockly.Microbit.ORDER_FUNCTION_CALL = 2.2;   // ()
 Blockly.Microbit.ORDER_EXPONENTIATION = 3;    // **
 Blockly.Microbit.ORDER_UNARY_SIGN = 4;        // + -
 Blockly.Microbit.ORDER_BITWISE_NOT = 4;       // ~
 Blockly.Microbit.ORDER_MULTIPLICATIVE = 5;    // * / // %
 Blockly.Microbit.ORDER_ADDITIVE = 6;          // + -
 Blockly.Microbit.ORDER_BITWISE_SHIFT = 7;     // << >>
 Blockly.Microbit.ORDER_BITWISE_AND = 8;       // &
 Blockly.Microbit.ORDER_BITWISE_XOR = 9;       // ^
 Blockly.Microbit.ORDER_BITWISE_OR = 10;       // |
 Blockly.Microbit.ORDER_RELATIONAL = 11;       // in, not in, is, is not, <, <=, >, >=, <>, !=, ==
 Blockly.Microbit.ORDER_LOGICAL_NOT = 12;      // not
 Blockly.Microbit.ORDER_LOGICAL_AND = 13;      // and
 Blockly.Microbit.ORDER_LOGICAL_OR = 14;       // or
 Blockly.Microbit.ORDER_CONDITIONAL = 15;      // if else
 Blockly.Microbit.ORDER_LAMBDA = 16;           // lambda
 Blockly.Microbit.ORDER_NONE = 99;             // (...)
 
 /**
  * List of outer-inner pairings that do NOT require parentheses.
  * @type {!Array.<!Array.<number>>}
  */
 Blockly.Microbit.ORDER_OVERRIDES = [
   [Blockly.Microbit.ORDER_FUNCTION_CALL, Blockly.Microbit.ORDER_MEMBER],
   [Blockly.Microbit.ORDER_FUNCTION_CALL, Blockly.Microbit.ORDER_FUNCTION_CALL],
   [Blockly.Microbit.ORDER_MEMBER, Blockly.Microbit.ORDER_MEMBER],
   [Blockly.Microbit.ORDER_MEMBER, Blockly.Microbit.ORDER_FUNCTION_CALL],
   [Blockly.Microbit.ORDER_LOGICAL_NOT, Blockly.Microbit.ORDER_LOGICAL_NOT],
   [Blockly.Microbit.ORDER_LOGICAL_AND, Blockly.Microbit.ORDER_LOGICAL_AND],
   [Blockly.Microbit.ORDER_LOGICAL_OR, Blockly.Microbit.ORDER_LOGICAL_OR]
 ];
 
 /**
  * Initialise the database of variable names.
  * @param {!Blockly.Workspace} workspace Workspace to generate code from.
  * @this {Blockly.Generator}
  */
 Blockly.Microbit.init = function(workspace) {
   Blockly.Microbit.PASS = this.INDENT + 'pass' + NEWLINE;
   // Create a dictionary of imports to be printed at the top of code.
   Blockly.Microbit.imports_ = Object.create(null);
   // Create a dictionary of constants to be printed after definitions.
   Blockly.Microbit.constants_ = Object.create(null);
   // Create a dictionary of object initializations to be printed after definitions.
   Blockly.Microbit.inits_ = Object.create(null);
   // Create a dictionary of code functions to be printed after user functions.
   Blockly.Microbit.codeFunctions_ = Object.create(null);
   // Create a dictionary mapping desired function names in codeFunctions_
   // to actual function names (to avoid collisions with user functions).
   Blockly.Microbit.functionNames_ = Object.create(null);
   // Create a dictionary of powers to be printed after definitions.
   Blockly.Microbit.powers_ = Object.create(null);
 
   // Create a dictionary of user functions to be printed after constants.
   Blockly.Microbit.userSetups_ = Object.create(null);
   // Create a dictionary of user setups to be printed after user functions.
   Blockly.Microbit.userFunctions_ = Object.create(null);
 
   if (!Blockly.Microbit.variableDB_) {
     Blockly.Microbit.variableDB_ =
         new Blockly.Names(Blockly.Microbit.RESERVED_WORDS_);
   } else {
     Blockly.Microbit.variableDB_.reset();
   }
   Blockly.Microbit.variableDB_.setVariableMap(workspace.getVariableMap());
 };
 
 /**
  * Prepend the generated code with the variable definitions.
  * @param {string} userLoop Generated code.
  * @return {string} Completed code.
  */
 Blockly.Microbit.finish = function(userLoop) {
   // Merge all functions defined by user and by blocks code.
   Blockly.Microbit.functions_ = Object.assign({},
       Blockly.Microbit.codeFunctions_,
       Blockly.Microbit.userFunctions_);
   
   // Create all definitions in head of Microbit code.
   var imports = Blockly.Microbit.convertObjectInLists(Blockly.Microbit.imports_),
       constants = Blockly.Microbit.convertObjectInLists(Blockly.Microbit.constants_),
       inits = Blockly.Microbit.convertObjectInLists(Blockly.Microbit.inits_),
       functions = Blockly.Microbit.convertObjectInLists(Blockly.Microbit.functions_),
       powers = Blockly.Microbit.convertObjectInLists(Blockly.Microbit.powers_);
 
   var head = imports.join("\n") + "\n\n" + constants.join("\n") + "\n" 
            + "\n\n" + inits.join("\n") + functions.join("\n\n") + powers.join("\n");
 
   // Add user setups after head of Microbit code (from block 'on_start').
   var userSetups = [];
   for (var setup in Blockly.Microbit.userSetups_) {
     userSetups.push(Blockly.Microbit.userSetups_[setup]);
   }
   var userSetup = userSetups.join("\n");
 
   // Deleting objects and reseting workspace variables.
   delete Blockly.Microbit.userSetups_;
   delete Blockly.Microbit.functionNames_;
   Blockly.Microbit.variableDB_.reset();
 
   return (head + userSetup).replace(/\n\n+/g, '\n\n').replace(/\n*$/, '\n\n') + userLoop;
 };
 
 /**
  * Adds a string of "import" code to be added at the top of sketch.
  * Once a include is added it will not get overwritten with new code.
  * @param {!string} importTag Identifier for this include code.
  * @param {!string} code Code to be included at the very top of the sketch.
  */
 Blockly.Microbit.addImport = function(importTag, code) {
   if (Blockly.Microbit.imports_[importTag] === undefined) {
     Blockly.Microbit.imports_[importTag] = code;
   }
 };
 
 /**
  * Adds a string of "constant" code to be added after the imports.
  * Once a include is added it will not get overwritten with new code.
  * @param {!string} constantTag Identifier for this include code.
  * @param {!string} code Code to be included at the very top of the sketch.
  */
 Blockly.Microbit.addConstant = function(constantTag, code) {
   if (Blockly.Microbit.constants_[constantTag] === undefined) {
     Blockly.Microbit.constants_[constantTag] = code;
   }
 };
 
 /**
  * Adds a string of "init" code to be added after functions.
  * Once a include is added it will not get overwritten with new code.
  * @param {!string} initTag Identifier for this include code.
  * @param {!string} code Code to be included at the very top of the sketch.
  */
 Blockly.Microbit.addInit = function(initTag, code) {
   if (Blockly.Microbit.inits_[initTag] === undefined) {
     Blockly.Microbit.inits_[initTag] = code;
   }
 };
 
 /**
  * Adds a string of "function" code to be added after constants.
  * Once a include is added it will not get overwritten with new code.
  * @param {!string} functionTag Identifier for this include code.
  * @param {!string} code Code to be included at the very top of the sketch.
  */
 Blockly.Microbit.addFunction = function(functionTag, code) {
   if (Blockly.Microbit.codeFunctions_[functionTag] === undefined) {
     Blockly.Microbit.codeFunctions_[functionTag] = code;
   }
 };
 
 /**
  * Adds a string of "powers" code to be added after initializations.
  * Once a include is added it will not get overwritten with new code.
  * @param {!string} initTag Identifier for this include code.
  * @param {!string} code Code to be included at the very top of the sketch.
  */
 Blockly.Microbit.addPowerOn = function(powerTag, code) {
   if (Blockly.Microbit.powers_[powerTag] === undefined) {
     Blockly.Microbit.powers_[powerTag] = code;
   }
 };
 
 /**
  * Naked values are top-level blocks with outputs that aren't plugged into
  * anything.
  * @param {string} line Line of generated code.
  * @return {string} Legal line of code.
  */
 Blockly.Microbit.scrubNakedValue = function(line) {
   return line + NEWLINE;
 };
 
 /**
  * Encode a string as a properly escaped Microbit string, complete with quotes.
  * @param {string} string Text to encode.
  * @return {string} Microbit string.
  * @private
  */
 Blockly.Microbit.quote_ = function(string) {
   // Can't use goog.string.quote since % must also be escaped.
   string = string.replace(/\\/g, '\\\\').replace(/\n/g, '\\\n');
 
   // Follow the CMicrobit behaviour of repr() for a non-byte string.
   var quote = '\'';
   if (string.indexOf('\'') !== -1) {
     if (string.indexOf('"') === -1) {
       quote = '"';
     } else {
       string = string.replace(/'/g, '\\\'');
     }
   };
   return quote + string + quote;
 };
 
 /**
  * Encode a string as a properly escaped multiline Microbit string, complete
  * with quotes.
  * @param {string} string Text to encode.
  * @return {string} Microbit string.
  * @private
  */
 Blockly.Microbit.multiline_quote_ = function(string) {
   // Can't use goog.string.quote since % must also be escaped.
   string = string.replace(/'''/g, '\\\'\\\'\\\'');
   return '\'\'\'' + string + '\'\'\'';
 };
 
 /**
  * Common tasks for generating Microbit from blocks.
  * Handles comments for the specified block and any connected value blocks.
  * Calls any statements following this block.
  * @param {!Blockly.Block} block The current block.
  * @param {string} code The Microbit code created for this block.
  * @param {boolean=} opt_thisOnly True to generate code for only this statement.
  * @return {string} Microbit code with comments and subsequent blocks added.
  * @private
  */
 Blockly.Microbit.scrub_ = function(block, code, opt_thisOnly) {
   var commentCode = '';
   // Only collect comments for blocks that aren't inline.
   if (!block.outputConnection || !block.outputConnection.targetConnection) {
     // Collect comment for this block.
     var comment = block.getCommentText();
     if (comment) {
       comment = Blockly.utils.string.wrap(comment,
           Blockly.Microbit.COMMENT_WRAP - 3);
       commentCode += Blockly.Microbit.prefixLines(comment + NEWLINE, '# ');
     }
     // Collect comments for all value arguments.
     // Don't collect comments for nested statements.
     for (var i = 0; i < block.inputList.length; i++) {
       if (block.inputList[i].type == Blockly.INPUT_VALUE) {
         var childBlock = block.inputList[i].connection.targetBlock();
         if (childBlock) {
           comment = Blockly.Microbit.allNestedComments(childBlock);
           if (comment) {
             commentCode += Blockly.Microbit.prefixLines(comment, '# ');
           }
         }
       }
     }
   }
   var nextBlock = block.nextConnection && block.nextConnection.targetBlock();
   var nextCode = opt_thisOnly ? '' : Blockly.Microbit.blockToCode(nextBlock);
   return commentCode + code + nextCode;
 };
 
 /**
  * Gets a property and adjusts the value, taking into account indexing, and
  * casts to an integer.
  * @param {!Blockly.Block} block The block.
  * @param {string} atId The property ID of the element to get.
  * @param {number=} opt_delta Value to add.
  * @param {boolean=} opt_negate Whether to negate the value.
  * @return {string|number}
  */
 Blockly.Microbit.getAdjustedInt = function(block, atId, opt_delta, opt_negate) {
   var delta = opt_delta || 0;
   if (block.workspace.options.oneBasedIndex) {
     delta--;
   }
   var defaultAtIndex = block.workspace.options.oneBasedIndex ? '1' : '0';
   var atOrder = delta ? Blockly.Microbit.ORDER_ADDITIVE :
       Blockly.Microbit.ORDER_NONE;
   var at = Blockly.Microbit.valueToCode(block, atId, atOrder) || defaultAtIndex;
 
   if (Blockly.isNumber(at)) {
     // If the index is a naked number, adjust it right now.
     at = parseInt(at, 10) + delta;
     if (opt_negate) {
       at = -at;
     }
   } else {
     // If the index is dynamic, adjust it in code.
     if (delta > 0) {
       at = 'int(' + at + ' + ' + delta + ')';
     } else if (delta < 0) {
       at = 'int(' + at + ' - ' + -delta + ')';
     } else {
       at = 'int(' + at + ')';
     }
     if (opt_negate) {
       at = '-' + at;
     }
   }
   return at;
 };
 
 /**
  * Convert an object into list of object values.
  * @param {Object} object
  * @return {!Array.<string>} list
  */
 Blockly.Microbit.convertObjectInLists = function(object) {
   var list = [];
   for (var i in object) {
     list.push(object[i]);
   }
   if (list.length) {
     list.push(NEWLINE);
   }
   delete object;
   return list;
 };
 
 /**
  * Get all blocks defined in workspace by type.
  * @return {object} list
  */
 Blockly.Microbit.getAllBlocksByType = function(type) {
   var blocks = Object.create(null)
   var blockDB = Blockly.Workspace.getAll()[0].blockDB_;
   for (block in blockDB) {
     if (blockDB[block].type == type) {
       blocks[block] = blockDB[block];
     }
   }
   return blocks;
 };
 
 