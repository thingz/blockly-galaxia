/**
 * @format
 * @fileoverview Input/Output generators for Galaxia.
 */

Blockly.GalaxiaGenerator.prototype.io_galaxia_pause = function (block) {
    this.addImport("time", IMPORT_TIME);
    var duration = this.valueToCode(block, "TIME", this.ORDER_NONE) || "0";
    return "time.sleep((" + duration + ")*" + block.getFieldValue("UNIT") + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.io_galaxia_timestamp = function (block) {
    return "print('/!\\ Timestamp non disponible sur ce firmware')" + NEWLINE
};

Blockly.GalaxiaGenerator.prototype.io_isTouchSensitiveButtonTouched = function (block) {
    var button = block.getFieldValue("BUTTON");
    var state = block.getFieldValue("STATE");
    switch (button) {
        case "n":
            this.addImport("import_galaxia_button_n", IMPORT_GALAXIA_TOUCH_N);
            break;
        case "s":
            this.addImport("import_galaxia_button_s", IMPORT_GALAXIA_TOUCH_S);
            break;
        case "e":
            this.addImport("import_galaxia_button_e", IMPORT_GALAXIA_TOUCH_E);
            break;
        case "w":
            this.addImport("import_galaxia_button_w", IMPORT_GALAXIA_TOUCH_W);
            break;
        default:
            break;
    }
    var code = "touch_" + button + "." + state + "touched()";
    return [code, this.ORDER_ATOMIC];
};

Blockly.GalaxiaGenerator.prototype.io_ifTouchSensitiveButtonTouched = function (block) {
    var button = block.getFieldValue("BUTTON");
    var state = block.getFieldValue("STATE");
    var branchCode = this.statementToCode(block, "DO") || this.PASS;
    switch (button) {
        case "n":
            this.addImport("import_galaxia_button_n", IMPORT_GALAXIA_TOUCH_N);
            break;
        case "s":
            this.addImport("import_galaxia_button_s", IMPORT_GALAXIA_TOUCH_S);
            break;
        case "e":
            this.addImport("import_galaxia_button_e", IMPORT_GALAXIA_TOUCH_E);
            break;
        case "w":
            this.addImport("import_galaxia_button_w", IMPORT_GALAXIA_TOUCH_W);
            break;
        default:
            break;
    }
    return "if touch_" + button + "." + state + "touched():" + NEWLINE + branchCode;
};

Blockly.GalaxiaGenerator.prototype.io_onTouchSensitiveButtonEvent = function (block) {
    var button = block.getFieldValue("BUTTON");
    var callbackName = "on_touch_" + button;
    var branchCode = this.statementToCode(block, "DO");
    if (!branchCode) {
        branchCode = this.PASS;
    }
    switch (button) {
        case "n":
            this.addImport("import_galaxia_button_n", IMPORT_GALAXIA_TOUCH_N);
            break;
        case "s":
            this.addImport("import_galaxia_button_s", IMPORT_GALAXIA_TOUCH_S);
            break;
        case "e":
            this.addImport("import_galaxia_button_e", IMPORT_GALAXIA_TOUCH_E);
            break;
        case "w":
            this.addImport("import_galaxia_button_w", IMPORT_GALAXIA_TOUCH_W);
            break;
        default:
            break;
    }

    var globalVar = this.getUsedGlobalVarInBlock(block, branchCode)
    var defEvent = "def " + callbackName + "(button):" + NEWLINE + globalVar + branchCode + NEWLINE;
    var callEvent = "touch_" + button +  ".on_touch_touched(" + callbackName + ")";
    this.codeFunctions_["%" + callbackName] = defEvent;
    this.userSetups_["%" + callbackName] = callEvent;
    return null;
};

Blockly.GalaxiaGenerator.prototype.io_isButtonPressed = function (block) {
    var code;
    var button = block.getFieldValue("BUTTON");
    var state = block.getFieldValue("STATE");

    if (button === "a") {
        this.addImport("import_galaxia_button_a", IMPORT_GALAXIA_BUTTON_A);
        code = "button_" + button + "." + state + "pressed()";
    } else if (button === "b") {
        this.addImport("import_galaxia_button_b", IMPORT_GALAXIA_BUTTON_B);
        code = "button_" + button + "." + state + "pressed()";
    } else {
        this.addImport("import_galaxia_button_a", IMPORT_GALAXIA_BUTTON_A);
        this.addImport("import_galaxia_button_b", IMPORT_GALAXIA_BUTTON_B);
        code = "button_a." + state + "pressed() and button_b." + state + "pressed()";
    }
    return [code, this.ORDER_ATOMIC];
};

Blockly.GalaxiaGenerator.prototype.io_ifButtonPressed = function (block) {
    var button = block.getFieldValue("BUTTON");
    var branchCode = this.statementToCode(block, "DO") || this.PASS;
    if (button === "a") {
        this.addImport("import_galaxia_button_a", IMPORT_GALAXIA_BUTTON_A);
    } else if (button === "b") {
        this.addImport("import_galaxia_button_b", IMPORT_GALAXIA_BUTTON_B);
    } else {
        this.addImport("import_galaxia_button_a", IMPORT_GALAXIA_BUTTON_A);
        this.addImport("import_galaxia_button_b", IMPORT_GALAXIA_BUTTON_B);
        return "if button_a." + block.getFieldValue("STATE") + "pressed() and button_b." + block.getFieldValue("STATE") + "pressed():" + NEWLINE + branchCode;
    }
    return "if button_" + button + "." + block.getFieldValue("STATE") + "pressed():" + NEWLINE + branchCode;
};

Blockly.GalaxiaGenerator.prototype.io_onButtonEvent = function (block) {
    var button = block.getFieldValue("BUTTON");
    var callbackName = "on_pressed_" + button;
    var branchCode = this.statementToCode(block, "DO");
    if (!branchCode) {
        branchCode = this.PASS;
    }
    switch (button) {
        case "a":
            this.addImport("import_galaxia_button_a", IMPORT_GALAXIA_BUTTON_A);
            break;
        case "b":
            this.addImport("import_galaxia_button_b", IMPORT_GALAXIA_BUTTON_B);
            break;
        default:
            break;
    }

    var globalVar = this.getUsedGlobalVarInBlock(block, branchCode)
    var defEvent = "def " + callbackName + "(button):" + NEWLINE+ globalVar + branchCode + NEWLINE;
    var callEvent = "button_" + button + ".on_button_pressed(" + callbackName + ")";
    this.codeFunctions_["%" + callbackName] = defEvent;
    this.userSetups_["%" + callbackName] = callEvent;
    return null;
};

Blockly.GalaxiaGenerator.prototype.io_configure_pin = function(){
    return "print('/!\\ Pins non disponible sur ce firmware')" + NEWLINE

}

Blockly.GalaxiaGenerator.prototype.io_analog_read = function(){
    return "print('/!\\ Pins non disponible sur ce firmware')" + NEWLINE
}

Blockly.GalaxiaGenerator.prototype.io_digital_read = function(){
    return "print('/!\\ Pins non disponible sur ce firmware')" + NEWLINE
}

Blockly.GalaxiaGenerator.prototype.io_digital_write = function(){
    return "print('/!\\ Pins non disponible sur ce firmware')" + NEWLINE
}

Blockly.GalaxiaGenerator.prototype.io_galaxia_digital_signal = function(block){
    let state = block.getFieldValue("BOOL");

    if(state == "HIGH"){
        return ["True", this.ORDER_ATOMIC]
    }else{
        return ["False",this.ORDER_ATOMIC]
    }
}

Blockly.GalaxiaGenerator.prototype.io_galaxia_readDigitalPin = function(block){
    return ["print('/!\\ Pins non disponible sur ce firmware')", Blockly.Python.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.io_galaxia_writeDigitalPin = function(block){
    return "print('/!\\ Pins non disponible sur ce firmware')" + NEWLINE
}

Blockly.GalaxiaGenerator.prototype.io_galaxia_readAnalogPin = function(block){
    return ["print('/!\\ Pins non disponible sur ce firmware')", Blockly.Python.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.io_galaxia_pwm = function(block){
    return "print('/!\\ Pins non disponible sur ce firmware')" + NEWLINE
}

Blockly.GalaxiaGenerator.prototype.io_galaxia_pwm_with_freq = function(block){
    return "print('/!\\ Pins non disponible sur ce firmware')" + NEWLINE
}

Blockly.GalaxiaGenerator.prototype.io_isTouchSensitiveButtonTouchedSignal = function(block){

    let value = "3.3 if "+this.io_isTouchSensitiveButtonTouched(block)[0]+" == True else 0";
    return  [value, this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.io_galaxia_readDigitalPinSignal = function(block){
    let value = "3.3 if "+this.io_galaxia_readDigitalPin(block)[0]+" == True else 0";
    return  [value, this.ORDER_ATOMIC]    
}

Blockly.GalaxiaGenerator.prototype.io_isButtonPressedSignal = function(block){
    let value = "3.3 if "+this.io_isButtonPressed(block)[0]+" == True else 0";
    return  [value, this.ORDER_ATOMIC]  
}