/**
 * @fileoverview Maqueen generators for Galaxia.
 */

Blockly.GalaxiaGenerator.prototype.codo_init = function(){
    return "print('/!\\ Codo non disponible sur ce firmware')" + NEWLINE

};

Blockly.GalaxiaGenerator.prototype.robots_galaxia_setCodoGo = function(block){
    return "print('/!\\ Codo non disponible sur ce firmware')" + NEWLINE
};

Blockly.GalaxiaGenerator.prototype.robots_galaxia_controlCodoMotor = function (block) {
    return "print('/!\\ Codo non disponible sur ce firmware')" + NEWLINE
};

Blockly.GalaxiaGenerator.prototype.robots_galaxia_stopCodoMotors = function (block) {
    return "print('/!\\ Codo non disponible sur ce firmware')" + NEWLINE
};