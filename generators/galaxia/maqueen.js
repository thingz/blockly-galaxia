/**
 * @fileoverview Maqueen generators for Galaxia.
 */

Blockly.GalaxiaGenerator.prototype.maqueen_init = function(){
    return "print('/!\\ Maqueen non disponible sur ce firmware')" + NEWLINE

};

Blockly.GalaxiaGenerator.prototype.robots_galaxia_setMaqueenGo = function(block){
    return "print('/!\\ Maqueen non disponible sur ce firmware')" + NEWLINE

};

Blockly.GalaxiaGenerator.prototype.robots_galaxia_controlMaqueenMotor = function (block) {
    return "print('/!\\ Maqueen non disponible sur ce firmware')" + NEWLINE
};

Blockly.GalaxiaGenerator.prototype.robots_galaxia_stopMaqueenMotors = function (block) {
    return "print('/!\\ Maqueen non disponible sur ce firmware')" + NEWLINE
};

Blockly.GalaxiaGenerator.prototype.robots_galaxia_readMaqueenPatrol = function (block) {
    return ["print('/!\\ Maqueen non disponible sur ce firmware')", Blockly.Python.ORDER_ATOMIC]
};

Blockly.GalaxiaGenerator.prototype.robots_galaxia_readMaqueenPlusPatrol = function (block) {
    return ["print('/!\\ Maqueen non disponible sur ce firmware')", Blockly.Python.ORDER_ATOMIC]
};

Blockly.GalaxiaGenerator.prototype.robots_galaxia_readMaqueenUltrasound = function(block){
    return ["print('/!\\ Maqueen non disponible sur ce firmware')", Blockly.Python.ORDER_ATOMIC]

}

Blockly.GalaxiaGenerator.prototype.robots_galaxia_readMaqueenPlusUltrasound = function(block){
    return ["print('/!\\ Maqueen non disponible sur ce firmware')", Blockly.Python.ORDER_ATOMIC]

}