Blockly.GalaxiaGenerator.prototype.motor_init = function(){
    this.addImport("import_drf0548", IMPORT_DRF0548);

};

Blockly.GalaxiaGenerator.prototype.motor_galaxia_speed = function(block){
    this.addImport("import_drf0548", IMPORT_DRF0548);

    let speed = this.valueToCode(block, "SPEED", this.ORDER_NONE) || "0";
    let motor = block.getFieldValue("MOTOR");
    let dir = block.getFieldValue("DIR")

    return "drf0548.motorRun("+motor+", "+dir+", "+speed+")"+NEWLINE
}

Blockly.GalaxiaGenerator.prototype.motor_galaxia_stop = function(block){
    this.addImport("import_drf0548", IMPORT_DRF0548);

    let motor = block.getFieldValue("MOTOR");

    return "drf0548.motorStop("+motor+")"+NEWLINE
}

Blockly.GalaxiaGenerator.prototype.motor_galaxia_stop_all = function(block){
    this.addImport("import_drf0548", IMPORT_DRF0548);

    return "drf0548.motorStopAll()"+NEWLINE
}

Blockly.GalaxiaGenerator.prototype.motor_galaxia_servo = function(block){
    this.addImport("import_drf0548", IMPORT_DRF0548);

    let angle = this.valueToCode(block, "ANGLE", this.ORDER_NONE) || "0";
    let motor = block.getFieldValue("MOTOR");

    return "drf0548.servo("+motor+", "+angle+")"+NEWLINE
}

Blockly.GalaxiaGenerator.prototype.motor_galaxia_step_42 = function(block){
    this.addImport("import_drf0548", IMPORT_DRF0548);

    let degree = this.valueToCode(block, "DEGREE", this.ORDER_NONE) || "0";
    let motor = block.getFieldValue("MOTOR");
    let dir = block.getFieldValue("DIR")

    return "drf0548.stepperDegree_42("+motor+", "+dir+", "+degree+")"+NEWLINE
}

Blockly.GalaxiaGenerator.prototype.motor_galaxia_step_28 = function(block){
    this.import("import_drf0548", IMPORT_DRF0548);

    let degree = this.valueToCode(block, "DEGREE", this.ORDER_NONE) || "0";
    let motor = block.getFieldValue("MOTOR");
    let dir = block.getFieldValue("DIR")

    return "drf0548.stepperDegree_28("+motor+", "+dir+", "+degree+")"+NEWLINE
}

Blockly.GalaxiaGenerator.prototype.motor_galaxia_step_42_turn = function(block){
    this.import("import_drf0548", IMPORT_DRF0548);

    let turn = this.valueToCode(block, "TURN", this.ORDER_NONE) || "0";
    let motor = block.getFieldValue("MOTOR");
    let dir = block.getFieldValue("DIR")

    return "drf0548.stepperTurn_42("+motor+", "+dir+", "+turn+")"+NEWLINE
}

Blockly.GalaxiaGenerator.prototype.motor_galaxia_step_28_turn = function(block){
    this.import("import_drf0548", IMPORT_DRF0548);

    let turn = this.valueToCode(block, "TURN", this.ORDER_NONE) || "0";
    let motor = block.getFieldValue("MOTOR");
    let dir = block.getFieldValue("DIR")

    return "drf0548.stepperTurn_28("+motor+", "+dir+", "+turn+")"+NEWLINE
}

Blockly.GalaxiaGenerator.prototype.motor_galaxia_step_dual_turn = function(block){
    this.import("import_drf0548", IMPORT_DRF0548);

    let turn1 = this.valueToCode(block, "TURN1", this.ORDER_NONE) || "0";
    let turn2 = this.valueToCode(block, "TURN2", this.ORDER_NONE) || "0";
    let type = block.getFieldValue("TYPE");
    let dir1 = block.getFieldValue("DIR1")
    let dir2 = block.getFieldValue("DIR2")

    return "drf0548.stepperTurnDual_42("+type+", "+dir1+", "+turn1+","+dir2+", "+turn2+")"+NEWLINE
}

Blockly.GalaxiaGenerator.prototype.motor_galaxia_step_dual = function(block){
    this.import("import_drf0548", IMPORT_DRF0548);

    let degree1 = this.valueToCode(block, "DEGREE1", this.ORDER_NONE) || "0";
    let degree2 = this.valueToCode(block, "DEGREE2", this.ORDER_NONE) || "0";
    let type = block.getFieldValue("TYPE");
    let dir1 = block.getFieldValue("DIR1")
    let dir2 = block.getFieldValue("DIR2")

    return "drf0548.stepperTurnDual_42("+type+", "+dir1+", "+degree1+","+dir2+", "+degree2+")"+NEWLINE
}