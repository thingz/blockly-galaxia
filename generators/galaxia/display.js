/**
 * @fileoverview Display generators for Galaxia.
 */

// CONTROL GALAXIA LED
Blockly.GalaxiaGenerator.prototype.display_galaxia_led_set_colors = function (block) {
    this.addImport("import_galaxia_led", IMPORT_GALAXIA_LED);
    var red = this.valueToCode(block, "RED", this.ORDER_NONE) || "0";
    var green = this.valueToCode(block, "GREEN", this.ORDER_NONE) || "0";
    var blue = this.valueToCode(block, "BLUE", this.ORDER_NONE) || "0";
    return "led.set_colors(" + red + ", " + green + ", " + blue + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.display_galaxia_led_set_red = function (block) {
    this.addImport("import_galaxia_led", IMPORT_GALAXIA_LED);
    var red = this.valueToCode(block, "RED", this.ORDER_NONE) || "0";
    return "led.set_red(" + red + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.display_galaxia_led_set_green = function (block) {
    this.addImport("import_galaxia_led", IMPORT_GALAXIA_LED);
    var green = this.valueToCode(block, "GREEN", this.ORDER_NONE) || "0";
    return "led.set_green(" + green + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.display_galaxia_led_set_blue = function (block) {
    this.addImport("import_galaxia_led", IMPORT_GALAXIA_LED);
    var blue = this.valueToCode(block, "BLUE", this.ORDER_NONE) || "0";
    return "led.set_blue(" + blue + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.display_galaxia_led_get_red = function () {
    this.addImport("import_galaxia_led", IMPORT_GALAXIA_LED);
    return ["led.get_red()", this.ORDER_NONE];
};

Blockly.GalaxiaGenerator.prototype.display_galaxia_led_get_green = function () {
    this.addImport("import_galaxia_led", IMPORT_GALAXIA_LED);
    return ["led.get_green()", this.ORDER_NONE];
};

Blockly.GalaxiaGenerator.prototype.display_galaxia_led_get_blue = function () {
    this.addImport("import_galaxia_led", IMPORT_GALAXIA_LED);
    return ["led.get_blue()", this.ORDER_NONE];
};

// CONTROL GALAXIA DIPLAY
Blockly.GalaxiaGenerator.prototype.display_galaxia_show = function (block) {
    var value = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "''";
    return "print(" + value + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.display_galaxia_clear = function (block) {

    this.codeFunctions_["%display_clear"] = "def display_clear():" 
    + NEWLINE + this.INDENT + "print('\x1b[2J', end='')"

    return "display_clear()" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.show_number = function (block) {
    var number = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "0";
    return "print(" + number + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.show_string = function (block) {
    var text = this.valueToCode(block, "TEXT", this.ORDER_NONE) || "''";
    return "print(" + text + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.display_galaxia_plot_add_point = function(block){
    this.addImport("import_galaxia_display", IMPORT_GALAXIA_DISPLAY);
    var point = this.valueToCode(block, "POINT", this.ORDER_NONE) || 0;
    return "display.plot.add_point(" + point + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.display_galaxia_plot_set_y_scale = function(block){
    this.addImport("import_galaxia_display", IMPORT_GALAXIA_DISPLAY);
    var min = this.valueToCode(block, "MIN", this.ORDER_NONE) || 0;
    var max = this.valueToCode(block, "MAX", this.ORDER_NONE) || 100;
    return "display.plot.set_y_scale(" + min + "," + max + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.display_galaxia_set_mode = function(block){
    this.addImport("import_galaxia_display", IMPORT_GALAXIA_DISPLAY);
    var mode = block.getFieldValue("MODE");
    return "display." + mode  + "()" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.display_galaxia_animate_function = function(block){
    this.addImport("import_galaxia_display", IMPORT_GALAXIA_DISPLAY);
    var callbackName = "on_display_animate_cb";
    var branchCode = this.statementToCode(block, "DO");
    var result = this.valueToCode(block, "POINT", this.ORDER_NONE) || 50;
    var interval = this.valueToCode(block, "INTERVAL", this.ORDER_NONE) || 1;

    var globalVar = this.getUsedGlobalVarInBlock(block, branchCode)
    var defEvent = "def " + callbackName + "():" + NEWLINE + globalVar + branchCode + this.INDENT + "return " + result + NEWLINE;
    var callEvent = "display.plot.set_animate_function(" + callbackName + ", " + interval*1000 +")";
    this.codeFunctions_["%" + callbackName] = defEvent;
    this.userSetups_["%" + callbackName] = callEvent;
    return null;
}
