/**
 * @format
 * @fileoverview Communication generators for Galaxia.
 */

// Galaxia radio

Blockly.GalaxiaGenerator.prototype.communication_galaxia_radioSendValue = function (block) {
    this.addImport("import_galaxia_radio", IMPORT_GALAXIA_RADIO);
    var value = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "''";
    return "radio.send(" + value + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_onRadioReceived = function (block) {
    this.addImport("import_galaxia_radio", IMPORT_GALAXIA_RADIO);
    // this.addPowerOn("radio", "radio.on()");
    var branchCode = this.statementToCode(block, "DO") || this.PASS;
    var dataVar = this.variableDB_.getName(block.getFieldValue("VALUE"), Blockly.VARIABLE_CATEGORY_NAME);
    return dataVar + " = radio.receive()" + NEWLINE + "if " + dataVar + ":" + NEWLINE + branchCode;
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_RadioDataReceived = function () {
    this.addImport("import_galaxia_radio", IMPORT_GALAXIA_RADIO);
    return ["radio.receive()", this.ORDER_NONE];
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_radioSetChannel = function(block){
    this.addImport("import_galaxia_radio", IMPORT_GALAXIA_RADIO);
    var channel = this.valueToCode(block, "CHANNEL", this.ORDER_NONE) || "1";
    return "radio.set_channel("+channel+")"+ NEWLINE;
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_radioSetChannelDropdown = function(block){
    this.addImport("import_galaxia_radio", IMPORT_GALAXIA_RADIO);
    var channel = block.getFieldValue("CHANNEL")
    return "radio.set_channel("+channel+")"+ NEWLINE;
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_radioGetChannel = function(block){
    this.addImport("import_galaxia_radio", IMPORT_GALAXIA_RADIO);
    return ["radio.get_channel()", this.ORDER_NONE];
}

// Galaxia WiFi

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_connect_basic = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    var SSID = this.valueToCode(block, "SSID", this.ORDER_NONE) || "''";
    var PWD = this.valueToCode(block, "PWD", this.ORDER_NONE) || "''";
    return "simpleWifi.connect(" + SSID + ", " + PWD + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_connect = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    var SSID = this.valueToCode(block, "SSID", this.ORDER_NONE) || "''";
    var PWD = this.valueToCode(block, "PWD", this.ORDER_NONE) || "''";
    var IP = this.valueToCode(block, "IP", this.ORDER_NONE) || "''";
    var args = {};
    var code = "simpleWifi.connect(" + SSID + ", " + PWD;
    args["IP"] = IP.length > 2;
    for (var arg in args) {
        if (arg == "IP" && args[arg] == true) {
            code += ", static_ip=" + IP;
        }
    }
    return code + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_connect_complete = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    var SSID = this.valueToCode(block, "SSID", this.ORDER_NONE) || "''";
    var PWD = this.valueToCode(block, "PWD", this.ORDER_NONE) || "''";
    var IP = this.valueToCode(block, "IP", this.ORDER_NONE) || "''";
    var PORT = this.valueToCode(block, "PORT", this.ORDER_NONE) || "0";
    var args = {};
    var code = "simpleWifi.connect(" + SSID + ", " + PWD;
    args["IP"] = IP.length > 2;
    args["PORT"] = PORT > 0;
    for (var arg in args) {
        if (arg == "IP" && args[arg] == true) {
            code += ", static_ip=" + IP;
        } else if (arg == "PORT" && args[arg] == true) {
            code += ", server_port=" + PORT;
        }
    }
    return code + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_connect_eth = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);

    var args = {};
    var code = "simpleWifi.connect_eth()";
    
    return code + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_connect_eth_with_ip = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    
    var IP = this.valueToCode(block, "IP", this.ORDER_NONE) || "''";
    
    var args = {};
    var code = "simpleWifi.connect_eth(";
    args["IP"] = IP.length > 2;

    for (var arg in args) {
        if (arg == "IP" && args[arg] == true) {
            code += "static_ip=" + IP;
        } 
    }
    return code + ")" + NEWLINE;
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_create_ap = function(block){
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    var SSID = this.valueToCode(block, "SSID", this.ORDER_NONE) || "''";
    var PWD = this.valueToCode(block, "PWD", this.ORDER_NONE) || "''";
    return "simpleWifi.start_access_point(" + SSID + ", " + PWD + ")" + NEWLINE;
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_getLastConnectedIp = function () {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    return ["simpleWifi.get_last_connected_ip()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_getMyIp = function () {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    return ["simpleWifi.get_my_ip()", Blockly.Python.ORDER_ATOMIC];
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_receive_basic = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    var branchCode = this.statementToCode(block, "DO") || this.PASS;
    var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VALUE"), Blockly.VARIABLE_CATEGORY_NAME);
    // retrait de l'indentation induite par la méthode statementToCode
    branchCode = branchCode.substring(this.INDENT.length);
    var regex = new RegExp("\n"+this.INDENT, "g");
    branchCode = branchCode.replace(regex, "\n");
    return dataVar + " = simpleWifi.receive(stopSequence=\"\\n\", reuseClient=True)" + NEWLINE + branchCode;
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_receive_delimiter = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    var branchCode = this.statementToCode(block, "DO") || this.PASS;
    var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VALUE"), Blockly.VARIABLE_CATEGORY_NAME);
    var delimiter = block.getFieldValue("DELIMITER");
    // retrait de l'indentation induite par la méthode statementToCode
    branchCode = branchCode.substring(this.INDENT.length);
    var regex = new RegExp("\n"+this.INDENT, "g");
    branchCode = branchCode.replace(regex, "\n");
    return dataVar + " = simpleWifi.receive(stopSequence=\""+delimiter+"\", reuseClient=True)" + NEWLINE + branchCode;
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_receive = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    var branchCode = this.statementToCode(block, "DO") || this.PASS;
    var IP = this.valueToCode(block, "IP", this.ORDER_NONE) || "''";
    var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VALUE"), Blockly.VARIABLE_CATEGORY_NAME);
    // retrait de l'indentation induite par la méthode statementToCode
    branchCode = branchCode.substring(this.INDENT.length);
    var regex = new RegExp("\n"+this.INDENT, "g");
    branchCode = branchCode.replace(regex, "\n");
    // var length egal 2 if the string is empty : "''"
    return dataVar + " = simpleWifi.receive(stopSequence=\"\\n\" " + (IP.length > 2 ? "ip=" + IP : "") + ")" + NEWLINE + branchCode;
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_receive_complete = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    var branchCode = this.statementToCode(block, "DO") || this.PASS;
    var IP = this.valueToCode(block, "IP", this.ORDER_NONE) || "''";
    var TIMEOUT = this.valueToCode(block, "TIMEOUT", this.ORDER_NONE) || "0";
    var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VALUE"), Blockly.VARIABLE_CATEGORY_NAME);
    // var length egal 2 if the string is empty : "''"
    return (
        dataVar +
        " = simpleWifi.receive(stopSequence=\"\\r\" " +
        (IP.length > 2 ? "ip=" + IP : "") +
        (IP.length > 2 && TIMEOUT > 0 ? ", " : "") +
        (TIMEOUT > 0 ? "timeout=" + TIMEOUT : "") +
        ")" +
        NEWLINE +
        "if " +
        dataVar +
        ":" +
        NEWLINE +
        branchCode
    );
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_server_receive_async = function(block){
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    this.userSetups_["%simple_wifi_receive_async"] = "simple_wifi_server_data_received = ''"

    this.codeFunctions_["%simple_wifi_receive_async"] = "def server_receive():"+NEWLINE
    +this.INDENT+"global simple_wifi_server_data_received"+NEWLINE
    +this.INDENT+'r = simpleWifi.receive(stopSequence="\\n", reuseClient=False, block=False)'+NEWLINE
    +this.INDENT+'if r:'+NEWLINE
    +this.INDENT+this.INDENT+"simple_wifi_server_data_received = r"

    return ["simple_wifi_server_data_received", Blockly.Python.ORDER_ATOMIC];

}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_server_receive_event = function(block){
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addImport("import_supervisor", IMPORT_SUPERVISOR);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    this.userSetups_["%simple_wifi_receive_async"] = "simple_wifi_server_data_received = ''"

    let branch = this.statementToCode(block, "DO", this.ORDER_NONE) || "''";
    // console.log(branch)
    branch = this.INDENT+branch;
    var regex = new RegExp("\n", "g");
    branch = branch.replace(regex, "\n"+this.INDENT);

    let dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("DATA"), Blockly.VARIABLE_CATEGORY_NAME);

    this.codeFunctions_["%simple_wifi_receive_async"] = "def server_receive():"+NEWLINE
    +this.INDENT+"global simple_wifi_server_data_received"+NEWLINE
    +this.INDENT+dataVar+' = simpleWifi.receive(stopSequence="\\n", reuseClient=False, block=False)'+NEWLINE
    +this.INDENT+'if '+dataVar+':'+NEWLINE
    +this.INDENT+this.INDENT+"simple_wifi_server_data_received = "+dataVar+NEWLINE
    +branch

    this.userSetups_["%simple_wifi_receive_event"] = 'supervisor.add_background_user_callback(server_receive)' + NEWLINE
    return "";
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_server_is_client_connected = function(block){
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);

    return ["simpleWifi.is_client_connected()", Blockly.Python.ORDER_ATOMIC];
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_server_respond = function(block){
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);

    var data = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "''";
    data = "str("+data+")"
    return "simpleWifi.send_to_client("+data+", stopSequence=\"\\n\")" + NEWLINE;
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_server_respond_with_delimiter = function(block){
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);

    var delimiter = block.getFieldValue("DELIMITER");
    var data = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "''";
    data = "str("+data+")"
    return "simpleWifi.send_to_client("+data+", stopSequence=\""+delimiter+"\")" + NEWLINE;
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_send_basic = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    var VALUE = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "''";
    var IP = this.valueToCode(block, "IP", this.ORDER_NONE) || "''";
    return "simpleWifi.send(" + VALUE + ", " + IP + ", stopSequence=\"\\n\")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_send_basic_with_delimiter = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    var VALUE = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "''";
    var IP = this.valueToCode(block, "IP", this.ORDER_NONE) || "''";
    var delimiter = block.getFieldValue("DELIMITER");
    return "simpleWifi.send(" + VALUE + ", " + IP + ", stopSequence='"+delimiter+"')" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_send_wait_respond_with_delimiter = function (block){
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    var VALUE = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "''";
    var IP = this.valueToCode(block, "IP", this.ORDER_NONE) || "''";
    var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("RESPONSE_VALUE"), Blockly.VARIABLE_CATEGORY_NAME);
    var branchCode = this.statementToCode(block, "DO") || this.PASS;
    // retrait de l'indentation induite par la méthode statementToCode
    branchCode = branchCode.substring(this.INDENT.length);
    var regex = new RegExp("\n"+this.INDENT, "g");
    branchCode = branchCode.replace(regex, "\n");
    var delimiter = block.getFieldValue("DELIMITER");
    return dataVar+" = simpleWifi.send(" + VALUE + ", " + IP + ", stopSequence='"+delimiter+"', waitResponse=True)" + NEWLINE + branchCode;
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_send_wait_respond = function (block){
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    var VALUE = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "''";
    var IP = this.valueToCode(block, "IP", this.ORDER_NONE) || "''";
    var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("RESPONSE_VALUE"), Blockly.VARIABLE_CATEGORY_NAME);
    var branchCode = this.statementToCode(block, "DO") || this.PASS;
    // retrait de l'indentation induite par la méthode statementToCode
    branchCode = branchCode.substring(this.INDENT.length);
    var regex = new RegExp("\n"+this.INDENT, "g");
    branchCode = branchCode.replace(regex, "\n");
    return dataVar+" = simpleWifi.send(" + VALUE + ", " + IP + ", stopSequence=\"\\n\", waitResponse=True)" + NEWLINE + branchCode;
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleWifi_send_complete = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    var VALUE = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "''";
    var IP = this.valueToCode(block, "IP", this.ORDER_NONE) || "''";
    var PORT = this.valueToCode(block, "PORT", this.ORDER_NONE) || "0";
    return "simpleWifi.send(" + VALUE + ", " + IP + (PORT > 0 ? ", port=" + PORT : "") + ")" + NEWLINE;
};

// Galaxia HTTP

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleHttp_respond_basic = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    this.addConstant("galaxia_http_init", GALAXIA_HTTP_INIT);
    var VALUE = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "''";
    return "http.respond(" + VALUE + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleHttp_respond_complete = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    this.addConstant("galaxia_http_init", GALAXIA_HTTP_INIT);
    var VALUE = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "''";
    var CODE = this.valueToCode(block, "CODE", this.ORDER_NONE) || "''";
    return "http.respond(" + VALUE + (CODE > 0 ? ", code=" + CODE : "") + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleHttp_wait_request = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    this.addConstant("galaxia_http_init", GALAXIA_HTTP_INIT);
    var branchCode = this.statementToCode(block, "DO") || "";
    var dataVar = Blockly.Python.variableDB_.getName(block.getFieldValue("VALUE"), Blockly.VARIABLE_CATEGORY_NAME);
    // retrait de l'indentation induite par la méthode statementToCode
    branchCode = branchCode.substring(this.INDENT.length);
    var regex = new RegExp("\n"+this.INDENT, "g");
    branchCode = branchCode.replace(regex, "\n");
    return "response = http.wait_request()" + NEWLINE + dataVar + " = response.get_url()" + NEWLINE + branchCode;
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleHttp_generate_page = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addImport("import_supervisor", IMPORT_SUPERVISOR);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    this.addConstant("galaxia_http_init", GALAXIA_HTTP_INIT);
    var branchCode = this.statementToCode(block, "DO") || "";
    var reload = this.valueToCode(block, "RELOAD", this.ORDER_NONE) || 10;

    // re-indent statemnt
    branchCode = this.INDENT+branchCode;
    var regex = new RegExp("\n", "g");
    branchCode = branchCode.replace(regex, "\n"+this.INDENT);

    branchCode = branchCode.substring(this.INDENT.length);
    var replace = "\n" + this.INDENT;
    var regex = new RegExp(replace, "g");
    branchCode = branchCode.replace(regex, "\n");

    var get_request = this.INDENT + "thgz_response = ''"

    var globalVar = this.getUsedGlobalVarInBlock(block, branchCode)
    var defEvent = "def index_page():" + NEWLINE
    + globalVar + get_request + NEWLINE 
    + branchCode 
    + this.INDENT+"http.respond_with_html(thgz_response, template=\"autoreload.html\", tags=[(\"reload\", "+reload*1000+")])"+ NEWLINE

    var callEvent = 'http.add_web_page_url("", index_page)'+ NEWLINE + "web_pages = lambda: http.handle_web_pages()"+ NEWLINE +"supervisor.set_background_user_callback(web_pages)";

    this.codeFunctions_["%user_callback"] = defEvent;
    this.userSetups_["%user_callback"] = callEvent;

    return ""
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleHttp_generate_page_no_refresh = function (block) {
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addImport("import_supervisor", IMPORT_SUPERVISOR);
    this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
    this.addConstant("galaxia_http_init", GALAXIA_HTTP_INIT);
    var branchCode = this.statementToCode(block, "DO") || "";

    // re-indent statemnt
    branchCode = this.INDENT+branchCode;
    var regex = new RegExp("\n", "g");
    branchCode = branchCode.replace(regex, "\n"+this.INDENT);

    branchCode = branchCode.substring(this.INDENT.length);
    var replace = "\n" + this.INDENT;
    var regex = new RegExp(replace, "g");
    branchCode = branchCode.replace(regex, "\n");

    var get_request = this.INDENT + "thgz_response = ''"

    var globalVar = this.getUsedGlobalVarInBlock(block, branchCode)
    var defEvent = "def index_page():" + NEWLINE
    + globalVar + get_request + NEWLINE 
    + branchCode 
    + this.INDENT+"http.respond_with_html(thgz_response, template=\"autoreload.html\")"+ NEWLINE

    var callEvent = 'http.add_web_page_url("", index_page)'+ NEWLINE + "web_pages = lambda: http.handle_web_pages()"+ NEWLINE +"supervisor.set_background_user_callback(web_pages)";

    this.codeFunctions_["%user_callback"] = defEvent;
    this.userSetups_["%user_callback"] = callEvent;

    return ""
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleHttp_add_to_page = function (block) {
    var VALUE = this.valueToCode(block, "DATA", this.ORDER_NONE) || "''";
    return "thgz_response += '<div>'+str("+VALUE + ") + '</div>'" + NEWLINE
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleHttp_add_button_to_page = function (block) {
    var VALUE = this.valueToCode(block, "TITLE", this.ORDER_NONE) || "''";
    var CMD = this.valueToCode(block, "CMD", this.ORDER_NONE) || "''";
    CMD = CMD.replace(/'/g, '"')
    VALUE = VALUE.replace(/'/g, "") 

    return "thgz_response += '<div><button class=\"tgz_button\" onclick=\\'cmd(\"/\"+"+CMD+", this)\\'>"+VALUE + "</button></div>'" + NEWLINE
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleHttp_button_event = function(block){

    var branchCode = this.statementToCode(block, "DO") || "";
    var cmd = this.valueToCode(block, "CMD", this.ORDER_NONE) || "";
    cmd = cmd.replace(/'/g, '')

    if(branchCode.length == 0){
        branchCode = this.INDENT+'pass'
    }
    // re-indent statemnt
    branchCode = this.INDENT+branchCode;
    var regex = new RegExp("\n", "g");
    branchCode = branchCode.replace(regex, "\n"+this.INDENT);

    branchCode = branchCode.substring(this.INDENT.length);
    var replace = "\n" + this.INDENT;
    var regex = new RegExp(replace, "g");
    branchCode = branchCode.replace(regex, "\n");

    var globalVar = this.getUsedGlobalVarInBlock(block, branchCode)
    var defEvent = "def on_web_button_"+cmd+"():" + NEWLINE
    
    if(globalVar.length > 0){
        defEvent += globalVar + NEWLINE
    }
    defEvent += branchCode 

    var callEvent = 'http.add_web_button("'+cmd+'", on_web_button_'+cmd+')'+ NEWLINE;

    this.codeFunctions_["%on_web_button_"+cmd] = defEvent;
    this.userSetups_["%on_web_button_"+cmd] = callEvent;

    return ""
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleHttp_add_gauge_to_page = function (block) {
    var VALUE = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "0";
    var MIN = this.valueToCode(block, "MIN", this.ORDER_NONE) || "0";
    var MAX = this.valueToCode(block, "MAX", this.ORDER_NONE) || "100";

    var TITLE = this.valueToCode(block, "TITLE", this.ORDER_NONE) || "";
    return "thgz_response += '<div>'+str("+TITLE+")+': <meter value=\"'+str("+VALUE+")+'\" min=\"'+str("+MIN+")+'\" max=\"'+str("+MAX+")+'\">'+str("+TITLE+")+'</meter>'+str("+VALUE+")+'</div>'" + NEWLINE
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simpleHttp_add_gauge_to_page_without_text = function (block) {
    var VALUE = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "0";
    var MIN = this.valueToCode(block, "MIN", this.ORDER_NONE) || "0";
    var MAX = this.valueToCode(block, "MAX", this.ORDER_NONE) || "100";

    var TITLE = this.valueToCode(block, "TITLE", this.ORDER_NONE) || "";
    return "thgz_response += '<div>'+str("+TITLE+")+': <meter value=\"'+str("+VALUE+")+'\" min=\"'+str("+MIN+")+'\" max=\"'+str("+MAX+")+'\">'+str("+TITLE+")+'</meter>'+'</div>'" + NEWLINE
};
// Galaxia simple HTTP REQUEST

// this.communication_galaxia_SimpleHttpRequest_getURL = function (block) {
//     this.addConstant("galaxia_wifi_init", GALAXIA_WIFI_INIT);
//     this.addConstant("galaxia_http_init", GALAXIA_HTTP_INIT);
//     var reponse = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "''";
//     return reponse + ".get_url()" + NEWLINE;
// };


Blockly.GalaxiaGenerator.prototype.communication_galaxia_simple_mqtt_receive = function (block) {
    var branchCode = this.statementToCode(block, "DO") || "";

    var topic = Blockly.Python.variableDB_.getName(block.getFieldValue("TOPIC"), Blockly.VARIABLE_CATEGORY_NAME);
    var message = Blockly.Python.variableDB_.getName(block.getFieldValue("VALUE"), Blockly.VARIABLE_CATEGORY_NAME);

    var globalVar = this.getUsedGlobalVarInBlock(block, branchCode, [topic, message])
    var defEvent = "def simple_mqtt_on_message("+topic+","+message+"):" + NEWLINE
    + globalVar + (globalVar.length ? NEWLINE : "")
    + (branchCode.length ? branchCode : this.INDENT+ "pass" + NEWLINE)

    this.codeFunctions_["%simple_mqtt_on_message"] = defEvent;

    return ""
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simple_mqtt_receive_v2 = function (block) {
    var branchCode = this.statementToCode(block, "DO") || "";

    var topic = this.valueToCode(block, "TOPIC", this.ORDER_NONE);
    var message = this.valueToCode(block, "VALUE", this.ORDER_NONE);

    var globalVar = this.getUsedGlobalVarInBlock(block, branchCode, [topic, message])
    var defEvent = "def simple_mqtt_on_message("+topic+","+message+"):" + NEWLINE
    + globalVar + (globalVar.length ? NEWLINE : "")
    + (branchCode.length ? branchCode : this.INDENT+ "pass" + NEWLINE)

    this.codeFunctions_["%simple_mqtt_on_message"] = defEvent;

    return ""
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simple_mqtt_receive_in_topic  = function (block) {
    var branchCode = this.statementToCode(block, "DO") || "";

    var topic = this.valueToCode(block, "TOPIC", this.ORDER_NONE);
    var message = this.valueToCode(block, "VALUE", this.ORDER_NONE);

    var globalVar = this.getUsedGlobalVarInBlock(block, branchCode, [topic, message])
    var timestamp = Date.now()
    var defEvent = "def simple_mqtt_on_message_cb_"+timestamp+"("+message+"):" + NEWLINE
    + globalVar + (globalVar.length ? NEWLINE : "")
    + (branchCode.length ? branchCode : this.INDENT+ "pass" + NEWLINE)

    this.codeFunctions_["%simple_mqtt_on_message_"+topic] = defEvent;
    this.codeFunctions_["%simple_mqtt_on_message_global"+topic] = "globals()['simple_mqtt_on_message_'+str(hash("+topic+"))] = simple_mqtt_on_message_cb_"+timestamp;

    return ""
};

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simple_mqtt_on_connect = function(block){
    var branchCode = this.statementToCode(block, "DO") || "";

    var globalVar = this.getUsedGlobalVarInBlock(block, branchCode)
    var defEvent = "def simple_mqtt_on_connected():" + NEWLINE
    + globalVar + (globalVar.length ? NEWLINE : "")
    + (branchCode.length ? branchCode : this.INDENT+ "pass" + NEWLINE)

    this.codeFunctions_["%simple_mqtt_on_connect"] = defEvent;

    return ""
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simple_mqtt_on_disconnect = function(block){
    var branchCode = this.statementToCode(block, "DO") || "";

    var globalVar = this.getUsedGlobalVarInBlock(block, branchCode)
    var defEvent = "def simple_mqtt_on_disconnected():" + NEWLINE
    + globalVar + (globalVar.length ? NEWLINE : "")
    + (branchCode.length ? branchCode : this.INDENT+ "pass" + NEWLINE)

    this.codeFunctions_["%simple_mqtt_on_disconnect"] = defEvent;

    return ""
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simple_mqtt_connect_complete = function(block){
    this.addImport("import_galaxia_wifi", IMPORT_GALAXIA_WIFI);
    this.addImport("import_galaxia_mqtt", IMPORT_GALAXIA_MQTT);
    this.addImport("import_supervisor", IMPORT_SUPERVISOR);
    
    if(!this.codeFunctions_["%simple_mqtt_on_disconnect"] || !this.codeFunctions_["%simple_mqtt_on_disconnect"].length){
        this.codeFunctions_["%simple_mqtt_on_disconnect"] = "def simple_mqtt_on_disconnected():" + NEWLINE + this.INDENT + "pass" + NEWLINE
    }

    if(!this.codeFunctions_["%simple_mqtt_on_connect"] || !this.codeFunctions_["%simple_mqtt_on_connect"].length){
        this.codeFunctions_["%simple_mqtt_on_connect"] = "def simple_mqtt_on_connected():" + NEWLINE + this.INDENT + "pass" + NEWLINE
    }

    if(!this.codeFunctions_["%simple_mqtt_on_message"] || !this.codeFunctions_["%simple_mqtt_on_message"].length){
        this.codeFunctions_["%simple_mqtt_on_message"] = "def simple_mqtt_on_message(topic, message):" + NEWLINE + this.INDENT + "pass" + NEWLINE
    }

    var broker = this.valueToCode(block, "BROKER", this.ORDER_NONE) || "''";
    var username = this.valueToCode(block, "USERNAME", this.ORDER_NONE) || "'"+Blockly.utils.genUid()+"'";
    var password = this.valueToCode(block, "PASSWORD", this.ORDER_NONE) || "''";
    var port = this.valueToCode(block, "PORT", this.ORDER_NONE) || 1883;

    if(username == "''"){
        username = "'"+Blockly.utils.genUid()+"'"
    }

    this.userSetups_["%simple_mqtt_instance"] = 'simpleMqtt = simple_mqtt.SimpleMQTT('+username+', '+password+', '+broker+', '+port+', simple_wifi._simple_tcp_socket_pool, simple_mqtt_on_connected, simple_mqtt_on_disconnected, simple_mqtt_on_message)'
    this.userSetups_["%simple_mqtt_cb"] = 'simpleMqttCb = lambda: simpleMqtt.process()'
    this.userSetups_["%simple_mqtt_supervisor"] = ""
    + "try:" + NEWLINE
    + this.INDENT + 'supervisor.add_background_user_callback(simpleMqttCb, period=300)' + NEWLINE
    + "except TypeError:" + NEWLINE
    + this.INDENT + 'supervisor.add_background_user_callback(simpleMqttCb)' + NEWLINE

    return "simpleMqtt.connect()" + NEWLINE
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simple_mqtt_subscribe = function(block){
    var topic = this.valueToCode(block, "TOPIC", this.ORDER_NONE) || "''";
    this.constants_["%simple_mqtt_cb_"+topic] = "globals()['simple_mqtt_on_message_'+str(hash("+topic+"))] =  None"+ NEWLINE 
    
    return "simpleMqtt.subscribe("+topic+", globals()['simple_mqtt_on_message_'+str(hash("+topic+"))])"+ NEWLINE
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simple_mqtt_publish = function(block){
    var topic = this.valueToCode(block, "TOPIC", this.ORDER_NONE) || "''";
    var message = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "''";
    
    return "simpleMqtt.publish("+topic+", "+message+")"+ NEWLINE
}

Blockly.GalaxiaGenerator.prototype.communication_galaxia_simple_mqtt_is_connected = function(block){
    var topic = this.valueToCode(block, "TOPIC", this.ORDER_NONE) || "''";
    var message = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "''";
    
    return ["simpleMqtt.is_connected()", Blockly.Python.ORDER_ATOMIC];
}