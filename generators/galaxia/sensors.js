/**
 * @fileoverview Sensors generators for Galaxia.
 */

// Galaxia board sensors

Blockly.GalaxiaGenerator.prototype.sensors_galaxia_getAcceleration = function (block) {
    this.addImport("import_galaxia_accelerometer", IMPORT_GALAXIA_ACCELEROMETER);
    var axis = block.getFieldValue("AXIS");
    if (axis !== "get_values") {
        return ["accelerometer.get_" + axis + "()", this.ORDER_ATOMIC];
    } else {
        return ["accelerometer.get_values()", this.ORDER_ATOMIC];
    }
};

Blockly.GalaxiaGenerator.prototype.sensors_getLight = function () {
    this.addImport("import_galaxia_led", IMPORT_GALAXIA_LED);
    return ["led.read_light_level()", this.ORDER_ATOMIC];
};

Blockly.GalaxiaGenerator.prototype.sensors_calibrateCompass = function () {
    this.addImport("import_galaxia_compass", IMPORT_GALAXIA_COMPASS);
    return "compass.calibrate()" + NEWLINE;
};

// PAS ENCORE SUPORTÉE PAR GALAXIA
Blockly.GalaxiaGenerator.prototype.sensors_getCompass = function () {
    this.addImport("import_galaxia_compass", IMPORT_GALAXIA_COMPASS);
    return ["compass.heading()", this.ORDER_ATOMIC];
};

Blockly.GalaxiaGenerator.prototype.sensors_getTemperature = function (block) {
    this.addImport("import_galaxia_temperature", IMPORT_GALAXIA_TEMPERATURE);
    var code = "temperature()";
    switch (block.getFieldValue("UNIT")) {
        case "FAHRENHEIT":
            code += "*9/5 + 32";
            break;
        case "KELVIN":
            code += " + 273.15";
            break;
    }
    return [code, this.ORDER_ATOMIC];
};

Blockly.GalaxiaGenerator.prototype.sensors_galaxia_getMagneticForce = function (block) {
    this.addImport("import_galaxia_compass", IMPORT_GALAXIA_COMPASS);
    var axis = block.getFieldValue("AXIS");
    if (axis !== "get_values") {
        return ["compass.get_" + axis + "()", this.ORDER_ATOMIC];
    } else {
        return ["compass.get_values()", this.ORDER_ATOMIC];
    }
};

Blockly.GalaxiaGenerator.prototype.sensors_galaxia_onAccelerometerEvent = function (block) {
    this.addImport("import_galaxia_accelerometer", IMPORT_GALAXIA_ACCELEROMETER);
    var gesture = block.getFieldValue("GESTURE");
    var callbackName = "on_gesture_" + gesture.replace(" ", "_");
    var accelFunc = "on_gesture";
    var branchCode = this.statementToCode(block, "DO");
    
    if (!branchCode) {
        branchCode = this.PASS;
    }

    var globalVar = Blockly.Python.getUsedGlobalVarInBlock(block, branchCode)

    var defEvent = "def " + callbackName + "(gesture):" + NEWLINE + globalVar + branchCode + NEWLINE;
    var callEvent = "accelerometer."+ accelFunc + "(\"" + gesture + "\", " + callbackName + ")";
    
    this.codeFunctions_["%" + callbackName] = defEvent;
    this.userSetups_["%" + callbackName] = callEvent;
    return null;
};

Blockly.GalaxiaGenerator.prototype.sensors_galaxia_getAccelerationSignal = function(block){
    return this.map(this.sensors_galaxia_getAcceleration(block), -2000, 2000, 0, 3.3)
}

Blockly.GalaxiaGenerator.prototype.sensors_galaxia_getMagneticForceSignal = function(block){
    return this.map(this.sensors_galaxia_getMagneticForce(block), -215, 215, 0, 3.3)    
}

Blockly.GalaxiaGenerator.prototype.sensors_getLightSignal = function(block){
    return this.map(this.sensors_getLight(block), 0, 255, 0, 3.3)    
}

Blockly.GalaxiaGenerator.prototype.sensors_getTemperatureSignal = function(block){
    return this.map(this.sensors_getTemperature(block), -20, 80, 0, 3.3)    
}

Blockly.GalaxiaGenerator.prototype.sensors_getCompassSignal = function(block){
    return this.map(this.sensors_getCompass(block), 0, 360, 0, 3.3)    
}