/**
 * @format
 * @fileoverview Actuators generators for Galaxia.
 */

Blockly.GalaxiaGenerator.prototype.actuators_play_frequency = function (block) {
    this.addImport("import_galaxia_sound", IMPORT_GALAXIA_SOUND);
    var freq = this.valueToCode(block, "FREQ", this.ORDER_NONE) || "0";
    return "sound.set_frequency(" + freq + ")" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.actuators_galaxia_play_frequency_with_duration = function (block) {
    this.addImport("import_galaxia_sound", IMPORT_GALAXIA_SOUND);
    this.addImport("time", IMPORT_TIME);
    var freq = this.valueToCode(block, "FREQUENCY", this.ORDER_NONE) || "0";
    var duration = this.valueToCode(block, "DURATION", this.ORDER_NONE) || "0";
    return "sound.set_frequency(" + freq + ")" + NEWLINE + "time.sleep((" + duration + ")*0.001)" + NEWLINE + "sound.set_frequency(0)" + NEWLINE;
};

Blockly.GalaxiaGenerator.prototype.actuators_galaxia_sound_power = function (block) {
    this.addImport("import_galaxia_sound", IMPORT_GALAXIA_SOUND);
    var freq = this.valueToCode(block, "FREQUENCY", this.ORDER_NONE) || "0";
    var mode = block.getFieldValue("SWITCH") || "ON";
    switch (mode) {
        case "ON":
            return "sound.play(True, " + freq + ")" + NEWLINE;
        case "OFF":
            return "sound.play(False, " + freq + ")" + NEWLINE;
    }
    throw Error("Unhandled combination (actuators_galaxia_sound_power).");
};

Blockly.GalaxiaGenerator.prototype.actuators_galaxia_play_set_volume = function (block) {
    this.addImport("import_galaxia_sound", IMPORT_GALAXIA_SOUND);
    var volume = this.valueToCode(block, "VOLUME", this.ORDER_NONE) || "100";
    return "sound.set_volume(" + volume + ")" + NEWLINE;
}

Blockly.GalaxiaGenerator.prototype.actuators_galaxia_play_sample = function (block) {
    this.addImport("import_galaxia_sound", IMPORT_GALAXIA_SOUND);
    var filename = this.valueToCode(block, "FILE", this.ORDER_NONE);
    return "sound.play_sample(" + filename + ")" + NEWLINE;
}
