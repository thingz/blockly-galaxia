Blockly.GalaxiaGenerator.prototype.time_galaxia_timer_start = function (block) {
    return "print('/!\\ Chrono non disponible sur ce firmware')"+ NEWLINE
};

Blockly.GalaxiaGenerator.prototype.time_galaxia_timer_measure = function (block) {
    
    return ["print('/!\\ Chrono non disponible sur ce firmware')", Blockly.Python.ORDER_ATOMIC];
};