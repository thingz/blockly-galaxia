/**
 * @fileoverview Maqueen generators for Galaxia.
 */

// Blockly.GalaxiaGenerator.prototype.maqueen_init = function(){
//     return "print('/!\\ Maqueen non disponible sur ce firmware')" + NEWLINE

// };

Blockly.GalaxiaGenerator.prototype.kitronik_access_move = function(block){
    return "print('/!\\ Kitronik non disponible sur ce firmware')" + NEWLINE

};

Blockly.GalaxiaGenerator.prototype.kitronik_access_sound = function (block) {
    return "print('/!\\ Kitronik non disponible sur ce firmware')" + NEWLINE
};

Blockly.GalaxiaGenerator.prototype.kitronik_lamp_deadband = function (block) {
    return ["print('/!\\ Kitronik non disponible sur ce firmware')", Blockly.Python.ORDER_ATOMIC]

};

Blockly.GalaxiaGenerator.prototype.kitronik_lamp_read_light_level = function (block) {
    return ["print('/!\\ Kitronik non disponible sur ce firmware')", Blockly.Python.ORDER_ATOMIC]
};

Blockly.GalaxiaGenerator.prototype.kitronik_lamp_set_light = function (block) {
    return "print('/!\\ Kitronik non disponible sur ce firmware')"
};

Blockly.GalaxiaGenerator.prototype.kitronik_lamp_set_lamp_deadband = function(block){
    return "print('/!\\ Kitronik non disponible sur ce firmware')" + NEWLINE
}

Blockly.GalaxiaGenerator.prototype.kitronik_stop_set_traffic_light = function(block){
    return "print('/!\\ Kitronik non disponible sur ce firmware')" + NEWLINE
}

Blockly.GalaxiaGenerator.prototype.kitronik_stop_set_color = function (block) {
    return "print('/!\\ Kitronik non disponible sur ce firmware')" + NEWLINE
};
