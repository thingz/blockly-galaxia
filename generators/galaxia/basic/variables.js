/**
 * @fileoverview Variables generators for Micro:bit.
 */

goog.provide("Blockly.Galaxia.variables");

Blockly.GalaxiaGenerator.prototype.variables_get = Blockly.Python.variables_get;

Blockly.GalaxiaGenerator.prototype.variables_set = Blockly.Python.variables_set;

Blockly.GalaxiaGenerator.prototype.variables_increment = Blockly.Python.variables_increment;

Blockly.GalaxiaGenerator.prototype.variables_force_type = Blockly.Python.variables_force_type;

Blockly.GalaxiaGenerator.prototype.variables_type_of = Blockly.Python.variables_type_of;

Blockly.GalaxiaGenerator.prototype.variables_local = function(block){
    var varName = block.getFieldValue("VAR");
    return [varName, Blockly.Python.ORDER_ATOMIC];
}