// /**
//  * @license
//  * Copyright 2018 Google LLC
//  * SPDX-License-Identifier: Apache-2.0
//  */

// /**
//  * @fileoverview Generating Galaxia Python for dynamic variable blocks.
//  * @author fenichel@google.com (Rachel Fenichel)
//  */
// "use strict";

// goog.provide("Blockly.Galaxia.variablesDynamic");

// goog.require("Blockly.Galaxia");
// goog.require("Blockly.Galaxia.variables");

// // Python is dynamically typed.
// Blockly.Galaxia["variables_get_dynamic"] = Blockly.Galaxia["variables_get"];
// Blockly.Galaxia["variables_set_dynamic"] = Blockly.Galaxia["variables_set"];
