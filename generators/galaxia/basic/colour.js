/**
 * @fileoverview Colour generators for Galaxia.
 */

Blockly.GalaxiaGenerator.prototype.colour_picker = Blockly.Python.colour_picker;

Blockly.GalaxiaGenerator.prototype.colour_random = Blockly.Python.colour_random;

Blockly.GalaxiaGenerator.prototype.colour_rgb = Blockly.Python.colour_rgb;

Blockly.GalaxiaGenerator.prototype.colour_blend = Blockly.Python.colour_blend;