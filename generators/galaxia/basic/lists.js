/**
 * @fileoverview Lists generators for Galaxia.
 */

Blockly.GalaxiaGenerator.prototype.lists_create_with = Blockly.Python.lists_create_with;

Blockly.GalaxiaGenerator.prototype.lists_repeat = Blockly.Python.lists_repeat;

Blockly.GalaxiaGenerator.prototype.lists_length = Blockly.Python.lists_length;

Blockly.GalaxiaGenerator.prototype.math_on_list = Blockly.Python.math_on_list;

Blockly.GalaxiaGenerator.prototype.lists_isEmpty = Blockly.Python.lists_isEmpty;

Blockly.GalaxiaGenerator.prototype.lists_indexOf = Blockly.Python.lists_indexOf;

Blockly.GalaxiaGenerator.prototype.lists_getIndex = Blockly.Python.lists_getIndex;

Blockly.GalaxiaGenerator.prototype.lists_setIndex = Blockly.Python.lists_setIndex;

Blockly.GalaxiaGenerator.prototype.lists_getSublist = Blockly.Python.lists_getSublist;

Blockly.GalaxiaGenerator.prototype.lists_sort = Blockly.Python.lists_sort;

Blockly.GalaxiaGenerator.prototype.lists_split = Blockly.Python.lists_split;

Blockly.GalaxiaGenerator.prototype.lists_reverse = Blockly.Python.lists_reverse;
