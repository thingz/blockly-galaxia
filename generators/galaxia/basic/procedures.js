/**
 * @fileoverview Procedures generators for Galaxia.
 */

// Blockly.Python.procedures_defreturn is a generic method used for defreturn and defnoreturn
Blockly.GalaxiaGenerator.prototype.procedures_defreturn = Blockly.Python.procedures_defreturn;

Blockly.GalaxiaGenerator.prototype.procedures_defnoreturn = Blockly.GalaxiaGenerator.prototype.procedures_defreturn;

Blockly.GalaxiaGenerator.prototype.procedures_callreturn = Blockly.Python.procedures_callreturn;

Blockly.GalaxiaGenerator.prototype.procedures_callnoreturn = Blockly.Python.procedures_callnoreturn;

Blockly.GalaxiaGenerator.prototype.procedures_ifreturn = Blockly.Python.procedures_ifreturn;
