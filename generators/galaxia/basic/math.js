/**
 * @fileoverview Math generators for Galaxia.
 */

// If any new block imports any library, add that library name here.
//Blockly.GalaxiaGenerator.addReservedWords("math,random");

Blockly.GalaxiaGenerator.prototype.math_number = Blockly.Python.math_number;

Blockly.GalaxiaGenerator.prototype.math_arithmetic = Blockly.Python.math_arithmetic;

Blockly.GalaxiaGenerator.prototype.math_single = Blockly.Python.math_single;

Blockly.GalaxiaGenerator.prototype.math_constant = Blockly.Python.math_constant;

Blockly.GalaxiaGenerator.prototype.math_number_property = Blockly.Python.math_number_property;

// Rounding functions have a single operand.
Blockly.GalaxiaGenerator.prototype.math_round = Blockly.GalaxiaGenerator.prototype.math_single;

// Trigonometry functions have a single operand.
Blockly.GalaxiaGenerator.prototype.math_trig = Blockly.GalaxiaGenerator.prototype.math_single;

Blockly.GalaxiaGenerator.prototype.math_map = Blockly.Python.math_map;

Blockly.GalaxiaGenerator.prototype.math_modulo = Blockly.Python.math_modulo;

Blockly.GalaxiaGenerator.prototype.math_constrain = Blockly.Python.math_constrain;

Blockly.GalaxiaGenerator.prototype.math_random_int = Blockly.Python.math_random_int;

Blockly.GalaxiaGenerator.prototype.math_random_float = Blockly.Python.math_random_float;

Blockly.GalaxiaGenerator.prototype.math_atan2 = Blockly.Python.math_atan2;
