/**
 * @fileoverview Loops generators for Galaxia.
 */

Blockly.GalaxiaGenerator.prototype.controls_repeat = Blockly.Python.controls_repeat;

Blockly.GalaxiaGenerator.prototype.controls_whileUntil = Blockly.Python.controls_whileUntil;

Blockly.GalaxiaGenerator.prototype.controls_for = Blockly.Python.controls_for;

Blockly.GalaxiaGenerator.prototype.controls_forEach = Blockly.Python.controls_forEach;

Blockly.GalaxiaGenerator.prototype.controls_flow_statements = Blockly.Python.controls_flow_statements;

Blockly.GalaxiaGenerator.prototype.setup = function (block) {    
    let setup = this.statementToCode(block, "SETUP_DO");
    let forever = this.statementToCode(block, "LOOP_DO");
    var indent = this.INDENT;
    if (!forever) forever = indent + "pass" + NEWLINE;
    //In general a statement is some code inside a block so statementToCode will automatically add this.INDENT to the generated code in oder to construct the indentation.
    //In this case we want the setup statement to be at the top left, so we remove one level of indentation
    setup = setup.substring(indent.length);
    var replace = "\n" + indent;
    var regex = new RegExp(replace, "g");
    setup = setup.replace(regex, "\n");
    let code = setup + NEWLINE + "while True:" + NEWLINE + forever;
    return code;
    
};

Blockly.GalaxiaGenerator.prototype.on_start = function () {
    let setup = this.statementToCode(this, "DO");
    
    //In general a statement is some code inside a block so statementToCode will automatically add this.INDENT to the generated code in oder to construct the indentation.
    //In this case we want the setup statement to be at the top left, so we remove one level of indentation
    var indent = this.INDENT;
    setup = setup.substring(indent.length);
    var replace = "\n" + indent;
    var regex = new RegExp(replace, "g");
    setup = setup.replace(regex, "\n");
   
    this.userSetups_["%user_on_start"] = setup;

    return null;
};

Blockly.GalaxiaGenerator.prototype.forever = function () {
    let forever = this.statementToCode(this, "DO");
    var indent = this.INDENT;
    if (!forever) forever = indent + "pass" + NEWLINE;
    
    let code = "while True:" + NEWLINE + forever;
    return code;
};
