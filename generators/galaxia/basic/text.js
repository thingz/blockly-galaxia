/**
 * @fileoverview Text generators for Micro:bit.
 */

Blockly.GalaxiaGenerator.prototype.text_comment = Blockly.Python.text_comment;

Blockly.GalaxiaGenerator.prototype.text = Blockly.Python.text;

/**
 * Enclose the provided value in "str(...)" function.
 * Leave string literals alone.
 * @param {string} value Code evaluating to a value.
 * @return {string} Code evaluating to a string.
 * @private
 */
Blockly.GalaxiaGenerator.prototype.text.forceString_ = Blockly.Python.text.forceString_;

/**
 * Regular expression to detect a single-quoted string literal.
 */
Blockly.GalaxiaGenerator.prototype.text.forceString_.strRegExp = /^\s*'([^']|\\')*'\s*$/;

Blockly.GalaxiaGenerator.prototype.text_join = Blockly.Python.text_join;

Blockly.GalaxiaGenerator.prototype.text_append = Blockly.Python.text_append;

Blockly.GalaxiaGenerator.prototype.text_length = Blockly.Python.text_length;

Blockly.GalaxiaGenerator.prototype.text_isEmpty = Blockly.Python.text_isEmpty;

Blockly.GalaxiaGenerator.prototype.text_indexOf = Blockly.Python.text_indexOf;

Blockly.GalaxiaGenerator.prototype.text_charAt = Blockly.Python.text_charAt;

Blockly.GalaxiaGenerator.prototype.text_getSubstring = Blockly.Python.text_getSubstring;

Blockly.GalaxiaGenerator.prototype.text_changeCase = Blockly.Python.text_changeCase;

Blockly.GalaxiaGenerator.prototype.text_trim = Blockly.Python.text_trim;

Blockly.GalaxiaGenerator.prototype.text_count = Blockly.Python.text_count;

Blockly.GalaxiaGenerator.prototype.text_replace = Blockly.Python.text_replace;

Blockly.GalaxiaGenerator.prototype.text_reverse = Blockly.Python.text_reverse;

Blockly.GalaxiaGenerator.prototype.text_to_number = function(block){
    let s = this.valueToCode(block, "TEXT", this.ORDER_NONE) || "";
    
    this.codeFunctions_["%convert_to_number"] = "def convert_to_number(s):"+NEWLINE
    +this.INDENT+"try:"+NEWLINE
    +this.INDENT+this.INDENT+'n = int(s)'+NEWLINE
    +this.INDENT+this.INDENT+'return n'+NEWLINE
    +this.INDENT+'except ValueError:'+NEWLINE
    +this.INDENT+this.INDENT+'return float(s)'+NEWLINE
    
    return ["convert_to_number("+s+")", this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.text_to_code = function(block){
    let code = block.getFieldValue("CODE")
    code = code.replace('    ', this.INDENT);
    if(code.length > 0 && !code.endsWith(NEWLINE)){
        code += NEWLINE
    }
    return code
}
