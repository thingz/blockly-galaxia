/**
 * @fileoverview Logic generators for Galaxia.
 */

Blockly.GalaxiaGenerator.prototype.controls_if = Blockly.Python.controls_if;

Blockly.GalaxiaGenerator.prototype.logic_compare = Blockly.Python.logic_compare;

Blockly.GalaxiaGenerator.prototype.logic_operation = Blockly.Python.logic_operation;

Blockly.GalaxiaGenerator.prototype.logic_boolean = Blockly.Python.logic_boolean;

Blockly.GalaxiaGenerator.prototype.logic_negate = Blockly.Python.logic_negate;

Blockly.GalaxiaGenerator.prototype.logic_null = Blockly.Python.logic_null;

Blockly.GalaxiaGenerator.prototype.logic_ternary = Blockly.Python.logic_ternary;