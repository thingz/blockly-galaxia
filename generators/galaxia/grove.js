Blockly.GalaxiaGenerator.prototype.grove_galaxia_temperature = function (block) {
    
    return ["print('/!\\ Grove non disponible sur ce firmware')", Blockly.Python.ORDER_ATOMIC]
};

Blockly.GalaxiaGenerator.prototype.grove_galaxia_ultrasonic = function (block){

    return ["print('/!\\ Grove non disponible sur ce firmware')", Blockly.Python.ORDER_ATOMIC]   
}


Blockly.GalaxiaGenerator.prototype.grove_galaxia_dht11 = function (block){

    return ["print('/!\\ Grove non disponible sur ce firmware')", Blockly.Python.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.grove_galaxia_dht11_signal = function(block){
    return ["print('/!\\ Grove non disponible sur ce firmware')", Blockly.Python.ORDER_ATOMIC] 
}

Blockly.GalaxiaGenerator.prototype.grove_galaxia_temperature_signal = function(block){
    return ["print('/!\\ Grove non disponible sur ce firmware')", Blockly.Python.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.grove_galaxia_servo = function(block){
    return "print('/!\\ Grove non disponible sur ce firmware')"
}

Blockly.GalaxiaGenerator.prototype.grove_galaxia_ir_sender = function(block){
    return "print('/!\\ Grove non disponible sur ce firmware')"
}