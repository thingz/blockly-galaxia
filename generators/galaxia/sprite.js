Blockly.GalaxiaGenerator.prototype.sprite_rectangle_create = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let r = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x = this.valueToCode(block, "X", this.ORDER_NONE) || 0;
    let y = this.valueToCode(block, "Y", this.ORDER_NONE) || 0;
    let width = this.valueToCode(block, "WIDTH", this.ORDER_NONE) || 10;
    let height = this.valueToCode(block, "HEIGHT", this.ORDER_NONE) || 20;
    let color = this.valueToCode(block, "COLOR", this.ORDER_NONE) || "(255, 0, 0)";
    let hex = color.match('([0-9]+), ([0-9]+), ([0-9]+)')

    hex[1] = parseInt(hex[1]).toString(16).padStart(2, '0')
    hex[2] = parseInt(hex[2]).toString(16).padStart(2, '0')
    hex[3] = parseInt(hex[3]).toString(16).padStart(2, '0')

    return r+" = sprites.rectangle("+x+", "+y+", 0x"+hex[1]+hex[2]+hex[3]+", "+width+", "+height+")\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_rectangle_set_x_y = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let r = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x_y = block.getFieldValue("X_Y")
    let value = this.valueToCode(block, "VALUE", this.ORDER_NONE) || 0;

    return r+"."+x_y+" = "+value+"\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_rectangle_get_x_y = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let r = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x_y = block.getFieldValue("X_Y")
    return [r+"."+x_y, this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.sprite_rectangle_set_width_height = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let r = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let w_h = block.getFieldValue("W_H")
    let value = this.valueToCode(block, "VALUE", this.ORDER_NONE) || 0;

    return r+"."+w_h+" = "+value+"\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_rectangle_get_width_height = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let r = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let w_h = block.getFieldValue("W_H")

    return [r+"."+w_h, this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.sprite_rectangle_set_color = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let r = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let color = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "(255, 0, 0)";
    let hex = color.match('([0-9]+), ([0-9]+), ([0-9]+)')

    hex[1] = parseInt(hex[1]).toString(16).padStart(2, '0')
    hex[2] = parseInt(hex[2]).toString(16).padStart(2, '0')
    hex[3] = parseInt(hex[3]).toString(16).padStart(2, '0')

    return r+".color = 0x"+hex[1]+hex[2]+hex[3]+"\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_rectangle_get_color = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let r = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    
    return [r+".color", this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.sprite_rectangle_set_hidden = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let r = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let hidden = block.getFieldValue("HIDDEN")

    return r+".hidden ="+hidden+"\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_rectangle_get_hidden = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let r = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return ["not "+r+".hidden", this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.sprite_rectangle_move_by = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let pixels = this.valueToCode(block, "VALUE", this.ORDER_NONE) || 0;

    return i+".moveBy("+pixels+")\n"
}   

Blockly.GalaxiaGenerator.prototype.sprite_rectangle_turn= function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let direction =  block.getFieldValue("DIRECTION")
    let angle = this.valueToCode(block, "VALUE", this.ORDER_NONE) || 0;
    return i+".direction += "+direction+"*"+angle+"\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_rectangle_bounce_if_edge= function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return i+".bounceIfEdge()\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_image_create = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x = this.valueToCode(block, "X", this.ORDER_NONE) || 0;
    let y = this.valueToCode(block, "Y", this.ORDER_NONE) || 0;
    let scale = this.valueToCode(block, "SCALE", this.ORDER_NONE) || 1;
    let path = this.valueToCode(block, "PATH", this.ORDER_NONE) || "'/img.bmp'";

    return i+" = sprites.image("+x+", "+y+", "+scale+", "+path+")\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_image_set_x_y = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x_y = block.getFieldValue("X_Y")
    let value = this.valueToCode(block, "VALUE", this.ORDER_NONE) || 0;

    return i+"."+x_y+" = "+value+"\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_image_get_x_y = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x_y = block.getFieldValue("X_Y")

    return [i+"."+x_y, this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.sprite_image_get_width_height = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let r = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let w_h = block.getFieldValue("W_H")

    return [r+"."+w_h, this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.sprite_image_set_scale = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let scale = this.valueToCode(block, "SCALE", this.ORDER_NONE) || 0;

    return i+".scale = "+scale+"\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_image_get_scale = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return [i+".scale", this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.sprite_icon_set_color = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let color = this.valueToCode(block, "VALUE", this.ORDER_NONE) || "(255, 0, 0)";
    let hex = color.match('([0-9]+), ([0-9]+), ([0-9]+)')

    hex[1] = parseInt(hex[1]).toString(16).padStart(2, '0')
    hex[2] = parseInt(hex[2]).toString(16).padStart(2, '0')
    hex[3] = parseInt(hex[3]).toString(16).padStart(2, '0')

    return i+".color = 0x"+hex[1]+hex[2]+hex[3]+"\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_icon_get_color = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    
    return [i+".color", this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.sprite_image_set_hidden = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let hidden = block.getFieldValue("HIDDEN")

    return i+".hidden ="+hidden+"\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_image_get_hidden = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return ["not "+i+".hidden", this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.sprite_image_move_by = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let pixels = this.valueToCode(block, "VALUE", this.ORDER_NONE) || 0;

    return i+".moveBy("+pixels+")\n"
}   

Blockly.GalaxiaGenerator.prototype.sprite_image_turn= function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let direction =  block.getFieldValue("DIRECTION")
    let angle = this.valueToCode(block, "VALUE", this.ORDER_NONE) || 0;
    return i+".direction += "+direction+"*"+angle+"\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_image_bounce_if_edge= function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return i+".bounceIfEdge()\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_icon_create = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x = this.valueToCode(block, "X", this.ORDER_NONE) || 0;
    let y = this.valueToCode(block, "Y", this.ORDER_NONE) || 0;
    let scale = this.valueToCode(block, "SCALE", this.ORDER_NONE) || 1;
    let name = block.getFieldValue("NAME") || "cross";
    let color = this.valueToCode(block, "COLOR", this.ORDER_NONE) || "(255, 0, 0)";
    let hex = color.match('([0-9]+), ([0-9]+), ([0-9]+)')

    hex[1] = parseInt(hex[1]).toString(16).padStart(2, '0')
    hex[2] = parseInt(hex[2]).toString(16).padStart(2, '0')
    hex[3] = parseInt(hex[3]).toString(16).padStart(2, '0')

    return i+" = sprites.icon("+x+", "+y+", "+scale+", '"+name+"', 0x"+hex[1]+hex[2]+hex[3]+")\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_icon_set_x_y = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x_y = block.getFieldValue("X_Y")
    let value = this.valueToCode(block, "VALUE", this.ORDER_NONE) || 0;

    return i+"."+x_y+" = "+value+"\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_icon_get_x_y = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let x_y = block.getFieldValue("X_Y")

    return [i+"."+x_y, this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.sprite_icon_get_width_height = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let r = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let w_h = block.getFieldValue("W_H")

    return [r+"."+w_h, this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.sprite_icon_set_scale = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let scale = this.valueToCode(block, "SCALE", this.ORDER_NONE) || 0;

    return i+".scale = "+scale+"\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_icon_get_scale = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return [i+".scale", this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.sprite_icon_set_hidden = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let hidden = block.getFieldValue("HIDDEN")

    return i+".hidden ="+hidden+"\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_icon_get_hidden = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return ["not "+i+".hidden", this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.sprite_icon_move_by = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let pixels = this.valueToCode(block, "VALUE", this.ORDER_NONE) || 0;

    return i+".moveBy("+pixels+")\n"
}   

Blockly.GalaxiaGenerator.prototype.sprite_icon_turn= function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let direction =  block.getFieldValue("DIRECTION")
    let angle = this.valueToCode(block, "VALUE", this.ORDER_NONE) || 0;
    return i+".direction += "+direction+"*"+angle+"\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_icon_bounce_if_edge= function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);
    let i = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)

    return i+".bounceIfEdge()\n"
}

Blockly.GalaxiaGenerator.prototype.sprite_collision = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let s1 = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let s2 = this.variableDB_.getName(block.getFieldValue("VAR2"), Blockly.VARIABLE_CATEGORY_NAME)

    return ["sprites.collision("+s1+","+s2+")", this.ORDER_ATOMIC]
}

Blockly.GalaxiaGenerator.prototype.sprite_border_collision = function(block){
    this.addImport("import_sprites", IMPORT_SPRITES);

    let s1 = this.variableDB_.getName(block.getFieldValue("VAR"), Blockly.VARIABLE_CATEGORY_NAME)
    let border = block.getFieldValue("BORDER")

    return ["sprites.border_collision('"+border+"',"+s1+")", this.ORDER_ATOMIC]
}