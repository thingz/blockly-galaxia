//display-screen
const IMG_MODULE_LCD_3V3 = '/public/content/products/ecran-lcd-grove-32-caracteres-front.jpg';
const IMG_MODULE_OLED = '/public/content/products/ecran-oled-grove-128-64-front.jpg';
//display-led
const IMG_MODULE_LED = '/public/content/products/module-led-grove-back.jpg';
const IMG_MODULE_LED_PWM = '/public/content/products/module-led-grove-schema-front.jpg';
const IMG_MODULE_NEOPIXEL = '/public/content/products/bandeau-anneau-neopixel-block-front.jpg';
const IMG_MODULE_4DIGITDISPLAY = '/public/content/products/afficheur-4-chiffres-grove-front.jpg';
const IMG_MODULE_LED_BAR = '/public/content/products/jauge-led-grove-front.jpg';
const IMG_MODULE_TRAFFIC_LIGHT = '/public/content/products/feu-de-signalisation-stopbit-back.jpg';
//input-output
const IMG_MODULE_KEYPAD = '/public/content/products/clavier-12-touches-grove-front.jpg';
const IMG_MODULE_JOYSTICK = '/public/content/products/mini-joystick-grove-front.jpg';
const IMG_MODULE_LED_BUTTON = '/public/content/products/bouton-led-jaune-bleu-rouge-block-front.jpg';
const IMG_MODULE_SLIDE_POT = '/public/content/products/potentiometre-lineaire-grove-front.jpg';
const IMG_MODULE_ROTARY_ANGLE = '/public/content/products/encodeur-rotatif-noir-grove-front.jpg';
const IMG_MODULE_TOUCH = '/public/content/products/bouton-tactile-grove-front.jpg';
const IMG_MODULE_SWITCH = '/public/content/products/interrupteur-grove-block-front.jpg';
const IMG_MODULE_BUTTON = '/public/content/products/bouton-poussoir-grove-block-front.jpg';
//communication
const IMG_MODULE_OPENLOG = '/public/content/products/module-openlog-gestion-donnees-block-front.jpg';
const IMG_MODULE_GPS = '/public/content/products/module-gps-grove-extra4.jpg';
const IMG_MODULE_HC05 = '/public/content/products/module-bluetooth-HC05-front.jpg';
//sensors-gas
const IMG_MODULE_SGP30 = '/public/content/products/capteur-de-co2-et-cov-groove-sgp30-front.jpg';
const IMG_MODULE_MULTICHANNEL = '/public/content/products/capteur-de-gaz-multicanal-grove-front.jpg';
const IMG_MODULE_MULTICHANNEL_V2 = '/public/content/products/capteur-de-gaz-multicanal-v2-grove-front.jpg';
const IMG_MODULE_O2 = '/public/content/products/capteur-de-dioxygene-grove-front.jpg';
const IMG_MODULE_AIR_QUALITY = '/public/content/products/capteur-de-qualité-de-l-air.jpg';
const IMG_MODULE_HM330X = '/public/content/products/capteur-de-particules-fines-laser-Grove-HM3301-front.jpg';
//sensors-climate
const IMG_MODULE_BMP280 = '/public/content/products/capteur-de-temperature-pression-et-altitude-grove-BMP280-block-front.jpg';
const IMG_MODULE_MOISTURE = '/public/content/products/capteur-d-humidite-du-sol-grove-front.jpg';
const IMG_MODULE_TEMPERATURE = '/public/content/products/capteur-de-temperature-grove-extra2.jpg';
const IMG_MODULE_HIGH_TEMPERATURE = '/public/content/products/capteur-haute-temperature-grove-front.jpg';
const IMG_MODULE_DHT11 = '/public/content/products/capteur-de-temperature-et-humidite-grove-front.jpg';
const IMG_MODULE_TH02 = '/public/content/products/capteur-de-temperature-et-humidite-th02-front.jpg';
const IMG_MODULE_SHT31 = '/public/content/products/capteur-de-temperature-et-humidite-sht31-front.jpg';
const IMG_MODULE_WATER = '/public/content/products/capteur-d-eau-grove-extra1.jpg';
const IMG_MODULE_RAIN_GAUGE = '/public/content/products/capteur-pluviometre-front.jpg';
const IMG_MODULE_ANEMOMETER = '/public/content/products/capteur-anenometre-front.jpg';
//sensors-sound-light
const IMG_MODULE_LIGHT = '/public/content/products/capteur-de-luminosite-grove-front.jpg';
const IMG_MODULE_SI1145 = '/public/content/products/capteur-lumiere-visible-infrarouge-et-ultraviolet-grove-block-front.jpg';
const IMG_MODULE_UV = '/public/content/products/capteur-ultraviolet-grove-front.jpg';
const IMG_MODULE_SOUND_LOUDNESS = '/public/content/products/capteur-sonore-grove-block-front.jpg';
//sensors-distance-movement
const IMG_MODULE_ULTRASONIC = '/public/content/products/telemetre-a-ultrasons-grove-front.jpg';
const IMG_MODULE_GESTURE = '/public/content/products/capteur-de-gestes-grove-back.jpg';
const IMG_MODULE_LINE_FINDER = '/public/content/products/suiveur-de-ligne-grove-front.jpg';
const IMG_MODULE_TILT = '/public/content/products/capteur-d-inclination-grove-front.jpg';
const IMG_MODULE_MOTION = '/public/content/products/detecteur-infrarouge-de-mouvement-PIR-grove-front.jpg';
const IMG_MODULE_VIBRATIONS = '/public/content/products/capteur-de-vibrations-piezo-grove-extra2.jpg';
//actuators
const IMG_MODULE_SERVO = '/public/content/products/servomoteur-grove-front.jpg';
const IMG_MODULE_CONTINUOUS_SERVO = '/public/content/products/continuous-servomoteur-front.jpg';
const IMG_MODULE_MOTOR = '/public/content/products/module-ventilateur-grove-back.jpg';
const IMG_MODULE_BUZZER_SPEAKER = '/public/content/products/module-buzzer-speaker-grove-block-front.jpg';
const IMG_MODULE_VIBRATION_MOTOR = '/public/content/products/module-haptique-vibrateur-grove-front.jpg';
const IMG_MODULE_RELAY = '/public/content/products/module-relais-grove-front.jpg';
//robots
const IMG_ROBOT_MAQUEEN = '/public/content/products/maqueen-dfrobot-extra2.jpg';
const IMG_ROBOT_BITBOT = '/public/content/products/robot_bit_bot_microbit_extra1.jpg';
const IMG_ROBOT_GAMEPAD = '/public/content/products/micro-gamepad-dfrobot-extra2.jpg';
const IMG_ROBOT_CODO = '/public/content/products/robot-codo-front.jpg';



