// You can modify functions but don't refactor strings writing format, it is used if python code has to be changed
// Spaces and indents are very important in python code

/****** COLOUR CATEGORY ******/

// Colour _ set rgb
const DEF_COLOUR_RGB = `def colour_rgb(r, g, b):
  r = round(min(100, max(0, r)) * 2.55)
  g = round(min(100, max(0, g)) * 2.55)
  b = round(min(100, max(0, b)) * 2.55)
  return '#%02x%02x%02x' % (r, g, b)`;

// Colour _ set rgb
const DEF_COLOUR_BLEND = `def colour_blend(colour1, colour2, ratio):
  r1, r2 = int(colour1[1:3], 16), int(colour2[1:3], 16)
  g1, g2 = int(colour1[3:5], 16), int(colour2[3:5], 16)
  b1, b2 = int(colour1[5:7], 16), int(colour2[5:7], 16)
  ratio = min(1, max(0, ratio))
  r = round(r1 * (1 - ratio) + r2 * ratio)
  g = round(g1 * (1 - ratio) + g2 * ratio)
  b = round(b1 * (1 - ratio) + b2 * ratio)
  return '#%02x%02x%02x' % (r, g, b)`;

/****** LOOPS CATEGORY ******/

// Loops _ up range
const DEF_LOOPS_UP_RANGE = `def upRange(start, stop, step):
  while start <= stop:
    yield start
    start += abs(step)`;

// Loops _ down range
const DEF_LOOPS_DOWN_RANGE = `def downRange(start, stop, step):
  while start >= stop:
    yield start
    start -= abs(step)`;

/****** MATH CATEGORY ******/

// Math _ is number prime
const DEF_MATH_IS_PRIME = `def math_isPrime(n):
  if not isinstance(n, int or float):
    try:
      n = float(n)
    except:
      return False
  if n == 2 or n == 3:
    return True
  if n <= 1 or n % 1 != 0 or n % 2 == 0 or n % 3 == 0:
    return False
  for x in range(6, int(math.sqrt(n)) + 2, 6):
    if n % (x - 1) == 0 or n % (x + 1) == 0:
      return False
  return True`;

// Math _ mean
const DEF_MATH_MEAN = `def math_mean(myList):
  localList = [e for e in myList if isinstance(e, int or float)]
  if not localList: return
  return float(sum(localList)) / len(localList)`;

// Math _ median
const DEF_MATH_MEDIAN = `def math_median(myList):
    localList = sorted([e for e in myList if isinstance(e, int or float)])
    if not localList: return
    if len(localList) % 2 == 0:
      return (localList[len(localList) // 2 - 1] + localList[len(localList) // 2]) / 2.0
    else:
      return localList[(len(localList) - 1) // 2]`;

// Math _ modes
const DEF_MATH_MODES = `def math_modes(some_list):
  modes = []
  counts = []
  maxCount = 1
  for item in some_list:
    found = False
    for count in counts:
      if count[0] == item:
        count[1] += 1
        maxCount = max(maxCount, count[1])
        found = True
    if not found:
      counts.append([item, 1])
  for counted_item, item_count in counts:
    if item_count == maxCount:
      modes.append(counted_item)
  return modes`;

// Math _ standard deviation
const DEF_MATH_STANDARD_DEVIATION = `def math_standard_deviation(numbers):
  n = len(numbers)
  if n == 0: 
    return
  mean = float(sum(numbers)) / n
  variance = sum((x - mean) ** 2 for x in numbers) / n
  return math.sqrt(variance)`;

/****** TEXT CATEGORY ******/

// Text _ random_letter
const DEF_TEXT_RANDOM_LETTER = `def random_letter(text):
  x = int(random.random() * len(text))
  return text[x]`;

// Text _ prompt ext
const DEF_TEXT_PROMPT = `def text_prompt(msg):
  try:
    return raw_input(msg)
  except NameError:
    return input(msg)`;

/****** LISTS CATEGORY ******/

// Lists _ remove random item
const DEF_LISTS_REMOVE_RANDOM_ITEM = `def remove_randomItem(myList):
  x = int(random.random() * len(myList))
  return myList.pop(x)`;

// Lists _ sort
const DEF_LISTS_SORT = `def lists_sort(my_list, type, reverse):
  def try_float(s):
    try:
      return float(s)
    except:
      return 0
  key_funcs = {
    'NUMERIC': try_float,
    'TEXT': str,
    'IGNORE_CASE': lambda s: str(s).lower()
  }
  key_func = key_funcs[type]
  list_cpy = list(my_list) # Clone the list.
  return sorted(list_cpy, key=key_func, reverse=reverse)`;

/****** DISPLAY CATEGORY ******/

//Microbit Screen _ set gauge
const DEF_MICROBIT_LED_GAUGE = `def plotBarGraph(val, max):
  if val > 0 and val < max/5:
    led_image = Image('00000:00000:00000:00000:99999')
    display.show(led_image)
  if val >= max/5 and val < 2*max/5:
    led_image = Image('00000:00000:00000:99999:99999')
    display.show(led_image)
  if val >= 2*max/5 and val < 3*max/5:
    led_image = Image('00000:00000:99999:99999:99999')
    display.show(led_image)
  if val >= 3*max/5 and val < 4*max/5:
    led_image = Image('00000:99999:99999:99999:99999')
    display.show(led_image)
  if val >= 4*max/5 and val <= max:
    led_image = Image('99999:99999:99999:99999:99999')
    display.show(led_image)`;

const DEF_MICROBIT_SHOW_CLOCK = `def showClock(clock):
  i = Image
  CLOCKS = [i.CLOCK1, i.CLOCK2, i.CLOCK3, i.CLOCK4, i.CLOCK5, i.CLOCK6, i.CLOCK7, i.CLOCK8, i.CLOCK9, i.CLOCK10, i.CLOCK11, i.CLOCK12]
  if (clock > 12): clock -= 12
  for j in range(12):
    if clock < j+1:
      display.show(CLOCKS[j-1])
      break`;

/****** INPUT/OUTPUT CATEGORY ******/

//Microbit IO _ read pulse in
const DEF_IO_PULSE_IN = `def pulseIn(pin, pulseState, maxDuration = 2000000):
  t_init = utime.ticks_us()
  while (pin.read_digital() != pulseState):
    if(utime.ticks_us() - t_init > maxDuration):
      return 0
  start = utime.ticks_us()
  while (pin.read_digital() == pulseState):
    if(utime.ticks_us() - t_init > maxDuration):
      return 0
  end =  utime.ticks_us()
  return end - start`;

/****** COMMUNICATION CATEGORY ******/

// Micro:bit radio _ send number
const DEF_COM_RADIO_SEND_NUMBER = `def radio_sendNumber(data, type='number'):
  if type == 'number':
    if data > int(data):
      radio.send("&&&float:" + str(data) + "&&&")
    else:
      radio.send("&&&int:" + str(data) + "&&&")
  elif type == 'boolean':
    radio.send("&&&bool:" + str(int(data)) + "&&&")
  else:
    raise ValueError("Unable to send number: '" + str(data) + "'")`;

// Micro:bit radio _ send data
const DEF_COM_RADIO_SEND_VALUE = `def radio_sendValue(name, value, type='number'):
  if type == 'number':
    if value > int(value):
      radio.send("&&&float:[" + name + ";" + str(value) + "]&&&")
    else:
      radio.send("&&&int:[" + name + ";" + str(value) + "]&&&")
  elif type == 'boolean':
    radio.send("&&&bool:[" + name + ";" + str(int(value)) + "]&&&")
  else:
    raise ValueError("Unable to send number: '" + str(value) + "'")`;

// Micro:bit radio _ receive data
const DEF_COM_RADIO_RECEIVE_DATA = `def radio_receiveData():
  data = radio.receive()
  if data:
    if data.find('&&&int:') != -1:
      return int(data[7:-3])
    elif data.find('&&&float:') != -1:
      return float(data[9:-3])
    elif data.find('&&&bool:') != -1:
      return bool(data[8:-3])
    else:
      return data
  else:
    return None`;

// Micro:bit radio _ receive value
const DEF_COM_RADIO_RECEIVE_VALUE = `def radio_receiveValue():
  data = radio.receive()
  if data:
    if data.find('&&&int:[') != -1:
      parseData = data[8:-4].split(';')
      return parseData[0], int(parseData[1])
    elif data.find('&&&float:[') != -1:
      parseData = data[10:-4].split(';')
      return parseData[0], float(parseData[1])
    elif data.find('&&&bool:[') != -1:
      parseData = data[9:-4].split(';')
      return parseData[0], bool(parseData[1])
    else:
      return None, None
  else:
    return None, None`;

// Gove GPS _ read info
const DEF_COM_GPS_READ = `def gps_getInformations(data, info = 0):
  data = str(data).split(',')
  if len(data) > 10:
    if 'GPGGA' in data[0] or 'GAGGA' in data[0] or 'BDGGA' in data[0] or 'GBGGA' in data[0] or 'GLGGA' in data[0] or 'GNGGA' in data[0]:
      if info == 0 or info == 7:
        if data[info] != None and data[info] != '':
          return data[info]
      if info == 1 :
        if data[1] != None:
          return gps_getTime(data[1])
      if info == 2 or info == 4:
        if data[info] != None:
          return gps_getPosition(data[info])
      if info == 9 :
        if float(data[9]) != None:
          return float(data[9])
  return None`;

// Grove GPS _ get clock
const DEF_COM_GPS_GET_CLOCK = `def gps_getTime(date):
  if date and float(date):
    h = int(float(date) / 10000)
    m = int((float(date) - h*10000) / 100)
    s = int(float(date) - h*10000 - m*100)
    return (h, m, s)`;

// Grove GPS _ get latitude/logitude
const DEF_COM_GPS_GET_POS = `def gps_getPosition(pos):
  if pos and float(pos):
    base = int(float(pos)/100)
    mn = float(pos) - base*100
    return "%03.5f"%(base + mn/60) `;

/****** SENSORS CATEGORY ******/

// Grove Temperature _ read
const DEF_GROVE_GET_TEMP = `def getGroveTemperature(pin, unit='celsius'):
  R = 1023.0/pin.read_analog() - 1
  t = 1/(math.log(R)/4250+1/298.15) - 273.15 # celsius
  if unit == 'fahrenheit':
    t = t * 9/5 + 32
  elif unit == 'kelvin':
    t += 273.15
  return t`;

// Grove High temperature sensor _ thermocouple table
const DEF_GROVE_HIGHTEMP_THMC_TABLE = `
Var_VtoT_K = [[0, 2.5173462e1, -1.1662878, -1.0833638, -8.9773540/1e1, -3.7342377/1e1, -8.6632643/1e2, -1.0450598/1e2, -5.1920577/1e4],
              [0, 2.508355e1, 7.860106/1e2, -2.503131/1e1, 8.315270/1e2, -1.228034/1e2, 9.804036/1e4, -4.413030/1e5, 1.057734/1e6, -1.052755/1e8],
              [-1.318058e2, 4.830222e1, -1.646031, 5.464731/1e2, -9.650715/1e4, 8.802193/1e6, -3.110810/1e8]]`;

// Grove High temperature sensor _ adapter constant
const DEF_GROVE_HIGHTEMP_ADAPTER = " - 130";

// Grove High temperature sensor _ get thermocouple temperature
const DEF_GROVE_HIGHTEMP_GET_THMC_TEMP = `def getThmcTemp(pinA0, tempRoom):
  vout = pinA0.read_analog()/1023.0 * 5.0 * 1000.0
  vol  = (vout-350) / 54.16 
  return K_VtoT(vol) + tempRoom - 130`;

// Grove High temperature sensor _ get room temperature
const DEF_GROVE_HIGHTEMP_GET_ROOM_TEMP = `def getRoomTemp(pinA1):
  somme = 0
  for i in range(32) :
    somme += pinA1.read_analog()
  a = ((somme>>5))*50.0/33.0
  res = (1023.0-a)*10000.0/a                                      
  return 1/(math.log(res/10000.0)/3975.0+1/298.15)-273.15`;

// Grove High temperature sensor _ thermocouple table use
const DEF_GROVE_HIGHTEMP_KVTOT = `def K_VtoT(mV):
  i = 0
  value = 0
  if mV >= -6.478 and mV < 0 :
    value = Var_VtoT_K[0][8]
    for i in range(8, 0, -1):
      value = mV * value + Var_VtoT_K[0][i-1]
  elif mV >= 0 and mV < 20.644 :
    value = Var_VtoT_K[1][9]
    for i in range(9, 0, -1):
      value = mV * value + Var_VtoT_K[1][i-1]
  elif mV >= 20.644 and mV <= 54.900 :
    value = Var_VtoT_K[2][6]
    for i in range(6, 0, -1):
      value = mV * value + Var_VtoT_K[2][i-1]
  return value`;

// Grove Ultrasonic sensor _ get data
const DEF_GROVE_ULTRASONIC = `def getUltrasonicData(trig, echo, data='distance', timeout_us=30000):
  trig.write_digital(0)
  utime.sleep_us(2)               
  trig.write_digital(1)
  utime.sleep_us(10)
  trig.write_digital(0)
  echo.read_digital()
  duration = time_pulse_us(echo, 1, timeout_us)
  if data == 'distance':
    return 340 * duration/2e4
  elif data == 'duration':
    return duration
  else:
    print(str(data) + " n'est pas une option")
    return 0`;

// Grove O2 sensor _ read O2 concentration
const DEF_O2SENSOR_READ = `def readO2(pin, volt=False, Vref=3.3):
  somme = 0
  for i in range(32): 
    somme += pin.read_analog()                             
  somme >>= 5
  return somme*(Vref/1023.0) if volt else somme*(Vref/1023.0)*0.21/2.0*100`;

// Grove UV sensor _ get UV sensor
const DEF_GROVE_GET_UV_INDEX = `def getUVindex(pin):
  somme = 0
  for i in range(1024):
    somme += pin.read_analog()
    sleep(2)                                    
  return (somme*1000/4.3/1024-83) / 21`;

/****** ACTUATORS CATEGORY ******/

// Servomoteur _ set angle
const DEF_SERVO_SET_ANGLE = `def setServoAngle(pin, angle):
  if (angle >= 0 and angle <= 180):
    pin.write_analog(26 + (angle*102)/180)
  else:
    raise ValueError("Servomotor angle have to be set between 0 and 180")`;

// Continuous servomoteur _ set speed
const DEF_SERVO_SET_SPEED = `def setServoSpeed(pin, direction, speed):
  if (speed >= 0 and speed <= 100):
    GAP = 14
    if direction == 1:
      speedAngle = 90*(1+speed/100) - GAP
      pin.write_analog(speedAngle)
    elif direction == -1:
      speedAngle = 90*(1-speed/100) - GAP
      if speedAngle < 0: speedAngle = 1
      pin.write_analog(speedAngle)
    else:
      raise ValueError("continuous servomotor has no direction: '" + str(direction) + "'")
  else:
    raise ValueError("continuous servomotor speed is out of range: '" + str(speed) + "'")`;

// Buzzer module _ play music
const DEF_BUZZER_BEEP = `def beep (pin, noteFrequency, noteDuration, silence_ms = 100):
  microsecondsPerWave = 1e6 / noteFrequency
  millisecondsPerCycle = 1000 / (microsecondsPerWave * 2)
  loopTime = noteDuration * millisecondsPerCycle
  for x in range(loopTime):
    pin.write_digital(1)
    utime.sleep_us(int(microsecondsPerWave))
    pin.write_digital(0)
    utime.sleep_us(int(microsecondsPerWave))
  sleep(silence_ms)`;

// Buzzer module _ play gamme
const DEF_BUZZER_GAMME = `def BuzzerGamme(pin): 
  G_NOTES = [261.63, 293.66, 329.54, 349.23, 392, 440, 493.88, 523.25] 
  for i in range(len(G_NOTES)): 
    beep(pin, G_NOTES[i], 250, 50)`;

// Buzzer module _ play Star Wars
const DEF_BUZZER_STARWARS = `def BuzzerStarWars(pin): 
  SW_NOTES = [293.66, 293.66, 293.66, 392.0, 622.25, 554.37, 523.25, 454, 932.32, 622.25, 554.37, 523.25, 454, 932.32, 622.25, 554.37, 523.25, 554.37, 454] 
  SW_DURATION = [180, 180, 180, 800, 800, 180, 180, 180, 800, 400, 180, 180, 180, 800, 400, 180, 180, 180, 1000] 
  SW_SLEEP = [40, 40, 40, 100, 100, 40, 40, 40, 100, 50, 40, 40, 40, 100, 50, 40, 40, 40, 100] 
  for i in range(len(SW_NOTES)): 
    beep(pin, SW_NOTES[i], SW_DURATION[i], SW_SLEEP[i])`;

// Buzzer module _ play R2D2
const DEF_BUZZER_R2D2 = `def BuzzerR2D2(pin): 
  R2D2_NOTES = [3520, 3135.96, 2637.02, 2093, 2349.32, 3951.07, 2793.83, 4186.01, 3520, 3135.96, 2637.02, 2093, 2349.32, 3951.07, 2793.83, 4186.01] 
  for i in range(len(R2D2_NOTES)): 
    beep(pin, R2D2_NOTES[i], 80, 20)`;

const DEF_NEOPIXEL_SHOW_ALL_LED = `def neopixel_showAllLed(neoPx, ledCount, R, G, B):
  for i in range(ledCount): 
    neoPx[i] = (R, G, B) 
  neoPx.show()`;

// Neopixel _ RAINBOW
const DEF_NEOPIXEL_RAINBOW = `def neopixel_rainbow(neoPx, ledCount): 
  R = 255 
  G = 50 
  B = 50 
  for G in range(50, 256, 5):
    neopixel_showAllLed(neoPx, ledCount, R, G, B)
    sleep(5)
  for R in range(255, 49, -5): 
    neopixel_showAllLed(neoPx, ledCount, R, G, B)
    sleep(5)
  for B in range(50, 256, 5): 
    neopixel_showAllLed(neoPx, ledCount, R, G, B)
    sleep(5)
  for G in range(255, 49, -5): 
    neopixel_showAllLed(neoPx, ledCount, R, G, B)
    sleep(5)
  for R in range(50, 256, 5): 
    neopixel_showAllLed(neoPx, ledCount, R, G, B)
    sleep(5)
  for B in range(255, 49, -5): 
    neopixel_showAllLed(neoPx, ledCount, R, G, B)
    sleep(5)`;

// Touch Keypad _ get touched number
const DEF_KEYPAD_GET_NUMBER = `def getKeypadNumber():
  if uart.any():
    data = str(uart.read())[5:-1] #select the content of byte
    if data == 'a': return '*'
    elif data == 'b': return '0'
    elif data == 'c': return '#'
    else: return data
  else: return ""`;

// 4 Digit Display _ NUMERIC TABLE
const DEF_4DIGIT_ASCII_NUMERIC_TABLE = "ASCII_4DD_NUM = bytearray(b'\\x3F\\x06\\x5B\\x4F\\x66\\x6D\\x7D\\x07\\x7F\\x6F')";

// 4 Digit Display _ initialization definitions
const DEF_4DIGIT_INIT = `def start4dd(clk,dio):
  dio.write_digital(0)
  clk.write_digital(0)

def stop4dd(clk,dio):
  dio.write_digital(0)
  clk.write_digital(1)
  dio.write_digital(1)

def writeByte4dd(clk,dio,cmd):
  for i in range(8):
    dio.write_digital((cmd>>i)&1)
    clk.write_digital(1)
    clk.write_digital(0)
  clk.write_digital(0)
  clk.write_digital(1)
  clk.write_digital(0)

def command4dd(clk,dio,cmd):
  start4dd(clk,dio)
  writeByte4dd(clk,dio,cmd)
  stop4dd(clk,dio)

def write4dd(clk,dio,s,p=0):
  if not 0<=p<=3:
    raise ValueError('Position out of range')
  command4dd(clk,dio,0x40)
  start4dd(clk,dio)
  writeByte4dd(clk,dio,0xC0|p)
  for i in s:
    writeByte4dd(clk,dio,i)
  stop4dd(clk,dio)
  command4dd(clk,dio,0x88|6)
  
def tm1637_init(clk,dio):
  command4dd(clk,dio,0x40)
  command4dd(clk,dio,0x88|6)`;

// 4 Digit Display _ encode integer
const DEF_4DIGIT_ENCODE_NUM = `def tm1637_encodeNum(str):
  s = bytearray(len(str))
  for i in range(len(str)):
    o = ord(str[i])
    if o >= 48 and o <= 57:
      s[i] = ASCII_4DD_NUM[o-48]
  return s`;

// 4 Digit Display _ set number
const DEF_4DIGIT_SET_NUMBER = `def tm1637_setNumber(clk, dio, n):
  if not -999 <= n <= 9999:
    raise ValueError('Number out of range')
  write4dd(clk, dio, tm1637_encodeNum('{0: >4d}'.format(int(n))))`;

// 4 Digit Display _ set temperature
const DEF_4DIGIT_SET_TEMP = `def tm1637_setTemp(clk, dio, n):
  if n<-9:
    write4dd(clk, dio, [0x38,0x3F])
  elif n>99:
    write4dd(clk, dio, [0x76,0x06])
  else:
    write4dd(clk, dio, tm1637_encodeNum('{0: >2d}'.format(int(n))))
  write4dd(clk, dio, [0x63,0x39], 2)`;

// 4 Digit Display _ set clock
const DEF_4DIGIT_SET_CLOCK = `def tm1637_setClock(clk, dio, h, mn, colon=True):
  segs = tm1637_encodeNum('{0:0>2d}{1:0>2d}'.format(h, mn))
  segs[1] |= 0x80
  write4dd(clk, dio, segs)`;

// 4 Digit Display _ get time add
const DEF_4DIGIT_GET_TIME = `def tm1637_getTime():
  mn=0
  h=0
  min0 = (running_time()-chrono0) / 1000 / 60
  h0 = min0/60
  last_mins = MIN_START + int(min0) - (int(h0)*60)
  if last_mins > 59:
    mn = last_mins-60
    h = HOUR_START + int(h0) + 1
  else:
    mn = last_mins
    h = HOUR_START + int(h0)
    return h,mn`;

// Grove LED Bar _ drivers
const DEF_LEDBAR_INIT = `def ledBar_latch(di):
  di.write_digital(0)
  sleep(1)
  for i in range(4):
    di.write_digital(1)
    di.write_digital(0)
  sleep(1)

def ledBar_write16(di, dcki, data):
  state = dcki.read_digital()
  for i in range(15, -1, -1):
    di.write_digital((data >> i) & 1)
    state = not state
    dcki.write_digital(state)`;

// Grove LED Bar _ set level
const DEF_LEDBAR_LEVEL = `def ledBar_setLevel(di, dcki, val, brightness=255, r=False):
  di.write_digital(0)
  dcki.write_digital(0)
  ledBar_write16(di, dcki, 0)
  for i in range(9, -1, -1) if not r else range(10):
    if int(val) > i:
      ledBar_write16(di, dcki, brightness)
    elif int(val) == i:
      ledBar_write16(di, dcki, int(brightness * (val - int(val))))
    else :
      ledBar_write16(di, dcki, 0)
  ledBar_write16(di, dcki, 0)
  ledBar_write16(di, dcki, 0)
  ledBar_latch(di)`;

/****** ROBOTS CATEGORY ******/

//Codo _ control left motor
const DEF_CODO_CONTROL_LEFT_MOTOR = `def codo_controlLeftMotor(cmd, speed):
  pin13.write_analog(cmd[0] * speed)
  pin14.write_analog(cmd[1] * speed)`;

//Codo _ control right motor
const DEF_CODO_CONTROL_RIGHT_MOTOR = `def codo_controlRightMotor(cmd, speed):
  pin15.write_analog(cmd[0] * speed)
  pin16.write_analog(cmd[1] * speed)`;

//Codo _ move
const DEF_CODO_MOVE = `def codo_move(direction, speed = 1023):
  if (speed >= 0 and speed <= 1023):
    if direction == 'forward':
      codo_controlLeftMotor([0, 1], speed)
      codo_controlRightMotor([0, 1], speed)
    elif direction == 'backward':
      codo_controlLeftMotor([1, 0], speed)
      codo_controlRightMotor([1, 0], speed)
    elif direction == 'right':
      codo_controlLeftMotor([0, 1], speed)
      codo_controlRightMotor([1, 0], speed)
    elif direction == 'left':
      codo_controlLeftMotor([1, 0], speed)
      codo_controlRightMotor([0, 1], speed)
    elif direction == 'stop':
      codo_controlLeftMotor([0, 0], speed)
      codo_controlRightMotor([0, 0], speed)
    else: 
      display.scroll("'" + str(direction) + "' is not a direction")
  else:
    raise ValueError('The speed of codo motors have to be set between 0 and 255')`;

// Bit:Bot Motors _ set bit:bot go
const DEF_BITBOT_GO = `def bitbot_controlMotors(dir, speed): 
  pin0.write_analog(speed)
  pin1.write_analog(speed)
  pin8.write_digital(dir)
  pin12.write_digital(dir)`;

// Bit:Bot Light sensor _ read light level
const DEF_BITBOT_LIGHT_SENSOR = `def bitbot_readLightSensor(sensor):
  pin16.write_digital(sensor)
  return pin2.read_analog()`;
