/****** COMMUNICATION CATEGORY ******/

// Galaxia WiFi initializer
const GALAXIA_WIFI_INIT = "simpleWifi = simple_wifi.SimpleWifi()";
const GALAXIA_MICROPYTHON_WIFI_INIT = "station = network.WLAN(network.STA_IF)";
const GALAXIA_HTTP_INIT = "http = simple_wifi.SimpleHttp(simpleWifi)";
const GALAXIA_CHRONOMETER = "galaxiaChronometer = time.monotonic_ns()"
const GALAXIA_MICROPYTHON_CHRONOMETER = "galaxiaChronometer = time.ticks_us()"