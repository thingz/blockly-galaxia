/**
 * @fileoverview English translation file for Micro:bit Toolbox. (EN)
 */

var MSG = {
  title: "Code",
  blocks: "Blocks",
  linkTooltip: "Save and link to blocks.",
  runTooltip: "Run the program defined by the blocks in the workspace.",
  badCode: "Program error:\n%1",
  timeout: "Maximum execution iterations exceeded.",
  trashTooltip: "Discard all blocks.",
  httpRequestError: "There was a problem with the request.",
  linkAlert: "Share your blocks with this link:\n\n%1",
  hashError: "Sorry, '%1' doesn't correspond with any saved program.",
  xmlError: "Could not load your saved file. Perhaps it was created with a different version of Blockly?",
  badXml: "Error parsing XML:\n%1\n\nSelect 'OK' to abandon your changes or 'Cancel' to further edit the XML.",
  textVariable: "text",
  listVariable: "list",
  hello: "Hello !",
  comment: "This code part serves to ...",
  data1: "Data1",
  data2: "Data2",
  radioMessage: "I'm the radio message !"
};

// Toolbox category names.
Blockly.Msg["CATEGORY_COMMUNICATION"] = "Communication";
Blockly.Msg["CATEGORY_SENSORS"] = "Sensors";
Blockly.Msg["CATEGORY_ROBOTS"] = "Robots";
Blockly.Msg["CATEGORY_TEXT"] = "Text";
Blockly.Msg["CATEGORY_VARIABLES"] = "Variables";
Blockly.Msg["CATEGORY_LISTS"] = "Lists";
Blockly.Msg["CATEGORY_PROCEDURES"] = "Functions";
Blockly.Msg["CATEGORY_COLOUR"] = "Colour";

Blockly.Msg["CATEGORY_DISPLAY"] = "Display";
Blockly.Msg["CATEGORY_IO"] = "Inputs/Outputs";
Blockly.Msg["CATEGORY_ACTUATORS"] = "Actuators";
Blockly.Msg["CATEGORY_LOOPS"] = "Loops";
Blockly.Msg["CATEGORY_LOGIC"] = "Logic";
Blockly.Msg["CATEGORY_MATH"] = "Math";

Blockly.Msg["CATEGORY_APPEARANCE"] = "Appearance";
Blockly.Msg["CATEGORY_SOUND"] = "Sound";
Blockly.Msg["CATEGORY_CONTROL"] = "Control";
Blockly.Msg["CATEGORY_OPERATORS"] = "Operators";

// Toolbox subcategory labels.
Blockly.Msg["SUBCATEGORY_LOOPS"] = "Loops";
Blockly.Msg["SUBCATEGORY_LOGIC"] = "Logic";
Blockly.Msg["SUBCATEGORY_MICROBIT"] = "Micro:bit";
Blockly.Msg["SUBCATEGORY_DISPLAYS"] = "Screens";
Blockly.Msg["SUBCATEGORY_LED_MODULES"] = "LED modules";
Blockly.Msg["SUBCATEGORY_MORPION"] = "Tic Tac Toe";
Blockly.Msg["SUBCATEGORY_GAMES"] = "Games";
Blockly.Msg["SUBCATEGORY_MICROPHONE"] = "Microphone";
Blockly.Msg["SUBCATEGORY_EXTERNAL_MODULES"] = "External modules";
Blockly.Msg["SUBCATEGORY_PINS"] = "Pins";
Blockly.Msg["SUBCATEGORY_DATA_LOGGING"] = "Data logging";
Blockly.Msg["SUBCATEGORY_WIRELESS_COMMUNICATION"] = "Wireless communication";
Blockly.Msg["SUBCATEGORY_TRACKING_MODULES"] = "Tracking modules";
Blockly.Msg["SUBCATEGORY_SERIAL_CONNECTION"] = "Serial connection";
Blockly.Msg["SUBCATEGORY_SENSORS_GAS"] = "Gas sensors";
Blockly.Msg["SUBCATEGORY_SENSORS_CLIMATE"] = "Climate sensors";
Blockly.Msg["SUBCATEGORY_SENSORS_SOUNDLIGHT"] = "Sound & Light sensors";
Blockly.Msg["SUBCATEGORY_SENSORS_DISTANCEMOVEMENT"] = "Distance & Motion sensors";
Blockly.Msg["SUBCATEGORY_SENSORS_OTHER"] = "Other sensors";
Blockly.Msg["SUBCATEGORY_MOTORS"] = "Motors";
Blockly.Msg["SUBCATEGORY_MUSIC"] = "Music";
Blockly.Msg["SUBCATEGORY_COMPUTER"] = "Computer";
Blockly.Msg["SUBCATEGORY_SPEECH"] = "Speech";
Blockly.Msg["SUBCATEGORY_MAQUEEN"] = "Maqueen";
Blockly.Msg["SUBCATEGORY_CODO"] = "Codo";
Blockly.Msg["SUBCATEGORY_BITBOT"] = "Bit:Bot";
Blockly.Msg["SUBCATEGORY_GAMEPAD"] = "Gamepad";

// French translation file for Micro:bit Toolbox. (FR)

Blockly.Msg["SUBCATEGORY_GALAXIA"] = "Galaxia";
