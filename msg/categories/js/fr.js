/**
 * @fileoverview French translation file for Micro:bit Toolbox. (FR)
 */

var MSG = {
  title: "Code",
  blocks: "Blocs",
  linkTooltip: "Sauvegarder et lier aux blocs.",
  runTooltip: "Lancer le programme défini par les blocs dans l’espace de travail.",
  badCode: "Erreur du programme :\n%1",
  timeout: "Nombre maximum d’itérations d’exécution dépassé.",
  trashTooltip: "Jeter tous les blocs.",
  httpRequestError: "Il y a eu un problème avec la demande.",
  linkAlert: "Partagez vos blocs grâce à ce lien:\n\n%1",
  hashError: "Désolé, '%1' ne correspond à aucun programme sauvegardé.",
  xmlError: "Impossible de charger le fichier de sauvegarde.  Peut être a t-il été créé avec une autre version de Blockly?",
  badXml: "Erreur d’analyse du XML :\n%1\n\nSélectionner 'OK' pour abandonner vos modifications ou 'Annuler' pour continuer à modifier le XML.",
  textVariable: "texte",
  listVariable: "liste",
  hello: "Bonjour !",
  comment: "Cette partie du code sert à ...",
  data1: "Donnée1",
  data2: "Donnée2",
  radioMessage: "Je suis le message radio !"
};

// Toolbox category names.
Blockly.Msg["CATEGORY_COMMUNICATION"] = "Communication";
Blockly.Msg["CATEGORY_SENSORS"] = "Capteurs";
Blockly.Msg["CATEGORY_ROBOTS"] = "Robots";
Blockly.Msg["CATEGORY_TEXT"] = "Texte";
Blockly.Msg["CATEGORY_VARIABLES"] = "Variables";
Blockly.Msg["CATEGORY_LISTS"] = "Listes";
Blockly.Msg["CATEGORY_PROCEDURES"] = "Fonctions";
Blockly.Msg["CATEGORY_COLOUR"] = "Couleur";

Blockly.Msg["CATEGORY_DISPLAY"] = "Affichage";
Blockly.Msg["CATEGORY_IO"] = "Entrées/Sorties";
Blockly.Msg["CATEGORY_ACTUATORS"] = "Actionneurs";
Blockly.Msg["CATEGORY_LOOPS"] = "Boucles";
Blockly.Msg["CATEGORY_LOGIC"] = "Logique";
Blockly.Msg["CATEGORY_MATH"] = "Math";

Blockly.Msg["CATEGORY_APPEARANCE"] = "Apparence";
Blockly.Msg["CATEGORY_SOUND"] = "Son";
Blockly.Msg["CATEGORY_CONTROL"] = "Contrôle";
Blockly.Msg["CATEGORY_OPERATORS"] = "Opérateurs";

// Toolbox subcategory labels.
Blockly.Msg["SUBCATEGORY_LOOPS"] = "Boucles";
Blockly.Msg["SUBCATEGORY_LOGIC"] = "Logique";
Blockly.Msg["SUBCATEGORY_MICROBIT"] = "Micro:bit";
Blockly.Msg["SUBCATEGORY_DISPLAYS"] = "Ecrans";
Blockly.Msg["SUBCATEGORY_LED_MODULES"] = "Modules à LED";
Blockly.Msg["SUBCATEGORY_MORPION"] = "Morpion";
Blockly.Msg["SUBCATEGORY_GAMES"] = "Jeux";
Blockly.Msg["SUBCATEGORY_MICROPHONE"] = "Microphone";
Blockly.Msg["SUBCATEGORY_EXTERNAL_MODULES"] = "Modules externes";
Blockly.Msg["SUBCATEGORY_PINS"] = "Broches";
Blockly.Msg["SUBCATEGORY_SERIAL_CONNECTION"] = "Connexion série";
Blockly.Msg["SUBCATEGORY_DATA_LOGGING"] = "Enregistrement de données";
Blockly.Msg["SUBCATEGORY_WIRELESS_COMMUNICATION"] = "Communication sans-fil";
Blockly.Msg["SUBCATEGORY_TRACKING_MODULES"] = "Modules de repérage";
Blockly.Msg["SUBCATEGORY_SENSORS_GAS"] = "Capteurs de gaz";
Blockly.Msg["SUBCATEGORY_SENSORS_CLIMATE"] = "Capteurs météorologiques";
Blockly.Msg["SUBCATEGORY_SENSORS_SOUNDLIGHT"] = "Capteurs de son et lumière";
Blockly.Msg["SUBCATEGORY_SENSORS_DISTANCEMOVEMENT"] = "Capteurs de distance et mouvement";
Blockly.Msg["SUBCATEGORY_SENSORS_OTHER"] = "Autres capteurs";
Blockly.Msg["SUBCATEGORY_MOTORS"] = "Moteurs";
Blockly.Msg["SUBCATEGORY_MUSIC"] = "Musique";
Blockly.Msg["SUBCATEGORY_COMPUTER"] = "Ordinateur";
Blockly.Msg["SUBCATEGORY_SPEECH"] = "Speech";
Blockly.Msg["SUBCATEGORY_MAQUEEN"] = "Maqueen";
Blockly.Msg["SUBCATEGORY_CODO"] = "Codo";
Blockly.Msg["SUBCATEGORY_BITBOT"] = "Bit:Bot";
Blockly.Msg["SUBCATEGORY_GAMEPAD"] = "Gamepad";


// French translation file for Micro:bit Toolbox. (FR)

Blockly.Msg["SUBCATEGORY_GALAXIA"] = "Galaxia";
