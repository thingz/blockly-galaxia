/**
 * @fileoverview French messages for Micro:bit. (FR)
 */

'use strict';

Blockly.Msg["VARIABLES_DEFAULT_NAME"] = "variable";
Blockly.Msg["UNNAMED_KEY"] = "non nommé";
Blockly.Msg["TODAY"] = "Aujourd'hui";
Blockly.Msg["ORDINAL_NUMBER_SUFFIX"] = "";

// Workspace.
Blockly.Msg["WORKSPACE_COMMENT_DEFAULT_TEXT"] = "Dire quelque chose…";
Blockly.Msg["WORKSPACE_ARIA_LABEL"] = "Espace de travail de Blocky";
Blockly.Msg["COLLAPSED_WARNINGS_WARNING"] = "Les blocs repliés contiennent des avertissements.";

// Context menus.
Blockly.Msg["DUPLICATE_BLOCK"] = "Dupliquer";
Blockly.Msg["ADD_COMMENT"] = "Ajouter un commentaire";
Blockly.Msg["REMOVE_COMMENT"] = "Supprimer un commentaire";
Blockly.Msg["DUPLICATE_COMMENT"] = "Dupliquer le commentaire";
Blockly.Msg["EXTERNAL_INPUTS"] = "Entrées externes";
Blockly.Msg["INLINE_INPUTS"] = "Entrées en ligne";
Blockly.Msg["DELETE_BLOCK"] = "Supprimer le bloc";
Blockly.Msg["DELETE_X_BLOCKS"] = "Supprimer %1 blocs";
Blockly.Msg["DELETE_ALL_BLOCKS"] = "Supprimer ces %1 blocs ?";
Blockly.Msg["CLEAN_UP"] = "Nettoyer les blocs";
Blockly.Msg["COLLAPSE_BLOCK"] = "Réduire le bloc";
Blockly.Msg["COLLAPSE_ALL"] = "Réduire les blocs";
Blockly.Msg["EXPAND_BLOCK"] = "Développer le bloc";
Blockly.Msg["EXPAND_ALL"] = "Développer les blocs";
Blockly.Msg["DISABLE_BLOCK"] = "Désactiver le bloc";
Blockly.Msg["ENABLE_BLOCK"] = "Activer le bloc";
Blockly.Msg["HELP"] = "Aide";
Blockly.Msg["UNDO"] = "Annuler";
Blockly.Msg["REDO"] = "Refaire";

// IOS.
Blockly.Msg["IOS_OK"] = "OK";
Blockly.Msg["IOS_CANCEL"] = "Annuler";
Blockly.Msg["IOS_ERROR"] = "Erreur";
Blockly.Msg["IOS_PROCEDURES_INPUTS"] = "ENTRÉES";
Blockly.Msg["IOS_PROCEDURES_ADD_INPUT"] = "+ Ajouter une entrée";
Blockly.Msg["IOS_PROCEDURES_ALLOW_STATEMENTS"] = "Ordres autorisés";
Blockly.Msg["IOS_PROCEDURES_DUPLICATE_INPUTS_ERROR"] = "Cette fonction a des entrées dupliquées.";
Blockly.Msg["IOS_VARIABLES_ADD_VARIABLE"] = "+ Ajouter une variable";
Blockly.Msg["IOS_VARIABLES_ADD_BUTTON"] = "Ajouter";
Blockly.Msg["IOS_VARIABLES_RENAME_BUTTON"] = "Renommer";
Blockly.Msg["IOS_VARIABLES_DELETE_BUTTON"] = "Supprimer";
Blockly.Msg["IOS_VARIABLES_VARIABLE_NAME"] = "Nom de la variable";
Blockly.Msg["IOS_VARIABLES_EMPTY_NAME_ERROR"] = "Vous ne pouvez pas utiliser un nom de variable vide.";

// Variable renaming.
Blockly.Msg["CHANGE_VALUE_TITLE"] = "Modifier la valeur :";
Blockly.Msg["RENAME_VARIABLE"] = "Renommer la variable…";
Blockly.Msg["RENAME_VARIABLE_TITLE"] = "Renommer toutes les variables « %1 » en :";

// Variable creation.
Blockly.Msg["NEW_VARIABLE"] = "Créer une variable...";
Blockly.Msg["NEW_STRING_VARIABLE"] = "Créer une variable chaîne…";
Blockly.Msg["NEW_NUMBER_VARIABLE"] = "Créer une variable nombre…";
Blockly.Msg["NEW_COLOUR_VARIABLE"] = "Créer une variable couleur…";
Blockly.Msg["NEW_VARIABLE_TITLE"] = "Le nom de la nouvelle variable :";
Blockly.Msg["NEW_VARIABLE_TYPE_TITLE"] = "Nouveau type de variable :";
Blockly.Msg["VARIABLE_ALREADY_EXISTS"] = "Une variable appelée '%1' existe déjà.";
Blockly.Msg["VARIABLE_ALREADY_EXISTS_FOR_ANOTHER_TYPE"] = "Une variable nommée '%1' existe déjà pour un autre type : '%2'.";

// Variable deletion.
Blockly.Msg["DELETE_VARIABLE"] = "Supprimer la variable '%1'";
Blockly.Msg["DELETE_VARIABLE_CONFIRMATION"] = "Supprimer %1 utilisations de la variable '%2' ?";
Blockly.Msg["CANNOT_DELETE_VARIABLE_PROCEDURE"] = "Impossible de supprimer la variable '%1' parce qu’elle fait partie de la définition de la fonction '%2'";

// Warning text.
Blockly.Msg["LOGIC_COMPARE_WARNING"] = "Impossible de comparer la première entrée de type '%1' \navec la seconde de type '%2'";
Blockly.Msg["LOGIC_TERNARY_WARNING"] = "Impossible de retourner la première entrée de type '%1' \navec la seconde de type '%2'";

// Start blocks.
Blockly.Msg["ON_START_TITLE"] = "Au démarrage";
Blockly.Msg["ON_START_TOOLTIP"] = "Ajouter des instructions dans ce bloc pour les exécuter au démarrage de la carte micro:bit.";
Blockly.Msg["FOREVER_TITLE"] = "Répéter indéfiniment";
Blockly.Msg["FOREVER_TOOLTIP"] = "Ajouter des instructions dans ce bloc pour les exécuter en boucle.";

// Colour blocks.
Blockly.Msg["COLOUR_PICKER_HELPURL"] = "https://fr.wikipedia.org/wiki/Couleur";
Blockly.Msg["COLOUR_PICKER_TOOLTIP"] = "Choisir une couleur dans la palette.";
Blockly.Msg["COLOUR_RANDOM_HELPURL"] = "http://randomcolour.com";
Blockly.Msg["COLOUR_RANDOM_TITLE"] = "couleur aléatoire";
Blockly.Msg["COLOUR_RANDOM_TOOLTIP"] = "Choisir une couleur au hasard.";
Blockly.Msg["COLOUR_RGB_HELPURL"] = "http://www.december.com/html/spec/colorper.html";
Blockly.Msg["COLOUR_RGB_TITLE"] = "colorier en";
Blockly.Msg["COLOUR_RGB_RED"] = "rouge";
Blockly.Msg["COLOUR_RGB_GREEN"] = "vert";
Blockly.Msg["COLOUR_RGB_BLUE"] = "bleu";
Blockly.Msg["COLOUR_RGB_TOOLTIP"] = "Créer une couleur avec la quantité spécifiée de rouge, vert et bleu. Les valeurs doivent être comprises entre 0 et 100.";
Blockly.Msg["COLOUR_BLEND_HELPURL"] = "http://meyerweb.com/eric/tools/color-blend/";
Blockly.Msg["COLOUR_BLEND_TITLE"] = "mélanger";
Blockly.Msg["COLOUR_BLEND_COLOUR1"] = "couleur 1";
Blockly.Msg["COLOUR_BLEND_COLOUR2"] = "couleur 2";
Blockly.Msg["COLOUR_BLEND_RATIO"] = "taux";
Blockly.Msg["COLOUR_BLEND_TOOLTIP"] = "Mélange deux couleurs dans une proportion donnée (de 0.0 à 1.0).";

// Logic blocks.
Blockly.Msg["CONTROLS_IF_HELPURL"] = "https://github.com/google/blockly/wiki/IfElse";
Blockly.Msg["CONTROLS_IF_TOOLTIP_1"] = "Si une valeur est vraie, alors exécuter certains ordres.";
Blockly.Msg["CONTROLS_IF_TOOLTIP_2"] = "Si une valeur est vraie, alors exécuter le premier bloc d’ordres. Sinon, exécuter le second bloc d’ordres.";
Blockly.Msg["CONTROLS_IF_TOOLTIP_3"] = "Si la première valeur est vraie, alors exécuter le premier bloc d’ordres. Sinon, si la seconde valeur est vraie, exécuter le second bloc d’ordres.";
Blockly.Msg["CONTROLS_IF_TOOLTIP_4"] = "Si la première valeur est vraie, alors exécuter le premier bloc d’ordres. Sinon, si la seconde valeur est vraie, exécuter le second bloc d’ordres. Si aucune des valeurs n’est vraie, exécuter le dernier bloc d’ordres.";
Blockly.Msg["CONTROLS_IF_MSG_IF"] = "si %1 alors";
Blockly.Msg["CONTROLS_IF_MSG_ELSEIF"] = "sinon si";
Blockly.Msg["CONTROLS_IF_MSG_ELSE"] = "sinon";
Blockly.Msg["CONTROLS_IF_IF_TOOLTIP"] = "Ajouter, supprimer ou réordonner les sections pour reconfigurer ce bloc si.";
Blockly.Msg["CONTROLS_IF_ELSEIF_TOOLTIP"] = "Ajouter une condition au bloc si.";
Blockly.Msg["CONTROLS_IF_ELSE_TOOLTIP"] = "Ajouter une condition finale fourre-tout au bloc si.";
Blockly.Msg["CONTROLS_IF_ELSEIF_TITLE_ELSEIF"] = Blockly.Msg["CONTROLS_IF_MSG_ELSEIF"];
Blockly.Msg["CONTROLS_IF_ELSE_TITLE_ELSE"] = Blockly.Msg["CONTROLS_IF_MSG_ELSE"];
Blockly.Msg["CONTROLS_IF_IF_TITLE_IF"] = Blockly.Msg["CONTROLS_IF_MSG_IF"];
Blockly.Msg["LOGIC_COMPARE_HELPURL"] = "https://fr.wikipedia.org/wiki/In%C3%A9galit%C3%A9_(math%C3%A9matiques)";
Blockly.Msg["LOGIC_COMPARE_TOOLTIP_EQ"] = "Renvoyer vrai si les deux entrées sont égales.";
Blockly.Msg["LOGIC_COMPARE_TOOLTIP_NEQ"] = "Renvoyer vrai si les deux entrées sont différentes.";
Blockly.Msg["LOGIC_COMPARE_TOOLTIP_LT"] = "Renvoyer vrai si la première entrée est plus petite que la seconde.";
Blockly.Msg["LOGIC_COMPARE_TOOLTIP_LTE"] = "Renvoyer vrai si la première entrée est plus petite ou égale à la seconde.";
Blockly.Msg["LOGIC_COMPARE_TOOLTIP_GT"] = "Renvoyer vrai si la première entrée est plus grande que la seconde.";
Blockly.Msg["LOGIC_COMPARE_TOOLTIP_GTE"] = "Renvoyer vrai si la première entrée est plus grande ou égale à la seconde.";
Blockly.Msg["LOGIC_OPERATION_HELPURL"] = "https://github.com/google/blockly/wiki/Logic#logical-operations";
Blockly.Msg["LOGIC_OPERATION_AND"] = "et";
Blockly.Msg["LOGIC_OPERATION_OR"] = "ou";
Blockly.Msg["LOGIC_OPERATION_TOOLTIP_AND"] = "Renvoyer vrai si les deux entrées sont vraies.";
Blockly.Msg["LOGIC_OPERATION_TOOLTIP_OR"] = "Renvoyer vrai si au moins une des entrées est vraie.";
Blockly.Msg["LOGIC_BOOLEAN_HELPURL"] = "https://github.com/google/blockly/wiki/Logic#values";
Blockly.Msg["LOGIC_BOOLEAN_TRUE"] = "vrai";
Blockly.Msg["LOGIC_BOOLEAN_FALSE"] = "faux";
Blockly.Msg["LOGIC_BOOLEAN_TOOLTIP"] = "Renvoie soit vrai soit faux.";
Blockly.Msg["LOGIC_NEGATE_HELPURL"] = "https://github.com/google/blockly/wiki/Logic#not";
Blockly.Msg["LOGIC_NEGATE_TITLE"] = "pas %1";
Blockly.Msg["LOGIC_NEGATE_TOOLTIP"] = "Renvoie vrai si l’entrée est fausse. Renvoie faux si l’entrée est vraie.";
Blockly.Msg["LOGIC_NULL_HELPURL"] = "https://en.wikipedia.org/wiki/Nullable_type";
Blockly.Msg["LOGIC_NULL_TITLE"] = "nul";
Blockly.Msg["LOGIC_NULL_TOOLTIP"] = "Renvoie nul.";
Blockly.Msg["LOGIC_TERNARY_HELPURL"] = "https://en.wikipedia.org/wiki/%3F:";
Blockly.Msg["LOGIC_TERNARY_CONDITION"] = "test";
Blockly.Msg["LOGIC_TERNARY_IF_TRUE"] = "si vrai";
Blockly.Msg["LOGIC_TERNARY_IF_FALSE"] = "si faux";
Blockly.Msg["LOGIC_TERNARY_TOOLTIP"] = "Vérifier la condition dans 'test'. Si elle est vraie, renvoie la valeur 'si vrai' ; sinon renvoie la valeur 'si faux'.";

// Loop blocks.
Blockly.Msg["CONTROLS_REPEAT_HELPURL"] = "http://fr.wikipedia.org/wiki/Boucle_for";
Blockly.Msg["CONTROLS_REPEAT_TITLE"] = "répéter %1 fois";
Blockly.Msg["CONTROLS_REPEAT_TOOLTIP"] = "Exécuter des instructions plusieurs fois.";
Blockly.Msg["CONTROLS_WHILEUNTIL_HELPURL"] = "https://github.com/google/blockly/wiki/Loops#repeat";
Blockly.Msg["CONTROLS_WHILEUNTIL_OPERATOR_UNTIL"] = "répéter jusqu’à";
Blockly.Msg["CONTROLS_WHILEUNTIL_OPERATOR_WHILE"] = "répéter tant que";
Blockly.Msg["CONTROLS_WHILEUNTIL_TOOLTIP_UNTIL"] = "Tant qu’une valeur est fausse, alors exécuter des instructions.";
Blockly.Msg["CONTROLS_WHILEUNTIL_TOOLTIP_WHILE"] = "Tant qu’une valeur est vraie, alors exécuter des instructions.";
Blockly.Msg["CONTROLS_FOR_HELPURL"] = "https://github.com/google/blockly/wiki/Loops#count-with";
Blockly.Msg["CONTROLS_FOR_TITLE"] = "compter avec %1 de %2 à %3 par pas de %4";
Blockly.Msg["CONTROLS_FOR_TOOLTIP"] = "Faire prendre à la variable « %1 » les valeurs depuis le nombre de début jusqu’au nombre de fin, en s’incrémentant du pas spécifié, et exécuter les instructions spécifiées.";
Blockly.Msg["CONTROLS_FOREACH_HELPURL"] = "https://github.com/google/blockly/wiki/Loops#for-each";
Blockly.Msg["CONTROLS_FOREACH_TITLE"] = "pour chaque élément %1 dans la liste %2";
Blockly.Msg["CONTROLS_FOREACH_TOOLTIP"] = "Pour chaque élément d’une liste, assigner la valeur de l’élément à la variable '%1', puis exécuter des instructions.";
Blockly.Msg["CONTROLS_FLOW_STATEMENTS_HELPURL"] = "https://github.com/google/blockly/wiki/Loops#loop-termination-blocks";
Blockly.Msg["CONTROLS_FLOW_STATEMENTS_OPERATOR_BREAK"] = "quitter la boucle";
Blockly.Msg["CONTROLS_FLOW_STATEMENTS_OPERATOR_CONTINUE"] = "passer à l’itération de boucle suivante";
Blockly.Msg["CONTROLS_FLOW_STATEMENTS_TOOLTIP_BREAK"] = "Sortir de la boucle englobante.";
Blockly.Msg["CONTROLS_FLOW_STATEMENTS_TOOLTIP_CONTINUE"] = "Sauter le reste de cette boucle, et poursuivre avec l’itération suivante.";
Blockly.Msg["CONTROLS_FLOW_STATEMENTS_WARNING"] = "Attention : Ce bloc ne devrait être utilisé que dans une boucle.";

// Math blocks.
Blockly.Msg["MATH_ADDITION_SYMBOL"] = "+";
Blockly.Msg["MATH_SUBTRACTION_SYMBOL"] = "-";
Blockly.Msg["MATH_DIVISION_SYMBOL"] = "÷";
Blockly.Msg["MATH_MULTIPLICATION_SYMBOL"] = "×";
Blockly.Msg["MATH_POWER_SYMBOL"] = "^";
Blockly.Msg["MATH_NUMBER_HELPURL"] = "https://fr.wikipedia.org/wiki/Nombre";
Blockly.Msg["MATH_NUMBER_TOOLTIP"] = "Un nombre.";
Blockly.Msg["MATH_ARITHMETIC_HELPURL"] = "https://fr.wikipedia.org/wiki/Arithmetique";
Blockly.Msg["MATH_ARITHMETIC_TOOLTIP_ADD"] = "Renvoie la somme des deux nombres.";
Blockly.Msg["MATH_ARITHMETIC_TOOLTIP_MINUS"] = "Renvoie la différence des deux nombres.";
Blockly.Msg["MATH_ARITHMETIC_TOOLTIP_DIVIDE"] = "Renvoie le quotient des deux nombres.";
Blockly.Msg["MATH_ARITHMETIC_TOOLTIP_MULTIPLY"] = "Renvoie le produit des deux nombres.";
Blockly.Msg["MATH_ARITHMETIC_TOOLTIP_POWER"] = "Renvoie le premier nombre élevé à la puissance du second.";
Blockly.Msg["MATH_SINGLE_HELPURL"] = "https://fr.wikipedia.org/wiki/Racine_carree";
Blockly.Msg["MATH_SINGLE_OP_ROOT"] = "racine carrée";
Blockly.Msg["MATH_SINGLE_OP_ABSOLUTE"] = "valeur absolue";
Blockly.Msg["MATH_SINGLE_TOOLTIP_ROOT"] = "Renvoie la racine carrée d’un nombre.";
Blockly.Msg["MATH_SINGLE_TOOLTIP_ABS"] = "Renvoie la valeur absolue d’un nombre.";
Blockly.Msg["MATH_SINGLE_TOOLTIP_NEG"] = "Renvoie l’opposé d’un nombre";
Blockly.Msg["MATH_SINGLE_TOOLTIP_EXP"] = "Renvoie e à la puissance d’un nombre.";
Blockly.Msg["MATH_SINGLE_TOOLTIP_LN"] = "Renvoie le logarithme naturel d’un nombre.";
Blockly.Msg["MATH_SINGLE_TOOLTIP_LOG10"] = "Renvoie le logarithme décimal d’un nombre.";
Blockly.Msg["MATH_SINGLE_TOOLTIP_POW10"] = "Renvoie 10 à la puissance d’un nombre.";
Blockly.Msg["MATH_TRIG_HELPURL"] = "https://fr.wikipedia.org/wiki/Fonction_trigonom%C3%A9trique";
Blockly.Msg["MATH_TRIG_COS"] = "cos";
Blockly.Msg["MATH_TRIG_SIN"] = "sin";
Blockly.Msg["MATH_TRIG_TAN"] = "tan";
Blockly.Msg["MATH_TRIG_ACOS"] = "acos";
Blockly.Msg["MATH_TRIG_ASIN"] = "asin";
Blockly.Msg["MATH_TRIG_ATAN"] = "atan";
Blockly.Msg["MATH_TRIG_TOOLTIP_COS"] = "Renvoie le cosinus d’un angle en degrés (pas en radians).";
Blockly.Msg["MATH_TRIG_TOOLTIP_SIN"] = "Renvoie le sinus d’un angle en degrés (pas en radians).";
Blockly.Msg["MATH_TRIG_TOOLTIP_TAN"] = "Renvoie la tangente d’un angle en degrés (pas en radians).";
Blockly.Msg["MATH_TRIG_TOOLTIP_ACOS"] = "Renvoie l’arccosinus d’un nombre.";
Blockly.Msg["MATH_TRIG_TOOLTIP_ASIN"] = "Renvoie l’arcsinus d’un nombre.";
Blockly.Msg["MATH_TRIG_TOOLTIP_ATAN"] = "Renvoie l’arctangente d’un nombre.";
Blockly.Msg["MATH_CONSTANT_HELPURL"] = "https://fr.wikipedia.org/wiki/Table_de_constantes_math%C3%A9matiques";
Blockly.Msg["MATH_CONSTANT_TOOLTIP"] = "Renvoie une des constantes courantes : π (3.141…), e (2.718…), φ (1.618…), sqrt(2) (1.414…), sqrt(½) (0.707…), ou ∞ (infini).";
Blockly.Msg["MATH_IS_EVEN"] = "est pair";
Blockly.Msg["MATH_IS_ODD"] = "est impair";
Blockly.Msg["MATH_IS_PRIME"] = "est premier";
Blockly.Msg["MATH_IS_WHOLE"] = "est entier";
Blockly.Msg["MATH_IS_POSITIVE"] = "est positif";
Blockly.Msg["MATH_IS_NEGATIVE"] = "est négatif";
Blockly.Msg["MATH_IS_DIVISIBLE_BY"] = "est divisible par";
Blockly.Msg["MATH_IS_TOOLTIP"] = "Vérifier si un nombre est pair, impair, premier, entier, positif, négatif, ou s’il est divisible par un certain nombre. Renvoie vrai ou faux.";
Blockly.Msg["MATH_MAP_TITLE"] = "transformer la valeur %1 de [ %2 à %3 ] vers [ %4 à %5 ]";
Blockly.Msg["MATH_MAP_TOOLTIP"] = "transform a value from an array given [min;max] to another array [min;max]";
Blockly.Msg["MATH_ROUND_HELPURL"] = "https://fr.wikipedia.org/wiki/Arrondi_(math%C3%A9matiques)";
Blockly.Msg["MATH_ROUND_OPERATOR_ROUND"] = "arrondir";
Blockly.Msg["MATH_ROUND_OPERATOR_ROUNDDOWN"] = "arrondir par défaut";
Blockly.Msg["MATH_ROUND_OPERATOR_ROUNDUP"] = "arrondir par excès";
Blockly.Msg["MATH_ROUND_TOOLTIP"] = "Arrondir un nombre au-dessus ou au-dessous.";
Blockly.Msg["MATH_MODULO_HELPURL"] = "https://fr.wikipedia.org/wiki/Modulo_(op%C3%A9ration)";
Blockly.Msg["MATH_MODULO_TITLE"] = "reste de %1 ÷ %2";
Blockly.Msg["MATH_MODULO_TOOLTIP"] = "Renvoyer le reste de la division euclidienne des deux nombres.";
Blockly.Msg["MATH_CONSTRAIN_HELPURL"] = "https://en.wikipedia.org/wiki/Clamping_(graphics)";
Blockly.Msg["MATH_CONSTRAIN_TITLE"] = "contraindre %1 entre %2 et %3";
Blockly.Msg["MATH_CONSTRAIN_TOOLTIP"] = "Contraindre un nombre à être entre les limites spécifiées (incluses).";
Blockly.Msg["MATH_RANDOM_INT_HELPURL"] = "https://fr.wikipedia.org/wiki/G%C3%A9n%C3%A9rateur_de_nombres_al%C3%A9atoires";
Blockly.Msg["MATH_RANDOM_INT_TITLE"] = "entier aléatoire entre %1 et %2";
Blockly.Msg["MATH_RANDOM_INT_TOOLTIP"] = "Renvoyer un entier aléatoire entre les deux limites spécifiées, incluses.";
Blockly.Msg["MATH_RANDOM_FLOAT_HELPURL"] = "https://fr.wikipedia.org/wiki/G%C3%A9n%C3%A9rateur_de_nombres_al%C3%A9atoires";
Blockly.Msg["MATH_RANDOM_FLOAT_TITLE_RANDOM"] = "fraction aléatoire";
Blockly.Msg["MATH_RANDOM_FLOAT_TOOLTIP"] = "Renvoyer une fraction aléatoire entre 0.0 (inclus) et 1.0 (exclus).";
Blockly.Msg["MATH_ATAN2_HELPURL"] = "https://fr.wikipedia.org/wiki/Atan2";
Blockly.Msg["MATH_ATAN2_TITLE"] = "atan2 de X:%1 Y:%2";
Blockly.Msg["MATH_ATAN2_TOOLTIP"] = "Renvoie l'arc tangente du point (X, Y) en degrés entre -180 et 180.";

// Text blocks.
Blockly.Msg["TEXT_TEXT_HELPURL"] = "https://fr.wikipedia.org/wiki/Cha%C3%AEne_de_caract%C3%A8res";
Blockly.Msg["TEXT_TEXT_TOOLTIP"] = "Une lettre, un mot ou une ligne de texte.";
Blockly.Msg["TEXT_JOIN_HELPURL"] = "https://github.com/google/blockly/wiki/Text#text-creation";
Blockly.Msg["TEXT_JOIN_TITLE_CREATEWITH"] = "créer le texte";
Blockly.Msg["TEXT_JOIN_TOOLTIP"] = "Créer un morceau de texte en agrégeant un nombre quelconque d’éléments.";
Blockly.Msg["TEXT_APPEND_HELPURL"] = "https://github.com/google/blockly/wiki/Text#text-modification";
Blockly.Msg["TEXT_APPEND_TITLE"] = "ajouter le texte %2 à %1";
Blockly.Msg["TEXT_APPEND_VARIABLE"] = Blockly.Msg["VARIABLES_DEFAULT_NAME"];
Blockly.Msg["TEXT_APPEND_TOOLTIP"] = "Ajouter du texte à la variable '%1'.";
Blockly.Msg["TEXT_LENGTH_HELPURL"] = "https://github.com/google/blockly/wiki/Text#text-modification";
Blockly.Msg["TEXT_LENGTH_TITLE"] = "longueur de %1";
Blockly.Msg["TEXT_LENGTH_TOOLTIP"] = "Renvoie le nombre de lettres (espaces compris) dans le texte fourni.";
Blockly.Msg["TEXT_ISEMPTY_HELPURL"] = "https://github.com/google/blockly/wiki/Text#checking-for-empty-text";
Blockly.Msg["TEXT_ISEMPTY_TITLE"] = "%1 est vide";
Blockly.Msg["TEXT_ISEMPTY_TOOLTIP"] = "Renvoie vrai si le texte fourni est vide.";
Blockly.Msg["TEXT_INDEXOF_HELPURL"] = "https://github.com/google/blockly/wiki/Text#finding-text";
Blockly.Msg["TEXT_INDEXOF_OPERATOR_FIRST"] = "trouver la première occurrence de la chaîne";
Blockly.Msg["TEXT_INDEXOF_OPERATOR_LAST"] = "trouver la dernière occurrence de la chaîne";
Blockly.Msg["TEXT_INDEXOF_TITLE"] = "dans le texte %1 %2 %3";
Blockly.Msg["TEXT_INDEXOF_TOOLTIP"] = "Renvoie l’index de la première/dernière occurrence de la première chaîne dans la seconde. Renvoie %1 si la chaîne n’est pas trouvée.";
Blockly.Msg["TEXT_CHARAT_HELPURL"] = "https://github.com/google/blockly/wiki/Text#extracting-text";
Blockly.Msg["TEXT_CHARAT_TITLE"] = "dans le texte %1 %2";
Blockly.Msg["TEXT_CHARAT_FIRST"] = "obtenir la première lettre";
Blockly.Msg["TEXT_CHARAT_LAST"] = "obtenir la dernière lettre";
Blockly.Msg["TEXT_CHARAT_FROM_START"] = "obtenir la lettre d'indice";
Blockly.Msg["TEXT_CHARAT_FROM_END"] = "obtenir la lettre d'indice (depuis la fin)";
Blockly.Msg["TEXT_CHARAT_RANDOM"] = "obtenir une lettre au hasard";
Blockly.Msg["TEXT_CHARAT_TAIL"] = "";
Blockly.Msg["TEXT_CHARAT_TOOLTIP"] = "Renvoie la lettre à la position indiquée.";
Blockly.Msg["TEXT_GET_SUBSTRING_HELPURL"] = "https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text";
Blockly.Msg["TEXT_GET_SUBSTRING_START_FIRST"] = "obtenir la sous-chaîne depuis la première lettre";
Blockly.Msg["TEXT_GET_SUBSTRING_START_FROM_START"] = "obtenir la sous-chaîne depuis la lettre d'indice";
Blockly.Msg["TEXT_GET_SUBSTRING_START_FROM_END"] = "obtenir la sous-chaîne depuis la lettre d'indice (depuis la fin)";
Blockly.Msg["TEXT_GET_SUBSTRING_END_LAST"] = "jusqu’à la dernière lettre";
Blockly.Msg["TEXT_GET_SUBSTRING_END_FROM_START"] = "jusqu’à la lettre d'indice";
Blockly.Msg["TEXT_GET_SUBSTRING_END_FROM_END"] = "jusqu’à la lettre d'indice (depuis la fin)";
Blockly.Msg["TEXT_GET_SUBSTRING_INPUT_IN_TEXT"] = "dans le texte";
Blockly.Msg["TEXT_GET_SUBSTRING_TAIL"] = "";
Blockly.Msg["TEXT_GET_SUBSTRING_TOOLTIP"] = "Renvoie une partie indiquée du texte.";
Blockly.Msg["TEXT_CHANGECASE_HELPURL"] = "https://github.com/google/blockly/wiki/Text#adjusting-text-case";
Blockly.Msg["TEXT_CHANGECASE_TITLE"] = "mettre %1 en %2";
Blockly.Msg["TEXT_CHANGECASE_OPERATOR_LOWERCASE"] = "minuscules";
Blockly.Msg["TEXT_CHANGECASE_OPERATOR_TITLECASE"] = "Majuscule Au Début De Chaque Mot";
Blockly.Msg["TEXT_CHANGECASE_OPERATOR_UPPERCASE"] = "MAJUSCULES";
Blockly.Msg["TEXT_CHANGECASE_TOOLTIP"] = "Renvoyer une copie du texte dans une autre casse.";
Blockly.Msg["TEXT_TRIM_HELPURL"] = "https://github.com/google/blockly/wiki/Text#trimming-removing-spaces";
Blockly.Msg["TEXT_TRIM_TITLE"] = "supprimer les espaces %1 de %2";
Blockly.Msg["TEXT_TRIM_OPERATOR_BOTH"] = "des deux côtés";
Blockly.Msg["TEXT_TRIM_OPERATOR_LEFT"] = "du côté gauche";
Blockly.Msg["TEXT_TRIM_OPERATOR_RIGHT"] = "du côté droit";
Blockly.Msg["TEXT_TRIM_TOOLTIP"] = "Renvoyer une copie du texte avec les espaces supprimés d’un bout ou des deux.";
Blockly.Msg["TEXT_COUNT_HELPURL"] = "https://github.com/google/blockly/wiki/Text#counting-substrings";
Blockly.Msg["TEXT_COUNT_TITLE"] = "compter occurences de %1 dans %2";
Blockly.Msg["TEXT_COUNT_TOOLTIP"] = "Compter combien de fois un texte donné apparait dans un autre.";
Blockly.Msg["TEXT_REPLACE_HELPURL"] = "https://github.com/google/blockly/wiki/Text#replacing-substrings";
Blockly.Msg["TEXT_REPLACE_TITLE"] = "remplacer %1 par %2 dans %3";
Blockly.Msg["TEXT_REPLACE_TOOLTIP"] = "Remplacer toutes les occurrences d’un texte par un autre.";
Blockly.Msg["TEXT_REVERSE_HELPURL"] = "https://github.com/google/blockly/wiki/Text#reversing-text";
Blockly.Msg["TEXT_REVERSE_TITLE"] = "inverser %1";
Blockly.Msg["TEXT_REVERSE_TOOLTIP"] = "Inverse l’ordre des caractères dans le texte.";
Blockly.Msg["TEXT_COMMENT_TITLE"] = "Commentaire %1";
Blockly.Msg["TEXT_COMMENT_TOOLTIP"] = "Ajouter un commentaire dans le code.";
Blockly.Msg["TEXT_PRINT_HELPURL"] = "https://github.com/google/blockly/wiki/Text#printing-text";
Blockly.Msg["TEXT_PRINT_TITLE"] = "afficher %1";
Blockly.Msg["TEXT_PRINT_TOOLTIP"] = "Afficher le texte, le nombre ou une autre valeur spécifiée.";
Blockly.Msg["TEXT_PROMPT_HELPURL"] = "https://github.com/google/blockly/wiki/Text#getting-input-from-the-user";
Blockly.Msg["TEXT_PROMPT_TOOLTIP_NUMBER"] = "Demander un nombre à l’utilisateur.";
Blockly.Msg["TEXT_PROMPT_TOOLTIP_TEXT"] = "Demander un texte à l’utilisateur.";
Blockly.Msg["TEXT_PROMPT_TYPE_NUMBER"] = "invite pour un nombre avec un message";
Blockly.Msg["TEXT_PROMPT_TYPE_TEXT"] = "invite pour un texte avec un message";

// Variables blocks.
Blockly.Msg["VARIABLES_SET_HELPURL"] = "https://github.com/google/blockly/wiki/Variables#set";
Blockly.Msg["VARIABLES_SET"] = "mettre %1 à %2";
Blockly.Msg["VARIABLES_SET_CREATE_GET"] = "Créer 'obtenir %1'";
Blockly.Msg["VARIABLES_SET_TOOLTIP"] = "Fixe cette variable pour qu’elle soit égale à la valeur de l’entrée.";
Blockly.Msg["VARIABLES_GET_HELPURL"] = "https://github.com/google/blockly/wiki/Variables#get";
Blockly.Msg["VARIABLES_GET_CREATE_SET"] = "Créer 'fixer %1'";
Blockly.Msg["VARIABLES_GET_TOOLTIP"] = "Renvoie la valeur de cette variable.";
Blockly.Msg["VARIABLES_INCREMENT_HELPURL"] = "https://fr.wikipedia.org/wiki/Idiome_de_programmation";
Blockly.Msg["VARIABLES_INCREMENT_TITLE"] = "ajouter %2 à %1";
Blockly.Msg["VARIABLES_INCREMENT_TOOLTIP"] = "Ajouter un nombre à cette variable.";
Blockly.Msg["VARIABLES_FORCE_TYPE_TITLE"] = "convertir %1 en %2";
Blockly.Msg["VARIABLES_FORCE_TYPE_TEXT"] = "texte (str)";
Blockly.Msg["VARIABLES_FORCE_TYPE_BOOLEAN"] = "booléen (bool)";
Blockly.Msg["VARIABLES_FORCE_TYPE_INTEGER"] = "entier (int)";
Blockly.Msg["VARIABLES_FORCE_TYPE_FLOAT"] = "flottant (float)";
Blockly.Msg["VARIABLES_FORCE_TYPE_LONG"] = "long (long)";
Blockly.Msg["VARIABLES_FORCE_TYPE_TOOLTIP"] = "Permet de convertir une variable dans le typage choisi.";
Blockly.Msg["VARIABLES_TYPEOF_TITLE"] = "type de %1";
Blockly.Msg["VARIABLES_TYPEOF_TOOLTIP"] = "Renvoie le type de cette variable.";

// Lists blocks.
Blockly.Msg["LISTS_CREATE_EMPTY_HELPURL"] = "https://github.com/google/blockly/wiki/Lists#create-empty-list";
Blockly.Msg["LISTS_CREATE_EMPTY_TITLE"] = "liste vide";
Blockly.Msg["LISTS_CREATE_EMPTY_TOOLTIP"] = "Renvoyer une liste, de longueur 0, ne contenant aucun enregistrement";
Blockly.Msg["LISTS_CREATE_WITH_HELPURL"] = "https://github.com/google/blockly/wiki/Lists#create-list-with";
Blockly.Msg["LISTS_CREATE_WITH_INPUT_WITH"] = "liste avec les éléments";
Blockly.Msg["LISTS_CREATE_WITH_TOOLTIP"] = "liste avec un nombre quelconque d’éléments.";
Blockly.Msg["LISTS_REPEAT_HELPURL"] = "https://github.com/google/blockly/wiki/Lists#create-list-with";
Blockly.Msg["LISTS_REPEAT_TITLE"] = "liste avec l’élément %1 répété %2 fois";
Blockly.Msg["LISTS_REPEAT_TOOLTIP"] = "Crée une liste consistant en la valeur fournie répétée le nombre de fois indiqué.";
Blockly.Msg["LISTS_LENGTH_HELPURL"] = "https://github.com/google/blockly/wiki/Lists#length-of";
Blockly.Msg["LISTS_LENGTH_TITLE"] = "longueur de %1";
Blockly.Msg["LISTS_LENGTH_TOOLTIP"] = "Renvoie la longueur d’une liste.";
Blockly.Msg["MATH_ONLIST_HELPURL"] = "";
Blockly.Msg["MATH_ONLIST_OPERATOR_AVERAGE"] = "moyenne de la liste";
Blockly.Msg["MATH_ONLIST_OPERATOR_MAX"] = "maximum de la liste";
Blockly.Msg["MATH_ONLIST_OPERATOR_MEDIAN"] = "médiane de la liste";
Blockly.Msg["MATH_ONLIST_OPERATOR_MIN"] = "minimum de la liste";
Blockly.Msg["MATH_ONLIST_OPERATOR_MODE"] = "majoritaires de la liste";
Blockly.Msg["MATH_ONLIST_OPERATOR_RANDOM"] = "élément aléatoire de la liste";
Blockly.Msg["MATH_ONLIST_OPERATOR_STD_DEV"] = "écart-type de la liste";
Blockly.Msg["MATH_ONLIST_OPERATOR_SUM"] = "somme de la liste";
Blockly.Msg["MATH_ONLIST_TOOLTIP_AVERAGE"] = "Renvoyer la moyenne (arithmétique) des valeurs numériques dans la liste.";
Blockly.Msg["MATH_ONLIST_TOOLTIP_MAX"] = "Renvoyer le plus grand nombre dans la liste.";
Blockly.Msg["MATH_ONLIST_TOOLTIP_MEDIAN"] = "Renvoyer le nombre médian de la liste.";
Blockly.Msg["MATH_ONLIST_TOOLTIP_MIN"] = "Renvoyer le plus petit nombre dans la liste.";
Blockly.Msg["MATH_ONLIST_TOOLTIP_MODE"] = "Renvoyer une liste des élément(s) le(s) plus courant(s) dans la liste.";
Blockly.Msg["MATH_ONLIST_TOOLTIP_RANDOM"] = "Renvoyer un élément dans la liste au hasard.";
Blockly.Msg["MATH_ONLIST_TOOLTIP_STD_DEV"] = "Renvoyer l’écart-type de la liste.";
Blockly.Msg["MATH_ONLIST_TOOLTIP_SUM"] = "Renvoyer la somme de tous les nombres dans la liste.";
Blockly.Msg["LISTS_ISEMPTY_HELPURL"] = "https://github.com/google/blockly/wiki/Lists#is-empty";
Blockly.Msg["LISTS_ISEMPTY_TITLE"] = "%1 est vide ?";
Blockly.Msg["LISTS_ISEMPTY_TOOLTIP"] = "Renvoie vrai si la liste est vide.";
Blockly.Msg["LISTS_REVERSE_HELPURL"] = "https://github.com/google/blockly/wiki/Lists#reversing-a-list";
Blockly.Msg["LISTS_REVERSE_TITLE"] = "inverser la liste %1";
Blockly.Msg["LISTS_REVERSE_TOOLTIP"] = "Inverser la copie d’une liste.";
Blockly.Msg["LISTS_INLIST"] = "dans la liste";
Blockly.Msg["LISTS_INDEX_OF_HELPURL"] = "https://github.com/google/blockly/wiki/Lists#getting-items-from-a-list";
Blockly.Msg["LISTS_INDEX_OF_FIRST"] = "trouver la première occurrence de l’élément";
Blockly.Msg["LISTS_INDEX_OF_LAST"] = "trouver la dernière occurrence de l’élément";
Blockly.Msg["LISTS_INDEX_OF_INPUT_IN_LIST"] = Blockly.Msg["LISTS_INLIST"];
Blockly.Msg["LISTS_INDEX_OF_TOOLTIP"] = "Renvoie l’index de la première/dernière occurrence de l’élément dans la liste. Renvoie %1 si l'élément n'est pas trouvé.";
Blockly.Msg["LISTS_GET_INDEX_HELPURL"] = Blockly.Msg["LISTS_INDEX_OF_HELPURL"];
Blockly.Msg["LISTS_GET_INDEX_GET"] = "obtenir";
Blockly.Msg["LISTS_GET_INDEX_GET_REMOVE"] = "obtenir et supprimer";
Blockly.Msg["LISTS_GET_INDEX_REMOVE"] = "supprimer";
Blockly.Msg["LISTS_GET_INDEX_FIRST"] = "le premier élément";
Blockly.Msg["LISTS_GET_INDEX_LAST"] = "le dernier élément";
Blockly.Msg["LISTS_GET_INDEX_FROM_START"] = "l'élément d'indice";
Blockly.Msg["LISTS_GET_INDEX_FROM_END"] = "l'élément d'indice (depuis la fin)";
Blockly.Msg["LISTS_GET_INDEX_RANDOM"] = "un élément aléatoire";
Blockly.Msg["LISTS_GET_INDEX_TAIL"] = "";
Blockly.Msg["LISTS_GET_INDEX_INPUT_IN_LIST"] = Blockly.Msg["LISTS_INLIST"];
Blockly.Msg["LISTS_INDEX_FROM_END_TOOLTIP"] = "%1 est le dernier élément.";
Blockly.Msg["LISTS_INDEX_FROM_START_TOOLTIP"] = "%1 est le premier élément.";
Blockly.Msg["LISTS_GET_INDEX_TOOLTIP_GET_FIRST"] = "Renvoie le premier élément dans une liste.";
Blockly.Msg["LISTS_GET_INDEX_TOOLTIP_GET_FROM"] = "Renvoie l’élément à la position indiquée dans une liste.";
Blockly.Msg["LISTS_GET_INDEX_TOOLTIP_GET_LAST"] = "Renvoie le dernier élément dans une liste.";
Blockly.Msg["LISTS_GET_INDEX_TOOLTIP_GET_RANDOM"] = "Renvoie un élément au hasard dans une liste.";
Blockly.Msg["LISTS_GET_INDEX_TOOLTIP_GET_REMOVE_FIRST"] = "Supprime et renvoie le premier élément dans une liste.";
Blockly.Msg["LISTS_GET_INDEX_TOOLTIP_GET_REMOVE_FROM"] = "Supprime et renvoie l’élément à la position indiquée dans une liste.";
Blockly.Msg["LISTS_GET_INDEX_TOOLTIP_GET_REMOVE_LAST"] = "Supprime et renvoie le dernier élément dans une liste.";
Blockly.Msg["LISTS_GET_INDEX_TOOLTIP_GET_REMOVE_RANDOM"] = "Supprime et renvoie un élément au hasard dans une liste.";
Blockly.Msg["LISTS_GET_INDEX_TOOLTIP_REMOVE_FIRST"] = "Supprime le premier élément dans une liste.";
Blockly.Msg["LISTS_GET_INDEX_TOOLTIP_REMOVE_FROM"] = "Supprime l’élément à la position indiquée dans une liste.";
Blockly.Msg["LISTS_GET_INDEX_TOOLTIP_REMOVE_LAST"] = "Supprime le dernier élément dans une liste.";
Blockly.Msg["LISTS_GET_INDEX_TOOLTIP_REMOVE_RANDOM"] = "Supprime un élément au hasard dans une liste.";
Blockly.Msg["LISTS_SET_INDEX_HELPURL"] = "https://github.com/google/blockly/wiki/Lists#in-list--set";
Blockly.Msg["LISTS_SET_INDEX_INPUT_TO"] = "comme";
Blockly.Msg["LISTS_SET_INDEX_INSERT"] = "insérer en";
Blockly.Msg["LISTS_SET_INDEX_SET"] = "mettre";
Blockly.Msg["LISTS_SET_INDEX_INPUT_IN_LIST"] = Blockly.Msg["LISTS_INLIST"];
Blockly.Msg["LISTS_SET_INDEX_TOOLTIP_INSERT_FIRST"] = "Insère l’élément au début d’une liste.";
Blockly.Msg["LISTS_SET_INDEX_TOOLTIP_INSERT_FROM"] = "Insère l’élément à la position indiquée dans une liste.";
Blockly.Msg["LISTS_SET_INDEX_TOOLTIP_INSERT_LAST"] = "Ajouter l’élément à la fin d’une liste.";
Blockly.Msg["LISTS_SET_INDEX_TOOLTIP_INSERT_RANDOM"] = "Insère l’élément au hasard dans une liste.";
Blockly.Msg["LISTS_SET_INDEX_TOOLTIP_SET_FIRST"] = "Fixe le premier élément dans une liste.";
Blockly.Msg["LISTS_SET_INDEX_TOOLTIP_SET_FROM"] = "Met à jour l’élément à la position indiquée dans une liste.";
Blockly.Msg["LISTS_SET_INDEX_TOOLTIP_SET_LAST"] = "Fixe le dernier élément dans une liste.";
Blockly.Msg["LISTS_SET_INDEX_TOOLTIP_SET_RANDOM"] = "Fixe un élément au hasard dans une liste.";
Blockly.Msg["LISTS_GET_SUBLIST_HELPURL"] = "https://github.com/google/blockly/wiki/Lists#getting-a-sublist";
Blockly.Msg["LISTS_GET_SUBLIST_START_FIRST"] = "obtenir la sous-liste depuis le début";
Blockly.Msg["LISTS_GET_SUBLIST_START_FROM_START"] = "obtenir la sous-liste depuis l'élément d'indice";
Blockly.Msg["LISTS_GET_SUBLIST_START_FROM_END"] = "obtenir la sous-liste depuis l'élément d'indice (depuis la fin)";
Blockly.Msg["LISTS_GET_SUBLIST_END_LAST"] = "jusqu’à la fin";
Blockly.Msg["LISTS_GET_SUBLIST_END_FROM_START"] = "jusqu’à l'élément d'indice";
Blockly.Msg["LISTS_GET_SUBLIST_END_FROM_END"] = "jusqu’à l'élément d'indice (depuis la fin)";
Blockly.Msg["LISTS_GET_SUBLIST_TAIL"] = "";
Blockly.Msg["LISTS_GET_SUBLIST_INPUT_IN_LIST"] = Blockly.Msg["LISTS_INLIST"];
Blockly.Msg["LISTS_GET_SUBLIST_TOOLTIP"] = "Crée une copie de la partie spécifiée d’une liste.";
Blockly.Msg["LISTS_SPLIT_HELPURL"] = "https://github.com/google/blockly/wiki/Lists#splitting-strings-and-joining-lists";
Blockly.Msg["LISTS_SPLIT_LIST_FROM_TEXT"] = "créer une liste depuis le texte";
Blockly.Msg["LISTS_SPLIT_TEXT_FROM_LIST"] = "créer un texte depuis la liste";
Blockly.Msg["LISTS_SPLIT_TOOLTIP_JOIN"] = "Réunir une liste de textes en un seul, en les séparant par un séparateur.";
Blockly.Msg["LISTS_SPLIT_TOOLTIP_SPLIT"] = "Couper un texte en une liste de textes, en coupant à chaque séparateur.";
Blockly.Msg["LISTS_SPLIT_WITH_DELIMITER"] = "avec séparateur";
Blockly.Msg["LISTS_SORT_HELPURL"] = "https://github.com/google/blockly/wiki/Lists#sorting-a-list";
Blockly.Msg["LISTS_SORT_ORDER_ASCENDING"] = "croissant";
Blockly.Msg["LISTS_SORT_ORDER_DESCENDING"] = "décroissant";
Blockly.Msg["LISTS_SORT_TITLE"] = "trier %1 %2 %3";
Blockly.Msg["LISTS_SORT_TOOLTIP"] = "Trier une copie d’une liste.";
Blockly.Msg["LISTS_SORT_TYPE_IGNORECASE"] = "alphabétique, en ignorant la casse";
Blockly.Msg["LISTS_SORT_TYPE_NUMERIC"] = "numérique";
Blockly.Msg["LISTS_SORT_TYPE_TEXT"] = "alphabétique";

// Procedures blocks.
Blockly.Msg["PROCEDURES_DEFNORETURN_HELPURL"] = "https://en.wikipedia.org/wiki/Subroutine";
Blockly.Msg["PROCEDURES_DEFNORETURN_TITLE"] = "définir";
Blockly.Msg["PROCEDURES_DEFNORETURN_PROCEDURE"] = "nom_de_la_fonction";
Blockly.Msg["PROCEDURES_BEFORE_PARAMS"] = "avec :";
Blockly.Msg["PROCEDURES_CALL_BEFORE_PARAMS"] = Blockly.Msg["PROCEDURES_BEFORE_PARAMS"];
Blockly.Msg["PROCEDURES_DEFNORETURN_DO"] = "";
Blockly.Msg["PROCEDURES_DEFNORETURN_TOOLTIP"] = "Crée une fonction sans sortie.";
Blockly.Msg["PROCEDURES_DEFNORETURN_COMMENT"] = "Décrire cette fonction…";
Blockly.Msg["PROCEDURES_DEFRETURN_HELPURL"] = "https://en.wikipedia.org/wiki/Subroutine";
Blockly.Msg["PROCEDURES_DEFRETURN_TITLE"] = Blockly.Msg["PROCEDURES_DEFNORETURN_TITLE"];
Blockly.Msg["PROCEDURES_DEFRETURN_PROCEDURE"] = Blockly.Msg["PROCEDURES_DEFNORETURN_PROCEDURE"];
Blockly.Msg["PROCEDURES_DEFRETURN_DO"] = Blockly.Msg["PROCEDURES_DEFNORETURN_DO"];
Blockly.Msg["PROCEDURES_DEFRETURN_COMMENT"] = Blockly.Msg["PROCEDURES_DEFNORETURN_COMMENT"];
Blockly.Msg["PROCEDURES_DEFRETURN_RETURN"] = "retour";
Blockly.Msg["PROCEDURES_DEFRETURN_TOOLTIP"] = "Crée une fonction avec une sortie.";
Blockly.Msg["PROCEDURES_ALLOW_STATEMENTS"] = "autoriser les ordres";
Blockly.Msg["PROCEDURES_DEF_DUPLICATE_WARNING"] = "Attention : Cette fonction a des paramètres en double.";
Blockly.Msg["PROCEDURES_CALLNORETURN_HELPURL"] = "https://fr.wikipedia.org/wiki/Sous-programme";
Blockly.Msg["PROCEDURES_CALLNORETURN_TOOLTIP"] = "Exécuter la fonction '%1' définie par l’utilisateur.";
Blockly.Msg["PROCEDURES_CALLRETURN_HELPURL"] = "https://fr.wikipedia.org/wiki/Sous-programme";
Blockly.Msg["PROCEDURES_CALLRETURN_TOOLTIP"] = "Exécuter la fonction '%1' définie par l’utilisateur et utiliser son résultat.";
Blockly.Msg["PROCEDURES_HIGHLIGHT_DEF"] = "Surligner la définition de la fonction";
Blockly.Msg["PROCEDURES_CREATE_DO"] = "Créer '%1'";
Blockly.Msg["PROCEDURES_IFRETURN_TITLE"] = "si";
Blockly.Msg["PROCEDURES_IFRETURN_HELPURL"] = "http://c2.com/cgi/wiki?GuardClause";
Blockly.Msg["PROCEDURES_IFRETURN_TOOLTIP"] = "Si une valeur est vraie, alors renvoyer une seconde valeur.";
Blockly.Msg["PROCEDURES_IFRETURN_WARNING"] = "Attention : Ce bloc pourrait n’être utilisé que dans une définition de fonction.";

// Display - Micro:bit 
Blockly.Msg["SHOW_LEDS_TITLE"] = "afficher l'image";
Blockly.Msg["SHOW_STRING_TITLE"] = "afficher le texte";
Blockly.Msg["SHOW_NUMBER_TITLE"] = "afficher le nombre";
Blockly.Msg["SHOW_ICON_TITLE"] = "afficher l'icone";
Blockly.Msg["SHOW_GAUGE_TITLE"] = "afficher la jauge de %1 Maximum %2";
Blockly.Msg["SHOW_GAUGE_TOOLTIP"] = "Affiche la gauge d'une mesure d'un capteur (ou d'un nombre) sur l'écran de la carte micro:bit en définissant la valeur maximale correspondant à la jauge remplie.";
Blockly.Msg["SET_PIXEL_TITLE"] = "contrôler la led x %1 y %2 état %3";
Blockly.Msg["SET_PIXEL_TOOLTIP"] = "Permet de contrôler l'état (ON/OFF) de chaque LED de l'écran de la carte micro:bit.";
Blockly.Msg["SET_LIGHT_PIXEL_TITLE"] = "contrôler la led x %1 y %2 luminosité %3";
Blockly.Msg["SET_LIGHT_PIXEL_TOOLTIP"] = "Permet de choisir la luminosité (de 0 à 9) des LED de l'écran de la carte micro:bit.";
Blockly.Msg["SHOW_CLOCK_TITLE"] = "afficher l'horloge";
Blockly.Msg["SHOW_ARROW_TITLE"] = "afficher la flèche";
Blockly.Msg["SHOW_ARROW_TOOLTIP"] = "Permet d'afficher les flèches de 8 directions (N,NE,E,SE,S...) sur l'écran de la carte micro:bit.";
Blockly.Msg["CLEAR_TITLE"] = "effacer l'écran";
// Display - Screen
Blockly.Msg["DISPLAY_LCD_SETTEXT_TITLE"] = "[LCD1602] afficher le texte %1 sur la ligne %2";
Blockly.Msg["DISPLAY_LCD_SETTEXT_TOOLTIP"] = IMG_MODULE_LCD_3V3 + Blockly.Tooltip.SEP + "Affiche du texte sur l'une des deux lignes de l'écran LCD1602 grove. Brancher le module sur un port I2C";
Blockly.Msg["DISPLAY_LCD_CLEAR_TITLE"] = "[LCD1602] nettoyer l'écran";
Blockly.Msg["DISPLAY_LCD_CLEAR_TOOLTIP"] = IMG_MODULE_LCD_3V3 + Blockly.Tooltip.SEP + "Permet d'effacer tous les caractères de l'écran LCD. Brancher le module sur un port I2C.";
Blockly.Msg["DISPLAY_OLED_ADDTEXT_TITLE"] = "[Ecran OLED] afficher le texte %1 à la position x %2 y %3";
Blockly.Msg["DISPLAY_OLED_ADDTEXT_TOOLTIP"] = IMG_MODULE_OLED + Blockly.Tooltip.SEP + "Permet d'écrire du texte sur un écran OLED grove avec 4x11 caractères. Brancher l'afficheur sur un port I2C.";
Blockly.Msg["DISPLAY_OLED_SETPIXEL_TITLE"] = "[Ecran OLED] controler le pixel x %1 y %2 état %3";
Blockly.Msg["DISPLAY_OLED_SETPIXEL_TOOLTIP"] = IMG_MODULE_OLED + Blockly.Tooltip.SEP + "Permet de contrôler chaque pixel de l'écran OLED 16x32 pixels. Brancher l'afficheur sur un port I2C.";
Blockly.Msg["DISPLAY_OLED_CLEARSCREEN_TITLE"] = "[Ecran OLED] effacer l'écran";
Blockly.Msg["DISPLAY_OLED_CLEARSCREEN_TOOLTIP"] = IMG_MODULE_OLED + Blockly.Tooltip.SEP + "Permet de nettoyer l'écran OLED. Brancher le module sur un port I2C.";
Blockly.Msg["DISPLAY_OLED_DRAWICON_TITLE"] = "[Ecran OLED] afficher l'icône %1 position x %2 y %3 état %4";
Blockly.Msg["DISPLAY_OLED_DRAWICON_TOOLTIP"] = IMG_MODULE_OLED + Blockly.Tooltip.SEP + "Permet d'afficher une icône de la librairie Image de micro:bit, à la position (x,y) sur l'écran grove OLED. Brancher l'afficheur sur un port I2C.";
// Display - LED modules
Blockly.Msg["DISPLAY_SETGROVELED_TITLE"] = "[Module LED] contrôler la LED %1 sur la broche  %2";

Blockly.Msg["DISPLAY_SETGROVELED_TOOLTIP"] = IMG_MODULE_LED + Blockly.Tooltip.SEP + "Permet d'activer ou désactiver la LED Grove (0 ou 1) sur les broches digitales P0 à P20.";
Blockly.Msg["DISPLAY_SETLEDINTENSITY_TITLE"] = "[LED] régler la luminosité à %1 sur la broche %2";
Blockly.Msg["DISPLAY_SETLEDINTENSITY_TOOLTIP"] = IMG_MODULE_LED_PWM + Blockly.Tooltip.SEP + "Permet de régler la luminosité d'une LED de 0 à 255 sur les broches PWM.";
Blockly.Msg["DISPLAY_NEOPIXEL_DEFINE_TITLE"] = "[Neopixel] définir %1 LED sur la broche %2";
Blockly.Msg["DISPLAY_NEOPIXEL_DEFINE_TOOLTIP"] = IMG_MODULE_NEOPIXEL + Blockly.Tooltip.SEP + "Permet de définir le nombre de LED du neopixel. Ce bloc doit être utilisé dans le bloc 'Au démarrage'.";
Blockly.Msg["DISPLAY_NEOPIXEL_LEDCONTROL_TITLE"] = "[Neopixel] contrôler la LED %1 à R %2 G %3 B %4 sur la broche %5";
Blockly.Msg["DISPLAY_NEOPIXEL_LEDCONTROL_TOOLTIP"] = IMG_MODULE_NEOPIXEL + Blockly.Tooltip.SEP + "Permet de contrôler la couleur de chaque LED tel que (R,G,B) de 0 à 255 du module neopixel.";
Blockly.Msg["DISPLAY_NEOPIXEL_SETPALETTECOLOR_TITLE"] = "[Neopixel] contrôler la LED %1 à %2 sur la broche %3";
Blockly.Msg["DISPLAY_NEOPIXEL_SETPALETTECOLOR_TOOLTIP"] = IMG_MODULE_NEOPIXEL + Blockly.Tooltip.SEP + "Permet de contrôler la couleur de chaque LED du module neopixel. Utiliser la palette pour changer la couleur.";
Blockly.Msg["DISPLAY_NEOPIXEL_SETALLLEDRGB_TITLE"] = "[Neopixel] contrôler toutes les LED à R %1 G %2 B %3 sur la broche %4";
Blockly.Msg["DISPLAY_NEOPIXEL_SETALLLEDRGB_TOOLTIP"] = IMG_MODULE_NEOPIXEL + Blockly.Tooltip.SEP + "Permet de contrôler toutes les LED du module neopixel à la couleur choisie telle que (R,G,B) soit de 0 à 255.";
Blockly.Msg["DISPLAY_NEOPIXEL_SETALLLEDCOLOR_TITLE"] = "[Neopixel] contrôler toutes les LED à %1 sur la broche %2";
Blockly.Msg["DISPLAY_NEOPIXEL_SETALLLEDCOLOR_TOOLTIP"] = IMG_MODULE_NEOPIXEL + Blockly.Tooltip.SEP + "Permet de contrôler toutes les LED du module neopixel à la couleur choisie. Utiliser la palette pour changer la couleur.";
Blockly.Msg["DISPLAY_NEOPIXEL_RAINBOW_TITLE"] = "[Neopixel] Arc-en-ciel sur la broche %1";
Blockly.Msg["DISPLAY_NEOPIXEL_RAINBOW_TOOLTIP"] = IMG_MODULE_NEOPIXEL + Blockly.Tooltip.SEP + "Afficher le spectre des couleurs sur les LED RGB. Il est possible de modifier la broche et le nombre de LED du module neopixel.";
Blockly.Msg["DISPLAY_4DIGIT_SETNUMBER_TITLE"] = "[Afficheur 4-digit] afficher %1 %2 sur les broches CLK %3 DIO %4";
Blockly.Msg["DISPLAY_4DIGIT_SETNUMBER_TOOLTIP"] = IMG_MODULE_4DIGITDISPLAY + Blockly.Tooltip.SEP + "Permet d'afficher un nombre, une température ou l'horloge sur l'afficheur 4-digit grove avec les broches digitales P0 à P20.";
Blockly.Msg["DISPLAY_4DIGIT_SETCLOCK_TITLE"] = "[Afficheur 4-digit] l'horloge sur les broches CLK %1 DIO %2";
Blockly.Msg["DISPLAY_4DIGIT_SETCLOCK_TOOLTIP"] = IMG_MODULE_4DIGITDISPLAY + Blockly.Tooltip.SEP + "Permet d'afficher l'horloge sur l'afficheur 4-digit grove avec les broches digitales P0 à P20. Attention, l'heure réelle est récupérée seulement la carte microbit reste allumée.";
Blockly.Msg["DISPLAY_4DIGIT_NUMBER"] = "le nombre entier";
Blockly.Msg["DISPLAY_4DIGIT_TEMPERATURE"] = "la température";
Blockly.Msg["DISPLAY_LEDBARSETLEVEL_TITLE"] = "[Module LED Bar] afficher le niveau de %1 sur les broches DI %2 DCKI %3";
Blockly.Msg["DISPLAY_LEDBARSETLEVEL_TOOLTIP"] = IMG_MODULE_LED_BAR + Blockly.Tooltip.SEP + "Permet d'afficher le niveau de la valeur en entrée sur le module LED Bar avec les broches digitales P0 à P20";
Blockly.Msg["DISPLAY_TRAFFICLIGHT_SETLED_TITLE"] = "[Panneau de signalisation] contrôler la LED %1 à l'état %2";
Blockly.Msg["DISPLAY_TRAFFICLIGHT_SETLED_TOOLTIP"] = IMG_MODULE_TRAFFIC_LIGHT + Blockly.Tooltip.SEP + "Permet de contrôler le panneau de signalisation de la carte micro:bit.";
Blockly.Msg["DISPLAY_TRAFFICLIGHT_RED"] = "rouge";
Blockly.Msg["DISPLAY_TRAFFICLIGHT_ORANGE"] = "orange";
Blockly.Msg["DISPLAY_TRAFFICLIGHT_GREEN"] = "verte";
// Display - Morpion
Blockly.Msg["DISPLAY_MORPION_NEWGAME_TITLE"] = "[Ecran OLED] Morpion - nouvelle partie";
Blockly.Msg["DISPLAY_MORPION_NEWGAME_TOOLTIP"] = IMG_MODULE_OLED + Blockly.Tooltip.SEP + "Permet de lancer une nouvelle partie de morpion sur un afficheur OLED. Brancher l'afficheur sur un port I2C. Attention, le jeu du Morpion utilise toute la mémoire de la micro:bit, vous ne pouvez pas utiliser d'autres modules pendant le jeu.";
Blockly.Msg["DISPLAY_MORPION_MOVECURSOR_TITLE"] = "[Ecran OLED] Morpion - déplacer le curseur";
Blockly.Msg["DISPLAY_MORPION_MOVECURSOR_TOOLTIP"] = IMG_MODULE_OLED + Blockly.Tooltip.SEP + "Permet de déplacer le curseur dans la grille du morpion sur un afficheur OLED. Le curseur parcourt toute la grille case par case. Brancher l'afficheur sur un port I2C. Attention, le jeu du Morpion utilise toute la mémoire de la micro:bit, vous ne pouvez pas utiliser d'autres modules pendant le jeu.";
Blockly.Msg["DISPLAY_MORPION_SETPLAYERFIGURE_TITLE"] = "[Ecran OLED] Morpion - ajouter %1";
Blockly.Msg["DISPLAY_MORPION_SETPLAYERFIGURE_TOOLTIP"] = IMG_MODULE_OLED + Blockly.Tooltip.SEP + "Permet d'ajouter une figure (croix ou cercle) dans la grille du morpion sur un afficheur OLED. Brancher l'afficheur sur un port I2C. Attention, le jeu du Morpion utilise toute la mémoire de la micro:bit, vous ne pouvez pas utiliser d'autres modules pendant le jeu.";
Blockly.Msg["DISPLAY_MORPION_CROSS"] = "une croix";
Blockly.Msg["DISPLAY_MORPION_CIRCLE"] = "un cercle";
Blockly.Msg["DISPLAY_MORPION_ISENDGAME_TITLE"] = "[Ecran OLED] Morpion - partie terminée ?";
Blockly.Msg["DISPLAY_MORPION_ISENDGAME_TOOLTIP"] = IMG_MODULE_OLED + Blockly.Tooltip.SEP + "Permet de retourner l'état de la partie en cours du morpion. Brancher l'afficheur sur un port I2C. Attention, le jeu du Morpion utilise toute la mémoire de la micro:bit, vous ne pouvez pas utiliser d'autres modules pendant le jeu.";
// Display - Games
Blockly.Msg["DISPLAY_GAMES_LEFT"] = "gauche";
Blockly.Msg["DISPLAY_GAMES_RIGHT"] = "droite";
Blockly.Msg["DISPLAY_GAMES_UP"] = "haut";
Blockly.Msg["DISPLAY_GAMES_DOWN"] = "bas";
Blockly.Msg["DISPLAY_GAMES_CREATESPRITE_TITLE"] = "créer un sprite à x %1 y %2";
Blockly.Msg["DISPLAY_GAMES_CREATESPRITE_TOOLTIP"] = "Créer un sprite sur l'écran de la micro:bit.";
Blockly.Msg["DISPLAY_GAMES_DELETESPRITE_TITLE"] = "supprimer %1";
Blockly.Msg["DISPLAY_GAMES_DELETESPRITE_TOOLTIP"] = "Supprime le sprite sélectionné dans les variables.";
Blockly.Msg["DISPLAY_GAMES_ISSPRITEDELETED_TITLE"] = "%1 est supprimé ?";
Blockly.Msg["DISPLAY_GAMES_ISSPRITEDELETED_TOOLTIP"] = "Retourne 'True' si le sprite selectionné est supprimé.";
Blockly.Msg["DISPLAY_GAMES_MOVESPRITE_TITLE"] = "déplacer %1 de %2 vers %3";
Blockly.Msg["DISPLAY_GAMES_MOVESPRITE_TOOLTIP"] = "Déplace le sprite sélectionné vers la direction choisie.";
Blockly.Msg["DISPLAY_GAMES_GETSPRITEPOSITION_TITLE"] = "position %2 de %1";
Blockly.Msg["DISPLAY_GAMES_GETSPRITEPOSITION_TOOLTIP"] = "Retourne les coordonnées du sprite sélectionné.";
Blockly.Msg["DISPLAY_GAMES_CHANGESCORE_TITLE"] = "incrémenter le score de %1";
Blockly.Msg["DISPLAY_GAMES_CHANGESCORE_TOOLTIP"] = "Augmente le score.";
Blockly.Msg["DISPLAY_GAMES_GETSCORE_TITLE"] = "score";
Blockly.Msg["DISPLAY_GAMES_GETSCORE_TOOLTIP"] = "Retourne le score du jeu.";
Blockly.Msg["DISPLAY_GAMES_STOPGAME_TITLE"] = "arrêter le jeu";
Blockly.Msg["DISPLAY_GAMES_STOPGAME_TOOLTIP"] = "Arrêter le jeu en cours.";
Blockly.Msg["DISPLAY_GAMES_ISENDGAME_TITLE"] = "fin du jeu";
Blockly.Msg["DISPLAY_GAMES_ISENDGAME_TOOLTIP"] = "Retourne 'True' si le jeu est arrêté.";
Blockly.Msg["DISPLAY_GAMES_RESTARTGAME_TITLE"] = "recommencer le jeu";
Blockly.Msg["DISPLAY_GAMES_RESTARTGAME_TOOLTIP"] = "Permet de démarrer le jeu.";

// Input/Output - Micro:bit
Blockly.Msg["IO_PAUSE_TITLE"] = "attendre %1 %2";
Blockly.Msg["IO_PAUSE_TOOLTIP"] = "Effectue une pause dans l'exécution du code.";
Blockly.Msg["IO_PAUSE_SECOND"] = "seconde(s)";
Blockly.Msg["IO_PAUSE_MILLISECOND"] = "milliseconde(s)";
Blockly.Msg["IO_INITCHRONOMETER_TITLE"] = "initialise le chronomètre";
Blockly.Msg["IO_INITCHRONOMETER_TOOLTIP"] = "Initialise un chronomètre à 0 (en secondes).";
Blockly.Msg["IO_GETCHRONOMETER_TITLE"] = "valeur du chronomètre en %1";
Blockly.Msg["IO_GETCHRONOMETER_TOOLTIP"] = "Retourne la valeur du chronomètre à partir de l'initialisation (en secondes ou millisecondes).";
Blockly.Msg["IO_IFBUTTONPRESSED_TITLE"] = "si bouton %1 %2 appuyé alors";

Blockly.Msg["IO_TOUCH_NORTH"] = "haut";
Blockly.Msg["IO_TOUCH_SOUTH"] = "bas";
Blockly.Msg["IO_TOUCH_EST"] = "droit";
Blockly.Msg["IO_TOUCH_WEST"] = "gauche";

Blockly.Msg["IO_ISPRESSED"] = "est";
Blockly.Msg["IO_WASPRESSED"] = "a été";
Blockly.Msg["IO_IFBUTTONPRESSED_TOOLTIP"] = "Exécute des instructions si les boutons A ou B sont préssés.";
Blockly.Msg["IO_ONPINTOUCHED_TITLE"] = "si %1 est touché alors";
Blockly.Msg["IO_ONPINTOUCHED_TOOLTIP"] = "Exécute des instructions si le logo de la carte micro:bit ou si la broche P0, P1 ou P2 est appuyée (ou touchée).";
Blockly.Msg["IO_ONMOVEMENT_TITLE"] = "si %1 alors";
Blockly.Msg["IO_ONMOVEMENT_SHAKE"] = "secoué";
Blockly.Msg["IO_ONMOVEMENT_UP"] = "logo vers le haut";
Blockly.Msg["IO_ONMOVEMENT_DOWN"] = "logo vers le bas";
Blockly.Msg["IO_ONMOVEMENT_FACE_UP"] = "écran vers le haut";
Blockly.Msg["IO_ONMOVEMENT_FACE_DOWN"] = "écran vers le bas";
Blockly.Msg["IO_ONMOVEMENT_LEFT"] = "penché à gauche";
Blockly.Msg["IO_ONMOVEMENT_RIGHT"] = "penché à droite";
Blockly.Msg["IO_ONMOVEMENT_FREEFALL"] = "chute libre";
Blockly.Msg["IO_ONMOVEMENT_TOOLTIP"] = "Exécute des instructions si la carte micro:bit est secouée.";
Blockly.Msg["IO_ISBUTTONPRESSED_TITLE"] = "bouton %1 %2 appuyé";
Blockly.Msg["IO_ISBUTTONPRESSED_TOOLTIP"] = "Retourne 'True' si le bouton A ou B est pressé, et 'False' sinon.";
Blockly.Msg["IO_ISPINTOUCHED_TITLE"] = "%1 est touché";
Blockly.Msg["IO_ISPINTOUCHED_TOOLTIP"] = "Retourne 'True' si le logo de la carte micro:bit ou si la broche P0, P1 ou P2 est touchée, et 'False' sinon.";
// Input/Output - Microphone module
Blockly.Msg["IO_MICRO_LOUD"] = "fort";
Blockly.Msg["IO_MICRO_QUIET"] = "faible";
Blockly.Msg["IO_MICRO_IS"] = "est";
Blockly.Msg["IO_MICRO_WAS"] = "a été";
Blockly.Msg["IO_MICRO_ONSOUNDDETECTED_TITLE"] = "%1 si son %2 %3 détecté alors";
Blockly.Msg["IO_MICRO_ONSOUNDDETECTED_TOOLTIP"] = "Exécute des instructions si un son (fort/faible) est détecté. Option 'was: Exécute des instructions si un son (fort/faible) s'est produit depuis le dernier appel de la fonction 'was_sound()'."
Blockly.Msg["IO_MICRO_GETCURRENTSOUND_TITLE"] = "%1 état du son";
Blockly.Msg["IO_MICRO_GETCURRENTSOUND_TOOLTIP"] = "Retourne l'état du son (fort/faible).";
Blockly.Msg["IO_MICRO_WASSOUNDDETECTED_TITLE"] = "%1 son %2 a été détecté";
Blockly.Msg["IO_MICRO_WASSOUNDDETECTED_TOOLTIP"] = "Retourne 'True' si un son (fort/faible) s'est produit depuis le dernier appel de la fonction 'was_sound()'.";
Blockly.Msg["IO_MICRO_GETSOUNDLEVEL_TITLE"] = "%1 intensité du son";
Blockly.Msg["IO_MICRO_GETSOUNDLEVEL_TOOLTIP"] = "Permet d'obtenir l'intensité du son.";
Blockly.Msg["IO_MICRO_GETHISTORYSOUND_TITLE"] = "%1 historique des sons";
Blockly.Msg["IO_MICRO_GETHISTORYSOUND_TOOLTIP"] = "Retourne l'historique des sons depuis le dernier appel de la fonction 'get_sounds()'.";
Blockly.Msg["IO_MICRO_SETSOUNDTHRESHOLD_TITLE"] = "%1 définir le seuil des sons %2 à %3";
Blockly.Msg["IO_MICRO_SETSOUNDTHRESHOLD_TOOLTIP"] = "Permet de définir un seuil du niveau sonore des sons forts/faibles de 0 à 255.";
Blockly.Msg["IO_MICRO_SOUNDCONDITION_TITLE"] = "%1 %2";
Blockly.Msg["IO_MICRO_SOUNDCONDITION_TOOLTIP"] = "Permet d'utiliser les constantes (LOUD/QUIET) de la librairie microphone dans les blocs de la catégorie 'Logique'";
// Input/Output - External modules
Blockly.Msg["IO_GROVEKEYPAD_GETNUMBER_TITLE"] = "[Clavier numérique] chiffre sur les broches RX %1 TX %2";
Blockly.Msg["IO_GROVEKEYPAD_GETNUMBER_TOOLTIP"] = IMG_MODULE_KEYPAD + Blockly.Tooltip.SEP + "Permet d'obtenir la touche appuyée du clavier numérique grove sur les broches TX et RX. Quand vous connectez le module, s'assurer que les broches sont 'croisées' : RX de la carte avec TX du module et inversement.";
Blockly.Msg["IO_GROVEJOYSTICK_GETAXIS_TITLE"] = "[Module joytsick] valeur de l'axe %1 sur les broches A0 %2 A1 %3";
Blockly.Msg["IO_GROVEJOYSTICK_GETAXIS_TOOLTIP"] = IMG_MODULE_JOYSTICK + Blockly.Tooltip.SEP + "Retourne la valeur de l'axe X ou Y (de 0 à 1023) du joystick Grove sur les broches P0 à P4, ou P10.";
Blockly.Msg["IO_GETGROVESLIDEPOTENTIOMETER_TITLE"] = "[Potentiomètre linéaire] valeur sur la broche %1";
Blockly.Msg["IO_GETGROVESLIDEPOTENTIOMETER_TOOLTIP"] = IMG_MODULE_SLIDE_POT + Blockly.Tooltip.SEP + "Retourne la position (de 0 à 1023) du potentiomètre linéaire Grove sur les broches P0 à P4, ou P10.";
Blockly.Msg["IO_GETGROVEROTARYANGLE_TITLE"] = "[Potentiomètre rotatif] valeur sur la broche %1";
Blockly.Msg["IO_GETGROVEROTARYANGLE_TOOLTIP"] = IMG_MODULE_ROTARY_ANGLE + Blockly.Tooltip.SEP + "Retourne l'angle (de 0 à 1023) du potentiomètre rotatif Grove sur les broches P0 à P4, ou P10.";
Blockly.Msg["IO_GROVECOLOREDBUTTON_GET_TITLE"] = "[Module bouton coloré] état sur la broche SIG2 %1 ";
Blockly.Msg["IO_GROVECOLOREDBUTTON_GET_TOOLTIP"] = IMG_MODULE_LED_BUTTON + Blockly.Tooltip.SEP + "Retourne l'état du bouton coloré grove (0 or 1) sur les broches digitales P0 à P20.";
Blockly.Msg["IO_GROVECOLOREDBUTTON_SETLED_TITLE"] = "[Module bouton coloré] controler la LED à l'état %1 sur la broche SIG1 %2";
Blockly.Msg["IO_GROVECOLOREDBUTTON_SETLED_TOOLTIP"] = IMG_MODULE_LED_BUTTON + Blockly.Tooltip.SEP + "Permet d'allumer ou éteindre la LED (0 or 1) sur les broches digitales P0 à P20.";
Blockly.Msg["IO_GETGROVETACTILE_TITLE"] = "[Capteur tactile] état sur la broche %1 ";
Blockly.Msg["IO_GETGROVETACTILE_TOOLTIP"] = IMG_MODULE_TOUCH + Blockly.Tooltip.SEP + "Retourne la valeur du capteur tactile Grove (0 ou 1) sur les broches digitales P0 à P20.";
Blockly.Msg["IO_GETGROVEBUTTON_TITLE"] = "[Module bouton] état sur la broche %1 ";
Blockly.Msg["IO_GETGROVEBUTTON_TOOLTIP"] = IMG_MODULE_BUTTON + Blockly.Tooltip.SEP + "Retourne la valeur du bouton Grove (0 ou 1) sur les broches digitales P0 à P20.";
Blockly.Msg["IO_GETGROVESWITCH_TITLE"] = "[Module interrupteur] état sur la broche %1 ";
Blockly.Msg["IO_GETGROVESWITCH_TOOLTIP"] = IMG_MODULE_SWITCH + Blockly.Tooltip.SEP + "Retourne la valeur de l'interrupteur Grove (0 ou 1) sur les broches digitales P0 à P20.";
// Input/Output - Pins
Blockly.Msg["IO_DIGITALSIGNAL_TITLE"] = "%1";
Blockly.Msg["IO_DIGITALSIGNAL_HIGH"] = "HAUT";
Blockly.Msg["IO_DIGITALSIGNAL_LOW"] = "BAS";
Blockly.Msg["IO_DIGITALSIGNAL_TOOLTIP"] = "Retourne une valeur booléene (1 si HAUT ou 0 si BAS).";
Blockly.Msg["IO_READDIGITALPIN_TITLE"] = "lire la broche numérique %1";
Blockly.Msg["IO_READDIGITALPIN_TOOLTIP"] = "Permet de lire la valeur d'une entrée digitale (0 ou 1).";
Blockly.Msg["IO_WRITEDIGITALPIN_TITLE"] = "écrire sur la broche numérique %1 l'état %2";
Blockly.Msg["IO_WRITEDIGITALPIN_TOOLTIP"] = "Permet d'écrire une valeur sur une entrée digitale (0 ou 1).";
Blockly.Msg["IO_READANALOGPIN_TITLE"] = "lire la broche analogique %1";
Blockly.Msg["IO_READANALOGPIN_TOOLTIP"] = "Permet de lire la valeur d'une entrée analogique (0-1023).";
Blockly.Msg["IO_WRITEANALOGPIN_TITLE"] = "écrire sur la broche analogique %1 la valeur %2";
Blockly.Msg["IO_WRITEANALOGPIN_TOOLTIP"] = "Permet d'écrire une valeur sur une entrée analogique (0-1023). Cette fonction n'écris pas réellement une valeur analogique. Elle utilise la PWM (Pulse-Width Modulation). Par exemple, écrire 511 fournit un signal ayant 50% du cycle, la tension moyenne est donc 1,65V.";
Blockly.Msg["IO_SETPWM_TITLE"] = "appliquer un signal de période (μs) %1 sur la broche %2";
Blockly.Msg["IO_SETPWM_TOOLTIP"] = "Permet d'appliquer un signal PWM sur une broche de période minimale 256 us.";
Blockly.Msg["IO_READPULSEIN_TITLE"] = "lire l'impulsion (μs) de l'état %1 sur la broche %2";
Blockly.Msg["IO_READPULSEIN_TOOLTIP"] = "Retourne la durée de l'impulsion entrante à l'état HAUT ou BAS en (us).";

// Communication - Micro:bit
Blockly.Msg["COMMUNICATION_RADIO_SENDSTRING_TITLE"] = "  %1 envoyer la chaîne %2";
Blockly.Msg["COMMUNICATION_RADIO_SENDSTRING_TOOLTIP"] = "Permet d'envoyer uen chaîne de caractères via la radio de la carte micro:bit.";
Blockly.Msg["COMMUNICATION_RADIO_SENDNUMBER_TITLE"] = "%1 envoyer le nombre %2";
Blockly.Msg["COMMUNICATION_RADIO_SENDNUMBER_TOOLTIP"] = "Permet d'envoyer des nombres via la radio de la carte micro:bit.";
Blockly.Msg["COMMUNICATION_RADIO_SENDVALUE_TITLE"] = "%1 envoyer la valeur %2 = %3";
Blockly.Msg["COMMUNICATION_RADIO_SENDVALUE_TOOLTIP"] = "Permet d'envoyer des données avec un nom et sa valeur via la radio de la carte micro:bit.";
Blockly.Msg["COMMUNICATION_RADIO_ONSTRINGRECEIVED_TITLE"] = "%1 si une chaîne est reçue dans %2 alors";
Blockly.Msg["COMMUNICATION_RADIO_ONSTRINGRECEIVED_TOOLTIP"] = "Permet d'exécuter des instructions si une chaîne de caractères est reçue par radio dans la variable 'stringData'.";
Blockly.Msg["COMMUNICATION_RADIO_ONNUMBERRECEIVED_TITLE"] = "%1 si un nombre est reçu dans %2 alors";
Blockly.Msg["COMMUNICATION_RADIO_ONNUMBERRECEIVED_TOOLTIP"] = "Permet d'exécuter des instructions si un nombre est reçu par radio dans la variable 'numberData'.";
Blockly.Msg["COMMUNICATION_RADIO_ONVALUERECEIVED_TITLE"] = "%1 si une valeur est reçue dans %2 %3 alors";
Blockly.Msg["COMMUNICATION_RADIO_ONVALUERECEIVED_TOOLTIP"] = "Permet d'exécuter des instructions si un nom et sa valeur sont reçus par radio dans les variables 'name' et 'value'.";
Blockly.Msg["COMMUNICATION_RADIO_CONFIG_TITLE"] = "%1 configurer Canal %2 Puissance %3 Taille des données %4 Groupe %5";
Blockly.Msg["COMMUNICATION_RADIO_CONFIG_TOOLTIP"] = "Permet de configurer le canal de la radio (de 0 to 83), la taille des données transmises (en octets), la puissance de transmission (de 0 à 7)  ainsi que le group (de 0 à 255).";
// Communication - Serial connection
Blockly.Msg["COMMUNICATION_SERIAL_WRITE_TITLE"] = "%1 écrire dans le port série %2";
Blockly.Msg["COMMUNICATION_SERIAL_WRITE_TOOLTIP"] = "Permet d'écrire des données dans le port série.";
Blockly.Msg["COMMUNICATION_SERIAL_INIT_TITLE"] = "%1 rediriger la connexion série vers RX %3 TX %4 Baudrate %2";
Blockly.Msg["COMMUNICATION_SERIAL_INIT_TOOLTIP"] = "Permet de rediriger la connexion série de la carte micro:bit. Quand vous connecter le module, être assuré que les broches sont 'croisées' : RX de la carte avec TX du module et inversement.";
Blockly.Msg["COMMUNICATION_SERIAL_REDIRECTTOUSB_TITLE"] = "%1 rediriger la connexion série vers USB";
Blockly.Msg["COMMUNICATION_SERIAL_REDIRECTTOUSB_TOOLTIP"] = "Permet de rediriger la connexion série vers l'USB (ordinateur). Cela peut être utilisé pour faire fonctionner plusieurs modules UART en même temps.";
Blockly.Msg["COMMUNICATION_SERIAL_ONDATARECEIVED_TITLE"] = "%1 si une donnée est reçue du port série dans %2 alors";
Blockly.Msg["COMMUNICATION_SERIAL_ONDATARECEIVED_TOOLTIP"] = "Permet d'exécuter des instructions si une donnée est reçue par le port série dans la variable 'serialData'.";
Blockly.Msg["COMMUNICATION_COMPUTER_PLAYNOTE_TITLE"] = "%1 jouer la note %2 sur l'ordinateur";
Blockly.Msg["COMMUNICATION_COMPUTER_PLAYNOTE_TOOLTIP"] = "Joue la note selectionnée jusqu'à l'exécution du bloc \"Fin de la note\".";
Blockly.Msg["NOTE_C"] = "Do";
Blockly.Msg["NOTE_C_SHARP"] = "Do#";
Blockly.Msg["NOTE_D"] = "Ré";
Blockly.Msg["NOTE_D_SHARP"] = "Ré#";
Blockly.Msg["NOTE_E"] = "Mi";
Blockly.Msg["NOTE_F"] = "Fa";
Blockly.Msg["NOTE_F_SHARP"] = "Fa#";
Blockly.Msg["NOTE_G"] = "Sol";
Blockly.Msg["NOTE_G_SHARP"] = "Sol#";
Blockly.Msg["NOTE_A"] = "La";
Blockly.Msg["NOTE_A_SHARP"] = "La#";
Blockly.Msg["NOTE_B"] = "Si";
Blockly.Msg["COMMUNICATION_COMPUTER_SETFREQUENCY_TITLE"] = "%1 Jouer la fréquence %2 (Hz) sur l'ordinateur";
Blockly.Msg["COMMUNICATION_COMPUTER_SETFREQUENCY_TOOLTIP"] = "Ce bloc permet de jouer une fréquence donnée sur l'ordinateur";
Blockly.Msg["COMMUNICATION_COMPUTER_STOPMUSIC_TITLE"] = "%1 terminer la note sur l'ordinateur";
Blockly.Msg["COMMUNICATION_COMPUTER_STOPMUSIC_TOOLTIP"] = "Arrête la note en cours.";
Blockly.Msg["COMMUNICATION_WRITEGRAPH_TITLE"] = "tracer le graphe";
Blockly.Msg["COMMUNICATION_WRITEGRAPH_TOOLTIP"] = "Ce bloc permet d'écrire des données (numériques) qui seront visibles dans le traceur. Il peut être utilisé avec un ou plusieurs blocs au format \"Nom\" et \"Données\". Pour visualiser les graphiques, cliquer sur l'icone \'Mode Graphique\' dans la console.";
Blockly.Msg["COMMUNICATION_DATA"] = "Donnée";
Blockly.Msg["COMMUNICATION_PRINT_DATAS_TITLE"] = "Nom %1 Données %2";
Blockly.Msg["COMMUNICATION_PRINT_DATAS_TOOLTIP"] = "Ce bloc est à utiliser avec le bloc \"Tracer le graphique\". Il doit lui-même contenir le nom de la valeur à afficher (texte), et la valeur en question (nombre).";
// Communication - Data logging
Blockly.Msg["COMMUNICATION_OPENLOG_WRITE_TITLE"] = "%1 écrire dans la carte SD %2 sur les broches RX %3 TX %4 %5 Données %6";
Blockly.Msg["COMMUNICATION_OPENLOG_WRITE_TOOLTIP"] = IMG_MODULE_OPENLOG + Blockly.Tooltip.SEP + "Permet d'écrire des données dans la carte micro SD avec le module Openlog. Fonctionnement en transmission UART.";
// Communication - Wireless transmission
Blockly.Msg["COMMUNICATION_BLUETOOTH_SENDDATA_TITLE"] = "%1 envoyer sur les broches RX %2 TX %3 message %4";
Blockly.Msg["COMMUNICATION_BLUETOOTH_SENDDATA_TOOLTIP"] = IMG_MODULE_HC05 + Blockly.Tooltip.SEP + "Permet d'envoyer des données via le module Bluetooth HC05 sur les broches RX/TX.";
Blockly.Msg["COMMUNICATION_BLUETOOTH_ONDATARECEIVED_TITLE"] = "%1 si message reçu sur les broches RX %2 TX %3 dans %4 alors";
Blockly.Msg["COMMUNICATION_BLUETOOTH_ONDATARECEIVED_TOOLTIP"] = IMG_MODULE_HC05 + Blockly.Tooltip.SEP + "Permet d'exécuter des instructions si une donnée est reçue par un module Bluetooth HC05 dans la variable 'bluetoothData' sur les broches RX/TX.";
// Communication - Tracking modules
Blockly.Msg["COMMUNICATION_GPS_ONDATARECEIVED_TITLE"] = "%1 si une donnée est reçue sur les broches RX %2 TX %3 dans %4 alors";
Blockly.Msg["COMMUNICATION_GPS_ONDATARECEIVED_TOOLTIP"] = IMG_MODULE_GPS + Blockly.Tooltip.SEP + "Permet d'exécuter des instructions si une donnée est reçue par GPS dans la variable 'gpsData' sur les broches RX/TX.";
Blockly.Msg["COMMUNICATION_GPS_GETINFORMATIONS_TITLE"] = "%1 obtenir %2 avec les données %3";
Blockly.Msg["COMMUNICATION_GPS_GETINFORMATIONS_TOOLTIP"] = IMG_MODULE_GPS + Blockly.Tooltip.SEP + "Retourne les données analysées du GPS parmi ('horloge', 'latitude, 'longitude', 'altitude', 'toute la trame')";
Blockly.Msg["COMMUNICATION_GPS_INFO_CLOCK"] = "l'heure";
Blockly.Msg["COMMUNICATION_GPS_INFO_LATITUDE"] = "la latitude °Nord";
Blockly.Msg["COMMUNICATION_GPS_INFO_LONGITUDE"] = "la longitude °Est";
Blockly.Msg["COMMUNICATION_GPS_INFO_SATELLITE"] = "le nombre de satellites utilisés";
Blockly.Msg["COMMUNICATION_GPS_INFO_ALTITUDE"] = "l'altitude (m)";
Blockly.Msg["COMMUNICATION_GPS_INFO_ALL_FRAME"] = "toute la trame";

// Sensors - Micro:bit
Blockly.Msg["SENSORS_GETACCELERATION_TITLE"] = "accélération (mg) %1";
Blockly.Msg["SENSORS_GETACCELERATION_TOOLTIP"] = "Retourne l'accélération (en mg) grâce à l'accéléromètre de la carte micro:bit.";
Blockly.Msg["SENSORS_GETLIGHT_TITLE"] = "luminosité";
// Blockly.Msg["SENSORS_GETLIGHT_TOOLTIP"] = "Retourne la luminosité (de 0 à 255) grâce à la quelques LED de l'écran de la carte micro:bit.";
Blockly.Msg["SENSORS_GETLIGHT_TOOLTIP"] = "Retourne la valeur de la luminosité (de 0 à 255) grâce à la LED de la carte Galaxia.";
Blockly.Msg["SENSORS_GETCOMPASS_TITLE"] = "direction de la boussole (°)";
Blockly.Msg["SENSORS_GETCOMPASS_TOOLTIP"] = "Retourne la direction (de 0° à 360°) de la carte micro:bit grâce au compas interne.";
Blockly.Msg["SENSORS_CALIBRATECOMPASS_TITLE"] = "calibrer la boussole";
Blockly.Msg["SENSORS_CALIBRATECOMPASS_TOOLTIP"] = "Permet de calibrer le compas de la carte micro:bit. Il suffit de secouée la carte pour le calibrer. Un smiley 'heureux' apparaît lorsque la calibration est terminée.";
Blockly.Msg["SENSORS_ISCOMPASSCALIBRATED_TITLE"] = "boussole est calibrée ?";
Blockly.Msg["SENSORS_ISCOMPASSCALIBRATED_TOOLTIP"] = "Retourne 'True' si la boussole est calibrée, retourne 'False' sinon.";
Blockly.Msg["SENSORS_GETTEMPERATURE_TITLE"] = "température en %1";
Blockly.Msg["SENSORS_GETTEMPERATURE_TOOLTIP"] = "Retourne la température en degré Celius (°C), Fahrenheit (°F) ou Kelvin (K) du processeur de la carte micro:bit.";
Blockly.Msg["SENSORS_GETROTATION_TITLE"] = "rotation (°) %1";
Blockly.Msg["SENSORS_GETROTATION_PITCH"] = "tangage";
Blockly.Msg["SENSORS_GETROTATION_ROLL"] = "roulis";
Blockly.Msg["SENSORS_GETROTATION_TOOLTIP"] = "Retourne la rotation (de -180° à 180°) grâce à l'accéléromètre de la carte micro:bit.";
Blockly.Msg["SENSORS_GETMAGNETICFORCE_TITLE"] = "force du champ magnétique (µT) %1";
Blockly.Msg["SENSORS_GETMAGNETICFORCE_TOOLTIP"] = "Retourne la valeur du champ magnétique (en µTesla) grâce au compas de la carte micro:bit.";
// Sensors - Gas
Blockly.Msg["SENSORS_SGP30_READDATA_TITLE"] = "[Capteur SGP30] gaz %1";
Blockly.Msg["SENSORS_SGP30_READDATA_TOOLTIP"] = IMG_MODULE_SGP30 + Blockly.Tooltip.SEP + "Renvoie la quantité de CO2 (en ppm) ou de TVOC (en ppb) contenu dans l'air grâce au capteur SGP30. Brancher le capteur sur un port I2C.";
Blockly.Msg["SENSORS_SGP30_CO2"] = "Dioxyde de carbone (CO2) (ppm)";
Blockly.Msg["SENSORS_SGP30_TVOC"] = "Composés organiques volatiles (TVOC) (ppb)";
Blockly.Msg["SENSORS_MULTICHANNEL_GETGAS_TITLE"] = "[Capteur de gaz multicanal] gaz %1 (ppm)";
Blockly.Msg["SENSORS_MULTICHANNEL_GETGAS_TOOLTIP"] = IMG_MODULE_MULTICHANNEL + Blockly.Tooltip.SEP + "Renvoie la quantité du gaz mesuré dans l'air (en ppm) grâce au capteur de gaz multicanal. Brancher le capteur sur un port I2C.";
Blockly.Msg["SENSORS_MULTICHANNELV2_GETGAS_TITLE"] = "[Capteur de gaz multicanal v2] gaz %1 (V)";
Blockly.Msg["SENSORS_MULTICHANNELV2_GETGAS_TOOLTIP"] = IMG_MODULE_MULTICHANNEL_V2 + Blockly.Tooltip.SEP + "Renvoie la quantité du gaz mesuré dans l'air (en V) grâce au capteur de gaz multicanal V2 grove. Brancher le capteur sur un port I2C.";
Blockly.Msg["GAS_CO"] = "Monoxyde de carbone (CO)";
Blockly.Msg["GAS_NO2"] = "Dioxyde d'azote (NO2)";
Blockly.Msg["GAS_C2H5OH"] = "Ethanol (C2H5OH)";
Blockly.Msg["GAS_H2"] = "Dihydrogène (H2)";
Blockly.Msg["GAS_NH3"] = "Ammoniac (NH3)";
Blockly.Msg["GAS_CH4"] = "Méthane (CH4)";
Blockly.Msg["GAS_C3H8"] = "Propane (C3H8)";
Blockly.Msg["GAS_C4H10"] = "Iso-propane (C4H10)";
Blockly.Msg["GAS_VOC"] = "Composés organiques volatiles (COV)";
Blockly.Msg["SENSORS_O2_GAS_READDATA_TITLE"] = "[Capteur de dioxygène] O2 (%) sur la broche %1";
Blockly.Msg["SENSORS_O2_GAS_READDATA_TOOLTIP"] = IMG_MODULE_O2 + Blockly.Tooltip.SEP + "Retourne la concentration de dioxygène (O2) dans l'air (en %) grâce au capteur de gas O2 grove sur les broches P0 à P4, ou P10.";
Blockly.Msg["SENSORS_AIR_QUALITY_GETVALUE_TITLE"] = "[Capteur de qualité d'air] valeur sur la broche %1";
Blockly.Msg["SENSORS_AIR_QUALITY_GETVALUE_TOOLTIP"] = IMG_MODULE_AIR_QUALITY + Blockly.Tooltip.SEP + "Retourne la valeur de la qualité de l'air (de 0 à 1023) sur les broches P0 à P4, ou P10.";
Blockly.Msg["SENSORS_HM330X_GETPARTICULE_TITLE"] = "[Capteur HM330X] concentration de particules fines %1 (µg/m3)";
Blockly.Msg["SENSORS_HM330X_GETPARTICULE_TOOLTIP"] = IMG_MODULE_HM330X + Blockly.Tooltip.SEP + "Détecte la densité de particules dans l'air avec le capteur HM330X. Bancher le capteur sur un port I2C.";
Blockly.Msg["SENSORS_HM330X_ATM_PM1"] = "PM1.0";
Blockly.Msg["SENSORS_HM330X_ATM_PM2_5"] = "PM2.5";
Blockly.Msg["SENSORS_HM330X_ATM_PM10"] = "PM10";
// Sensors - Climate
Blockly.Msg["SENSORS_TEMPERATURE"] = "température";
Blockly.Msg["SENSORS_HUMIDITY"] = "humidité (%)";
Blockly.Msg["SENSORS_TEMPERATURE_IN"] = "en";
Blockly.Msg["SENSORS_BMP280_READDATA_TITLE"] = "[Capteur BMP280 %1] %2";
Blockly.Msg["SENSORS_BMP280_READDATA_TOOLTIP"] = IMG_MODULE_BMP280 + Blockly.Tooltip.SEP + "Renvoie la température ambiante en degré Celsius (°C), Fahrenheit (°F) ou Kelvin (K), la pression (en Pascal) ou l'altitude (en m). L'altitude est calculée avec la pression et est initialisée à 0 au début du programme. Le bloc a besoin du capteur Grove BMP280 (adresse I2C: 0x77, couleur: bleu) ou le capteur HW-611 280 (adresse I2C: 0x76, couleur: violet). Brancher le capteur sur un port I2C.";
Blockly.Msg["SENSORS_BMP280_TEMPERATURE"] = "température";
Blockly.Msg["SENSORS_BMP280_PRESSURE"] = "pression (Pa)";
Blockly.Msg["SENSORS_BMP280_ALTITUDE"] = "altitude (m)";
Blockly.Msg["SENSORS_GETGROVEHIGHTEMP_TITLE"] = "[Capteur H.T°] température en %1 sur les broches A0 %2 A1 %3";
Blockly.Msg["SENSORS_GETGROVEHIGHTEMP_TOOLTIP"] = IMG_MODULE_HIGH_TEMPERATURE + Blockly.Tooltip.SEP + "Renvoie la température du thermocouple en degré Celsius (50 à 600 °C), Fahrenheit (°F) ou Kelvin (K) grâce au capteur grove de haute température sur les broches P0 à P4, ou P10.";
Blockly.Msg["SENSORS_GETGROVEMOISTURE_TITLE"] = "[Capteur d'humidité] humidité du sol sur la broche %1";
Blockly.Msg["SENSORS_GETGROVEMOISTURE_TOOLTIP"] = IMG_MODULE_MOISTURE + Blockly.Tooltip.SEP + "Retourne l'humidité (de 0 à 1023) mesurée grâce au capteur d'humidité grove sur les broches P0/P14, P1/P15 ou P2/P16.";
Blockly.Msg["SENSORS_DHT_READDATA_TITLE"] = "[Capteur DHT11] %1 sur la broche %2";
Blockly.Msg["SENSORS_DHT_READDATA_TOOLTIP"] = IMG_MODULE_DHT11 + Blockly.Tooltip.SEP + "Retourne la température en degré Celsius (°C), Fahrenheit (°F) ou Kelvin (K), ou l'humidité (en %) grâce au capteur dht11 grove sur les broches digitales de P0 à P20.";
Blockly.Msg["SENSORS_TH02_READDATA_TITLE"] = "[Capteur TH02] %1";
Blockly.Msg["SENSORS_TH02_READDATA_TOOLTIP"] = IMG_MODULE_TH02 + Blockly.Tooltip.SEP + "Retourne la température en degré Celsius (°C), Fahrenheit (°F) ou Kelvin (K), ou l'humidité (en %) grâce au capteur TH02. Brancher le capteur sur un port I2C.";
Blockly.Msg["SENSORS_SHT31_READDATA_TITLE"] = "[Capteur SHT31] %1";
Blockly.Msg["SENSORS_SHT31_READDATA_TOOLTIP"] = IMG_MODULE_SHT31 + Blockly.Tooltip.SEP + "Retourne la température en degré Celsius (°C), Fahrenheit (°F) ou Kelvin (K), ou l'humidité (en %) grâce au capteur SHT31. Brancher le capteur sur un port I2C.";
Blockly.Msg["SENSORS_GETGROVETEMPERATURE_TITLE"] = "[Capteur de T°] température en %1 sur la broche %2";
Blockly.Msg["SENSORS_GETGROVETEMPERATURE_TOOLTIP"] = IMG_MODULE_TEMPERATURE + Blockly.Tooltip.SEP + "Retourne la température en degré Celsius (°C), Fahrenheit (°F) ou Kelvin (K) du capteur de température Grove sur les broches P0 à P4, ou P10.";
Blockly.Msg["SENSORS_GETGROVEWATER_TITLE"] = "[Capteur d'eau] quantité d'eau sur la broche %1";
Blockly.Msg["SENSORS_GETGROVEWATER_TOOLTIP"] = IMG_MODULE_WATER + Blockly.Tooltip.SEP + "Retourne la quantité d'eau (de 0 à 255) mesurée grâce au capteur d'eau grove sur les broches P0/P14, P1/P15 ou P2/P16.";
Blockly.Msg["SENSORS_GETRAINGAUGE_TITLE"] = "[Capteur de pluie] état sur la broche %1";
Blockly.Msg["SENSORS_GETRAINGAUGE_TOOLTIP"] = IMG_MODULE_RAIN_GAUGE + Blockly.Tooltip.SEP + "Retourne l'état du capteur de pluie (1 s'il pleut ou 0 sinon) sur les broches digitales P0 à P20.";
Blockly.Msg["SENSORS_GETANEMOMETER_TITLE"] = "[Anémomètre] état sur la broche %1";
Blockly.Msg["SENSORS_GETANEMOMETER_TOOLTIP"] = IMG_MODULE_ANEMOMETER + Blockly.Tooltip.SEP + "Retourne l'état de l'anémomètre (deux fois état HAUT à chaque rotation) sur les broches digitales P0 à P20.";
// Sensors - Sound & Light
Blockly.Msg["SENSORS_GETGROVELIGHT_TITLE"] = "[Capteur de lumière] luminosité sur la broche %1";
Blockly.Msg["SENSORS_GETGROVELIGHT_TOOLTIP"] = IMG_MODULE_LIGHT + Blockly.Tooltip.SEP + "Retourne la luminosité (de 0 à 1023) du capteur de lumière Grove sur les broches P0 à P4, ou P10.";
Blockly.Msg["SENSORS_SI1145_GETLIGHT_TITLE"] = "[Capteur SI1145] luminosité %1";
Blockly.Msg["SENSORS_SI1145_GETLIGHT_TOOLTIP"] = IMG_MODULE_SI1145 + Blockly.Tooltip.SEP + "Renvoie l'indice de lumière ultraviolette, la luminosité visible (en lumen) ou infrarouge (en lumen) grâce au capteur Grove Sunlight ou le capteur GY1145. Brancher le capteur sur un port I2C.";
Blockly.Msg["SENSORS_SI1145_UV"] = "indice UV";
Blockly.Msg["SENSORS_SI1145_VISIBLE"] = "visible (lumen)";
Blockly.Msg["SENSORS_SI1145_IR"] = "infrarouge (lumen)";
Blockly.Msg["SENSORS_GETUVINDEX_TITLE"] = "[Capteur ultraviolet] indice UV sur la broche %1";
Blockly.Msg["SENSORS_GETUVINDEX_TOOLTIP"] = IMG_MODULE_UV + Blockly.Tooltip.SEP + "Retourne l'indice de la lumière ultraviolette pour des ondes entre 240 et 380 nm du capteur Grove sur les broches P0 à P4, ou P10.";
Blockly.Msg["SENSORS_GETGROVESOUND_TITLE"] = "[Capteur de son] niveau sonore sur la broche %1";
Blockly.Msg["SENSORS_GETGROVESOUND_TOOLTIP"] = IMG_MODULE_SOUND_LOUDNESS + Blockly.Tooltip.SEP + "Retourne le niveau sonore (0 à 1023) avec le capteur de son Grove sur les broches digitales P0 à P20.";
// Sensors - Distance & Movement
Blockly.Msg["SENSORS_GETGROVEULTRASONIC_TITLE"] = "[Capteur à ultrasons] %1 sur les broches TRIG %2 ECHO %3";
Blockly.Msg["SENSORS_GETGROVEULTRASONIC_TOOLTIP"] = IMG_MODULE_ULTRASONIC + Blockly.Tooltip.SEP + "Retourne la distance (in cm) mesurée grâce au capteur grove à ultrasons sur les broches digitales P0 à P20. Attention, si le capteur est un modèle grove, TRIG et ECHO sont sur la même broche SIG.";
Blockly.Msg["SENSORS_ULTRASONIC_DISTANCE"] = "distance (cm)";
Blockly.Msg["SENSORS_ULTRASONIC_DURATION"] = "durée de l'aller-retour (µs)";
Blockly.Msg["SENSORS_GETGESTURE_TITLE"] = "[Capteur de gestes] type de geste";
Blockly.Msg["SENSORS_GETGESTURE_TOOLTIP"] = IMG_MODULE_GESTURE + Blockly.Tooltip.SEP + "Retourne le type de geste analysé ('right', 'left', 'up', 'down', 'forward', 'backward', 'clockwise', 'anticlockwise') grâce au capteur de gestes grove. Brancher le capteur sur un port I2C.";
Blockly.Msg["SENSORS_ONGESTUREDETECTED_TITLE"] = "[Capteur de gestes] si le geste %1 est détecté";
Blockly.Msg["SENSORS_ONGESTUREDETECTED_TOOLTIP"] = IMG_MODULE_GESTURE + Blockly.Tooltip.SEP + "Exécute des instructions si le geste selectionné est détecté par le capteur de gestes grove. Brancher le capteur sur un port I2C.";
Blockly.Msg["SENSORS_GESTURE_RIGHT"] = "droit";
Blockly.Msg["SENSORS_GESTURE_LEFT"] = "gauche";
Blockly.Msg["SENSORS_GESTURE_UP"] = "haut";
Blockly.Msg["SENSORS_GESTURE_DOWN"] = "bas";
Blockly.Msg["SENSORS_GESTURE_FORWARD"] = "avant";
Blockly.Msg["SENSORS_GESTURE_BACKWARD"] = "arrière";
Blockly.Msg["SENSORS_GESTURE_CLOCKWISE"] = "horaire";
Blockly.Msg["SENSORS_GESTURE_ANTICLOCKWISE"] = "antihoraire";
Blockly.Msg["SENSORS_GESTURE_WAVE"] = "onde";
Blockly.Msg["SENSORS_GETGROVELINEFINDER_TITLE"] = "[Capteur de ligne noire] état sur la broche %1";
Blockly.Msg["SENSORS_GETGROVELINEFINDER_TOOLTIP"] = IMG_MODULE_LINE_FINDER + Blockly.Tooltip.SEP + "Retourne la valeur du capteur de ligne noire grove (0 ou 1) sur les broches digitales P0 à P20.";
Blockly.Msg["SENSORS_GETGROVEMOTION_TITLE"] = "[Capteur de mouvement] état sur la broche %1";
Blockly.Msg["SENSORS_GETGROVEMOTION_TOOLTIP"] = IMG_MODULE_MOTION + Blockly.Tooltip.SEP + "Retourne la valeur du capteur de mouvement Grove PIR Motion (0 ou 1) sur les broches digitales P0 à P20.";
Blockly.Msg["SENSORS_GETPIEZOVIBRATION_TITLE"] = "[Capteur de vibrations] état sur la broche %1";
Blockly.Msg["SENSORS_GETPIEZOVIBRATION_TOOLTIP"] = IMG_MODULE_VIBRATIONS + Blockly.Tooltip.SEP + "Retourne l'état de la vibration (0 ou 1) grâce au capteur de vibration piezoélectrique sur les broches digitales P0 à P20.";
Blockly.Msg["SENSORS_GETGROVETILT_TITLE"] = "[Module inclinaison] état sur la broche %1";
Blockly.Msg["SENSORS_GETGROVETILT_TOOLTIP"] = IMG_MODULE_TILT + Blockly.Tooltip.SEP + "Retourne la valeur de l'inclinaison du module Grove (0 ou 1) sur les broches digitales P0 à P20.";
// Autres capteurs
Blockly.Msg["SENSORS_GETGROVEBUTTON_TITLE"] = "[Module bouton] %1 sur la broche %2";
Blockly.Msg["SENSORS_GETGROVEBUTTON_TOOLTIP"] = IMG_MODULE_BUTTON + Blockly.Tooltip.SEP + "Retourne la valeur numérique du bouton Grove (0/1 ou 0V/3.3V) sur les broches digitales P0 à P20.";
Blockly.Msg["SENSORS_GETGROVEBUTTON_VOLTAGE"] = "tension";
Blockly.Msg["SENSORS_GETGROVEBUTTON_STATE"] = "état";

// Actuators
Blockly.Msg["ACTUATORS_SERVO_SETANGLE_TITLE"] = "[Servomoteur] contrôler l'angle à %1 sur la broche %2";
Blockly.Msg["ACTUATORS_SERVO_SETANGLE_TOOLTIP"] = IMG_MODULE_SERVO + Blockly.Tooltip.SEP + "Permet de contrôler l'angle d'un servomoteur (de 0 à 180) sur les broches digitales de P0 à P20. Attention, le montage doit être alimenté par une batterie pour fournir assez de courant au servomoteur.";
Blockly.Msg["ACTUATORS_CONTINUOUS_SERVO_SETSPEED_TITLE"] = "[Servomoteur continu] contrôler la vitesse à %1 (%) direction %2 sur la broche %3";
Blockly.Msg["ACTUATORS_CONTINUOUS_SERVO_SETSPEED_TOOLTIP"] = IMG_MODULE_CONTINUOUS_SERVO + Blockly.Tooltip.SEP + "Permet de contrôler la vittesse (de 0 à 100 %) d'un servomoteur continu sur les broches PWM.";
Blockly.Msg["ACTUATORS_MOTOR_SETPOWER_TITLE"] = "[Moteur] contrôler la puissance à %1 sur la broche %2";
Blockly.Msg["ACTUATORS_MOTOR_SETPOWER_TOOLTIP"] = IMG_MODULE_MOTOR + Blockly.Tooltip.SEP + "Permet de contrôler la puissance d'un moteur (de 0 à 1023) sur les broches digitales de P0 à P20. Attention, le montage doit être alimenté par une batterie pour fournir assez de courant au moteur.";
Blockly.Msg["ACTUATORS_GROVEVIBRATIONMOTOR_CONTROL_TITLE"] = "[Moteur à vibration] contrôler le moteur à l'état %1 sur la broche  %2";
Blockly.Msg["ACTUATORS_GROVEVIBRATIONMOTOR_CONTROL_TOOLTIP"] = IMG_MODULE_VIBRATION_MOTOR + Blockly.Tooltip.SEP + "Permet d'activer ou de désactiver le moteur à vibration grove (0 ou 1) sur les broches digitales P0 à P20.";
Blockly.Msg["ACTUATORS_GROVERELAY_CONTROL_TITLE"] = "[Module relais] contrôler le relais à l'état %1 sur la broche %2";
Blockly.Msg["ACTUATORS_GROVERELAY_CONTROL_TOOLTIP"] = IMG_MODULE_RELAY + Blockly.Tooltip.SEP + "Permet de contrôler la valeur du relais (0 ou 1) sur les broches digitales P0 à P20.";
Blockly.Msg["ACTUATORS_MUSIC_PLAYMUSIC_TITLE"] = "%1 jouer la musique %2 sur %3";
Blockly.Msg["ACTUATORS_MUSIC_PLAYMUSIC_TOOLTIP"] = IMG_MODULE_BUZZER_SPEAKER + Blockly.Tooltip.SEP + "Permet de jouer une musique avec le module buzzer ou speaker grove sur les broches digitales P0 à P20.";
Blockly.Msg["ACTUATORS_MUSIC_PLAYSONG_TITLE"] = "%1 jouer la mélodie %2 %3 sur %4";
Blockly.Msg["ACTUATORS_MUSIC_PLAYSONG_ONCE"] = "une fois";
Blockly.Msg["ACTUATORS_MUSIC_PLAYSONG_LOOP"] = "en boucle";
Blockly.Msg["ACTUATORS_MUSIC_PLAYSONG_TOOLTIP"] = IMG_MODULE_BUZZER_SPEAKER + Blockly.Tooltip.SEP + "Permet de jouer une musique avec un module buzzer ou speaker sur les broches digitales P0 à P20.";
Blockly.Msg["ACTUATORS_MUSIC_PLAYNOTES_TITLE"] = "jouer les notes sur";
Blockly.Msg["ACTUATORS_MUSIC_PLAYNOTES_TOOLTIP"] = IMG_MODULE_BUZZER_SPEAKER + Blockly.Tooltip.SEP + "Permet de jouer des notes avec un module buzzer ou speaker sur les broches digitales P0 à P20.";
Blockly.Msg["ACTUATORS_MUSIC_NOTE_TITLE"] = "note %1 à l'octave %2 durée %3";
Blockly.Msg["ACTUATORS_MUSIC_NOTE_TOOLTIP"] = IMG_MODULE_BUZZER_SPEAKER + Blockly.Tooltip.SEP + "Permet de définir une note avec son octave et sa durée.";
Blockly.Msg["NOTE_C"] = "Do";
Blockly.Msg["NOTE_C_SHARP"] = "Do#";
Blockly.Msg["NOTE_D"] = "Ré";
Blockly.Msg["NOTE_D_SHARP"] = "Ré#";
Blockly.Msg["NOTE_E"] = "Mi";
Blockly.Msg["NOTE_F"] = "Fa";
Blockly.Msg["NOTE_F_SHARP"] = "Fa#";
Blockly.Msg["NOTE_G"] = "Sol";
Blockly.Msg["NOTE_G_SHARP"] = "Sol#";
Blockly.Msg["NOTE_A"] = "La";
Blockly.Msg["NOTE_A_SHARP"] = "La#";
Blockly.Msg["NOTE_B"] = "Si";
Blockly.Msg["MUSIC_SILENCE"] = "Silence";
Blockly.Msg["ACTUATORS_MUSIC_PLAYFREQUENCY_TITLE"] = "%1 jouer la fréquence %2 pendant %3 (ms) sur %4";
Blockly.Msg["ACTUATORS_MUSIC_PLAYFREQUENCY_TOOLTIP"] = "Permet de jouer une fréquence sur le haut-parleur intégré à la micro:bit ou avec un module buzzer (ou speaker) sur les broches digitales P0 à P20.";
Blockly.Msg["ACTUATORS_MUSIC_STOP_TITLE"] = "%1 arrêter la musique sur %2";
Blockly.Msg["ACTUATORS_MUSIC_STOP_TOOLTIP"] = "Permet d'arrêter la musique en cours du haut-parleur intégré ou du buzzer sur les broches digitales P0 à P20.";
Blockly.Msg["ACTUATORS_MUSIC_SETVOLUME_TITLE"] = "%1 définir le volume à %2";
Blockly.Msg["ACTUATORS_MUSIC_SETVOLUME_TOOLTIP"] = "Permet de changer le volume du haut-parleur intégré à la micro:bit de 0 à 255.";
Blockly.Msg["ACTUATORS_MUSIC_SETTEMPO_TITLE"] = "%1 definir la pulsation %2 et le tempo %3";
Blockly.Msg["ACTUATORS_MUSIC_SETTEMPO_TOOLTIP"] = "Permet de définir le nombre de pulsations par mesure et le tempo de la musique jouée par les fonctions de la librairie music.";
Blockly.Msg["ACTUATORS_MUSIC_GETTEMPO_TITLE"] = "%1 tempo de la musique";
Blockly.Msg["ACTUATORS_MUSIC_GETTEMPO_TOOLTIP"] = "Permet d'obtenir le tempo de la musique dans un tuple d'entiers (ticks, bpm).";
Blockly.Msg["ACTUATORS_SPEECH_SAYSOMETHING_TITLE"] = "[Speech] dire %1 vitesse %2 hauteur %3";
Blockly.Msg["ACTUATORS_SPEECH_SAYSOMETHING_TOOLTIP"] = "Permet de dire une phrase avec la carte micro:bit. Il est possible de régler la vitesse et la hauteur de la voix (de 0 à 255).";

// Robots - Maqueen
Blockly.Msg["ROBOTS_MAQUEEN_RIGHT"] = "droit";
Blockly.Msg["ROBOTS_MAQUEEN_LEFT"] = "gauche";
Blockly.Msg["ROBOTS_MAQUEEN_RIGHT&LEFT"] = "droit & gauche";
Blockly.Msg["ROBOTS_MAQUEEN_ULTRASONICRANGER_TITLE"] = "%1 %2";
Blockly.Msg["ROBOTS_MAQUEEN_ULTRASONICRANGER_TOOLTIP"] = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + "Retourne la distance (en cm) ou la durée de l'aller-retour (en μs) de l'onde entre un object et le robot Maqueen grâce au capteur à ultrasons.";
Blockly.Msg["ROBOTS_MAQUEEN_ULTRASONIC_DISTANCE"] = "distance (cm)";
Blockly.Msg["ROBOTS_MAQUEEN_ULTRASONIC_DURATION"] = "durée de l'aller-retour (μs)";
Blockly.Msg["ROBOTS_MAQUEEN_CONTROLLED_TITLE"] = "%1 contrôler la LED %2 état %3";
Blockly.Msg["ROBOTS_MAQUEEN_CONTROLLED_TOOLTIP"] = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + "Permet de contrôler l'état de la LED gauche (connectée à la broche P8) ou droite (connectée à la broche P12) du robot Maqueen.";
Blockly.Msg["ROBOTS_MAQUEEN_GO_TITLE"] = "%1 contrôler le robot %2 vitesse %3";
Blockly.Msg["ROBOTS_MAQUEEN_GO_FORWARD"] = "avancer";
Blockly.Msg["ROBOTS_MAQUEEN_GO_REVERSE"] = "reculer";
Blockly.Msg["ROBOTS_MAQUEEN_GO_TOOLTIP"] = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + "Permet de contrôler la marche (AVANT/ARRIERE) ainsi que la vitesse (de 0 à 255) du robot Maqueen.";
Blockly.Msg["ROBOTS_MAQUEEN_CONTROLMOTOR_TITLE"] = "%1 contrôler le moteur %2 direction %3 vitesse %4";
Blockly.Msg["ROBOTS_MAQUEEN_CONTROLMOTOR_TOOLTIP"] = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + "Permet de contrôler les moteurs droit (adresse I2C: 0x02) et gauche (adresse I2C: 0x00) en changeant la direction (↻ : AVANT, ↺ : ARRIERE) et la vitesse (de 0 à 255) du robot Maqueen.";
Blockly.Msg["ROBOTS_MAQUEEN_STOPMOTORS_TITLE"] = "%1 arrêter le moteur %2";
Blockly.Msg["ROBOTS_MAQUEEN_STOPMOTORS_TOOLTIP"] = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + "Permet d'arrêter le moteur droit ou gauche du robot Maqueen.";
Blockly.Msg["ROBOTS_MAQUEEN_SETSERVOANGLE_TITLE"] = "%1 positionner le servomoteur %2 à l'angle %3";
Blockly.Msg["ROBOTS_MAQUEEN_SETSERVOANGLE_TOOLTIP"] = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + "Permet de contrôler les servomoteurs S1 (adresse I2C: 0x14) and S2 (adresse I2C: 0x15) en changeant l'angle (de 0 à 180) du robot Maqueen.";
Blockly.Msg["ROBOTS_MAQUEEN_S1"] = "S1";
Blockly.Msg["ROBOTS_MAQUEEN_S2"] = "S2";
Blockly.Msg["ROBOTS_MAQUEEN_SERVO_BOTH"] = "les deux";
Blockly.Msg["ROBOTS_MAQUEEN_READPATROL_TITLE"] = "%1 état du capteur de ligne %2";
Blockly.Msg["ROBOTS_MAQUEEN_READPATROL_TOOLTIP"] = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + "Permet de lire l'état du capteur suiveur de ligne gauche ou droit du robot Maqueen. Le bloc renvoit 1 si le capteur se trouve au dessus de la ligne.";
Blockly.Msg["ROBOTS_MAQUEEN_SETNEOPIXEL_TITLE"] = "%1 contrôler la LED %2 à R %3 G %4 B %5";
Blockly.Msg["ROBOTS_MAQUEEN_SETNEOPIXEL_TOOLTIP"] = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + "Permet de contrôler la couleur de chaque LED du neopixel du robot Maqueen (pin15) tel que les valeurs R,G,B soient comprises entre 0 et 255. Le neopixel est connecté à la broche P15 sur le robot.";
Blockly.Msg["ROBOTS_MAQUEEN_SETPALETTECOLOR_TITLE"] = "%1 contrôler la LED %2 à %3";
Blockly.Msg["ROBOTS_MAQUEEN_SETPALETTECOLOR_TOOLTIP"] = IMG_MODULE_NEOPIXEL + Blockly.Tooltip.SEP + "Permet de contrôler la couleur de chaque LED du module neopixel. Utiliser la palette pour changer la couleur.";
Blockly.Msg["ROBOTS_MAQUEEN_SETRAINBOW_TITLE"] = "%1 Arc-en-ciel";
Blockly.Msg["ROBOTS_MAQUEEN_SETRAINBOW_TOOLTIP"] = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + "Afficher le spectre des couleurs sur les LED RGB (pin15).";
Blockly.Msg["ROBOTS_MAQUEEN_SETBUZZER_TITLE"] = "%1 contrôler le buzzer à la fréquence %2 pendant %3 (ms)";
Blockly.Msg["ROBOTS_MAQUEEN_SETBUZZER_TOOLTIP"] = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + "Permet de définir une fréquence sur le buzzer du robot Maqueen (pin0). Le buzzer est connecté à la broche P0 sur le robot.";
Blockly.Msg["ROBOTS_MAQUEEN_PLAYMUSIC_TITLE"] = "%1 jouer la musique %2";
Blockly.Msg["ROBOTS_MAQUEEN_PLAYMUSIC_TOOLTIP"] = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + "Permet de jouer une musique avec le buzzer (pin0) du robot Maqueen.";
// Robots - Codo
Blockly.Msg["ROBOTS_CODO_GO_TITLE"] = "%1 contrôler le robot %2 vitesse %3";
Blockly.Msg["ROBOTS_CODO_GO_FORWARD"] = "avancer";
Blockly.Msg["ROBOTS_CODO_GO_BACKWARD"] = "reculer";
Blockly.Msg["ROBOTS_CODO_GO_TOOLTIP"] = IMG_ROBOT_CODO + Blockly.Tooltip.SEP + "Permet de contrôler la direction (avant/arrière) ainsi que la vitesse (de 0 à 255) du robot Codo.";
Blockly.Msg["ROBOTS_CODO_TURN_TITLE"] = "%1 tourner %2 vitesse %3";
Blockly.Msg["ROBOTS_CODO_TURN_RIGHT"] = "à droite";
Blockly.Msg["ROBOTS_CODO_TURN_LEFT"] = "à gauche";
Blockly.Msg["ROBOTS_CODO_TURN_TOOLTIP"] = IMG_ROBOT_CODO + Blockly.Tooltip.SEP + "Permet de contrôler la direction (droite/gauche) ainsi que la vitesse (de 0 to 255) du robot Codo.";
Blockly.Msg["ROBOTS_CODO_STOP_TITLE"] = "%1 arrêter la course du robot";
Blockly.Msg["ROBOTS_CODO_STOP_TOOLTIP"] = IMG_ROBOT_CODO + Blockly.Tooltip.SEP + "Permet d'arrêter la course du robot Codo.";
Blockly.Msg["ROBOTS_CODO_CONTROLMOTOR_TITLE"] = "%1 contrôler le moteur %2 direction %3 vitesse %4";
Blockly.Msg["ROBOTS_CODO_MOTOR_RIGHT"] = "droit";
Blockly.Msg["ROBOTS_CODO_MOTOR_LEFT"] = "gauche";
Blockly.Msg["ROBOTS_CODO_CONTROLMOTOR_TOOLTIP"] = IMG_ROBOT_CODO + Blockly.Tooltip.SEP + "Permet de contrôler le moteur (droit/gauche), direction (horaire/antihoraire) et la vitesse (de 0 à 255) du robot Codo.";
// Robots - Bit:Bot
Blockly.Msg["ROBOTS_BITBOT_READLIGHTSENSOR_TITLE"] = "[Bit:Bot] luminosité du capteur %1";
Blockly.Msg["ROBOTS_BITBOT_READLIGHTSENSOR_TOOLTIP"] = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + "Retourne la luminosité (de 0 à 255) grâce au capteur de lumière interne droit ou gauche du robot Bit:Bot.";
Blockly.Msg["ROBOTS_BITBOT_READPATROL_TITLE"] = "[Bit:Bot] état du capteur de ligne %1";
Blockly.Msg["ROBOTS_BITBOT_READPATROL_TOOLTIP"] = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + "Permet de lire l'état du capteur suiveur de ligne gauche ou droit du robot Bit:Bot. Le bloc renvoit 1 si le capteur se trouve au dessus de la ligne.";
Blockly.Msg["ROBOTS_BITBOT_GO_TITLE"] = "[Bit:Bot] contrôler le robot %1 vitesse %2";
Blockly.Msg["ROBOTS_BITBOT_GO_FORWARD"] = "Avancer";
Blockly.Msg["ROBOTS_BITBOT_GO_REVERSE"] = "Reculer";
Blockly.Msg["ROBOTS_BITBOT_GO_TOOLTIP"] = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + "Permet de contrôler la marche (AVANT/ARRIERE) ainsi que sa vitesse (de 0 à 1023) du robot Bit:Bot";
Blockly.Msg["ROBOTS_BITBOT_CONTROLMOTOR_TITLE"] = "[Bit:Bot] contrôler le moteur %1 direction %2 vitesse %3";
Blockly.Msg["ROBOTS_BITBOT_RIGHT"] = "Droit";
Blockly.Msg["ROBOTS_BITBOT_LEFT"] = "Gauche";
Blockly.Msg["ROBOTS_BITBOT_RIGHTANDLEFT"] = "Droit & Gauche";
Blockly.Msg["ROBOTS_BITBOT_CONTROLMOTOR_TOOLTIP"] = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + "Permet de contrôler les moteurs droit (pin0,pin8) et gauche (pin1,pin12) en changeant la direction (↻ : AVANT, ↺ : ARRIERE) et la vitesse (de 0 à 1023) du robot Bit:Bot.";
Blockly.Msg["ROBOTS_BITBOT_STOPMOTORS_TITLE"] = "[Bit:Bot] arrêter le moteur %1";
Blockly.Msg["ROBOTS_BITBOT_STOPMOTORS_TOOLTIP"] = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + "Permet d'arrêter le moteur droit ou gauche du robot Bit:Bot.";
Blockly.Msg["ROBOTS_BITBOT_SETNEOPIXEL_TITLE"] = "[Bit:Bot] contrôler la LED %1 à  R %2 G %3 B %4";
Blockly.Msg["ROBOTS_BITBOT_SETNEOPIXEL_TOOLTIP"] = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + "Permet de contrôler la couleur de chaque LED du neopixel du robot Bit:Bot tel que les valeurs R,G,B soient comprises entre 0 et 255. Le neopixel est connecté à la broche P13 sur le robot.";
Blockly.Msg["ROBOTS_BITBOT_SETPALETTECOLOR_TITLE"] = "[Bit:Bot] contrôler la LED %1 à %2";
Blockly.Msg["ROBOTS_BITBOT_SETPALETTECOLOR_TOOLTIP"] = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + "Permet de contrôler la couleur de chaque LED du module neopixel. Utiliser la palette pour changer la couleur des LED RGB du robot Bit:Bot.";
Blockly.Msg["ROBOTS_BITBOT_SETRAINBOW_TITLE"] = "[Bit:Bot] Arc-en-ciel";
Blockly.Msg["ROBOTS_BITBOT_SETRAINBOW_TOOLTIP"] = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + "Afficher le spectre des couleurs sur les LED RGB du robot Bit:Bot.";
// Robots - Gamepad
Blockly.Msg["ROBOTS_GAMEPAD_CONTROLLED_TITLE"] = "%1 contrôler la LED à l'état %2";
Blockly.Msg["ROBOTS_GAMEPAD_CONTROLLED_TOOLTIP"] = IMG_ROBOT_GAMEPAD + Blockly.Tooltip.SEP + "Permet de contrôler l'état de la LED de la manette 'Gamepad expansion DFR0536'. La LED est connectée à la broche P16 sur le module.";
Blockly.Msg["ROBOTS_GAMEPAD_SETMOTORVIBRATION_TITLE"] = "%1 contrôler la vibration de la manette à %2";
Blockly.Msg["ROBOTS_GAMEPAD_SETMOTORVIBRATION_TOOLTIP"] = IMG_ROBOT_GAMEPAD + Blockly.Tooltip.SEP + "Permet de contrôler l'état du moteur à vibrations de la manette 'Gamepad expansion DFR0536'. Le moteur à vibrations est connecté à la broche P12 sur le module.";
Blockly.Msg["ROBOTS_GAMEPAD_SETBUZZERFREQ_TITLE"] = "%1 contrôler le buzzer à la fréquence %2 pendant %3 (ms)";
Blockly.Msg["ROBOTS_GAMEPAD_SETBUZZERFREQ_TOOLTIP"] = IMG_ROBOT_GAMEPAD + Blockly.Tooltip.SEP + "Permet de définir une fréquence sur le buzzer de la manette 'Gamepad expansion DFR0536'. Le buzzer est connecté à la broche P0 sur le module.";
Blockly.Msg["ROBOTS_GAMEPAD_PLAYMUSIC_TITLE"] = "%1 jouer la musique %2";
Blockly.Msg["ROBOTS_GAMEPAD_PLAYMUSIC_TOOLTIP"] = IMG_ROBOT_GAMEPAD + Blockly.Tooltip.SEP + "Permet de jouer une musique avec le buzzer de la manette 'Gamepad expansion DFR0536'. Le buzzer est connecté à la broche P0 sur le module.";
Blockly.Msg["ROBOTS_GAMEPAD_ONBUTTONEVENT_TITLE"] = "%1 si le bouton %2 est %3 alors";
Blockly.Msg["ROBOTS_GAMEPAD_PRESSED"] = "pressé";
Blockly.Msg["ROBOTS_GAMEPAD_RELEASED"] = "relaché";
Blockly.Msg["ROBOTS_GAMEPAD_ONBUTTONEVENT_TOOLTIP"] = IMG_ROBOT_GAMEPAD + Blockly.Tooltip.SEP + "Exécute des instructions si un des boutons de la manette 'Gamepad Expansion DFR0536' est utilisé. Un bouton a deux états : 'pressé' ou 'relaché'. Table des broches : ([X,P1],[Y,P2],[UP,P8],[DOWN,P13],[LEFT,P14],[RIGHT,P15])";

// French messages for Galaxia. (FR)

// Display

Blockly.Msg["DISPLAY_GALAXIA_SHOW_TITLE"] = "afficher %1";
Blockly.Msg["DISPLAY_GALAXIA_SHOW_TOOLTIP"] = "Permet d'afficher un nombres ou une chaîne de caractère sur l'écran intégré à Galaxia.";

// Display - LED modules

Blockly.Msg["DISPLAY_GALAXIA_SET_LED_COLORS_TITLE"] = "contrôler la LED à R %1 V %2 B %3";
Blockly.Msg["DISPLAY_GALAXIA_SET_LED_COLORS_TOOLTIP"] = "Permet de régler l'intensité de la LED intégrée à la carte Galaxia, de 0 à 100.";

Blockly.Msg["DISPLAY_GALAXIA_LED_RED_CONTROL_TITLE"] = "régler l'intensité du rouge à %1";
Blockly.Msg["DISPLAY_GALAXIA_LED_RED_CONTROL_TOOLTIP"] = "Permet de régler l'intensité du rouge, de 0 à 100, pour la LED intégrée à la carte Galaxia.";

Blockly.Msg["DISPLAY_GALAXIA_LED_GREEN_CONTROL_TITLE"] = "régler l'intensité du vert à %1";
Blockly.Msg["DISPLAY_GALAXIA_LED_GREEN_CONTROL_TOOLTIP"] = "Permet de régler l'intensité du vert, de 0 à 100, pour la LED intégrée à la carte Galaxia.";

Blockly.Msg["DISPLAY_GALAXIA_LED_BLUE_CONTROL_TITLE"] = "régler l'intensité du bleu à %1";
Blockly.Msg["DISPLAY_GALAXIA_LED_BLUE_CONTROL_TOOLTIP"] = "Permet de régler l'intensité du bleu, de 0 à 100, pour la LED intégrée à la carte Galaxia.";

Blockly.Msg["DISPLAY_GALAXIA_LED_GET_RED_TITLE"] = "intensité de rouge";
Blockly.Msg["DISPLAY_GALAXIA_LED_GET_RED_TOOLTIP"] = "Retourne une valeur entre 0 et 100 correspondant à l'intensité de rouge actuellement tranférée à la LED.";

Blockly.Msg["DISPLAY_GALAXIA_LED_GET_GREEN_TITLE"] = "intensité de vert";
Blockly.Msg["DISPLAY_GALAXIA_LED_GET_GREEN_TOOLTIP"] = "Retourne une valeur entre 0 et 100 correspondant à l'intensité de vert actuellement tranférée à la LED.";

Blockly.Msg["DISPLAY_GALAXIA_LED_GET_BLUE_TITLE"] = "intensité de bleu";
Blockly.Msg["DISPLAY_GALAXIA_LED_GET_BLUE_TOOLTIP"] = "Retourne une valeur entre 0 et 100 correspondant à l'intensité de bleu actuellement tranférée à la LED.";

//  IO

Blockly.Msg["IO_ISTOUCHSENSITIVEBUTTONTOUCHED_TITLE"] = "bouton tactile %1 %2 touché";
Blockly.Msg["IO_ISTOUCHSENSITIVEBUTTONTOUCHED_TOOLTIP"] = "Retourne 'True' si le bouton tactile selectionné est touché, et 'False' sinon.";

Blockly.Msg["IO_GALAXIA_PAUSE_TITLE"] = "attendre %1 %2";
Blockly.Msg["IO_GALAXIA_PAUSE_TOOLTIP"] = "Effectue une pause dans l'exécution du code.";

// Actuators

Blockly.Msg["ACTUATORS_PLAY_FREQUENCY_TITLE"] = "définir la fréquence à %1 Hz";
Blockly.Msg["ACTUATORS_PLAY_FREQUENCY_TOOLTIP"] = "Permet de moduler la fréquence du son émis lorsque celui-ci est allumé.";

Blockly.Msg["ACTUATORS_BUZZER_ON"] = "Allumer";
Blockly.Msg["ACTUATORS_BUZZER_OFF"] = "Éteindre";

Blockly.Msg["ACTUATORS_BUZZER_ON_TITLE"] = "le son sur la fréquence";
Blockly.Msg["ACTUATORS_BUZZER_ON_TOOLTIP"] = "Permet de couper le son ou de l'allumer, sur la fréquence sélectionnée.";

Blockly.Msg["ACTUATORS_BUZZER_OFF_TITLE"] = "le son";
Blockly.Msg["ACTUATORS_BUZZER_OFF_TOOLTIP"] = "Permet de couper le son ou de l'allumer, sur la fréquence sélectionnée.";

// Communication

Blockly.Msg["COMMUNICATION_GALAXIA_RADIO_SENDVALUE_TITLE"] = "envoyer la valeur %1";
Blockly.Msg["COMMUNICATION_GALAXIA_RADIO_SENDVALUE_TOOLTIP"] = "Permet d'envoyer des données via la radio intégré à la carte Galaxia.";

Blockly.Msg["COMMUNICATION_GALAXIA_RADIO_ONRECEIVED_TITLE"] = "si une valeur est reçue dans %1 alors";
Blockly.Msg["COMMUNICATION_GALAXIA_RADIO_ONRECEIVED_TOOLTIP"] = "Permet d'exécuter des instructions si une valeur est reçue par radio dans la variable 'valeur'.";

Blockly.Msg["COMMUNICATION_GALAXIA_RADIO_DATA_RECEIVED_TITLE"] = "donnée reçu via radio";
Blockly.Msg["COMMUNICATION_GALAXIA_RADIO_DATA_RECEIVED_TOOLTIP"] = "Retourne la valeur de la donnée reçu via la radio intégré à la carte Galaxia.";

Blockly.Msg["COMMUNICATION_GALAXIA_RADIO_VALUE"] = "valeurRecue";

// Sensor

Blockly.Msg["SENSORS_GALAXIA_GETACCELERATION_TITLE"] = "accélération (mg) %1";
Blockly.Msg["SENSORS_GALAXIA_GETACCELERATION_TOOLTIP"] = "Retourne l'accélération (en milli-g) grâce à l'accéléromètre de la carte Galaxia.";

Blockly.Msg["SENSORS_GALAXIA_GETMAGNETICFORCE_TITLE"] = "force du champ magnétique (µT) %1";
Blockly.Msg["SENSORS_GALAXIA_GETMAGNETICFORCE_TOOLTIP"] = "Retourne la valeur du champ magnétique (en µTesla) grâce au compas de la carte Galaxia.";

Blockly.Msg["SETUP_TITLE"] = "Au démarrage de la carte %1 faire %2 Pour toujours %3 faire %4";
Blockly.Msg["SETUP_TOOLTIP"] = "Si tu veux faire une action une seule fois, mets-la dans « Au démarrage » sinon dans « Pour toujours ».";