/**
 * @license
 * Copyright 2012 Google LLC
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @fileoverview English strings.
 * @author fraser@google.com (Neil Fraser)
 *
 * After modifying this file, either run 'build.py' from the parent directory,
 * or run (from this directory):
 * ../i18n/js_to_json.py
 * to regenerate json/{en,qqq,synonyms}.json.
 *
 * To convert all of the json files to .js files, run:
 * ../i18n/create_messages.py json/*.json
 */
'use strict';


/**
 * Due to the frequency of long strings, the 80-column wrap rule need not apply
 * to message files.
 */

/**
 * Each message is preceded with a triple-slash comment that becomes the
 * message descriptor.  The build process extracts these descriptors, adds
 * them to msg/json/qqq.json, and they show up in the translation console.
 */

/** @type {string} */
/// {{Notranslate}} Hue value for all logic blocks.
Blockly.Msg.LOGIC_HUE = '210';
/** @type {string} */
/// {{Notranslate}} Hue value for all loop blocks.
Blockly.Msg.LOOPS_HUE = '120';
/** @type {string} */
/// {{Notranslate}} Hue value for all math blocks.
Blockly.Msg.MATH_HUE = '230';
/** @type {string} */
/// {{Notranslate}} Hue value for all text blocks.
Blockly.Msg.TEXTS_HUE = '160';
/** @type {string} */
/// {{Notranslate}} Hue value for all list blocks.
Blockly.Msg.LISTS_HUE = '260';
/** @type {string} */
/// {{Notranslate}} Hue value for all colour blocks.
Blockly.Msg.COLOUR_HUE = '20';
/** @type {string} */
/// {{Notranslate}} Hue value for all variable blocks.
Blockly.Msg.VARIABLES_HUE = '330';
/** @type {string} */
/// {{Notranslate}} Hue value for all variable dynamic blocks.
Blockly.Msg.VARIABLES_DYNAMIC_HUE = '310';
/** @type {string} */
/// {{Notranslate}} Hue value for all procedure blocks.
Blockly.Msg.PROCEDURES_HUE = '290';

/** @type {string} */
/// default name - A simple, general default name for a variable, preferably short.
/// For more context, see
/// [[Translating:Blockly#infrequent_message_types]].\n{{Identical|Item}}
Blockly.Msg.VARIABLES_DEFAULT_NAME = 'item';
/** @type {string} */
/// default name - A simple, default name for an unnamed function or variable. Preferably indicates that the item is unnamed.
Blockly.Msg.UNNAMED_KEY = 'unnamed';
/** @type {string} */
/// button text - Button that sets a calendar to today's date.\n{{Identical|Today}}
Blockly.Msg.TODAY = 'Today';

// Context menus.
/** @type {string} */
/// context menu - Make a copy of the selected block (and any blocks it contains).\n{{Identical|Duplicate}}
Blockly.Msg.DUPLICATE_BLOCK = 'Duplicate';
/** @type {string} */
/// context menu - Add a descriptive comment to the selected block.
Blockly.Msg.ADD_COMMENT = 'Add Comment';
/** @type {string} */
/// context menu - Remove the descriptive comment from the selected block.
Blockly.Msg.REMOVE_COMMENT = 'Remove Comment';
/** @type {string} */
/// context menu - Make a copy of the selected workspace comment.\n{{Identical|Duplicate}}
Blockly.Msg.DUPLICATE_COMMENT = 'Duplicate Comment';
/** @type {string} */
/// context menu - Change from 'external' to 'inline' mode for displaying blocks used as inputs to the selected block.  See [[Translating:Blockly#context_menus]].
Blockly.Msg.EXTERNAL_INPUTS = 'External Inputs';
/** @type {string} */
/// context menu - Change from 'internal' to 'external' mode for displaying blocks used as inputs to the selected block.  See [[Translating:Blockly#context_menus]].
Blockly.Msg.INLINE_INPUTS = 'Inline Inputs';
/** @type {string} */
/// context menu - Permanently delete the selected block.
Blockly.Msg.DELETE_BLOCK = 'Delete Block';
/** @type {string} */
/// context menu - Permanently delete the %1 selected blocks.\n\nParameters:\n* %1 - an integer greater than 1.
Blockly.Msg.DELETE_X_BLOCKS = 'Delete %1 Blocks';
/** @type {string} */
/// confirmation prompt - Question the user if they really wanted to permanently delete all %1 blocks.\n\nParameters:\n* %1 - an integer greater than 1.
Blockly.Msg.DELETE_ALL_BLOCKS = 'Delete all %1 blocks?';
/** @type {string} */
/// context menu - Reposition all the blocks so that they form a neat line.
Blockly.Msg.CLEAN_UP = 'Clean up Blocks';
/** @type {string} */
/// context menu - Make the appearance of the selected block smaller by hiding some information about it.
Blockly.Msg.COLLAPSE_BLOCK = 'Collapse Block';
/** @type {string} */
/// context menu - Make the appearance of all blocks smaller by hiding some information about it.  Use the same terminology as in the previous message.
Blockly.Msg.COLLAPSE_ALL = 'Collapse Blocks';
/** @type {string} */
/// context menu - Restore the appearance of the selected block by showing information about it that was hidden (collapsed) earlier.
Blockly.Msg.EXPAND_BLOCK = 'Expand Block';
/** @type {string} */
/// context menu - Restore the appearance of all blocks by showing information about it that was hidden (collapsed) earlier.  Use the same terminology as in the previous message.
Blockly.Msg.EXPAND_ALL = 'Expand Blocks';
/** @type {string} */
/// context menu - Make the selected block have no effect (unless reenabled).
Blockly.Msg.DISABLE_BLOCK = 'Disable Block';
/** @type {string} */
/// context menu - Make the selected block have effect (after having been disabled earlier).
Blockly.Msg.ENABLE_BLOCK = 'Enable Block';
/** @type {string} */
/// context menu - Provide helpful information about the selected block.\n{{Identical|Help}}
Blockly.Msg.HELP = 'Help';
/** @type {string} */
/// context menu - Undo the previous action.\n{{Identical|Undo}}
Blockly.Msg.UNDO = 'Undo';
/** @type {string} */
/// context menu - Undo the previous undo action.\n{{Identical|Redo}}
Blockly.Msg.REDO = 'Redo';

// Variable renaming.
/** @type {string} */
/// prompt - This message is only seen in the Opera browser.  With most browsers, users can edit numeric values in blocks by just clicking and typing.  Opera does not allows this, so we have to open a new window and prompt users with this message to chanage a value.
Blockly.Msg.CHANGE_VALUE_TITLE = 'Change value:';
/** @type {string} */
/// dropdown choice - When the user clicks on a variable block, this is one of the dropdown menu choices.  It is used to rename the current variable.  See [https://github.com/google/blockly/wiki/Variables#dropdown-menu https://github.com/google/blockly/wiki/Variables#dropdown-menu].
Blockly.Msg.RENAME_VARIABLE = 'Rename variable...';
/** @type {string} */
/// prompt - Prompts the user to enter the new name for the selected variable.  See [https://github.com/google/blockly/wiki/Variables#dropdown-menu https://github.com/google/blockly/wiki/Variables#dropdown-menu].\n\nParameters:\n* %1 - the name of the variable to be renamed.
Blockly.Msg.RENAME_VARIABLE_TITLE = 'Rename all \'%1\' variables to:';

// Variable creation
/** @type {string} */
/// button text - Text on the button used to launch the variable creation dialogue.
Blockly.Msg.NEW_VARIABLE = 'Create variable...';
/** @type {string} */
/// button text - Text on the button used to launch the variable creation dialogue.
Blockly.Msg.NEW_STRING_VARIABLE = 'Create string variable...';
/** @type {string} */
/// button text - Text on the button used to launch the variable creation dialogue.
Blockly.Msg.NEW_NUMBER_VARIABLE = 'Create number variable...';
/** @type {string} */
/// button text - Text on the button used to launch the variable creation dialogue.
Blockly.Msg.NEW_COLOUR_VARIABLE = 'Create colour variable...';
/** @type {string} */
/// prompt - Prompts the user to enter the type for a variable.
Blockly.Msg.NEW_VARIABLE_TYPE_TITLE = 'New variable type:';
/** @type {string} */
/// prompt - Prompts the user to enter the name for a new variable.  See [https://github.com/google/blockly/wiki/Variables#dropdown-menu https://github.com/google/blockly/wiki/Variables#dropdown-menu].
Blockly.Msg.NEW_VARIABLE_TITLE = 'New variable name:';
/** @type {string} */
/// alert - Tells the user that the name they entered is already in use.
Blockly.Msg.VARIABLE_ALREADY_EXISTS = 'A variable named \'%1\' already exists.';
/** @type {string} */
/// alert - Tells the user that the name they entered is already in use for another type.
Blockly.Msg.VARIABLE_ALREADY_EXISTS_FOR_ANOTHER_TYPE = 'A variable named \'%1\' already exists for another type: \'%2\'.';

// Variable deletion.
/** @type {string} */
/// confirm -  Ask the user to confirm their deletion of multiple uses of a variable.
Blockly.Msg.DELETE_VARIABLE_CONFIRMATION = 'Delete %1 uses of the \'%2\' variable?';
/** @type {string} */
/// alert - Tell the user that they can't delete a variable because it's part of the definition of a function.
Blockly.Msg.CANNOT_DELETE_VARIABLE_PROCEDURE = 'Can\'t delete the variable \'%1\' because it\'s part of the definition of the function \'%2\'';
/** @type {string} */
/// dropdown choice - Delete the currently selected variable.
Blockly.Msg.DELETE_VARIABLE = 'Delete the \'%1\' variable';

// Colour Blocks.
/** @type {string} */
/// {{Optional}} url - Information about colour.
Blockly.Msg.COLOUR_PICKER_HELPURL = 'https://en.wikipedia.org/wiki/Color';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Colour#picking-a-colour-from-a-palette https://github.com/google/blockly/wiki/Colour#picking-a-colour-from-a-palette].
Blockly.Msg.COLOUR_PICKER_TOOLTIP = 'Choose a colour from the palette.';
/** @type {string} */
/// {{Optional}} url - A link that displays a random colour each time you visit it.
Blockly.Msg.COLOUR_RANDOM_HELPURL = 'http://randomcolour.com';
/** @type {string} */
/// block text - Title of block that generates a colour at random.
Blockly.Msg.COLOUR_RANDOM_TITLE = 'random colour';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Colour#generating-a-random-colour https://github.com/google/blockly/wiki/Colour#generating-a-random-colour].
Blockly.Msg.COLOUR_RANDOM_TOOLTIP = 'Choose a colour at random.';
/** @type {string} */
/// {{Optional}} url - A link for colour codes with percentages (0-100%) for each component, instead of the more common 0-255, which may be more difficult for beginners.
Blockly.Msg.COLOUR_RGB_HELPURL = 'https://www.december.com/html/spec/colorpercompact.html';
/** @type {string} */
/// block text - Title of block for [https://github.com/google/blockly/wiki/Colour#creating-a-colour-from-red-green-and-blue-components https://github.com/google/blockly/wiki/Colour#creating-a-colour-from-red-green-and-blue-components].
Blockly.Msg.COLOUR_RGB_TITLE = 'colour with';
/** @type {string} */
/// block input text - The amount of red (from 0 to 100) to use when [https://github.com/google/blockly/wiki/Colour#creating-a-colour-from-red-green-and-blue-components https://github.com/google/blockly/wiki/Colour#creating-a-colour-from-red-green-and-blue-components].\n{{Identical|Red}}
Blockly.Msg.COLOUR_RGB_RED = 'red';
/** @type {string} */
/// block input text - The amount of green (from 0 to 100) to use when [https://github.com/google/blockly/wiki/Colour#creating-a-colour-from-red-green-and-blue-components https://github.com/google/blockly/wiki/Colour#creating-a-colour-from-red-green-and-blue-components].
Blockly.Msg.COLOUR_RGB_GREEN = 'green';
/** @type {string} */
/// block input text - The amount of blue (from 0 to 100) to use when [https://github.com/google/blockly/wiki/Colour#creating-a-colour-from-red-green-and-blue-components https://github.com/google/blockly/wiki/Colour#creating-a-colour-from-red-green-and-blue-components].\n{{Identical|Blue}}
Blockly.Msg.COLOUR_RGB_BLUE = 'blue';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Colour#creating-a-colour-from-red-green-and-blue-components https://github.com/google/blockly/wiki/Colour#creating-a-colour-from-red-green-and-blue-components].
Blockly.Msg.COLOUR_RGB_TOOLTIP = 'Create a colour with the specified amount of red, green, and blue. All values must be between 0 and 100.';
/** @type {string} */
/// {{Optional}} url - A useful link that displays blending of two colours.
Blockly.Msg.COLOUR_BLEND_HELPURL = 'https://meyerweb.com/eric/tools/color-blend/#:::rgbp';
/** @type {string} */
/// block text - A verb for blending two shades of paint.
Blockly.Msg.COLOUR_BLEND_TITLE = 'blend';
/** @type {string} */
/// block input text - The first of two colours to [https://github.com/google/blockly/wiki/Colour#blending-colours blend].
Blockly.Msg.COLOUR_BLEND_COLOUR1 = 'colour 1';
/** @type {string} */
/// block input text - The second of two colours to [https://github.com/google/blockly/wiki/Colour#blending-colours blend].
Blockly.Msg.COLOUR_BLEND_COLOUR2 = 'colour 2';
/** @type {string} */
/// block input text - The proportion of the [https://github.com/google/blockly/wiki/Colour#blending-colours blend] containing the first colour; the remaining proportion is of the second colour.  For example, if the first colour is red and the second colour blue, a ratio of 1 would yield pure red, a ratio of .5 would yield purple (equal amounts of red and blue), and a ratio of 0 would yield pure blue.\n{{Identical|Ratio}}
Blockly.Msg.COLOUR_BLEND_RATIO = 'ratio';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Colour#blending-colours https://github.com/google/blockly/wiki/Colour#blending-colours].
Blockly.Msg.COLOUR_BLEND_TOOLTIP = 'Blends two colours together with a given ratio (0.0 - 1.0).';

// Loop Blocks.
/** @type {string} */
/// {{Optional}} url - Describes 'repeat loops' in computer programs; consider using the translation of the page [https://en.wikipedia.org/wiki/Control_flow https://en.wikipedia.org/wiki/Control_flow].
Blockly.Msg.CONTROLS_REPEAT_HELPURL = 'https://en.wikipedia.org/wiki/For_loop';
/** @type {string} */
/// block input text - Title of [https://github.com/google/blockly/wiki/Loops#repeat repeat block].\n\nParameters:\n* %1 - the number of times the body of the loop should be repeated.
Blockly.Msg.CONTROLS_REPEAT_TITLE = 'repeat %1 times';
/** @type {string} */
/// block text - Preceding the blocks in the body of the loop.  See [https://github.com/google/blockly/wiki/Loops https://github.com/google/blockly/wiki/Loops].\n{{Identical|Do}}
Blockly.Msg.CONTROLS_REPEAT_INPUT_DO = 'do';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Loops#repeat https://github.com/google/blockly/wiki/Loops#repeat].
Blockly.Msg.CONTROLS_REPEAT_TOOLTIP = 'Do some statements several times.';
/** @type {string} */
/// {{Optional}} url - Describes 'while loops' in computer programs; consider using the translation of [https://en.wikipedia.org/wiki/While_loop https://en.wikipedia.org/wiki/While_loop], if present, or [https://en.wikipedia.org/wiki/Control_flow https://en.wikipedia.org/wiki/Control_flow].
Blockly.Msg.CONTROLS_WHILEUNTIL_HELPURL = 'https://github.com/google/blockly/wiki/Loops#repeat';
/** @type {string} */
Blockly.Msg.CONTROLS_WHILEUNTIL_INPUT_DO = Blockly.Msg.CONTROLS_REPEAT_INPUT_DO;
/** @type {string} */
/// dropdown - Specifies that a loop should [https://github.com/google/blockly/wiki/Loops#repeat-while repeat while] the following condition is true.
Blockly.Msg.CONTROLS_WHILEUNTIL_OPERATOR_WHILE = 'repeat while';
/** @type {string} */
/// dropdown - Specifies that a loop should [https://github.com/google/blockly/wiki/Loops#repeat-until repeat until] the following condition becomes true.
Blockly.Msg.CONTROLS_WHILEUNTIL_OPERATOR_UNTIL = 'repeat until';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Loops#repeat-while Loops#repeat-while https://github.com/google/blockly/wiki/Loops#repeat-while Loops#repeat-while].
Blockly.Msg.CONTROLS_WHILEUNTIL_TOOLTIP_WHILE = 'While a value is true, then do some statements.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Loops#repeat-until https://github.com/google/blockly/wiki/Loops#repeat-until].
Blockly.Msg.CONTROLS_WHILEUNTIL_TOOLTIP_UNTIL = 'While a value is false, then do some statements.';

/** @type {string} */
/// {{Optional}} url - Describes 'for loops' in computer programs.  Consider using your language's translation of [https://en.wikipedia.org/wiki/For_loop https://en.wikipedia.org/wiki/For_loop], if present.
Blockly.Msg.CONTROLS_FOR_HELPURL = 'https://github.com/google/blockly/wiki/Loops#count-with';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Loops#count-with https://github.com/google/blockly/wiki/Loops#count-with].\n\nParameters:\n* %1 - the name of the loop variable.
Blockly.Msg.CONTROLS_FOR_TOOLTIP = 'Have the variable \'%1\' take on the values from the start number to the end number, counting by the specified interval, and do the specified blocks.';
/** @type {string} */
/// block text - Repeatedly counts a variable (%1)
/// starting with a (usually lower) number in a range (%2),
/// ending with a (usually higher) number in a range (%3), and counting the
/// iterations by a number of steps (%4).  As in
/// [https://github.com/google/blockly/wiki/Loops#count-with
/// https://github.com/google/blockly/wiki/Loops#count-with].
/// [[File:Blockly-count-with.png]]
Blockly.Msg.CONTROLS_FOR_TITLE = 'count with %1 from %2 to %3 by %4';
/** @type {string} */
Blockly.Msg.CONTROLS_FOR_INPUT_DO = Blockly.Msg.CONTROLS_REPEAT_INPUT_DO;

/** @type {string} */
/// {{Optional}} url - Describes 'for-each loops' in computer programs.  Consider using your language's translation of [https://en.wikipedia.org/wiki/Foreach https://en.wikipedia.org/wiki/Foreach] if present.
Blockly.Msg.CONTROLS_FOREACH_HELPURL = 'https://github.com/google/blockly/wiki/Loops#for-each';
/** @type {string} */
/// block text - Title of [https://github.com/google/blockly/wiki/Loops#for-each for each block].
/// Sequentially assigns every item in array %2 to the valiable %1.
Blockly.Msg.CONTROLS_FOREACH_TITLE = 'for each item %1 in list %2';
/** @type {string} */
Blockly.Msg.CONTROLS_FOREACH_INPUT_DO = Blockly.Msg.CONTROLS_REPEAT_INPUT_DO;
/** @type {string} */
/// block text - Description of [https://github.com/google/blockly/wiki/Loops#for-each for each blocks].\n\nParameters:\n* %1 - the name of the loop variable.
Blockly.Msg.CONTROLS_FOREACH_TOOLTIP = 'For each item in a list, set the variable \'%1\' to the item, and then do some statements.';

/** @type {string} */
/// {{Optional}} url - Describes control flow in computer programs.  Consider using your language's translation of [https://en.wikipedia.org/wiki/Control_flow https://en.wikipedia.org/wiki/Control_flow], if it exists.
Blockly.Msg.CONTROLS_FLOW_STATEMENTS_HELPURL = 'https://github.com/google/blockly/wiki/Loops#loop-termination-blocks';
/** @type {string} */
/// dropdown - The current loop should be exited.  See [https://github.com/google/blockly/wiki/Loops#break https://github.com/google/blockly/wiki/Loops#break].
Blockly.Msg.CONTROLS_FLOW_STATEMENTS_OPERATOR_BREAK = 'break out of loop';
/** @type {string} */
/// dropdown - The current iteration of the loop should be ended and the next should begin.  See [https://github.com/google/blockly/wiki/Loops#continue-with-next-iteration https://github.com/google/blockly/wiki/Loops#continue-with-next-iteration].
Blockly.Msg.CONTROLS_FLOW_STATEMENTS_OPERATOR_CONTINUE = 'continue with next iteration of loop';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Loops#break-out-of-loop https://github.com/google/blockly/wiki/Loops#break-out-of-loop].
Blockly.Msg.CONTROLS_FLOW_STATEMENTS_TOOLTIP_BREAK = 'Break out of the containing loop.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Loops#continue-with-next-iteration https://github.com/google/blockly/wiki/Loops#continue-with-next-iteration].
Blockly.Msg.CONTROLS_FLOW_STATEMENTS_TOOLTIP_CONTINUE = 'Skip the rest of this loop, and continue with the next iteration.';
/** @type {string} */
/// warning - The user has tried placing a block outside of a loop (for each, while, repeat, etc.), but this type of block may only be used within a loop.  See [https://github.com/google/blockly/wiki/Loops#loop-termination-blocks https://github.com/google/blockly/wiki/Loops#loop-termination-blocks].
Blockly.Msg.CONTROLS_FLOW_STATEMENTS_WARNING = 'Warning: This block may only be used within a loop.';

// Logic Blocks.
/** @type {string} */
/// {{Optional}} url - Describes conditional statements (if-then-else) in computer programs.  Consider using your language's translation of [https://en.wikipedia.org/wiki/If_else https://en.wikipedia.org/wiki/If_else], if present.
Blockly.Msg.CONTROLS_IF_HELPURL = 'https://github.com/google/blockly/wiki/IfElse';
/** @type {string} */
/// tooltip - Describes [https://github.com/google/blockly/wiki/IfElse#if-blocks 'if' blocks].  Consider using your language's translation of [https://en.wikipedia.org/wiki/If_statement https://en.wikipedia.org/wiki/If_statement], if present.
Blockly.Msg.CONTROLS_IF_TOOLTIP_1 = 'If a value is true, then do some statements.';
/** @type {string} */
/// tooltip - Describes [https://github.com/google/blockly/wiki/IfElse#if-else-blocks if-else blocks].  Consider using your language's translation of [https://en.wikipedia.org/wiki/If_statement https://en.wikipedia.org/wiki/If_statement], if present.
Blockly.Msg.CONTROLS_IF_TOOLTIP_2 = 'If a value is true, then do the first block of statements. Otherwise, do the second block of statements.';
/** @type {string} */
/// tooltip - Describes [https://github.com/google/blockly/wiki/IfElse#if-else-if-blocks if-else-if blocks].  Consider using your language's translation of [https://en.wikipedia.org/wiki/If_statement https://en.wikipedia.org/wiki/If_statement], if present.
Blockly.Msg.CONTROLS_IF_TOOLTIP_3 = 'If the first value is true, then do the first block of statements. Otherwise, if the second value is true, do the second block of statements.';
/** @type {string} */
/// tooltip - Describes [https://github.com/google/blockly/wiki/IfElse#if-else-if-else-blocks if-else-if-else blocks].  Consider using your language's translation of [https://en.wikipedia.org/wiki/If_statement https://en.wikipedia.org/wiki/If_statement], if present.
Blockly.Msg.CONTROLS_IF_TOOLTIP_4 = 'If the first value is true, then do the first block of statements. Otherwise, if the second value is true, do the second block of statements. If none of the values are true, do the last block of statements.';
/** @type {string} */
/// block text - See [https://github.com/google/blockly/wiki/IfElse https://github.com/google/blockly/wiki/IfElse].
/// It is recommended, but not essential, that this have text in common with the translation of 'else if'\n{{Identical|If}}
Blockly.Msg.CONTROLS_IF_MSG_IF = 'if';
/** @type {string} */
/// block text - See [https://github.com/google/blockly/wiki/IfElse https://github.com/google/blockly/wiki/IfElse].  The English words 'otherwise if' would probably be clearer than 'else if', but the latter is used because it is traditional and shorter.
Blockly.Msg.CONTROLS_IF_MSG_ELSEIF = 'else if';
/** @type {string} */
/// block text - See [https://github.com/google/blockly/wiki/IfElse https://github.com/google/blockly/wiki/IfElse].  The English word 'otherwise' would probably be superior to 'else', but the latter is used because it is traditional and shorter.
Blockly.Msg.CONTROLS_IF_MSG_ELSE = 'else';
/** @type {string} */
Blockly.Msg.CONTROLS_IF_MSG_THEN = Blockly.Msg.CONTROLS_REPEAT_INPUT_DO;
/** @type {string} */
Blockly.Msg.CONTROLS_IF_IF_TITLE_IF = Blockly.Msg.CONTROLS_IF_MSG_IF;
/** @type {string} */
/// tooltip - Describes [https://github.com/google/blockly/wiki/IfElse#block-modification if block modification].
Blockly.Msg.CONTROLS_IF_IF_TOOLTIP = 'Add, remove, or reorder sections to reconfigure this if block.';
/** @type {string} */
Blockly.Msg.CONTROLS_IF_ELSEIF_TITLE_ELSEIF = Blockly.Msg.CONTROLS_IF_MSG_ELSEIF;
/** @type {string} */
/// tooltip - Describes the 'else if' subblock during [https://github.com/google/blockly/wiki/IfElse#block-modification if block modification].
Blockly.Msg.CONTROLS_IF_ELSEIF_TOOLTIP = 'Add a condition to the if block.';
/** @type {string} */
Blockly.Msg.CONTROLS_IF_ELSE_TITLE_ELSE = Blockly.Msg.CONTROLS_IF_MSG_ELSE;
/** @type {string} */
/// tooltip - Describes the 'else' subblock during [https://github.com/google/blockly/wiki/IfElse#block-modification if block modification].
Blockly.Msg.CONTROLS_IF_ELSE_TOOLTIP = 'Add a final, catch-all condition to the if block.';

/** @type {string} */
/// button text - Text on a button inside a dialogue window, which will accept or acknowledge the contents of the dialogue when pressed.\n{{Identical|OK}}
Blockly.Msg.IOS_OK = 'OK';
/** @type {string} */
/// button text - Text on a button inside a dialogue window, which will close or cancel the dialogue when pressed.\n{{Identical|Cancel}}
Blockly.Msg.IOS_CANCEL = 'Cancel';
/** @type {string} */
/// alert - Title text for an error dialogue.\n{{Identical|Error}}
Blockly.Msg.IOS_ERROR = 'Error';
/** @type {string} */
/// header text - Title of a section that displays a list of parameters (aka. 'inputs') that have been defined for a procedure. This is used inside a dialogue window to configure a procedure.\n{{Identical|Input}}
Blockly.Msg.IOS_PROCEDURES_INPUTS = 'INPUTS';
/** @type {string} */
/// button text - Text on a button which will add a parameter (aka. 'input') to a procedure. This is used inside a dialogue window to configure a procedure. NOTE: The '+' should be preserved at the beginning of the text.
Blockly.Msg.IOS_PROCEDURES_ADD_INPUT = '+ Add Input';
/** @type {string} */
/// option text - Text describing an option to allow statements to be added within a procedure. This is used inside a dialogue window to configure a procedure.
Blockly.Msg.IOS_PROCEDURES_ALLOW_STATEMENTS = 'Allow statements';
/** @type {string} */
/// alert - Error message when duplicate parameters (aka. 'inputs') have been defined on a procedure. This is used inside a dialogue window to configure procedure parameters.
Blockly.Msg.IOS_PROCEDURES_DUPLICATE_INPUTS_ERROR = 'This function has duplicate inputs.';
/** @type {string} */
/// button text - Text on a button which will open a variable creation dialogue when pressed. NOTE: The '+' should be preserved at the beginning of the text.
Blockly.Msg.IOS_VARIABLES_ADD_VARIABLE = '+ Add Variable';
/** @type {string} */
/// button text - Text on a button inside a variable creation dialogue, which will add a variable when pressed.\n{{Identical|Add}}
Blockly.Msg.IOS_VARIABLES_ADD_BUTTON = 'Add';
/** @type {string} */
/// button text - Text on a button inside a variable rename dialogue, which will rename a variable when pressed.\n{{Identical|Rename}}
Blockly.Msg.IOS_VARIABLES_RENAME_BUTTON = 'Rename';
/** @type {string} */
/// button text - Text on a button inside a variable deletion dialogue, which will delete a variable when pressed.\n{{Identical|Delete}}
Blockly.Msg.IOS_VARIABLES_DELETE_BUTTON = 'Delete';
/** @type {string} */
/// placeholder text - Placeholder text used inside a text input, where a variable name should be entered.
Blockly.Msg.IOS_VARIABLES_VARIABLE_NAME = 'Variable name';
/** @type {string} */
/// alert - Error message that is displayed when the user attempts to create a variable without a name.
Blockly.Msg.IOS_VARIABLES_EMPTY_NAME_ERROR = 'You can\'t use an empty variable name.';

/** @type {string} */
/// {{Optional}} url - Information about comparisons.
Blockly.Msg.LOGIC_COMPARE_HELPURL = 'https://en.wikipedia.org/wiki/Inequality_(mathematics)';
/** @type {string} */
/// tooltip - Describes the equals (=) block.
Blockly.Msg.LOGIC_COMPARE_TOOLTIP_EQ = 'Return true if both inputs equal each other.';
/** @type {string} */
/// tooltip - Describes the not equals (≠) block.
Blockly.Msg.LOGIC_COMPARE_TOOLTIP_NEQ = 'Return true if both inputs are not equal to each other.';
/** @type {string} */
/// tooltip - Describes the less than (<) block.
Blockly.Msg.LOGIC_COMPARE_TOOLTIP_LT = 'Return true if the first input is smaller than the second input.';
/** @type {string} */
/// tooltip - Describes the less than or equals (≤) block.
Blockly.Msg.LOGIC_COMPARE_TOOLTIP_LTE = 'Return true if the first input is smaller than or equal to the second input.';
/** @type {string} */
/// tooltip - Describes the greater than (>) block.
Blockly.Msg.LOGIC_COMPARE_TOOLTIP_GT = 'Return true if the first input is greater than the second input.';
/** @type {string} */
/// tooltip - Describes the greater than or equals (≥) block.
Blockly.Msg.LOGIC_COMPARE_TOOLTIP_GTE = 'Return true if the first input is greater than or equal to the second input.';

/** @type {string} */
/// {{Optional}} url - Information about the Boolean conjunction ('and') and disjunction ('or') operators.  Consider using the translation of [https://en.wikipedia.org/wiki/Boolean_logic https://en.wikipedia.org/wiki/Boolean_logic], if it exists in your language.
Blockly.Msg.LOGIC_OPERATION_HELPURL = 'https://github.com/google/blockly/wiki/Logic#logical-operations';
/** @type {string} */
/// tooltip - See [https://en.wikipedia.org/wiki/Logical_conjunction https://en.wikipedia.org/wiki/Logical_conjunction].
Blockly.Msg.LOGIC_OPERATION_TOOLTIP_AND = 'Return true if both inputs are true.';
/** @type {string} */
/// block text - See [https://en.wikipedia.org/wiki/Logical_conjunction https://en.wikipedia.org/wiki/Logical_conjunction].\n{{Identical|And}}
Blockly.Msg.LOGIC_OPERATION_AND = 'and';
/** @type {string} */
/// block text - See [https://en.wikipedia.org/wiki/Disjunction https://en.wikipedia.org/wiki/Disjunction].
Blockly.Msg.LOGIC_OPERATION_TOOLTIP_OR = 'Return true if at least one of the inputs is true.';
/** @type {string} */
/// block text - See [https://en.wikipedia.org/wiki/Disjunction https://en.wikipedia.org/wiki/Disjunction].\n{{Identical|Or}}
Blockly.Msg.LOGIC_OPERATION_OR = 'or';

/** @type {string} */
/// {{Optional}} url - Information about logical negation.  The translation of [https://en.wikipedia.org/wiki/Logical_negation https://en.wikipedia.org/wiki/Logical_negation] is recommended if it exists in the target language.
Blockly.Msg.LOGIC_NEGATE_HELPURL = 'https://github.com/google/blockly/wiki/Logic#not';
/** @type {string} */
/// block text - This is a unary operator that returns ''false'' when the input is ''true'', and ''true'' when the input is ''false''.
/// \n\nParameters:\n* %1 - the input (which should be either the value 'true' or 'false')
Blockly.Msg.LOGIC_NEGATE_TITLE = 'not %1';
/** @type {string} */
/// tooltip - See [https://en.wikipedia.org/wiki/Logical_negation https://en.wikipedia.org/wiki/Logical_negation].
Blockly.Msg.LOGIC_NEGATE_TOOLTIP = 'Returns true if the input is false. Returns false if the input is true.';

/** @type {string} */
/// {{Optional}} url - Information about the logic values ''true'' and ''false''.  Consider using the translation of [https://en.wikipedia.org/wiki/Truth_value https://en.wikipedia.org/wiki/Truth_value] if it exists in your language.
Blockly.Msg.LOGIC_BOOLEAN_HELPURL = 'https://github.com/google/blockly/wiki/Logic#values';
/** @type {string} */
/// block text - The word for the [https://en.wikipedia.org/wiki/Truth_value logical value] ''true''.\n{{Identical|True}}
Blockly.Msg.LOGIC_BOOLEAN_TRUE = 'true';
/** @type {string} */
/// block text - The word for the [https://en.wikipedia.org/wiki/Truth_value logical value] ''false''.\n{{Identical|False}}
Blockly.Msg.LOGIC_BOOLEAN_FALSE = 'false';
/** @type {string} */
/// tooltip - Indicates that the block returns either of the two possible [https://en.wikipedia.org/wiki/Truth_value logical values].
Blockly.Msg.LOGIC_BOOLEAN_TOOLTIP = 'Returns either true or false.';

/** @type {string} */
/// {{Optional}} url - Provide a link to the translation of [https://en.wikipedia.org/wiki/Nullable_type https://en.wikipedia.org/wiki/Nullable_type], if it exists in your language; otherwise, do not worry about translating this advanced concept.
Blockly.Msg.LOGIC_NULL_HELPURL = 'https://en.wikipedia.org/wiki/Nullable_type';
/** @type {string} */
/// block text - In computer languages, ''null'' is a special value that indicates that no value has been set.  You may use your language's word for 'nothing' or 'invalid'.\n{{Identical|Null}}
Blockly.Msg.LOGIC_NULL = 'null';
/** @type {string} */
/// tooltip - This should use the word from the previous message.
Blockly.Msg.LOGIC_NULL_TOOLTIP = 'Returns null.';

/** @type {string} */
/// {{Optional}} url - Describes the programming language operator known as the ''ternary'' or ''conditional'' operator.  It is recommended that you use the translation of [https://en.wikipedia.org/wiki/%3F: https://en.wikipedia.org/wiki/%3F:] if it exists.
Blockly.Msg.LOGIC_TERNARY_HELPURL = 'https://en.wikipedia.org/wiki/%3F:';
/** @type {string} */
/// block input text - Label for the input whose value determines which of the other two inputs is returned.  In some programming languages, this is called a ''''predicate''''.
Blockly.Msg.LOGIC_TERNARY_CONDITION = 'test';
/** @type {string} */
/// block input text - Indicates that the following input should be returned (used as output) if the test input is true.  Remember to try to keep block text terse (short).
Blockly.Msg.LOGIC_TERNARY_IF_TRUE = 'if true';
/** @type {string} */
/// block input text - Indicates that the following input should be returned (used as output) if the test input is false.
Blockly.Msg.LOGIC_TERNARY_IF_FALSE = 'if false';
/** @type {string} */
/// tooltip - See [https://en.wikipedia.org/wiki/%3F: https://en.wikipedia.org/wiki/%3F:].
Blockly.Msg.LOGIC_TERNARY_TOOLTIP = 'Check the condition in \'test\'. If the condition is true, returns the \'if true\' value; otherwise returns the \'if false\' value.';

// Math Blocks.
/** @type {string} */
/// {{Optional}} url - Information about (real) numbers.
Blockly.Msg.MATH_NUMBER_HELPURL = 'https://en.wikipedia.org/wiki/Number';
/** @type {string} */
/// tooltip - Any positive or negative number, not necessarily an integer.
Blockly.Msg.MATH_NUMBER_TOOLTIP = 'A number.';

/** @type {string} */
/// {{Optional}} math - The symbol for the binary operation addition.
Blockly.Msg.MATH_ADDITION_SYMBOL = '+';
/** @type {string} */
/// {{Optional}} math - The symbol for the binary operation indicating that the right operand should be
/// subtracted from the left operand.
Blockly.Msg.MATH_SUBTRACTION_SYMBOL = '-';
/** @type {string} */
/// {{Optional}} math - The binary operation indicating that the left operand should be divided by
/// the right operand.
Blockly.Msg.MATH_DIVISION_SYMBOL = '÷';
/** @type {string} */
/// {{Optional}} math - The symbol for the binary operation multiplication.
Blockly.Msg.MATH_MULTIPLICATION_SYMBOL = '×';
/** @type {string} */
/// {{Optional}} math - The symbol for the binary operation exponentiation.  Specifically, if the
/// value of the left operand is L and the value of the right operand (the exponent) is
/// R, multiply L by itself R times.  (Fractional and negative exponents are also legal.)
Blockly.Msg.MATH_POWER_SYMBOL = '^';

/** @type {string} */
/// math - The short name of the trigonometric function
/// [https://en.wikipedia.org/wiki/Trigonometric_functions#Sine.2C_cosine_and_tangent sine].
Blockly.Msg.MATH_TRIG_SIN = 'sin';
/** @type {string} */
/// math - The short name of the trigonometric function
/// [https://en.wikipedia.org/wiki/Trigonometric_functions#Sine.2C_cosine_and_tangent cosine].
Blockly.Msg.MATH_TRIG_COS = 'cos';
/** @type {string} */
/// math - The short name of the trigonometric function
/// [https://en.wikipedia.org/wiki/Trigonometric_functions#Sine.2C_cosine_and_tangent tangent].
Blockly.Msg.MATH_TRIG_TAN = 'tan';
/** @type {string} */
/// math - The short name of the ''inverse of'' the trigonometric function
/// [https://en.wikipedia.org/wiki/Trigonometric_functions#Sine.2C_cosine_and_tangent sine].
Blockly.Msg.MATH_TRIG_ASIN = 'asin';
/** @type {string} */
/// math - The short name of the ''inverse of'' the trigonometric function
/// [https://en.wikipedia.org/wiki/Trigonometric_functions#Sine.2C_cosine_and_tangent cosine].
Blockly.Msg.MATH_TRIG_ACOS = 'acos';
/** @type {string} */
/// math - The short name of the ''inverse of'' the trigonometric function
/// [https://en.wikipedia.org/wiki/Trigonometric_functions#Sine.2C_cosine_and_tangent tangent].
Blockly.Msg.MATH_TRIG_ATAN = 'atan';

/** @type {string} */
/// {{Optional}} url - Information about addition, subtraction, multiplication, division, and exponentiation.
Blockly.Msg.MATH_ARITHMETIC_HELPURL = 'https://en.wikipedia.org/wiki/Arithmetic';
/** @type {string} */
/// tooltip - See [https://en.wikipedia.org/wiki/Addition https://en.wikipedia.org/wiki/Addition].
Blockly.Msg.MATH_ARITHMETIC_TOOLTIP_ADD = 'Return the sum of the two numbers.';
/** @type {string} */
/// tooltip - See [https://en.wikipedia.org/wiki/Subtraction https://en.wikipedia.org/wiki/Subtraction].
Blockly.Msg.MATH_ARITHMETIC_TOOLTIP_MINUS = 'Return the difference of the two numbers.';
/** @type {string} */
/// tooltip - See [https://en.wikipedia.org/wiki/Multiplication https://en.wikipedia.org/wiki/Multiplication].
Blockly.Msg.MATH_ARITHMETIC_TOOLTIP_MULTIPLY = 'Return the product of the two numbers.';
/** @type {string} */
/// tooltip - See [https://en.wikipedia.org/wiki/Division_(mathematics) https://en.wikipedia.org/wiki/Division_(mathematics)].
Blockly.Msg.MATH_ARITHMETIC_TOOLTIP_DIVIDE = 'Return the quotient of the two numbers.';
/** @type {string} */
/// tooltip - See [https://en.wikipedia.org/wiki/Exponentiation https://en.wikipedia.org/wiki/Exponentiation].
Blockly.Msg.MATH_ARITHMETIC_TOOLTIP_POWER = 'Return the first number raised to the power of the second number.';

/** @type {string} */
/// {{Optional}} url - Information about the square root operation.
Blockly.Msg.MATH_SINGLE_HELPURL = 'https://en.wikipedia.org/wiki/Square_root';
/** @type {string} */
/// dropdown - This computes the positive [https://en.wikipedia.org/wiki/Square_root square root] of its input.  For example, the square root of 16 is 4.
Blockly.Msg.MATH_SINGLE_OP_ROOT = 'square root';
/** @type {string} */
/// tooltip - Please use the same term as in the previous message.
Blockly.Msg.MATH_SINGLE_TOOLTIP_ROOT = 'Return the square root of a number.';
/** @type {string} */
/// dropdown - This leaves positive numeric inputs changed and inverts negative inputs.  For example, the absolute value of 5 is 5; the absolute value of -5 is also 5.  For more information, see [https://en.wikipedia.org/wiki/Absolute_value https://en.wikipedia.org/wiki/Absolute_value].
Blockly.Msg.MATH_SINGLE_OP_ABSOLUTE = 'absolute';
/** @type {string} */
/// tooltip - Please use the same term as in the previous message.
Blockly.Msg.MATH_SINGLE_TOOLTIP_ABS = 'Return the absolute value of a number.';

/** @type {string} */
/// tooltip - Calculates '''0-n''', where '''n''' is the single numeric input.
Blockly.Msg.MATH_SINGLE_TOOLTIP_NEG = 'Return the negation of a number.';
/** @type {string} */
/// tooltip - Calculates the [https://en.wikipedia.org/wiki/Natural_logarithm|natural logarithm] of its single numeric input.
Blockly.Msg.MATH_SINGLE_TOOLTIP_LN = 'Return the natural logarithm of a number.';
/** @type {string} */
/// tooltip - Calculates the [https://en.wikipedia.org/wiki/Common_logarithm common logarithm] of its single numeric input.
Blockly.Msg.MATH_SINGLE_TOOLTIP_LOG10 = 'Return the base 10 logarithm of a number.';
/** @type {string} */
/// tooltip - Multiplies [https://en.wikipedia.org/wiki/E_(mathematical_constant) e] by itself n times, where n is the single numeric input.
Blockly.Msg.MATH_SINGLE_TOOLTIP_EXP = 'Return e to the power of a number.';
/** @type {string} */
/// tooltip - Multiplies 10 by itself n times, where n is the single numeric input.
Blockly.Msg.MATH_SINGLE_TOOLTIP_POW10 = 'Return 10 to the power of a number.';

/** @type {string} */
/// {{Optional}} url - Information about the trigonometric functions sine, cosine, tangent, and their inverses (ideally using degrees, not radians).
Blockly.Msg.MATH_TRIG_HELPURL = 'https://en.wikipedia.org/wiki/Trigonometric_functions';
/** @type {string} */
/// tooltip - Return the [https://en.wikipedia.org/wiki/Trigonometric_functions#Sine.2C_cosine_and_tangent sine] of an [https://en.wikipedia.org/wiki/Degree_(angle) angle in degrees], not radians.
Blockly.Msg.MATH_TRIG_TOOLTIP_SIN = 'Return the sine of a degree (not radian).';
/** @type {string} */
/// tooltip - Return the [https://en.wikipedia.org/wiki/Trigonometric_functions#Sine.2C_cosine_and_tangent cosine] of an [https://en.wikipedia.org/wiki/Degree_(angle) angle in degrees], not radians.
Blockly.Msg.MATH_TRIG_TOOLTIP_COS = 'Return the cosine of a degree (not radian).';
/** @type {string} */
/// tooltip - Return the [https://en.wikipedia.org/wiki/Trigonometric_functions#Sine.2C_cosine_and_tangent tangent] of an [https://en.wikipedia.org/wiki/Degree_(angle) angle in degrees], not radians.
Blockly.Msg.MATH_TRIG_TOOLTIP_TAN = 'Return the tangent of a degree (not radian).';
/** @type {string} */
/// tooltip - The [https://en.wikipedia.org/wiki/Inverse_trigonometric_functions inverse] of the [https://en.wikipedia.org/wiki/Cosine#Sine.2C_cosine_and_tangent sine function], using [https://en.wikipedia.org/wiki/Degree_(angle) degrees], not radians.
Blockly.Msg.MATH_TRIG_TOOLTIP_ASIN = 'Return the arcsine of a number.';
/** @type {string} */
/// tooltip - The [https://en.wikipedia.org/wiki/Inverse_trigonometric_functions inverse] of the [https://en.wikipedia.org/wiki/Cosine#Sine.2C_cosine_and_tangent cosine] function, using [https://en.wikipedia.org/wiki/Degree_(angle) degrees], not radians.
Blockly.Msg.MATH_TRIG_TOOLTIP_ACOS = 'Return the arccosine of a number.';
/** @type {string} */
/// tooltip - The [https://en.wikipedia.org/wiki/Inverse_trigonometric_functions inverse] of the [https://en.wikipedia.org/wiki/Cosine#Sine.2C_cosine_and_tangent tangent] function, using [https://en.wikipedia.org/wiki/Degree_(angle) degrees], not radians.
Blockly.Msg.MATH_TRIG_TOOLTIP_ATAN = 'Return the arctangent of a number.';

/** @type {string} */
/// {{Optional}} url - Information about the mathematical constants Pi (π), e, the golden ratio (φ), √ 2, √ 1/2, and infinity (∞).
Blockly.Msg.MATH_CONSTANT_HELPURL = 'https://en.wikipedia.org/wiki/Mathematical_constant';
/** @type {string} */
/// tooltip - Provides the specified [https://en.wikipedia.org/wiki/Mathematical_constant mathematical constant].
Blockly.Msg.MATH_CONSTANT_TOOLTIP = 'Return one of the common constants: π (3.141…), e (2.718…), φ (1.618…), sqrt(2) (1.414…), sqrt(½) (0.707…), or ∞ (infinity).';
/** @type {string} */
/// dropdown - A number is '''even''' if it is a multiple of 2.  For example, 4 is even (yielding true), but 3 is not (false).
Blockly.Msg.MATH_IS_EVEN = 'is even';
/** @type {string} */
/// dropdown - A number is '''odd''' if it is not a multiple of 2.  For example, 3 is odd (yielding true), but 4 is not (false).  The opposite of 'odd' is 'even'.
Blockly.Msg.MATH_IS_ODD = 'is odd';
/** @type {string} */
/// dropdown - A number is [https://en.wikipedia.org/wiki/Prime prime] if it cannot be evenly divided by any positive integers except for 1 and itself.  For example, 5 is prime, but 6 is not because 2 × 3 = 6.
Blockly.Msg.MATH_IS_PRIME = 'is prime';
/** @type {string} */
/// dropdown - A number is '''whole''' if it is an [https://en.wikipedia.org/wiki/Integer integer].  For example, 5 is whole, but 5.1 is not.
Blockly.Msg.MATH_IS_WHOLE = 'is whole';
/** @type {string} */
/// dropdown - A number is '''positive''' if it is greater than 0.  (0 is neither negative nor positive.)
Blockly.Msg.MATH_IS_POSITIVE = 'is positive';
/** @type {string} */
/// dropdown - A number is '''negative''' if it is less than 0.  (0 is neither negative nor positive.)
Blockly.Msg.MATH_IS_NEGATIVE = 'is negative';
/** @type {string} */
/// dropdown - A number x is divisible by y if y goes into x evenly.  For example, 10 is divisible by 5, but 10 is not divisible by 3.
Blockly.Msg.MATH_IS_DIVISIBLE_BY = 'is divisible by';
/** @type {string} */
/// tooltip - This block lets the user specify via a dropdown menu whether to check if the numeric input is even, odd, prime, whole, positive, negative, or divisible by a given value.
Blockly.Msg.MATH_IS_TOOLTIP = 'Check if a number is an even, odd, prime, whole, positive, negative, or if it is divisible by certain number. Returns true or false.';

/** @type {string} */
/// {{Optional}} url - Information about incrementing (increasing the value of) a variable.
/// For other languages, just use the translation of the Wikipedia page about
/// addition ([https://en.wikipedia.org/wiki/Addition https://en.wikipedia.org/wiki/Addition]).
Blockly.Msg.MATH_CHANGE_HELPURL = 'https://en.wikipedia.org/wiki/Programming_idiom#Incrementing_a_counter';
/** @type {string} */
/// - As in: ''change'' [the value of variable] ''item'' ''by'' 1 (e.g., if the variable named 'item' had the value 5, change it to 6).
/// %1 is a variable name.
/// %2 is the amount of change.
Blockly.Msg.MATH_CHANGE_TITLE = 'change %1 by %2';
/** @type {string} */
Blockly.Msg.MATH_CHANGE_TITLE_ITEM = Blockly.Msg.VARIABLES_DEFAULT_NAME;
/** @type {string} */
/// tooltip - This updates the value of the variable by adding to it the following numeric input.\n\nParameters:\n* %1 - the name of the variable whose value should be increased.
Blockly.Msg.MATH_CHANGE_TOOLTIP = 'Add a number to variable \'%1\'.';

/** @type {string} */
/// {{Optional}} url - Information about how numbers are rounded to the nearest integer
Blockly.Msg.MATH_ROUND_HELPURL = 'https://en.wikipedia.org/wiki/Rounding';
/** @type {string} */
/// tooltip - See [https://en.wikipedia.org/wiki/Rounding https://en.wikipedia.org/wiki/Rounding].
Blockly.Msg.MATH_ROUND_TOOLTIP = 'Round a number up or down.';
/** @type {string} */
/// dropdown - This rounds its input to the nearest whole number.  For example, 3.4 is rounded to 3.
Blockly.Msg.MATH_ROUND_OPERATOR_ROUND = 'round';
/** @type {string} */
/// dropdown - This rounds its input up to the nearest whole number.  For example, if the input was 2.2, the result would be 3.
Blockly.Msg.MATH_ROUND_OPERATOR_ROUNDUP = 'round up';
/** @type {string} */
/// dropdown - This rounds its input down to the nearest whole number.  For example, if the input was 3.8, the result would be 3.
Blockly.Msg.MATH_ROUND_OPERATOR_ROUNDDOWN = 'round down';

/** @type {string} */
/// {{Optional}} url - Information about applying a function to a list of numbers.  (We were unable to find such information in English.  Feel free to skip this and any other URLs that are difficult.)
Blockly.Msg.MATH_ONLIST_HELPURL = '';
/** @type {string} */
/// dropdown - This computes the sum of the numeric elements in the list.  For example, the sum of the list {1, 4} is 5.
Blockly.Msg.MATH_ONLIST_OPERATOR_SUM = 'sum of list';
/** @type {string} */
/// tooltip - Please use the same term for 'sum' as in the previous message.
Blockly.Msg.MATH_ONLIST_TOOLTIP_SUM = 'Return the sum of all the numbers in the list.';
/** @type {string} */
/// dropdown - This finds the smallest (minimum) number in a list.  For example, the smallest number in the list [-5, 0, 3] is -5.
Blockly.Msg.MATH_ONLIST_OPERATOR_MIN = 'min of list';
/** @type {string} */
/// tooltip - Please use the same term for 'min' or 'minimum' as in the previous message.
Blockly.Msg.MATH_ONLIST_TOOLTIP_MIN = 'Return the smallest number in the list.';
/** @type {string} */
/// dropdown - This finds the largest (maximum) number in a list.  For example, the largest number in the list [-5, 0, 3] is 3.
Blockly.Msg.MATH_ONLIST_OPERATOR_MAX = 'max of list';
/** @type {string} */
/// tooltip
Blockly.Msg.MATH_ONLIST_TOOLTIP_MAX = 'Return the largest number in the list.';
/** @type {string} */
/// dropdown - This adds up all of the numbers in a list and divides the sum by the number of elements in the list.  For example, the [https://en.wikipedia.org/wiki/Arithmetic_mean average] of the list [1, 2, 3, 4] is 2.5 (10/4).
Blockly.Msg.MATH_ONLIST_OPERATOR_AVERAGE = 'average of list';
/** @type {string} */
/// tooltip - See [https://en.wikipedia.org/wiki/Arithmetic_mean https://en.wikipedia.org/wiki/Arithmetic_mean] for more informatin.
Blockly.Msg.MATH_ONLIST_TOOLTIP_AVERAGE = 'Return the average (arithmetic mean) of the numeric values in the list.';
/** @type {string} */
/// dropdown - This finds the [https://en.wikipedia.org/wiki/Median median] of the numeric values in a list.  For example, the median of the list {1, 2, 7, 12, 13} is 7.
Blockly.Msg.MATH_ONLIST_OPERATOR_MEDIAN = 'median of list';
/** @type {string} */
/// tooltip - See [https://en.wikipedia.org/wiki/Median median https://en.wikipedia.org/wiki/Median median] for more information.
Blockly.Msg.MATH_ONLIST_TOOLTIP_MEDIAN = 'Return the median number in the list.';
/** @type {string} */
/// dropdown - This finds the most common numbers ([https://en.wikipedia.org/wiki/Mode_(statistics) modes]) in a list.  For example, the modes of the list {1, 3, 9, 3, 9}  are {3, 9}.
Blockly.Msg.MATH_ONLIST_OPERATOR_MODE = 'modes of list';
/** @type {string} */
/// tooltip - See [https://en.wikipedia.org/wiki/Mode_(statistics) https://en.wikipedia.org/wiki/Mode_(statistics)] for more information.
Blockly.Msg.MATH_ONLIST_TOOLTIP_MODE = 'Return a list of the most common item(s) in the list.';
/** @type {string} */
/// dropdown - This finds the [https://en.wikipedia.org/wiki/Standard_deviation standard deviation] of the numeric values in a list.
Blockly.Msg.MATH_ONLIST_OPERATOR_STD_DEV = 'standard deviation of list';
/** @type {string} */
/// tooltip - See [https://en.wikipedia.org/wiki/Standard_deviation https://en.wikipedia.org/wiki/Standard_deviation] for more information.
Blockly.Msg.MATH_ONLIST_TOOLTIP_STD_DEV = 'Return the standard deviation of the list.';
/** @type {string} */
/// dropdown - This choose an element at random from a list.  Each element is chosen with equal probability.
Blockly.Msg.MATH_ONLIST_OPERATOR_RANDOM = 'random item of list';
/** @type {string} */
/// tooltip - Please use same term for 'random' as in previous entry.
Blockly.Msg.MATH_ONLIST_TOOLTIP_RANDOM = 'Return a random element from the list.';

/** @type {string} */
/// {{Optional}} url - information about the modulo (remainder) operation.
Blockly.Msg.MATH_MODULO_HELPURL = 'https://en.wikipedia.org/wiki/Modulo_operation';
/** @type {string} */
/// block text - Title of block providing the remainder when dividing the first numerical input by the second.  For example, the remainder of 10 divided by 3 is 1.\n\nParameters:\n* %1 - the dividend (10, in our example)\n* %2 - the divisor (3 in our example).
Blockly.Msg.MATH_MODULO_TITLE = 'remainder of %1 ÷ %2';
/** @type {string} */
/// tooltip - For example, the remainder of 10 divided by 3 is 1.
Blockly.Msg.MATH_MODULO_TOOLTIP = 'Return the remainder from dividing the two numbers.';

/** @type {string} */
/// {{Optional}} url - Information about constraining a numeric value to be in a specific range.  (The English URL is not ideal.  Recall that translating URLs is the lowest priority.)
Blockly.Msg.MATH_CONSTRAIN_HELPURL = 'https://en.wikipedia.org/wiki/Clamping_(graphics)';
/** @type {string} */
/// block text - The title of the block that '''constrain'''s (forces) a number to be in a given range.
///For example, if the number 150 is constrained to be between 5 and 100, the result will be 100.
///\n\nParameters:\n* %1 - the value to constrain (e.g., 150)\n* %2 - the minimum value (e.g., 5)\n* %3 - the maximum value (e.g., 100).
Blockly.Msg.MATH_CONSTRAIN_TITLE = 'constrain %1 low %2 high %3';
/** @type {string} */
/// tooltip - This compares a number ''x'' to a low value ''L'' and a high value ''H''.  If ''x'' is less then ''L'', the result is ''L''.  If ''x'' is greater than ''H'', the result is ''H''.  Otherwise, the result is ''x''.
Blockly.Msg.MATH_CONSTRAIN_TOOLTIP = 'Constrain a number to be between the specified limits (inclusive).';

/** @type {string} */
/// {{Optional}} url - Information about how computers generate random numbers.
Blockly.Msg.MATH_RANDOM_INT_HELPURL = 'https://en.wikipedia.org/wiki/Random_number_generation';
/** @type {string} */
/// block text - The title of the block that generates a random integer (whole number) in the specified range.  For example, if the range is from 5 to 7, this returns 5, 6, or 7 with equal likelihood. %1 is a placeholder for the lower number, %2 is the placeholder for the larger number.
Blockly.Msg.MATH_RANDOM_INT_TITLE = 'random integer from %1 to %2';
/** @type {string} */
/// tooltip - Return a random integer between two values specified as inputs.  For example, if one input was 7 and another 9, any of the numbers 7, 8, or 9 could be produced.
Blockly.Msg.MATH_RANDOM_INT_TOOLTIP = 'Return a random integer between the two specified limits, inclusive.';

/** @type {string} */
/// {{Optional}} url - Information about how computers generate random numbers (specifically, numbers in the range from 0 to just below 1).
Blockly.Msg.MATH_RANDOM_FLOAT_HELPURL = 'https://en.wikipedia.org/wiki/Random_number_generation';
/** @type {string} */
/// block text - The title of the block that generates a random number greater than or equal to 0 and less than 1.
Blockly.Msg.MATH_RANDOM_FLOAT_TITLE_RANDOM = 'random fraction';
/** @type {string} */
/// tooltip - Return a random fraction between 0 and 1.  The value may be equal to 0 but must be less than 1.
Blockly.Msg.MATH_RANDOM_FLOAT_TOOLTIP = 'Return a random fraction between 0.0 (inclusive) and 1.0 (exclusive).';

/** @type {string} */
/// {{Optional}} url - Information about how to calculate atan2.
Blockly.Msg.MATH_ATAN2_HELPURL = 'https://en.wikipedia.org/wiki/Atan2';
/** @type {string} */
/// block text - The title of the block that calculates atan2 of point (X, Y).  For example, if the point is (-1, -1), this returns -135. %1 is a placeholder for the X coordinate, %2 is the placeholder for the Y coordinate.
Blockly.Msg.MATH_ATAN2_TITLE = 'atan2 of X:%1 Y:%2';
/** @type {string} */
/// tooltip - Return the arctangent of point (X, Y) in degrees from -180 to 180. For example, if the point is (-1, -1) this returns -135.
Blockly.Msg.MATH_ATAN2_TOOLTIP = 'Return the arctangent of point (X, Y) in degrees from -180 to 180.';

// Text Blocks.
/** @type {string} */
/// {{Optional}} url - Information about how computers represent text (sometimes referred to as ''string''s).
Blockly.Msg.TEXT_TEXT_HELPURL = 'https://en.wikipedia.org/wiki/String_(computer_science)';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Text https://github.com/google/blockly/wiki/Text].
Blockly.Msg.TEXT_TEXT_TOOLTIP = 'A letter, word, or line of text.';

/** @type {string} */
/// {{Optional}} url - Information on concatenating/appending pieces of text.
Blockly.Msg.TEXT_JOIN_HELPURL = 'https://github.com/google/blockly/wiki/Text#text-creation';
/** @type {string} */
/// block text - See [https://github.com/google/blockly/wiki/Text#text-creation https://github.com/google/blockly/wiki/Text#text-creation].
Blockly.Msg.TEXT_JOIN_TITLE_CREATEWITH = 'create text with';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Text#text-creation create text with] for more information.
Blockly.Msg.TEXT_JOIN_TOOLTIP = 'Create a piece of text by joining together any number of items.';

/** @type {string} */
/// block text - This is shown when the programmer wants to change the number of pieces of text being joined together.  See [https://github.com/google/blockly/wiki/Text#text-creation https://github.com/google/blockly/wiki/Text#text-creation], specifically the last picture in the 'Text creation' section.\n{{Identical|Join}}
Blockly.Msg.TEXT_CREATE_JOIN_TITLE_JOIN = 'join';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Text#text-creation https://github.com/google/blockly/wiki/Text#text-creation], specifically the last picture in the 'Text creation' section.
Blockly.Msg.TEXT_CREATE_JOIN_TOOLTIP = 'Add, remove, or reorder sections to reconfigure this text block.';
/** @type {string} */
Blockly.Msg.TEXT_CREATE_JOIN_ITEM_TITLE_ITEM = Blockly.Msg.VARIABLES_DEFAULT_NAME;
/** @type {string} */
/// block text - See [https://github.com/google/blockly/wiki/Text#text-creation https://github.com/google/blockly/wiki/Text#text-creation], specifically the last picture in the 'Text creation' section.
Blockly.Msg.TEXT_CREATE_JOIN_ITEM_TOOLTIP = 'Add an item to the text.';

/** @type {string} */
/// {{Optional}} url - This and the other text-related URLs are going to be hard to translate.  As always, it is okay to leave untranslated or paste in the English-language URL.  For these URLs, you might also consider a general URL about how computers represent text (such as the translation of [https://en.wikipedia.org/wiki/String_(computer_science) this Wikipedia page]).
Blockly.Msg.TEXT_APPEND_HELPURL = 'https://github.com/google/blockly/wiki/Text#text-modification';
/** @type {string} */
/// block input text - Message that the variable name at %1 will have the item at %2 appended to it.
/// [[File:blockly-append-text.png]]
Blockly.Msg.TEXT_APPEND_TITLE = 'to %1 append text %2';
/** @type {string} */
Blockly.Msg.TEXT_APPEND_VARIABLE = Blockly.Msg.VARIABLES_DEFAULT_NAME;
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Text#text-modification https://github.com/google/blockly/wiki/Text#text-modification] for more information.\n\nParameters:\n* %1 - the name of the variable to which text should be appended
Blockly.Msg.TEXT_APPEND_TOOLTIP = 'Append some text to variable \'%1\'.';

/** @type {string} */
/// {{Optional}} url - Information about text on computers (usually referred to as 'strings').
Blockly.Msg.TEXT_LENGTH_HELPURL = 'https://github.com/google/blockly/wiki/Text#text-modification';
/** @type {string} */
/// block text - See [https://github.com/google/blockly/wiki/Text#text-length https://github.com/google/blockly/wiki/Text#text-length].
/// \n\nParameters:\n* %1 - the piece of text to take the length of
Blockly.Msg.TEXT_LENGTH_TITLE = 'length of %1';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Text#text-length https://github.com/google/blockly/wiki/Text#text-length].
Blockly.Msg.TEXT_LENGTH_TOOLTIP = 'Returns the number of letters (including spaces) in the provided text.';

/** @type {string} */
/// {{Optional}} url - Information about empty pieces of text on computers (usually referred to as 'empty strings').
Blockly.Msg.TEXT_ISEMPTY_HELPURL = 'https://github.com/google/blockly/wiki/Text#checking-for-empty-text';
/** @type {string} */
/// block text - See [https://github.com/google/blockly/wiki/Text#checking-for-empty-text https://github.com/google/blockly/wiki/Text#checking-for-empty-text].
/// \n\nParameters:\n* %1 - the piece of text to test for emptiness
Blockly.Msg.TEXT_ISEMPTY_TITLE = '%1 is empty';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Text#checking-for-empty-text https://github.com/google/blockly/wiki/Text#checking-for-empty-text].
Blockly.Msg.TEXT_ISEMPTY_TOOLTIP = 'Returns true if the provided text is empty.';

/** @type {string} */
/// {{Optional}} url - Information about finding a character in a piece of text.
Blockly.Msg.TEXT_INDEXOF_HELPURL = 'https://github.com/google/blockly/wiki/Text#finding-text';
/** @type {string} */
/// tooltip - %1 will be replaced by either the number 0 or -1 depending on the indexing mode. See [https://github.com/google/blockly/wiki/Text#finding-text https://github.com/google/blockly/wiki/Text#finding-text].
Blockly.Msg.TEXT_INDEXOF_TOOLTIP = 'Returns the index of the first/last occurrence of the first text in the second text. Returns %1 if text is not found.';
/** @type {string} */
/// block text - Title of blocks allowing users to find text.  See
/// [https://github.com/google/blockly/wiki/Text#finding-text
/// https://github.com/google/blockly/wiki/Text#finding-text].
/// [[File:Blockly-find-text.png]].
/// In English the expanded message is 'in text %1 find (first|last) occurance of text %3'
/// where %1 and %3 are added by the user. See TEXT_INDEXOF_OPERATOR_FIRST and
/// TEXT_INDEXOF_OPERATOR_LAST for the dropdown text that replaces %2.
Blockly.Msg.TEXT_INDEXOF_TITLE = 'in text %1 %2 %3';
/** @type {string} */
/// dropdown - See [https://github.com/google/blockly/wiki/Text#finding-text
/// https://github.com/google/blockly/wiki/Text#finding-text].
/// [[File:Blockly-find-text.png]].
Blockly.Msg.TEXT_INDEXOF_OPERATOR_FIRST = 'find first occurrence of text';
/** @type {string} */
/// dropdown - See [https://github.com/google/blockly/wiki/Text#finding-text
/// https://github.com/google/blockly/wiki/Text#finding-text].  This would
/// replace 'find first occurrence of text' below.  (For more information on
/// how common text is factored out of dropdown menus, see
/// [https://translatewiki.net/wiki/Translating:Blockly#Drop-Down_Menus
/// https://translatewiki.net/wiki/Translating:Blockly#Drop-Down_Menus)].)
/// [[File:Blockly-find-text.png]].
Blockly.Msg.TEXT_INDEXOF_OPERATOR_LAST = 'find last occurrence of text';
/** @type {string} */

/// {{Optional}} url - Information about extracting characters (letters, number, symbols, etc.) from text.
Blockly.Msg.TEXT_CHARAT_HELPURL = 'https://github.com/google/blockly/wiki/Text#extracting-text';
/** @type {string} */
/// block text - Text for a block to extract a letter (or number,
/// punctuation character, etc.) from a string, as shown below. %1 is added by
/// the user and %2 is replaced by a dropdown of options, possibly followed by
/// another user supplied string. TEXT_CHARAT_TAIL is then added to the end.  See
/// [https://github.com/google/blockly/wiki/Text#extracting-a-single-character
/// https://github.com/google/blockly/wiki/Text#extracting-a-single-character].
/// [[File:Blockly-text-get.png]]
Blockly.Msg.TEXT_CHARAT_TITLE = 'in text %1 %2';
/** @type {string} */
/// dropdown - Indicates that the letter (or number, punctuation character, etc.) with the
/// specified index should be obtained from the preceding piece of text.  See
/// [https://github.com/google/blockly/wiki/Text#extracting-a-single-character
/// https://github.com/google/blockly/wiki/Text#extracting-a-single-character].
/// [[File:Blockly-text-get.png]]
Blockly.Msg.TEXT_CHARAT_FROM_START = 'get letter #';
/** @type {string} */
/// block text - Indicates that the letter (or number, punctuation character, etc.) with the
/// specified index from the end of a given piece of text should be obtained. See
/// [https://github.com/google/blockly/wiki/Text#extracting-a-single-character
/// https://github.com/google/blockly/wiki/Text#extracting-a-single-character].
/// [[File:Blockly-text-get.png]]
Blockly.Msg.TEXT_CHARAT_FROM_END = 'get letter # from end';
/** @type {string} */
/// block text - Indicates that the first letter of the following piece of text should be
/// retrieved.  See [https://github.com/google/blockly/wiki/Text#extracting-a-single-character
/// https://github.com/google/blockly/wiki/Text#extracting-a-single-character].
/// [[File:Blockly-text-get.png]]
Blockly.Msg.TEXT_CHARAT_FIRST = 'get first letter';
/** @type {string} */
/// block text - Indicates that the last letter (or number, punctuation mark, etc.) of the
/// following piece of text should be retrieved.  See
/// [https://github.com/google/blockly/wiki/Text#extracting-a-single-character
/// https://github.com/google/blockly/wiki/Text#extracting-a-single-character].
/// [[File:Blockly-text-get.png]]
Blockly.Msg.TEXT_CHARAT_LAST = 'get last letter';
/** @type {string} */
/// block text - Indicates that any letter (or number, punctuation mark, etc.) in the
/// following piece of text should be randomly selected.  See
/// [https://github.com/google/blockly/wiki/Text#extracting-a-single-character
/// https://github.com/google/blockly/wiki/Text#extracting-a-single-character].
/// [[File:Blockly-text-get.png]]
Blockly.Msg.TEXT_CHARAT_RANDOM = 'get random letter';
/** @type {string} */
/// block text - Text that goes after the rightmost block/dropdown when getting a single letter from
/// a piece of text, as in [https://blockly-demo.appspot.com/static/apps/code/index.html#3m23km these
/// blocks] or shown below.  For most languages, this will be blank.
/// [[File:Blockly-text-get.png]]
Blockly.Msg.TEXT_CHARAT_TAIL = '';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Text#extracting-a-single-character
/// https://github.com/google/blockly/wiki/Text#extracting-a-single-character].
/// [[File:Blockly-text-get.png]]
Blockly.Msg.TEXT_CHARAT_TOOLTIP = 'Returns the letter at the specified position.';

/** @type {string} */
/// See [https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text
/// https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text].
Blockly.Msg.TEXT_GET_SUBSTRING_TOOLTIP = 'Returns a specified portion of the text.';
/** @type {string} */
/// {{Optional}} url - Information about extracting characters from text.  Reminder: urls are the
/// lowest priority translations.  Feel free to skip.
Blockly.Msg.TEXT_GET_SUBSTRING_HELPURL = 'https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text';
/** @type {string} */
/// block text - Precedes a piece of text from which a portion should be extracted.
/// [[File:Blockly-get-substring.png]]
Blockly.Msg.TEXT_GET_SUBSTRING_INPUT_IN_TEXT = 'in text';
/** @type {string} */
/// dropdown - Indicates that the following number specifies the position (relative to the start
/// position) of the beginning of the region of text that should be obtained from the preceding
/// piece of text.  See [https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text
/// https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text].
/// [[File:Blockly-get-substring.png]]
Blockly.Msg.TEXT_GET_SUBSTRING_START_FROM_START = 'get substring from letter #';
/** @type {string} */
/// dropdown - Indicates that the following number specifies the position (relative to the end
/// position) of the beginning of the region of text that should be obtained from the preceding
/// piece of text.  See [https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text
/// https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text].
/// Note: If {{msg-blockly|ORDINAL_NUMBER_SUFFIX}} is defined, it will
/// automatically appear ''after'' this and any other
/// [https://translatewiki.net/wiki/Translating:Blockly#Ordinal_numbers ordinal numbers]
/// on this block.
/// [[File:Blockly-get-substring.png]]
Blockly.Msg.TEXT_GET_SUBSTRING_START_FROM_END = 'get substring from letter # from end';
/** @type {string} */
/// block text - Indicates that a region starting with the first letter of the preceding piece
/// of text should be extracted.  See
/// [https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text
/// https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text].
/// [[File:Blockly-get-substring.png]]
Blockly.Msg.TEXT_GET_SUBSTRING_START_FIRST = 'get substring from first letter';
/** @type {string} */
/// dropdown - Indicates that the following number specifies the position (relative to
/// the start position) of the end of the region of text that should be obtained from the
/// preceding piece of text.  See
/// [https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text
/// https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text].
/// [[File:Blockly-get-substring.png]]
Blockly.Msg.TEXT_GET_SUBSTRING_END_FROM_START = 'to letter #';
/** @type {string} */
/// dropdown - Indicates that the following number specifies the position (relative to the
/// end position) of the end of the region of text that should be obtained from the preceding
/// piece of text.  See
/// [https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text
/// https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text].
/// [[File:Blockly-get-substring.png]]
Blockly.Msg.TEXT_GET_SUBSTRING_END_FROM_END = 'to letter # from end';
/** @type {string} */
/// block text - Indicates that a region ending with the last letter of the preceding piece
/// of text should be extracted.  See
/// [https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text
/// https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text].
/// [[File:Blockly-get-substring.png]]
Blockly.Msg.TEXT_GET_SUBSTRING_END_LAST = 'to last letter';
/** @type {string} */
/// block text - Text that should go after the rightmost block/dropdown when
/// [https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text
/// extracting a region of text].  In most languages, this will be the empty string.
/// [[File:Blockly-get-substring.png]]
Blockly.Msg.TEXT_GET_SUBSTRING_TAIL = '';

/** @type {string} */
/// {{Optional}} url - Information about the case of letters (upper-case and lower-case).
Blockly.Msg.TEXT_CHANGECASE_HELPURL = 'https://github.com/google/blockly/wiki/Text#adjusting-text-case';
/** @type {string} */
/// tooltip - Describes a block to adjust the case of letters.  For more information on this block,
/// see [https://github.com/google/blockly/wiki/Text#adjusting-text-case
/// https://github.com/google/blockly/wiki/Text#adjusting-text-case].
Blockly.Msg.TEXT_CHANGECASE_TOOLTIP = 'Return a copy of the text in a different case.';
/** @type {string} */
/// block text - Indicates that all of the letters in the following piece of text should be
/// capitalized.  If your language does not use case, you may indicate that this is not
/// applicable to your language.  For more information on this block, see
/// [https://github.com/google/blockly/wiki/Text#adjusting-text-case
/// https://github.com/google/blockly/wiki/Text#adjusting-text-case].
Blockly.Msg.TEXT_CHANGECASE_OPERATOR_UPPERCASE = 'to UPPER CASE';
/** @type {string} */
/// block text - Indicates that all of the letters in the following piece of text should be converted to lower-case.  If your language does not use case, you may indicate that this is not applicable to your language.  For more information on this block, see [https://github.com/google/blockly/wiki/Text#adjusting-text-case https://github.com/google/blockly/wiki/Text#adjusting-text-case].
Blockly.Msg.TEXT_CHANGECASE_OPERATOR_LOWERCASE = 'to lower case';
/** @type {string} */
/// block text - Indicates that the first letter of each of the following words should be capitalized and the rest converted to lower-case.  If your language does not use case, you may indicate that this is not applicable to your language.  For more information on this block, see [https://github.com/google/blockly/wiki/Text#adjusting-text-case https://github.com/google/blockly/wiki/Text#adjusting-text-case].
Blockly.Msg.TEXT_CHANGECASE_OPERATOR_TITLECASE = 'to Title Case';

/** @type {string} */
/// {{Optional}} url - Information about trimming (removing) text off the beginning and ends of pieces of text.
Blockly.Msg.TEXT_TRIM_HELPURL = 'https://github.com/google/blockly/wiki/Text#trimming-removing-spaces';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Text#trimming-removing-spaces
/// https://github.com/google/blockly/wiki/Text#trimming-removing-spaces].
Blockly.Msg.TEXT_TRIM_TOOLTIP = 'Return a copy of the text with spaces removed from one or both ends.';
/** @type {string} */
/// dropdown - Removes spaces from the beginning and end of a piece of text.  See
/// [https://github.com/google/blockly/wiki/Text#trimming-removing-spaces
/// https://github.com/google/blockly/wiki/Text#trimming-removing-spaces].  Note that neither
/// this nor the other options modify the original piece of text (that follows);
/// the block just returns a version of the text without the specified spaces.
Blockly.Msg.TEXT_TRIM_OPERATOR_BOTH = 'trim spaces from both sides of';
/** @type {string} */
/// dropdown - Removes spaces from the beginning of a piece of text.  See
/// [https://github.com/google/blockly/wiki/Text#trimming-removing-spaces
/// https://github.com/google/blockly/wiki/Text#trimming-removing-spaces].
/// Note that in right-to-left scripts, this will remove spaces from the right side.
Blockly.Msg.TEXT_TRIM_OPERATOR_LEFT = 'trim spaces from left side of';
/** @type {string} */
/// dropdown - Removes spaces from the end of a piece of text.  See
/// [https://github.com/google/blockly/wiki/Text#trimming-removing-spaces
/// https://github.com/google/blockly/wiki/Text#trimming-removing-spaces].
/// Note that in right-to-left scripts, this will remove spaces from the left side.
Blockly.Msg.TEXT_TRIM_OPERATOR_RIGHT = 'trim spaces from right side of';

/** @type {string} */
/// {{Optional}} url - Information about displaying text on computers.
Blockly.Msg.TEXT_PRINT_HELPURL = 'https://github.com/google/blockly/wiki/Text#printing-text';
/** @type {string} */
/// block text - Display the input on the screen.  See
/// [https://github.com/google/blockly/wiki/Text#printing-text
/// https://github.com/google/blockly/wiki/Text#printing-text].
/// \n\nParameters:\n* %1 - the value to print
Blockly.Msg.TEXT_PRINT_TITLE = 'print %1';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Text#printing-text
/// https://github.com/google/blockly/wiki/Text#printing-text].
Blockly.Msg.TEXT_PRINT_TOOLTIP = 'Print the specified text, number or other value.';
/** @type {string} */
/// {{Optional}} url - Information about getting text from users.
Blockly.Msg.TEXT_PROMPT_HELPURL = 'https://github.com/google/blockly/wiki/Text#getting-input-from-the-user';
/** @type {string} */
/// dropdown - Specifies that a piece of text should be requested from the user with
/// the following message.  See [https://github.com/google/blockly/wiki/Text#printing-text
/// https://github.com/google/blockly/wiki/Text#printing-text].
Blockly.Msg.TEXT_PROMPT_TYPE_TEXT = 'prompt for text with message';
/** @type {string} */
/// dropdown - Specifies that a number should be requested from the user with the
/// following message.  See [https://github.com/google/blockly/wiki/Text#printing-text
/// https://github.com/google/blockly/wiki/Text#printing-text].
Blockly.Msg.TEXT_PROMPT_TYPE_NUMBER = 'prompt for number with message';
/** @type {string} */
/// dropdown - Precedes the message with which the user should be prompted for
/// a number.  See [https://github.com/google/blockly/wiki/Text#printing-text
/// https://github.com/google/blockly/wiki/Text#printing-text].
Blockly.Msg.TEXT_PROMPT_TOOLTIP_NUMBER = 'Prompt for user for a number.';
/** @type {string} */
/// dropdown - Precedes the message with which the user should be prompted for some text.
/// See [https://github.com/google/blockly/wiki/Text#printing-text
/// https://github.com/google/blockly/wiki/Text#printing-text].
Blockly.Msg.TEXT_PROMPT_TOOLTIP_TEXT = 'Prompt for user for some text.';

/** @type {string} */
/// block text - Title of a block that counts the number of instances of
/// a smaller pattern (%1) inside a longer string (%2).
Blockly.Msg.TEXT_COUNT_MESSAGE0 = 'count %1 in %2';
/** @type {string} */
/// {{Optional}} url - Information about counting how many times a string appears in another string.
Blockly.Msg.TEXT_COUNT_HELPURL = 'https://github.com/google/blockly/wiki/Text#counting-substrings';
/** @type {string} */
/// tooltip - Short description of a block that counts how many times some text occurs within some other text.
Blockly.Msg.TEXT_COUNT_TOOLTIP = 'Count how many times some text occurs within some other text.';

/** @type {string} */
/// block text - Title of a block that returns a copy of text (%3) with all
/// instances of some smaller text (%1) replaced with other text (%2).
Blockly.Msg.TEXT_REPLACE_MESSAGE0 = 'replace %1 with %2 in %3';
/** @type {string} */
/// {{Optional}} url - Information about replacing each copy text (or string, in computer lingo) with other text.
Blockly.Msg.TEXT_REPLACE_HELPURL = 'https://github.com/google/blockly/wiki/Text#replacing-substrings';
/** @type {string} */
/// tooltip - Short description of a block that replaces copies of text in a large text with other text.
Blockly.Msg.TEXT_REPLACE_TOOLTIP = 'Replace all occurances of some text within some other text.';

/** @type {string} */
/// block text - Title of block that returns a copy of text (%1) with the order
/// of letters and characters reversed.
Blockly.Msg.TEXT_REVERSE_MESSAGE0 = 'reverse %1';
/** @type {string} */
/// {{Optional}} url - Information about reversing a letters/characters in text.
Blockly.Msg.TEXT_REVERSE_HELPURL = 'https://github.com/google/blockly/wiki/Text#reversing-text';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Text].
Blockly.Msg.TEXT_REVERSE_TOOLTIP = 'Reverses the order of the characters in the text.';
Blockly.Msg.TEXT_TO_NUMBER_TITLE = 'convert %1 to number';
Blockly.Msg.TEXT_TO_NUMBER_TOOLTIP = 'Convert a string to a number';

// Lists Blocks.
/** @type {string} */
/// {{Optional}} url - Information on empty lists.
Blockly.Msg.LISTS_CREATE_EMPTY_HELPURL = 'https://github.com/google/blockly/wiki/Lists#create-empty-list';
/** @type {string} */
/// block text - See [https://github.com/google/blockly/wiki/Lists#create-empty-list https://github.com/google/blockly/wiki/Lists#create-empty-list].
Blockly.Msg.LISTS_CREATE_EMPTY_TITLE = 'create empty list';
/** @type {string} */
/// block text - See [https://github.com/google/blockly/wiki/Lists#create-empty-list https://github.com/google/blockly/wiki/Lists#create-empty-list].
Blockly.Msg.LISTS_CREATE_EMPTY_TOOLTIP = 'Returns a list, of length 0, containing no data records';

/** @type {string} */
/// {{Optional}} url - Information on building lists.
Blockly.Msg.LISTS_CREATE_WITH_HELPURL = 'https://github.com/google/blockly/wiki/Lists#create-list-with';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#create-list-with https://github.com/google/blockly/wiki/Lists#create-list-with].
Blockly.Msg.LISTS_CREATE_WITH_TOOLTIP = 'Create a list with any number of items.';
/** @type {string} */
/// block text - See [https://github.com/google/blockly/wiki/Lists#create-list-with https://github.com/google/blockly/wiki/Lists#create-list-with].
Blockly.Msg.LISTS_CREATE_WITH_INPUT_WITH = 'create list with';
/** @type {string} */
/// block text - This appears in a sub-block when [https://github.com/google/blockly/wiki/Lists#changing-number-of-inputs changing the number of inputs in a ''''create list with'''' block].\n{{Identical|List}}
Blockly.Msg.LISTS_CREATE_WITH_CONTAINER_TITLE_ADD = 'list';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#changing-number-of-inputs https://github.com/google/blockly/wiki/Lists#changing-number-of-inputs].
Blockly.Msg.LISTS_CREATE_WITH_CONTAINER_TOOLTIP = 'Add, remove, or reorder sections to reconfigure this list block.';
/** @type {string} */
Blockly.Msg.LISTS_CREATE_WITH_ITEM_TITLE = Blockly.Msg.VARIABLES_DEFAULT_NAME;
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#changing-number-of-inputs https://github.com/google/blockly/wiki/Lists#changing-number-of-inputs].
Blockly.Msg.LISTS_CREATE_WITH_ITEM_TOOLTIP = 'Add an item to the list.';

/** @type {string} */
/// {{Optional}} url - Information about [https://github.com/google/blockly/wiki/Lists#create-list-with creating a list with multiple copies of a single item].
Blockly.Msg.LISTS_REPEAT_HELPURL = 'https://github.com/google/blockly/wiki/Lists#create-list-with';
/** @type {string} */
/// {{Optional}} url - See [https://github.com/google/blockly/wiki/Lists#create-list-with creating a list with multiple copies of a single item].
Blockly.Msg.LISTS_REPEAT_TOOLTIP = 'Creates a list consisting of the given value repeated the specified number of times.';
/** @type {string} */
/// block text - See [https://github.com/google/blockly/wiki/Lists#create-list-with
/// https://github.com/google/blockly/wiki/Lists#create-list-with].
///\n\nParameters:\n* %1 - the item (text) to be repeated\n* %2 - the number of times to repeat it
Blockly.Msg.LISTS_REPEAT_TITLE = 'create list with item %1 repeated %2 times';

/** @type {string} */
/// {{Optional}} url - Information about how the length of a list is computed (i.e., by the total number of elements, not the number of different elements).
Blockly.Msg.LISTS_LENGTH_HELPURL = 'https://github.com/google/blockly/wiki/Lists#length-of';
/** @type {string} */
/// block text - See [https://github.com/google/blockly/wiki/Lists#length-of https://github.com/google/blockly/wiki/Lists#length-of].
/// \n\nParameters:\n* %1 - the list whose length is desired
Blockly.Msg.LISTS_LENGTH_TITLE = 'length of %1';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#length-of https://github.com/google/blockly/wiki/Lists#length-of Blockly:Lists:length of].
Blockly.Msg.LISTS_LENGTH_TOOLTIP = 'Returns the length of a list.';

/** @type {string} */
/// {{Optional}} url - See [https://github.com/google/blockly/wiki/Lists#is-empty https://github.com/google/blockly/wiki/Lists#is-empty].
Blockly.Msg.LISTS_ISEMPTY_HELPURL = 'https://github.com/google/blockly/wiki/Lists#is-empty';
/** @type {string} */
/// block text - See [https://github.com/google/blockly/wiki/Lists#is-empty
/// https://github.com/google/blockly/wiki/Lists#is-empty].
/// \n\nParameters:\n* %1 - the list to test
Blockly.Msg.LISTS_ISEMPTY_TITLE = '%1 is empty';
/** @type {string} */
/// block tooltip - See [https://github.com/google/blockly/wiki/Lists#is-empty
/// https://github.com/google/blockly/wiki/Lists#is-empty].
Blockly.Msg.LISTS_ISEMPTY_TOOLTIP = 'Returns true if the list is empty.';

/** @type {string} */
/// block text - Title of blocks operating on [https://github.com/google/blockly/wiki/Lists lists].
Blockly.Msg.LISTS_INLIST = 'in list';

/** @type {string} */
/// {{Optional}} url - See [https://github.com/google/blockly/wiki/Lists#getting-items-from-a-list
/// https://github.com/google/blockly/wiki/Lists#getting-items-from-a-list].
Blockly.Msg.LISTS_INDEX_OF_HELPURL = 'https://github.com/google/blockly/wiki/Lists#getting-items-from-a-list';
/** @type {string} */
Blockly.Msg.LISTS_INDEX_OF_INPUT_IN_LIST = Blockly.Msg.LISTS_INLIST;
/** @type {string} */
/// dropdown - See [https://github.com/google/blockly/wiki/Lists#finding-items-in-a-list
/// Lists#finding-items-in-a-list].
/// [[File:Blockly-list-find.png]]
Blockly.Msg.LISTS_INDEX_OF_FIRST = 'find first occurrence of item';
/** @type {string} */
/// dropdown - See [https://github.com/google/blockly/wiki/Lists#finding-items-in-a-list
/// https://github.com/google/blockly/wiki/Lists#finding-items-in-a-list].
/// [[File:Blockly-list-find.png]]
Blockly.Msg.LISTS_INDEX_OF_LAST = 'find last occurrence of item';
/** @type {string} */
/// tooltip - %1 will be replaced by either the number 0 or -1 depending on the indexing mode.  See [https://github.com/google/blockly/wiki/Lists#finding-items-in-a-list
/// https://github.com/google/blockly/wiki/Lists#finding-items-in-a-list].
/// [[File:Blockly-list-find.png]]
Blockly.Msg.LISTS_INDEX_OF_TOOLTIP = 'Returns the index of the first/last occurrence of the item in the list. Returns %1 if item is not found.';

/** @type {string} */
Blockly.Msg.LISTS_GET_INDEX_HELPURL = Blockly.Msg.LISTS_INDEX_OF_HELPURL;
/** @type {string} */
/// dropdown - Indicates that the user wishes to
/// [https://github.com/google/blockly/wiki/Lists#getting-a-single-item
/// get an item from a list] without removing it from the list.
Blockly.Msg.LISTS_GET_INDEX_GET = 'get';
/** @type {string} */
/// dropdown - Indicates that the user wishes to
/// [https://github.com/google/blockly/wiki/Lists#getting-a-single-item
/// get and remove an item from a list], as opposed to merely getting
/// it without modifying the list.
Blockly.Msg.LISTS_GET_INDEX_GET_REMOVE = 'get and remove';
/** @type {string} */
/// dropdown - Indicates that the user wishes to
/// [https://github.com/google/blockly/wiki/Lists#removing-an-item
/// remove an item from a list].\n{{Identical|Remove}}
Blockly.Msg.LISTS_GET_INDEX_REMOVE = 'remove';
/** @type {string} */
/// dropdown - Indicates that an index relative to the front of the list should be used to
/// [https://github.com/google/blockly/wiki/Lists#getting-a-single-item get and/or remove
/// an item from a list].  Note: If {{msg-blockly|ORDINAL_NUMBER_SUFFIX}} is defined, it will
/// automatically appear ''after'' this number (and any other ordinal numbers on this block).
/// See [[Translating:Blockly#Ordinal_numbers]] for more information on ordinal numbers in Blockly.
/// [[File:Blockly-list-get-item.png]]
Blockly.Msg.LISTS_GET_INDEX_FROM_START = '#';
/** @type {string} */
/// dropdown - Indicates that an index relative to the end of the list should be used
/// to [https://github.com/google/blockly/wiki/Lists#getting-a-single-item access an item in a list].
/// [[File:Blockly-list-get-item.png]]
Blockly.Msg.LISTS_GET_INDEX_FROM_END = '# from end';
/** @type {string} */
/// dropdown - Indicates that the '''first''' item should be
/// [https://github.com/google/blockly/wiki/Lists#getting-a-single-item accessed in a list].
/// [[File:Blockly-list-get-item.png]]
Blockly.Msg.LISTS_GET_INDEX_FIRST = 'first';
/** @type {string} */
/// dropdown - Indicates that the '''last''' item should be
/// [https://github.com/google/blockly/wiki/Lists#getting-a-single-item accessed in a list].
/// [[File:Blockly-list-get-item.png]]
Blockly.Msg.LISTS_GET_INDEX_LAST = 'last';
/** @type {string} */
/// dropdown - Indicates that a '''random''' item should be
/// [https://github.com/google/blockly/wiki/Lists#getting-a-single-item accessed in a list].
/// [[File:Blockly-list-get-item.png]]
Blockly.Msg.LISTS_GET_INDEX_RANDOM = 'random';
/** @type {string} */
/// block text - Text that should go after the rightmost block/dropdown when
/// [https://github.com/google/blockly/wiki/Lists#getting-a-single-item
/// accessing an item from a list].  In most languages, this will be the empty string.
/// [[File:Blockly-list-get-item.png]]
Blockly.Msg.LISTS_GET_INDEX_TAIL = '';
/** @type {string} */
Blockly.Msg.LISTS_GET_INDEX_INPUT_IN_LIST = Blockly.Msg.LISTS_INLIST;
/** @type {string} */
/// tooltip - Indicates the ordinal number that the first item in a list is referenced by.  %1 will be replaced by either '#0' or '#1' depending on the indexing mode.
Blockly.Msg.LISTS_INDEX_FROM_START_TOOLTIP = '%1 is the first item.';
/** @type {string} */
/// tooltip - Indicates the ordinal number that the last item in a list is referenced by.  %1 will be replaced by either '#0' or '#1' depending on the indexing mode.
Blockly.Msg.LISTS_INDEX_FROM_END_TOOLTIP = '%1 is the last item.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-a-single-item https://github.com/google/blockly/wiki/Lists#getting-a-single-item] for more information.
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_FROM = 'Returns the item at the specified position in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-a-single-item https://github.com/google/blockly/wiki/Lists#getting-a-single-item] for more information.
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_FIRST = 'Returns the first item in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-a-single-item https://github.com/google/blockly/wiki/Lists#getting-a-single-item] for more information.
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_LAST = 'Returns the last item in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-a-single-item https://github.com/google/blockly/wiki/Lists#getting-a-single-item] for more information.
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_RANDOM = 'Returns a random item in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-and-removing-an-item] (for remove and return) and [https://github.com/google/blockly/wiki/Lists#getting-a-single-item] for '#' or '# from end'.
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_REMOVE_FROM = 'Removes and returns the item at the specified position in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-and-removing-an-item] (for remove and return) and [https://github.com/google/blockly/wiki/Lists#getting-a-single-item] for 'first'.
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_REMOVE_FIRST = 'Removes and returns the first item in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-and-removing-an-item] (for remove and return) and [https://github.com/google/blockly/wiki/Lists#getting-a-single-item] for 'last'.
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_REMOVE_LAST = 'Removes and returns the last item in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-and-removing-an-item] (for remove and return) and [https://github.com/google/blockly/wiki/Lists#getting-a-single-item] for 'random'.
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_REMOVE_RANDOM = 'Removes and returns a random item in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-and-removing-an-item] (for remove and return) and [https://github.com/google/blockly/wiki/Lists#getting-a-single-item] for '#' or '# from end'.
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_REMOVE_FROM = 'Removes the item at the specified position in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-and-removing-an-item] (for remove and return) and [https://github.com/google/blockly/wiki/Lists#getting-a-single-item] for 'first'.
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_REMOVE_FIRST = 'Removes the first item in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-and-removing-an-item] (for remove and return) and [https://github.com/google/blockly/wiki/Lists#getting-a-single-item] for 'last'.
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_REMOVE_LAST = 'Removes the last item in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-and-removing-an-item] (for remove and return) and [https://github.com/google/blockly/wiki/Lists#getting-a-single-item] for 'random'.
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_REMOVE_RANDOM = 'Removes a random item in a list.';
/** @type {string} */
/// {{Optional}} url - Information about putting items in lists.
Blockly.Msg.LISTS_SET_INDEX_HELPURL = 'https://github.com/google/blockly/wiki/Lists#in-list--set';
/** @type {string} */
Blockly.Msg.LISTS_SET_INDEX_INPUT_IN_LIST = Blockly.Msg.LISTS_INLIST;
/** @type {string} */
/// block text - [https://github.com/google/blockly/wiki/Lists#in-list--set
/// Replaces an item in a list].
/// [[File:Blockly-in-list-set-insert.png]]
Blockly.Msg.LISTS_SET_INDEX_SET = 'set';
/** @type {string} */
/// block text - [https://github.com/google/blockly/wiki/Lists#in-list--insert-at
/// Inserts an item into a list].
/// [[File:Blockly-in-list-set-insert.png]]
Blockly.Msg.LISTS_SET_INDEX_INSERT = 'insert at';
/** @type {string} */
/// block text - The word(s) after the position in the list and before the item to be set/inserted.
/// [[File:Blockly-in-list-set-insert.png]]
Blockly.Msg.LISTS_SET_INDEX_INPUT_TO = 'as';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-a-single-item} (even though the page describes the 'get' block, the idea is the same for the 'set' block).
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_SET_FROM = 'Sets the item at the specified position in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-a-single-item} (even though the page describes the 'get' block, the idea is the same for the 'set' block).
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_SET_FIRST = 'Sets the first item in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-a-single-item} (even though the page describes the 'get' block, the idea is the same for the 'set' block).
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_SET_LAST = 'Sets the last item in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-a-single-item} (even though the page describes the 'get' block, the idea is the same for the 'set' block).
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_SET_RANDOM = 'Sets a random item in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-a-single-item} (even though the page describes the 'get' block, the idea is the same for the 'insert' block).
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_INSERT_FROM = 'Inserts the item at the specified position in a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-a-single-item} (even though the page describes the 'get' block, the idea is the same for the 'insert' block).
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_INSERT_FIRST = 'Inserts the item at the start of a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-a-single-item} (even though the page describes the 'get' block, the idea is the same for the 'insert' block).
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_INSERT_LAST = 'Append the item to the end of a list.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-a-single-item} (even though the page describes the 'get' block, the idea is the same for the 'insert' block).
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_INSERT_RANDOM = 'Inserts the item randomly in a list.';

/** @type {string} */
/// {{Optional}} url - Information describing extracting a sublist from an existing list.
Blockly.Msg.LISTS_GET_SUBLIST_HELPURL = 'https://github.com/google/blockly/wiki/Lists#getting-a-sublist';
/** @type {string} */
Blockly.Msg.LISTS_GET_SUBLIST_INPUT_IN_LIST = Blockly.Msg.LISTS_INLIST;
/** @type {string} */
/// dropdown - Indicates that an index relative to the front of the list should be used
/// to specify the beginning of the range from which to
/// [https://github.com/google/blockly/wiki/Lists#getting-a-sublist get a sublist].
/// [[File:Blockly-get-sublist.png]]
/// Note: If {{msg-blockly|ORDINAL_NUMBER_SUFFIX}} is defined, it will
/// automatically appear ''after'' this number (and any other ordinal numbers on this block).
/// See [[Translating:Blockly#Ordinal_numbers]] for more information on ordinal numbers in Blockly.
Blockly.Msg.LISTS_GET_SUBLIST_START_FROM_START = 'get sub-list from #';
/** @type {string} */
/// dropdown - Indicates that an index relative to the end of the list should be used
/// to specify the beginning of the range from which to
/// [https://github.com/google/blockly/wiki/Lists#getting-a-sublist get a sublist].
Blockly.Msg.LISTS_GET_SUBLIST_START_FROM_END = 'get sub-list from # from end';
/** @type {string} */
/// dropdown - Indicates that the
/// [https://github.com/google/blockly/wiki/Lists#getting-a-sublist sublist to extract]
/// should begin with the list's first item.
Blockly.Msg.LISTS_GET_SUBLIST_START_FIRST = 'get sub-list from first';
/** @type {string} */
/// dropdown - Indicates that an index relative to the front of the list should be
/// used to specify the end of the range from which to
/// [https://github.com/google/blockly/wiki/Lists#getting-a-sublist get a sublist].
/// [[File:Blockly-get-sublist.png]]
Blockly.Msg.LISTS_GET_SUBLIST_END_FROM_START = 'to #';
/** @type {string} */
/// dropdown - Indicates that an index relative to the end of the list should be
/// used to specify the end of the range from which to
/// [https://github.com/google/blockly/wiki/Lists#getting-a-sublist get a sublist].
/// [[File:Blockly-get-sublist.png]]
Blockly.Msg.LISTS_GET_SUBLIST_END_FROM_END = 'to # from end';
/** @type {string} */
/// dropdown - Indicates that the '''last''' item in the given list should be
/// [https://github.com/google/blockly/wiki/Lists#getting-a-sublist the end
/// of the selected sublist].
/// [[File:Blockly-get-sublist.png]]
Blockly.Msg.LISTS_GET_SUBLIST_END_LAST = 'to last';
/** @type {string} */
/// block text - This appears in the rightmost position ('tail') of the
/// sublist block, as described at
/// [https://github.com/google/blockly/wiki/Lists#getting-a-sublist
/// https://github.com/google/blockly/wiki/Lists#getting-a-sublist].
/// In English and most other languages, this is the empty string.
/// [[File:Blockly-get-sublist.png]]
Blockly.Msg.LISTS_GET_SUBLIST_TAIL = '';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#getting-a-sublist
/// https://github.com/google/blockly/wiki/Lists#getting-a-sublist] for more information.
/// [[File:Blockly-get-sublist.png]]
Blockly.Msg.LISTS_GET_SUBLIST_TOOLTIP = 'Creates a copy of the specified portion of a list.';

/** @type {string} */
/// {{Optional}} url - Information describing sorting a list.
Blockly.Msg.LISTS_SORT_HELPURL = 'https://github.com/google/blockly/wiki/Lists#sorting-a-list';
/** @type {string} */
/// Sort as type %1 (numeric or alphabetic) in order %2 (ascending or descending) a list of items %3.\n{{Identical|Sort}}
Blockly.Msg.LISTS_SORT_TITLE = 'sort %1 %2 %3';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#sorting-a-list].
Blockly.Msg.LISTS_SORT_TOOLTIP = 'Sort a copy of a list.';
/** @type {string} */
/// sorting order or direction from low to high value for numeric, or A-Z for alphabetic.\n{{Identical|Ascending}}
Blockly.Msg.LISTS_SORT_ORDER_ASCENDING = 'ascending';
/** @type {string} */
/// sorting order or direction from high to low value for numeric, or Z-A for alphabetic.\n{{Identical|Descending}}
Blockly.Msg.LISTS_SORT_ORDER_DESCENDING = 'descending';
/** @type {string} */
/// sort by treating each item as a number.
Blockly.Msg.LISTS_SORT_TYPE_NUMERIC = 'numeric';
/** @type {string} */
/// sort by treating each item alphabetically, case-sensitive.
Blockly.Msg.LISTS_SORT_TYPE_TEXT = 'alphabetic';
/** @type {string} */
/// sort by treating each item alphabetically, ignoring differences in case.
Blockly.Msg.LISTS_SORT_TYPE_IGNORECASE = 'alphabetic, ignore case';

/** @type {string} */
/// {{Optional}} url - Information describing splitting text into a list, or joining a list into text.
Blockly.Msg.LISTS_SPLIT_HELPURL = 'https://github.com/google/blockly/wiki/Lists#splitting-strings-and-joining-lists';
/** @type {string} */
/// dropdown - Indicates that text will be split up into a list (e.g. 'a-b-c' -> ['a', 'b', 'c']).
Blockly.Msg.LISTS_SPLIT_LIST_FROM_TEXT = 'make list from text';
/** @type {string} */
/// dropdown - Indicates that a list will be joined together to form text (e.g. ['a', 'b', 'c'] -> 'a-b-c').
Blockly.Msg.LISTS_SPLIT_TEXT_FROM_LIST = 'make text from list';
/** @type {string} */
/// block text - Prompts for a letter to be used as a separator when splitting or joining text.
Blockly.Msg.LISTS_SPLIT_WITH_DELIMITER = 'with delimiter';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#make-list-from-text
/// https://github.com/google/blockly/wiki/Lists#make-list-from-text] for more information.
Blockly.Msg.LISTS_SPLIT_TOOLTIP_SPLIT = 'Split text into a list of texts, breaking at each delimiter.';
/** @type {string} */
/// tooltip - See [https://github.com/google/blockly/wiki/Lists#make-text-from-list
/// https://github.com/google/blockly/wiki/Lists#make-text-from-list] for more information.
Blockly.Msg.LISTS_SPLIT_TOOLTIP_JOIN = 'Join a list of texts into one text, separated by a delimiter.';

/** @type {string} */
/// {{Optional}} url - Information describing reversing a list.
Blockly.Msg.LISTS_REVERSE_HELPURL = 'https://github.com/google/blockly/wiki/Lists#reversing-a-list';
/** @type {string} */
/// block text - Title of block that returns a copy of a list (%1) with the order of items reversed.
Blockly.Msg.LISTS_REVERSE_MESSAGE0 = 'reverse %1';
/** @type {string} */
/// tooltip - Short description for a block that reverses a copy of a list.
Blockly.Msg.LISTS_REVERSE_TOOLTIP = 'Reverse a copy of a list.';

/** @type {string} */
/// grammar - Text that follows an ordinal number (a number that indicates
/// position relative to other numbers).  In most languages, such text appears
/// before the number, so this should be blank.  An exception is Hungarian.
/// See [[Translating:Blockly#Ordinal_numbers]] for more information.
Blockly.Msg.ORDINAL_NUMBER_SUFFIX = '';

// Variables Blocks.
/** @type {string} */
/// {{Optional}} url - Information about ''variables'' in computer programming.  Consider using your language's translation of [https://en.wikipedia.org/wiki/Variable_(computer_science) https://en.wikipedia.org/wiki/Variable_(computer_science)], if it exists.
Blockly.Msg.VARIABLES_GET_HELPURL = 'https://github.com/google/blockly/wiki/Variables#get';
/** @type {string} */
/// tooltip - This gets the value of the named variable without modifying it.
Blockly.Msg.VARIABLES_GET_TOOLTIP = 'Returns the value of this variable.';
/** @type {string} */
/// context menu - Selecting this creates a block to set (change) the value of this variable.
/// \n\nParameters:\n* %1 - the name of the variable.
Blockly.Msg.VARIABLES_GET_CREATE_SET = 'Create \'set %1\'';

/** @type {string} */
/// {{Optional}} url - Information about ''variables'' in computer programming.  Consider using your language's translation of [https://en.wikipedia.org/wiki/Variable_(computer_science) https://en.wikipedia.org/wiki/Variable_(computer_science)], if it exists.
Blockly.Msg.VARIABLES_SET_HELPURL = 'https://github.com/google/blockly/wiki/Variables#set';
/** @type {string} */
/// block text - Change the value of a mathematical variable: '''set [the value of] x to 7'''.\n\nParameters:\n* %1 - the name of the variable.\n* %2 - the value to be assigned.
Blockly.Msg.VARIABLES_SET = 'set %1 to %2';
/** @type {string} */
/// tooltip - This initializes or changes the value of the named variable.
Blockly.Msg.VARIABLES_SET_TOOLTIP = 'Sets this variable to be equal to the input.';
/** @type {string} */
/// context menu - Selecting this creates a block to get (change) the value of
/// this variable.\n\nParameters:\n* %1 - the name of the variable.
Blockly.Msg.VARIABLES_SET_CREATE_GET = 'Create \'get %1\'';

// Procedures Blocks.
/** @type {string} */
/// {{Optional}} url - Information about defining [https://en.wikipedia.org/wiki/Subroutine functions] that do not have return values.
Blockly.Msg.PROCEDURES_DEFNORETURN_HELPURL = 'https://en.wikipedia.org/wiki/Subroutine';
/** @type {string} */
/// block text - This precedes the name of the function when defining it.  See
/// [https://blockly-demo.appspot.com/static/apps/code/index.html?lang=en#c84aoc this sample
/// function definition].
Blockly.Msg.PROCEDURES_DEFNORETURN_TITLE = 'to';
/** @type {string} */
/// default name - This acts as a placeholder for the name of a function on a
/// function definition block, as shown on
/// [https://blockly-demo.appspot.com/static/apps/code/index.html?lang=en#w7cfju this block].
/// The user will replace it with the function's name.
Blockly.Msg.PROCEDURES_DEFNORETURN_PROCEDURE = 'do something';
/** @type {string} */
/// block text - This precedes the list of parameters on a function's definition block.  See
/// [https://blockly-demo.appspot.com/static/apps/code/index.html?lang=en#voztpd this sample
/// function with parameters].
Blockly.Msg.PROCEDURES_BEFORE_PARAMS = 'with:';
/** @type {string} */
/// block text - This precedes the list of parameters on a function's caller block.  See
/// [https://blockly-demo.appspot.com/static/apps/code/index.html?lang=en#voztpd this sample
/// function with parameters].
Blockly.Msg.PROCEDURES_CALL_BEFORE_PARAMS = 'with:';
/** @type {string} */
/// block text - This appears next to the function's 'body', the blocks that should be
/// run when the function is called, as shown in
/// [https://blockly-demo.appspot.com/static/apps/code/index.html?lang=en#voztpd this sample
/// function definition].
Blockly.Msg.PROCEDURES_DEFNORETURN_DO = '';
/** @type {string} */
/// tooltip
Blockly.Msg.PROCEDURES_DEFNORETURN_TOOLTIP = 'Creates a function with no output.';
/** @type {string} */
/// Placeholder text that the user is encouraged to replace with a description of what their function does.
Blockly.Msg.PROCEDURES_DEFNORETURN_COMMENT = 'Describe this function...';
/** @type {string} */
/// {{Optional}} url - Information about defining [https://en.wikipedia.org/wiki/Subroutine functions] that have return values.
Blockly.Msg.PROCEDURES_DEFRETURN_HELPURL = 'https://en.wikipedia.org/wiki/Subroutine';
/** @type {string} */
Blockly.Msg.PROCEDURES_DEFRETURN_TITLE = Blockly.Msg.PROCEDURES_DEFNORETURN_TITLE;
/** @type {string} */
Blockly.Msg.PROCEDURES_DEFRETURN_PROCEDURE = Blockly.Msg.PROCEDURES_DEFNORETURN_PROCEDURE;
/** @type {string} */
Blockly.Msg.PROCEDURES_DEFRETURN_DO = Blockly.Msg.PROCEDURES_DEFNORETURN_DO;
/** @type {string} */
Blockly.Msg.PROCEDURES_DEFRETURN_COMMENT = Blockly.Msg.PROCEDURES_DEFNORETURN_COMMENT;
/** @type {string} */
/// block text - This imperative or infinite verb precedes the value that is used as the return value
/// (output) of this function.  See
/// [https://blockly-demo.appspot.com/static/apps/code/index.html?lang=en#6ot5y5 this sample
/// function that returns a value].
Blockly.Msg.PROCEDURES_DEFRETURN_RETURN = 'return';
/** @type {string} */
/// tooltip
Blockly.Msg.PROCEDURES_DEFRETURN_TOOLTIP = 'Creates a function with an output.';
/** @type {string} */
/// Label for a checkbox that controls if statements are allowed in a function.
Blockly.Msg.PROCEDURES_ALLOW_STATEMENTS = 'allow statements';

/** @type {string} */
/// alert - The user has created a function with two parameters that have the same name.  Every parameter must have a different name.
Blockly.Msg.PROCEDURES_DEF_DUPLICATE_WARNING = 'Warning: This function has duplicate parameters.';

/** @type {string} */
/// {{Optional}} url - Information about calling [https://en.wikipedia.org/wiki/Subroutine functions] that do not return values.
Blockly.Msg.PROCEDURES_CALLNORETURN_HELPURL = 'https://en.wikipedia.org/wiki/Subroutine';
/** @type {string} */
/// tooltip - This block causes the body (blocks inside) of the named function definition to be run.
Blockly.Msg.PROCEDURES_CALLNORETURN_TOOLTIP = 'Run the user-defined function \'%1\'.';

/** @type {string} */
/// {{Optional}} url - Information about calling [https://en.wikipedia.org/wiki/Subroutine functions] that return values.
Blockly.Msg.PROCEDURES_CALLRETURN_HELPURL = 'https://en.wikipedia.org/wiki/Subroutine';
/** @type {string} */
/// tooltip - This block causes the body (blocks inside) of the named function definition to be run.\n\nParameters:\n* %1 - the name of the function.
Blockly.Msg.PROCEDURES_CALLRETURN_TOOLTIP = 'Run the user-defined function \'%1\' and use its output.';

/** @type {string} */
/// block text - This text appears on a block in a window that appears when the user clicks
/// on the plus sign or star on a function definition block.  It refers to the set of parameters
/// (referred to by the simpler term 'inputs') to the function.  See
/// [[Translating:Blockly#function_definitions]].\n{{Identical|Input}}
Blockly.Msg.PROCEDURES_MUTATORCONTAINER_TITLE = 'inputs';
/** @type {string} */
/// tooltip
Blockly.Msg.PROCEDURES_MUTATORCONTAINER_TOOLTIP = 'Add, remove, or reorder inputs to this function.';
/** @type {string} */
/// block text - This text appears on a block in a window that appears when the user clicks
/// on the plus sign or star on a function definition block].  It appears on the block for
/// adding an individual parameter (referred to by the simpler term 'inputs') to the function.
/// See [[Translating:Blockly#function_definitions]].
Blockly.Msg.PROCEDURES_MUTATORARG_TITLE = 'input name:';
/** @type {string} */
/// tooltip
Blockly.Msg.PROCEDURES_MUTATORARG_TOOLTIP = 'Add an input to the function.';

/** @type {string} */
/// context menu - This appears on the context menu for function calls.  Selecting
/// it causes the corresponding function definition to be highlighted (as shown at
/// [[Translating:Blockly#context_menus]].
Blockly.Msg.PROCEDURES_HIGHLIGHT_DEF = 'Highlight function definition';
/** @type {string} */
/// context menu - This appears on the context menu for function definitions.
/// Selecting it creates a block to call the function.\n\nParameters:\n* %1 - the name of the function.\n{{Identical|Create}}
Blockly.Msg.PROCEDURES_CREATE_DO = 'Create \'%1\'';

/** @type {string} */
/// tooltip - If the first value is true, this causes the second value to be returned
/// immediately from the enclosing function.
Blockly.Msg.PROCEDURES_IFRETURN_TOOLTIP = 'If a value is true, then return a second value.';
/** @type {string} */
/// {{Optional}} url - Information about guard clauses.
Blockly.Msg.PROCEDURES_IFRETURN_HELPURL = 'http://c2.com/cgi/wiki?GuardClause';
/** @type {string} */
/// warning - This appears if the user tries to use this block outside of a function definition.
Blockly.Msg.PROCEDURES_IFRETURN_WARNING = 'Warning: This block may be used only within a function definition.';

/** @type {string} */
/// comment text - This text appears in a new workspace comment, to hint that
/// the user can type here.
Blockly.Msg.WORKSPACE_COMMENT_DEFAULT_TEXT = 'Say something...';

/** @type {string} */
/// workspace - This text is read out when a user navigates to the workspace while
/// using a screen reader.
Blockly.Msg.WORKSPACE_ARIA_LABEL = 'Blockly Workspace';

/** @type {string} */
/// warning - This appears if the user collapses a block, and blocks inside
/// that block have warnings attached to them. It should inform the user that the
/// block they collapsed contains blocks that have warnings.
Blockly.Msg.COLLAPSED_WARNINGS_WARNING = 'Collapsed blocks contain warnings.';

// GALAXIA BLOCKS AND CATEGORIES

Blockly.Msg.CATEGORY_COMMUNICATION = 'Communication';
Blockly.Msg.CATEGORY_SENSORS = 'Sensors';
Blockly.Msg.CATEGORY_ROBOTS = 'Robots';
Blockly.Msg.CATEGORY_TEXT = 'Text';
Blockly.Msg.CATEGORY_VARIABLES = 'Variables';
Blockly.Msg.CATEGORY_LISTS = 'Lists';
Blockly.Msg.CATEGORY_PROCEDURES = 'Functions';
Blockly.Msg.CATEGORY_COLOUR = 'Colour';

Blockly.Msg.CATEGORY_DISPLAY = 'Display';
Blockly.Msg.CATEGORY_IO = 'Inputs/Outputs';
Blockly.Msg.CATEGORY_ACTUATORS = 'Actuators';
Blockly.Msg.CATEGORY_LOOPS = 'Loops';
Blockly.Msg.CATEGORY_LOGIC = 'Logic';
Blockly.Msg.CATEGORY_MATH = 'Math';
Blockly.Msg.CATEGORY_TIME = 'Time';
Blockly.Msg.CATEGORY_GROVE = 'Grove';
Blockly.Msg.CATEGORY_DATA = 'Data logger';

Blockly.Msg.CATEGORY_APPEARANCE = 'Appearance';
Blockly.Msg.CATEGORY_SOUND = 'Sound';
Blockly.Msg.CATEGORY_CONTROL = 'Control';
Blockly.Msg.CATEGORY_OPERATORS = 'Operators';

// Toolbox subcategory labels.
Blockly.Msg.SUBCATEGORY_DISPLAY_PLOT = 'Plot';
Blockly.Msg.SUBCATEGORY_LOOPS = 'Loops';
Blockly.Msg.SUBCATEGORY_LOGIC = 'Logic';
Blockly.Msg.SUBCATEGORY_MICROBIT = 'Micro:bit';
Blockly.Msg.SUBCATEGORY_DISPLAYS = 'Screens';
Blockly.Msg.SUBCATEGORY_LED_MODULES = 'LED modules';
Blockly.Msg.SUBCATEGORY_MORPION = 'Tic Tac Toe';
Blockly.Msg.SUBCATEGORY_GAMES = 'Games';
Blockly.Msg.SUBCATEGORY_MICROPHONE = 'Microphone';
Blockly.Msg.SUBCATEGORY_EXTERNAL_MODULES = 'External modules';
Blockly.Msg.SUBCATEGORY_PINS = 'Pins';
Blockly.Msg.SUBCATEGORY_DATA_LOGGING = 'Data logging';
Blockly.Msg.SUBCATEGORY_WIRELESS_COMMUNICATION = 'Wireless communication';
Blockly.Msg.SUBCATEGORY_TRACKING_MODULES = 'Tracking modules';
Blockly.Msg.SUBCATEGORY_SERIAL_CONNECTION = 'Serial connection';
Blockly.Msg.SUBCATEGORY_SENSORS_GAS = 'Gas sensors';
Blockly.Msg.SUBCATEGORY_SENSORS_CLIMATE = 'Climate sensors';
Blockly.Msg.SUBCATEGORY_SENSORS_SOUNDLIGHT = 'Sound & Light sensors';
Blockly.Msg.SUBCATEGORY_SENSORS_DISTANCEMOVEMENT = 'Distance & Motion sensors';
Blockly.Msg.SUBCATEGORY_SENSORS_OTHER = 'Other sensors';
Blockly.Msg.SUBCATEGORY_MOTORS = 'Motors';
Blockly.Msg.SUBCATEGORY_MUSIC = 'Music';
Blockly.Msg.SUBCATEGORY_COMPUTER = 'Computer';
Blockly.Msg.SUBCATEGORY_SPEECH = 'Speech';
Blockly.Msg.SUBCATEGORY_MAQUEEN = 'Maqueen';
Blockly.Msg.SUBCATEGORY_CODO = 'Codo';
Blockly.Msg.SUBCATEGORY_BITBOT = 'Bit:Bot';
Blockly.Msg.SUBCATEGORY_GAMEPAD = 'Gamepad';
Blockly.Msg.SUBCATEGORY_GALAXIA = 'Galaxia';
Blockly.Msg.SUBCATEGORY_RADIO = 'Radio';
Blockly.Msg.SUBCATEGORY_SIMPLE_WIFI_COMMON = 'WiFi Common';
Blockly.Msg.SUBCATEGORY_SIMPLE_WIFI_CLIENT = 'WiFi Client';
Blockly.Msg.SUBCATEGORY_SIMPLE_WIFI_SERVER = 'WiFi Server';
Blockly.Msg.SUBCATEGORY_SIMPLE_HTTP = 'Web Server';
Blockly.Msg.SUBCATEGORY_SIMPLE_HTTP_REQUEST = 'HTTP Request';
Blockly.Msg.SUBCATEGORY_SIMPLE_MQTT = 'MQTT';
Blockly.Msg.SUBCATEGORY_SENSOR_ACCELEROMETER = 'Accelerometer';
Blockly.Msg.SUBCATEGORY_SENSOR_MAGNETOMETER = 'Magnetometer';
Blockly.Msg.SUBCATEGORY_SENSOR_LIGHT_SENSOR = 'Light sensor';
Blockly.Msg.SUBCATEGORY_SENSOR_TEMPERATURE = 'Temperature';
Blockly.Msg.SUBCATEGORY_SIGNALS = 'Signals';
Blockly.Msg.VARIABLES_DEFAULT_NAME = 'variable';
Blockly.Msg.UNNAMED_KEY = 'unnamed';
Blockly.Msg.TODAY = 'Today';
Blockly.Msg.ORDINAL_NUMBER_SUFFIX = '';

// Workspace.
Blockly.Msg.WORKSPACE_COMMENT_DEFAULT_TEXT = 'Say something...';
Blockly.Msg.WORKSPACE_ARIA_LABEL = 'Blockly Workspace';
Blockly.Msg.COLLAPSED_WARNINGS_WARNING = 'Collapsed blocks contain warnings.';

// Context menus.
Blockly.Msg.DUPLICATE_BLOCK = 'Duplicate';
Blockly.Msg.ADD_COMMENT = 'Add Comment';
Blockly.Msg.REMOVE_COMMENT = 'Remove Comment';
Blockly.Msg.DUPLICATE_COMMENT = 'Duplicate Comment';
Blockly.Msg.EXTERNAL_INPUTS = 'External Inputs';
Blockly.Msg.INLINE_INPUTS = 'Inline Inputs';
Blockly.Msg.DELETE_BLOCK = 'Delete Block';
Blockly.Msg.DELETE_X_BLOCKS = 'Delete %1 Blocks';
Blockly.Msg.DELETE_ALL_BLOCKS = 'Delete all %1 blocks?';
Blockly.Msg.CLEAN_UP = 'Clean up Blocks';
Blockly.Msg.COLLAPSE_BLOCK = 'Collapse Block';
Blockly.Msg.COLLAPSE_ALL = 'Collapse Blocks';
Blockly.Msg.EXPAND_BLOCK = 'Expand Block';
Blockly.Msg.EXPAND_ALL = 'Expand Blocks';
Blockly.Msg.DISABLE_BLOCK = 'Disable Block';
Blockly.Msg.ENABLE_BLOCK = 'Enable Block';
Blockly.Msg.HELP = 'Help';
Blockly.Msg.UNDO = 'Undo';
Blockly.Msg.REDO = 'Redo';

// IOS.
Blockly.Msg.IOS_OK = 'OK';
Blockly.Msg.IOS_CANCEL = 'Cancel';
Blockly.Msg.IOS_ERROR = 'Error';
Blockly.Msg.IOS_PROCEDURES_INPUTS = 'INPUTS';
Blockly.Msg.IOS_PROCEDURES_ADD_INPUT = '+ Add Input';
Blockly.Msg.IOS_PROCEDURES_ALLOW_STATEMENTS = 'Allow statements';
Blockly.Msg.IOS_PROCEDURES_DUPLICATE_INPUTS_ERROR = 'This function has duplicate inputs.';
Blockly.Msg.IOS_VARIABLES_ADD_VARIABLE = '+ Add Variable';
Blockly.Msg.IOS_VARIABLES_ADD_BUTTON = 'Add';
Blockly.Msg.IOS_VARIABLES_RENAME_BUTTON = 'Rename';
Blockly.Msg.IOS_VARIABLES_DELETE_BUTTON = 'Delete';
Blockly.Msg.IOS_VARIABLES_VARIABLE_NAME = 'Variable name';
Blockly.Msg.IOS_VARIABLES_EMPTY_NAME_ERROR = 'You can\'t use an empty variable name.';

// Variable renaming.
Blockly.Msg.CHANGE_VALUE_TITLE = 'Change value:';
Blockly.Msg.RENAME_VARIABLE = 'Rename variable...';
Blockly.Msg.RENAME_VARIABLE_TITLE = 'Rename all \'%1\' variables to:';

// Variable creation.
Blockly.Msg.NEW_VARIABLE = 'Create variable...';
Blockly.Msg.NEW_STRING_VARIABLE = 'Create string variable...';
Blockly.Msg.NEW_NUMBER_VARIABLE = 'Create number variable...';
Blockly.Msg.NEW_COLOUR_VARIABLE = 'Create colour variable...';
Blockly.Msg.NEW_VARIABLE_TITLE = 'New variable name:';
Blockly.Msg.NEW_VARIABLE_TYPE_TITLE = 'New variable type:';
Blockly.Msg.VARIABLE_ALREADY_EXISTS = 'A variable named \'%1\' already exists.';
Blockly.Msg.VARIABLE_ALREADY_EXISTS_FOR_ANOTHER_TYPE = 'A variable named \'%1\' already exists for another type: \'%2\'.';

// Variable deletion.
Blockly.Msg.DELETE_VARIABLE = 'Delete the \'%1\' variable';
Blockly.Msg.DELETE_VARIABLE_CONFIRMATION = 'Delete %1 uses of the \'%2\' variable?';
Blockly.Msg.CANNOT_DELETE_VARIABLE_PROCEDURE = 'Can\'t delete the variable \'%1\' because it\'s part of the definition of the function \'%2\'';

// Warning text.
Blockly.Msg.LOGIC_COMPARE_WARNING = 'Unable to compare first input type \'%1\' with second input type \'%2\'';
Blockly.Msg.LOGIC_TERNARY_WARNING = 'Unable to return first input type \'%1\' with second input type \'%2\'';

// Start blocks.
Blockly.Msg.ON_START_TITLE = 'On start    ';
Blockly.Msg.ON_START_TOOLTIP = 'Add instructions in this block to execute them when the micro:bit card is powered up.';
Blockly.Msg.FOREVER_TITLE = 'Forever    ';
Blockly.Msg.FOREVER_TOOLTIP = 'Add instructions to this block to execute them in a loop.';

// Colour blocks.
Blockly.Msg.COLOUR_PICKER_HELPURL = 'https://en.wikipedia.org/wiki/Color';
Blockly.Msg.COLOUR_PICKER_TOOLTIP = 'Choose a colour from the palette.';
Blockly.Msg.COLOUR_RANDOM_HELPURL = 'http://randomcolour.com';
Blockly.Msg.COLOUR_RANDOM_TITLE = 'random colour';
Blockly.Msg.COLOUR_RANDOM_TOOLTIP = 'Choose a colour at random.';
Blockly.Msg.COLOUR_RGB_HELPURL = 'http://www.december.com/html/spec/colorper.html';
Blockly.Msg.COLOUR_RGB_TITLE = 'colour with';
Blockly.Msg.COLOUR_RGB_RED = 'red';
Blockly.Msg.COLOUR_RGB_GREEN = 'green';
Blockly.Msg.COLOUR_RGB_BLUE = 'blue';
Blockly.Msg.COLOUR_RGB_TOOLTIP = 'Create a colour with the specified amount of red, green, and blue. All values must be between 0 and 100.';
Blockly.Msg.COLOUR_BLEND_HELPURL = 'http://meyerweb.com/eric/tools/color-blend/';
Blockly.Msg.COLOUR_BLEND_TITLE = 'blend';
Blockly.Msg.COLOUR_BLEND_COLOUR1 = 'colour 1';
Blockly.Msg.COLOUR_BLEND_COLOUR2 = 'colour 2';
Blockly.Msg.COLOUR_BLEND_RATIO = 'ratio';
Blockly.Msg.COLOUR_BLEND_TOOLTIP = 'Blends two colours together with a given ratio (0.0 - 1.0).';

// Logic blocks.
Blockly.Msg.CONTROLS_IF_HELPURL = 'https://github.com/google/blockly/wiki/IfElse';
Blockly.Msg.CONTROLS_IF_TOOLTIP_1 = 'If a value is true, then do some statements.';
Blockly.Msg.CONTROLS_IF_TOOLTIP_2 = 'If a value is true, then do the first block of statements. Otherwise, do the second block of statements.';
Blockly.Msg.CONTROLS_IF_TOOLTIP_3 = 'If the first value is true, then do the first block of statements. Otherwise, if the second value is true, do the second block of statements.';
Blockly.Msg.CONTROLS_IF_TOOLTIP_4 = 'If the first value is true, then do the first block of statements. Otherwise, if the second value is true, do the second block of statements. If none of the values are true, do the last block of statements.';
Blockly.Msg.CONTROLS_IF_MSG_IF = 'if %1 do';
Blockly.Msg.CONTROLS_IF_MSG_ELSEIF = 'else if';
Blockly.Msg.CONTROLS_IF_MSG_ELSE = 'else';
Blockly.Msg.CONTROLS_IF_IF_TOOLTIP = 'Add, remove, or reorder sections to reconfigure this if block.';
Blockly.Msg.CONTROLS_IF_ELSEIF_TOOLTIP = 'Add a condition to the if block.';
Blockly.Msg.CONTROLS_IF_ELSE_TOOLTIP = 'Add a final, catch-all condition to the if block.';
Blockly.Msg.CONTROLS_IF_ELSEIF_TITLE_ELSEIF = Blockly.Msg.CONTROLS_IF_MSG_ELSEIF;
Blockly.Msg.CONTROLS_IF_ELSE_TITLE_ELSE = Blockly.Msg.CONTROLS_IF_MSG_ELSE;
Blockly.Msg.CONTROLS_IF_IF_TITLE_IF = Blockly.Msg.CONTROLS_IF_MSG_IF;
Blockly.Msg.LOGIC_COMPARE_HELPURL = 'https://en.wikipedia.org/wiki/Inequality_(mathematics)';
Blockly.Msg.LOGIC_COMPARE_TOOLTIP_EQ = 'Return true if both inputs equal each other.';
Blockly.Msg.LOGIC_COMPARE_TOOLTIP_NEQ = 'Return true if both inputs are not equal to each other.';
Blockly.Msg.LOGIC_COMPARE_TOOLTIP_LT = 'Return true if the first input is smaller than the second input.';
Blockly.Msg.LOGIC_COMPARE_TOOLTIP_LTE = 'Return true if the first input is smaller than or equal to the second input.';
Blockly.Msg.LOGIC_COMPARE_TOOLTIP_GT = 'Return true if the first input is greater than the second input.';
Blockly.Msg.LOGIC_COMPARE_TOOLTIP_GTE = 'Return true if the first input is greater than or equal to the second input.';
Blockly.Msg.LOGIC_OPERATION_HELPURL = 'https://github.com/google/blockly/wiki/Logic#logical-operations';
Blockly.Msg.LOGIC_OPERATION_AND = 'and';
Blockly.Msg.LOGIC_OPERATION_OR = 'or';
Blockly.Msg.LOGIC_OPERATION_TOOLTIP_AND = 'Return true if both inputs are true.';
Blockly.Msg.LOGIC_OPERATION_TOOLTIP_OR = 'Return true if at least one of the inputs is true.';
Blockly.Msg.LOGIC_BOOLEAN_HELPURL = 'https://github.com/google/blockly/wiki/Logic#values';
Blockly.Msg.LOGIC_BOOLEAN_TRUE = 'true';
Blockly.Msg.LOGIC_BOOLEAN_FALSE = 'false';
Blockly.Msg.LOGIC_BOOLEAN_TOOLTIP = 'Returns either true or false.';
Blockly.Msg.LOGIC_NEGATE_HELPURL = 'https://github.com/google/blockly/wiki/Logic#not';
Blockly.Msg.LOGIC_NEGATE_TITLE = 'not %1';
Blockly.Msg.LOGIC_NEGATE_TOOLTIP = 'Returns true if the input is false. Returns false if the input is true.';
Blockly.Msg.LOGIC_NULL_HELPURL = 'https://en.wikipedia.org/wiki/Nullable_type';
Blockly.Msg.LOGIC_NULL_TITLE = 'null';
Blockly.Msg.LOGIC_NULL_TOOLTIP = 'Returns null.';
Blockly.Msg.LOGIC_TERNARY_HELPURL = 'https://en.wikipedia.org/wiki/%3F:';
Blockly.Msg.LOGIC_TERNARY_CONDITION = 'test';
Blockly.Msg.LOGIC_TERNARY_IF_TRUE = 'if true';
Blockly.Msg.LOGIC_TERNARY_IF_FALSE = 'if false';
Blockly.Msg.LOGIC_TERNARY_TOOLTIP = 'Check the condition in \'test\'. If the condition is true, returns the \'if true\' value; otherwise returns the \'if false\' value.';

// Loop blocks.
Blockly.Msg.CONTROLS_REPEAT_HELPURL = 'https://en.wikipedia.org/wiki/For_loop';
Blockly.Msg.CONTROLS_REPEAT_TITLE = 'repeat %1 times';
Blockly.Msg.CONTROLS_REPEAT_TOOLTIP = 'Do some statements several times.';
Blockly.Msg.CONTROLS_WHILEUNTIL_HELPURL = 'https://github.com/google/blockly/wiki/Loops#repeat';
Blockly.Msg.CONTROLS_WHILEUNTIL_OPERATOR_UNTIL = 'repeat until';
Blockly.Msg.CONTROLS_WHILEUNTIL_OPERATOR_WHILE = 'repeat while';
Blockly.Msg.CONTROLS_WHILEUNTIL_TOOLTIP_UNTIL = 'While a value is false, then do some statements.';
Blockly.Msg.CONTROLS_WHILEUNTIL_TOOLTIP_WHILE = 'While a value is true, then do some statements.';
Blockly.Msg.CONTROLS_FOR_HELPURL = 'https://github.com/google/blockly/wiki/Loops#count-with';
Blockly.Msg.CONTROLS_FOR_TITLE = 'count with %1 from %2 to %3 by step %4';
Blockly.Msg.CONTROLS_FOR_TOOLTIP = 'Have the variable \'%1\' take on the values from the start number to the end number, counting by the specified interval, and do the specified blocks.';
Blockly.Msg.CONTROLS_FOREACH_HELPURL = 'https://github.com/google/blockly/wiki/Loops#for-each';
Blockly.Msg.CONTROLS_FOREACH_TITLE = 'for each item %1 in list %2';
Blockly.Msg.CONTROLS_FOREACH_TOOLTIP = 'For each item in a list, set the variable \'%1\' to the item, and then do some statements.';
Blockly.Msg.CONTROLS_FLOW_STATEMENTS_HELPURL = 'https://github.com/google/blockly/wiki/Loops#loop-termination-blocks';
Blockly.Msg.CONTROLS_FLOW_STATEMENTS_OPERATOR_BREAK = 'break out of loop';
Blockly.Msg.CONTROLS_FLOW_STATEMENTS_OPERATOR_CONTINUE = 'continue with next iteration of loop';
Blockly.Msg.CONTROLS_FLOW_STATEMENTS_TOOLTIP_BREAK = 'Break out of the containing loop.';
Blockly.Msg.CONTROLS_FLOW_STATEMENTS_TOOLTIP_CONTINUE = 'Skip the rest of this loop, and continue with the next iteration.';
Blockly.Msg.CONTROLS_FLOW_STATEMENTS_WARNING = 'Warning: This block may only be used within a loop.';

// Math blocks.
Blockly.Msg.MATH_NUMBER_HELPURL = 'https://en.wikipedia.org/wiki/Number';
Blockly.Msg.MATH_NUMBER_TOOLTIP = 'A number.';
Blockly.Msg.MATH_ADDITION_SYMBOL = '+';
Blockly.Msg.MATH_SUBTRACTION_SYMBOL = '-';
Blockly.Msg.MATH_DIVISION_SYMBOL = '÷';
Blockly.Msg.MATH_MULTIPLICATION_SYMBOL = '×';
Blockly.Msg.MATH_POWER_SYMBOL = '^';
Blockly.Msg.MATH_ARITHMETIC_HELPURL = 'https://en.wikipedia.org/wiki/Arithmetic';
Blockly.Msg.MATH_ARITHMETIC_TOOLTIP_ADD = 'Return the sum of the two numbers.';
Blockly.Msg.MATH_ARITHMETIC_TOOLTIP_DIVIDE = 'Return the quotient of the two numbers.';
Blockly.Msg.MATH_ARITHMETIC_TOOLTIP_MINUS = 'Return the difference of the two numbers.';
Blockly.Msg.MATH_ARITHMETIC_TOOLTIP_MULTIPLY = 'Return the product of the two numbers.';
Blockly.Msg.MATH_ARITHMETIC_TOOLTIP_POWER = 'Return the first number raised to the power of the second number.';
Blockly.Msg.MATH_SINGLE_HELPURL = 'https://en.wikipedia.org/wiki/Square_root';
Blockly.Msg.MATH_SINGLE_OP_ROOT = 'square root';
Blockly.Msg.MATH_SINGLE_OP_ABSOLUTE = 'absolute';
Blockly.Msg.MATH_SINGLE_TOOLTIP_ROOT = 'Return the square root of a number.';
Blockly.Msg.MATH_SINGLE_TOOLTIP_ABS = 'Return the absolute value of a number.';
Blockly.Msg.MATH_SINGLE_TOOLTIP_NEG = 'Return the negation of a number.';
Blockly.Msg.MATH_SINGLE_TOOLTIP_LN = 'Return the natural logarithm of a number.';
Blockly.Msg.MATH_SINGLE_TOOLTIP_LOG10 = 'Return the base 10 logarithm of a number.';
Blockly.Msg.MATH_SINGLE_TOOLTIP_EXP = 'Return e to the power of a number.';
Blockly.Msg.MATH_SINGLE_TOOLTIP_POW10 = 'Return 10 to the power of a number.';
Blockly.Msg.MATH_TRIG_HELPURL = 'https://en.wikipedia.org/wiki/Trigonometric_functions';
Blockly.Msg.MATH_TRIG_COS = 'cos';
Blockly.Msg.MATH_TRIG_SIN = 'sin';
Blockly.Msg.MATH_TRIG_TAN = 'tan';
Blockly.Msg.MATH_TRIG_ACOS = 'acos';
Blockly.Msg.MATH_TRIG_ASIN = 'asin';
Blockly.Msg.MATH_TRIG_ATAN = 'atan';
Blockly.Msg.MATH_TRIG_TOOLTIP_COS = 'Return the cosine of a degree (not radian).';
Blockly.Msg.MATH_TRIG_TOOLTIP_SIN = 'Return the sine of a degree (not radian).';
Blockly.Msg.MATH_TRIG_TOOLTIP_TAN = 'Return the tangent of a degree (not radian).';
Blockly.Msg.MATH_TRIG_TOOLTIP_ACOS = 'Return the arccosine of a number.';
Blockly.Msg.MATH_TRIG_TOOLTIP_ASIN = 'Return the arcsine of a number.';
Blockly.Msg.MATH_TRIG_TOOLTIP_ATAN = 'Return the arctangent of a number.';
Blockly.Msg.MATH_CONSTANT_HELPURL = 'https://en.wikipedia.org/wiki/Mathematical_constant';
Blockly.Msg.MATH_CONSTANT_TOOLTIP = 'Return one of the common constants: π (3.141…), e (2.718…), φ (1.618…), sqrt(2) (1.414…), sqrt(½) (0.707…), or ∞ (infinity).';
Blockly.Msg.MATH_IS_EVEN = 'is even';
Blockly.Msg.MATH_IS_ODD = 'is odd';
Blockly.Msg.MATH_IS_PRIME = 'is prime';
Blockly.Msg.MATH_IS_WHOLE = 'is whole';
Blockly.Msg.MATH_IS_POSITIVE = 'is positive';
Blockly.Msg.MATH_IS_NEGATIVE = 'is negative';
Blockly.Msg.MATH_IS_DIVISIBLE_BY = 'is divisible by';
Blockly.Msg.MATH_IS_TOOLTIP = 'Check if a number is an even, odd, prime, whole, positive, negative, or if it is divisible by certain number. Returns true or false.';
Blockly.Msg.MATH_MAP_TITLE = 'transform the value %1 of [from %2 to %3 ] in [from %4 to %5 ]';
Blockly.Msg.MATH_MAP_TOOLTIP = 'transform a value contained in [from min to max] with [from min to max].';
Blockly.Msg.MATH_ROUND_HELPURL = 'https://en.wikipedia.org/wiki/Rounding';
Blockly.Msg.MATH_ROUND_OPERATOR_ROUND = 'round';
Blockly.Msg.MATH_ROUND_OPERATOR_ROUNDDOWN = 'round down';
Blockly.Msg.MATH_ROUND_OPERATOR_ROUNDUP = 'round up';
Blockly.Msg.MATH_ROUND_TOOLTIP = 'Round a number up or down.';
Blockly.Msg.MATH_MODULO_HELPURL = 'https://en.wikipedia.org/wiki/Modulo_operation';
Blockly.Msg.MATH_MODULO_TITLE = 'remainder of %1 ÷ %2';
Blockly.Msg.MATH_MODULO_TOOLTIP = 'Return the remainder from dividing the two numbers.';
Blockly.Msg.MATH_CONSTRAIN_HELPURL = 'https://en.wikipedia.org/wiki/Clamping_(graphics)';
Blockly.Msg.MATH_CONSTRAIN_TITLE = 'constrain %1 low %2 high %3';
Blockly.Msg.MATH_CONSTRAIN_TOOLTIP = 'Constrain a number to be between the specified limits (inclusive).';
Blockly.Msg.MATH_RANDOM_INT_HELPURL = 'https://en.wikipedia.org/wiki/Random_number_generation';
Blockly.Msg.MATH_RANDOM_INT_TITLE = 'random integer from %1 to %2';
Blockly.Msg.MATH_RANDOM_INT_TOOLTIP = 'Return a random integer between the two specified limits, inclusive.';
Blockly.Msg.MATH_RANDOM_FLOAT_HELPURL = 'https://en.wikipedia.org/wiki/Random_number_generation';
Blockly.Msg.MATH_RANDOM_FLOAT_TITLE_RANDOM = 'random fraction';
Blockly.Msg.MATH_RANDOM_FLOAT_TOOLTIP = 'Return a random fraction between 0.0 (inclusive) and 1.0 (exclusive).';
Blockly.Msg.MATH_ATAN2_HELPURL = 'https://en.wikipedia.org/wiki/Atan2';
Blockly.Msg.MATH_ATAN2_TITLE = 'atan2 of X:%1 Y:%2';
Blockly.Msg.MATH_ATAN2_TOOLTIP = 'Return the arctangent of point (X, Y) in degrees from -180 to 180.';

// Text blocks.
Blockly.Msg.TEXT_TEXT_HELPURL = 'https://en.wikipedia.org/wiki/String_(computer_science)';
Blockly.Msg.TEXT_TEXT_TOOLTIP = 'A letter, word, or line of text.';
Blockly.Msg.TEXT_JOIN_HELPURL = 'https://github.com/google/blockly/wiki/Text#text-creation';
Blockly.Msg.TEXT_JOIN_TITLE_CREATEWITH = 'create text with';
Blockly.Msg.TEXT_JOIN_TOOLTIP = 'Create a piece of text by joining together any number of items.';
Blockly.Msg.TEXT_APPEND_HELPURL = 'https://github.com/google/blockly/wiki/Text#text-modification';
Blockly.Msg.TEXT_APPEND_TITLE = 'to %1 append text %2';
Blockly.Msg.TEXT_APPEND_VARIABLE = Blockly.Msg.VARIABLES_DEFAULT_NAME;
Blockly.Msg.TEXT_APPEND_TOOLTIP = 'Append some text to variable \'%1\'.';
Blockly.Msg.TEXT_LENGTH_HELPURL = 'https://github.com/google/blockly/wiki/Text#text-modification';
Blockly.Msg.TEXT_LENGTH_TITLE = 'length of %1';
Blockly.Msg.TEXT_LENGTH_TOOLTIP = 'Returns the number of letters (including spaces) in the provided text.';
Blockly.Msg.TEXT_ISEMPTY_HELPURL = 'https://github.com/google/blockly/wiki/Text#checking-for-empty-text';
Blockly.Msg.TEXT_ISEMPTY_TITLE = '%1 is empty';
Blockly.Msg.TEXT_ISEMPTY_TOOLTIP = 'Returns true if the provided text is empty.';
Blockly.Msg.TEXT_INDEXOF_HELPURL = 'https://github.com/google/blockly/wiki/Text#finding-text';
Blockly.Msg.TEXT_INDEXOF_OPERATOR_FIRST = 'find first occurrence of text';
Blockly.Msg.TEXT_INDEXOF_OPERATOR_LAST = 'find last occurrence of text';
Blockly.Msg.TEXT_INDEXOF_TITLE = 'in text %1 %2 %3';
Blockly.Msg.TEXT_INDEXOF_TOOLTIP = 'Returns the index of the first/last occurrence of the first text in the second text. Returns %1 if text is not found.';
Blockly.Msg.TEXT_CHARAT_HELPURL = 'https://github.com/google/blockly/wiki/Text#extracting-text';
Blockly.Msg.TEXT_CHARAT_TITLE = 'in text %1 %2';
Blockly.Msg.TEXT_CHARAT_FIRST = 'get first letter';
Blockly.Msg.TEXT_CHARAT_LAST = 'get last letter';
Blockly.Msg.TEXT_CHARAT_FROM_START = 'get letter at index';
Blockly.Msg.TEXT_CHARAT_FROM_END = 'get letter at index (from end)';
Blockly.Msg.TEXT_CHARAT_RANDOM = 'get random letter';
Blockly.Msg.TEXT_CHARAT_TAIL = '';
Blockly.Msg.TEXT_CHARAT_TOOLTIP = 'Return the letter at the specified position.';
Blockly.Msg.TEXT_GET_SUBSTRING_HELPURL = 'https://github.com/google/blockly/wiki/Text#extracting-a-region-of-text';
Blockly.Msg.TEXT_GET_SUBSTRING_START_FIRST = 'get substring from first letter';
Blockly.Msg.TEXT_GET_SUBSTRING_START_FROM_START = 'get substring from letter at index';
Blockly.Msg.TEXT_GET_SUBSTRING_START_FROM_END = 'get substring from letter at index (from end)';
Blockly.Msg.TEXT_GET_SUBSTRING_END_LAST = 'to last letter';
Blockly.Msg.TEXT_GET_SUBSTRING_END_FROM_START = 'to letter at index';
Blockly.Msg.TEXT_GET_SUBSTRING_END_FROM_END = 'to letter at index (from end)';
Blockly.Msg.TEXT_GET_SUBSTRING_INPUT_IN_TEXT = 'in text';
Blockly.Msg.TEXT_GET_SUBSTRING_TAIL = '';
Blockly.Msg.TEXT_GET_SUBSTRING_TOOLTIP = 'Returns a specified portion of the text.';
Blockly.Msg.TEXT_CHANGECASE_HELPURL = 'https://github.com/google/blockly/wiki/Text#adjusting-text-case';
Blockly.Msg.TEXT_CHANGECASE_TITLE = 'set %1 to %2';
Blockly.Msg.TEXT_CHANGECASE_OPERATOR_LOWERCASE = 'lower case';
Blockly.Msg.TEXT_CHANGECASE_OPERATOR_TITLECASE = 'Title Case';
Blockly.Msg.TEXT_CHANGECASE_OPERATOR_UPPERCASE = 'UPPER CASE';
Blockly.Msg.TEXT_CHANGECASE_TOOLTIP = 'Return a copy of the text in a different case.';
Blockly.Msg.TEXT_TRIM_HELPURL = 'https://github.com/google/blockly/wiki/Text#trimming-removing-spaces';
Blockly.Msg.TEXT_TRIM_TITLE = 'trim spaces from %1 of %2';
Blockly.Msg.TEXT_TRIM_OPERATOR_BOTH = 'both sides';
Blockly.Msg.TEXT_TRIM_OPERATOR_LEFT = 'left side';
Blockly.Msg.TEXT_TRIM_OPERATOR_RIGHT = 'right side';
Blockly.Msg.TEXT_TRIM_TOOLTIP = 'Return a copy of the text with spaces removed from one or both ends.';
Blockly.Msg.TEXT_COUNT_HELPURL = 'https://github.com/google/blockly/wiki/Text#counting-substrings';
Blockly.Msg.TEXT_COUNT_TITLE = 'count %1 in %2';
Blockly.Msg.TEXT_COUNT_TOOLTIP = 'Count how many times some text occurs within some other text.';
Blockly.Msg.TEXT_REPLACE_HELPURL = 'https://github.com/google/blockly/wiki/Text#replacing-substrings';
Blockly.Msg.TEXT_REPLACE_TITLE = 'replace %1 with %2 in %3';
Blockly.Msg.TEXT_REPLACE_TOOLTIP = 'Replace all occurances of some text within some other text.';
Blockly.Msg.TEXT_REVERSE_HELPURL = 'https://github.com/google/blockly/wiki/Text#reversing-text';
Blockly.Msg.TEXT_REVERSE_TITLE = 'reverse %1';
Blockly.Msg.TEXT_REVERSE_TOOLTIP = 'Reverses the order of the characters in the text.';
Blockly.Msg.TEXT_COMMENT_TITLE = 'Comment %1';
Blockly.Msg.TEXT_COMMENT_TOOLTIP = 'Add comment in code.';
Blockly.Msg.TEXT_PRINT_HELPURL = 'https://github.com/google/blockly/wiki/Text#printing-text';
Blockly.Msg.TEXT_PRINT_TITLE = 'print %1';
Blockly.Msg.TEXT_PRINT_TOOLTIP = 'Print the specified text, number or other value.';
Blockly.Msg.TEXT_PROMPT_HELPURL = 'https://github.com/google/blockly/wiki/Text#getting-input-from-the-user';
Blockly.Msg.TEXT_PROMPT_TOOLTIP_NUMBER = 'Prompt for user for a number.';
Blockly.Msg.TEXT_PROMPT_TOOLTIP_TEXT = 'Prompt for user for some text.';
Blockly.Msg.TEXT_PROMPT_TYPE_NUMBER = 'prompt for number with message';
Blockly.Msg.TEXT_PROMPT_TYPE_TEXT = 'prompt for text with message';

// Variables blocks.
Blockly.Msg.VARIABLES_SET_HELPURL = 'https://github.com/google/blockly/wiki/Variables#set';
Blockly.Msg.VARIABLES_SET = 'set %1 to %2';
Blockly.Msg.VARIABLES_SET_CREATE_GET = 'Create \'get %1\'';
Blockly.Msg.VARIABLES_SET_TOOLTIP = 'Sets this variable to be equal to the input.';
Blockly.Msg.VARIABLES_GET_HELPURL = 'https://github.com/google/blockly/wiki/Variables#get';
Blockly.Msg.VARIABLES_GET_CREATE_SET = 'Create \'set %1\'';
Blockly.Msg.VARIABLES_GET_TOOLTIP = 'Returns the value of this variable.';
Blockly.Msg.VARIABLES_INCREMENT_HELPURL = 'https://en.wikipedia.org/wiki/Programming_idiom#Incrementing_a_counter';
Blockly.Msg.VARIABLES_INCREMENT_TITLE = 'change %1 by %2';
Blockly.Msg.VARIABLES_INCREMENT_TOOLTIP = 'Add a number to this variable.';
Blockly.Msg.VARIABLES_FORCE_TYPE_TITLE = 'convert %1 to %2';
Blockly.Msg.VARIABLES_FORCE_TYPE_TEXT = 'text (str)';
Blockly.Msg.VARIABLES_FORCE_TYPE_BOOLEAN = 'boolean (bool)';
Blockly.Msg.VARIABLES_FORCE_TYPE_INTEGER = 'integer (int)';
Blockly.Msg.VARIABLES_FORCE_TYPE_FLOAT = 'float (float)';
Blockly.Msg.VARIABLES_FORCE_TYPE_LONG = 'long (long)';
Blockly.Msg.VARIABLES_FORCE_TYPE_TOOLTIP = 'Enable to convert any variable in choosen type.';
Blockly.Msg.VARIABLES_TYPEOF_TITLE = 'type of %1';
Blockly.Msg.VARIABLES_TYPEOF_TOOLTIP = 'Return the type of this variable.';

// Lists blocks.
Blockly.Msg.LISTS_CREATE_EMPTY_HELPURL = 'https://github.com/google/blockly/wiki/Lists#create-empty-list';
Blockly.Msg.LISTS_CREATE_EMPTY_TITLE = 'empty list';
Blockly.Msg.LISTS_CREATE_EMPTY_TOOLTIP = 'Returns a list, of length 0, containing no data records';
Blockly.Msg.LISTS_CREATE_WITH_HELPURL = 'https://github.com/google/blockly/wiki/Lists#create-list-with';
Blockly.Msg.LISTS_CREATE_WITH_INPUT_WITH = 'list with items';
Blockly.Msg.LISTS_CREATE_WITH_TOOLTIP = 'list with any number of items.';
Blockly.Msg.LISTS_REPEAT_HELPURL = 'https://github.com/google/blockly/wiki/Lists#create-list-with';
Blockly.Msg.LISTS_REPEAT_TITLE = 'list with item %1 repeated %2 times';
Blockly.Msg.LISTS_REPEAT_TOOLTIP = 'Creates a list consisting of the given value repeated the specified number of times.';
Blockly.Msg.LISTS_LENGTH_HELPURL = 'https://github.com/google/blockly/wiki/Lists#length-of';
Blockly.Msg.LISTS_LENGTH_TITLE = 'length of %1';
Blockly.Msg.LISTS_LENGTH_TOOLTIP = 'Returns the length of a list.';
Blockly.Msg.MATH_ONLIST_HELPURL = '';
Blockly.Msg.MATH_ONLIST_OPERATOR_AVERAGE = 'average of list';
Blockly.Msg.MATH_ONLIST_OPERATOR_MAX = 'max of list';
Blockly.Msg.MATH_ONLIST_OPERATOR_MEDIAN = 'median of list';
Blockly.Msg.MATH_ONLIST_OPERATOR_MIN = 'min of list';
Blockly.Msg.MATH_ONLIST_OPERATOR_MODE = 'modes of list';
Blockly.Msg.MATH_ONLIST_OPERATOR_RANDOM = 'random item of list';
Blockly.Msg.MATH_ONLIST_OPERATOR_STD_DEV = 'standard deviation of list';
Blockly.Msg.MATH_ONLIST_OPERATOR_SUM = 'sum of list';
Blockly.Msg.MATH_ONLIST_TOOLTIP_AVERAGE = 'Return the average (arithmetic mean) of the numeric values in the list.';
Blockly.Msg.MATH_ONLIST_TOOLTIP_MAX = 'Return the largest number in the list.';
Blockly.Msg.MATH_ONLIST_TOOLTIP_MEDIAN = 'Return the median number in the list.';
Blockly.Msg.MATH_ONLIST_TOOLTIP_MIN = 'Return the smallest number in the list.';
Blockly.Msg.MATH_ONLIST_TOOLTIP_MODE = 'Return a list of the most common item(s) in the list.';
Blockly.Msg.MATH_ONLIST_TOOLTIP_RANDOM = 'Return a random item from the list.';
Blockly.Msg.MATH_ONLIST_TOOLTIP_STD_DEV = 'Return the standard deviation of the list.';
Blockly.Msg.MATH_ONLIST_TOOLTIP_SUM = 'Return the sum of all the numbers in the list.';
Blockly.Msg.LISTS_ISEMPTY_HELPURL = 'https://github.com/google/blockly/wiki/Lists#is-empty';
Blockly.Msg.LISTS_ISEMPTY_TITLE = '%1 is empty';
Blockly.Msg.LISTS_ISEMPTY_TOOLTIP = 'Returns true if the list is empty.';
Blockly.Msg.LISTS_REVERSE_HELPURL = 'https://github.com/google/blockly/wiki/Lists#reversing-a-list';
Blockly.Msg.LISTS_REVERSE_TITLE = 'reverse list %1';
Blockly.Msg.LISTS_REVERSE_TOOLTIP = 'Reverse a copy of a list.';
Blockly.Msg.LISTS_INLIST = 'in list';
Blockly.Msg.LISTS_INDEX_OF_HELPURL = 'https://github.com/google/blockly/wiki/Lists#getting-items-from-a-list';
Blockly.Msg.LISTS_INDEX_OF_FIRST = 'find first occurrence of item';
Blockly.Msg.LISTS_INDEX_OF_LAST = 'find last occurrence of item';
Blockly.Msg.LISTS_INDEX_OF_INPUT_IN_LIST = Blockly.Msg.LISTS_INLIST;
Blockly.Msg.LISTS_INDEX_OF_TOOLTIP = 'Returns the index of the first/last occurrence of the item in the list. Returns %1 if item is not found.';
Blockly.Msg.LISTS_GET_INDEX_HELPURL = Blockly.Msg.LISTS_INDEX_OF_HELPURL;
Blockly.Msg.LISTS_GET_INDEX_GET = 'get';
Blockly.Msg.LISTS_GET_INDEX_GET_REMOVE = 'get and remove';
Blockly.Msg.LISTS_GET_INDEX_REMOVE = 'remove';
Blockly.Msg.LISTS_GET_INDEX_FIRST = 'first item';
Blockly.Msg.LISTS_GET_INDEX_LAST = 'last item';
Blockly.Msg.LISTS_GET_INDEX_FROM_START = 'item at index';
Blockly.Msg.LISTS_GET_INDEX_FROM_END = 'item at index (from end)';
Blockly.Msg.LISTS_GET_INDEX_RANDOM = 'random item';
Blockly.Msg.LISTS_GET_INDEX_TAIL = '';
Blockly.Msg.LISTS_GET_INDEX_INPUT_IN_LIST = Blockly.Msg.LISTS_INLIST;
Blockly.Msg.LISTS_INDEX_FROM_START_TOOLTIP = '%1 is the first item.';
Blockly.Msg.LISTS_INDEX_FROM_END_TOOLTIP = '%1 is the last item.';
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_FIRST = 'Returns the first item in a list.';
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_FROM = 'Returns the item at the specified position in a list.';
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_LAST = 'Returns the last item in a list.';
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_RANDOM = 'Returns a random item in a list.';
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_REMOVE_FIRST = 'Removes and returns the first item in a list.';
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_REMOVE_FROM = 'Removes and returns the item at the specified position in a list.';
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_REMOVE_LAST = 'Removes and returns the last item in a list.';
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_GET_REMOVE_RANDOM = 'Removes and returns a random item in a list.';
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_REMOVE_FIRST = 'Removes the first item in a list.';
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_REMOVE_FROM = 'Removes the item at the specified position in a list.';
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_REMOVE_LAST = 'Removes the last item in a list.';
Blockly.Msg.LISTS_GET_INDEX_TOOLTIP_REMOVE_RANDOM = 'Removes a random item in a list.';
Blockly.Msg.LISTS_GET_SUBLIST_END_FROM_END = 'to item at index (from end)';
Blockly.Msg.LISTS_GET_SUBLIST_END_FROM_START = 'to item at index';
Blockly.Msg.LISTS_GET_SUBLIST_END_LAST = 'to last item';
Blockly.Msg.LISTS_GET_SUBLIST_HELPURL = 'https://github.com/google/blockly/wiki/Lists#getting-a-sublist';
Blockly.Msg.LISTS_GET_SUBLIST_START_FIRST = 'get sub-list from first item';
Blockly.Msg.LISTS_GET_SUBLIST_START_FROM_END = 'get sub-list from item at index (from end)';
Blockly.Msg.LISTS_GET_SUBLIST_START_FROM_START = 'get sub-list from item at index';
Blockly.Msg.LISTS_GET_SUBLIST_TAIL = '';
Blockly.Msg.LISTS_GET_SUBLIST_INPUT_IN_LIST = Blockly.Msg.LISTS_INLIST;
Blockly.Msg.LISTS_GET_SUBLIST_TOOLTIP = 'Creates a copy of the specified portion of a list.';
Blockly.Msg.LISTS_SET_INDEX_HELPURL = 'https://github.com/google/blockly/wiki/Lists#in-list--set';
Blockly.Msg.LISTS_SET_INDEX_INPUT_TO = 'as';
Blockly.Msg.LISTS_SET_INDEX_INSERT = 'insert at';
Blockly.Msg.LISTS_SET_INDEX_SET = 'set';
Blockly.Msg.LISTS_SET_INDEX_INPUT_IN_LIST = Blockly.Msg.LISTS_INLIST;
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_INSERT_FIRST = 'Inserts the item at the start of a list.';
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_INSERT_FROM = 'Inserts the item at the specified position in a list.';
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_INSERT_LAST = 'Append the item to the end of a list.';
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_INSERT_RANDOM = 'Inserts the item randomly in a list.';
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_SET_FIRST = 'Sets the first item in a list.';
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_SET_FROM = 'Sets the item at the specified position in a list.';
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_SET_LAST = 'Sets the last item in a list.';
Blockly.Msg.LISTS_SET_INDEX_TOOLTIP_SET_RANDOM = 'Sets a random item in a list.';
Blockly.Msg.LISTS_SPLIT_HELPURL = 'https://github.com/google/blockly/wiki/Lists#splitting-strings-and-joining-lists';
Blockly.Msg.LISTS_SPLIT_LIST_FROM_TEXT = 'make list from text';
Blockly.Msg.LISTS_SPLIT_TEXT_FROM_LIST = 'make text from list';
Blockly.Msg.LISTS_SPLIT_TOOLTIP_JOIN = 'Join a list of texts into one text, separated by a delimiter.';
Blockly.Msg.LISTS_SPLIT_TOOLTIP_SPLIT = 'Split text into a list of texts, breaking at each delimiter.';
Blockly.Msg.LISTS_SPLIT_WITH_DELIMITER = 'with delimiter';
Blockly.Msg.LISTS_SORT_HELPURL = 'https://github.com/google/blockly/wiki/Lists#sorting-a-list';
Blockly.Msg.LISTS_SORT_ORDER_ASCENDING = 'ascending';
Blockly.Msg.LISTS_SORT_ORDER_DESCENDING = 'descending';
Blockly.Msg.LISTS_SORT_TITLE = 'sort %1 %2 %3';
Blockly.Msg.LISTS_SORT_TOOLTIP = 'Sort a copy of a list.';
Blockly.Msg.LISTS_SORT_TYPE_IGNORECASE = 'alphabetic, ignore case';
Blockly.Msg.LISTS_SORT_TYPE_NUMERIC = 'numeric';
Blockly.Msg.LISTS_SORT_TYPE_TEXT = 'alphabetic';

// Procedures blocks.
Blockly.Msg.PROCEDURES_DEFNORETURN_HELPURL = 'https://en.wikipedia.org/wiki/Subroutine';
Blockly.Msg.PROCEDURES_DEFNORETURN_TITLE = 'define';
Blockly.Msg.PROCEDURES_DEFNORETURN_PROCEDURE = 'function_name';
Blockly.Msg.PROCEDURES_BEFORE_PARAMS = 'with:';
Blockly.Msg.PROCEDURES_CALL_BEFORE_PARAMS = Blockly.Msg.PROCEDURES_BEFORE_PARAMS;
Blockly.Msg.PROCEDURES_DEFNORETURN_DO = '';
Blockly.Msg.PROCEDURES_DEFNORETURN_TOOLTIP = 'Creates a function with no output.';
Blockly.Msg.PROCEDURES_DEFNORETURN_COMMENT = 'Describe this function...';
Blockly.Msg.PROCEDURES_DEFRETURN_HELPURL = 'https://en.wikipedia.org/wiki/Subroutine';
Blockly.Msg.PROCEDURES_DEFRETURN_TITLE = Blockly.Msg.PROCEDURES_DEFNORETURN_TITLE;
Blockly.Msg.PROCEDURES_DEFRETURN_PROCEDURE = Blockly.Msg.PROCEDURES_DEFNORETURN_PROCEDURE;
Blockly.Msg.PROCEDURES_DEFRETURN_DO = Blockly.Msg.PROCEDURES_DEFNORETURN_DO;
Blockly.Msg.PROCEDURES_DEFRETURN_COMMENT = Blockly.Msg.PROCEDURES_DEFNORETURN_COMMENT;
Blockly.Msg.PROCEDURES_DEFRETURN_RETURN = 'return';
Blockly.Msg.PROCEDURES_DEFRETURN_TOOLTIP = 'Creates a function with an output.';
Blockly.Msg.PROCEDURES_ALLOW_STATEMENTS = 'allow statements';
Blockly.Msg.PROCEDURES_DEF_DUPLICATE_WARNING = 'Warning: This function has duplicate parameters.';
Blockly.Msg.PROCEDURES_CALLNORETURN_HELPURL = 'https://en.wikipedia.org/wiki/Subroutine';
Blockly.Msg.PROCEDURES_CALLNORETURN_TOOLTIP = 'Run the user-defined function \'%1\'.';
Blockly.Msg.PROCEDURES_CALLRETURN_HELPURL = 'https://en.wikipedia.org/wiki/Subroutine';
Blockly.Msg.PROCEDURES_CALLRETURN_TOOLTIP = 'Run the user-defined function \'%1\' and use its output.';
Blockly.Msg.PROCEDURES_HIGHLIGHT_DEF = 'Highlight function definition';
Blockly.Msg.PROCEDURES_CREATE_DO = 'Create \'%1\'';
Blockly.Msg.PROCEDURES_IFRETURN_TITLE = 'if';
Blockly.Msg.PROCEDURES_IFRETURN_HELPURL = 'http://c2.com/cgi/wiki?GuardClause';
Blockly.Msg.PROCEDURES_IFRETURN_TOOLTIP = 'If a value is true, then return a second value.';
Blockly.Msg.PROCEDURES_IFRETURN_WARNING = 'Warning: This block may be used only within a function definition.';

// Display - Micro:bit 
Blockly.Msg.SHOW_LEDS_TITLE = 'show leds';
Blockly.Msg.SHOW_STRING_TITLE = 'show string';
Blockly.Msg.SHOW_NUMBER_TITLE = 'show number';
Blockly.Msg.SHOW_ICON_TITLE = 'show icon';
Blockly.Msg.SHOW_GAUGE_TITLE = 'show gauge of %1 Maximum %2';
Blockly.Msg.SHOW_GAUGE_TOOLTIP = 'Display the dipstick of a sensor (or number) on the micro-bit card screen by setting the maximum value corresponding to the filled dipstick.';
Blockly.Msg.SET_PIXEL_TITLE = 'control led x %1 y %2 to state %3';
Blockly.Msg.SET_PIXEL_TOOLTIP = 'Enable to control the state (ON/OFF) of each micro:bit screen LED.';
Blockly.Msg.SET_LIGHT_PIXEL_TITLE = 'control led x %1 y %2 light %3';
Blockly.Msg.SET_LIGHT_PIXEL_TOOLTIP = 'Enable to control the light (from 0 to 9) of each micro:bit screen LED.';
Blockly.Msg.SHOW_CLOCK_TITLE = 'show clock';
Blockly.Msg.SHOW_ARROW_TITLE = 'show arrow';
Blockly.Msg.SHOW_ARROW_TOOLTIP = 'Enable to display arrows of 8 directions (N,NE,E,SE,S...) on micro:bit screen.';
Blockly.Msg.CLEAR_TITLE = 'clear screen';
// Display - Screen
Blockly.Msg.DISPLAY_LCD_SETTEXT_TITLE = '[LCD1602] show text %1 on line %2';
Blockly.Msg.DISPLAY_LCD_SETTEXT_TOOLTIP = IMG_MODULE_LCD_3V3 + Blockly.Tooltip.SEP + 'Show text on the grove lcd 1602 display. Connect lcd on I2C port.';
Blockly.Msg.DISPLAY_LCD_CLEAR_TITLE = '[LCD1602] clear display';
Blockly.Msg.DISPLAY_LCD_CLEAR_TOOLTIP = IMG_MODULE_LCD_3V3 + Blockly.Tooltip.SEP + 'Enable to clear the entire lcd text. Connect lcd on I2C port.';
Blockly.Msg.DISPLAY_OLED_ADDTEXT_TITLE = '[OLED display] show text %1 at position x %2 y %3';
Blockly.Msg.DISPLAY_OLED_ADDTEXT_TOOLTIP = IMG_MODULE_OLED + Blockly.Tooltip.SEP + 'Enable write text on OLED display. Connect the OLED display on I2C port.';
Blockly.Msg.DISPLAY_OLED_SETPIXEL_TITLE = '[OLED display] control pixel x %1 y %2 to state %3';
Blockly.Msg.DISPLAY_OLED_SETPIXEL_TOOLTIP = IMG_MODULE_OLED + Blockly.Tooltip.SEP + 'Enable to control each OLED display pixel. Connect the OLED display on I2C port.';
Blockly.Msg.DISPLAY_OLED_CLEARSCREEN_TITLE = '[OLED display] clear screen';
Blockly.Msg.DISPLAY_OLED_CLEARSCREEN_TOOLTIP = IMG_MODULE_OLED + Blockly.Tooltip.SEP + 'Enable to clear the entire grove oled screen. Connect the OLED display on I2C port.';
Blockly.Msg.DISPLAY_OLED_DRAWICON_TITLE = '[OLED display] show icon %1 on x %2 y %3 to state %4';
Blockly.Msg.DISPLAY_OLED_DRAWICON_TOOLTIP = IMG_MODULE_OLED + Blockly.Tooltip.SEP + 'Enable show micro:bit icon from Image library. Connect the OLED display on I2C port.';
// Display - LED modules
Blockly.Msg.DISPLAY_SETGROVELED_TITLE = '[LED module] control LED to state %1 on pin %2';

Blockly.Msg.DISPLAY_SETGROVELED_TOOLTIP = IMG_MODULE_LED + Blockly.Tooltip.SEP + 'Enable to switch on or switch off the LED socket kit Grove (0 or 1) on digitals pins P0 up through P20.';
Blockly.Msg.DISPLAY_SETLEDINTENSITY_TITLE = '[LED] set LED intensity to %1 on pin %2';
Blockly.Msg.DISPLAY_SETLEDINTENSITY_TOOLTIP = IMG_MODULE_LED_PWM + Blockly.Tooltip.SEP + 'Enable to set the LED intensity from 0 to 255 on PWM pins.';
Blockly.Msg.DISPLAY_NEOPIXEL_DEFINE_TITLE = '[Neopixel] define %1 LED on pin %2';
Blockly.Msg.DISPLAY_NEOPIXEL_DEFINE_TOOLTIP = IMG_MODULE_NEOPIXEL + Blockly.Tooltip.SEP + 'Enable to define LED number of neopixel. This block have to be used in setup.';
Blockly.Msg.DISPLAY_NEOPIXEL_LEDCONTROL_TITLE = '[Neopixel] set LED %1 to R %2 G %3 B %4 on pin %5';
Blockly.Msg.DISPLAY_NEOPIXEL_LEDCONTROL_TOOLTIP = IMG_MODULE_NEOPIXEL + Blockly.Tooltip.SEP + 'Enable to control each LED color of neopixels as (R,G,B) from 0 to 255. Use P15 to set Maqueen neopixel.';
Blockly.Msg.DISPLAY_NEOPIXEL_SETPALETTECOLOR_TITLE = '[Neopixel] set LED %1 to %2 on pin %3';
Blockly.Msg.DISPLAY_NEOPIXEL_SETPALETTECOLOR_TOOLTIP = IMG_MODULE_NEOPIXEL + Blockly.Tooltip.SEP + 'Enable to control each LED color of neopixel. Use P15 to set Maqueen neopixel.';
Blockly.Msg.DISPLAY_NEOPIXEL_SETALLLEDRGB_TITLE = '[Neopixel] set all LED to colour R %1 G %2 B %3 on pin %4';
Blockly.Msg.DISPLAY_NEOPIXEL_SETALLLEDRGB_TOOLTIP = IMG_MODULE_NEOPIXEL + Blockly.Tooltip.SEP + 'Enable to control all LED of neopixel to the choosed colour value as (R,G,B) from 0 to 255. Use P15 to set Maqueen neopixel.';
Blockly.Msg.DISPLAY_NEOPIXEL_SETALLLEDCOLOR_TITLE = '[Neopixel] set all LED to colour %1 on pin %2';
Blockly.Msg.DISPLAY_NEOPIXEL_SETALLLEDCOLOR_TOOLTIP = IMG_MODULE_NEOPIXEL + Blockly.Tooltip.SEP + 'Enable to control all LED of neopixel to the choosed colour value. Use P15 to set Maqueen neopixel.';
Blockly.Msg.DISPLAY_NEOPIXEL_RAINBOW_TITLE = '[Neopixel] set a rainbow on pin %1';
Blockly.Msg.DISPLAY_NEOPIXEL_RAINBOW_TOOLTIP = IMG_MODULE_NEOPIXEL + Blockly.Tooltip.SEP + 'Enable to show a rainbow on neopixel module, set pin and the number of LED.';
Blockly.Msg.DISPLAY_4DIGIT_SETNUMBER_TITLE = '[4-Digit module] show %1 %2 on pins CLK %3 DIO %4';
Blockly.Msg.DISPLAY_4DIGIT_SETNUMBER_TOOLTIP = IMG_MODULE_4DIGITDISPLAY + Blockly.Tooltip.SEP + 'Enable to show numbers or temperature on grove 4-digit display on digital pins from P0 up through P20.';
Blockly.Msg.DISPLAY_4DIGIT_SETCLOCK_TITLE = '[4-Digit module] show clock on pins CLK %1 DIO %2';
Blockly.Msg.DISPLAY_4DIGIT_SETCLOCK_TOOLTIP = IMG_MODULE_4DIGITDISPLAY + Blockly.Tooltip.SEP + 'Enable to show clock on grove 4-digit display on digital pins from P0 up through P20. Warning, getting real clock is possible only if microbit stay in power on mode.';
Blockly.Msg.DISPLAY_4DIGIT_NUMBER = 'number';
Blockly.Msg.DISPLAY_4DIGIT_TEMPERATURE = 'temperature';
Blockly.Msg.DISPLAY_LEDBARSETLEVEL_TITLE = '[LED Bar module] set level of %1 on pins DI %2 DCKI %3';
Blockly.Msg.DISPLAY_LEDBARSETLEVEL_TOOLTIP = IMG_MODULE_LED_BAR + Blockly.Tooltip.SEP + 'Enable to show level of input value on grove LED bar display on digital pins from P0 up through P20.';
Blockly.Msg.DISPLAY_TRAFFICLIGHT_SETLED_TITLE = '[Traffic Light] set %1 LED to state %2';
Blockly.Msg.DISPLAY_TRAFFICLIGHT_SETLED_TOOLTIP = IMG_MODULE_TRAFFIC_LIGHT + Blockly.Tooltip.SEP + 'Allows you to control Kitronik Traffic Light.';
Blockly.Msg.DISPLAY_TRAFFICLIGHT_RED = 'red';
Blockly.Msg.DISPLAY_TRAFFICLIGHT_ORANGE = 'orange';
Blockly.Msg.DISPLAY_TRAFFICLIGHT_GREEN = 'green';
// Display - Morpion
Blockly.Msg.DISPLAY_MORPION_NEWGAME_TITLE = '[OLED display] Tic Tac Toe - new game';
Blockly.Msg.DISPLAY_MORPION_NEWGAME_TOOLTIP = IMG_MODULE_OLED + Blockly.Tooltip.SEP + 'Enable to start new game of Tic Tac Toe on OLED display. Connect the OLED display on I2C port. Warning, the game uses all micro:bit memory, you cannot connect any other device during playing Tic Tac Toe.';
Blockly.Msg.DISPLAY_MORPION_MOVECURSOR_TITLE = '[OLED display] Tic Tac Toe - move cursor';
Blockly.Msg.DISPLAY_MORPION_MOVECURSOR_TOOLTIP = IMG_MODULE_OLED + Blockly.Tooltip.SEP + 'Enable to move cursor of Tic Tac Toe grid on OLED display. The cursor goes through the grid box by box. Connect the OLED display on I2C port. Warning, the game uses all micro:bit memory, you cannot connect any other device during playing Tic Tac Toe.';
Blockly.Msg.DISPLAY_MORPION_SETPLAYERFIGURE_TITLE = '[OLED display] Tic Tac Toe - add %1';
Blockly.Msg.DISPLAY_MORPION_SETPLAYERFIGURE_TOOLTIP = IMG_MODULE_OLED + Blockly.Tooltip.SEP + 'Enable to add figure player (Cross or Circle) in Tic Tac Toe grid on OLED display. Connect the OLED display on I2C port. Warning, the game uses all micro:bit memory, you cannot connect any other device during playing Tic Tac Toe.';
Blockly.Msg.DISPLAY_MORPION_CROSS = 'cross';
Blockly.Msg.DISPLAY_MORPION_CIRCLE = 'circle';
Blockly.Msg.DISPLAY_MORPION_ISENDGAME_TITLE = '[OLED display] Tic Tac Toe - is end game ?';
Blockly.Msg.DISPLAY_MORPION_ISENDGAME_TOOLTIP = IMG_MODULE_OLED + Blockly.Tooltip.SEP + 'Enable to return Tic Tac Toe game curent state. Connect the OLED display on I2C port. Warning, the game uses all micro:bit memory, you cannot connect any other device during playing Tic Tac Toe.';
// Display - Games
Blockly.Msg.DISPLAY_GAMES_LEFT = 'left';
Blockly.Msg.DISPLAY_GAMES_RIGHT = 'right';
Blockly.Msg.DISPLAY_GAMES_UP = 'up';
Blockly.Msg.DISPLAY_GAMES_DOWN = 'down';
Blockly.Msg.DISPLAY_GAMES_CREATESPRITE_TITLE = 'create sprite at x %1 y %2';
Blockly.Msg.DISPLAY_GAMES_CREATESPRITE_TOOLTIP = 'Create a sprite on micro:bit screen.';
Blockly.Msg.DISPLAY_GAMES_DELETESPRITE_TITLE = 'delete %1';
Blockly.Msg.DISPLAY_GAMES_DELETESPRITE_TOOLTIP = 'Deleted the selected sprite.';
Blockly.Msg.DISPLAY_GAMES_ISSPRITEDELETED_TITLE = 'is %1 deleted ?';
Blockly.Msg.DISPLAY_GAMES_ISSPRITEDELETED_TOOLTIP = 'Return True if selected sprite is deleted.';
Blockly.Msg.DISPLAY_GAMES_MOVESPRITE_TITLE = 'move %1 by %2 to %3';
Blockly.Msg.DISPLAY_GAMES_MOVESPRITE_TOOLTIP = 'Move selected sprite by choosen step number and direction.';
Blockly.Msg.DISPLAY_GAMES_GETSPRITEPOSITION_TITLE = 'position %1 %2';
Blockly.Msg.DISPLAY_GAMES_GETSPRITEPOSITION_TOOLTIP = 'Get slected sprite position.';
Blockly.Msg.DISPLAY_GAMES_CHANGESCORE_TITLE = 'change score by %1';
Blockly.Msg.DISPLAY_GAMES_CHANGESCORE_TOOLTIP = 'Increment the score of game.';
Blockly.Msg.DISPLAY_GAMES_GETSCORE_TITLE = 'score';
Blockly.Msg.DISPLAY_GAMES_GETSCORE_TOOLTIP = 'Return the score of game.';
Blockly.Msg.DISPLAY_GAMES_STOPGAME_TITLE = 'stop game';
Blockly.Msg.DISPLAY_GAMES_STOPGAME_TOOLTIP = 'Stop the game.';
Blockly.Msg.DISPLAY_GAMES_ISENDGAME_TITLE = 'is end of game';
Blockly.Msg.DISPLAY_GAMES_ISENDGAME_TOOLTIP = 'Return True if game is stopped.';
Blockly.Msg.DISPLAY_GAMES_RESTARTGAME_TITLE = 'restart game';
Blockly.Msg.DISPLAY_GAMES_RESTARTGAME_TOOLTIP = 'Restart the game.';

// Input/Output - Micro:bit
Blockly.Msg.IO_PAUSE_TITLE = 'wait %1 %2';
Blockly.Msg.IO_PAUSE_TOOLTIP = 'Stop the code execution (duration in seconds or milliseconds).';
Blockly.Msg.IO_PAUSE_SECOND = 'second(s)';
Blockly.Msg.IO_PAUSE_MILLISECOND = 'millisecond(s)';
Blockly.Msg.IO_INITCHRONOMETER_TITLE = 'Initialize the chronometer';
Blockly.Msg.IO_INITCHRONOMETER_TOOLTIP = 'Allows you to initialize the chronometer (in seconds).';
Blockly.Msg.IO_GETCHRONOMETER_TITLE = 'get chronometer in %1';
Blockly.Msg.IO_GETCHRONOMETER_TOOLTIP = 'Return the chronometer value from the initialization in seconds or milliseconds.';
Blockly.Msg.IO_IFBUTTONPRESSED_TITLE = 'if button %1 %2 pressed do';
Blockly.Msg.IO_IFBUTTONPRESSED_TOOLTIP = 'Execute instructions if the choosen button (A or B) is pressed.';

Blockly.Msg.IO_TOUCH_NORTH = 'up';
Blockly.Msg.IO_TOUCH_SOUTH = 'down';
Blockly.Msg.IO_TOUCH_EST = 'right';
Blockly.Msg.IO_TOUCH_WEST = 'left';

Blockly.Msg.IO_ISPRESSED = 'is';
Blockly.Msg.IO_WASPRESSED = 'was';
Blockly.Msg.IO_ONPINTOUCHED_TITLE = 'on %1 touched do';
Blockly.Msg.IO_ONPINTOUCHED_TOOLTIP = 'Execute instructions if micro:bit logo or the pin P0, P1 or P2 is touched (or pressed).';
Blockly.Msg.IO_ONMOVEMENT_TITLE = 'on %1 do';
Blockly.Msg.IO_ONMOVEMENT_SHAKE = 'shake';
Blockly.Msg.IO_ONMOVEMENT_UP = 'logo up';
Blockly.Msg.IO_ONMOVEMENT_DOWN = 'logo down';
Blockly.Msg.IO_ONMOVEMENT_FACE_UP = 'screen up';
Blockly.Msg.IO_ONMOVEMENT_FACE_DOWN = 'screen down';
Blockly.Msg.IO_ONMOVEMENT_LEFT = 'tilt left';
Blockly.Msg.IO_ONMOVEMENT_RIGHT = 'tilt right';
Blockly.Msg.IO_ONMOVEMENT_FREEFALL = 'free fall';
Blockly.Msg.IO_ONMOVEMENT_TOOLTIP = 'Execute instructions if the micro:bit card is shaken.';
Blockly.Msg.IO_ISBUTTONPRESSED_TITLE = 'button %1 %2 pressed';
Blockly.Msg.IO_ISBUTTONPRESSED_TOOLTIP = 'Return \'True\' if button A or B is pressed, else return \'False\'.';
Blockly.Msg.IO_ISBUTTONPRESSED_SIGNAL_TITLE = 'button %1 %2 pressed as a signal between 0 and 3,3';
Blockly.Msg.IO_ISBUTTONPRESSED_SIGNAL_TOOLTIP = 'Return 3,3 if button A or B is pressed, else return 0.';
Blockly.Msg.IO_ISPINTOUCHED_TITLE = '%1 is touched';
Blockly.Msg.IO_ISPINTOUCHED_TOOLTIP = 'Return \'True\' if micro:bit logo or pin P0, P1 or P2 is touched (or pressed), else return \'False\'.';
// Input/Output - Microphone module
Blockly.Msg.IO_MICRO_LOUD = 'loud';
Blockly.Msg.IO_MICRO_QUIET = 'quiet';
Blockly.Msg.IO_MICRO_IS = 'is';
Blockly.Msg.IO_MICRO_WAS = 'was';
Blockly.Msg.IO_MICRO_ONSOUNDDETECTED_TITLE = '%1 if %2 sound %3 detected';
Blockly.Msg.IO_MICRO_ONSOUNDDETECTED_TOOLTIP = 'Execute instructions if sound condition (loud/quiet) is detected. \'was\' option: Execute instructions if a (loud/quiet) sound occured since the last call to \'was_sound()\'.';
Blockly.Msg.IO_MICRO_GETCURRENTSOUND_TITLE = '%1 sound condition';
Blockly.Msg.IO_MICRO_GETCURRENTSOUND_TOOLTIP = 'Return the sound condition (loud/quiet).';
Blockly.Msg.IO_MICRO_WASSOUNDDETECTED_TITLE = '%1 %2 sound was detected';
Blockly.Msg.IO_MICRO_WASSOUNDDETECTED_TOOLTIP = 'Return True if a (loud/quiet) sound occured since the last call to \'was_sound()\'.';
Blockly.Msg.IO_MICRO_GETSOUNDLEVEL_TITLE = '%1 sound level';
Blockly.Msg.IO_MICRO_GETSOUNDLEVEL_TOOLTIP = 'Enable du get sound level from 0 to 255.';
Blockly.Msg.IO_MICRO_GETHISTORYSOUND_TITLE = '%1 history of sounds';
Blockly.Msg.IO_MICRO_GETHISTORYSOUND_TOOLTIP = 'Return history of sounds since last call to \'get_sounds()\'.';
Blockly.Msg.IO_MICRO_SETSOUNDTHRESHOLD_TITLE = '%1 set %2 sound threshold to %3';
Blockly.Msg.IO_MICRO_SETSOUNDTHRESHOLD_TOOLTIP = 'Enable to set sound level threshold loud/quiet from 0 to 255.';
Blockly.Msg.IO_MICRO_SOUNDCONDITION_TITLE = '%1 %2';
Blockly.Msg.IO_MICRO_SOUNDCONDITION_TOOLTIP = 'Enable to use (LOUD/QUIET) constants from microphone module in \'Logic\' category.';
// Input/Output - External modules
Blockly.Msg.IO_GROVEKEYPAD_GETNUMBER_TITLE = '[Numeric Touch Keypad] get number on pins RX %1 TX %2';
Blockly.Msg.IO_GROVEKEYPAD_GETNUMBER_TOOLTIP = IMG_MODULE_KEYPAD + Blockly.Tooltip.SEP + 'Enable to get touched number from keypad grove module on pins RX & TX. When you connect device, make sure you “cross” the wires. microbit TX pin needs to be connected with device\'s RX pin, and the RX pin with the device\'s TX pin.';
Blockly.Msg.IO_GROVEJOYSTICK_GETAXIS_TITLE = '[Joystick Module] joystick axis %1 value on pins A0 %2 A1 %3';
Blockly.Msg.IO_GROVEJOYSTICK_GETAXIS_TOOLTIP = IMG_MODULE_JOYSTICK + Blockly.Tooltip.SEP + 'Return grove joystick axis value (from 0 to 1023) on analog pins P0 through P4, or P10.';
Blockly.Msg.IO_GROVECOLOREDBUTTON_GET_TITLE = '[Colored Button Module] state on pin SIG2 %1';
Blockly.Msg.IO_GROVECOLOREDBUTTON_GET_TOOLTIP = IMG_MODULE_LED_BUTTON + Blockly.Tooltip.SEP + 'Return grove colored button state (0 or 1) on digital pins P0 up through P20.';
Blockly.Msg.IO_GROVECOLOREDBUTTON_SETLED_TITLE = '[Colored Button Module] control LED to state %1 on pin SIG1 %2';
Blockly.Msg.IO_GROVECOLOREDBUTTON_SETLED_TOOLTIP = IMG_MODULE_LED_BUTTON + Blockly.Tooltip.SEP + 'Enable to switch on or switch off the LED Button Grove (0 or 1) on digitals pins P0 up through P20.';
Blockly.Msg.IO_GETGROVEROTARYANGLE_TITLE = '[Rotary Angle Module] angle on pin %1';
Blockly.Msg.IO_GETGROVEROTARYANGLE_TOOLTIP = IMG_MODULE_ROTARY_ANGLE + Blockly.Tooltip.SEP + 'Return grove rotary angle position (from 0 to 1023) on analog pins P0 through P4, or P10.';
Blockly.Msg.IO_GETGROVESLIDEPOTENTIOMETER_TITLE = '[Slide Potentiometer] position value on pin %1';
Blockly.Msg.IO_GETGROVESLIDEPOTENTIOMETER_TOOLTIP = IMG_MODULE_SLIDE_POT + Blockly.Tooltip.SEP + 'Return grove slide potentiometer position value (from 0 to 1023) on analog pins P0 through P4, or P10.';
Blockly.Msg.IO_GETGROVETACTILE_TITLE = '[Touch Sensor] touch state on pin %1 ';
Blockly.Msg.IO_GETGROVETACTILE_TOOLTIP = IMG_MODULE_TOUCH + Blockly.Tooltip.SEP + 'Return grove touch sensor state (0 or 1) on digital pins P0 up through P20.';
Blockly.Msg.IO_GETGROVEBUTTON_TITLE = '[Button Module] button state on pin %1 ';
Blockly.Msg.IO_GETGROVEBUTTON_TOOLTIP = IMG_MODULE_BUTTON + Blockly.Tooltip.SEP + 'Return grove button state (0 or 1) on digital pins P0 up through P20.';
Blockly.Msg.IO_GETGROVESWITCH_TITLE = '[Switch Module] switch state on pin %1 ';
Blockly.Msg.IO_GETGROVESWITCH_TOOLTIP = IMG_MODULE_SWITCH + Blockly.Tooltip.SEP + 'Return grove switch state (0 or 1) on digital pins P0 up through P20.';
// Input/Output - Pins
Blockly.Msg.IO_DIGITALSIGNAL_TITLE = '%1';
Blockly.Msg.IO_DIGITALSIGNAL_HIGH = 'HIGH';
Blockly.Msg.IO_DIGITALSIGNAL_LOW = 'LOW';
Blockly.Msg.IO_DIGITALSIGNAL_TOOLTIP = 'Return boolean value (1 if HIGH or 0 if LOW).';
Blockly.Msg.IO_READDIGITALPIN_TITLE = 'read digital pin %1';
Blockly.Msg.IO_READDIGITALPIN_TOOLTIP = 'Enable to read the digital value of pins (0 or 1).';
Blockly.Msg.IO_READDIGITALPIN_SIGNAL_TITLE = 'read digital pin %1 as a signal between 0 and 3,3';
Blockly.Msg.IO_READDIGITALPIN_SIGNAL_TOOLTIP = 'Read the value of pins (0 or 3,3).';
Blockly.Msg.IO_WRITEDIGITALPIN_TITLE = 'write on digital pin %1 state %2';
Blockly.Msg.IO_WRITEDIGITALPIN_TOOLTIP = 'Enable to write the value (0 or 1) on digital pin.';
Blockly.Msg.IO_READANALOGPIN_TITLE = 'read analog pin %1';
Blockly.Msg.IO_READANALOGPIN_TOOLTIP = 'Enable to read the analog value of pins (0-1023).';
Blockly.Msg.IO_WRITEANALOGPIN_TITLE = 'write on analog pin %1 value %2';
Blockly.Msg.IO_WRITEANALOGPIN_TOOLTIP = 'Enable to write on analog pin the value (0-1023). This function does not really write analog value, it writes PWM signal. For example, writing 511 has 50% duty cycle, the average voltage is 1,65V.';
Blockly.Msg.IO_SETPWM_TITLE = 'apply a signal of period (µs) %1 on pin %2';
Blockly.Msg.IO_SETPWM_TOOLTIP = 'Enable to apply a PWM signal on a pin';
Blockly.Msg.IO_READPULSEIN_TITLE = 'read pulse in (μs) of state %1 on pin %2';
Blockly.Msg.IO_READPULSEIN_TOOLTIP = 'Return the duration of pulse in. Choice state (HIGH or LOW)';
Blockly.Msg.IO_PWM_WITH_FREQ_TITLE = 'apply pwn on pin %1 with freqency: %3 Hz duty cycle: %2 %'
Blockly.Msg.IO_PWM_WITH_FREQ_TOOLTIP = 'Generate a PWM signal'
Blockly.Msg.IO_PWM_TITLE = 'apply pwm on pin %1 with duty cycle: %2 %'
Blockly.Msg.IO_PWM_TOOLTIP = 'Generate a PWM signal'

// Communication - Micro:bit
Blockly.Msg.COMMUNICATION_RADIO_SENDSTRING_TITLE = '  %1 send string %2';
Blockly.Msg.COMMUNICATION_RADIO_SENDSTRING_TOOLTIP = 'Enable to send string by micro:bit radio module.';
Blockly.Msg.COMMUNICATION_RADIO_SENDNUMBER_TITLE = '%1 send number %2';
Blockly.Msg.COMMUNICATION_RADIO_SENDNUMBER_TOOLTIP = 'Enable to send numbers by radio module.';
Blockly.Msg.COMMUNICATION_RADIO_SENDVALUE_TITLE = '%1 send value %2 as %3';
Blockly.Msg.COMMUNICATION_RADIO_SENDVALUE_TOOLTIP = 'Enable to send data with \'name\' and its value by radio module.';
Blockly.Msg.COMMUNICATION_RADIO_ONSTRINGRECEIVED_TITLE = '%1 on data received in %2';
Blockly.Msg.COMMUNICATION_RADIO_ONSTRINGRECEIVED_TOOLTIP = 'Allows you to execute instructions on string received by radio';
Blockly.Msg.COMMUNICATION_RADIO_ONNUMBERRECEIVED_TITLE = '%1 on data received in %2';
Blockly.Msg.COMMUNICATION_RADIO_ONNUMBERRECEIVED_TOOLTIP = 'Allows you to execute instructions on number received by radio.';
Blockly.Msg.COMMUNICATION_RADIO_ONVALUERECEIVED_TITLE = '%1 on data received in %2 %3';
Blockly.Msg.COMMUNICATION_RADIO_ONVALUERECEIVED_TOOLTIP = 'Allows you to execute instructions on name as string and value as number received by radio in the \'name\' and \'value\' variables.';
Blockly.Msg.COMMUNICATION_RADIO_CONFIG_TITLE = '%1 set Channel %2 Power %3 Data size %4 Group %5';
Blockly.Msg.COMMUNICATION_RADIO_CONFIG_TOOLTIP = 'Allows you to configure the frequence channel (from 0 to 83), data size (bytes), transmission power (from 0 to 7), and group (from 0 to 255).';
// Communication - Serial connection
Blockly.Msg.COMMUNICATION_SERIAL_WRITE_TITLE = '%1 write on serial port %2';
Blockly.Msg.COMMUNICATION_SERIAL_WRITE_TOOLTIP = 'Write a string on serial port.';
Blockly.Msg.COMMUNICATION_SERIAL_INIT_TITLE = '%1 redirect serial to RX %3 TX %4 Baudrate %2';
Blockly.Msg.COMMUNICATION_SERIAL_INIT_TOOLTIP = 'Enable to redirect serial connection with RX & TX. When you connect device, make sure you “cross” the wires. microbit TX pin needs to be connected with device\'s RX pin, and the RX pin with the device\'s TX pin.';
Blockly.Msg.COMMUNICATION_SERIAL_REDIRECTTOUSB_TITLE = '%1 redirect serial to USB port';
Blockly.Msg.COMMUNICATION_SERIAL_REDIRECTTOUSB_TOOLTIP = 'Enable to redirect serial connection to USB. It is used if you connect some UART devices in same time.';
Blockly.Msg.COMMUNICATION_SERIAL_ONDATARECEIVED_TITLE = '%1 on serial data received in %2';
Blockly.Msg.COMMUNICATION_SERIAL_ONDATARECEIVED_TOOLTIP = 'Allows you to execute instructions if data is received by serial port in the \'serialData\' variable.';
Blockly.Msg.COMMUNICATION_COMPUTER_PLAYNOTE_TITLE = '%1 Play music %2 in the serial port';
Blockly.Msg.COMMUNICATION_COMPUTER_PLAYNOTE_TOOLTIP = 'Play selected note until execution of \'Stop music\' block.';
Blockly.Msg.NOTE_C = 'C';
Blockly.Msg.NOTE_C_SHARP = 'C#';
Blockly.Msg.NOTE_D = 'D';
Blockly.Msg.NOTE_D_SHARP = 'D#';
Blockly.Msg.NOTE_E = 'E';
Blockly.Msg.NOTE_F = 'F';
Blockly.Msg.NOTE_F_SHARP = 'F#';
Blockly.Msg.NOTE_G = 'G';
Blockly.Msg.NOTE_G_SHARP = 'G#';
Blockly.Msg.NOTE_A = 'A';
Blockly.Msg.NOTE_A_SHARP = 'A#';
Blockly.Msg.NOTE_B = 'B';
Blockly.Msg.COMMUNICATION_COMPUTER_SETFREQUENCY_TITLE = '%1 Play frequency %2 (Hz) on the computer';
Blockly.Msg.COMMUNICATION_COMPUTER_SETFREQUENCY_TOOLTIP = 'This block allows to play a given frequency on the computer';
Blockly.Msg.COMMUNICATION_COMPUTER_STOPMUSIC_TITLE = '%1 Stop music of serial port';
Blockly.Msg.COMMUNICATION_COMPUTER_STOPMUSIC_TOOLTIP = 'Stop the current note of serial port.';
Blockly.Msg.COMMUNICATION_WRITEGRAPH_TITLE = '%1 write graph';
Blockly.Msg.COMMUNICATION_WRITEGRAPH_TOOLTIP = 'This block makes it possible to write (digital) data that will be visible in the plotter. It can be used with one or more blocks in \'Name\' and \'Data\' format. Click on icon \'Graphic mode\' to display graphics.';
Blockly.Msg.COMMUNICATION_DATA = 'Data';
Blockly.Msg.COMMUNICATION_PRINT_DATAS_TITLE = 'Name %1 Data %2';
Blockly.Msg.COMMUNICATION_PRINT_DATAS_TOOLTIP = 'This block is to be used in the \'Write in graphic\' block. It must contain the name of the (text) value to display and the value in question.';
// Communication - Data logging
Blockly.Msg.COMMUNICATION_OPENLOG_WRITE_TITLE = '%1 write in the SD card %2 on pins RX %3 TX %4 %5 Datas %6';
Blockly.Msg.COMMUNICATION_OPENLOG_WRITE_TOOLTIP = IMG_MODULE_OPENLOG + Blockly.Tooltip.SEP + 'Block enables writing data in the SD card of Openlog module.';
// Communication - Wireless
Blockly.Msg.COMMUNICATION_BLUETOOTH_SENDDATA_TITLE = '%1 send on pins RX %2 TX %3 message %4';
Blockly.Msg.COMMUNICATION_BLUETOOTH_SENDDATA_TOOLTIP = IMG_MODULE_HC05 + Blockly.Tooltip.SEP + 'Enable to send any data by bluetooth HC05 module on pins RX/TX.';
Blockly.Msg.COMMUNICATION_BLUETOOTH_ONDATARECEIVED_TITLE = '%1 on message received RX %2 TX %3 in %4';
Blockly.Msg.COMMUNICATION_BLUETOOTH_ONDATARECEIVED_TOOLTIP = IMG_MODULE_HC05 + Blockly.Tooltip.SEP + 'Allows you to execute instructions on data received by Bluetooth HC05 module in the \'bluetoothData\' variable on pins TX/RX.';
// Communication - Tracking modules
Blockly.Msg.COMMUNICATION_GPS_ONDATARECEIVED_TITLE = '%1 on data received on pins RX %2 TX %3 in %4';
Blockly.Msg.COMMUNICATION_GPS_ONDATARECEIVED_TOOLTIP = IMG_MODULE_GPS + Blockly.Tooltip.SEP + 'Allows you to execute instructions on data received by GPS grove module in the \'gpsData\' variable on pin TX/RX.';
Blockly.Msg.COMMUNICATION_GPS_GETINFORMATIONS_TITLE = '%1 get %2 with datas %3';
Blockly.Msg.COMMUNICATION_GPS_GETINFORMATIONS_TOOLTIP = IMG_MODULE_GPS + Blockly.Tooltip.SEP + 'Return analyzed datas from gps grove module choosen in (\'clock\',\'latitude\',\'longitude\')';
Blockly.Msg.COMMUNICATION_GPS_INFO_CLOCK = 'clock';
Blockly.Msg.COMMUNICATION_GPS_INFO_LATITUDE = 'latitude °North';
Blockly.Msg.COMMUNICATION_GPS_INFO_LONGITUDE = 'longitude °East';
Blockly.Msg.COMMUNICATION_GPS_INFO_SATELLITE = 'number of satellites used';
Blockly.Msg.COMMUNICATION_GPS_INFO_ALTITUDE = 'altitude (m)';
Blockly.Msg.COMMUNICATION_GPS_INFO_ALL_FRAME = 'all frame';

// Sensors - Micro:bit
Blockly.Msg.SENSORS_GETACCELERATION_TITLE = 'acceleration (mg) %1';
Blockly.Msg.SENSORS_GETACCELERATION_TOOLTIP = 'Return the acceleration (in mg) with internal micro:bit accelerometer.';
Blockly.Msg.SENSORS_GETLIGHT_TITLE = 'light level';
Blockly.Msg.SENSORS_GETLIGHT_TOOLTIP = 'Return light level (from 0 to 255) using the Galaxia card LED.';
Blockly.Msg.SENSORS_GETLIGHT_SIGNAL_TITLE = 'light level as a signal between 0 and 3,3';
Blockly.Msg.SENSORS_GETLIGHT_SIGNAL_TOOLTIP = 'Return light level as a signal between 0 (0 dark) and 3,3 (255 luminous) using the Galaxia card LED.';
Blockly.Msg.SENSORS_CALIBRATECOMPASS_TITLE = 'calibrate compass';
Blockly.Msg.SENSORS_CALIBRATECOMPASS_TOOLTIP = 'Enable to calibrate the internal micro:bit compass. Just shake the card to calibrate it. A \'happy\' smiley appears when the calibration is completed.';
Blockly.Msg.SENSORS_GETCOMPASS_TITLE = 'compass heading (°)';
Blockly.Msg.SENSORS_GETCOMPASS_TOOLTIP = 'Return the compass heading (from 0° to 360°) with the internal Galaxia compass.';
Blockly.Msg.SENSORS_GETCOMPASS_SIGNAL_TITLE = 'compass heading as signal between 0 and 3.3'
Blockly.Msg.SENSORS_GETCOMPASS_SIGNAL_TOOLTIP = 'Return the compass heading as a signal between 0 (0°) and 3,3 (360°).';
Blockly.Msg.SENSORS_ISCOMPASSCALIBRATED_TITLE = 'compass calibrated ?';
Blockly.Msg.SENSORS_ISCOMPASSCALIBRATED_TOOLTIP = 'Return \'True\' if the compass calibrated, else return \'False\'.';
Blockly.Msg.SENSORS_GETTEMPERATURE_TITLE = 'temperature in %1';
Blockly.Msg.SENSORS_GETTEMPERATURE_TOOLTIP = 'Return the current temperature in Celius degree (°C), Fahrenheit (°F) or Kelvin (K).';
Blockly.Msg.SENSORS_GETTEMPERATURE_SIGNAL_TITLE = 'temperature in %1 as signal between 0 and 3.3';
Blockly.Msg.SENSORS_GETTEMPERATURE_SIGNAL_TOOLTIP = 'Return the current temperature as a signal between 0 (-20°C) and 3,3 (80°C)';
Blockly.Msg.SENSORS_GETROTATION_TITLE = 'rotation (°) %1';
Blockly.Msg.SENSORS_GETROTATION_PITCH = 'pitch';
Blockly.Msg.SENSORS_GETROTATION_ROLL = 'roll';
Blockly.Msg.SENSORS_GETROTATION_TOOLTIP = 'Return the rotation (from -180° to 180°) with the internal micro:bit accelerometer.';
Blockly.Msg.SENSORS_GETMAGNETICFORCE_TITLE = 'magnetic field strength (µT) %1';
Blockly.Msg.SENSORS_GETMAGNETICFORCE_TOOLTIP = 'Return the magnetic field strength (in µTesla) in the choosen direction from internal micro:bit compass.';
// Sensors - Gas
Blockly.Msg.SENSORS_SGP30_READDATA_TITLE = '[SGP30 Sensor] gas %1';
Blockly.Msg.SENSORS_SGP30_READDATA_TOOLTIP = IMG_MODULE_SGP30 + Blockly.Tooltip.SEP + 'Return the amount of CO2 (in ppm) or TVOC (in ppb) in the air from sgp30 sensor. Connect sensor on I2C port.';
Blockly.Msg.SENSORS_SGP30_CO2 = 'carbon dioxide (CO2) (ppm)';
Blockly.Msg.SENSORS_SGP30_TVOC = 'volatile organic compounds (TVOC) (ppb)';
Blockly.Msg.SENSORS_MULTICHANNEL_GETGAS_TITLE = '[Multichannel Gas Sensor] gas %1 (ppm)';
Blockly.Msg.SENSORS_MULTICHANNEL_GETGAS_TOOLTIP = IMG_MODULE_MULTICHANNEL + Blockly.Tooltip.SEP + 'Return the amount of choosen gas in the air (in ppm) from grove multichannel gas sensor. Connect sensor on I2C port.';
Blockly.Msg.SENSORS_MULTICHANNELV2_GETGAS_TITLE = '[Multichannel Gas Sensor v2] gas %1 (V)';
Blockly.Msg.SENSORS_MULTICHANNELV2_GETGAS_TOOLTIP = IMG_MODULE_MULTICHANNEL_V2 + Blockly.Tooltip.SEP + 'Return the amount of choosen gas in the air (in V) from grove multichannel gas V2 sensor. Connect sensor on I2C port.';
Blockly.Msg.GAS_CO = 'carbon monoxide (CO)';
Blockly.Msg.GAS_NO2 = 'nitrogen dioxide (NO2)';
Blockly.Msg.GAS_C2H5OH = 'ethanol (C2H5OH)';
Blockly.Msg.GAS_H2 = 'dihydrogen (H2)';
Blockly.Msg.GAS_NH3 = 'ammonia (NH3)';
Blockly.Msg.GAS_CH4 = 'methane (CH4)';
Blockly.Msg.GAS_C3H8 = 'propane (C3H8)';
Blockly.Msg.GAS_C4H10 = 'iso-propane (C4H10)';
Blockly.Msg.GAS_VOC = 'volatile organic compounds (VOC)';
Blockly.Msg.SENSORS_O2_GAS_READDATA_TITLE = '[Oxygen Gas Sensor] O2 (%) on pin %1';
Blockly.Msg.SENSORS_O2_GAS_READDATA_TOOLTIP = IMG_MODULE_O2 + Blockly.Tooltip.SEP + 'Return O2 concentration (in %) from the grove O2 sensor on pins P0 through P4, or P10.';
Blockly.Msg.SENSORS_AIR_QUALITY_GETVALUE_TITLE = '[Air Quality Sensor] value on pin %1';
Blockly.Msg.SENSORS_AIR_QUALITY_GETVALUE_TOOLTIP = IMG_MODULE_AIR_QUALITY + Blockly.Tooltip.SEP + 'Return value of air quality (from 0 to 1023) on pins P0 through P4, or P10.';
Blockly.Msg.SENSORS_HM330X_GETPARTICULE_TITLE = '[HM330X Sensor] concentration of particle matter %1 (µg/m3)';
Blockly.Msg.SENSORS_HM330X_GETPARTICULE_TOOLTIP = IMG_MODULE_HM330X + Blockly.Tooltip.SEP + 'Detect the density of particles in the air with the HM330X sensor. Connect sensor on I2C port.';
Blockly.Msg.SENSORS_HM330X_ATM_PM1 = 'PM1.0';
Blockly.Msg.SENSORS_HM330X_ATM_PM2_5 = 'PM2.5';
Blockly.Msg.SENSORS_HM330X_ATM_PM10 = 'PM10';
// Sensors - Climate
Blockly.Msg.SENSORS_TEMPERATURE = 'temperature';
Blockly.Msg.SENSORS_HUMIDITY = 'humidity (%)';
Blockly.Msg.SENSORS_TEMPERATURE_IN = 'in';
Blockly.Msg.SENSORS_BMP280_READDATA_TITLE = '[BMP280 Sensor %1] %2';
Blockly.Msg.SENSORS_BMP280_READDATA_TOOLTIP = IMG_MODULE_BMP280 + Blockly.Tooltip.SEP + 'Return the ambient temperature in Celius degree (°C), Fahrenheit (°F) or Kelvin (K), pressure (in Pa). The altitude is initialized at 0 when program is flashed. It use Grove Barometer Sensor (address: 0x77, color: blue) or HW-611 280 sensor (address: 0x76, color: purple). Connect sensor on I2C port.';
Blockly.Msg.SENSORS_BMP280_TEMPERATURE = 'temperature';
Blockly.Msg.SENSORS_BMP280_PRESSURE = 'pressure (Pa)';
Blockly.Msg.SENSORS_BMP280_ALTITUDE = 'altitude (m)';
Blockly.Msg.SENSORS_GETGROVEHIGHTEMP_TITLE = '[H.T° sensor] temperature in %1 on pins A0 %2 A1 %3';
Blockly.Msg.SENSORS_GETGROVEHIGHTEMP_TOOLTIP = IMG_MODULE_HIGH_TEMPERATURE + Blockly.Tooltip.SEP + 'Return thermocouple temperature dorm 50 to 600 °C with grove high temperature sensor. Connect sensor on analog pins.';
Blockly.Msg.SENSORS_GETGROVEMOISTURE_TITLE = '[Moisture Sensor] moisture on pin %1';
Blockly.Msg.SENSORS_GETGROVEMOISTURE_TOOLTIP = IMG_MODULE_MOISTURE + Blockly.Tooltip.SEP + 'Return moisture measurement (from 0 to 1023) from the grove moisture sensor on pins P0 through P4, or P10.';
Blockly.Msg.SENSORS_GETGROVETEMPERATURE_TITLE = '[Temperature Sensor] temperature in %1 on pin %2';
Blockly.Msg.SENSORS_GETGROVETEMPERATURE_TOOLTIP = IMG_MODULE_TEMPERATURE + Blockly.Tooltip.SEP + 'Return grove temperature sensor value in Celius degree (°C), Fahrenheit (°F) or Kelvin (K) on analog pins P0 through P4, or P10.';
Blockly.Msg.SENSORS_DHT_READDATA_TITLE = '[DHT11 Sensor] %1 on pin %2';
Blockly.Msg.SENSORS_DHT_READDATA_TOOLTIP = IMG_MODULE_DHT11 + Blockly.Tooltip.SEP + 'Return temperature in Celius degree (°C), Fahrenheit (°F) or Kelvin (K), or air humidity (in %) from dht11 sensor on digital pins P0 up through P20.';
Blockly.Msg.SENSORS_TH02_READDATA_TITLE = '[TH02 Sensor] %1';
Blockly.Msg.SENSORS_TH02_READDATA_TOOLTIP = IMG_MODULE_TH02 + Blockly.Tooltip.SEP + 'Return temperature in Celsius degree (°C), Fahrenheit (°F) or Kelvin (K), or air humidity (in %) from TH02 sensor. Connect sensor on I2C port.';
Blockly.Msg.SENSORS_SHT31_READDATA_TITLE = '[SHT31 Sensor] %1';
Blockly.Msg.SENSORS_SHT31_READDATA_TOOLTIP = IMG_MODULE_SHT31 + Blockly.Tooltip.SEP + 'Return temperature in Celsius degree (°C), Fahrenheit (°F) or Kelvin (K), or air humidity (in %) from SHT31 sensor. Connect sensor on I2C port.';
Blockly.Msg.SENSORS_GETGROVEWATER_TITLE = '[Water Sensor] water amount on pin %1';
Blockly.Msg.SENSORS_GETGROVEWATER_TOOLTIP = IMG_MODULE_WATER + Blockly.Tooltip.SEP + 'Return water amount (from 0 to 255) from the grove water sensor on pins P0 through P4, or P10.';
Blockly.Msg.SENSORS_GETRAINGAUGE_TITLE = '[Rain Gauge sensor] state value on pin %1';
Blockly.Msg.SENSORS_GETRAINGAUGE_TOOLTIP = IMG_MODULE_RAIN_GAUGE + Blockly.Tooltip.SEP + 'Return rain gauge grove state (1 if it\'s raining or 0 else) on digital pins P0 up through P20.';
Blockly.Msg.SENSORS_GETANEMOMETER_TITLE = '[Anemometer] state value on pin %1';
Blockly.Msg.SENSORS_GETANEMOMETER_TOOLTIP = IMG_MODULE_ANEMOMETER + Blockly.Tooltip.SEP + 'Return grove anemometer state (twice state HIGH on each rotation) on digital pins P0 up through P20.';
// Sensors - Sound & Light
Blockly.Msg.SENSORS_GETGROVELIGHT_TITLE = '[Light Sensor] light level on pin %1';
Blockly.Msg.SENSORS_GETGROVELIGHT_TOOLTIP = IMG_MODULE_LIGHT + Blockly.Tooltip.SEP + 'Return grove light sensor value (from 0 to 1023) on analog pins P0 through P4, or P10.';
Blockly.Msg.SENSORS_SI1145_GETLIGHT_TITLE = '[SI1145 Sensor] get light %1';
Blockly.Msg.SENSORS_SI1145_GETLIGHT_TOOLTIP = IMG_MODULE_SI1145 + Blockly.Tooltip.SEP + 'Return Ultraviolet light index, IR light (in lumen) or Visible light (in lumen) from si1145 sensor. It works with Grove Sunlight Sensor or GY1145 sensor. Connect sensor on I2C port.';
Blockly.Msg.SENSORS_SI1145_UV = 'UV index';
Blockly.Msg.SENSORS_SI1145_VISIBLE = 'visible (lumen)';
Blockly.Msg.SENSORS_SI1145_IR = 'infrared (lumen)';
Blockly.Msg.SENSORS_GETUVINDEX_TITLE = '[Ultraviolet Sensor] UV index on pin %1';
Blockly.Msg.SENSORS_GETUVINDEX_TOOLTIP = IMG_MODULE_UV + Blockly.Tooltip.SEP + 'Return UV index, for waves between 240 nm and 380 nm, with UV grove sensor on analog pins A0 to A5.';
Blockly.Msg.SENSORS_GETGROVESOUND_TITLE = '[Sound Sensor] sound level (dB) on pin %1 ';
Blockly.Msg.SENSORS_GETGROVESOUND_TOOLTIP = IMG_MODULE_SOUND_LOUDNESS + Blockly.Tooltip.SEP + 'Return grove sound sensor value (from 0 to 1023 converted in dB) on analog pins P0 through P4, or P10.';
// Sensors - Distance & Motion
Blockly.Msg.SENSORS_GETGROVEULTRASONIC_TITLE = '[Ultrasonic Sensor] get %1 on pins TRIG %2 ECHO %3';
Blockly.Msg.SENSORS_GETGROVEULTRASONIC_TOOLTIP = IMG_MODULE_ULTRASONIC + Blockly.Tooltip.SEP + 'Return distance measurement (in centimeters) from the ultrasonic ranger sensor on digital pins P0 up through P20. Warning, if it\'s a grove sensor, TRIG and ECHO are both connected to SIG.';
Blockly.Msg.SENSORS_ULTRASONIC_DISTANCE = 'distance (cm)';
Blockly.Msg.SENSORS_ULTRASONIC_DURATION = 'round-trip duration (µs)';
Blockly.Msg.SENSORS_GETGESTURE_TITLE = '[Gesture Sensor] gesture type';
Blockly.Msg.SENSORS_GETGESTURE_TOOLTIP = IMG_MODULE_GESTURE + Blockly.Tooltip.SEP + 'Return the gesture type (\'right\', \'left\', \'up\', \'down\', \'forward\', \'backward\', \'clockwise\', \'anticlockwise\') from grove gesture sensor. Connect sensor on I2C port.';
Blockly.Msg.SENSORS_ONGESTUREDETECTED_TITLE = '[Gesture Sensor] on gesture %1 detected';
Blockly.Msg.SENSORS_ONGESTUREDETECTED_TOOLTIP = IMG_MODULE_GESTURE + Blockly.Tooltip.SEP + 'Execute instructions if selected gesture id detected by the grove gesture sensor. Connect sensor on I2C port.';
Blockly.Msg.SENSORS_GESTURE_RIGHT = 'right';
Blockly.Msg.SENSORS_GESTURE_LEFT = 'left';
Blockly.Msg.SENSORS_GESTURE_UP = 'up';
Blockly.Msg.SENSORS_GESTURE_DOWN = 'down';
Blockly.Msg.SENSORS_GESTURE_FORWARD = 'forward';
Blockly.Msg.SENSORS_GESTURE_BACKWARD = 'backward';
Blockly.Msg.SENSORS_GESTURE_CLOCKWISE = 'clockwise';
Blockly.Msg.SENSORS_GESTURE_ANTICLOCKWISE = 'anticlockwise';
Blockly.Msg.SENSORS_GESTURE_WAVE = 'wave';
Blockly.Msg.SENSORS_GETGROVELINEFINDER_TITLE = '[Line Finder Sensor] line finder state on pin %1';
Blockly.Msg.SENSORS_GETGROVELINEFINDER_TOOLTIP = IMG_MODULE_LINE_FINDER + Blockly.Tooltip.SEP + 'Return grove touch sensor state (0 or 1) on digital pins P0 up through P20.';
Blockly.Msg.SENSORS_GETGROVEMOTION_TITLE = '[PIR Motion Sensor] movement state value on pin %1';
Blockly.Msg.SENSORS_GETGROVEMOTION_TOOLTIP = IMG_MODULE_MOTION + Blockly.Tooltip.SEP + 'Return grove PIR Motion state (0 if there is movement or 1 else) on digital pins P0 up through P20.';
Blockly.Msg.SENSORS_GETPIEZOVIBRATION_TITLE = '[Piezo Vibration Sensor] state value on pin %1';
Blockly.Msg.SENSORS_GETPIEZOVIBRATION_TOOLTIP = IMG_MODULE_VIBRATIONS + Blockly.Tooltip.SEP + 'Return vibration state (0 or 1) from piezo vibration grove sensor on digital pins P0 up through P20.';
Blockly.Msg.SENSORS_GETGROVETILT_TITLE = '[Tilt Module] tilt state on pin %1';
Blockly.Msg.SENSORS_GETGROVETILT_TOOLTIP = IMG_MODULE_TILT + Blockly.Tooltip.SEP + 'Return grove tilt state (0 or 1) on digital pins P0 up through P20.';
// Other sensors
Blockly.Msg.SENSORS_GETGROVEBUTTON_TITLE = '[Button Module] button %1 on pin %2';
Blockly.Msg.SENSORS_GETGROVEBUTTON_TOOLTIP = IMG_MODULE_BUTTON + Blockly.Tooltip.SEP + 'Return numeric value of grove button (0/1 or 0V/3.3V) on digital pins P0 up through P20.';
Blockly.Msg.SENSORS_GETGROVEBUTTON_VOLTAGE = 'voltage';
Blockly.Msg.SENSORS_GETGROVEBUTTON_STATE = 'state';

// Actuators
Blockly.Msg.ACTUATORS_SERVO_SETANGLE_TITLE = '[Servomotor] set angle to %1 on pin %2';
Blockly.Msg.ACTUATORS_SERVO_SETANGLE_TOOLTIP = IMG_MODULE_SERVO + Blockly.Tooltip.SEP + 'Enable to control servo angle (from 0 to 180) on digital pins from P0 up through P20. Warning, microbit has to be powered by external batterie in order to provide enough energy to servomotor.';
Blockly.Msg.ACTUATORS_CONTINUOUS_SERVO_SETSPEED_TITLE = '[Continuous Servomotor] set speed to %1 (%) direction %2 on pin %3';
Blockly.Msg.ACTUATORS_CONTINUOUS_SERVO_SETSPEED_TOOLTIP = IMG_MODULE_CONTINUOUS_SERVO + Blockly.Tooltip.SEP + 'Enable to control continuous servo speed (from 0 to 100 %) on PWM pins.';
Blockly.Msg.ACTUATORS_MOTOR_SETPOWER_TITLE = '[Motor] set power to %1 on pin %2';
Blockly.Msg.ACTUATORS_MOTOR_SETPOWER_TOOLTIP = IMG_MODULE_MOTOR + Blockly.Tooltip.SEP + 'Enable to control DC motor power (from 0 to 1023) on digital pins P0 up through P20. Warning, microbit has to be powered by external batterie in order to provide enough energy to DC motor.';
Blockly.Msg.ACTUATORS_GROVERELAY_CONTROL_TITLE = '[Relay module] control relay to state %1 on pin %2';
Blockly.Msg.ACTUATORS_GROVERELAY_CONTROL_TOOLTIP = IMG_MODULE_RELAY + Blockly.Tooltip.SEP + 'Enable to control state grove relay module (0 or 1) on digitals pins P0 up through P20.';
Blockly.Msg.ACTUATORS_GROVEVIBRATIONMOTOR_CONTROL_TITLE = '[Vibration motor] control motor to state %1 on pin %2';
Blockly.Msg.ACTUATORS_GROVEVIBRATIONMOTOR_CONTROL_TOOLTIP = IMG_MODULE_VIBRATION_MOTOR + Blockly.Tooltip.SEP + 'Enable to control state of grove vibration motor (0 or 1) on digitals pins P0 up through P20.';
Blockly.Msg.ACTUATORS_MUSIC_PLAYMUSIC_TITLE = '%1 play music %2 on %3';
Blockly.Msg.ACTUATORS_MUSIC_PLAYMUSIC_TOOLTIP = IMG_MODULE_BUZZER_SPEAKER + Blockly.Tooltip.SEP + 'Enable to play the choosen music on built-in micro:bit speaker or on grove buzzer module on digital pins P0 up through P20.';
Blockly.Msg.ACTUATORS_MUSIC_PLAYSONG_TITLE = '%1 play song %2 %3 on %4';
Blockly.Msg.ACTUATORS_MUSIC_PLAYSONG_ONCE = 'once';
Blockly.Msg.ACTUATORS_MUSIC_PLAYSONG_LOOP = 'endlessly';
Blockly.Msg.ACTUATORS_MUSIC_PLAYSONG_TOOLTIP = IMG_MODULE_BUZZER_SPEAKER + Blockly.Tooltip.SEP + 'Enable to play the choosen song on built-in micro:bit speaker or on buzzer module or speaker on digital pins P0 up through P20.';
Blockly.Msg.ACTUATORS_MUSIC_PLAYNOTES_TITLE = 'play notes on';
Blockly.Msg.ACTUATORS_MUSIC_PLAYNOTES_TOOLTIP = IMG_MODULE_BUZZER_SPEAKER + Blockly.Tooltip.SEP + 'Enable to play notes on built-in micro:bit speaker or on buzzer module (or speaker) on digital pins P0 up through P20.';
Blockly.Msg.ACTUATORS_MUSIC_NOTE_TITLE = 'note %1 at octave %2 with duration %3';
Blockly.Msg.ACTUATORS_MUSIC_NOTE_TOOLTIP = IMG_MODULE_BUZZER_SPEAKER + Blockly.Tooltip.SEP + 'Define a note with octave and duration.';
Blockly.Msg.NOTE_C = 'C';
Blockly.Msg.NOTE_C_SHARP = 'C#';
Blockly.Msg.NOTE_D = 'D';
Blockly.Msg.NOTE_D_SHARP = 'D#';
Blockly.Msg.NOTE_E = 'E';
Blockly.Msg.NOTE_F = 'F';
Blockly.Msg.NOTE_F_SHARP = 'F#';
Blockly.Msg.NOTE_G = 'G';
Blockly.Msg.NOTE_G_SHARP = 'G#';
Blockly.Msg.NOTE_A = 'A';
Blockly.Msg.NOTE_A_SHARP = 'A#';
Blockly.Msg.NOTE_B = 'B';
Blockly.Msg.MUSIC_SILENCE = 'Silence';
Blockly.Msg.ACTUATORS_MUSIC_PLAYFREQUENCY_TITLE = '%1 play frequency %2 during %3 (ms) on %4';

Blockly.Msg.ACTUATORS_MUSIC_PLAYFREQUENCY_TOOLTIP = IMG_MODULE_BUZZER_SPEAKER + Blockly.Tooltip.SEP + 'Enable to play integer frequency on buzzer module or speaker on digital pins P0 up through P20.';
Blockly.Msg.ACTUATORS_MUSIC_STOP_TITLE = '%1 stop music on %2';
Blockly.Msg.ACTUATORS_MUSIC_STOP_TOOLTIP = 'Enable to stop music from buil-in micro:bit speaker or on digital pins P0 up through P20.';
Blockly.Msg.ACTUATORS_MUSIC_SETVOLUME_TITLE = '%1 set volume to %2';
Blockly.Msg.ACTUATORS_MUSIC_SETVOLUME_TOOLTIP = 'Enable to change volume of built-in speaker from 0 to 255.';
Blockly.Msg.ACTUATORS_MUSIC_SETTEMPO_TITLE = '%1 set ticks %2 and tempo %3';
Blockly.Msg.ACTUATORS_MUSIC_SETTEMPO_TOOLTIP = 'Enable to set ticks music and beats per minute.';
Blockly.Msg.ACTUATORS_MUSIC_GETTEMPO_TITLE = '%1 get tempo of music';
Blockly.Msg.ACTUATORS_MUSIC_GETTEMPO_TOOLTIP = 'Enable to get the current tempo of music as a tuple of integers (ticks, bpm).';
Blockly.Msg.ACTUATORS_SPEECH_SAYSOMETHING_TITLE = '[Speech] say %1 speed %2 pitch %3';
Blockly.Msg.ACTUATORS_SPEECH_SAYSOMETHING_TOOLTIP = 'Enable to say something with micro:bit. Change speed or pitch from 0 to 255.';

// Robots - Maqueen
Blockly.Msg.ROBOTS_MAQUEEN_RIGHT = 'Right';
Blockly.Msg.ROBOTS_MAQUEEN_LEFT = 'Left';
Blockly.Msg.ROBOTS_MAQUEEN_RIGHT2 = 'Right2';
Blockly.Msg.ROBOTS_MAQUEEN_LEFT2 = 'Left2';
Blockly.Msg.ROBOTS_MAQUEEN_MIDDLE = 'Middle';
Blockly.Msg.ROBOTS_MAQUEEN_RIGHTANDLEFT = 'Right & Left';
Blockly.Msg.ROBOTS_MAQUEEN_ULTRASONICRANGER_TITLE = '%1 ultrasonic %2';
Blockly.Msg.ROBOTS_MAQUEEN_ULTRASONICRANGER_TOOLTIP = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + 'Return distance value (in cm) or the round-trip duration of wave (in μs) from any object to maqueen robot with ultrasonic range sensor.';
Blockly.Msg.ROBOTS_MAQUEEN_ULTRASONIC_DISTANCE = 'distance (cm)';
Blockly.Msg.ROBOTS_MAQUEEN_ULTRASONIC_DURATION = 'round-trip duration (μs)';
Blockly.Msg.ROBOTS_MAQUEEN_CONTROLLED_TITLE = '%1 control LED %2 to state %3';
Blockly.Msg.ROBOTS_MAQUEEN_CONTROLLED_TOOLTIP = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + 'Enable to control LED Right or Left state value of maqueen robot.';
Blockly.Msg.ROBOTS_MAQUEEN_GO_TITLE = '%1 control robot %2 speed %3';
Blockly.Msg.ROBOTS_MAQUEEN_GO_FORWARD = 'forward';
Blockly.Msg.ROBOTS_MAQUEEN_GO_REVERSE = 'backward';
Blockly.Msg.ROBOTS_MAQUEEN_GO_TOOLTIP = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + 'Enable to control car running (FORWARD/BACKWARD) and speed (from 0 to 255) of maqueen robot.';
Blockly.Msg.ROBOTS_MAQUEEN_CONTROLMOTOR_TITLE = '%1 control motor %2 direction %3 speed %4';
Blockly.Msg.ROBOTS_MAQUEEN_CONTROLMOTOR_TOOLTIP = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + 'Enable to control right motor (I2C address : 0x02) and left motor (I2C address : 0x00) changing direction (↻ : FOWARD, ↺ : REVERSE) or speed (from 0 to 255) of maqueen robot.';
Blockly.Msg.ROBOTS_MAQUEEN_STOPMOTORS_TITLE = '%1 stop motor %2';
Blockly.Msg.ROBOTS_MAQUEEN_STOPMOTORS_TOOLTIP = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + 'Enable to stop right, left or both motors of maqueen robot.';
Blockly.Msg.ROBOTS_MAQUEEN_SETSERVOANGLE_TITLE = '%1 set servo %2 to angle %3';
Blockly.Msg.ROBOTS_MAQUEEN_SETSERVOANGLE_TOOLTIP = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + 'Enable to control servomotor S1 (I2C address : 0x14) and S2 (I2C address : 0x15) by setting angle (from 0 to 180) of maqueen robot.';
Blockly.Msg.ROBOTS_MAQUEEN_S1 = 'S1';
Blockly.Msg.ROBOTS_MAQUEEN_S2 = 'S2';
Blockly.Msg.ROBOTS_MAQUEEN_SERVO_BOTH = 'both';
Blockly.Msg.ROBOTS_MAQUEEN_READPATROL_TITLE = '%1 read line patrol %2';
Blockly.Msg.ROBOTS_MAQUEEN_READPATROL_TOOLTIP = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + 'Enable to read state of line patrol sensor of maqueen robot. The block returns 1 if the sensor is above the line.';
Blockly.Msg.ROBOTS_MAQUEEN_SETNEOPIXEL_TITLE = '%1 set color R %3 G %4 B %5 on LED %2';
Blockly.Msg.ROBOTS_MAQUEEN_SETNEOPIXEL_TOOLTIP = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + 'Enable to control each LED color of Maqueen neopixel as (R,G,B) from 0 to 255. Neopixel is connected on pin15 on Maqueen.';
Blockly.Msg.ROBOTS_MAQUEEN_SETPALETTECOLOR_TITLE = '%1 set color %3 on LED %2';
Blockly.Msg.ROBOTS_MAQUEEN_SETPALETTECOLOR_TOOLTIP = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + 'Enable to control each LED color of Maqueen neopixel. Choice color in the palette. Use P15 to set Maqueen neopixel.';
Blockly.Msg.ROBOTS_MAQUEEN_SETRAINBOW_TITLE = '%1 Rainbow';
Blockly.Msg.ROBOTS_MAQUEEN_SETRAINBOW_TOOLTIP = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + 'Enable to show a rainbow on Maqueen RGB LED. Neopixel is connected to pin15 on Maqueen.';
Blockly.Msg.ROBOTS_MAQUEEN_SETBUZZER_TITLE = '%1 set buzzer at frequency %2 during %3 (ms)';
Blockly.Msg.ROBOTS_MAQUEEN_SETBUZZER_TOOLTIP = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + 'Enable to control Maqueen buzzer at any frequency. Buzzer is connected to pin0 on Maqueen.';
Blockly.Msg.ROBOTS_MAQUEEN_PLAYMUSIC_TITLE = '%1 play music %2';
Blockly.Msg.ROBOTS_MAQUEEN_PLAYMUSIC_TOOLTIP = IMG_ROBOT_MAQUEEN + Blockly.Tooltip.SEP + 'Enable to play music on Maqueen buzzer. Buzzer is connected to pin0 on Maqueen.';
// Robots - Codo
Blockly.Msg.ROBOTS_CODO_GO_TITLE = '%1 control robot %2 speed %3';
Blockly.Msg.ROBOTS_CODO_GO_FORWARD = 'forward';
Blockly.Msg.ROBOTS_CODO_GO_BACKWARD = 'backward';
Blockly.Msg.ROBOTS_CODO_GO_TOOLTIP = IMG_ROBOT_CODO + Blockly.Tooltip.SEP + 'Enable to control car running (forward/backward) and speed (from 0 to 255) of Codo robot.';
Blockly.Msg.ROBOTS_CODO_TURN_TITLE = '%1 turn %2 speed %3';
Blockly.Msg.ROBOTS_CODO_TURN_RIGHT = 'right';
Blockly.Msg.ROBOTS_CODO_TURN_LEFT = 'left';
Blockly.Msg.ROBOTS_CODO_TURN_TOOLTIP = IMG_ROBOT_CODO + Blockly.Tooltip.SEP + 'Enable to control direction (right/left) and speed (from 0 to 255) of Codo robot.';
Blockly.Msg.ROBOTS_CODO_STOP_TITLE = '%1 stop run of robot';
Blockly.Msg.ROBOTS_CODO_STOP_TOOLTIP = IMG_ROBOT_CODO + Blockly.Tooltip.SEP + 'Stop car running of Codo robot.';
Blockly.Msg.ROBOTS_CODO_CONTROLMOTOR_TITLE = '%1 control motor %2 direction %3 speed %4';
Blockly.Msg.ROBOTS_CODO_MOTOR_RIGHT = 'right';
Blockly.Msg.ROBOTS_CODO_MOTOR_LEFT = 'left';
Blockly.Msg.ROBOTS_CODO_CONTROLMOTOR_TOOLTIP = IMG_ROBOT_CODO + Blockly.Tooltip.SEP + 'Enable to control motor (right/left), direction (clockwise/anticlockwise) and speed (from 0 to 255) of Codo robot.';
// Robots - Bit:Bot
Blockly.Msg.ROBOTS_BITBOT_READLIGHTSENSOR_TITLE = '[Bit:Bot] read light level on %1 sensor';
Blockly.Msg.ROBOTS_BITBOT_READLIGHTSENSOR_TOOLTIP = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + 'Return light level (from 0 to 255) with internal bit:bot light sensor';
Blockly.Msg.ROBOTS_BITBOT_READPATROL_TITLE = '[Bit:Bot] read line patrol %1';
Blockly.Msg.ROBOTS_BITBOT_READPATROL_TOOLTIP = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + 'Enable to read state of line patrol sensor of bit:bot robot. The block returns 1 if the sensor is above the line.';
Blockly.Msg.ROBOTS_BITBOT_RIGHT = 'Right';
Blockly.Msg.ROBOTS_BITBOT_LEFT = 'Left';
Blockly.Msg.ROBOTS_BITBOT_RIGHTANDLEFT = 'Right & Left';
Blockly.Msg.ROBOTS_BITBOT_GO_TITLE = '[Bit:Bot] control robot %1 speed %2';
Blockly.Msg.ROBOTS_BITBOT_GO_FORWARD = 'Forward';
Blockly.Msg.ROBOTS_BITBOT_GO_REVERSE = 'Reverse';
Blockly.Msg.ROBOTS_BITBOT_GO_TOOLTIP = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + 'Enable to control car running (FORWARD/REVERSE) or speed (from 0 to 1023) of bit:bot robot.';
Blockly.Msg.ROBOTS_BITBOT_CONTROLMOTOR_TITLE = '[Bit:Bot] control motor %1 direction %2 speed %3';
Blockly.Msg.ROBOTS_BITBOT_CONTROLMOTOR_TOOLTIP = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + 'Enable to control right motor (pin0,pin8) and left motor (pin1,pin12) changing direction (↻ : FOWARD, ↺ : REVERSE) or speed (from 0 to 1023) of bit:bot robot.';
Blockly.Msg.ROBOTS_BITBOT_STOPMOTORS_TITLE = '[Bit:Bot] stop motor %1';
Blockly.Msg.ROBOTS_BITBOT_STOPMOTORS_TOOLTIP = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + 'Enable to stop right, left or both motors of bit:bot robot.';
Blockly.Msg.ROBOTS_BITBOT_SETNEOPIXEL_TITLE = '[Bit:Bot] set color R %2 G %3 B %4 on LED %1';
Blockly.Msg.ROBOTS_BITBOT_SETNEOPIXEL_TOOLTIP = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + 'Enable to control each LED color of Maqueen neopixel as (R,G,B) from 0 to 255. Neopixel is connected on pin P13 on bit:bot.';
Blockly.Msg.ROBOTS_BITBOT_SETPALETTECOLOR_TITLE = '[Bit:Bot] set color %2 on LED %1';
Blockly.Msg.ROBOTS_BITBOT_SETPALETTECOLOR_TOOLTIP = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + 'Enable to control each LED color of Bit:Bot neopixel. Choice color in the palette.';
Blockly.Msg.ROBOTS_BITBOT_SETRAINBOW_TITLE = '[Bit:Bot] rainbow';
Blockly.Msg.ROBOTS_BITBOT_SETRAINBOW_TOOLTIP = IMG_ROBOT_BITBOT + Blockly.Tooltip.SEP + 'Enable to show a rainbow on bit:bot RGB LED.';
// Robots - Gamepad
Blockly.Msg.ROBOTS_GAMEPAD_CONTROLLED_TITLE = '%1 set LED to state %2';
Blockly.Msg.ROBOTS_GAMEPAD_CONTROLLED_TOOLTIP = IMG_ROBOT_GAMEPAD + Blockly.Tooltip.SEP + 'Enable to control LED state of Gamepad Expansion DFR035. LED is connected to pin16';
Blockly.Msg.ROBOTS_GAMEPAD_SETMOTORVIBRATION_TITLE = '%1 set motor vibration to state %2';
Blockly.Msg.ROBOTS_GAMEPAD_SETMOTORVIBRATION_TOOLTIP = IMG_ROBOT_GAMEPAD + Blockly.Tooltip.SEP + 'Enable to control motorvibration state of Gamepad Expansion DFR035. Vibration motor is connected to pin12';
Blockly.Msg.ROBOTS_GAMEPAD_SETBUZZERFREQ_TITLE = '%1 set buzzer at frequency %2 during %3 (ms)';
Blockly.Msg.ROBOTS_GAMEPAD_SETBUZZERFREQ_TOOLTIP = IMG_ROBOT_GAMEPAD + Blockly.Tooltip.SEP + 'Enable to control Gamepad buzzer at any frequency. Buzzer is connected to pin0 on Gamepad.';
Blockly.Msg.ROBOTS_GAMEPAD_PLAYMUSIC_TITLE = '%1 play music %2';
Blockly.Msg.ROBOTS_GAMEPAD_PLAYMUSIC_TOOLTIP = IMG_ROBOT_GAMEPAD + Blockly.Tooltip.SEP + 'Enable to play music on Gamepad buzzer. Buzzer is connected to pin0 on Gamepad.';
Blockly.Msg.ROBOTS_GAMEPAD_ONBUTTONEVENT_TITLE = '%1 on button %2 %3';
Blockly.Msg.ROBOTS_GAMEPAD_PRESSED = 'pressed';
Blockly.Msg.ROBOTS_GAMEPAD_RELEASED = 'released';
Blockly.Msg.ROBOTS_GAMEPAD_ONBUTTONEVENT_TOOLTIP = IMG_ROBOT_GAMEPAD + Blockly.Tooltip.SEP + 'Execute instructions if any gamepad button is used. The button have two states : \'pressed\' or \'released\'. Table of pins : ([X,P1],[Y,P2],[UP,P8],[DOWN,P13],[LEFT,P14],[RIGHT,P15])';

// English messages for Galaxia. (EN)

// Display

Blockly.Msg.DISPLAY_GALAXIA_SHOW_TITLE = 'show %1';
Blockly.Msg.DISPLAY_GALAXIA_SHOW_TOOLTIP = 'Allows you to display a number or a character string on the screen integrated into Galaxia';

Blockly.Msg.DISPLAY_GALAXIA_CLEAR_TITLE = 'clear display';
Blockly.Msg.DISPLAY_GALAXIA_CLEAR_TOOLTIP = 'Clear the content of the display'

// Display - LED modules

Blockly.Msg.DISPLAY_GALAXIA_SET_LED_COLORS_TITLE = 'set LED to R %1 G %2 B %3';
Blockly.Msg.DISPLAY_GALAXIA_SET_LED_COLORS_TOOLTIP = 'Allows you to adjust the intensity of the LED integrated in the Galaxia card, from 0 to 100.';

Blockly.Msg.DISPLAY_GALAXIA_LED_RED_CONTROL_TITLE = 'set red LED intensity to %1';
Blockly.Msg.DISPLAY_GALAXIA_LED_RED_CONTROL_TOOLTIP = 'Adjusts the intensity of the red for the LED integrated in the Galaxia card from 0 to 100.';

Blockly.Msg.DISPLAY_GALAXIA_LED_GREEN_CONTROL_TITLE = 'set green LED intensity to %1';
Blockly.Msg.DISPLAY_GALAXIA_LED_GREEN_CONTROL_TOOLTIP = 'Adjusts the intensity of the green for the LED integrated in the Galaxia card from 0 to 100.';

Blockly.Msg.DISPLAY_GALAXIA_LED_BLUE_CONTROL_TITLE = 'set blue LED intensity to %1';
Blockly.Msg.DISPLAY_GALAXIA_LED_BLUE_CONTROL_TOOLTIP = 'Adjusts the intensity of the blue for the LED integrated in the Galaxia card from 0 to 100.';

Blockly.Msg.DISPLAY_GALAXIA_LED_GET_RED_TITLE = 'red LED intensity';
Blockly.Msg.DISPLAY_GALAXIA_LED_GET_RED_TOOLTIP = 'Returns a value between 0 and 100 corresponding to the intensity of red currently transferred to the LED.';

Blockly.Msg.DISPLAY_GALAXIA_LED_GET_GREEN_TITLE = 'green LED intensity';
Blockly.Msg.DISPLAY_GALAXIA_LED_GET_GREEN_TOOLTIP = 'Returns a value between 0 and 100 corresponding to the intensity of green currently transferred to the LED.';

Blockly.Msg.DISPLAY_GALAXIA_LED_GET_BLUE_TITLE = 'blue LED intensity';
Blockly.Msg.DISPLAY_GALAXIA_LED_GET_BLUE_TOOLTIP = 'Returns a value between 0 and 100 corresponding to the intensity of blue currently transferred to the LED.';

Blockly.Msg.DISPLAY_GALAXIA_PLOT_ADD_POINT_TITLE = 'plot: add value %1';
Blockly.Msg.DISPLAY_GALAXIA_PLOT_ADD_POINT_TOOLTIP = 'Add a point to the plot';

Blockly.Msg.DISPLAY_GALAXIA_PLOT_SET_Y_SCALE_TITLE = 'plot: set y axis scale to min value %1 max value %2';
Blockly.Msg.DISPLAY_GALAXIA_PLOT_SET_Y_SCALE_TOOLTIP = 'Set the scale of the y axis';

Blockly.Msg.DISPLAY_GALAXIA_SET_MODE_TITLE = 'set display mode to %1';
Blockly.Msg.DISPLAY_GALAXIA_SET_MODE_TOOLTIP = 'Change the display mode';
Blockly.Msg.DISPLAY_GALAXIA_SET_MODE_PLOT = 'plot';
Blockly.Msg.DISPLAY_GALAXIA_SET_MODE_CONSOLE = 'console';

Blockly.Msg.DISPLAY_GALAXIA_ANIMATE_FUNCTION_TITLE = 'every %1 seconds compute new point';
Blockly.Msg.DISPLAY_GALAXIA_ANIMATE_FUNCTION_TOOLTIP = 'Add a point to the plot every x seconds';
Blockly.Msg.DISPLAY_GALAXIA_ANIMATE_FUNCTION_NEW_POINT = 'add result %1 to plot Y axis';

// IO

Blockly.Msg.IO_GALAXIA_PAUSE_TITLE = 'wait %1 %2';
Blockly.Msg.IO_GALAXIA_PAUSE_TOOLTIP = 'Stop the code execution (duration in seconds or milliseconds).';

Blockly.Msg.IO_ONBUTTONEVENT_TITLE = 'when button %1 is pressed';
Blockly.Msg.IO_ONBUTTONEVENT_TOOLTIP = 'Do something when a button (A or B) is pressed down and released again.';

Blockly.Msg.IO_ISTOUCHSENSITIVEBUTTONTOUCHED_TITLE = 'the %1 touch button %2 touched';
Blockly.Msg.IO_ISTOUCHSENSITIVEBUTTONTOUCHED_TOOLTIP = 'Return \'True\' if the selected touch-sensitive button is touched, else return \'False\'.';

Blockly.Msg.IO_ISTOUCHSENSITIVEBUTTONTOUCHED_SIGNAL_TITLE = '%1 touch button %2 touched as a signal between 0 and 3,3';
Blockly.Msg.IO_ISTOUCHSENSITIVEBUTTONTOUCHED_SIGNAL_TOOLTIP = 'Return 3,3 if the selected touch-sensitive button is touched, else return 0.';

Blockly.Msg.IO_IFTOUCHSENSITIVEBUTTONTOUCHED_TITLE = 'if the %1 touch button %2 pressed do';
Blockly.Msg.IO_IFTOUCHSENSITIVEBUTTONTOUCHED_TOOLTIP = 'Execute instructions if the choosen touch button (up, down, right or left) is touched.';

Blockly.Msg.IO_ONTOUCHSENSITIVEBUTTONEVENT_TITLE = 'when %1 touch button is touched';
Blockly.Msg.IO_ONTOUCHSENSITIVEBUTTONEVENT_TOOLTIP = 'Do something when a touch button (up, down, right or left) is touched and released again.';

// Actuators

Blockly.Msg.ACTUATORS_PLAY_FREQUENCY_TITLE = 'set the frequency to %1 Hz';
Blockly.Msg.ACTUATORS_PLAY_FREQUENCY_TOOLTIP = 'Allows you to modulate the frequency of the sound emitted when it is switched on.';

Blockly.Msg.ACTUATORS_GALAXIA_PLAY_FREQUENCY_WITH_DURATION_TITLE = 'play the frequency %1 Hz during %2 ms';
Blockly.Msg.ACTUATORS_GALAXIA_PLAY_FREQUENCY_WITH_DURATION_TOOLTIP = 'Allows you to play a frequency for the set time.';

Blockly.Msg.ACTUATORS_GALAXIA_PLAY_SET_VOLUME_TITLE = 'set the volume to %1 %';
Blockly.Msg.ACTUATORS_GALAXIA_PLAY_SET_VOLUME_TOOLTIP = 'Set the volume of the sound';

Blockly.Msg.ACTUATORS_GALAXIA_PLAY_SAMPLE_TITLE = 'play the audio file (.wav) %1';
Blockly.Msg.ACTUATORS_GALAXIA_PLAY_SAMPLE_TOOLTIP = 'Play an audio file, must be a wav file';

Blockly.Msg.ACTUATORS_BUZZER_ON = 'play';
Blockly.Msg.ACTUATORS_BUZZER_OFF = 'stop playing';

Blockly.Msg.ACTUATORS_BUZZER_ON_TITLE = 'the frequency';
Blockly.Msg.ACTUATORS_BUZZER_ON_TOOLTIP = 'Allows you to mute the sound or switch it on, on the selected frequency.';

Blockly.Msg.ACTUATORS_BUZZER_OFF_TITLE = 'the frequency';
Blockly.Msg.ACTUATORS_BUZZER_OFF_TOOLTIP = 'Allows you to mute the sound or switch it on, on the selected frequency.';

// Communication

Blockly.Msg.COMMUNICATION_GALAXIA_RADIO_SENDVALUE_TITLE = 'send value %1 by radio';
Blockly.Msg.COMMUNICATION_GALAXIA_RADIO_SENDVALUE_TOOLTIP = 'Allows data to be sent via the radio integrated into the Galaxia card.';

Blockly.Msg.COMMUNICATION_GALAXIA_RADIO_ONRECEIVED_TITLE = 'if a value is received by radio in %1 then';
Blockly.Msg.COMMUNICATION_GALAXIA_RADIO_ONRECEIVED_TOOLTIP = 'Allows you to execute instructions on string received by radio in the \'value\' variable.';

Blockly.Msg.COMMUNICATION_GALAXIA_RADIO_DATA_RECEIVED_TITLE = 'data received via radio';
Blockly.Msg.COMMUNICATION_GALAXIA_RADIO_DATA_RECEIVED_TOOLTIP = 'Returns the value of the data received via the radio integrated into the Galaxia card.';

Blockly.Msg.COMMUNICATION_GALAXIA_RADIO_VALUE = 'valueReceived';

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_BASIC_TITLE = 'connection to access point SSID %1 with password %2'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_BASIC_TOOLTIP = 'Allow you to connect the Galaxia card to the local WiFi network. SSID: Network identifier or name, Router password.'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_TITLE = 'connection to access point SSID %1 with password %2 with IP adress %3'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_TOOLTIP = 'Allow you to connect the Galaxia card to the local WiFi network. SSID: Network identifier or name, Router password, IP: static IP address of the Galaxia card'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_COMPLETE_TITLE = 'connection to access point SSID %1 with password %2 with IP adress %3 on port %4'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_COMPLETE_TOOLTIP = 'Allow you to connect the Galaxia card to the local WiFi network. SSID: Network identifier or name, Router password, IP: static IP address of the Galaxia card, PORT: Server port.'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_CREATE_AP_TITLE = 'create access point with SSID %1 and password %2'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_CREATE_AP_TOOLTIP = 'Allow you to create an access point.'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_ETH_WITH_IP_TITLE = 'connect to network using ethernet with IP %1'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_ETH_WITH_IP_TOOLTIP = 'connect using ethernet adaptor using static ip'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_ETH_TITLE = 'connect to network using ethernet'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_CONNECT_ETH_TOOLTIP = 'connect using ethernet adaptor'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_GETLASTCONNECTEDIP_TITLE = 'latest connected IP'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_GETLASTCONNECTEDIP_TOOLTIP = 'Allow you to return the IP address of the last device connected to the card.'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_GETMYIP_TITLE = 'my IP address'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_GETMYIP_TOOLTIP = 'Allow to return the IP address of the card.'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_BASIC_TITLE = 'wait for a message to be received in %1 then do'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_BASIC_TOOLTIP = 'Receive block basic'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_TITLE = 'wait for a message to be received in %2 from the IP %1 then do'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_TOOLTIP = 'Receive block; ip'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_COMPLETE_TITLE = 'if a message is received in %3 via IP %1 within %2 second(s) then'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_COMPLETE_TOOLTIP = 'Receive block complete; timeout, ip'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_TITLE = 'wait for a message ending with %2 to be received in %1 then do'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_TOOLTIP = 'Receive block with delimiter'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_R = 'CR'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_N = 'LF'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_RN = 'CR/LF'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_RECEIVE_DELIMITER_NONE = 'none'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RECEIVE_ASYNC_TITLE = 'data received'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RECEIVE_ASYNC_TOOLTIP = 'Data send from client, received by the server'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RECEIVED_DATA = 'data'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RECEIVE_EVENT_TITLE = 'when server receives %1';
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RECEIVE_EVENT_TOOLTIP = 'Data send from client, received by the server'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_BASIC_TITLE = 'send %1 to IP %2'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_BASIC_TOOLTIP = 'Send block basic; data, ip'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_BASIC_WITH_DELIMITER_TITLE = 'send %1 to IP %2 end message with %3'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_BASIC_WITH_DELIMITER_TOOLTIP = 'Send block basic; data, ip'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_WAIT_RESPOND_WITH_DELIMITER_TITLE = 'send %1 to IP %2 end message with %3 and wait for response in %4'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_WAIT_RESPOND_WITH_DELIMITER_TOOLTIP = 'Send data to IP and wait for the response'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_WAIT_RESPOND_TITLE = 'send %1 to IP %2 and wait for response in %3'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_WAIT_RESPOND_TOOLTIP = 'Send data to IP and wait for the response'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_COMPLETE_TITLE = 'send %1 to IP %2 via port %3'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SEND_COMPLETE_TOOLTIP = 'Send block complete; data, ip, port'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_RESPONSE = 'response';
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_MESSAGE = 'message';
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RESPOND_TITLE = 'respond %1 to client'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RESPOND_TOOLTIP = 'Respond to a connected client'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RESPOND_WITH_DELIMITER_TITLE = 'respond %1 to client and end message with %2'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_RESPOND_WITH_DELIMITER_TOOLTIP = 'Respond to a connected client with a end character'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_IS_CLIENT_CONNECTED_TITLE = 'is client connected to server'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLEWIFI_SERVER_IS_CLIENT_CONNECTED_TOOLTIP = 'Check if a client is connected to the TCP server'

Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_RESPONSE = 'request';

Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_RESPOND_BASIC_TITLE = 'respond %1'
Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_RESPOND_BASIC_TOOLTIP = 'Respond basic block'

Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_RESPOND_COMPLETE_TITLE = 'respond %1 code %2'
Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_RESPOND_COMPLETE_TOOLTIP = 'Respond complete block'

Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_WAIT_REQUEST_TITLE = 'wait for a request to be received in %1 then do'
Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_WAIT_REQUEST_TOOLTIP = 'Wait request block'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMLPLE_HTPP_REQUEST_TITLE = 'get URL of %1'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMLPLE_HTPP_REQUEST_TOOLTIP = 'Get the URL of the given response.'

Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_GENERATE_PAGE = 'generate new page every %1 seconds'
Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_GENERATE_PAGE_TOOLTIP = 'Update the page served by the web server every x seconds'

Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_GENERATE_PAGE_NO_REFRESH = 'generate web page'
Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_GENERATE_PAGE_NO_REFRESH_TOOLTIP = 'Generate a web page at url /'

Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_ADD_TO_PAGE = 'add text %1 into web page'
Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_ADD_TO_PAGE_TOOLTIP = 'Add text to the web page served by the web server'

Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_ADD_BUTTON_TO_PAGE = 'add button into web page with title %1 who send the request %2'
Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_ADD_BUTTON_TO_PAGE_TOOLTIP = 'Add button to the web page served by the web server'

Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_BUTTON_EVENT_TITLE = 'on web button %1 clicked do'
Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_BUTTON_EVENT_TOOLTIP = 'Choose what to do on web button click'

Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_ADD_GAUGE_TO_PAGE = 'add gauge into web page with title %1 value %2 min value %3 max value %4 (display value as text)'
Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_ADD_GAUGE_TO_PAGE_WITHOUT_TEXT = 'add gauge into web page with title %1 value %2 min value %3 max value %4'
Blockly.Msg.COMMUNICATION_GALAXIA_HTTP_ADD_GAUGE_TO_PAGE_TOOLTIP = 'Add gauge to the web page served by the web server'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLE_MQTT_RECEIVE = 'on mqtt %2 from %1'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLE_MQTT_TOPIC = 'topic'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLE_MQTT_MESSAGE = 'message'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLE_MQTT_RECEIVE_TOOLTIP = 'Do something when a new message is received on a topic'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLE_MQTT_ON_CONNECT = 'on mqtt connected to broker'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLE_MQTT_ON_CONNECT_TOOLTIP = 'Do something when the connection to the broker is established'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLE_MQTT_ON_DISCONNECT = 'on mqtt disconnected from broker'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLE_MQTT_ON_DISCONNECT_TOOLTIP = 'Do something when the connection to the broker is closed'

Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLE_MQTT_CONNECT_COMPLETE_TITLE = 'connect to mqtt broker %1 on port %4 with username %2 and password %3'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLE_MQTT_CONNECT_COMPLETE_TOOLTIP = 'connect to an mqtt broker'

Blockly.Msg.COMMUNICATION_GALAXIA_MQTT_SUBSCRIBE = 'subscribe to mqtt topic %1'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLE_MQTT_SUBSCRIBE_TOOLTIP = 'subscribe to an mqtt topic'

Blockly.Msg.COMMUNICATION_GALAXIA_MQTT_PUBLISH = 'publish %2 to topic %1'
Blockly.Msg.COMMUNICATION_GALAXIA_SIMPLE_MQTT_PUBLISH_TOOLTIP = 'publish a message to a topic'

Blockly.Msg.COMMUNICATION_GALAXIA_MQTT_IS_CONNECTED = 'is connected to broker'
Blockly.Msg.COMMUNICATION_GALAXIA_MQTT_IS_CONNECTED_TOOLTIP = 'Check if Galaxia is connected to broker'
// Sensor

Blockly.Msg.SENSORS_GALAXIA_GETACCELERATION_TITLE = 'acceleration (mg) %1';
Blockly.Msg.SENSORS_GALAXIA_GETACCELERATION_TOOLTIP = 'Returns the acceleration (in mg) using the Galaxia card accelerometer. ';
Blockly.Msg.SENSORS_GALAXIA_GETACCELERATION_SIGNAL_TITLE = 'acceleration as a signal between 0 and 3 %1';
Blockly.Msg.SENSORS_GALAXIA_GETACCELERATION_SIGNAL_TOOLTIP = 'Returns the acceleration as a signal between 0 (-2000mG) and 3,3 (2000mG). ';

Blockly.Msg.SENSORS_GALAXIA_ONACCELEROMETEREVENT_TITLE = 'When accelerometer is %1';
Blockly.Msg.SENSORS_GALAXIA_ONACCELEROMETEREVENT_TOOLTIP = 'Do something when the accelerator detect a specific movement.';
Blockly.Msg.SENSORS_GALAXIA_ONACCELEROMETEREVENT_UP = 'up facing';
Blockly.Msg.SENSORS_GALAXIA_ONACCELEROMETEREVENT_LEFT = 'left facing';
Blockly.Msg.SENSORS_GALAXIA_ONACCELEROMETEREVENT_RIGHT = 'right facing';
Blockly.Msg.SENSORS_GALAXIA_ONACCELEROMETEREVENT_DOWN = 'down facing';
Blockly.Msg.SENSORS_GALAXIA_ONACCELEROMETEREVENT_FACEUP = 'face up';
Blockly.Msg.SENSORS_GALAXIA_ONACCELEROMETEREVENT_FACEDOWN = 'face down';
Blockly.Msg.SENSORS_GALAXIA_ONACCELEROMETEREVENT_FREEFALL = 'in freefall';
Blockly.Msg.SENSORS_GALAXIA_ONACCELEROMETEREVENT_SHAKE = 'shake';
Blockly.Msg.SENSORS_GALAXIA_ONACCELEROMETEREVENT_3G = '3g';
Blockly.Msg.SENSORS_GALAXIA_ONACCELEROMETEREVENT_6G = '6g';
Blockly.Msg.SENSORS_GALAXIA_ONACCELEROMETEREVENT_8G = '8g';

Blockly.Msg.SENSORS_GALAXIA_GETMAGNETICFORCE_TITLE = 'magnetic field strength (µT) %1';
Blockly.Msg.SENSORS_GALAXIA_GETMAGNETICFORCE_TOOLTIP = 'Return the magnetic field strength (in µTesla) in the choosen direction from internal micro:bit compass.';
Blockly.Msg.SENSORS_GALAXIA_GETMAGNETICFORCE_SIGNAL_TITLE = 'magnetic field strength as a signal between 0 and 3,3 %1';
Blockly.Msg.SENSORS_GALAXIA_GETMAGNETICFORCE_SIGNAL_TOOLTIP = 'Return the magnetic field strength as a signal between 0 (-215µT) and 3,3(215µT)';

Blockly.Msg.SETUP_TITLE = 'On startup %1 do %2 Forever %3 do %4';
Blockly.Msg.SETUP_TOOLTIP = 'If you want to do an action only once, put it in \'On startup\' otherwise in \'Forever\'.';

Blockly.Msg.ROBOTS_GALAXIA_MAQUEEN_GO_TITLE = '[maqueen] control robot %1 speed %2';
Blockly.Msg.ROBOTS_GALAXIA_MAQUEEN_CONTROLMOTOR_TITLE = '[maqueen] control motor %1 direction %2 speed %3';
Blockly.Msg.ROBOTS_GALAXIA_MAQUEEN_STOPMOTORS_TITLE = '[maqueen] stop motor %1';
Blockly.Msg.ROBOTS_GALAXIA_MAQUEEN_READPATROL_TITLE = '[maqueen] read line patrol %1'
Blockly.Msg.ROBOTS_GALAXIA_MAQUEEN_PLUS_READPATROL_TITLE = '[maqueen plus] read line patrol %1'
Blockly.Msg.ROBOTS_GALAXIA_MAQUEEN_GO_TOOLTIP = 'Control robot speed and direction';
Blockly.Msg.ROBOTS_GALAXIA_MAQUEEN_CONTROLMOTOR_TOOLTIP = 'Control motor speed and direction';
Blockly.Msg.ROBOTS_GALAXIA_MAQUEEN_STOPMOTORS_TOOLTIP = 'Stop robot motors';
Blockly.Msg.ROBOTS_GALAXIA_MAQUEEN_READPATROL_TOOLTIP = 'read state of line patrol sensor of maqueen robot.'
Blockly.Msg.ROBOTS_GALAXIA_MAQUEEN_PLUS_READPATROL_TOOLTIP = 'read state of line patrol sensor of maqueen plus robot.'
Blockly.Msg.ROBOTS_GALAXIA_MAQUEEN_READ_ULTRASOUND_TITLE = '[maqueen] read ultrasonic sensor in %1'
Blockly.Msg.ROBOTS_GALAXIA_MAQUEEN_PLUS_READ_ULTRASOUND_TITLE = '[maqueen plus] read ultrasonic sensor in %1'
Blockly.Msg.ROBOTS_GALAXIA_MAQUEEN_READ_ULTRASOUND_TOOLTIP = 'Return distance in cm or mm'

Blockly.Msg.ROBOTS_GALAXIA_CODO_GO_TITLE = '[codo] control robot %1 speed %2';
Blockly.Msg.ROBOTS_GALAXIA_CODO_CONTROLMOTOR_TITLE = '[codo] control motor %1 direction %2 speed %3';
Blockly.Msg.ROBOTS_GALAXIA_CODO_STOPMOTORS_TITLE = '[codo] stop motor %1';
Blockly.Msg.ROBOTS_GALAXIA_CODO_GO_TOOLTIP = 'Control robot speed and direction';
Blockly.Msg.ROBOTS_GALAXIA_CODO_CONTROLMOTOR_TOOLTIP = 'Control motor speed and direction';
Blockly.Msg.ROBOTS_GALAXIA_CODO_STOPMOTORS_TOOLTIP = 'Stop robot motors';
Blockly.Msg.ROBOTS_CODO_RIGHT = 'Right';
Blockly.Msg.ROBOTS_CODO_LEFT = 'Left';
Blockly.Msg.ROBOTS_CODO_RIGHTANDLEFT = 'Right & Left';

Blockly.Msg.MOTOR_GALAXIA_SPEED_TITLE = '[DRF0548] set motor %1 direction to %2 speed to %3';
Blockly.Msg.MOTOR_GALAXIA_SPEED_TOOLTIP = 'Set motor direction and speed 0-255';
Blockly.Msg.MOTOR_GALAXIA_CLOCKWISE = 'clockwise';
Blockly.Msg.MOTOR_GALAXIA_COUNTER_CLOCKWISE = 'counter-clockwise';

Blockly.Msg.MOTOR_GALAXIA_STOP_TITLE = '[DRF0548] stop motor %1';
Blockly.Msg.MOTOR_GALAXIA_STOP_TOOLTIP = 'Stop motor';

Blockly.Msg.MOTOR_GALAXIA_STOP_ALL_TITLE = '[DRF0548] stop all motors';
Blockly.Msg.MOTOR_GALAXIA_STOP_ALL_TOOLTIP = 'Stop all motor';

Blockly.Msg.MOTOR_GALAXIA_SERVO_TITLE = '[DRF0548] Set servo %1 to %2 degree';
Blockly.Msg.MOTOR_GALAXIA_SERVO_TOOLTIP = 'Set servo angle 0-180';

Blockly.Msg.MOTOR_GALAXIA_STEP_42_TITLE = '[DRF0548] stepper motor 42 %1 turn %3 degree %2';
Blockly.Msg.MOTOR_GALAXIA_STEP_42_TOOLTIP = 'Turn by x degree';

Blockly.Msg.MOTOR_GALAXIA_STEP_42_TURN_TITLE = '[DRF0548] stepper motor 42 %1 turn %3 time %2';
Blockly.Msg.MOTOR_GALAXIA_STEP_42_TURN_TOOLTIP = 'Turn by x turn';

Blockly.Msg.MOTOR_GALAXIA_STEP_28_TITLE = '[DRF0548] stepper motor 28 %1 turn %3 degree %2';
Blockly.Msg.MOTOR_GALAXIA_STEP_28_TOOLTIP = 'Turn by x degree';

Blockly.Msg.MOTOR_GALAXIA_STEP_28_TURN_TITLE = '[DRF0548] stepper motor 28 %1 turn %3 time %2';
Blockly.Msg.MOTOR_GALAXIA_STEP_28_TURN_TOOLTIP = 'Turn by x turn';

Blockly.Msg.MOTOR_GALAXIA_STEP_DUAL_TURN_TITLE = '[DRF0548] dual stepper motor %1 motor1: turn %3 time %2 motor2: turn %5 time %4';
Blockly.Msg.MOTOR_GALAXIA_STEP_DUAL_TURN_TOOLTIP = 'Turn by x turn';

Blockly.Msg.MOTOR_GALAXIA_STEP_DUAL_TITLE = '[DRF0548] dual stepper motor %1 motor1: turn %3 degree %2 motor2: turn %5 degree %4';
Blockly.Msg.MOTOR_GALAXIA_STEP_DUAL_TOOLTIP = 'Turn by x degree';

Blockly.Msg.TIME_GALAXIA_TIMER_START_TITLE = 'chronometer start';
Blockly.Msg.TIME_GALAXIA_TIMER_START_TOOLTIP = 'Start the chronometer';

Blockly.Msg.TIME_GALAXIA_TIMER_MEASURE_TITLE = 'chronometer mesure time since start (in %1)';
Blockly.Msg.TIME_GALAXIA_TIMER_MEASURE_TOOLTIP = 'Returns time elapsed since chronometer start'
Blockly.Msg.TIME_GALAXIA_TIMER_MEASURE_S = 's'
Blockly.Msg.TIME_GALAXIA_TIMER_MEASURE_MS = 'ms'
Blockly.Msg.TIME_GALAXIA_TIMER_MEASURE_US = 'µs'

Blockly.Msg.GROVE_GALAXIA_TEMPERATURE_TITLE = '[Temperature] read on %1'
Blockly.Msg.GROVE_GALAXIA_GROVE_1 = 'Grove 1 (P19/P20)'
Blockly.Msg.GROVE_GALAXIA_GROVE_2 = 'Grove 2 (P7/P6)'
Blockly.Msg.GROVE_GALAXIA_TEMPERATURE_TOOLTIP = 'Read the temperature of the sensor plugged in grove connector'
Blockly.Msg.GROVE_GALAXIA_TEMPERATURE_SIGNAL_TITLE = '[Temperature] read on %1 as a signal between 0 and 3,3'
Blockly.Msg.GROVE_GALAXIA_TEMPERATURE_SIGNAL_TOOLTIP = 'Return the temperature of the sensor plugged in grove connector as a signal between 0 (-20°C) and 3,3 (80°C)'


Blockly.Msg.GROVE_GALAXIA_ULTRASONIC_TITLE = '[Ultrasonic] measure distance on %1 in %2'
Blockly.Msg.GROVE_GALAXIA_ULTRASONIC_TOOLTIP = 'Measure distance with ultrasonic plugged in grove connector'
Blockly.Msg.GROVE_GALAXIA_ULTRASONIC_MM = 'mm'
Blockly.Msg.GROVE_GALAXIA_ULTRASONIC_CM = 'cm'

Blockly.Msg.GROVE_GALAXIA_DHT11_TITLE = '[DHT11] read %2 on %1'
Blockly.Msg.GROVE_GALAXIA_DHT11_TEMP = 'temperature'
Blockly.Msg.GROVE_GALAXIA_DHT11_HUMIDITY = 'humidity'
Blockly.Msg.GROVE_GALAXIA_DHT11_TOOLTIP = 'Read temperature or humidity from sensor plugged into Grove connectors'
Blockly.Msg.GROVE_GALAXIA_DHT11_SIGNAL_TITLE = '[DHT11] read %2 on %1 as a signal between 0 and 3,3'
Blockly.Msg.GROVE_GALAXIA_DHT11_SIGNAL_TOOLTIP = 'Return the current temperature read from Grove connectors as a signal between 0 (-20°C) and 3,3 (80°C)'

Blockly.Msg.GROVE_GALAXIA_SERVO_TITLE = '[Servo] set angle to %1 on pin %2'
Blockly.Msg.GROVE_GALAXIA_SERVO_TOOLTIP = 'Drive a servomotor'


Blockly.Msg.GROVE_GALAXIA_IR_SENDER_TITLE = '[IR] %1 ir signal on pin %2'
Blockly.Msg.GROVE_GALAXIA_IR_SENDER_TOOLTIP = 'Send a Infrared signal'
Blockly.Msg.GROVE_GALAXIA_DISABLE = 'disable'
Blockly.Msg.GROVE_GALAXIA_ENABLE = 'enable'

Blockly.Msg.SPRITE_GALAXIA_CATEGORY = 'Drawing'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_CREATE = 'create rectangle %1 position x: %2 y: %3 size width: %4 height: %5 color: %6'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_CREATE_TOOLTIP = 'Create a rectangle'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_SET_X_Y = 'set rectangle %1 %2 to %3'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_SET_X_Y_TOOLTIP = 'Set rectangle position'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_GET_X_Y = 'get rectangle %1 %2'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_GET_X_Y_TOOLTIP = 'Get rectangle position'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_SET_WIDTH_HEIGHT = 'set rectangle %1 %2 to %3'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_SET_WIDTH_HEIGHT_TOOLTIP = "Set rectangle size"
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_WIDTH = 'width'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_HEIGHT = 'heigth'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_GET_WIDTH_HEIGHT = 'get rectangle %1 %2'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_GET_WIDTH_HEIGHT_TOOLTIP = 'Get rectangle size'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_SET_COLOR = 'rectangle %1 set color to %2'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_SET_COLOR_TOOLTIP  = 'Set rectangle color'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_GET_COLOR = 'rectangle %1 get color'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_GET_COLOR_TOOLTIP = 'Get rectangle color'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_SET_HIDDEN = 'rectangle %1 set %2'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_SET_HIDDEN_TOOLTIP = 'Set rectangle visibility'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_HIDDEN = 'hidden'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_VISIBLE = 'visible'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_GET_HIDDEN = 'rectangle %1 is visible'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_GET_HIDDEN_TOOLTIP = 'Get rectangle visibility'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_MOVE_BY = 'rectangle %1 move by %2'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_MOVE_BY_TOOLTIP = 'Move the rectangle in the configured direction'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_TURN = 'rectangle %1 turn toward %2 by %3'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_TURN_TOOLTIP = 'Turn by X degrees toward left or right'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_BOUNCE_IF_EDGE = 'rectangle %1 if on edge bounce'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_BOUNCE_IF_EDGE_TOOLTIP = 'Make a 180 degrees turn if rectangle is on a edge'

Blockly.Msg.SPRITE_GALAXIA_IMAGE_CREATE = 'create image %1 position x: %2 y: %3 scale: %4 path: %5'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_CREATE_TOOLTIP = 'Create a image'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_SET_X_Y = 'set image %1 %2 to %3'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_SET_X_Y_TOOLTIP = 'Set image position'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_GET_X_Y = 'get image %1 %2'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_GET_X_Y_TOOLTIP = 'Get image position'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_GET_WIDTH_HEIGHT = 'get image %1 %2'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_GET_WIDTH_HEIGHT_TOOLTIP = 'Get image size'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_SET_SCALE = 'set image %1 scale to %2'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_SET_SCALE_TOOLTIP = 'Set image scale'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_GET_SCALE = 'get image %1 scale'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_GET_SCALE_TOOLTIP = 'Get image scale'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_SET_HIDDEN = 'image %1 set %2'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_SET_HIDDEN_TOOLTIP = 'Set image visibility'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_GET_HIDDEN = 'image %1 is visible'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_GET_HIDDEN_TOOLTIP = 'Get image visibility'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_MOVE_BY = 'image %1 move by %2'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_MOVE_BY_TOOLTIP = 'Move the image in the configured direction'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_TURN = 'image %1 turn toward %2 by %3'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_TURN_TOOLTIP = 'Turn by X degrees toward left or right'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_BOUNCE_IF_EDGE = 'image %1 if on edge bounce'
Blockly.Msg.SPRITE_GALAXIA_IMAGE_BOUNCE_IF_EDGE_TOOLTIP = 'Make a 180 degrees turn if image is on a edge'

Blockly.Msg.SPRITE_GALAXIA_ICON_CREATE = 'create icon %1 position x: %2 y: %3 scale: %4 color: %5 name: %6'
Blockly.Msg.SPRITE_GALAXIA_ICON_CREATE_TOOLTIP = 'Create a icon'
Blockly.Msg.SPRITE_GALAXIA_ICON_SET_X_Y = 'set icon %1 %2 to %3'
Blockly.Msg.SPRITE_GALAXIA_ICON_SET_X_Y_TOOLTIP = 'Set icon position'
Blockly.Msg.SPRITE_GALAXIA_ICON_GET_X_Y = 'get icon %1 %2'
Blockly.Msg.SPRITE_GALAXIA_ICON_GET_X_Y_TOOLTIP = 'Get icon position'
Blockly.Msg.SPRITE_GALAXIA_ICON_GET_WIDTH_HEIGHT = 'get icon %1 %2'
Blockly.Msg.SPRITE_GALAXIA_ICON_GET_WIDTH_HEIGHT_TOOLTIP = 'Get icon size'
Blockly.Msg.SPRITE_GALAXIA_ICON_SET_SCALE = 'set icon %1 scale to %2'
Blockly.Msg.SPRITE_GALAXIA_ICON_SET_SCALE_TOOLTIP = "Set icon scale"
Blockly.Msg.SPRITE_GALAXIA_ICON_GET_SCALE = 'get icon %1 scale'
Blockly.Msg.SPRITE_GALAXIA_ICON_GET_SCALE_TOOLTIP = 'Get icon scale'
Blockly.Msg.SPRITE_GALAXIA_ICON_SET_COLOR = 'icon %1 set color to %2'
Blockly.Msg.SPRITE_GALAXIA_ICON_SET_COLOR_TOOLTIP  = 'Set icon color'
Blockly.Msg.SPRITE_GALAXIA_ICON_GET_COLOR = 'icon %1 get color'
Blockly.Msg.SPRITE_GALAXIA_ICON_GET_COLOR_TOOLTIP = 'Get icon color'
Blockly.Msg.SPRITE_GALAXIA_ICON_SET_HIDDEN = 'icon %1 set %2'
Blockly.Msg.SPRITE_GALAXIA_ICON_SET_HIDDEN_TOOLTIP = 'Set icon visibility'
Blockly.Msg.SPRITE_GALAXIA_ICON_GET_HIDDEN = 'icon %1 is visible'
Blockly.Msg.SPRITE_GALAXIA_ICON_GET_HIDDEN_TOOLTIP = 'Get icon visibility'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_CROSS = 'cross'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_CIRCLE = 'circle'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_EMOJI = 'emoji'
Blockly.Msg.SPRITE_GALAXIA_RECTANGLE_HEART = 'heart'
Blockly.Msg.SPRITE_GALAXIA_ICON_MOVE_BY = 'icon %1 move by %2'
Blockly.Msg.SPRITE_GALAXIA_ICON_MOVE_BY_TOOLTIP = 'Move the icon in the configured direction'
Blockly.Msg.SPRITE_GALAXIA_ICON_TURN = 'icon %1 turn toward %2 by %3'
Blockly.Msg.SPRITE_GALAXIA_ICON_TURN_TOOLTIP = 'Turn by X degrees toward left or right'
Blockly.Msg.SPRITE_GALAXIA_ICON_BOUNCE_IF_EDGE = 'icon %1 if on edge bounce'
Blockly.Msg.SPRITE_GALAXIA_ICON_BOUNCE_IF_EDGE_TOOLTIP = 'Make a 180 degrees turn if icon is on a edge'

Blockly.Msg.SPRITE_GALAXIA_COLLISION = '%1 is in collision with %2'
Blockly.Msg.SPRITE_GALAXIA_COLLISION_TOOLTIP = 'Check if sprite is in collision with another sprite'
Blockly.Msg.SPRITE_GALAXIA_BORDER_COLLISION = '%1 is in collision with border %2'
Blockly.Msg.SPRITE_GALAXIA_BORDER_COLLISION_TOOLTIP = 'Check if sprite is in collision with a border'
Blockly.Msg.SPRITE_GALAXIA_BORDER_NORTH = 'norh'
Blockly.Msg.SPRITE_GALAXIA_BORDER_SOUTH = 'south'
Blockly.Msg.SPRITE_GALAXIA_BORDER_WEST = 'west'
Blockly.Msg.SPRITE_GALAXIA_BORDER_EAST = 'east'

Blockly.Msg.SPRITE_GALAXIA_LEFT = 'left'
Blockly.Msg.SPRITE_GALAXIA_RIGHT = 'right'

Blockly.Msg.DATA_GALAXIA_SET_COLUMNS = 'data create columns:'
Blockly.Msg.DATA_GALAXIA_SET_COLUMNS_TOOLTIP = 'Create columns used in data logger csv file'


Blockly.Msg.DATA_GALAXIA_ADD = 'data add:'
Blockly.Msg.DATA_GALAXIA_COLUMN = 'column:'
Blockly.Msg.DATA_GALAXIA_VALUE = 'value:'
Blockly.Msg.DATA_GALAXIA_ADD_TOOLTIP = 'Add data to the data logger csv file'

Blockly.Msg.DATA_GALAXIA_DELETE_TITLE = 'data delete all entries'
Blockly.Msg.DATA_GALAXIA_DELTE_TOOLTIP = 'Delete all data store in data logger file'

Blockly.Msg.DATA_GALAXIA_COLUMN_VALUE_TITLE = 'column: %1 value: %2'
Blockly.Msg.DATA_GALAXIA_COLUMN_VALUE_TOOLTIP = 'Set a column and the value to apply to it'

Blockly.Msg.GALAXIA_NEOPIXEL_DEFINE_TITLE = '[neopixel] create %1 leds connected to %2'
Blockly.Msg.GALAXIA_NEOPIXEL_DEFINE_TOOLTIP = 'Create neopixel leds'
Blockly.Msg.GALAXIA_NEOPIXEL_SET_LED_RGB_TITLE = '[neopixel] set led %1 of %5 to R %2 G %3 B %4'
Blockly.Msg.GALAXIA_NEOPIXEL_SET_LED_RGB_TOOLTIP = 'Set color of a specific neopixel led'
Blockly.Msg.GALAXIA_NEOPIXEL_SET_LED_COLOR_TITLE = '[neopixel] set led %1 of %3 to %2'
Blockly.Msg.GALAXIA_NEOPIXEL_SET_LED_COLOR_TOOLTIP = 'Set color of a specific neopixel led'
Blockly.Msg.GALAXIA_NEOPIXEL_SET_LEDS_RGB_TITLE = '[neopixel] set all leds of %4 to R %1 G %2 B %3'
Blockly.Msg.GALAXIA_NEOPIXEL_SET_LEDS_RGB_TOOLTIP = 'Set color of all neopixel leds'
Blockly.Msg.GALAXIA_NEOPIXEL_SET_LEDS_COLOR_TITLE = '[neopixel] set all leds of %2 to %1'
Blockly.Msg.GALAXIA_NEOPIXEL_SET_LEDS_COLOR_TOOLTIP = 'Set color of all neopixel leds'
Blockly.Msg.IO_GALAXIA_TIMESTAMP_TITLE = 'time since boot in %1'
Blockly.Msg.IO_GALAXIA_TIMESTAMP_TOOLTIP = 'Get time since boot in ms or s'

Blockly.Msg.TEXT_TO_CODE_TITLE = 'Insert code %1'
Blockly.Msg.TEXT_TO_CODE_TOOLTIP = 'Insert source code'

Blockly.Msg.COMMUNICATION_GALAXIA_RADIO_GET_CHANNEL_TITLE = 'get channel used by radio'
Blockly.Msg.COMMUNICATION_GALAXIA_RADIO_GET_CHANNEL_TOOLTIP = 'Get channel used by radio'

Blockly.Msg.COMMUNICATION_GALAXIA_RADIO_SET_CHANNEL_TITLE = 'set channel used by radio to %1'
Blockly.Msg.COMMUNICATION_GALAXIA_RADIO_SET_CHANNEL_TOOLTIP = 'Set channel used by radio. Must be between 1 and 10'

Blockly.Msg.KITRONIK_ACCESS_MOVE_TITLE = '[kitronik access] move barrier %1'
Blockly.Msg.KITRONIK_ACCESS_MOVE_TOOLTIP = 'Move barrier up or down'
Blockly.Msg.KITRONIK_ACCESS_UP = 'up'
Blockly.Msg.KITRONIK_ACCESS_DOWN = 'down'

Blockly.Msg.KITRONIK_ACCESS_SOUND_TITLE = '[kitronik access] sound %1 beep %2 times'
Blockly.Msg.KITRONIK_ACCESS_SOUND_TOOLTIP = 'Make short or long beep'
Blockly.Msg.KITRONIK_ACCESS_SHORT = 'short'
Blockly.Msg.KITRONIK_ACCESS_LONG = 'long'

Blockly.Msg.KITRONIK_LAMP_DEADBAND_TITLE = '[kitronik lamp] deadband value'
Blockly.Msg.KITRONIK_LAMP_DEADBAND_TOOLTIP = 'Return deadband value'

Blockly.Msg.KITRONIK_LAMP_READ_LIGHT_LEVEL_TITLE = '[kitronik lamp] read light level'
Blockly.Msg.KITRONIK_LAMP_READ_LIGHT_LEVEL_TOOLTIP = 'Return value between 0-1023'

Blockly.Msg.KITRONIK_LAMP_SET_LIGHT_TITLE = '[kitronik lamp] turn lamp %1'
Blockly.Msg.KITRONIK_LAMP_SET_LIGHT_TOOLTIP = 'Control lamp'
Blockly.Msg.KITRONIK_LAMP_ON = 'on'
Blockly.Msg.KITRONIK_LAMP_OFF = 'off'

Blockly.Msg.KITRONIK_LAMP_SET_LAMP_DEADBAND_TITLE = '[kitronik lamp] set lamp deadband to %1 %'
Blockly.Msg.KITRONIK_LAMP_SET_LAMP_DEADBAND_TOOLTIP = 'Set lamp deadband'

Blockly.Msg.KITRONIK_STOP_SET_TRAFFIC_LIGHT_TITLE = '[kitronik stop] set traffic light to %1'
Blockly.Msg.KITRONIK_STOP_SET_TRAFFIC_LIGHT_TOOLTIP = 'Set traffic light state'
Blockly.Msg.KITRONIK_STOP_TRAFFIC_LIGHT_STATE_STOP = 'stop'
Blockly.Msg.KITRONIK_STOP_TRAFFIC_LIGHT_STATE_GET_READY = 'get ready'
Blockly.Msg.KITRONIK_STOP_TRAFFIC_LIGHT_STATE_GO = 'go'
Blockly.Msg.KITRONIK_STOP_TRAFFIC_LIGHT_STATE_READY_TO_STOP = 'ready to stop'

Blockly.Msg.KITRONIK_STOP_SET_COLOR_TITLE = '[kitronik stop] turn %1 traffic light %2'
Blockly.Msg.KITRONIK_STOP_SET_COLOR_TOOLTIP = 'Set individual traffic light'
Blockly.Msg.KITRONIK_STOP_COLOR_RED = 'red'
Blockly.Msg.KITRONIK_STOP_COLOR_ORANGE = 'jaune'
Blockly.Msg.KITRONIK_STOP_COLOR_GREEN = 'green'
Blockly.Msg.KITRONIK_STOP_ON = 'on'
Blockly.Msg.KITRONIK_STOP_OFF = 'off'

Blockly.Msg.EDIT = 'Edit'
Blockly.Msg.PROTECT = 'Protect'