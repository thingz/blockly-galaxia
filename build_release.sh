#!/bin/sh


echo $1
mkdir ./blockly\ compressed/$1

cp ./blockly_compressed.js ./blockly\ compressed/$1/blockly_compressed_$(echo $1 | sed -r 's/\./_/g').js
cp ./blocks_compressed.js ./blockly\ compressed/$1/blocks_compressed_$(echo $1 | sed -r 's/\./_/g').js
cp ./galaxia_compressed.js ./blockly\ compressed/$1/galaxia_compressed_$(echo $1 | sed -r 's/\./_/g').js
cp ./galaxia_micropython_compressed.js ./blockly\ compressed/$1/galaxia_micropython_compressed_$(echo $1 | sed -r 's/\./_/g').js
cp ./python_compressed.js ./blockly\ compressed/$1/python_compressed_$(echo $1 | sed -r 's/\./_/g').js

cp ./msg/js/en.js ./blockly\ compressed/$1/en_$(echo $1 | sed -r 's/\./_/g').js
cp ./msg/js/fr.js ./blockly\ compressed/$1/fr_$(echo $1 | sed -r 's/\./_/g').js