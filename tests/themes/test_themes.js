/**
 * @license
 * Copyright 2020 Google LLC
 * SPDX-License-Identifier: Apache-2.0
 */
"use strict";

goog.provide("Blockly.TestThemes");

/**
 * A theme with classic colours but enables start hats.
 */
Blockly.Themes.TestHats = Blockly.Theme.defineTheme("testhats", {
  base: Blockly.Themes.Classic,
});
Blockly.Themes.TestHats.setStartHats(true);

/**
 * A theme with classic colours but a different font.
 */
Blockly.Themes.TestFont = Blockly.Theme.defineTheme("testfont", {
  base: Blockly.Themes.Classic,
});
Blockly.Themes.TestFont.setFontStyle({
  family: '"Times New Roman", Times, serif',
  weight: null, // Use default font-weight
  size: 16,
});

/**
 * Thingz theme.
 */
Blockly.Themes.Thingz = Blockly.Theme.defineTheme("thingz", {
  base: Blockly.Themes.Zelos,
});

Blockly.Themes.Thingz.setBlockStyle("display_blocks", { colourPrimary: "#E50E0E" });
Blockly.Themes.Thingz.setBlockStyle("io_blocks", { colourPrimary: "#FFA401" });
Blockly.Themes.Thingz.setBlockStyle("communication_blocks", { colourPrimary: "#5CBB38" });
Blockly.Themes.Thingz.setBlockStyle("sensors_blocks", { colourPrimary: "#5CB1D6" });
Blockly.Themes.Thingz.setBlockStyle("actuators_blocks", { colourPrimary: "#38BBAD" });
Blockly.Themes.Thingz.setBlockStyle("robots_blocks", { colourPrimary: "#87599D" });

Blockly.Themes.Thingz.setCategoryStyle("display_category", { colour: "#E50E0E" });
Blockly.Themes.Thingz.setCategoryStyle("io_category", { colour: "#FFA401" });
Blockly.Themes.Thingz.setCategoryStyle("communication_category", { colour: "#5CBB38" });
Blockly.Themes.Thingz.setCategoryStyle("sensors_category", { colour: "#5CB1D6" });
Blockly.Themes.Thingz.setCategoryStyle("actuators_category", { colour: "#38BBAD" });
Blockly.Themes.Thingz.setCategoryStyle("robots_category", { colour: "#87599D" });

/**
 * Holds the test theme name to Theme instance mapping.
 * @type {!Object<string, Blockly.Theme>}
 * @private
 */
Blockly.Themes.testThemes_ = {
  "Test Hats": Blockly.Themes.TestHats,
  "Test Font": Blockly.Themes.TestFont,
  Thingz: Blockly.Themes.Thingz,
};

/**
 * Get a test theme by name.
 * @param {string} value The theme name.
 * @return {Blockly.Theme} A theme object or undefined if one doesn't exist.
 * @package
 */
function getTestTheme(value) {
  return Blockly.Themes.testThemes_[value];
}

/**
 * Populate the theme changer dropdown to list the set of test themes.
 * @package
 */
function populateTestThemes() {
  var themeChanger = document.getElementById("themeChanger");
  var keys = Object.keys(Blockly.Themes.testThemes_);
  for (var i = 0, key; (key = keys[i]); i++) {
    var option = document.createElement("option");
    option.setAttribute("value", key);
    option.textContent = key;
    themeChanger.appendChild(option);
  }
}
